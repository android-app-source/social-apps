.class public Lcom/facebook/video/player/plugins/VideoCastControllerFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/7Iw;


# static fields
.field public static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/video/player/RichVideoPlayer;

.field public B:Landroid/widget/ViewAnimator;

.field public C:Landroid/widget/ImageView;

.field private D:Lcom/facebook/widget/text/BetterTextView;

.field public E:Lcom/facebook/resources/ui/FbButton;

.field public F:Lcom/facebook/resources/ui/FbButton;

.field private G:Landroid/widget/LinearLayout;

.field public H:Landroid/widget/SeekBar;

.field private I:Lcom/facebook/resources/ui/FbTextView;

.field private J:Lcom/facebook/resources/ui/FbTextView;

.field private K:LX/384;

.field public L:I

.field private M:I

.field private N:I

.field public O:Z

.field public P:LX/7O1;

.field public Q:I

.field public R:LX/7Ny;

.field public o:LX/38h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/7J3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/37f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/37Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/37e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public x:Lcom/facebook/widget/text/BetterTextView;

.field public y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public z:Landroid/widget/ViewAnimator;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1201063
    const-class v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    sput-object v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->m:Ljava/lang/Class;

    .line 1201064
    const-class v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1201074
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1201075
    iput v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->M:I

    .line 1201076
    iput v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->N:I

    .line 1201077
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->O:Z

    .line 1201078
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->Q:I

    .line 1201079
    return-void
.end method

.method private a(Landroid/widget/ViewAnimator;I)V
    .locals 2

    .prologue
    .line 1201070
    invoke-virtual {p0, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ViewAnimator;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 1201071
    invoke-virtual {p1}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1201072
    invoke-virtual {p1, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 1201073
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/384;)V
    .locals 2

    .prologue
    .line 1201065
    iput-object p1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->K:LX/384;

    .line 1201066
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    .line 1201067
    iget-object v1, p1, LX/384;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1201068
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1201069
    return-void
.end method

.method public static c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1200820
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    .line 1200821
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->p:LX/0wM;

    const v2, 0x7f0207e2

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1200822
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->p:LX/0wM;

    const v3, 0x7f0208b7

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1200823
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v2, v4, v1, v4}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1200824
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 1200825
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1200826
    return-void

    .line 1200827
    :cond_0
    const v0, -0x6e685d

    goto :goto_0
.end method

.method private d(Z)V
    .locals 1

    .prologue
    .line 1201055
    const v0, 0x7f0d307f

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    .line 1201056
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201057
    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 1201058
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201059
    if-eqz p1, :cond_0

    .line 1201060
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 1201061
    :goto_0
    return-void

    .line 1201062
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_0
.end method

.method public static v(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1201005
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1201006
    iget-object v2, v1, LX/7O1;->a:LX/7JN;

    move-object v1, v2

    .line 1201007
    iget-object v2, v1, LX/7JN;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1201008
    invoke-virtual {v0, v1}, LX/37Y;->a(Ljava/lang/String;)Z

    move-result v0

    .line 1201009
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->g()LX/38p;

    move-result-object v1

    .line 1201010
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->s()LX/38j;

    move-result-object v2

    .line 1201011
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1201012
    invoke-virtual {v1}, LX/38p;->a()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1201013
    if-eqz v0, :cond_2

    .line 1201014
    sget-object v0, LX/7Ny;->CASTING_STOPPED:LX/7Ny;

    .line 1201015
    :goto_0
    move-object v0, v0

    .line 1201016
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    if-ne v1, v0, :cond_0

    .line 1201017
    :goto_1
    return-void

    .line 1201018
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1201019
    sget-object v1, LX/7Nw;->a:[I

    invoke-virtual {v0}, LX/7Ny;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1201020
    :cond_1
    :goto_2
    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    .line 1201021
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w()V

    goto :goto_1

    .line 1201022
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d307c

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201023
    invoke-direct {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->d(Z)V

    .line 1201024
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f081a74

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1201025
    invoke-static {p0, v4}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V

    .line 1201026
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081a71

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1201027
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_2

    .line 1201028
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d307d

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201029
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d307f

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201030
    invoke-direct {p0, v4}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->d(Z)V

    .line 1201031
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f081a75

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1201032
    invoke-static {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V

    .line 1201033
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081a72

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1201034
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1201035
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_2

    .line 1201036
    :pswitch_2
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d307d

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201037
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d307f

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201038
    invoke-direct {p0, v4}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->d(Z)V

    .line 1201039
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f081a75

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1201040
    invoke-static {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V

    .line 1201041
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081a73

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1201042
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1201043
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_2

    .line 1201044
    :pswitch_3
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    const v2, 0x7f0d3081

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a(Landroid/widget/ViewAnimator;I)V

    .line 1201045
    invoke-direct {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->d(Z)V

    .line 1201046
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f081a76

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1201047
    invoke-static {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V

    .line 1201048
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f081a73

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1201049
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1201050
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_2

    .line 1201051
    :cond_2
    invoke-virtual {v2}, LX/38j;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, LX/38j;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-object v0, LX/7Ny;->CASTING_CURRENT:LX/7Ny;

    goto/16 :goto_0

    :cond_4
    sget-object v0, LX/7Ny;->CASTING_LOADING:LX/7Ny;

    goto/16 :goto_0

    .line 1201052
    :cond_5
    invoke-virtual {v1}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1201053
    sget-object v0, LX/7Ny;->DISCONNECTED:LX/7Ny;

    goto/16 :goto_0

    .line 1201054
    :cond_6
    sget-object v0, LX/7Ny;->CONNECTING:LX/7Ny;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1200994
    const/4 v0, 0x1

    .line 1200995
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200996
    iget-object v3, v2, LX/7O1;->a:LX/7JN;

    move-object v2, v3

    .line 1200997
    invoke-virtual {v2}, LX/7JN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 1200998
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->G:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1200999
    return-void

    .line 1201000
    :cond_1
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object v3, LX/7Ny;->CASTING_LOADING:LX/7Ny;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object v3, LX/7Ny;->CASTING_CURRENT:LX/7Ny;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->R:LX/7Ny;

    sget-object v3, LX/7Ny;->CASTING_STOPPED:LX/7Ny;

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 1201001
    goto :goto_0

    .line 1201002
    :cond_2
    iget v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->M:I

    if-gez v2, :cond_0

    move v0, v1

    .line 1201003
    goto :goto_0

    .line 1201004
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public static x(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V
    .locals 4

    .prologue
    .line 1200982
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->s:LX/37f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200983
    iget-object v3, v2, LX/7O1;->a:LX/7JN;

    move-object v2, v3

    .line 1200984
    iget-object v3, v2, LX/7JN;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1200985
    invoke-virtual {v0, v1, v2}, LX/37f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200986
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string v1, "connect"

    sget-object v2, LX/38a;->CONNECTED:LX/38a;

    invoke-virtual {v0, v1, v2}, LX/7J3;->a(Ljava/lang/String;LX/38a;)V

    .line 1200987
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200988
    iget-object v2, v1, LX/7O1;->a:LX/7JN;

    move-object v1, v2

    .line 1200989
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200990
    iget-object v3, v2, LX/7O1;->b:LX/2pa;

    move-object v2, v3

    .line 1200991
    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1, v2}, LX/37Y;->b(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1200992
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->K:LX/384;

    invoke-virtual {v0, v1}, LX/37Y;->a(LX/384;)V

    .line 1200993
    return-void
.end method

.method public static y(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)Z
    .locals 1

    .prologue
    .line 1200977
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1200978
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1200979
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1200980
    const/4 v0, 0x0

    .line 1200981
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1201080
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1201081
    new-instance v1, LX/7Np;

    invoke-direct {v1, p0}, LX/7Np;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1201082
    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1200818
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    .line 1200819
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1200828
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    .line 1200829
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    .line 1200830
    iget v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    if-nez v0, :cond_1

    .line 1200831
    :cond_0
    :goto_0
    return-void

    .line 1200832
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    .line 1200833
    int-to-long v2, p1

    int-to-long v4, v0

    mul-long/2addr v2, v4

    iget v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 1200834
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    const/4 v3, 0x0

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1200835
    div-int/lit16 v0, p1, 0x3e8

    .line 1200836
    iget v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    sub-int/2addr v1, p1

    div-int/lit16 v1, v1, 0x3e8

    .line 1200837
    iget v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->M:I

    if-ne v0, v2, :cond_2

    iget v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->N:I

    if-eq v1, v2, :cond_0

    .line 1200838
    :cond_2
    iput v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->M:I

    .line 1200839
    iput v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->N:I

    .line 1200840
    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    invoke-static {v2, v3}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1200841
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v4, v1

    invoke-static {v4, v5}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1200842
    iget-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->I:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1200843
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1200844
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w()V

    goto :goto_0
.end method

.method public final dK_()V
    .locals 3

    .prologue
    .line 1200845
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    .line 1200846
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    invoke-virtual {v0}, LX/38j;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1200847
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->C:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200848
    iget-object p0, v0, LX/7O1;->a:LX/7JN;

    move-object v0, p0

    .line 1200849
    invoke-virtual {v0}, LX/7JN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0218a5

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1200850
    :goto_1
    return-void

    .line 1200851
    :cond_0
    const v0, 0x7f0213fa

    goto :goto_0

    .line 1200852
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->C:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0214b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final dL_()V
    .locals 1

    .prologue
    .line 1200853
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->O:Z

    if-nez v0, :cond_1

    .line 1200854
    iget v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    if-gtz v0, :cond_0

    .line 1200855
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->o()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    .line 1200856
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->n()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->b(I)V

    .line 1200857
    :cond_1
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2b4baacb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200858
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1200859
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-static {v1}, LX/38h;->a(LX/0QB;)LX/38h;

    move-result-object v5

    check-cast v5, LX/38h;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v1}, LX/7J3;->a(LX/0QB;)LX/7J3;

    move-result-object v8

    check-cast v8, LX/7J3;

    invoke-static {v1}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v9

    check-cast v9, LX/37f;

    invoke-static {v1}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v10

    check-cast v10, LX/37Y;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/37e;->a(LX/0QB;)LX/37e;

    move-result-object v1

    check-cast v1, LX/37e;

    iput-object v5, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->o:LX/38h;

    iput-object v6, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->p:LX/0wM;

    iput-object v7, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->q:LX/0So;

    iput-object v8, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    iput-object v9, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->s:LX/37f;

    iput-object v10, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    iput-object p1, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->u:Ljava/util/concurrent/Executor;

    iput-object v1, v4, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v:LX/37e;

    .line 1200860
    const v1, 0x7f0e0749

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1200861
    const/16 v1, 0x2b

    const v2, -0x3b23de13

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2bb0e504

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200862
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1200863
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1200864
    const v2, 0x7f031585

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x524a9162

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2e7c6d6f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200865
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v1, p0}, LX/37Y;->b(LX/7Iw;)V

    .line 1200866
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    .line 1200867
    if-nez v1, :cond_1

    .line 1200868
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200869
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 1200870
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200871
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    .line 1200872
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1200873
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    .line 1200874
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->C:Landroid/widget/ImageView;

    .line 1200875
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    .line 1200876
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    .line 1200877
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    .line 1200878
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->G:Landroid/widget/LinearLayout;

    .line 1200879
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    .line 1200880
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->I:Lcom/facebook/resources/ui/FbTextView;

    .line 1200881
    iput-object v2, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->J:Lcom/facebook/resources/ui/FbTextView;

    .line 1200882
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1200883
    const/16 v1, 0x2b

    const v2, -0x9f0f82e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1200884
    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v4

    .line 1200885
    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget v5, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->Q:I

    if-eq v4, v5, :cond_0

    .line 1200886
    iget v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->Q:I

    invoke-virtual {v1, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1200887
    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, -0x1

    const/16 v0, 0x2a

    const v1, -0x79f56159

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1200888
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1200889
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1200890
    if-eqz v1, :cond_0

    .line 1200891
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 1200892
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7a6f911b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1200893
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1200894
    :goto_0
    return-void

    .line 1200895
    :cond_0
    const v0, 0x7f0d3076

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200896
    const v0, 0x7f0d3078

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 1200897
    const v0, 0x7f0d307a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200898
    const v0, 0x7f0d307b

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    .line 1200899
    const v0, 0x7f0d307c

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1200900
    const v0, 0x7f0d307e

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    .line 1200901
    const v0, 0x7f0d3081

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->C:Landroid/widget/ImageView;

    .line 1200902
    const v0, 0x7f0d3083

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->D:Lcom/facebook/widget/text/BetterTextView;

    .line 1200903
    const v0, 0x7f0d3084

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    .line 1200904
    const v0, 0x7f0d3085

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    .line 1200905
    const v0, 0x7f0d3082

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->G:Landroid/widget/LinearLayout;

    .line 1200906
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    .line 1200907
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->H:Landroid/widget/SeekBar;

    new-instance v1, LX/7Nx;

    invoke-direct {v1, p0}, LX/7Nx;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1200908
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->I:Lcom/facebook/resources/ui/FbTextView;

    .line 1200909
    const v0, 0x7f0d132b

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->J:Lcom/facebook/resources/ui/FbTextView;

    .line 1200910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x10a0000

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 1200911
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x10a0001

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 1200912
    iget-object v5, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    invoke-virtual {v5, v3}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 1200913
    iget-object v5, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    invoke-virtual {v5, v4}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 1200914
    iget-object v5, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    invoke-virtual {v5, v3}, Landroid/widget/ViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 1200915
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->B:Landroid/widget/ViewAnimator;

    invoke-virtual {v3, v4}, Landroid/widget/ViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 1200916
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->F:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, LX/7Nr;

    invoke-direct {v4, p0}, LX/7Nr;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1200917
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->C:Landroid/widget/ImageView;

    new-instance v4, LX/7Ns;

    invoke-direct {v4, p0}, LX/7Ns;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1200918
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200919
    iget-object v5, v4, LX/7O1;->a:LX/7JN;

    move-object v4, v5

    .line 1200920
    iget-object v5, v4, LX/7JN;->c:Ljava/lang/String;

    move-object v4, v5

    .line 1200921
    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1200922
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->c(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;Z)V

    .line 1200923
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    .line 1200924
    iget-object v4, v3, LX/37Y;->m:LX/37h;

    .line 1200925
    iget-object v3, v4, LX/37h;->h:LX/384;

    move-object v4, v3

    .line 1200926
    move-object v3, v4

    .line 1200927
    if-nez v3, :cond_1

    .line 1200928
    iget-object v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v4}, LX/37Y;->b()Ljava/util/List;

    move-result-object v4

    .line 1200929
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1200930
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/384;

    .line 1200931
    :cond_1
    if-eqz v3, :cond_2

    .line 1200932
    invoke-static {p0, v3}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->a$redex0(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/384;)V

    .line 1200933
    :cond_2
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->E:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, LX/7Nt;

    invoke-direct {v4, p0}, LX/7Nt;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1200934
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200935
    iget-object v4, v3, LX/7O1;->a:LX/7JN;

    move-object v3, v4

    .line 1200936
    iget-object v4, v3, LX/7JN;->m:LX/1bf;

    move-object v3, v4

    .line 1200937
    if-eqz v3, :cond_4

    .line 1200938
    iget-object v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200939
    iget-object v5, v3, LX/1bf;->b:Landroid/net/Uri;

    move-object v5, v5

    .line 1200940
    sget-object v6, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1200941
    iget-object v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200942
    iget-object v5, v3, LX/1bf;->b:Landroid/net/Uri;

    move-object v3, v5

    .line 1200943
    sget-object v5, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1200944
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v3

    .line 1200945
    if-eqz v3, :cond_3

    .line 1200946
    invoke-virtual {v3}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v4

    iput v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->Q:I

    .line 1200947
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1200948
    :cond_3
    iget-object v6, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v7, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v6, v7}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1200949
    iget-object v6, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1200950
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1200951
    new-instance v9, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v10, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v9, v7, v10}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v8, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1200952
    new-instance v9, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v9, v7}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1200953
    const/4 v10, 0x1

    .line 1200954
    iput-boolean v10, v9, Lcom/facebook/video/player/plugins/VideoPlugin;->d:Z

    .line 1200955
    invoke-virtual {v8, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1200956
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    move-object v7, v8

    .line 1200957
    invoke-virtual {v6, v7}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    .line 1200958
    iget-object v6, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    invoke-virtual {v6}, Landroid/widget/ViewAnimator;->getWidth()I

    move-result v6

    int-to-double v6, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->z:Landroid/widget/ViewAnimator;

    invoke-virtual {v8}, Landroid/widget/ViewAnimator;->getHeight()I

    move-result v8

    int-to-double v8, v8

    div-double/2addr v6, v8

    .line 1200959
    iget-object v8, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200960
    iget-object v9, v8, LX/7O1;->b:LX/2pa;

    move-object v8, v9

    .line 1200961
    invoke-virtual {v8}, LX/2pa;->g()LX/2pZ;

    move-result-object v8

    .line 1200962
    iput-wide v6, v8, LX/2pZ;->e:D

    .line 1200963
    move-object v6, v8

    .line 1200964
    invoke-virtual {v6}, LX/2pZ;->b()LX/2pa;

    move-result-object v6

    .line 1200965
    iget-object v7, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v7, v6}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1200966
    iget-object v6, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200967
    iget-object v7, v6, LX/7O1;->c:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v6, v7

    .line 1200968
    iget-object v7, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->A:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->k()Z

    move-result v6

    :goto_2
    sget-object v8, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-virtual {v7, v6, v8}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1200969
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->v(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;)V

    .line 1200970
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->t:LX/37Y;

    invoke-virtual {v0, p0}, LX/37Y;->a(LX/7Iw;)V

    goto/16 :goto_0

    .line 1200971
    :cond_4
    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->r:LX/7J3;

    const-string v4, "fetch_background_image"

    invoke-virtual {v3, v4}, LX/7J3;->a(Ljava/lang/String;)LX/7J2;

    move-result-object v3

    .line 1200972
    iget-object v4, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->o:LX/38h;

    iget-object v5, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1200973
    iget-object v6, v5, LX/7O1;->a:LX/7JN;

    move-object v5, v6

    .line 1200974
    iget-object v6, v5, LX/7JN;->a:Ljava/lang/String;

    move-object v5, v6

    .line 1200975
    invoke-virtual {v4, v5}, LX/38h;->a(Ljava/lang/String;)LX/1Zp;

    move-result-object v4

    new-instance v5, LX/7Nq;

    invoke-direct {v5, p0, v3}, LX/7Nq;-><init>(Lcom/facebook/video/player/plugins/VideoCastControllerFragment;LX/7J2;)V

    iget-object v3, p0, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->u:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_1

    .line 1200976
    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method
