.class public Lcom/facebook/video/player/plugins/PopoutButtonPlugin;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/7Ro;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200120
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200122
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1200124
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200125
    new-instance v0, LX/7NH;

    invoke-direct {v0, p0}, LX/7NH;-><init>(Lcom/facebook/video/player/plugins/PopoutButtonPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->b:Landroid/view/View$OnClickListener;

    .line 1200126
    const-class v0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1200127
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200128
    const v0, 0x7f030fea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200129
    const v0, 0x7f0d265d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1200130
    iget-object v1, p0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1200131
    :goto_0
    return-void

    .line 1200132
    :cond_0
    const v0, 0x7f03047a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;

    invoke-static {v0}, LX/7Ro;->a(LX/0QB;)LX/7Ro;

    move-result-object v0

    check-cast v0, LX/7Ro;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->a:LX/7Ro;

    return-void
.end method


# virtual methods
.method public final isEnabled()Z
    .locals 5

    .prologue
    .line 1200133
    iget-object v0, p0, Lcom/facebook/video/player/plugins/PopoutButtonPlugin;->a:LX/7Ro;

    .line 1200134
    iget-object v1, v0, LX/7Ro;->a:LX/0W3;

    sget-wide v3, LX/0X5;->hM:J

    const/4 v2, 0x0

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JZ)Z

    move-result v1

    move v0, v1

    .line 1200135
    return v0
.end method
