.class public Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/1Sj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/79m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/15W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/fbui/glyph/GlyphView;

.field private f:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field public p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1198734
    invoke-direct {p0, p1}, LX/2oy;-><init>(Landroid/content/Context;)V

    .line 1198735
    invoke-direct {p0, p1}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Landroid/content/Context;)V

    .line 1198736
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1198731
    invoke-direct {p0, p1, p2}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198732
    invoke-direct {p0, p1}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Landroid/content/Context;)V

    .line 1198733
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1198728
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198729
    invoke-direct {p0, p1}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Landroid/content/Context;)V

    .line 1198730
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1198721
    const-class v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1198722
    const v0, 0x7f03026a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198723
    const v0, 0x7f0d0907

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1198724
    const v0, 0x7f080d64

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->f:Ljava/lang/String;

    .line 1198725
    const v0, 0x7f080d65

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->n:Ljava/lang/String;

    .line 1198726
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->c:LX/0ad;

    sget-short v1, LX/34q;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->o:Z

    .line 1198727
    return-void
.end method

.method private static a(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;LX/1Sj;LX/79m;LX/0ad;LX/15W;)V
    .locals 0

    .prologue
    .line 1198720
    iput-object p1, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a:LX/1Sj;

    iput-object p2, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->b:LX/79m;

    iput-object p3, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->c:LX/0ad;

    iput-object p4, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->d:LX/15W;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    invoke-static {v3}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v0

    check-cast v0, LX/1Sj;

    invoke-static {v3}, LX/79m;->a(LX/0QB;)LX/79m;

    move-result-object v1

    check-cast v1, LX/79m;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v3}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v3

    check-cast v3, LX/15W;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;LX/1Sj;LX/79m;LX/0ad;LX/15W;)V

    return-void
.end method

.method public static setButtonState(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;Z)V
    .locals 2

    .prologue
    .line 1198714
    if-eqz p1, :cond_0

    .line 1198715
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f0207d6

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1198716
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1198717
    :goto_0
    return-void

    .line 1198718
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020781

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1198719
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1198675
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->o:Z

    if-nez v0, :cond_0

    .line 1198676
    :goto_0
    return-void

    .line 1198677
    :cond_0
    const/4 v2, 0x0

    .line 1198678
    invoke-virtual {p1}, LX/2pa;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v2

    .line 1198679
    :goto_1
    move-object v2, v0

    .line 1198680
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->a:LX/1Sj;

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 1198681
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198682
    if-eqz v0, :cond_1

    .line 1198683
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198684
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1198685
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198686
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1198687
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198688
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_2

    .line 1198689
    :cond_1
    invoke-virtual {p0}, LX/2oy;->n()V

    goto :goto_0

    .line 1198690
    :cond_2
    if-eqz p2, :cond_3

    .line 1198691
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1198692
    :cond_3
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198693
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 1198694
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_4

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->p:Z

    .line 1198695
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->p:Z

    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->setButtonState(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;Z)V

    .line 1198696
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/7MO;

    invoke-direct {v1, p0, v2}, LX/7MO;-><init>(Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1198697
    goto :goto_2

    .line 1198698
    :cond_5
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_6

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1198699
    :cond_6
    const/4 v0, 0x0

    .line 1198700
    :goto_3
    move-object v3, v0

    .line 1198701
    if-nez v3, :cond_7

    move-object v0, v2

    .line 1198702
    goto :goto_1

    .line 1198703
    :cond_7
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1198704
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1198705
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->as()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    move-object v0, v2

    .line 1198706
    goto/16 :goto_1

    .line 1198707
    :cond_9
    invoke-static {v3}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1198708
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v3, v4, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v3, v4, :cond_b

    :cond_a
    move-object v0, v2

    .line 1198709
    goto/16 :goto_1

    .line 1198710
    :cond_b
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto/16 :goto_1

    .line 1198711
    :cond_c
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1198712
    instance-of v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1198713
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_3
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 1198672
    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->p:Z

    if-nez v0, :cond_0

    .line 1198673
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->d:LX/15W;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/7MK;

    iget-object v4, p0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 1198674
    :cond_0
    return-void
.end method
