.class public Lcom/facebook/video/player/plugins/VideoControlPlugin;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Landroid/widget/ImageButton;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:Landroid/widget/ImageButton;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2qV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1201125
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1201126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1201127
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1201129
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201130
    const v0, 0x7f031587

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1201131
    const-class v0, Lcom/facebook/video/player/plugins/VideoControlPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1201132
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7O4;

    invoke-direct {v1, p0}, LX/7O4;-><init>(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1201133
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7O5;

    invoke-direct {v1, p0}, LX/7O5;-><init>(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1201134
    const v0, 0x7f0d3087

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    .line 1201135
    const v0, 0x7f0d3088

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->b:Landroid/widget/ImageButton;

    .line 1201136
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    new-instance v1, LX/7O2;

    invoke-direct {v1, p0}, LX/7O2;-><init>(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1201137
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->b:Landroid/widget/ImageButton;

    new-instance v1, LX/7O3;

    invoke-direct {v1, p0}, LX/7O3;-><init>(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1201138
    return-void
.end method

.method private a(LX/2qV;)V
    .locals 2

    .prologue
    .line 1201139
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->d:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/2qV;->PAUSED:LX/2qV;

    if-ne p1, v0, :cond_0

    .line 1201140
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    invoke-static {v0}, LX/7QU;->b(Landroid/view/View;)V

    .line 1201141
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v0

    check-cast v0, LX/19m;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->c:LX/19m;

    return-void
.end method

.method public static g(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1201142
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201143
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1201144
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1201145
    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1201146
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1201147
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1201148
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a(LX/2qV;)V

    .line 1201149
    return-void

    .line 1201150
    :cond_0
    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 1201151
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1201152
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1201153
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1201154
    iget-object v1, p0, Lcom/facebook/video/player/plugins/VideoControlPlugin;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1201155
    invoke-static {p0}, Lcom/facebook/video/player/plugins/VideoControlPlugin;->g(Lcom/facebook/video/player/plugins/VideoControlPlugin;)V

    .line 1201156
    return-void
.end method
