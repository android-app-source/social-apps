.class public Lcom/facebook/video/player/plugins/FullScreenCastPlugin;
.super LX/7Mr;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final o:Lcom/facebook/common/callercontext/CallerContext;

.field private static final p:LX/0Tn;


# instance fields
.field private A:Z

.field public B:I

.field public C:I

.field private D:LX/7Mr;

.field private E:Landroid/widget/ProgressBar;

.field private F:LX/0hs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Z

.field private H:Landroid/widget/TextView;

.field public I:LX/7J3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private J:LX/38h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private K:LX/7Kk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0AU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:LX/37f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private N:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private O:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private P:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Q:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/37Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private S:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private U:LX/2pa;

.field private V:LX/7JN;

.field public a:Landroid/widget/SeekBar;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private q:LX/7Ml;

.field public r:Landroid/widget/ImageView;

.field private final s:Landroid/animation/Animator$AnimatorListener;

.field private t:Lcom/facebook/fbui/glyph/GlyphButton;

.field private u:Lcom/facebook/fbui/glyph/GlyphButton;

.field private v:Lcom/facebook/fbui/glyph/GlyphButton;

.field private w:Landroid/view/View;

.field private x:LX/03R;

.field public y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private z:Ljava/lang/Double;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1199265
    const-class v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    sput-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->b:Ljava/lang/Class;

    .line 1199266
    const-class v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->o:Lcom/facebook/common/callercontext/CallerContext;

    .line 1199267
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "cast_media_tool_tip_has_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->p:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199257
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199258
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199268
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1199270
    invoke-direct {p0, p1, p2, p3}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199271
    new-instance v0, LX/7Mf;

    invoke-direct {v0, p0}, LX/7Mf;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->s:Landroid/animation/Animator$AnimatorListener;

    .line 1199272
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    .line 1199273
    iput-boolean v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->G:Z

    .line 1199274
    const-class v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1199275
    sget-object v0, LX/03r;->Cover_Image_Plugin:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1199276
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->A:Z

    .line 1199277
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1199278
    new-instance v0, LX/7Ml;

    invoke-direct {v0, p0}, LX/7Ml;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->q:LX/7Ml;

    .line 1199279
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mn;

    invoke-direct {v1, p0}, LX/7Mn;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199280
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mm;

    invoke-direct {v1, p0}, LX/7Mm;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199281
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->K:LX/7Kk;

    invoke-virtual {v0, v1}, LX/37Y;->a(LX/7Iw;)V

    .line 1199282
    return-void
.end method

.method public static A(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1199283
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_0

    .line 1199284
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->L(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199285
    :goto_0
    return-void

    .line 1199286
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->G()V

    .line 1199287
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getCurrentVideoId(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)Ljava/lang/String;

    move-result-object v3

    .line 1199288
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->O:LX/0Uh;

    sget v4, LX/19n;->c:I

    invoke-virtual {v0, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 1199289
    :goto_1
    if-eqz v0, :cond_3

    .line 1199290
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->M:LX/37f;

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, LX/37f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199291
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    const-string v3, "auto_connect"

    sget-object v4, LX/38a;->CONNECTED:LX/38a;

    invoke-virtual {v0, v3, v4}, LX/7J3;->a(Ljava/lang/String;LX/38a;)V

    .line 1199292
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v3, v4}, LX/37Y;->b(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1199293
    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    invoke-virtual {v3, v0}, LX/37Y;->a(LX/384;)V

    .line 1199294
    :goto_2
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199295
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199296
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    const-string v2, "plugin.showCastScreen"

    invoke-virtual {v0, v2}, LX/7J3;->a(Ljava/lang/String;)LX/7J2;

    move-result-object v0

    .line 1199297
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1199298
    new-instance v3, LX/7O0;

    invoke-direct {v3}, LX/7O0;-><init>()V

    move-object v3, v3

    .line 1199299
    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    .line 1199300
    if-eqz v4, :cond_1

    .line 1199301
    iput-object v4, v3, LX/7O0;->a:LX/7JN;

    .line 1199302
    :cond_1
    move-object v3, v3

    .line 1199303
    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    .line 1199304
    iput-object v4, v3, LX/7O0;->b:LX/2pa;

    .line 1199305
    move-object v3, v3

    .line 1199306
    iget-object v4, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1199307
    iput-object v4, v3, LX/7O0;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1199308
    move-object v3, v3

    .line 1199309
    new-instance v4, LX/7O1;

    iget-object v5, v3, LX/7O0;->a:LX/7JN;

    iget-object v6, v3, LX/7O0;->b:LX/2pa;

    iget-object v7, v3, LX/7O0;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {v4, v5, v6, v7}, LX/7O1;-><init>(LX/7JN;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;)V

    move-object v3, v4

    .line 1199310
    new-instance v5, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;

    invoke-direct {v5}, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;-><init>()V

    .line 1199311
    iput-object v3, v5, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1199312
    iget-object v4, v5, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->P:LX/7O1;

    .line 1199313
    iget-object v3, v4, LX/7O1;->a:LX/7JN;

    move-object v4, v3

    .line 1199314
    iget v3, v4, LX/7JN;->i:I

    move v4, v3

    .line 1199315
    iput v4, v5, Lcom/facebook/video/player/plugins/VideoCastControllerFragment;->L:I

    .line 1199316
    const-class v4, LX/0ew;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ew;

    .line 1199317
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199318
    invoke-interface {v4}, LX/0ew;->iC_()LX/0gc;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1199319
    invoke-interface {v4}, LX/0ew;->iC_()LX/0gc;

    move-result-object v4

    invoke-virtual {v4}, LX/0gc;->b()Z

    .line 1199320
    invoke-virtual {v0, v1}, LX/7J2;->a(Z)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 1199321
    goto/16 :goto_1

    .line 1199322
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v2, v3}, LX/37Y;->a(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)Z

    goto :goto_2
.end method

.method private B()V
    .locals 3

    .prologue
    .line 1199323
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->z:Ljava/lang/Double;

    if-nez v0, :cond_1

    .line 1199324
    :cond_0
    :goto_0
    return-void

    .line 1199325
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    .line 1199326
    iget-object v1, v0, LX/7JN;->l:LX/1bf;

    move-object v0, v1

    .line 1199327
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    .line 1199328
    iget-object v2, v1, LX/7JN;->m:LX/1bf;

    move-object v1, v2

    .line 1199329
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 1199330
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->C(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199331
    if-eqz v1, :cond_3

    .line 1199332
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1199333
    iget-object v2, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v2

    .line 1199334
    sget-object v2, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1199335
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J:LX/38h;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/38h;->a(Ljava/lang/String;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/7Mi;

    invoke-direct {v1, p0}, LX/7Mi;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->T:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static C(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 11

    .prologue
    .line 1199336
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->x()I

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->y()I

    .line 1199337
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1199338
    :goto_0
    return-void

    .line 1199339
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->x()I

    move-result v0

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->y()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1199340
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->x()I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->y()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->z:Ljava/lang/Double;

    .line 1199341
    :cond_1
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->z:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-boolean v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->A:Z

    .line 1199342
    const/4 v10, 0x0

    move-object v5, v0

    move-object v6, v1

    move-wide v7, v2

    move v9, v4

    invoke-static/range {v5 .. v10}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;DZZ)V

    .line 1199343
    goto :goto_0
.end method

.method public static D(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 6

    .prologue
    .line 1199344
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    invoke-virtual {v0}, LX/38j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199345
    const-string v2, "plugin.pause"

    .line 1199346
    const/4 v1, 0x3

    .line 1199347
    const v0, 0x7f020bbf

    .line 1199348
    :goto_0
    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->r:Landroid/widget/ImageView;

    const/16 v4, 0xfa

    iget-object v5, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->s:Landroid/animation/Animator$AnimatorListener;

    invoke-static {v3, v4, v0, v5}, LX/2pC;->a(Landroid/widget/ImageView;IILandroid/animation/Animator$AnimatorListener;)V

    .line 1199349
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    invoke-virtual {v0, v2, v1}, LX/7J3;->a(Ljava/lang/String;I)V

    .line 1199350
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->p()V

    .line 1199351
    return-void

    .line 1199352
    :cond_0
    const-string v2, "plugin.play"

    .line 1199353
    const/4 v1, 0x2

    .line 1199354
    const v0, 0x7f020bc0

    goto :goto_0
.end method

.method private E()Z
    .locals 1

    .prologue
    .line 1199355
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 1199356
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1}, LX/37Y;->a(Lcom/facebook/video/engine/VideoPlayerParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 15

    .prologue
    .line 1199450
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199451
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->S:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    .line 1199452
    iget-object v3, v2, LX/2pb;->D:LX/04G;

    move-object v2, v3

    .line 1199453
    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->h()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->s()LX/04D;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v6, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iget-object v7, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v7}, LX/37Y;->g()LX/38p;

    move-result-object v7

    invoke-virtual {v7}, LX/38p;->a()Z

    move-result v7

    .line 1199454
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v9, LX/0JC;->CHROMECAST_CAST_CLICKED:LX/0JC;

    iget-object v9, v9, LX/0JC;->value:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v9, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v9, v9, LX/04F;->value:Ljava/lang/String;

    int-to-float v10, v3

    const/high16 v11, 0x447a0000    # 1000.0f

    div-float/2addr v10, v11

    float-to-double v10, v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 1199455
    if-eqz v7, :cond_0

    .line 1199456
    sget-object v8, LX/0JB;->CAST_BUTTON_CLICK_TYPE:LX/0JB;

    iget-object v8, v8, LX/0JB;->value:Ljava/lang/String;

    sget-object v10, LX/0JA;->SHOW_CAST_MENU:LX/0JA;

    iget-object v10, v10, LX/0JA;->value:Ljava/lang/String;

    invoke-virtual {v9, v8, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    :goto_0
    move-object v8, v0

    move-object v10, v4

    move-object v11, v1

    move v12, v6

    move-object v13, v5

    move-object v14, v2

    .line 1199457
    invoke-static/range {v8 .. v14}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 1199458
    return-void

    .line 1199459
    :cond_0
    sget-object v8, LX/0JB;->CAST_BUTTON_CLICK_TYPE:LX/0JB;

    iget-object v8, v8, LX/0JB;->value:Ljava/lang/String;

    sget-object v10, LX/0JA;->ATTEMPT_TO_CONNECT:LX/0JA;

    iget-object v10, v10, LX/0JA;->value:Ljava/lang/String;

    invoke-virtual {v9, v8, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private H()V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1199357
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199358
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->S:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v3, p0, LX/2oy;->j:LX/2pb;

    .line 1199359
    iget-object v4, v3, LX/2pb;->D:LX/04G;

    move-object v3, v4

    .line 1199360
    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v5}, LX/2pb;->s()LX/04D;

    move-result-object v5

    sget-object v6, LX/38o;->a:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    .line 1199361
    iget v9, v8, LX/7JN;->p:I

    move v8, v9

    .line 1199362
    iget-object v9, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1199363
    return-void
.end method

.method public static I(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1199364
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getCurrentVideoId(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)Ljava/lang/String;

    move-result-object v4

    .line 1199365
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0, v4}, LX/37Y;->b(Ljava/lang/String;)Z

    move-result v5

    .line 1199366
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v6

    .line 1199367
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->s()LX/38j;

    move-result-object v7

    .line 1199368
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, LX/38p;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v5, :cond_1

    move v0, v1

    .line 1199369
    :goto_0
    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p0, v8, v2

    aput-object v6, v8, v1

    const/4 v1, 0x2

    aput-object v7, v8, v1

    const/4 v1, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v8, v1

    const/4 v1, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v8, v1

    .line 1199370
    invoke-virtual {v7}, LX/38j;->d()Z

    move-result v6

    .line 1199371
    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->setSeekBarVisibility(I)V

    .line 1199372
    iget-object v7, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->E:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v1, :cond_3

    if-nez v6, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v7, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1199373
    iget-object v6, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->w:Landroid/view/View;

    if-eqz v0, :cond_4

    move v1, v2

    :goto_3
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1199374
    if-eqz v0, :cond_5

    .line 1199375
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->K()V

    .line 1199376
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1199377
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B()V

    .line 1199378
    sget-object v1, LX/7Lu;->CAST_INITIATED:LX/7Lu;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(LX/7Lu;)V

    .line 1199379
    :goto_4
    if-eqz v0, :cond_6

    .line 1199380
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->N()V

    .line 1199381
    :cond_0
    :goto_5
    return-void

    :cond_1
    move v0, v2

    .line 1199382
    goto :goto_0

    :cond_2
    move v1, v3

    .line 1199383
    goto :goto_1

    :cond_3
    move v1, v3

    .line 1199384
    goto :goto_2

    :cond_4
    move v1, v3

    .line 1199385
    goto :goto_3

    .line 1199386
    :cond_5
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1199387
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1199388
    sget-object v1, LX/7Lu;->CAST_STOPPED:LX/7Lu;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(LX/7Lu;)V

    goto :goto_4

    .line 1199389
    :cond_6
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0, v4}, LX/37Y;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1199390
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->P()V

    goto :goto_5

    .line 1199391
    :cond_7
    if-eqz v5, :cond_0

    .line 1199392
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->O()V

    goto :goto_5
.end method

.method public static J(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 1199393
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->E()Z

    move-result v3

    .line 1199394
    iget-object v4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v4}, LX/37Y;->g()LX/38p;

    move-result-object v4

    invoke-virtual {v4}, LX/38p;->c()Z

    move-result v4

    .line 1199395
    iget-object v5, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getCurrentVideoId(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/37Y;->b(Ljava/lang/String;)Z

    move-result v5

    .line 1199396
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p0, v6, v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v7, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v8, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v7

    .line 1199397
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v0, :cond_5

    .line 1199398
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1199399
    if-nez v4, :cond_0

    if-nez v5, :cond_3

    .line 1199400
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1199401
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->v:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1199402
    :goto_2
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->M(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199403
    return-void

    :cond_1
    move v0, v1

    .line 1199404
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1199405
    goto :goto_1

    .line 1199406
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1199407
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->v:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v3, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    .line 1199408
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v3, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1199409
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->v:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_2

    :cond_6
    move v1, v2

    .line 1199410
    goto :goto_4
.end method

.method private K()V
    .locals 2

    .prologue
    .line 1199411
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->f()Ljava/lang/String;

    move-result-object v0

    .line 1199412
    if-eqz v0, :cond_0

    .line 1199413
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1199414
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1199415
    :goto_0
    return-void

    .line 1199416
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static L(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 2

    .prologue
    .line 1199417
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1199418
    :goto_0
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1}, Lcom/facebook/fbui/glyph/GlyphButton;->isClickable()Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 1199419
    :goto_1
    return-void

    .line 1199420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1199421
    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1199422
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setClickable(Z)V

    .line 1199423
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setLongClickable(Z)V

    .line 1199424
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    if-eqz v1, :cond_2

    .line 1199425
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setClickable(Z)V

    .line 1199426
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setLongClickable(Z)V

    .line 1199427
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->M(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    goto :goto_1
.end method

.method public static M(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1199428
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphButton;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphButton;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->G:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 1199429
    :goto_0
    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->N:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->p:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 1199430
    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->Q:LX/0ad;

    sget-short v4, LX/0ws;->e:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1199431
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    if-eqz v0, :cond_1

    .line 1199432
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1199433
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    .line 1199434
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 1199435
    goto :goto_0

    .line 1199436
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    if-nez v0, :cond_4

    move v2, v1

    :cond_4
    invoke-static {v2}, LX/03g;->a(Z)V

    .line 1199437
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    .line 1199438
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    const/16 v2, 0x1f40

    .line 1199439
    iput v2, v0, LX/0hs;->t:I

    .line 1199440
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    const v2, 0x7f080ddc

    invoke-virtual {v0, v2}, LX/0hs;->a(I)V

    .line 1199441
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    const v2, 0x7f080ddd

    invoke-virtual {v0, v2}, LX/0hs;->b(I)V

    .line 1199442
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->P:LX/0wM;

    const v3, 0x7f0209f6

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1199443
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1199444
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1199445
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->N:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->p:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1
.end method

.method private N()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1199446
    const/4 v0, 0x1

    new-array v0, v0, [LX/2ol;

    new-instance v1, LX/2qb;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-direct {v1, v2}, LX/2qb;-><init>(LX/04g;)V

    aput-object v1, v0, v3

    invoke-direct {p0, v3, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Z[LX/2ol;)Z

    .line 1199447
    return-void
.end method

.method private O()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1199259
    iget v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    mul-int/2addr v0, v2

    iget-object v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    div-int/2addr v0, v2

    .line 1199260
    iget v2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    if-ge v0, v2, :cond_1

    .line 1199261
    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [LX/2ol;

    new-instance v3, LX/2qc;

    sget-object v4, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-direct {v3, v0, v4}, LX/2qc;-><init>(ILX/04g;)V

    aput-object v3, v2, v1

    new-instance v0, LX/2qa;

    sget-object v1, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-direct {v0, v1}, LX/2qa;-><init>(LX/04g;)V

    aput-object v0, v2, v5

    invoke-direct {p0, v5, v2}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Z[LX/2ol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1199262
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H()V

    .line 1199263
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1199264
    goto :goto_0
.end method

.method private P()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1199448
    const/4 v0, 0x3

    new-array v0, v0, [LX/2ol;

    new-instance v1, LX/2qc;

    sget-object v2, LX/04g;->BY_CHROME_CAST:LX/04g;

    invoke-direct {v1, v5, v2}, LX/2qc;-><init>(ILX/04g;)V

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v2, LX/2ou;

    iget-object v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    sget-object v4, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    invoke-direct {v2, v3, v4}, LX/2ou;-><init>(Ljava/lang/String;LX/2qV;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/2op;

    iget v3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    invoke-direct {v2, v3}, LX/2op;-><init>(I)V

    aput-object v2, v0, v1

    invoke-direct {p0, v5, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Z[LX/2ol;)Z

    .line 1199449
    return-void
.end method

.method private a(LX/7Lu;)V
    .locals 2

    .prologue
    .line 1199159
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    .line 1199160
    if-eqz v0, :cond_0

    .line 1199161
    new-instance v1, LX/7Lv;

    invoke-direct {v1, p1}, LX/7Lv;-><init>(LX/7Lu;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1199162
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;LX/7J3;LX/38h;LX/7Kk;LX/0AU;LX/37f;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0wM;LX/0ad;LX/37Y;LX/1C2;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1199158
    iput-object p1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    iput-object p2, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J:LX/38h;

    iput-object p3, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->K:LX/7Kk;

    iput-object p4, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->L:LX/0AU;

    iput-object p5, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->M:LX/37f;

    iput-object p6, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->N:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p7, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->O:LX/0Uh;

    iput-object p8, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->P:LX/0wM;

    iput-object p9, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->Q:LX/0ad;

    iput-object p10, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iput-object p11, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->S:LX/1C2;

    iput-object p12, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->T:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-static {v12}, LX/7J3;->a(LX/0QB;)LX/7J3;

    move-result-object v1

    check-cast v1, LX/7J3;

    invoke-static {v12}, LX/38h;->a(LX/0QB;)LX/38h;

    move-result-object v2

    check-cast v2, LX/38h;

    invoke-static {v12}, LX/7Kk;->a(LX/0QB;)LX/7Kk;

    move-result-object v3

    check-cast v3, LX/7Kk;

    invoke-static {v12}, LX/0AU;->a(LX/0QB;)LX/0AU;

    move-result-object v4

    check-cast v4, LX/0AU;

    invoke-static {v12}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v5

    check-cast v5, LX/37f;

    invoke-static {v12}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v12}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v12}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v12}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v12}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v10

    check-cast v10, LX/37Y;

    invoke-static {v12}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v11

    check-cast v11, LX/1C2;

    invoke-static {v12}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/Executor;

    invoke-static/range {v0 .. v12}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;LX/7J3;LX/38h;LX/7Kk;LX/0AU;LX/37f;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0wM;LX/0ad;LX/37Y;LX/1C2;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private a(Z[LX/2ol;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1199163
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    invoke-virtual {v1, p1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eq v1, p1, :cond_1

    .line 1199164
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    invoke-virtual {v1}, LX/03R;->asBooleanObject()Ljava/lang/Boolean;

    .line 1199165
    :cond_0
    :goto_0
    return v0

    .line 1199166
    :cond_1
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    .line 1199167
    if-eqz v1, :cond_0

    .line 1199168
    array-length v2, p2

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p2, v0

    .line 1199169
    invoke-virtual {v1, v3}, LX/2oj;->a(LX/2ol;)V

    .line 1199170
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1199171
    :cond_2
    if-eqz p1, :cond_3

    sget-object v0, LX/03R;->NO:LX/03R;

    :goto_2
    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    .line 1199172
    const/4 v0, 0x1

    goto :goto_0

    .line 1199173
    :cond_3
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_2
.end method

.method public static getCurrentVideoId(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1199174
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 4

    .prologue
    .line 1199175
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-wide v0, v0, LX/2pa;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-wide v0, v0, LX/2pa;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->z:Ljava/lang/Double;

    .line 1199176
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    .line 1199177
    if-lez v0, :cond_0

    .line 1199178
    iput v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    .line 1199179
    :cond_0
    return-void

    .line 1199180
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    .line 1199181
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1199182
    :cond_0
    sget-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->b:Ljava/lang/Class;

    const-string v1, "%s.onLoad(%s, %s): VideoId is missing"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1199183
    :goto_0
    return-void

    .line 1199184
    :cond_1
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1199185
    iput-object p1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    .line 1199186
    invoke-direct {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->z()V

    .line 1199187
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199188
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->s()LX/04D;

    move-result-object v1

    invoke-static {v0, v1}, LX/7JO;->a(LX/2pa;LX/04D;)LX/7JN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    .line 1199189
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->h()I

    move-result v1

    .line 1199190
    iput v1, v0, LX/7JN;->o:I

    .line 1199191
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->V:LX/7JN;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->t()I

    move-result v1

    .line 1199192
    iput v1, v0, LX/7JN;->p:I

    .line 1199193
    if-eqz p2, :cond_3

    .line 1199194
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->q:LX/7Ml;

    invoke-virtual {v0, v1}, LX/37Y;->a(LX/7Iw;)V

    .line 1199195
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1199196
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    const-string v1, "plugin.reconnect"

    sget-object v2, LX/38a;->CONNECTED:LX/38a;

    invoke-virtual {v0, v1, v2}, LX/7J3;->a(Ljava/lang/String;LX/38a;)V

    .line 1199197
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->h()V

    .line 1199198
    :cond_2
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->x:LX/03R;

    .line 1199199
    :cond_3
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->L(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199200
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199201
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1199202
    invoke-super {p0}, LX/7Mr;->d()V

    .line 1199203
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->L(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199204
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    .line 1199205
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->q:LX/7Ml;

    invoke-virtual {v0, v1}, LX/37Y;->b(LX/7Iw;)V

    .line 1199206
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    if-eqz v0, :cond_0

    .line 1199207
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1199208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->F:LX/0hs;

    .line 1199209
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1199210
    invoke-super {p0}, LX/7Mr;->f()V

    .line 1199211
    const v0, 0x7f0d0b87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1199212
    const v0, 0x7f0d1328

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->w:Landroid/view/View;

    .line 1199213
    const v0, 0x7f0d08e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1199214
    const v0, 0x7f0d132c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->v:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1199215
    const v0, 0x7f0d1326

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->H:Landroid/widget/TextView;

    .line 1199216
    const v0, 0x7f0d1327

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->E:Landroid/widget/ProgressBar;

    .line 1199217
    const v0, 0x7f0d094f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->r:Landroid/widget/ImageView;

    .line 1199218
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a:Landroid/widget/SeekBar;

    .line 1199219
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1199220
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mk;

    invoke-direct {v1, p0}, LX/7Mk;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199221
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mq;

    invoke-direct {v1, p0}, LX/7Mq;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199222
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Mo;

    invoke-direct {v1, p0}, LX/7Mo;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199223
    return-void
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1199224
    const/4 v0, 0x0

    return v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1199225
    const v0, 0x7f03072c

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1199226
    new-instance v0, LX/7Mg;

    invoke-direct {v0, p0}, LX/7Mg;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199227
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199228
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->v:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199229
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a:Landroid/widget/SeekBar;

    new-instance v1, LX/7Mp;

    invoke-direct {v1, p0}, LX/7Mp;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1199230
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 1199231
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->U:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_1

    .line 1199232
    :cond_0
    :goto_0
    return-void

    .line 1199233
    :cond_1
    iget v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->C:I

    iget v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    .line 1199234
    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, LX/7Mr;->a(LX/7Mr;IIZ)V

    .line 1199235
    goto :goto_0
.end method

.method public setOtherControls(LX/7Mr;)V
    .locals 4

    .prologue
    .line 1199236
    iput-object p1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->D:LX/7Mr;

    .line 1199237
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->D:LX/7Mr;

    if-eqz v0, :cond_0

    .line 1199238
    const v0, 0x7f0d08e4

    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    .line 1199239
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1199240
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1199241
    :goto_0
    move-object v0, v0

    .line 1199242
    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1199243
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    if-nez v0, :cond_1

    .line 1199244
    sget-object v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->b:Ljava/lang/Class;

    const-string v1, "%s.setOtherControls(_): No alternative media button is provided"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1199245
    :cond_0
    :goto_1
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199246
    invoke-static {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199247
    return-void

    .line 1199248
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->u:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/7Mh;

    invoke-direct {v1, p0}, LX/7Mh;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSeekBarVisibility(I)V
    .locals 2

    .prologue
    .line 1199249
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->D:LX/7Mr;

    if-eqz v0, :cond_0

    .line 1199250
    iget-object v1, p0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->D:LX/7Mr;

    if-nez p1, :cond_2

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, LX/7Mr;->setSeekBarVisibility(I)V

    .line 1199251
    :cond_0
    invoke-super {p0, p1}, LX/7Mr;->setSeekBarVisibility(I)V

    .line 1199252
    if-nez p1, :cond_1

    .line 1199253
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->i()V

    .line 1199254
    :cond_1
    return-void

    .line 1199255
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1199256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
