.class public Lcom/facebook/video/player/plugins/OverflowMenuPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lk;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Lcom/facebook/graphql/model/GraphQLMedia;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200060
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200061
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200020
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200021
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1200022
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200023
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->d:Z

    .line 1200024
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->getContentViewResource()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200025
    const v0, 0x7f0d0c13

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    .line 1200026
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    new-instance v1, LX/7NC;

    invoke-direct {v1, p0}, LX/7NC;-><init>(Lcom/facebook/video/player/plugins/OverflowMenuPlugin;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1200027
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7ND;

    invoke-direct {v1, p0}, LX/7ND;-><init>(Lcom/facebook/video/player/plugins/OverflowMenuPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200028
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 1200029
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1200030
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1200031
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1200032
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto :goto_0

    .line 1200033
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(LX/2pa;)Z
    .locals 2

    .prologue
    .line 1200034
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1200035
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 1200036
    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LX/2pa;->f()LX/3HY;

    move-result-object v0

    sget-object v1, LX/3HY;->EXTRA_SMALL:LX/3HY;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 1200037
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1200038
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1200039
    :goto_0
    return-void

    .line 1200040
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1200041
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1200042
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1200043
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v0

    .line 1200044
    :goto_1
    if-eqz v3, :cond_5

    .line 1200045
    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1200046
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1200047
    invoke-static {v3}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lk;

    invoke-interface {v0}, LX/7Lk;->n()LX/1SX;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lk;

    invoke-interface {v0}, LX/7Lk;->n()LX/1SX;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1200048
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lk;

    invoke-interface {v0}, LX/7Lk;->n()LX/1SX;

    move-result-object v0

    .line 1200049
    iget-object v1, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v3}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move-object v3, v1

    .line 1200050
    goto :goto_1

    .line 1200051
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1200052
    goto :goto_3

    .line 1200053
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1200054
    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1200055
    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1200056
    return-void
.end method

.method public getContentViewResource()I
    .locals 1

    .prologue
    .line 1200057
    const v0, 0x7f03088f

    return v0
.end method

.method public setShouldPauseVideo(Z)V
    .locals 0

    .prologue
    .line 1200058
    iput-boolean p1, p0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->d:Z

    .line 1200059
    return-void
.end method
