.class public Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/widget/FbImageView;

.field public d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1201291
    const-class v0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1201292
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1201293
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1201294
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201295
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1201296
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1201297
    const/4 p2, -0x2

    .line 1201298
    const p1, 0x7f0315b7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1201299
    const p1, 0x7f0d30fc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1201300
    const p1, 0x7f0d30fd

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/FbImageView;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->c:Lcom/facebook/widget/FbImageView;

    .line 1201301
    const p1, 0x7f0d30fe

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1201302
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->a()V

    .line 1201303
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p1, p2, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1201304
    const p2, 0x7f0b06a0

    invoke-static {p0, p2}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b(Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;I)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1201305
    const p2, 0x7f0b06a1

    invoke-static {p0, p2}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b(Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;I)I

    move-result p2

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1201306
    invoke-virtual {p0, p1}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1201307
    return-void
.end method

.method public static b(Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;I)I
    .locals 1

    .prologue
    .line 1201308
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1201309
    const v0, 0x7f021a5d

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->setBackgroundResource(I)V

    .line 1201310
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1201311
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1201312
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->c:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1201313
    return-void
.end method
