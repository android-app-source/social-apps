.class public final Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7Nb;


# direct methods
.method public constructor <init>(LX/7Nb;)V
    .locals 0

    .prologue
    .line 1200427
    iput-object p1, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1200428
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget v0, v0, LX/7Nb;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1200429
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-static {v0}, LX/7Nb;->g(LX/7Nb;)V

    .line 1200430
    :goto_0
    return-void

    .line 1200431
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    .line 1200432
    iget-object v1, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget v1, v1, LX/7Nb;->b:I

    if-ge v0, v1, :cond_3

    .line 1200433
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget v0, v0, LX/7Nb;->e:I

    if-eq v0, v2, :cond_2

    .line 1200434
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    .line 1200435
    iput v2, v0, LX/7Nb;->e:I

    .line 1200436
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-static {v0}, LX/7Nb;->h(LX/7Nb;)V

    .line 1200437
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-static {v0}, LX/7Nb;->g(LX/7Nb;)V

    goto :goto_0

    .line 1200438
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget v1, v1, LX/7Nb;->b:I

    if-le v0, v1, :cond_4

    iget-object v1, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-static {v1}, LX/7Nb;->getVideoEndTime(LX/7Nb;)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1200439
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    const/4 v1, 0x1

    .line 1200440
    iput v1, v0, LX/7Nb;->e:I

    .line 1200441
    goto :goto_1

    .line 1200442
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    iget v0, v0, LX/7Nb;->e:I

    if-eq v0, v2, :cond_2

    .line 1200443
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    .line 1200444
    iput v2, v0, LX/7Nb;->e:I

    .line 1200445
    iget-object v0, p0, Lcom/facebook/video/player/plugins/TrimmedVideoLoopingPlugin$1;->a:LX/7Nb;

    invoke-static {v0}, LX/7Nb;->h(LX/7Nb;)V

    goto :goto_1
.end method
