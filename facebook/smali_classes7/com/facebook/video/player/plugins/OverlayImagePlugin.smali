.class public Lcom/facebook/video/player/plugins/OverlayImagePlugin;
.super LX/2oy;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1200068
    const-class v0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    const-string v1, "create_profile_video_android"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200069
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200070
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200071
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200072
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1200073
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200074
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    .line 1200075
    const-class v0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    invoke-static {v0, p0}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1200076
    const v0, 0x7f03039b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200077
    const v0, 0x7f0d0b87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1200078
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NF;

    invoke-direct {v1, p0}, LX/7NF;-><init>(Lcom/facebook/video/player/plugins/OverlayImagePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200079
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->b:LX/1Ad;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/OverlayImagePlugin;LX/2qV;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1200080
    sget-object v0, LX/7NE;->a:[I

    invoke-virtual {p1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1200081
    :cond_0
    iget v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    if-eq v0, v3, :cond_1

    .line 1200082
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1200083
    :cond_1
    iput v3, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    .line 1200084
    :goto_0
    return-void

    .line 1200085
    :pswitch_0
    iget v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    if-eq v0, v2, :cond_2

    .line 1200086
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1200087
    :cond_2
    iput v2, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    goto :goto_0

    .line 1200088
    :pswitch_1
    iget v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    if-ne v0, v4, :cond_0

    .line 1200089
    :cond_3
    iput v4, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->d:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1200090
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "OverlayImageParamsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1200091
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1200092
    :cond_0
    :goto_0
    return-void

    .line 1200093
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "OverlayImageParamsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1200094
    instance-of v0, v1, Landroid/net/Uri;

    if-nez v0, :cond_2

    instance-of v0, v1, LX/1bf;

    if-nez v0, :cond_2

    .line 1200095
    invoke-virtual {p0}, LX/2oy;->n()V

    goto :goto_0

    .line 1200096
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->b:LX/1Ad;

    sget-object v2, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1200097
    instance-of v2, v1, Landroid/net/Uri;

    if-eqz v2, :cond_3

    .line 1200098
    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    .line 1200099
    :goto_1
    iget-object v1, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1200100
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 1200101
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1200102
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1200103
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a$redex0(Lcom/facebook/video/player/plugins/OverlayImagePlugin;LX/2qV;)V

    goto :goto_0

    .line 1200104
    :cond_3
    check-cast v1, LX/1bf;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1200105
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1200106
    iget-object v0, p0, Lcom/facebook/video/player/plugins/OverlayImagePlugin;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1200107
    return-void
.end method
