.class public Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;
.super LX/7MM;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public c:Z

.field public final d:Landroid/view/View;

.field public final e:Landroid/view/View;

.field public final f:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1199985
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199986
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199987
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199988
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1199989
    invoke-direct {p0, p1, p2, p3}, LX/7MM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199990
    const v0, 0x7f03047e

    move v0, v0

    .line 1199991
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199992
    const v0, 0x7f0d0d69

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->d:Landroid/view/View;

    .line 1199993
    const v0, 0x7f0d0d6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->e:Landroid/view/View;

    .line 1199994
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/7NB;

    invoke-direct {v1, p0}, LX/7NB;-><init>(Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->f:Landroid/view/GestureDetector;

    .line 1199995
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->setOnTouchListener(Landroid/view/View;)V

    .line 1199996
    iget-object v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->setOnTouchListener(Landroid/view/View;)V

    .line 1199997
    return-void
.end method

.method private setOnTouchListener(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1199998
    new-instance v0, LX/7NA;

    invoke-direct {v0, p0}, LX/7NA;-><init>(Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1199999
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1200000
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->c:Z

    .line 1200001
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1200002
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/plugins/LiveVideoFullscreenEnableGesturePlugin;->c:Z

    .line 1200003
    return-void
.end method
