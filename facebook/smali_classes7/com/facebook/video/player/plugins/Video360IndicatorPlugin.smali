.class public Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:Lcom/facebook/spherical/ui/SphericalIndicator360View;

.field public b:Landroid/view/View;

.field public c:Landroid/animation/AnimatorSet;

.field private d:Landroid/animation/ObjectAnimator;

.field private e:Landroid/animation/ObjectAnimator;

.field private f:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1200653
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200654
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200651
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200652
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1200640
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200641
    const p1, 0x7f03157c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200642
    const p1, 0x7f0d305d

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/spherical/ui/SphericalIndicator360View;

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->a:Lcom/facebook/spherical/ui/SphericalIndicator360View;

    .line 1200643
    const p1, 0x7f0d305c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    .line 1200644
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/7Nk;

    invoke-direct {p2, p0}, LX/7Nk;-><init>(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200645
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/7Ni;

    invoke-direct {p2, p0}, LX/7Ni;-><init>(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200646
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/7Nj;

    invoke-direct {p2, p0}, LX/7Nj;-><init>(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200647
    invoke-static {p0}, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->h(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V

    .line 1200648
    iget-object p1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1200649
    iget-object p1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 1200650
    return-void
.end method

.method public static h(Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;)V
    .locals 11

    .prologue
    const v10, 0x3ea3d70a    # 0.32f

    const v9, 0x3e6b851f    # 0.23f

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 1200619
    invoke-virtual {p0}, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b063a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 1200620
    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    aput v0, v2, v7

    const/4 v3, 0x0

    aput v3, v2, v8

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 1200621
    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v4, v3, v7

    aput v0, v3, v8

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 1200622
    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 1200623
    iget-object v3, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    new-array v4, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v4, v7

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->f:Landroid/animation/ObjectAnimator;

    .line 1200624
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->f:Landroid/animation/ObjectAnimator;

    invoke-static {v9, v6, v10, v6}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1200625
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->f:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1200626
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    new-array v3, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v7

    invoke-static {v1, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->d:Landroid/animation/ObjectAnimator;

    .line 1200627
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->d:Landroid/animation/ObjectAnimator;

    invoke-static {v9, v6, v10, v6}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1200628
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->d:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1200629
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->a:Lcom/facebook/spherical/ui/SphericalIndicator360View;

    .line 1200630
    iget-object v1, v0, Lcom/facebook/spherical/ui/SphericalIndicator360View;->d:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1200631
    iput-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->e:Landroid/animation/ObjectAnimator;

    .line 1200632
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->e:Landroid/animation/ObjectAnimator;

    invoke-static {v9, v6, v10, v6}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1200633
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    new-array v1, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1200634
    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1200635
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    .line 1200636
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1200637
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1200638
    iget-object v1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1200639
    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 1200614
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1200615
    return-void
.end method

.method public getContainerView()Landroid/view/View;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1200618
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->b:Landroid/view/View;

    return-object v0
.end method

.method public setAnimatorSet(Landroid/animation/AnimatorSet;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1200616
    iput-object p1, p0, Lcom/facebook/video/player/plugins/Video360IndicatorPlugin;->c:Landroid/animation/AnimatorSet;

    .line 1200617
    return-void
.end method
