.class public Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1200406
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200407
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200424
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200425
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1200419
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200420
    const p1, 0x7f0313bb

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200421
    const p1, 0x7f0d2d8b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;->a:Landroid/view/View;

    .line 1200422
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/7NZ;

    invoke-direct {p2, p0}, LX/7NZ;-><init>(Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200423
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;LX/2qV;)V
    .locals 2

    .prologue
    .line 1200415
    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1200416
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1200417
    :goto_0
    return-void

    .line 1200418
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1200410
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200411
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1200412
    iget-object p1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p1

    .line 1200413
    invoke-static {p0, v0}, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;->a$redex0(Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;LX/2qV;)V

    .line 1200414
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1200408
    iget-object v0, p0, Lcom/facebook/video/player/plugins/Static360IndicatorPlugin;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1200409
    return-void
.end method
