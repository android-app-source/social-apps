.class public Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;
.super LX/7MR;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final b:Landroid/animation/Animator$AnimatorListener;

.field public final c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1198774
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198775
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198776
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198777
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1198778
    invoke-direct {p0, p1, p2, p3}, LX/7MR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198779
    new-instance v0, LX/7MQ;

    invoke-direct {v0, p0}, LX/7MQ;-><init>(Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;)V

    iput-object v0, p0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->b:Landroid/animation/Animator$AnimatorListener;

    .line 1198780
    const v0, 0x7f0d094f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->c:Landroid/widget/ImageView;

    .line 1198781
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 1198782
    iget-object v0, p0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->c:Landroid/widget/ImageView;

    const/16 v1, 0xfa

    iget-object v2, p0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->b:Landroid/animation/Animator$AnimatorListener;

    invoke-static {v0, v1, p1, v2}, LX/2pC;->a(Landroid/widget/ImageView;IILandroid/animation/Animator$AnimatorListener;)V

    .line 1198783
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1198784
    invoke-super {p0, p1}, LX/7MR;->a(Z)V

    .line 1198785
    iget-object v1, p0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1198786
    return-void

    .line 1198787
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1198788
    invoke-super {p0}, LX/7MR;->f()V

    .line 1198789
    const v0, 0x7f020bc0

    invoke-direct {p0, v0}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->c(I)V

    .line 1198790
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1198791
    invoke-super {p0}, LX/7MR;->g()V

    .line 1198792
    const v0, 0x7f020bbf

    invoke-direct {p0, v0}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;->c(I)V

    .line 1198793
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1198794
    const v0, 0x7f030298

    return v0
.end method
