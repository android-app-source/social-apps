.class public Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lj;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field public b:LX/2pa;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1199477
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1199478
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1199466
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199467
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1199472
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1199473
    const v0, 0x7f030734

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1199474
    const v0, 0x7f0d1331

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;->a:Landroid/view/View;

    .line 1199475
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;->a:Landroid/view/View;

    new-instance v1, LX/7Ms;

    invoke-direct {v1, p0}, LX/7Ms;-><init>(Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199476
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1199470
    iput-object p1, p0, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;->b:LX/2pa;

    .line 1199471
    return-void
.end method

.method public setPluginVisibility(I)V
    .locals 1

    .prologue
    .line 1199468
    iget-object v0, p0, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1199469
    return-void
.end method
