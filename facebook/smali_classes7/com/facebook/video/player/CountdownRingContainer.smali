.class public Lcom/facebook/video/player/CountdownRingContainer;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final c:I


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7Km;

.field private d:F

.field private e:I

.field private f:I

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/RectF;

.field public j:LX/7Kl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:J

.field public l:J

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1196433
    const v0, 0x7f0a00d2

    sput v0, Lcom/facebook/video/player/CountdownRingContainer;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1196426
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1196427
    new-instance v0, LX/7Km;

    invoke-direct {v0, p0}, LX/7Km;-><init>(Lcom/facebook/video/player/CountdownRingContainer;)V

    iput-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->b:LX/7Km;

    .line 1196428
    const-class v0, Lcom/facebook/video/player/CountdownRingContainer;

    invoke-static {v0, p0}, Lcom/facebook/video/player/CountdownRingContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1196429
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/CountdownRingContainer;->setWillNotDraw(Z)V

    .line 1196430
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/player/CountdownRingContainer;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1196431
    invoke-direct {p0}, Lcom/facebook/video/player/CountdownRingContainer;->c()V

    .line 1196432
    return-void
.end method

.method private a(F)I
    .locals 4

    .prologue
    .line 1196425
    invoke-virtual {p0}, Lcom/facebook/video/player/CountdownRingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1196417
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->CountdownRingContainer:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1196418
    :try_start_0
    const/16 v0, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {p0, v2}, Lcom/facebook/video/player/CountdownRingContainer;->a(F)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->d:F

    .line 1196419
    const/16 v0, 0x3

    const/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    .line 1196420
    const/16 v0, 0x1

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->e:I

    .line 1196421
    const/16 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/video/player/CountdownRingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/facebook/video/player/CountdownRingContainer;->c:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1196422
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1196423
    return-void

    .line 1196424
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1196415
    iget-object v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->i:Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1196416
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/CountdownRingContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/player/CountdownRingContainer;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->a:LX/0SG;

    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    .line 1196412
    iget-wide v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    long-to-float v0, v0

    div-float v0, v4, v0

    invoke-static {p0}, Lcom/facebook/video/player/CountdownRingContainer;->getElapsedMillisSinceCountdownStart(Lcom/facebook/video/player/CountdownRingContainer;)J

    move-result-wide v2

    long-to-float v1, v2

    mul-float/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 1196413
    iget-object v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->i:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1196414
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1196401
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    .line 1196402
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->e:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1196403
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1196404
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1196405
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1196406
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    .line 1196407
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1196408
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1196409
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1196410
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1196411
    return-void
.end method

.method public static d(Lcom/facebook/video/player/CountdownRingContainer;)V
    .locals 1

    .prologue
    .line 1196398
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    if-eqz v0, :cond_0

    .line 1196399
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    invoke-interface {v0}, LX/7Kl;->a()V

    .line 1196400
    :cond_0
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1196395
    iget v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->d:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 1196396
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/video/player/CountdownRingContainer;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/facebook/video/player/CountdownRingContainer;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->i:Landroid/graphics/RectF;

    .line 1196397
    return-void
.end method

.method public static getElapsedMillisSinceCountdownStart(Lcom/facebook/video/player/CountdownRingContainer;)J
    .locals 4

    .prologue
    .line 1196368
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/video/player/CountdownRingContainer;->k:J

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1196391
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->k:J

    .line 1196392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->m:Z

    .line 1196393
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->b:LX/7Km;

    invoke-virtual {v0}, LX/7Km;->a()V

    .line 1196394
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1196386
    iget-boolean v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->m:Z

    if-eqz v0, :cond_0

    .line 1196387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->m:Z

    .line 1196388
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->b:LX/7Km;

    invoke-virtual {v0}, LX/7Km;->b()V

    .line 1196389
    invoke-virtual {p0}, Lcom/facebook/video/player/CountdownRingContainer;->invalidate()V

    .line 1196390
    :cond_0
    return-void
.end method

.method public getCountdownDurationMillis()J
    .locals 2

    .prologue
    .line 1196385
    iget-wide v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    return-wide v0
.end method

.method public getCountdownRingContainerListener()LX/7Kl;
    .locals 1

    .prologue
    .line 1196384
    iget-object v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6aec0d0e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1196381
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1196382
    iget-object v1, p0, Lcom/facebook/video/player/CountdownRingContainer;->b:LX/7Km;

    invoke-virtual {v1}, LX/7Km;->b()V

    .line 1196383
    const/16 v1, 0x2d

    const v2, -0x7a9e172a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1196376
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1196377
    invoke-direct {p0, p1}, Lcom/facebook/video/player/CountdownRingContainer;->a(Landroid/graphics/Canvas;)V

    .line 1196378
    iget-boolean v0, p0, Lcom/facebook/video/player/CountdownRingContainer;->m:Z

    if-eqz v0, :cond_0

    .line 1196379
    invoke-direct {p0, p1}, Lcom/facebook/video/player/CountdownRingContainer;->b(Landroid/graphics/Canvas;)V

    .line 1196380
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7877d44d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1196373
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 1196374
    invoke-direct {p0}, Lcom/facebook/video/player/CountdownRingContainer;->e()V

    .line 1196375
    const/16 v1, 0x2d

    const v2, 0x65736794

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCountdownDurationMillis(J)V
    .locals 1

    .prologue
    .line 1196371
    iput-wide p1, p0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    .line 1196372
    return-void
.end method

.method public setCountdownRingContainerListener(LX/7Kl;)V
    .locals 0

    .prologue
    .line 1196369
    iput-object p1, p0, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    .line 1196370
    return-void
.end method
