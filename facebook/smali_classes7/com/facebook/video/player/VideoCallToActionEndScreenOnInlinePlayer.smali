.class public Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1198119
    const-class v0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1198117
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1198101
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198102
    const v0, 0x7f031592

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198103
    const v0, 0x7f0d30a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->b:Landroid/view/View;

    .line 1198104
    const v0, 0x7f0d30ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->c:Landroid/view/View;

    .line 1198105
    const v0, 0x7f0d30ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->d:Landroid/widget/TextView;

    .line 1198106
    const v0, 0x7f0d30af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->e:Landroid/widget/TextView;

    .line 1198107
    const v0, 0x7f0d30ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1198108
    const v0, 0x7f0d30ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->g:Landroid/widget/TextView;

    .line 1198109
    const v0, 0x7f0d30aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->h:Landroid/widget/ImageView;

    .line 1198110
    invoke-direct {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->a()V

    .line 1198111
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1198112
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1198113
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->c:Landroid/view/View;

    sget-object v1, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1198114
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1198091
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0676

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1198092
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0679

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1198093
    invoke-direct {p0, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->setReplayLabelTextSize(I)V

    .line 1198094
    invoke-direct {p0, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->setCTATitleTextSize(I)V

    .line 1198095
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0677

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->setCTASourceTextSize(I)V

    .line 1198096
    invoke-direct {p0, v1, v1}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->a(II)V

    .line 1198097
    invoke-direct {p0, v1, v1}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->b(II)V

    .line 1198098
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0675

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->setCTAContainerTopMargin(I)V

    .line 1198099
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0678

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->setCTASourceTextTopMargin(I)V

    .line 1198100
    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 1198088
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1198089
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1198090
    return-void
.end method

.method private b(II)V
    .locals 1

    .prologue
    .line 1198085
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1198086
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1198087
    return-void
.end method

.method private setCTAContainerTopMargin(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1198081
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1198082
    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1198083
    iget-object v1, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1198084
    return-void
.end method

.method private setCTASourceTextSize(I)V
    .locals 3

    .prologue
    .line 1198079
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1198080
    return-void
.end method

.method private setCTASourceTextTopMargin(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1198065
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1198066
    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1198067
    iget-object v1, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1198068
    return-void
.end method

.method private setCTATitleTextSize(I)V
    .locals 3

    .prologue
    .line 1198077
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1198078
    return-void
.end method

.method private setReplayLabelTextSize(I)V
    .locals 3

    .prologue
    .line 1198075
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1198076
    return-void
.end method


# virtual methods
.method public setCallToActionIcon(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1198071
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1198072
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1198073
    :goto_0
    return-void

    .line 1198074
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public setVideoReplayListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1198069
    iget-object v0, p0, Lcom/facebook/video/player/VideoCallToActionEndScreenOnInlinePlayer;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198070
    return-void
.end method
