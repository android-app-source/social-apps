.class public Lcom/facebook/video/player/SphericalVideoView;
.super Lcom/facebook/video/player/RichVideoPlayer;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/video/player/plugins/Video360Plugin;

.field private final p:LX/3Ie;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1198026
    const-class v0, Lcom/facebook/video/player/SphericalVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/SphericalVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198024
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/SphericalVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198025
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198022
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/SphericalVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198023
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1198001
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198002
    const-class v0, Lcom/facebook/video/player/SphericalVideoView;

    invoke-static {v0, p0}, Lcom/facebook/video/player/SphericalVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1198003
    new-instance v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/video/player/SphericalVideoView;->o:Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1198004
    new-instance v0, LX/3Ie;

    invoke-direct {v0, p1, p2, p3}, LX/3Ie;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/video/player/SphericalVideoView;->p:LX/3Ie;

    .line 1198005
    iget-object v0, p0, Lcom/facebook/video/player/SphericalVideoView;->o:Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1198006
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198007
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v1, Lcom/facebook/video/player/SphericalVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p1, v1}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1198008
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198009
    new-instance v0, LX/3Gh;

    invoke-virtual {p0}, Lcom/facebook/video/player/SphericalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Gh;-><init>(Landroid/content/Context;)V

    .line 1198010
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198011
    new-instance v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1198012
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198013
    new-instance v0, LX/2pM;

    invoke-direct {v0, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    .line 1198014
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198015
    new-instance v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/player/SphericalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    .line 1198016
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198017
    new-instance v0, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    .line 1198018
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198019
    iget-object v0, p0, Lcom/facebook/video/player/SphericalVideoView;->p:LX/3Ie;

    .line 1198020
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1198021
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/SphericalVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/player/SphericalVideoView;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v0

    check-cast v0, LX/19m;

    iput-object v0, p0, Lcom/facebook/video/player/SphericalVideoView;->m:LX/19m;

    return-void
.end method


# virtual methods
.method public final b(LX/04g;)V
    .locals 3

    .prologue
    .line 1197998
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/7Lp;

    sget-object v2, LX/7Lo;->INDICATOR_STOP:LX/7Lo;

    invoke-direct {v1, v2}, LX/7Lp;-><init>(LX/7Lo;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1197999
    iget-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->b:LX/2oj;

    new-instance v1, LX/2qb;

    invoke-direct {v1, p1}, LX/2qb;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1198000
    return-void
.end method
