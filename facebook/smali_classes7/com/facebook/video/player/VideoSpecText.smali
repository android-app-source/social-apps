.class public Lcom/facebook/video/player/VideoSpecText;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3np;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1198390
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1198391
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    .line 1198392
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198387
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198388
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    .line 1198389
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1198384
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198385
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    .line 1198386
    return-void
.end method

.method private a(LX/3np;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1198393
    iget-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1198394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/3np;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1198372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1198373
    sget-object v1, LX/3np;->VIDEO_MIME:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3np;->API_CONFIG:LX/3np;

    invoke-direct {p0, v2}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198374
    sget-object v1, LX/3np;->NEW_START_TIME:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198375
    sget-object v1, LX/3np;->RELATED_VIDEO:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198376
    sget-object v1, LX/3np;->CURRENT_STATE:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3np;->TARGET_STATE:LX/3np;

    invoke-direct {p0, v2}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198377
    sget-object v1, LX/3np;->NEW_PLAYER:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/3np;->VIDEO_REUSE:LX/3np;

    invoke-direct {p0, v2}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198378
    sget-object v1, LX/3np;->STREAMING_FORMAT:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198379
    sget-object v1, LX/3np;->DASH_STREAM:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198380
    sget-object v1, LX/3np;->STREAM_TYPE:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198381
    sget-object v1, LX/3np;->AUDIO_CHANNEL_LAYOUT:LX/3np;

    invoke-direct {p0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198382
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/VideoSpecText;->setText(Ljava/lang/CharSequence;)V

    .line 1198383
    return-void
.end method

.method private a(LX/3np;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1198368
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoSpecText;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1198369
    :goto_0
    return-void

    .line 1198370
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198371
    invoke-direct {p0}, Lcom/facebook/video/player/VideoSpecText;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7IE;)V
    .locals 2
    .param p1    # LX/7IE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1198360
    if-nez p1, :cond_0

    .line 1198361
    :goto_0
    return-void

    .line 1198362
    :cond_0
    sget-object v0, LX/3np;->VIDEO_MIME:LX/3np;

    invoke-virtual {p1}, LX/7IE;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;Ljava/lang/String;)V

    .line 1198363
    sget-object v0, LX/3np;->STREAMING_FORMAT:LX/3np;

    iget-object v1, p1, LX/7IE;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;Ljava/lang/String;)V

    .line 1198364
    sget-object v0, LX/3np;->STREAM_TYPE:LX/3np;

    iget-object v1, p1, LX/7IE;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;Ljava/lang/String;)V

    .line 1198365
    sget-object v0, LX/3np;->AUDIO_CHANNEL_LAYOUT:LX/3np;

    .line 1198366
    iget-object v1, p1, LX/7IE;->i:LX/03z;

    move-object v1, v1

    .line 1198367
    invoke-virtual {v1}, LX/03z;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/player/VideoSpecText;->a(LX/3np;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1198356
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/VideoSpecText;->setVisibility(I)V

    .line 1198357
    iget-object v0, p0, Lcom/facebook/video/player/VideoSpecText;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1198358
    return-void

    .line 1198359
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 0

    .prologue
    .line 1198355
    return-void
.end method
