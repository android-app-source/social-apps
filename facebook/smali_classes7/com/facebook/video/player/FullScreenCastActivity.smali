.class public Lcom/facebook/video/player/FullScreenCastActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/37Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/video/player/RichVideoPlayer;

.field public r:Ljava/lang/String;

.field private s:LX/7Kp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1196523
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1196524
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/video/engine/VideoPlayerParams;Landroid/net/Uri;D)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 1196516
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/player/FullScreenCastActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1196517
    const-string v1, "videoParams"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1196518
    const-string v1, "videoURI"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1196519
    const-string v1, "videoAspectRation"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1196520
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1196521
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1196522
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/high16 v2, -0x1000000

    .line 1196512
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1196513
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1196514
    :goto_0
    return-void

    .line 1196515
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/player/FullScreenCastActivity;

    invoke-static {v0}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v0

    check-cast v0, LX/37Y;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->p:LX/37Y;

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    .line 1196495
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "videoParams"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1196496
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "videoURI"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1196497
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    .line 1196498
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->finish()V

    .line 1196499
    :goto_0
    return-void

    .line 1196500
    :cond_1
    iget-object v2, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/facebook/video/player/FullScreenCastActivity;->r:Ljava/lang/String;

    .line 1196501
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1196502
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v4, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v2, v2

    div-double v2, v4, v2

    .line 1196503
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "videoAspectRation"

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    .line 1196504
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    const-string v5, "CoverImageParamsKey"

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 1196505
    new-instance v4, LX/2pZ;

    invoke-direct {v4}, LX/2pZ;-><init>()V

    .line 1196506
    iput-object v0, v4, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1196507
    move-object v0, v4

    .line 1196508
    iput-wide v2, v0, LX/2pZ;->e:D

    .line 1196509
    move-object v0, v0

    .line 1196510
    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1196511
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1196489
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    .line 1196490
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1196491
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;-><init>(Landroid/content/Context;)V

    .line 1196492
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1196493
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    const v1, 0x7f0d0b87

    invoke-virtual {v0, v1}, LX/2oX;->setInnerResource(I)V

    .line 1196494
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 1196487
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 1196488
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1196474
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1196475
    invoke-static {p0, p0}, Lcom/facebook/video/player/FullScreenCastActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1196476
    new-instance v0, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {v0, p0}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1196477
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1196478
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/7Ko;

    invoke-direct {v1, p0}, LX/7Ko;-><init>(Lcom/facebook/video/player/FullScreenCastActivity;)V

    .line 1196479
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1196480
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->m()V

    .line 1196481
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->q:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenCastActivity;->setContentView(Landroid/view/View;)V

    .line 1196482
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->a()V

    .line 1196483
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->l()V

    .line 1196484
    new-instance v0, LX/7Kp;

    invoke-direct {v0, p0}, LX/7Kp;-><init>(Lcom/facebook/video/player/FullScreenCastActivity;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenCastActivity;->s:LX/7Kp;

    .line 1196485
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenCastActivity;->b()V

    .line 1196486
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x316c43c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1196471
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1196472
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenCastActivity;->p:LX/37Y;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenCastActivity;->s:LX/7Kp;

    invoke-virtual {v1, v2}, LX/37Y;->b(LX/7Iw;)V

    .line 1196473
    const/16 v1, 0x23

    const v2, -0xf9a1c8c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x474ae940

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1196468
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1196469
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenCastActivity;->p:LX/37Y;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenCastActivity;->s:LX/7Kp;

    invoke-virtual {v1, v2}, LX/37Y;->a(LX/7Iw;)V

    .line 1196470
    const/16 v1, 0x23

    const v2, -0x22c90a4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
