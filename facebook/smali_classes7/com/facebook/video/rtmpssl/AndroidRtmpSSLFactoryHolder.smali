.class public Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile b:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1201947
    const-class v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->a:Ljava/lang/String;

    .line 1201948
    const-string v0, "android-rtmpssl"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1201949
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Mg;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1201950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201951
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fbtlsx_rtmp.store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1201952
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1201953
    iget-boolean v1, p2, LX/1Mg;->h:Z

    move v1, v1

    .line 1201954
    invoke-direct {p0, v0, v1}, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->initHybrid(Ljava/lang/String;Z)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1201955
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;
    .locals 3

    .prologue
    .line 1201956
    sget-object v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->b:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    if-nez v0, :cond_1

    .line 1201957
    const-class v1, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    monitor-enter v1

    .line 1201958
    :try_start_0
    sget-object v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->b:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1201959
    if-eqz v2, :cond_0

    .line 1201960
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->b(LX/0QB;)Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->b:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1201961
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1201962
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1201963
    :cond_1
    sget-object v0, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->b:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    return-object v0

    .line 1201964
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1201965
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;
    .locals 3

    .prologue
    .line 1201966
    new-instance v2, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Mg;->a(LX/0QB;)LX/1Mg;

    move-result-object v1

    check-cast v1, LX/1Mg;

    invoke-direct {v2, v0, v1}, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;-><init>(Landroid/content/Context;LX/1Mg;)V

    .line 1201967
    return-object v2
.end method

.method private native initHybrid(Ljava/lang/String;Z)Lcom/facebook/jni/HybridData;
.end method
