.class public Lcom/facebook/video/server/NetworkRangeWriter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/7Oi;


# instance fields
.field private final a:[LX/7P5;

.field public final b:Ljava/net/URL;

.field private final c:LX/0WJ;

.field private d:LX/0lC;

.field public e:Lcom/facebook/http/interfaces/RequestPriority;

.field public final f:Lcom/facebook/common/callercontext/CallerContext;

.field public final g:LX/1Lw;

.field public final h:LX/03V;

.field private final i:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;LX/1Lw;LX/03V;LX/0WJ;LX/0lC;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1202543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202544
    const/4 v0, 0x3

    new-array v0, v0, [LX/7P5;

    new-instance v1, LX/7P6;

    invoke-direct {v1, p0}, LX/7P6;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;)V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, LX/7P8;

    invoke-direct {v2, p0}, LX/7P8;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/7PA;

    invoke-direct {v2, p0}, LX/7PA;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->a:[LX/7P5;

    .line 1202545
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->i:Ljava/util/Collection;

    .line 1202546
    iput-object p1, p0, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    .line 1202547
    iput-object p2, p0, Lcom/facebook/video/server/NetworkRangeWriter;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202548
    if-nez p3, :cond_0

    .line 1202549
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "video"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202550
    :goto_0
    iput-object p4, p0, Lcom/facebook/video/server/NetworkRangeWriter;->g:LX/1Lw;

    .line 1202551
    iput-object p6, p0, Lcom/facebook/video/server/NetworkRangeWriter;->c:LX/0WJ;

    .line 1202552
    iput-object p7, p0, Lcom/facebook/video/server/NetworkRangeWriter;->d:LX/0lC;

    .line 1202553
    iput-object p5, p0, Lcom/facebook/video/server/NetworkRangeWriter;->h:LX/03V;

    .line 1202554
    return-void

    .line 1202555
    :cond_0
    const-string v0, "video"

    invoke-static {p3, v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->f:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method

.method private a(LX/15D;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1202556
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->i:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1202557
    iget-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->g:LX/1Lw;

    .line 1202558
    iget-object v1, v0, LX/1Lw;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202559
    iget-object v1, p0, Lcom/facebook/video/server/NetworkRangeWriter;->i:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1202560
    return-object v0

    .line 1202561
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/video/server/NetworkRangeWriter;->i:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/video/server/NetworkRangeWriter;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    .line 1202562
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202563
    iget-object v0, p0, Lcom/facebook/video/server/NetworkRangeWriter;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1202564
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1202565
    :goto_0
    if-eqz v0, :cond_0

    .line 1202566
    const-string v1, "Cookie"

    invoke-interface {p1, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202567
    :cond_0
    return-void

    .line 1202568
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/server/NetworkRangeWriter;->d:LX/0lC;

    .line 1202569
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, p0

    .line 1202570
    invoke-static {v1, v0}, Lcom/facebook/auth/credentials/SessionCookie;->b(LX/0lC;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 11

    .prologue
    .line 1202571
    new-instance v1, LX/7PC;

    invoke-direct {v1, p0, p2}, LX/7PC;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;Ljava/io/OutputStream;)V

    .line 1202572
    :try_start_0
    const/4 v10, 0x1

    .line 1202573
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    iget-object v5, p0, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    invoke-virtual {v5}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 1202574
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bytes="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p1, LX/2WF;->a:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p1, LX/2WF;->b:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1202575
    const-string v6, "Range"

    invoke-virtual {v4, v6, v5}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202576
    invoke-static {p0, v4}, Lcom/facebook/video/server/NetworkRangeWriter;->a$redex0(Lcom/facebook/video/server/NetworkRangeWriter;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1202577
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    invoke-static {v5, v10}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 1202578
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v5

    .line 1202579
    iput-object v4, v5, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1202580
    move-object v4, v5

    .line 1202581
    iget-object v5, p0, Lcom/facebook/video/server/NetworkRangeWriter;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202582
    iput-object v5, v4, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202583
    move-object v4, v4

    .line 1202584
    const-string v5, "rangeRequestForVideo"

    .line 1202585
    iput-object v5, v4, LX/15E;->c:Ljava/lang/String;

    .line 1202586
    move-object v4, v4

    .line 1202587
    iput-boolean v10, v4, LX/15E;->p:Z

    .line 1202588
    move-object v4, v4

    .line 1202589
    sget-object v5, LX/14P;->RETRY_SAFE:LX/14P;

    .line 1202590
    iput-object v5, v4, LX/15E;->j:LX/14P;

    .line 1202591
    move-object v4, v4

    .line 1202592
    iget-object v5, p0, Lcom/facebook/video/server/NetworkRangeWriter;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202593
    iput-object v5, v4, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202594
    move-object v4, v4

    .line 1202595
    iput-object v1, v4, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1202596
    move-object v4, v4

    .line 1202597
    invoke-virtual {v4}, LX/15E;->a()LX/15D;

    move-result-object v4

    move-object v0, v4
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202598
    iput-object v0, v1, LX/7PC;->d:LX/15D;

    .line 1202599
    :try_start_1
    invoke-direct {p0, v0}, Lcom/facebook/video/server/NetworkRangeWriter;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    .line 1202600
    iget-object v0, v1, LX/7PC;->c:Ljava/io/IOException;

    move-object v0, v0

    .line 1202601
    if-eqz v0, :cond_0

    .line 1202602
    iget-object v0, v1, LX/7PC;->c:Ljava/io/IOException;

    move-object v0, v0

    .line 1202603
    throw v0

    .line 1202604
    :catch_0
    move-exception v0

    .line 1202605
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1202606
    :catch_1
    move-exception v0

    .line 1202607
    new-instance v1, LX/7P1;

    const-string v2, "Error requesting data"

    invoke-direct {v1, v2, v0}, LX/7P1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1202608
    :cond_0
    return-wide v2
.end method

.method public final a()LX/3Dd;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 1202609
    iget-object v3, p0, Lcom/facebook/video/server/NetworkRangeWriter;->a:[LX/7P5;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 1202610
    :try_start_0
    invoke-interface {v0}, LX/7P5;->a()LX/15D;

    move-result-object v0

    .line 1202611
    invoke-direct {p0, v0}, Lcom/facebook/video/server/NetworkRangeWriter;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nJ;

    .line 1202612
    if-eqz v0, :cond_0

    .line 1202613
    const-string v9, "Content-Range"

    invoke-virtual {v0, v9}, LX/4nJ;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1202614
    if-eqz v9, :cond_2

    .line 1202615
    const/16 v10, 0x2f

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    .line 1202616
    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1202617
    :try_start_1
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    move-result-wide v9

    .line 1202618
    const-wide/16 v11, 0x0

    cmp-long v11, v9, v11

    if-lez v11, :cond_2

    .line 1202619
    :goto_1
    move-wide v7, v9

    .line 1202620
    const-string v5, "Content-Type"

    invoke-virtual {v0, v5}, LX/4nJ;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1202621
    new-instance v6, LX/3Dd;

    invoke-direct {v6, v7, v8, v5}, LX/3Dd;-><init>(JLjava/lang/String;)V

    .line 1202622
    const-string v5, "Cache-Control"

    const-string v7, "Cache-Control"

    invoke-virtual {v0, v7}, LX/4nJ;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, LX/3Dd;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1202623
    move-object v0, v6
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1202624
    return-object v0

    .line 1202625
    :catch_0
    move-exception v0

    .line 1202626
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1202627
    :catch_1
    move-exception v0

    .line 1202628
    new-instance v1, LX/7P1;

    const-string v2, "Error reading headers"

    invoke-direct {v1, v2, v0}, LX/7P1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1202629
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1202630
    :cond_1
    new-instance v0, LX/7PB;

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Resource length failed 3 times: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/video/server/NetworkRangeWriter;->b:Ljava/net/URL;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/7PB;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 1202631
    :catch_2
    :cond_2
    const-string v9, "Content-Length"

    invoke-virtual {v0, v9}, LX/4nJ;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1202632
    if-eqz v9, :cond_3

    .line 1202633
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    goto :goto_1

    .line 1202634
    :cond_3
    const-wide/16 v9, -0x1

    goto :goto_1
.end method
