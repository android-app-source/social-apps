.class public Lcom/facebook/video/chromecast/notification/CastNotificationActionService;
.super LX/0te;
.source ""


# instance fields
.field private a:LX/7J3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/37Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1193894
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/chromecast/notification/CastNotificationActionService;LX/7J3;LX/37Y;)V
    .locals 0

    .prologue
    .line 1193893
    iput-object p1, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->a:LX/7J3;

    iput-object p2, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->b:LX/37Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;

    invoke-static {v1}, LX/7J3;->a(LX/0QB;)LX/7J3;

    move-result-object v0

    check-cast v0, LX/7J3;

    invoke-static {v1}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v1

    check-cast v1, LX/37Y;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->a(Lcom/facebook/video/chromecast/notification/CastNotificationActionService;LX/7J3;LX/37Y;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193877
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x62db7b90

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1193890
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 1193891
    invoke-static {p0, p0}, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1193892
    const/16 v1, 0x25

    const v2, 0x3527530c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/16 v0, 0x24

    const v2, -0x1d14c12d

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1193878
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1193879
    :cond_0
    const/16 v0, 0x25

    const v3, 0x638aee77

    invoke-static {v1, v0, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1193880
    :goto_0
    return v1

    .line 1193881
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.facebook.video.chromecast.CAST_PLAY_PAUSE_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1193882
    iget-object v0, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->b:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    invoke-virtual {v0}, LX/38j;->a()Z

    move-result v3

    .line 1193883
    iget-object v4, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->a:LX/7J3;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "notification."

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_3

    const-string v0, "pause"

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_4

    const/4 v0, 0x3

    :goto_2
    invoke-virtual {v4, v5, v0}, LX/7J3;->a(Ljava/lang/String;I)V

    .line 1193884
    iget-object v0, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->b:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->p()V

    .line 1193885
    :cond_2
    :goto_3
    const v0, 0x733e8ceb

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 1193886
    :cond_3
    const-string v0, "play"

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    .line 1193887
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.facebook.video.chromecast.CAST_DISCONNECT_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1193888
    iget-object v0, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->a:LX/7J3;

    const-string v3, "notification.disconnect"

    sget-object v4, LX/38a;->DISCONNECTED:LX/38a;

    invoke-virtual {v0, v3, v4}, LX/7J3;->a(Ljava/lang/String;LX/38a;)V

    .line 1193889
    iget-object v0, p0, Lcom/facebook/video/chromecast/notification/CastNotificationActionService;->b:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->e()V

    goto :goto_3
.end method
