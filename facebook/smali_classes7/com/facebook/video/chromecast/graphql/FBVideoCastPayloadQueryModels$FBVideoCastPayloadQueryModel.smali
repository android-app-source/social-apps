.class public final Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2b02dcb1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1193871
    const-class v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1193844
    const-class v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1193869
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1193870
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193867
    iget-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->f:Ljava/lang/String;

    .line 1193868
    iget-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1193858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1193859
    invoke-direct {p0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1193860
    invoke-virtual {p0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1193861
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1193862
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1193863
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1193864
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1193865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1193866
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1193855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1193856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1193857
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193872
    invoke-direct {p0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1193852
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1193853
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->e:Z

    .line 1193854
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1193849
    new-instance v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;-><init>()V

    .line 1193850
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1193851
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1193848
    const v0, -0x4bece132

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1193847
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193845
    iget-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->g:Ljava/lang/String;

    .line 1193846
    iget-object v0, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method
