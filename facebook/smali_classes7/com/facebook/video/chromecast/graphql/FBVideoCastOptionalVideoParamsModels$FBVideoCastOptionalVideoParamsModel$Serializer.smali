.class public final Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1193719
    const-class v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    new-instance v1, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1193720
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1193718
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1193721
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1193722
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1193723
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1193724
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1193725
    if-eqz v2, :cond_0

    .line 1193726
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1193727
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1193728
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1193729
    if-eqz v2, :cond_2

    .line 1193730
    const-string p0, "image_blurred"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1193731
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1193732
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1193733
    if-eqz p0, :cond_1

    .line 1193734
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1193735
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1193736
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1193737
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1193738
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1193717
    check-cast p1, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Serializer;->a(Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;LX/0nX;LX/0my;)V

    return-void
.end method
