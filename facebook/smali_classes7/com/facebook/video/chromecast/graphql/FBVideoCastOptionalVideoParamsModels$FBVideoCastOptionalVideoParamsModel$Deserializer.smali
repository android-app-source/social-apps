.class public final Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1193674
    const-class v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    new-instance v1, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1193675
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1193676
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1193677
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1193678
    const/4 v2, 0x0

    .line 1193679
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1193680
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1193681
    :goto_0
    move v1, v2

    .line 1193682
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1193683
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1193684
    new-instance v1, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;

    invoke-direct {v1}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$FBVideoCastOptionalVideoParamsModel;-><init>()V

    .line 1193685
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1193686
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1193687
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1193688
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1193689
    :cond_0
    return-object v1

    .line 1193690
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1193691
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1193692
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1193693
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1193694
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_2

    if-eqz v4, :cond_2

    .line 1193695
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1193696
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1193697
    :cond_3
    const-string v5, "image_blurred"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1193698
    const/4 v4, 0x0

    .line 1193699
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_9

    .line 1193700
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1193701
    :goto_2
    move v1, v4

    .line 1193702
    goto :goto_1

    .line 1193703
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1193704
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1193705
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1193706
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1193707
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1193708
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_8

    .line 1193709
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1193710
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1193711
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v5, :cond_7

    .line 1193712
    const-string p0, "uri"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1193713
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1193714
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1193715
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1193716
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v1, v4

    goto :goto_3
.end method
