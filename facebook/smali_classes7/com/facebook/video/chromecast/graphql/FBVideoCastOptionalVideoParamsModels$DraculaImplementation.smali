.class public final Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1193672
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1193673
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1193670
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1193671
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1193661
    if-nez p1, :cond_0

    .line 1193662
    :goto_0
    return v0

    .line 1193663
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1193664
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1193665
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1193666
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1193667
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1193668
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1193669
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x644206f2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1193660
    new-instance v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1193657
    packed-switch p0, :pswitch_data_0

    .line 1193658
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1193659
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x644206f2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1193656
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1193654
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->b(I)V

    .line 1193655
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1193649
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1193650
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1193651
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->a:LX/15i;

    .line 1193652
    iput p2, p0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->b:I

    .line 1193653
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1193623
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1193648
    new-instance v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1193645
    iget v0, p0, LX/1vt;->c:I

    .line 1193646
    move v0, v0

    .line 1193647
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1193642
    iget v0, p0, LX/1vt;->c:I

    .line 1193643
    move v0, v0

    .line 1193644
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1193639
    iget v0, p0, LX/1vt;->b:I

    .line 1193640
    move v0, v0

    .line 1193641
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193636
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1193637
    move-object v0, v0

    .line 1193638
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1193627
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1193628
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1193629
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1193630
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1193631
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1193632
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1193633
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1193634
    invoke-static {v3, v9, v2}, Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/chromecast/graphql/FBVideoCastOptionalVideoParamsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1193635
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1193624
    iget v0, p0, LX/1vt;->c:I

    .line 1193625
    move v0, v0

    .line 1193626
    return v0
.end method
