.class public Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1mD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/video/settings/IsAutoplaySettingsMigrationEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1mC;
    .annotation runtime Lcom/facebook/video/settings/DefaultAutoPlaySettingsFromServer;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/4oi;

.field public g:LX/4oi;

.field public h:LX/4oi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1203626
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1203627
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;
    .locals 1

    .prologue
    .line 1203628
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1203629
    invoke-virtual {v0, p2}, LX/4oi;->a(LX/0Tn;)V

    .line 1203630
    invoke-virtual {v0, p3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1203631
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1203632
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1203633
    invoke-direct {p0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->b()LX/1mC;

    move-result-object v0

    .line 1203634
    iget-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1, v0}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    .line 1203635
    invoke-direct {p0, v0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->b(LX/1mC;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, LX/4oi;

    .line 1203636
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1203637
    invoke-static {p0, v0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a$redex0(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;Landroid/preference/Preference;)V

    .line 1203638
    return-void
.end method

.method private static a(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;LX/0Zb;LX/1mD;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;",
            "LX/0Zb;",
            "LX/1mD;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1mC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1203639
    iput-object p1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->b:LX/1mD;

    iput-object p3, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->e:LX/1mC;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v5}, LX/1mD;->a(LX/0QB;)LX/1mD;

    move-result-object v2

    check-cast v2, LX/1mD;

    const/16 v3, 0x386

    invoke-static {v5, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v5}, LX/1mG;->a(LX/0QB;)LX/1mC;

    move-result-object v5

    check-cast v5, LX/1mC;

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;LX/0Zb;LX/1mD;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;Landroid/preference/Preference;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1203640
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    if-ne p1, v0, :cond_1

    .line 1203641
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    invoke-virtual {v0, v2}, LX/4oi;->setChecked(Z)V

    .line 1203642
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1203643
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1203644
    :cond_0
    :goto_0
    return-void

    .line 1203645
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    if-ne p1, v0, :cond_2

    .line 1203646
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    invoke-virtual {v0, v2}, LX/4oi;->setChecked(Z)V

    .line 1203647
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1203648
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    goto :goto_0

    .line 1203649
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    if-ne p1, v0, :cond_0

    .line 1203650
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    invoke-virtual {v0, v2}, LX/4oi;->setChecked(Z)V

    .line 1203651
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1203652
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    goto :goto_0
.end method

.method private b()LX/1mC;
    .locals 3

    .prologue
    .line 1203653
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->b:LX/1mD;

    iget-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->e:LX/1mC;

    iget-object v2, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0, v1, v2}, LX/1mD;->a(LX/1mC;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1mC;

    move-result-object v0

    .line 1203654
    return-object v0
.end method

.method private b(LX/1mC;)Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 1203655
    sget-object v0, LX/7Q8;->a:[I

    invoke-virtual {p1}, LX/1mC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1203656
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    :goto_0
    return-object v0

    .line 1203657
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    goto :goto_0

    .line 1203658
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(LX/1mC;)V
    .locals 4

    .prologue
    .line 1203659
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1203660
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->b:LX/1mD;

    iget-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p1}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1mC;->valueOf(Ljava/lang/String;)LX/1mC;

    move-result-object v2

    .line 1203661
    const-string v3, "SETTING_CHANGE"

    invoke-virtual {v0, v1, v2, v3}, LX/1mD;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;Ljava/lang/String;)V

    .line 1203662
    :goto_0
    return-void

    .line 1203663
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "autoplay_setting_changed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "video"

    .line 1203664
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1203665
    move-object v0, v0

    .line 1203666
    const-string v1, "autoplay_setting_value"

    invoke-virtual {p1}, LX/1mC;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "settings"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1203667
    iget-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1mC;)V
    .locals 1

    .prologue
    .line 1203668
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0, p1}, LX/1mS;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1mC;)V

    .line 1203669
    invoke-direct {p0, p1}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->c(LX/1mC;)V

    .line 1203670
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1203671
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1203672
    const v0, 0x7f081ae6

    invoke-virtual {p0, v0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1203673
    invoke-static {p0, p0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1203674
    invoke-virtual {p0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1203675
    invoke-virtual {p0, v0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1203676
    new-instance v1, LX/4om;

    invoke-direct {v1, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 1203677
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f081aea

    invoke-virtual {p0, v3}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f081aeb

    invoke-virtual {p0, v3}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 1203678
    invoke-virtual {v1, v4}, LX/4om;->setEnabled(Z)V

    .line 1203679
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1203680
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1203681
    const v2, 0x7f081aec

    invoke-virtual {p0, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1203682
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1203683
    sget-object v1, LX/1mI;->d:LX/0Tn;

    const v2, 0x7f081ae9

    invoke-virtual {p0, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    .line 1203684
    sget-object v1, LX/1mI;->e:LX/0Tn;

    const v2, 0x7f081ae8

    invoke-virtual {p0, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    .line 1203685
    sget-object v1, LX/1mI;->f:LX/0Tn;

    const v2, 0x7f081ae7

    invoke-virtual {p0, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    .line 1203686
    invoke-direct {p0}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->a()V

    .line 1203687
    new-instance v1, LX/4om;

    invoke-direct {v1, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 1203688
    const v2, 0x7f081aed

    invoke-virtual {p0, v2}, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 1203689
    invoke-virtual {v1, v4}, LX/4om;->setEnabled(Z)V

    .line 1203690
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1203691
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1203692
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1203693
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->f:LX/4oi;

    new-instance v1, LX/7Q9;

    invoke-direct {v1, p0}, LX/7Q9;-><init>(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1203694
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->g:LX/4oi;

    new-instance v1, LX/7Q9;

    invoke-direct {v1, p0}, LX/7Q9;-><init>(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1203695
    iget-object v0, p0, Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;->h:LX/4oi;

    new-instance v1, LX/7Q9;

    invoke-direct {v1, p0}, LX/7Q9;-><init>(Lcom/facebook/video/settings/VideoAutoPlaySettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1203696
    return-void
.end method
