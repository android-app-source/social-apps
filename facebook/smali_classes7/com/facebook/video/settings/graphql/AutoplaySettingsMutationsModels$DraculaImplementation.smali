.class public final Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1204154
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1204155
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1204152
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1204153
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1204138
    if-nez p1, :cond_0

    .line 1204139
    :goto_0
    return v0

    .line 1204140
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1204141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1204142
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAutoplaySettingEffective;

    move-result-object v1

    .line 1204143
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1204144
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1204145
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1204146
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 1204147
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1204148
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1204149
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1204150
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 1204151
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x492c28f5
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1204137
    new-instance v0, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1204134
    packed-switch p0, :pswitch_data_0

    .line 1204135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1204136
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x492c28f5
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1204133
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1204131
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->b(I)V

    .line 1204132
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1204156
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1204157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1204158
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 1204159
    iput p2, p0, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->b:I

    .line 1204160
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1204130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1204129
    new-instance v0, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1204105
    iget v0, p0, LX/1vt;->c:I

    .line 1204106
    move v0, v0

    .line 1204107
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1204126
    iget v0, p0, LX/1vt;->c:I

    .line 1204127
    move v0, v0

    .line 1204128
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1204123
    iget v0, p0, LX/1vt;->b:I

    .line 1204124
    move v0, v0

    .line 1204125
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1204120
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1204121
    move-object v0, v0

    .line 1204122
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1204111
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1204112
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1204113
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1204114
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1204115
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1204116
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1204117
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1204118
    invoke-static {v3, v9, v2}, Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/video/settings/graphql/AutoplaySettingsMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1204119
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1204108
    iget v0, p0, LX/1vt;->c:I

    .line 1204109
    move v0, v0

    .line 1204110
    return v0
.end method
