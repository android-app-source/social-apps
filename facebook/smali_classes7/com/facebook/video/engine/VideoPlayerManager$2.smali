.class public final Lcom/facebook/video/engine/VideoPlayerManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/video/engine/VideoDataSource;

.field public final synthetic d:Z

.field public final synthetic e:LX/19P;


# direct methods
.method public constructor <init>(LX/19P;Ljava/lang/String;ZLcom/facebook/video/engine/VideoDataSource;Z)V
    .locals 0

    .prologue
    .line 1194501
    iput-object p1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iput-object p2, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->b:Z

    iput-object p4, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iput-boolean p5, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x0

    .line 1194502
    :try_start_0
    iget-boolean v1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1194503
    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 1194504
    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    if-nez v1, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 1194505
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iget-object v0, v0, LX/19P;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "VideoPlayerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AbrManifestContent is null: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194506
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iget-object v0, v0, LX/19P;->o:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->K:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->b:Z

    if-eqz v0, :cond_4

    :cond_1
    move-object v2, v9

    .line 1194507
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iget-object v0, v0, LX/19P;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qA;

    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->a:Ljava/lang/String;

    const-string v3, ""

    iget-object v4, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iget-object v5, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    iget-object v6, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    const/4 v7, 0x0

    iget-boolean v8, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->d:Z

    invoke-virtual/range {v0 .. v8}, LX/2qA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;ZZ)Lcom/facebook/exoplayer/ipc/VideoPlayRequest;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1194508
    :try_start_1
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iget-object v0, v0, LX/19P;->N:LX/04j;

    invoke-interface {v0, v1}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    move-result-object v0

    .line 1194509
    iget-object v2, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->e:LX/19P;

    iget-object v2, v2, LX/19P;->N:LX/04j;

    iget-object v3, p0, Lcom/facebook/video/engine/VideoPlayerManager$2;->c:Lcom/facebook/video/engine/VideoDataSource;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    invoke-interface {v2, v0, v3}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1194510
    if-eqz v1, :cond_2

    .line 1194511
    invoke-virtual {v1}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->close()V

    .line 1194512
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v1, v0

    .line 1194513
    goto :goto_0

    .line 1194514
    :cond_4
    :try_start_2
    sget-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v2, v0, LX/04G;->value:Ljava/lang/String;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1194515
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 1194516
    :goto_3
    :try_start_3
    const-string v2, "VideoPlayerManager"

    const-string v3, "Failed to prepare exo service"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1194517
    if-eqz v1, :cond_2

    .line 1194518
    invoke-virtual {v1}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->close()V

    goto :goto_2

    .line 1194519
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v9, :cond_5

    .line 1194520
    invoke-virtual {v9}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->close()V

    :cond_5
    throw v0

    .line 1194521
    :catchall_1
    move-exception v0

    move-object v9, v1

    goto :goto_4

    .line 1194522
    :catch_1
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1194523
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/video/engine/VideoPlayerManager$2;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194524
    :goto_0
    return-void

    .line 1194525
    :catch_0
    move-exception v0

    .line 1194526
    const-string v1, "VideoPlayerManager"

    const-string v2, "Unexpected exception in exo service prepare"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
