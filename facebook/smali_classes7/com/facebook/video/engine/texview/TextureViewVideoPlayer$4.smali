.class public final Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/ref/WeakReference;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;Ljava/lang/ref/WeakReference;Z)V
    .locals 0

    .prologue
    .line 1194681
    iput-object p1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;->c:Lcom/facebook/video/engine/texview/TextureViewVideoPlayer;

    iput-object p2, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;->a:Ljava/lang/ref/WeakReference;

    iput-boolean p3, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1194682
    iget-object v0, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaPlayer;

    .line 1194683
    if-eqz v0, :cond_1

    .line 1194684
    iget-boolean v1, p0, Lcom/facebook/video/engine/texview/TextureViewVideoPlayer$4;->b:Z

    if-eqz v1, :cond_0

    .line 1194685
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 1194686
    :cond_0
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1194687
    :cond_1
    return-void
.end method
