.class public final Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/Surface;

.field public final synthetic b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 1194719
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iput-object p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->a:Landroid/view/Surface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1194720
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_0

    .line 1194721
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    const-string v1, "Player already released when attach surface"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194722
    :goto_0
    return-void

    .line 1194723
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->b:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;->a:Landroid/view/Surface;

    invoke-interface {v0, v1, v2, v3}, LX/0Kx;->b(LX/0GS;ILjava/lang/Object;)V

    goto :goto_0
.end method
