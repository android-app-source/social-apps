.class public Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;
.super LX/2q6;
.source ""

# interfaces
.implements LX/0Jp;
.implements LX/0Jy;
.implements LX/0KW;
.implements LX/0KZ;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final Y:Z

.field private final Z:I

.field private aA:I

.field private aB:I

.field private aC:Ljava/lang/String;

.field private aD:Ljava/lang/String;

.field private aE:LX/2p8;

.field private final aF:LX/7KK;

.field private final aG:LX/1m6;

.field private aH:LX/7K1;

.field private aI:I

.field private final aJ:Z

.field private final aK:Z

.field private aL:I

.field private final aa:LX/2px;

.field public ab:LX/0Kx;

.field public ac:LX/0GT;

.field private ad:LX/0GT;

.field private ae:Ljava/lang/String;

.field public af:I

.field public ag:I

.field private ah:F

.field private ai:Landroid/net/Uri;

.field private aj:Landroid/net/Uri;

.field private ak:Landroid/net/Uri;

.field private al:Landroid/net/Uri;

.field private am:Ljava/lang/String;

.field private an:Z

.field private ao:I

.field private ap:I

.field private aq:I

.field private ar:LX/2oi;

.field private as:I

.field public at:I

.field public au:I

.field public av:LX/19Z;

.field public aw:I

.field private ax:J

.field private ay:LX/0wq;

.field private az:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/2pf;LX/1C2;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/0wq;LX/2q9;LX/2pw;LX/2qI;LX/2px;LX/13m;LX/2q3;LX/0Ot;LX/0ad;LX/0Uh;ZZLX/19j;LX/19a;LX/0Ot;)V
    .locals 23
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/engine/IsPausedBitmapEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            "LX/2pf;",
            "LX/1C2;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/lang/Boolean;",
            "Z",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/19Z;",
            "LX/0So;",
            "LX/0wq;",
            "Lcom/facebook/video/subtitles/controller/SubtitleAdapter;",
            "LX/2pw;",
            "LX/2qI;",
            "LX/2px;",
            "LX/13m;",
            "LX/2q3;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "ZZ",
            "LX/19j;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1195326
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p20

    move-object/from16 v8, p5

    move-object/from16 v9, p16

    move-object/from16 v10, p14

    move-object/from16 v11, p7

    move-object/from16 v12, p10

    move-object/from16 v13, p8

    move/from16 v14, p9

    move-object/from16 v15, p12

    move-object/from16 v16, p17

    move-object/from16 v17, p18

    move-object/from16 v18, p19

    move-object/from16 v19, p6

    move-object/from16 v20, p21

    move-object/from16 v21, p22

    move-object/from16 v22, p25

    invoke-direct/range {v2 .. v22}, LX/2q6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/0Ot;LX/2pf;LX/2qI;LX/2q9;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;Ljava/lang/Boolean;ZLX/0So;LX/2px;LX/13m;LX/2q3;LX/1C2;LX/0ad;LX/0Uh;LX/19j;)V

    .line 1195327
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ae:Ljava/lang/String;

    .line 1195328
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    .line 1195329
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aw:I

    .line 1195330
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    .line 1195331
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    .line 1195332
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aA:I

    .line 1195333
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aB:I

    .line 1195334
    new-instance v2, LX/7KK;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/7KK;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aF:LX/7KK;

    .line 1195335
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aI:I

    .line 1195336
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aJ:Z

    .line 1195337
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->I:Landroid/view/Surface;

    .line 1195338
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    .line 1195339
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    .line 1195340
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    .line 1195341
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    .line 1195342
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    .line 1195343
    move/from16 v0, p23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aK:Z

    .line 1195344
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->b:Landroid/content/Context;

    const/high16 v3, 0x43960000    # 300.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Z:I

    .line 1195345
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195346
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v2

    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1195347
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    .line 1195348
    new-instance v2, LX/09M;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->m:LX/0So;

    move-object/from16 v0, p26

    move-object/from16 v1, p27

    invoke-direct {v2, v3, v0, v1}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->N:LX/09M;

    .line 1195349
    new-instance v2, LX/09M;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->m:LX/0So;

    move-object/from16 v0, p26

    move-object/from16 v1, p27

    invoke-direct {v2, v3, v0, v1}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->O:LX/09M;

    .line 1195350
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 1195351
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->E:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 1195352
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ay:LX/0wq;

    .line 1195353
    move/from16 v0, p24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Y:Z

    .line 1195354
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/2q9;->a(LX/2pw;)V

    .line 1195355
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    new-instance v3, LX/7KJ;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/7KJ;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    invoke-virtual {v2, v3}, LX/2q9;->a(LX/2qI;)V

    .line 1195356
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    .line 1195357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2px;->a(LX/16V;)V

    .line 1195358
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->n:LX/2q3;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->q:LX/16V;

    invoke-virtual {v2, v3}, LX/2q3;->a(LX/16V;)V

    .line 1195359
    new-instance v2, LX/1m6;

    invoke-direct {v2}, LX/1m6;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    .line 1195360
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aL:I

    .line 1195361
    return-void
.end method

.method public static F(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1195296
    const-string v0, "Initializing ExoPlayer"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195297
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1195298
    iput v11, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    .line 1195299
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1195300
    const-string v0, "Set data source = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195301
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 1195302
    iget-object v0, p0, LX/2q6;->n:LX/2q3;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1195303
    iput v1, v0, LX/2q3;->h:I

    .line 1195304
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Landroid/net/Uri;)V

    .line 1195305
    :goto_0
    return-void

    .line 1195306
    :cond_1
    const-string v0, "Data source is invalid. Try next one."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195307
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->I()Z

    move-result v1

    .line 1195308
    if-nez v1, :cond_2

    .line 1195309
    const-string v0, "No data source!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195310
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195311
    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    sget-object v4, LX/7Jj;->NO_SOURCE:LX/7Jj;

    invoke-interface {v0, v3, v4}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 1195312
    :catch_0
    move-exception v0

    .line 1195313
    :try_start_1
    const-string v1, "dataSourceNPE"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195314
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->I()Z

    move-result v1

    .line 1195315
    if-nez v1, :cond_3

    .line 1195316
    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1195317
    :catch_1
    move-exception v9

    .line 1195318
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1195319
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195320
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    invoke-direct {p0, v0, v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1195321
    :goto_2
    if-nez v0, :cond_0

    goto :goto_0

    .line 1195322
    :catch_2
    move-exception v9

    .line 1195323
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1195324
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195325
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    invoke-direct {p0, v0, v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private I()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1195284
    const-string v0, "moveToNextVideoSource: %d"

    new-array v3, v1, [Ljava/lang/Object;

    iget v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195285
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    .line 1195286
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    if-gez v0, :cond_0

    .line 1195287
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    .line 1195288
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1195289
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1195290
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 1195291
    iget-object v2, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1195292
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->S:LX/097;

    move v0, v1

    .line 1195293
    :goto_1
    return v0

    .line 1195294
    :cond_1
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1195295
    goto :goto_1
.end method

.method public static J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1195282
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v1, :cond_1

    .line 1195283
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private K()I
    .locals 2

    .prologue
    .line 1195281
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aI:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/2q6;->B:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aI:I

    goto :goto_0
.end method

.method private N()V
    .locals 3

    .prologue
    .line 1195278
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195279
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ar:LX/2oi;

    invoke-interface {v0, v2}, LX/2pf;->a(LX/2oi;)V

    goto :goto_0

    .line 1195280
    :cond_0
    return-void
.end method

.method private O()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1195250
    const-string v0, "onPrepared for %s"

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195251
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1195252
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->P(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->A:I

    .line 1195253
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    iget v2, p0, LX/2q6;->A:I

    invoke-virtual {v0, v1, v2}, LX/19Z;->a(II)V

    .line 1195254
    sget-object v0, LX/2qE;->STATE_UNKNOWN:LX/2qE;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->V:LX/2qE;

    .line 1195255
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195256
    sget-object v0, LX/2qE;->STATE_CREATED:LX/2qE;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->V:LX/2qE;

    .line 1195257
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    .line 1195258
    iget-object v1, v0, LX/2p8;->a:Landroid/view/Surface;

    move-object v0, v1

    .line 1195259
    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;Landroid/view/Surface;)V

    .line 1195260
    :cond_0
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ah:F

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;F)V

    .line 1195261
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195262
    invoke-interface {v0}, LX/2pf;->a()V

    goto :goto_0

    .line 1195263
    :cond_1
    new-instance v0, LX/7IE;

    iget v1, p0, LX/2q6;->A:I

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aA:I

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aB:I

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->P:LX/09L;

    iget-object v6, v6, LX/09L;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->S:LX/097;

    iget-object v8, v8, LX/097;->value:Ljava/lang/String;

    sget-object v9, LX/03z;->UNKNOWN:LX/03z;

    invoke-direct/range {v0 .. v9}, LX/7IE;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/03z;)V

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->F:LX/7IE;

    .line 1195264
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ae:Ljava/lang/String;

    .line 1195265
    iput-object v1, v0, LX/7IE;->j:Ljava/lang/String;

    .line 1195266
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    if-lez v0, :cond_3

    move v0, v10

    .line 1195267
    :goto_1
    iput v11, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    .line 1195268
    iget-object v1, p0, LX/2q6;->q:LX/16V;

    new-instance v2, LX/09S;

    invoke-direct {v2}, LX/09S;-><init>()V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V

    .line 1195269
    iget-object v1, p0, LX/2q6;->E:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v1, v2, :cond_4

    .line 1195270
    iget-object v1, p0, LX/2q6;->R:LX/04g;

    iget-object v2, p0, LX/2q6;->C:LX/7K4;

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;LX/7K4;Z)V

    .line 1195271
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v11

    .line 1195272
    goto :goto_1

    .line 1195273
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Y:Z

    if-nez v0, :cond_2

    .line 1195274
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_5

    .line 1195275
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->c(LX/04g;)V

    goto :goto_2

    .line 1195276
    :cond_5
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_IDLE:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 1195277
    iget-object v0, p0, LX/2q6;->R:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;)V

    goto :goto_2
.end method

.method private static P(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)I
    .locals 4

    .prologue
    .line 1195078
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1195079
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->e()J

    move-result-wide v0

    .line 1195080
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x112a880

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    :cond_0
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    :goto_1
    return v0

    .line 1195081
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1195082
    :cond_2
    long-to-int v0, v0

    goto :goto_1
.end method

.method private static Q(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V
    .locals 5

    .prologue
    .line 1195242
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_1

    .line 1195243
    :cond_0
    return-void

    .line 1195244
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->i()I

    move-result v0

    .line 1195245
    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    if-le v0, v1, :cond_0

    .line 1195246
    const-string v1, "BufferingUpdate: from %d to %d, sid=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v4}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195247
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    .line 1195248
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195249
    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    invoke-interface {v0, v2}, LX/2pf;->b(I)V

    goto :goto_0
.end method

.method private a(LX/7Jj;Ljava/lang/Throwable;)V
    .locals 18

    .prologue
    .line 1195224
    const-string v2, "handleError %s; exception: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195225
    sget-object v2, LX/7Jj;->ERROR_IO:LX/7Jj;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_0

    sget-object v2, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_0

    sget-object v2, LX/7Jj;->MALFORMED:LX/7Jj;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_4

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_4

    .line 1195226
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    .line 1195227
    const-string v2, "Re-init ExoPlayer after malformed/io errors, try #%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195228
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v2}, LX/2q9;->c()V

    .line 1195229
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v2, v3, :cond_1

    .line 1195230
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    sget-object v8, LX/04g;->BY_PLAYER_INTERNAL_ERROR:LX/04g;

    iget-object v8, v8, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v9

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1195231
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1195232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1195233
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->l:LX/0Sh;

    new-instance v3, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$4;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    invoke-static {v4}, LX/0wq;->a(I)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 1195234
    :cond_3
    return-void

    .line 1195235
    :cond_4
    sget-object v2, LX/2qD;->STATE_ERROR:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 1195236
    sget-object v2, LX/2qD;->STATE_ERROR:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 1195237
    sget-object v2, LX/2qk;->FROM_ERROR:LX/2qk;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1195238
    const-string v2, "[ExoVideoPlayer]"

    const-string v3, "playback failed: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p2

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195239
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    const-string v3, "ExoPlayer Error: %s, exception: %s"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v3, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->z:LX/04g;

    iget-object v7, v7, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->w:LX/04D;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v2 .. v12}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1195240
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 1195241
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->G:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v2, v4, v0}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 1195154
    const-string v0, "prepareAsync"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195155
    sget-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1195156
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1195157
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/0Kx;Z)V

    .line 1195158
    :cond_0
    const-string v0, "Allocate new ExoPlayer"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195159
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09P;

    invoke-direct {v1}, LX/09P;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1195160
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    invoke-virtual {v0, v7}, LX/1m6;->a(Z)V

    .line 1195161
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1m6;->a(Ljava/lang/String;)V

    .line 1195162
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_1

    move v0, v6

    :goto_0
    invoke-virtual {v1, v0}, LX/1m6;->c(Z)V

    .line 1195163
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v5

    .line 1195164
    iget-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->an:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2q6;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yk;

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, LX/1Yk;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195165
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195166
    iget-object v1, v0, LX/7K1;->n:LX/0Ji;

    invoke-virtual {v1}, LX/0Ji;->a()LX/09L;

    move-result-object v1

    move-object v0, v1

    .line 1195167
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->P:LX/09L;

    .line 1195168
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    iget-object v1, p0, LX/2q6;->P:LX/09L;

    iget-object v1, v1, LX/09L;->value:Ljava/lang/String;

    .line 1195169
    iput-object v1, v0, LX/2px;->k:Ljava/lang/String;

    .line 1195170
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09R;

    invoke-direct {v1}, LX/09R;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1195171
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195172
    iget-object v1, v0, LX/7K1;->m:LX/0Kx;

    move-object v0, v1

    .line 1195173
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    .line 1195174
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195175
    iget-object v1, v0, LX/7K1;->m:LX/0Kx;

    iget-object v2, v0, LX/7K1;->t:LX/7Jz;

    invoke-interface {v1, v2}, LX/0Kx;->b(LX/0KW;)V

    .line 1195176
    iget-object v1, v0, LX/7K1;->m:LX/0Kx;

    invoke-interface {v1, p0}, LX/0Kx;->a(LX/0KW;)V

    .line 1195177
    iput-object p0, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    .line 1195178
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195179
    iget-object v1, v0, LX/7K1;->o:LX/0GT;

    move-object v0, v1

    .line 1195180
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    .line 1195181
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195182
    iget-object v1, v0, LX/7K1;->p:LX/0GT;

    move-object v0, v1

    .line 1195183
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ad:LX/0GT;

    .line 1195184
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195185
    iget v1, v0, LX/7K1;->q:I

    move v0, v1

    .line 1195186
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->X:I

    .line 1195187
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195188
    iget-object v1, v0, LX/7K1;->r:Ljava/lang/String;

    move-object v0, v1

    .line 1195189
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ae:Ljava/lang/String;

    .line 1195190
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195191
    iget v1, v0, LX/7K1;->j:I

    move v0, v1

    .line 1195192
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aB:I

    .line 1195193
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195194
    iget v1, v0, LX/7K1;->i:I

    move v0, v1

    .line 1195195
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aA:I

    .line 1195196
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195197
    iget-object v1, v0, LX/7K1;->g:Ljava/lang/String;

    move-object v0, v1

    .line 1195198
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    .line 1195199
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195200
    iget-object v1, v0, LX/7K1;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1195201
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    .line 1195202
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195203
    iget v1, v0, LX/7K1;->e:I

    move v0, v1

    .line 1195204
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    .line 1195205
    const-string v0, "NativePlayerPool returned player state=%s"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195206
    iget v3, v2, LX/7K1;->f:I

    move v2, v3

    .line 1195207
    invoke-static {v2}, LX/7K1;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195208
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195209
    iget v2, v1, LX/7K1;->f:I

    move v1, v2

    .line 1195210
    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(ZI)V

    .line 1195211
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/09Q;

    invoke-direct {v1}, LX/09Q;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1195212
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195213
    invoke-interface {v0}, LX/2pf;->b()V

    goto :goto_2

    :cond_1
    move v0, v7

    .line 1195214
    goto/16 :goto_0

    .line 1195215
    :cond_2
    iget-object v0, p0, LX/2q6;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yk;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->al:Landroid/net/Uri;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->am:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1Yk;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;

    move-result-object v0

    goto/16 :goto_1

    .line 1195216
    :cond_3
    new-instance v0, LX/7II;

    iget-object v1, p0, LX/2q6;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/7II;-><init>(LX/2q8;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1195217
    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 1195218
    iput v1, v0, LX/7IG;->c:I

    .line 1195219
    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1195220
    iput v1, v0, LX/7IG;->d:I

    .line 1195221
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    iput v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aw:I

    .line 1195222
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aw:I

    invoke-virtual {v1, v2, v0}, LX/19Z;->a(ILX/7IG;)V

    .line 1195223
    return-void
.end method

.method private static a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;LX/7K4;Z)V
    .locals 10

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1195115
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1195116
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1195117
    const-string v0, "playNow"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195118
    iput-boolean v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->L:Z

    .line 1195119
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->M:LX/04g;

    .line 1195120
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->K:J

    .line 1195121
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qN;

    iget v4, p2, LX/7K4;->c:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qN;-><init>(ILX/7IB;)V

    invoke-virtual {v0, v3}, LX/16V;->a(LX/1AD;)V

    .line 1195122
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195123
    invoke-interface {v0, p1, v1}, LX/2pf;->a(LX/04g;Z)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1195124
    goto :goto_0

    .line 1195125
    :cond_1
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    if-eq v0, v6, :cond_5

    invoke-virtual {p0}, LX/2q6;->B()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 1195126
    :goto_2
    if-eqz v0, :cond_2

    .line 1195127
    const-string v3, "Seek to: %d"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195128
    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    invoke-static {p0, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;I)V

    .line 1195129
    :cond_2
    iget-boolean v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aK:Z

    if-nez v3, :cond_3

    .line 1195130
    invoke-virtual {p0}, LX/2q6;->y()V

    .line 1195131
    :cond_3
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v3, v1}, LX/0Kx;->a(Z)V

    .line 1195132
    iget-object v1, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->c()V

    .line 1195133
    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v1}, LX/2q6;->a(LX/2qD;)V

    .line 1195134
    if-eqz v0, :cond_6

    iget v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195135
    :goto_3
    iput v6, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195136
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->b()V

    .line 1195137
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1C2;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 1195138
    invoke-static {p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->l(LX/04g;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1195139
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v7, p0, LX/2q6;->w:LX/04D;

    iget-object v8, p0, LX/2q6;->z:LX/04g;

    iget-object v8, v8, LX/04g;->value:Ljava/lang/String;

    iget-object v9, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v9}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 1195140
    :goto_4
    iget-object v0, p0, LX/2q6;->N:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 1195141
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/2qL;

    sget-object v2, LX/7IB;->b:LX/7IB;

    invoke-direct {v1, v5, v2}, LX/2qL;-><init>(ILX/7IB;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1195142
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 1195143
    invoke-virtual {p2}, LX/7K4;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1195144
    iget v0, p2, LX/7K4;->d:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    .line 1195145
    :cond_4
    :goto_5
    iget-object v0, p0, LX/2q6;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    .line 1195146
    return-void

    :cond_5
    move v0, v2

    .line 1195147
    goto/16 :goto_2

    .line 1195148
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v5

    goto :goto_3

    .line 1195149
    :cond_7
    if-nez v0, :cond_8

    if-nez p3, :cond_9

    .line 1195150
    :cond_8
    invoke-virtual {p0, p1, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/04g;I)V

    goto :goto_4

    .line 1195151
    :cond_9
    const-string v0, "InternalRetry, skip logging"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 1195152
    :cond_a
    invoke-static {p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->l(LX/04g;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1195153
    iput v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    goto :goto_5
.end method

.method public static a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/0Kx;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1195100
    if-eqz p2, :cond_0

    .line 1195101
    invoke-interface {p1}, LX/0Kx;->c()V

    .line 1195102
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ay:LX/0wq;

    iget v0, v0, LX/0wq;->d:I

    .line 1195103
    iget-object v1, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$2;

    invoke-direct {v2, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$2;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/0Kx;)V

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1195104
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0, p0}, LX/0Kx;->b(LX/0KW;)V

    .line 1195105
    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    .line 1195106
    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ad:LX/0GT;

    .line 1195107
    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    .line 1195108
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->X:I

    .line 1195109
    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ae:Ljava/lang/String;

    .line 1195110
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    if-eqz v0, :cond_1

    .line 1195111
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195112
    const/4 v1, 0x0

    iput-object v1, v0, LX/7K1;->s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    .line 1195113
    :cond_1
    iput-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aH:LX/7K1;

    .line 1195114
    return-void
.end method

.method private a(LX/7Jj;I)Z
    .locals 1

    .prologue
    .line 1195099
    sget-object v0, LX/7Jj;->ERROR_IO:LX/7Jj;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/04g;LX/7K4;)V
    .locals 16

    .prologue
    .line 1195091
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->a()V

    .line 1195092
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->N:LX/09M;

    invoke-virtual {v1}, LX/09M;->b()V

    .line 1195093
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->l(LX/04g;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1195094
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->al:Landroid/net/Uri;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->al:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->am:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v15, 0x1

    .line 1195095
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/04g;->value:Ljava/lang/String;

    sget-object v5, LX/7K4;->a:LX/7K4;

    move-object/from16 v0, p2

    if-eq v0, v5, :cond_2

    move-object/from16 v0, p2

    iget v5, v0, LX/7K4;->c:I

    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->S:LX/097;

    iget-object v6, v6, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->w:LX/04D;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->z:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v13, v13, Lcom/facebook/video/engine/VideoPlayerParams;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v1 .. v15}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;Ljava/lang/String;LX/098;Z)LX/1C2;

    .line 1195096
    :cond_0
    return-void

    .line 1195097
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 1195098
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v5

    goto :goto_1
.end method

.method private static b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;F)V
    .locals 4

    .prologue
    .line 1195087
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ah:F

    .line 1195088
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1195089
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ad:LX/0GT;

    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ah:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 1195090
    :cond_0
    return-void
.end method

.method private c(LX/04g;LX/7K4;)V
    .locals 1

    .prologue
    .line 1195083
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->R:LX/04g;

    .line 1195084
    iput-object p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->C:LX/7K4;

    .line 1195085
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195086
    return-void
.end method

.method public static c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;I)V
    .locals 4

    .prologue
    .line 1195411
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1195412
    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->ad:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_1

    .line 1195413
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, LX/0Kx;->b(J)V

    .line 1195414
    :cond_0
    :goto_0
    return-void

    .line 1195415
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    int-to-long v2, p1

    invoke-interface {v0, v2, v3}, LX/0Kx;->a(J)V

    goto :goto_0
.end method

.method private static c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 1195364
    const-string v0, "sendSurfaceToVideoRenderer: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195365
    iget-object v0, p0, LX/2q6;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$3;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;Landroid/view/Surface;)V

    const v2, 0x71cf7888

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1195366
    return-void
.end method

.method private i(LX/04g;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 1195416
    const-string v0, "resetNow"

    new-array v1, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195417
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->c()V

    .line 1195418
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 1195419
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-eq p1, v0, :cond_0

    .line 1195420
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v5

    iget v6, p0, LX/2q6;->B:I

    iget-object v7, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p0, LX/2q6;->w:LX/04D;

    iget-object v9, p0, LX/2q6;->z:LX/04g;

    iget-object v9, v9, LX/04g;->value:Ljava/lang/String;

    iget-object v10, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v10, v10, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iget-object v11, p0, LX/2q6;->S:LX/097;

    iget-object v11, v11, LX/097;->value:Ljava/lang/String;

    invoke-virtual/range {v0 .. v11}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;ZLjava/lang/String;)LX/1C2;

    .line 1195421
    :cond_0
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1195422
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195423
    iput v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    .line 1195424
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    .line 1195425
    iput v12, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    .line 1195426
    return-void
.end method

.method private j(LX/04g;)V
    .locals 18

    .prologue
    .line 1195427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    invoke-virtual {v2}, LX/2px;->a()V

    .line 1195428
    invoke-static/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Q(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    .line 1195429
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/2q6;->u:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    const/16 v3, 0x63

    if-le v2, v3, :cond_1

    .line 1195430
    :cond_0
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195431
    invoke-static/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->k(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;)V

    .line 1195432
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/2q6;->D()V

    .line 1195433
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    .line 1195434
    return-void

    .line 1195435
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->D:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v2, v3, :cond_4

    .line 1195436
    const-string v2, "current state = %s, seek time = %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->D:LX/2qD;

    iget-object v5, v5, LX/2qD;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195437
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 1195438
    :goto_1
    if-eqz v2, :cond_2

    .line 1195439
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    .line 1195440
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->C:LX/7K4;

    iget v2, v2, LX/7K4;->c:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195441
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1195442
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_0

    .line 1195443
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 1195444
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, LX/04g;->value:Ljava/lang/String;

    sget-object v3, LX/04g;->BY_ANDROID:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1195445
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195446
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    move-object/from16 v0, p0

    iget v3, v0, LX/2q6;->B:I

    if-ge v2, v3, :cond_7

    .line 1195447
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195448
    :cond_7
    const-string v2, "stop-for-pause: %s, seek time = %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, LX/04g;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195449
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->l(LX/04g;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1195450
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v7, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    move-object/from16 v0, p0

    iget v10, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v11, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2q6;->z:LX/04g;

    iget-object v13, v13, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->S:LX/097;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, LX/097;->value:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v2 .. v17}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1195451
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 1195452
    invoke-virtual/range {p0 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;)V

    goto/16 :goto_0
.end method

.method private static k(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1195453
    const-string v0, "pause"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195454
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195455
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PAUSED:LX/2qD;

    if-ne v0, v1, :cond_1

    .line 1195456
    sget-object v0, LX/2qD;->STATE_PAUSED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195457
    :cond_0
    :goto_0
    return-void

    .line 1195458
    :cond_1
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_2

    .line 1195459
    invoke-virtual {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->f(LX/04g;)V

    goto :goto_0

    .line 1195460
    :cond_2
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195461
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195462
    invoke-interface {v0, p1, v4}, LX/2pf;->b(LX/04g;Z)V

    goto :goto_1

    .line 1195463
    :cond_3
    invoke-virtual {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->g(LX/04g;)V

    .line 1195464
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195465
    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    goto :goto_2
.end method

.method private static l(LX/04g;)Z
    .locals 1

    .prologue
    .line 1195492
    sget-object v0, LX/2q6;->a:LX/0Rf;

    invoke-virtual {v0, p0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final E()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 1195466
    const-string v0, "onCompletion"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195467
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->c()V

    .line 1195468
    invoke-virtual {p0}, LX/2q6;->C()V

    .line 1195469
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 1195470
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->P(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->A:I

    .line 1195471
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    .line 1195472
    invoke-virtual {p0}, LX/2q6;->D()V

    .line 1195473
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aL:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->r:I

    if-ge v0, v1, :cond_2

    .line 1195474
    :cond_0
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v2, v2}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 1195475
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195476
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v2, v2}, LX/7K4;-><init>(II)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(LX/04g;LX/7K4;)V

    .line 1195477
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v1, LX/7K4;

    invoke-direct {v1, v2, v2}, LX/7K4;-><init>(II)V

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;LX/7K4;Z)V

    .line 1195478
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aL:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aL:I

    .line 1195479
    :cond_1
    return-void

    .line 1195480
    :cond_2
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_3

    .line 1195481
    iget-object v0, p0, LX/2q6;->q:LX/16V;

    new-instance v1, LX/2qT;

    iget v2, p0, LX/2q6;->A:I

    sget-object v3, LX/7IF;->a:LX/7IF;

    invoke-direct {v1, v2, v3}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1195482
    :cond_3
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1195483
    sget-object v0, LX/2qD;->STATE_PLAYBACK_COMPLETED:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195484
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195485
    invoke-virtual {p0}, LX/2q6;->n()V

    .line 1195486
    sget-object v0, LX/2qk;->FROM_ONCOMPLETE:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1195487
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195488
    iget v2, p0, LX/2q6;->A:I

    invoke-interface {v0, v2}, LX/2pf;->a(I)V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1195489
    const-string v0, "setVolume"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195490
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;F)V

    .line 1195491
    return-void
.end method

.method public final a(IIIF)V
    .locals 4

    .prologue
    .line 1195401
    const-string v0, "onVideoSizeChanged: %d, %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195402
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aA:I

    .line 1195403
    iput p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aB:I

    .line 1195404
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    .line 1195405
    iput p1, v0, LX/7IE;->f:I

    .line 1195406
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    .line 1195407
    iput p2, v0, LX/7IE;->g:I

    .line 1195408
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195409
    invoke-interface {v0, p1, p2}, LX/2pf;->a(II)V

    goto :goto_0

    .line 1195410
    :cond_0
    return-void
.end method

.method public final a(IJ)V
    .locals 0

    .prologue
    .line 1195400
    return-void
.end method

.method public final a(IJJ)V
    .locals 0

    .prologue
    .line 1195399
    return-void
.end method

.method public final a(IJJJ)V
    .locals 0

    .prologue
    .line 1195398
    return-void
.end method

.method public final a(IJJJJ)V
    .locals 0

    .prologue
    .line 1195397
    return-void
.end method

.method public final a(ILX/04g;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1195385
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "seekTo ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195386
    iget-object v1, p0, LX/2q6;->G:Ljava/lang/String;

    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195387
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    if-ne p2, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, LX/1m6;->b(Z)V

    .line 1195388
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->B:I

    .line 1195389
    sget-object v0, LX/7KI;->a:[I

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    invoke-virtual {v1}, LX/2qD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1195390
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/2q6;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1195391
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;I)V

    .line 1195392
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0, p1}, LX/2q9;->a(I)V

    .line 1195393
    :cond_1
    :goto_0
    return-void

    .line 1195394
    :pswitch_0
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195395
    goto :goto_0

    .line 1195396
    :pswitch_1
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILX/0AR;IJ)V
    .locals 18

    .prologue
    .line 1195374
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->W:LX/0AR;

    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget-object v2, v0, LX/0AR;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    iget-object v3, v3, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1195375
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, LX/2q6;->X:I

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v8

    invoke-direct/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->K()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->z:LX/04g;

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->W:LX/0AR;

    const/4 v13, 0x0

    sget-object v14, LX/0JM;->STREAM_SWITCH:LX/0JM;

    const/4 v15, 0x0

    invoke-virtual/range {v2 .. v15}, LX/1C2;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/04G;Ljava/lang/String;ILjava/lang/String;IILX/04D;Ljava/lang/String;LX/0AR;LX/0AR;LX/0JM;Z)LX/1C2;

    .line 1195376
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->W:LX/0AR;

    .line 1195377
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aI:I

    .line 1195378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    if-eqz v2, :cond_1

    .line 1195379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->W:LX/0AR;

    invoke-virtual {v2, v3}, LX/1m6;->a(LX/0AR;)V

    .line 1195380
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->F:LX/7IE;

    invoke-virtual/range {p0 .. p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7IE;->a(Ljava/lang/String;)V

    .line 1195381
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 1195382
    invoke-virtual/range {p0 .. p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, LX/2pf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1195383
    :cond_2
    const-string v2, "onDownstreamFormatChanged: %s, sourceId: %d, trigger: %d, mediaTimeMs: %d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, LX/2q6;->A()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195384
    return-void
.end method

.method public final a(ILjava/io/IOException;)V
    .locals 0

    .prologue
    .line 1195373
    return-void
.end method

.method public final a(LX/04G;)V
    .locals 1

    .prologue
    .line 1195369
    invoke-super {p0, p1}, LX/2q6;->a(LX/04G;)V

    .line 1195370
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    if-eqz v0, :cond_0

    .line 1195371
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    invoke-virtual {v0, p1}, LX/1m6;->a(LX/04G;)V

    .line 1195372
    :cond_0
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1195367
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/04g;LX/7K4;)V

    .line 1195368
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 17

    .prologue
    .line 1195362
    move-object/from16 v0, p0

    iget-object v1, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->P:LX/09L;

    iget-object v4, v4, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->S:LX/097;

    iget-object v8, v7, LX/097;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2q6;->z:LX/04g;

    iget-object v12, v7, LX/04g;->value:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2q6;->x:LX/04H;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2q6;->N:LX/09M;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v16, v0

    move/from16 v7, p2

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 1195363
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1194767
    const-string v2, "play"

    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1194768
    const-string v2, "%s, %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v4, v3, v1

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194769
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v2

    .line 1194770
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    sget-object v4, LX/7IJ;->START:LX/7IJ;

    invoke-virtual {v3, v2, v4}, LX/19Z;->a(ILX/7IJ;)V

    .line 1194771
    iget-object v2, p0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qK;

    iget v4, p2, LX/7K4;->c:I

    sget-object v5, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v4, v5}, LX/2qK;-><init>(ILX/7IB;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 1194772
    invoke-virtual {p2}, LX/7K4;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1194773
    iget v2, p2, LX/7K4;->c:I

    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1194774
    :cond_0
    iget-object v2, p0, LX/2q6;->E:LX/2qD;

    sget-object v3, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v2, v3, :cond_4

    .line 1194775
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(LX/04g;LX/7K4;)V

    .line 1194776
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1194777
    if-nez v0, :cond_1

    .line 1194778
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 1194779
    :cond_1
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v0, v1, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1194780
    if-nez v0, :cond_3

    .line 1194781
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->F(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    .line 1194782
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 1194783
    goto :goto_0

    .line 1194784
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(LX/04g;LX/7K4;)V

    .line 1194785
    invoke-static {p0, p1, p2, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;LX/7K4;Z)V

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/0Jw;LX/0GT;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1194953
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onRenderers, video: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " audio: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194954
    if-nez p1, :cond_0

    new-instance p1, LX/0Ku;

    invoke-direct {p1}, LX/0Ku;-><init>()V

    :cond_0
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    .line 1194955
    if-nez p2, :cond_1

    new-instance p2, LX/0Ku;

    invoke-direct {p2}, LX/0Ku;-><init>()V

    :cond_1
    iput-object p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ad:LX/0GT;

    .line 1194956
    iput p3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->X:I

    .line 1194957
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    .line 1194958
    if-eqz p4, :cond_2

    .line 1194959
    iput-object p4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ae:Ljava/lang/String;

    .line 1194960
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    if-eqz v0, :cond_2

    .line 1194961
    iget-object v0, p0, LX/2q6;->F:LX/7IE;

    .line 1194962
    iput-object p4, v0, LX/7IE;->j:Ljava/lang/String;

    .line 1194963
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_3

    .line 1194964
    const-string v0, "[ExoVideoPlayer]"

    const-string v1, "null mExoPlayer in onRenderers"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194965
    :goto_0
    return-void

    .line 1194966
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    const/4 v1, 0x2

    new-array v1, v1, [LX/0GT;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ad:LX/0GT;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/0Kx;->a([LX/0GT;)V

    goto :goto_0
.end method

.method public final a(LX/0Kv;)V
    .locals 1

    .prologue
    .line 1194948
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    .line 1194949
    invoke-virtual {p1}, LX/0Kv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 1194950
    sget-object v0, LX/7Jj;->ERROR_IO:LX/7Jj;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    .line 1194951
    :goto_0
    return-void

    .line 1194952
    :cond_0
    sget-object v0, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/0L3;)V
    .locals 1

    .prologue
    .line 1194946
    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    .line 1194947
    return-void
.end method

.method public final a(LX/0L4;)V
    .locals 1

    .prologue
    .line 1194940
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1194941
    :cond_0
    :goto_0
    return-void

    .line 1194942
    :cond_1
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1194943
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    .line 1194944
    :cond_2
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0Al;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194945
    iget-object v0, p1, LX/0L4;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/0LK;)V
    .locals 1

    .prologue
    .line 1194938
    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    .line 1194939
    return-void
.end method

.method public final a(LX/0LM;)V
    .locals 0

    .prologue
    .line 1194937
    return-void
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 1194893
    const-string v0, "setVideoResolution"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1194894
    iget-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194895
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ar:LX/2oi;

    .line 1194896
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 1194897
    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_1

    .line 1194898
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->an:Z

    .line 1194899
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ak:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1194900
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aq:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1194901
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ak:Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1194902
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1194903
    :goto_0
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->N()V

    .line 1194904
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_2

    .line 1194905
    :cond_0
    :goto_1
    return-void

    .line 1194906
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->an:Z

    .line 1194907
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1194908
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ap:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1194909
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1194910
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ak:Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1194911
    :cond_2
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 1194912
    :goto_2
    if-eqz v1, :cond_3

    .line 1194913
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->c(LX/04g;)V

    .line 1194914
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1194915
    :cond_3
    :try_start_0
    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1194916
    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1194917
    :try_start_1
    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v3}, LX/0Kx;->c()V

    .line 1194918
    iget-object v3, p0, LX/2q6;->n:LX/2q3;

    invoke-virtual {v3, v0}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 1194919
    iget-object v3, p0, LX/2q6;->n:LX/2q3;

    iget v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1194920
    iput v4, v3, LX/2q3;->h:I

    .line 1194921
    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1194922
    if-eqz v1, :cond_5

    .line 1194923
    :try_start_2
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    sget-object v1, LX/7K4;->a:LX/7K4;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(LX/04g;LX/7K4;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1194924
    :catch_0
    move-exception v9

    .line 1194925
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1194926
    const-string v0, "Caught IllegalStateException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194927
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    invoke-direct {p0, v0, v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1194928
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    .line 1194929
    :catch_1
    move-exception v0

    .line 1194930
    :try_start_3
    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1194931
    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1194932
    :catch_2
    move-exception v9

    .line 1194933
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    invoke-virtual {v9}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1194934
    const-string v0, "Caught NullPointerException - Unable to open content %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194935
    sget-object v0, LX/7Jj;->UNKNOWN:LX/7Jj;

    invoke-direct {p0, v0, v9}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1194936
    :cond_5
    :try_start_4
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->k(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;)V
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 1194885
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    .line 1194886
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aF:LX/7KK;

    .line 1194887
    iput-object v1, v0, LX/2p8;->j:LX/2qG;

    .line 1194888
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194889
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aF:LX/7KK;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    .line 1194890
    iget-object p0, v1, LX/2p8;->a:Landroid/view/Surface;

    move-object v1, p0

    .line 1194891
    invoke-virtual {v0, v1}, LX/7KK;->a(Landroid/view/Surface;)V

    .line 1194892
    :cond_0
    return-void
.end method

.method public final a(LX/2qk;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1194865
    const-string v0, "release"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1194866
    iget-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194867
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 1194868
    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    .line 1194869
    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    .line 1194870
    iput-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->F:LX/7IE;

    .line 1194871
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 1194872
    const/4 p1, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1194873
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aw:I

    invoke-virtual {v0, v1}, LX/19Z;->a(I)V

    .line 1194874
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aw:I

    .line 1194875
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1194876
    const-string v0, "unprepare ExoPlayer"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194877
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/0Kx;Z)V

    .line 1194878
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->a(LX/2qD;)V

    .line 1194879
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1194880
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    .line 1194881
    iput p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    .line 1194882
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->au:I

    .line 1194883
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1194884
    :cond_0
    return-void
.end method

.method public final a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1194848
    const-string v0, "switchPlayableUri"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1194849
    iget-object v0, p0, LX/2q6;->G:Ljava/lang/String;

    new-array v2, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194850
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_1

    .line 1194851
    :cond_0
    :goto_0
    return-void

    .line 1194852
    :cond_1
    sget-object v0, LX/7K7;->VIDEO_SOURCE_HLS:LX/7K7;

    if-ne p1, v0, :cond_2

    .line 1194853
    if-eqz p2, :cond_2

    .line 1194854
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1194855
    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1194856
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1194857
    :cond_2
    sget-object v0, LX/2qD;->STATE_PLAYING:LX/2qD;

    iget-object v2, p0, LX/2q6;->D:LX/2qD;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    .line 1194858
    :goto_1
    if-eqz v0, :cond_3

    .line 1194859
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, LX/2q6;->c(LX/04g;)V

    .line 1194860
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1194861
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->c()V

    .line 1194862
    iput v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    .line 1194863
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1194864
    goto :goto_1
.end method

.method public final a(LX/7QP;)V
    .locals 1

    .prologue
    .line 1194845
    iget-object v0, p0, LX/2q6;->h:LX/2q9;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, LX/2q9;->a(LX/7QP;)Z

    .line 1194846
    return-void

    .line 1194847
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 0

    .prologue
    .line 1194844
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 1194842
    const-string v0, "DrawToSurface"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194843
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .locals 6
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1194793
    const-string v0, "bindVideoSources"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1194794
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 1194795
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->u:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ap:I

    .line 1194796
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->v:I

    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aq:I

    .line 1194797
    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    .line 1194798
    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ak:Landroid/net/Uri;

    .line 1194799
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ar:LX/2oi;

    .line 1194800
    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->W:LX/0AR;

    .line 1194801
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aI:I

    .line 1194802
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->az:I

    .line 1194803
    iput v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aL:I

    .line 1194804
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    if-eqz v0, :cond_0

    .line 1194805
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aG:LX/1m6;

    iget-object v1, p0, LX/2q6;->W:LX/0AR;

    invoke-virtual {v0, v1}, LX/1m6;->a(LX/0AR;)V

    .line 1194806
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1194807
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1194808
    const-string v0, "bindVideoSources: No valid video paths"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194809
    sget-object v0, LX/2qk;->FROM_BIND:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1194810
    iput v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    .line 1194811
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1194812
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->S:LX/097;

    .line 1194813
    :goto_0
    return-void

    .line 1194814
    :cond_1
    iput v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    .line 1194815
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1194816
    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->m:Z

    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    if-eqz v1, :cond_5

    move v2, v3

    .line 1194817
    :goto_1
    if-eqz v2, :cond_6

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    .line 1194818
    :goto_2
    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-virtual {v5, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1194819
    :cond_2
    sget-object v5, LX/2qk;->FROM_BIND:LX/2qk;

    invoke-virtual {p0, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1194820
    :cond_3
    if-eqz v0, :cond_4

    .line 1194821
    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->S:LX/097;

    .line 1194822
    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    .line 1194823
    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->c:Landroid/net/Uri;

    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ak:Landroid/net/Uri;

    .line 1194824
    iget-object v5, v0, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    iput-object v5, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->am:Ljava/lang/String;

    .line 1194825
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->d:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->al:Landroid/net/Uri;

    .line 1194826
    :cond_4
    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    .line 1194827
    if-eqz v2, :cond_8

    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aq:I

    :goto_3
    iput v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1194828
    if-eqz v2, :cond_9

    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    :goto_4
    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ar:LX/2oi;

    .line 1194829
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->N()V

    .line 1194830
    const-string v0, "bindVideoSources: (%s): %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/2q6;->S:LX/097;

    iget-object v2, v2, LX/097;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194831
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->a()V

    .line 1194832
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->w:I

    .line 1194833
    iput v1, v0, LX/2px;->i:I

    .line 1194834
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ao:I

    .line 1194835
    iput v1, v0, LX/2px;->j:I

    .line 1194836
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2px;->b(Ljava/lang/String;)V

    .line 1194837
    iput-boolean v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->an:Z

    goto/16 :goto_0

    :cond_5
    move v2, v4

    .line 1194838
    goto :goto_1

    .line 1194839
    :cond_6
    if-eqz v0, :cond_7

    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aj:Landroid/net/Uri;

    goto :goto_2

    .line 1194840
    :cond_8
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ap:I

    goto :goto_3

    .line 1194841
    :cond_9
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_4
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1194787
    const-string v2, "Builder build threw exception: %s"

    new-array v3, v1, [Ljava/lang/Object;

    aput-object p1, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194788
    iget-object v2, p0, LX/2q6;->P:LX/09L;

    sget-object v3, LX/09L;->DASH:LX/09L;

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 1194789
    :cond_0
    instance-of v2, p1, LX/0L6;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1194790
    iput-boolean v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->an:Z

    .line 1194791
    :cond_1
    sget-object v0, LX/7Jj;->MALFORMED:LX/7Jj;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/7Jj;Ljava/lang/Throwable;)V

    .line 1194792
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 1194786
    return-void
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1194765
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/2q6;->D:LX/2qD;

    iget-object v2, v2, LX/2qD;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/2q6;->E:LX/2qD;

    iget-object v2, v2, LX/2qD;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    invoke-static {v2}, LX/7K1;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 1194766
    return-void
.end method

.method public final a(ZI)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1195010
    const-string v0, "PlayerStateChanged: playWhenReady=%s playbackState=%s"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p2}, LX/7K1;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195011
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    if-eq p2, v0, :cond_6

    .line 1195012
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    .line 1195013
    iput p2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ag:I

    .line 1195014
    iget-object v1, p0, LX/2q6;->D:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v1, v2, :cond_1

    if-eq p2, v6, :cond_0

    if-ne p2, v7, :cond_1

    .line 1195015
    :cond_0
    const-string v1, "ExoPlayer prepared: %s, %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aC:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aD:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195016
    invoke-direct {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->O()V

    .line 1195017
    :cond_1
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    if-ne p2, v6, :cond_3

    .line 1195018
    const-string v1, "onStartBuffering"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195019
    iget-object v1, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2pf;

    .line 1195020
    invoke-interface {v1}, LX/2pf;->e()V

    goto :goto_0

    .line 1195021
    :cond_2
    if-ne v0, v7, :cond_3

    .line 1195022
    iget-object v1, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/09M;->a(J)V

    .line 1195023
    :cond_3
    if-ne v0, v6, :cond_5

    if-ne p2, v7, :cond_5

    .line 1195024
    const-string v0, "onStopBuffering"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195025
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195026
    invoke-interface {v0}, LX/2pf;->f()V

    goto :goto_1

    .line 1195027
    :cond_4
    iget-object v0, p0, LX/2q6;->O:LX/09M;

    invoke-virtual {v0}, LX/09M;->c()V

    .line 1195028
    :cond_5
    const/4 v0, 0x5

    if-ne p2, v0, :cond_6

    .line 1195029
    const-string v0, "Playback complete, sid=%s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195030
    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->E()V

    .line 1195031
    :cond_6
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Q(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    .line 1195032
    return-void
.end method

.method public final a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 1195074
    const-string v0, "mute"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195075
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;F)V

    .line 1195076
    return-void

    .line 1195077
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1195072
    const-string v1, "isPlaying"

    iput-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195073
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v1}, LX/0Kx;->b()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1195061
    const-string v0, "getCurrentPosition"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195062
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1195063
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->at:I

    .line 1195064
    :cond_0
    :goto_0
    return v2

    .line 1195065
    :cond_1
    iget-object v0, p0, LX/2q6;->s:LX/19j;

    iget-boolean v0, v0, LX/19j;->ad:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1195066
    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->g()J

    move-result-wide v0

    :goto_2
    long-to-int v1, v0

    .line 1195067
    iget v0, p0, LX/2q6;->A:I

    if-lez v0, :cond_4

    iget v0, p0, LX/2q6;->A:I

    .line 1195068
    :goto_3
    invoke-static {v1, v2, v0}, LX/13m;->a(III)I

    move-result v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1195069
    goto :goto_1

    .line 1195070
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->f()J

    move-result-wide v0

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1195071
    goto :goto_3
.end method

.method public final b(IJ)V
    .locals 0

    .prologue
    .line 1195060
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1195046
    const-string v0, "stop"

    iput-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->G:Ljava/lang/String;

    .line 1195047
    const-string v0, "%s, %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/2q6;->G:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p1, LX/04g;->value:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195048
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aa:LX/2px;

    invoke-virtual {v0}, LX/2px;->a()V

    .line 1195049
    iget-object v0, p0, LX/2q6;->D:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v1, :cond_1

    .line 1195050
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->R:LX/04g;

    .line 1195051
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195052
    :cond_0
    :goto_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ax:J

    .line 1195053
    return-void

    .line 1195054
    :cond_1
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->J(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195055
    iget-object v0, p0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 1195056
    invoke-interface {v0, p1, v4}, LX/2pf;->c(LX/04g;Z)V

    goto :goto_1

    .line 1195057
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/19Z;->a(I)V

    .line 1195058
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->i(LX/04g;)V

    .line 1195059
    iget-object v0, p0, LX/2q6;->l:LX/0Sh;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer$1;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;LX/04g;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/RectF;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 1195040
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1195041
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    if-eqz v0, :cond_0

    .line 1195042
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Potential IndexOutOfBoundsException:mCurrentDataSourceIndex = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but the size of the datastructure = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    iget-object v5, p0, LX/2q6;->z:LX/04g;

    iget-object v5, v5, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, LX/2q6;->w:LX/04D;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v10, v9

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 1195043
    :cond_0
    :goto_0
    return-void

    .line 1195044
    :cond_1
    iget-object v0, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->as:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1195045
    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    iget-object v2, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->j()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->k()I

    move-result v3

    iget-object v4, v0, Lcom/facebook/video/engine/VideoDataSource;->h:LX/2oF;

    if-nez p1, :cond_2

    iget-object p1, v0, Lcom/facebook/video/engine/VideoDataSource;->g:Landroid/graphics/RectF;

    :cond_2
    invoke-static {v2, v3, v4, p1}, LX/7K8;->a(IILX/2oF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2p8;->a(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 1195034
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-nez v0, :cond_1

    .line 1195035
    iget-object v0, p0, LX/2q6;->E:LX/2qD;

    sget-object v1, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v1, :cond_0

    .line 1195036
    invoke-static {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->F(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;)V

    .line 1195037
    :cond_0
    :goto_0
    return-void

    .line 1195038
    :cond_1
    iget v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->af:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1195039
    invoke-static {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1195033
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public final c(LX/04g;)V
    .locals 0

    .prologue
    .line 1194967
    invoke-super {p0, p1}, LX/2q6;->c(LX/04g;)V

    .line 1194968
    invoke-direct {p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->j(LX/04g;)V

    .line 1194969
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1195009
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/2oi;
    .locals 1

    .prologue
    .line 1195008
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ar:LX/2oi;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1195006
    sget-object v0, LX/2qk;->EXTERNAL:LX/2qk;

    invoke-virtual {p0, v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(LX/2qk;)V

    .line 1195007
    return-void
.end method

.method public final f(LX/04g;)V
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 1195002
    iget-object v0, p0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/2q6;->y:LX/04G;

    iget-object v3, p0, LX/2q6;->P:LX/09L;

    iget-object v3, v3, LX/09L;->value:Ljava/lang/String;

    iget-object v4, p1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v5

    iget-object v6, p0, LX/2q6;->S:LX/097;

    iget-object v6, v6, LX/097;->value:Ljava/lang/String;

    iget-object v7, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v7, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p0, LX/2q6;->w:LX/04D;

    iget-object v10, p0, LX/2q6;->z:LX/04g;

    iget-object v10, v10, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v11

    iget-object v13, p0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v12, v9

    invoke-virtual/range {v0 .. v13}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;)LX/1C2;

    .line 1195003
    iput-object p1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->R:LX/04g;

    .line 1195004
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    invoke-virtual {p0, v0}, LX/2q6;->b(LX/2qD;)V

    .line 1195005
    return-void
.end method

.method public final g(LX/04g;)V
    .locals 24
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1194978
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/0Kx;->a(Z)V

    .line 1194979
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->h:LX/2q9;

    invoke-virtual {v2}, LX/2q9;->c()V

    .line 1194980
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->a(LX/2qD;)V

    .line 1194981
    sget-object v2, LX/2qD;->STATE_PAUSED:LX/2qD;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/2q6;->b(LX/2qD;)V

    .line 1194982
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ai:Landroid/net/Uri;

    invoke-static {v2}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v2

    .line 1194983
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->av:LX/19Z;

    sget-object v4, LX/7IJ;->PAUSED:LX/7IJ;

    invoke-virtual {v3, v2, v4}, LX/19Z;->a(ILX/7IJ;)V

    .line 1194984
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 1194985
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 1194986
    invoke-interface {v2}, LX/2pf;->c()V

    goto :goto_0

    .line 1194987
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v2}, LX/2p8;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1194988
    invoke-virtual/range {p0 .. p0}, LX/2q6;->n()V

    .line 1194989
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->Z:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v3}, LX/2p8;->h()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v4}, LX/2p8;->i()I

    move-result v4

    invoke-static {v2, v3, v4}, LX/7K8;->b(III)D

    move-result-wide v2

    .line 1194990
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    invoke-virtual {v4, v2, v3, v2, v3}, LX/2p8;->a(DD)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->H:Landroid/graphics/Bitmap;

    .line 1194991
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2pf;

    .line 1194992
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->H:Landroid/graphics/Bitmap;

    invoke-interface {v2, v4}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 1194993
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->b()I

    move-result v7

    .line 1194994
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v2}, LX/09M;->d()V

    .line 1194995
    invoke-static/range {p1 .. p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->l(LX/04g;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1194996
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2q6;->P:LX/09L;

    iget-object v5, v5, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, LX/2q6;->B:I

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2q6;->z:LX/04g;

    iget-object v11, v11, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v12, v12, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v2 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Z)LX/1C2;

    .line 1194997
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->q:LX/16V;

    new-instance v3, LX/2qT;

    sget-object v4, LX/7IB;->b:LX/7IB;

    invoke-direct {v3, v7, v4}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v2, v3}, LX/16V;->a(LX/1AD;)V

    .line 1194998
    return-void

    .line 1194999
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->P:LX/09L;

    iget-object v2, v2, LX/09L;->value:Ljava/lang/String;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1195000
    move-object/from16 v0, p0

    iget-object v8, v0, LX/2q6;->g:LX/1C2;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2q6;->y:LX/04G;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->P:LX/09L;

    iget-object v11, v2, LX/09L;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2q6;->W:LX/0AR;

    move-object/from16 v0, p0

    iget v13, v0, LX/2q6;->X:I

    move-object/from16 v0, p1

    iget-object v14, v0, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, LX/2q6;->B:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->w:LX/04D;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->z:LX/04g;

    iget-object v0, v2, LX/04g;->value:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->r()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->O:LX/09M;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->S:LX/097;

    iget-object v0, v2, LX/097;->value:Ljava/lang/String;

    move-object/from16 v23, v0

    move v15, v7

    invoke-virtual/range {v8 .. v23}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 1195001
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2q6;->O:LX/09M;

    invoke-virtual {v2}, LX/09M;->a()V

    goto :goto_2
.end method

.method public final k()Landroid/view/View;
    .locals 1

    .prologue
    .line 1194975
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->aE:LX/2p8;

    .line 1194976
    iget-object p0, v0, LX/2p8;->i:Landroid/view/TextureView;

    move-object v0, p0

    .line 1194977
    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1194974
    const-string v0, "old_api_exo_deprecated"

    return-object v0
.end method

.method public final z()V
    .locals 4

    .prologue
    .line 1194970
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1194971
    const-string v0, "to VideoRenderer, MSG_SET_SURFACE to null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1194972
    iget-object v0, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ab:LX/0Kx;

    iget-object v1, p0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;->ac:LX/0GT;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0Kx;->b(LX/0GS;ILjava/lang/Object;)V

    .line 1194973
    :cond_0
    return-void
.end method
