.class public final Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x24bdc496
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1201891
    const-class v0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1201892
    const-class v0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1201876
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1201877
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1201885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1201886
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1201887
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1201888
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1201889
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1201890
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1201893
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1201894
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1201895
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1201896
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1201897
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;

    .line 1201898
    iput-object v0, v1, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1201899
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1201900
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201883
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1201884
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1201880
    new-instance v0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;-><init>()V

    .line 1201881
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1201882
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1201879
    const v0, 0x3ad3ef7b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1201878
    const v0, 0x4ed245b

    return v0
.end method
