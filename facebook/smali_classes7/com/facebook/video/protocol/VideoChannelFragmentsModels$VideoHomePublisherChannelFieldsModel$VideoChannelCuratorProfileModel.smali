.class public final Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5347516b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1201502
    const-class v0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1201503
    const-class v0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1201486
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1201487
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1201504
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1201505
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1201506
    return-void
.end method

.method public static a(Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;)Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;
    .locals 10

    .prologue
    .line 1201509
    if-nez p0, :cond_0

    .line 1201510
    const/4 p0, 0x0

    .line 1201511
    :goto_0
    return-object p0

    .line 1201512
    :cond_0
    instance-of v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    if-eqz v0, :cond_1

    .line 1201513
    check-cast p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    goto :goto_0

    .line 1201514
    :cond_1
    new-instance v0, LX/7OR;

    invoke-direct {v0}, LX/7OR;-><init>()V

    .line 1201515
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/7OR;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1201516
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7OR;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1201517
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->c()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;->a(Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;)Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7OR;->c:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    .line 1201518
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1201519
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1201520
    iget-object v3, v0, LX/7OR;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1201521
    iget-object v5, v0, LX/7OR;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1201522
    iget-object v7, v0, LX/7OR;->c:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1201523
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1201524
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1201525
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1201526
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1201527
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1201528
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1201529
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1201530
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1201531
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1201532
    new-instance v3, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    invoke-direct {v3, v2}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;-><init>(LX/15i;)V

    .line 1201533
    move-object p0, v3

    .line 1201534
    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201507
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1201508
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201545
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->g:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->g:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    .line 1201546
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->g:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1201535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1201536
    invoke-virtual {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1201537
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1201538
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1201539
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1201540
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1201541
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1201542
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1201543
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1201544
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1201488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1201489
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1201490
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1201491
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1201492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    .line 1201493
    iput-object v0, v1, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1201494
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1201495
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    .line 1201496
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1201497
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    .line 1201498
    iput-object v0, v1, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->g:Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    .line 1201499
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1201500
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1201501
    new-instance v0, LX/7OS;

    invoke-direct {v0, p1}, LX/7OS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201476
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1201477
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1201478
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1201473
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1201474
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1201475
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201479
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1201480
    new-instance v0, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    invoke-direct {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;-><init>()V

    .line 1201481
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1201482
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1201483
    invoke-direct {p0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->k()Lcom/facebook/video/protocol/core/VideoFragmentsModels$DefaultStreamingImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1201484
    const v0, 0x6d6326f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1201485
    const v0, 0x50c72189

    return v0
.end method
