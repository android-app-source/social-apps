.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ccb50ff
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192424
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192425
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192426
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192427
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1192428
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192429
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192430
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1192438
    iput-boolean p1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->h:Z

    .line 1192439
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1192440
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1192441
    if-eqz v0, :cond_0

    .line 1192442
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1192443
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192431
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1192432
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1192433
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192434
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    .line 1192435
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192436
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1192437
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1192389
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1192390
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->h:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1192413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192414
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192415
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1192416
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1192417
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1192418
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1192419
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1192420
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1192421
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1192422
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192423
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1192410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192412
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1192409
    new-instance v0, LX/7Ik;

    invoke-direct {v0, p1}, LX/7Ik;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192408
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1192405
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1192406
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->h:Z

    .line 1192407
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1192399
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192400
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1192401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1192402
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 1192403
    :goto_0
    return-void

    .line 1192404
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1192396
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192397
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;->a(Z)V

    .line 1192398
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192393
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;-><init>()V

    .line 1192394
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192395
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192392
    const v0, -0x3d998d9e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192391
    const v0, 0x2d116428

    return v0
.end method
