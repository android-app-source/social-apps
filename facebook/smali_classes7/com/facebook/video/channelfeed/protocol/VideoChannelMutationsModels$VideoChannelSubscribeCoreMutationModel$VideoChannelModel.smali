.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xce48967
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192576
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192575
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192573
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192574
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1192570
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192571
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192572
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1192564
    iput-boolean p1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->g:Z

    .line 1192565
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1192566
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1192567
    if-eqz v0, :cond_0

    .line 1192568
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1192569
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192561
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1192562
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1192563
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192559
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    .line 1192560
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1192557
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1192558
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->g:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1192577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192578
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192579
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1192580
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1192581
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1192582
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1192583
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1192584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1192535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192537
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1192541
    new-instance v0, LX/7In;

    invoke-direct {v0, p1}, LX/7In;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192542
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1192543
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1192544
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->g:Z

    .line 1192545
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1192546
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192547
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1192548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1192549
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1192550
    :goto_0
    return-void

    .line 1192551
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1192552
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192553
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;->a(Z)V

    .line 1192554
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192538
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;-><init>()V

    .line 1192539
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192540
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192555
    const v0, -0x480f3679

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192556
    const v0, 0x2d116428

    return v0
.end method
