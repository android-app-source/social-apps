.class public final Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2d9d03a8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192068
    const-class v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192067
    const-class v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192065
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192066
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192063
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->e:Ljava/lang/String;

    .line 1192064
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192050
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->f:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->f:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    .line 1192051
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->f:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1192053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192054
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1192055
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->l()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1192056
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->j()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1192057
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1192058
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1192059
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1192060
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1192061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192062
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1192069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192070
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->l()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192071
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->l()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    .line 1192072
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->l()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1192073
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;

    .line 1192074
    iput-object v0, v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->f:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$OwnerModel;

    .line 1192075
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->j()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1192076
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->j()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    .line 1192077
    invoke-virtual {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->j()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1192078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;

    .line 1192079
    iput-object v0, v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->g:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    .line 1192080
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192081
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192052
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192047
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;-><init>()V

    .line 1192048
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192049
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192046
    const v0, -0xe774bee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192045
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192043
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->g:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->g:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    .line 1192044
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;->g:Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    return-object v0
.end method
