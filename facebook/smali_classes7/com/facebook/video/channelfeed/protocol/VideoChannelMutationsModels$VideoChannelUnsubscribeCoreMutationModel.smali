.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d938458
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192901
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192900
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192898
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192899
    return-void
.end method

.method private a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192896
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    .line 1192897
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1192890
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192891
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192892
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1192893
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1192894
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192895
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1192882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192883
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192884
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    .line 1192885
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1192886
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;

    .line 1192887
    iput-object v0, v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel$VideoChannelModel;

    .line 1192888
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192889
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192879
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnsubscribeCoreMutationModel;-><init>()V

    .line 1192880
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192881
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192878
    const v0, -0x5303a5d9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192877
    const v0, -0x45eca6bc

    return v0
.end method
