.class public final Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1192041
    const-class v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;

    new-instance v1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1192042
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1192022
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1192024
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1192025
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1192026
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1192027
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1192028
    if-eqz v2, :cond_0

    .line 1192029
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1192030
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1192031
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1192032
    if-eqz v2, :cond_1

    .line 1192033
    const-string p0, "owner"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1192034
    invoke-static {v1, v2, p1}, LX/7IX;->a(LX/15i;ILX/0nX;)V

    .line 1192035
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1192036
    if-eqz v2, :cond_2

    .line 1192037
    const-string p0, "video_channel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1192038
    invoke-static {v1, v2, p1, p2}, LX/7IW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1192039
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1192040
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1192023
    check-cast p1, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel$Serializer;->a(Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideoChannelByVideoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
