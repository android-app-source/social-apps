.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x151020d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192742
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192741
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192739
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192740
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1192733
    iput-boolean p1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->h:Z

    .line 1192734
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1192735
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1192736
    if-eqz v0, :cond_0

    .line 1192737
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1192738
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1192727
    iput-boolean p1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->i:Z

    .line 1192728
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1192729
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1192730
    if-eqz v0, :cond_0

    .line 1192731
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1192732
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192724
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1192725
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1192726
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192722
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    .line 1192723
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192720
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1192721
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1192718
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1192719
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->h:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1192716
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1192717
    iget-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->i:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1192704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192705
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192706
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1192707
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1192708
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1192709
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1192710
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1192711
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1192712
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1192713
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1192714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192715
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1192675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192677
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1192703
    new-instance v0, LX/7Io;

    invoke-direct {v0, p1}, LX/7Io;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192702
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1192698
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1192699
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->h:Z

    .line 1192700
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->i:Z

    .line 1192701
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1192688
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192689
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1192690
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1192691
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 1192692
    :goto_0
    return-void

    .line 1192693
    :cond_0
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1192694
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1192695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1192696
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1192697
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1192683
    const-string v0, "video_channel_has_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1192684
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->a(Z)V

    .line 1192685
    :cond_0
    :goto_0
    return-void

    .line 1192686
    :cond_1
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192687
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;->b(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192680
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelUnfollowCoreMutationModel$VideoChannelModel;-><init>()V

    .line 1192681
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192682
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192679
    const v0, -0x2bd6a1d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192678
    const v0, 0x2d116428

    return v0
.end method
