.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ea7cc9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192471
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192470
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192468
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192469
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1192465
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192466
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192467
    return-void
.end method

.method private a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192463
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    .line 1192464
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1192444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192445
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192446
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1192447
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1192448
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192449
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1192455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192456
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192457
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    .line 1192458
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1192459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;

    .line 1192460
    iput-object v0, v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel$VideoChannelModel;

    .line 1192461
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192462
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192452
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelFollowCoreMutationModel;-><init>()V

    .line 1192453
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192454
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192451
    const v0, 0xd912f68

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192450
    const v0, -0x46c69d2c

    return v0
.end method
