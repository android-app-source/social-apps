.class public final Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x78c85c3e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192611
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1192610
    const-class v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1192608
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192609
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1192605
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1192606
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192607
    return-void
.end method

.method private a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1192612
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    .line 1192613
    iget-object v0, p0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1192599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192600
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1192601
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1192602
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1192603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192604
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1192586
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1192587
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192588
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    .line 1192589
    invoke-direct {p0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->a()Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1192590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;

    .line 1192591
    iput-object v0, v1, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;->e:Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel$VideoChannelModel;

    .line 1192592
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1192593
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1192596
    new-instance v0, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/video/channelfeed/protocol/VideoChannelMutationsModels$VideoChannelSubscribeCoreMutationModel;-><init>()V

    .line 1192597
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1192598
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1192595
    const v0, -0x4d9c4a12

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1192594
    const v0, -0x95dc855

    return v0
.end method
