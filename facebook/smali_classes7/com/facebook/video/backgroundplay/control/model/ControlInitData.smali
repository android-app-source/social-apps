.class public Lcom/facebook/video/backgroundplay/control/model/ControlInitData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/backgroundplay/control/model/ControlInitData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final c:I

.field public final d:Landroid/app/PendingIntent;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1191624
    new-instance v0, LX/7IK;

    invoke-direct {v0}, LX/7IK;-><init>()V

    sput-object v0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7IL;)V
    .locals 1

    .prologue
    .line 1191618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191619
    iget-object v0, p1, LX/7IL;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1191620
    iget-object v0, p1, LX/7IL;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1191621
    iget v0, p1, LX/7IL;->c:I

    iput v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->c:I

    .line 1191622
    iget-object v0, p1, LX/7IL;->d:Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->d:Landroid/app/PendingIntent;

    .line 1191623
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1191612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191613
    const-class v0, Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1191614
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1191615
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->c:I

    .line 1191616
    const-class v0, Landroid/app/PendingIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->d:Landroid/app/PendingIntent;

    .line 1191617
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1191611
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1191606
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1191607
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1191608
    iget v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1191609
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;->d:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1191610
    return-void
.end method
