.class public Lcom/facebook/video/subtitles/views/FbSubtitleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:LX/0Sh;

.field private b:LX/19R;

.field public c:Landroid/widget/TextView;

.field private d:LX/7QP;

.field public e:LX/2q9;

.field private f:LX/7QT;

.field private g:LX/2pw;

.field public h:I

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1204301
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1204302
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1204362
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1204363
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1204349
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1204350
    new-instance v0, LX/7QT;

    invoke-direct {v0, p0}, LX/7QT;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V

    iput-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->f:LX/7QT;

    .line 1204351
    iput v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h:I

    .line 1204352
    iput-boolean v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    .line 1204353
    const v0, 0x7f031416

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1204354
    const-class v0, Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-static {v0, p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1204355
    const v0, 0x7f0d2e22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c:Landroid/widget/TextView;

    .line 1204356
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->b:LX/19R;

    invoke-virtual {v0}, LX/19R;->a()LX/2q9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    .line 1204357
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    iget-object v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->f:LX/7QT;

    .line 1204358
    iput-object v1, v0, LX/2q9;->h:LX/2pw;

    .line 1204359
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    iget-object v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->f:LX/7QT;

    .line 1204360
    iput-object v1, v0, LX/2q9;->a:LX/2qI;

    .line 1204361
    return-void
.end method

.method private a(LX/0Sh;LX/19R;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1204346
    iput-object p1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    .line 1204347
    iput-object p2, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->b:LX/19R;

    .line 1204348
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-static {v1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {v1}, LX/19R;->a(LX/0QB;)LX/19R;

    move-result-object v1

    check-cast v1, LX/19R;

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(LX/0Sh;LX/19R;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/subtitles/views/FbSubtitleView;J)V
    .locals 3

    .prologue
    .line 1204344
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/subtitles/views/FbSubtitleView$5;

    invoke-direct {v1, p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView$5;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V

    invoke-virtual {v0, v1, p1, p2}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 1204345
    return-void
.end method

.method public static synthetic b(Lcom/facebook/video/subtitles/views/FbSubtitleView;)I
    .locals 2

    .prologue
    .line 1204343
    iget v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h:I

    return v0
.end method

.method public static synthetic c(Lcom/facebook/video/subtitles/views/FbSubtitleView;)I
    .locals 2

    .prologue
    .line 1204342
    iget v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h:I

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1204340
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/subtitles/views/FbSubtitleView$3;

    invoke-direct {v1, p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView$3;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1204341
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1204336
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1204337
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1204338
    :goto_0
    return-void

    .line 1204339
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/subtitles/views/FbSubtitleView$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView$1;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(LX/2pw;LX/7QP;)V
    .locals 2

    .prologue
    .line 1204330
    iput-object p1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->g:LX/2pw;

    .line 1204331
    iput-object p2, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->d:LX/7QP;

    .line 1204332
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    iget-object v1, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->d:LX/7QP;

    invoke-virtual {v0, v1}, LX/2q9;->a(LX/7QP;)Z

    .line 1204333
    invoke-direct {p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h()V

    .line 1204334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    .line 1204335
    return-void
.end method

.method public final a(LX/7QO;)V
    .locals 2

    .prologue
    .line 1204328
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/subtitles/views/FbSubtitleView$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/subtitles/views/FbSubtitleView$4;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;LX/7QO;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1204329
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1204324
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1204325
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1204326
    :goto_0
    return-void

    .line 1204327
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/video/subtitles/views/FbSubtitleView$2;

    invoke-direct {v1, p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView$2;-><init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1204321
    iget-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1204322
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->b()V

    .line 1204323
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1204318
    iget-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1204319
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    invoke-virtual {v0, p1}, LX/2q9;->a(I)V

    .line 1204320
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1204315
    iget-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1204316
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->c()V

    .line 1204317
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1204311
    iget-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    move v0, v0

    .line 1204312
    if-eqz v0, :cond_0

    .line 1204313
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    invoke-virtual {v0}, LX/2q9;->d()V

    .line 1204314
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1204307
    invoke-virtual {p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e()V

    .line 1204308
    invoke-direct {p0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->h()V

    .line 1204309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->i:Z

    .line 1204310
    return-void
.end method

.method public getMediaTimeMs()I
    .locals 1

    .prologue
    .line 1204304
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->g:LX/2pw;

    if-eqz v0, :cond_0

    .line 1204305
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->g:LX/2pw;

    invoke-interface {v0}, LX/2pw;->a()I

    move-result v0

    .line 1204306
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubtitleAdapter()LX/2q9;
    .locals 1

    .prologue
    .line 1204303
    iget-object v0, p0, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    return-object v0
.end method
