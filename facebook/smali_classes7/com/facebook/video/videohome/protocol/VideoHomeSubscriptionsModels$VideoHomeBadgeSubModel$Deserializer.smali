.class public final Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1206185
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1206186
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1206187
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1206188
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1206189
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1206190
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1206191
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1206192
    :goto_0
    move v1, v2

    .line 1206193
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1206194
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1206195
    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;-><init>()V

    .line 1206196
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1206197
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1206198
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1206199
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1206200
    :cond_0
    return-object v1

    .line 1206201
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_6

    .line 1206202
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1206203
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1206204
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v10, :cond_1

    .line 1206205
    const-string p0, "video_home_badge_count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1206206
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v3

    goto :goto_1

    .line 1206207
    :cond_2
    const-string p0, "video_home_badge_update_reason"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1206208
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1206209
    :cond_3
    const-string p0, "video_home_max_badge_count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1206210
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v3

    goto :goto_1

    .line 1206211
    :cond_4
    const-string p0, "video_home_prefetch_unit_count"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1206212
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 1206213
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1206214
    :cond_6
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1206215
    if-eqz v5, :cond_7

    .line 1206216
    invoke-virtual {v0, v2, v9, v2}, LX/186;->a(III)V

    .line 1206217
    :cond_7
    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1206218
    if-eqz v4, :cond_8

    .line 1206219
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7, v2}, LX/186;->a(III)V

    .line 1206220
    :cond_8
    if-eqz v1, :cond_9

    .line 1206221
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6, v2}, LX/186;->a(III)V

    .line 1206222
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
