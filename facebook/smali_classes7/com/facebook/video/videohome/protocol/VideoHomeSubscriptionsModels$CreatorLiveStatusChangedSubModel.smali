.class public final Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2b9f5e4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205826
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205825
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1205823
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1205824
    return-void
.end method

.method private k()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205821
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->g:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->g:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    .line 1205822
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->g:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1205811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205812
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1205813
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->j()Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1205814
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->k()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1205815
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1205816
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1205817
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1205818
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1205819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205820
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1205789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205790
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1205791
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    .line 1205792
    invoke-virtual {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1205793
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    .line 1205794
    iput-object v0, v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->e:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    .line 1205795
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->k()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1205796
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->k()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    .line 1205797
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->k()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1205798
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    .line 1205799
    iput-object v0, v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->g:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$VideoModel;

    .line 1205800
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205801
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205809
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->e:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->e:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    .line 1205810
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->e:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$CreatorModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1205806
    new-instance v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    invoke-direct {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;-><init>()V

    .line 1205807
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1205808
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1205805
    const v0, 0x768f55eb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1205804
    const v0, -0x20bd2d7d

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205802
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    .line 1205803
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;->f:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    return-object v0
.end method
