.class public final Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x23a96375
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205123
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205083
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1205121
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1205122
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1205118
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1205119
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1205120
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 4

    .prologue
    .line 1205111
    iput-object p1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1205112
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1205113
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1205114
    if-eqz v0, :cond_0

    .line 1205115
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1205116
    :cond_0
    return-void

    .line 1205117
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205109
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->e:Ljava/lang/String;

    .line 1205110
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205107
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1205108
    iget-object v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1205099
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205100
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1205101
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1205102
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1205103
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1205104
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1205105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205106
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1205124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205126
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1205098
    new-instance v0, LX/7Qy;

    invoke-direct {v0, p1}, LX/7Qy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1205097
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1205091
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1205092
    invoke-direct {p0}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1205093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1205094
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1205095
    :goto_0
    return-void

    .line 1205096
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1205088
    const-string v0, "seen_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1205089
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-direct {p0, p2}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    .line 1205090
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1205085
    new-instance v0, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeMutationsModels$VideoHomeNotificationStorySeenStateMutationFieldsModel;-><init>()V

    .line 1205086
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1205087
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1205084
    const v0, -0x2c4f6959

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1205082
    const v0, 0x4c808d5

    return v0
.end method
