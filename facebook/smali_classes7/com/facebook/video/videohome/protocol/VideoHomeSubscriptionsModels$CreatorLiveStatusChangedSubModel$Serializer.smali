.class public final Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1205727
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1205728
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1205730
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1205731
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1205732
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 1205733
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1205734
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1205735
    if-eqz v2, :cond_0

    .line 1205736
    const-string v3, "creator"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205737
    invoke-static {v1, v2, p1}, LX/7RK;->a(LX/15i;ILX/0nX;)V

    .line 1205738
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1205739
    if-eqz v2, :cond_1

    .line 1205740
    const-string v2, "creator_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205741
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1205742
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1205743
    if-eqz v2, :cond_2

    .line 1205744
    const-string v3, "video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205745
    invoke-static {v1, v2, p1}, LX/7RL;->a(LX/15i;ILX/0nX;)V

    .line 1205746
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1205747
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1205729
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel$Serializer;->a(Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;LX/0nX;LX/0my;)V

    return-void
.end method
