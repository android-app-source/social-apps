.class public final Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1206223
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1206224
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1206225
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1206226
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1206227
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 1206228
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1206229
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1206230
    if-eqz v2, :cond_0

    .line 1206231
    const-string v3, "video_home_badge_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206232
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1206233
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1206234
    if-eqz v2, :cond_1

    .line 1206235
    const-string v2, "video_home_badge_update_reason"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206236
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1206237
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1206238
    if-eqz v2, :cond_2

    .line 1206239
    const-string v3, "video_home_max_badge_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206240
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1206241
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1206242
    if-eqz v2, :cond_3

    .line 1206243
    const-string v3, "video_home_prefetch_unit_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1206244
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1206245
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1206246
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1206247
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel$Serializer;->a(Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;LX/0nX;LX/0my;)V

    return-void
.end method
