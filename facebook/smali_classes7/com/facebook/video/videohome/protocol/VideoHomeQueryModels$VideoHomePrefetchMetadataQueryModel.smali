.class public final Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3c24ee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205547
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1205546
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1205544
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1205545
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1205542
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1205543
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1205535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205536
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1205537
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 1205538
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1205539
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1205540
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205541
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1205548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1205549
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1205550
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1205530
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1205531
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->e:I

    .line 1205532
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->f:I

    .line 1205533
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->g:I

    .line 1205534
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1205527
    new-instance v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;-><init>()V

    .line 1205528
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1205529
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1205526
    const v0, 0x457d1e2f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1205525
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1205523
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1205524
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->f:I

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1205521
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1205522
    iget v0, p0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->g:I

    return v0
.end method
