.class public final Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1205419
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;

    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1205420
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1205394
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1205395
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1205396
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1205397
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1205398
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1205399
    if-eqz v2, :cond_0

    .line 1205400
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205401
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1205402
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1205403
    if-eqz v2, :cond_1

    .line 1205404
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205405
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1205406
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1205407
    if-eqz v2, :cond_3

    .line 1205408
    const-string p0, "video_channel_creator_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205409
    const/4 v0, 0x0

    .line 1205410
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1205411
    invoke-virtual {v1, v2, v0}, LX/15i;->g(II)I

    move-result p0

    .line 1205412
    if-eqz p0, :cond_2

    .line 1205413
    const-string p0, "creator_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205414
    invoke-virtual {v1, v2, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1205415
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1205416
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1205417
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1205418
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel$Serializer;->a(Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;LX/0nX;LX/0my;)V

    return-void
.end method
