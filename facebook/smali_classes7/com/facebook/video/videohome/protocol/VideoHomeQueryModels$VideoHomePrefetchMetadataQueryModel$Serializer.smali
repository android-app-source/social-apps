.class public final Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1205502
    const-class v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    new-instance v1, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1205503
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1205501
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1205504
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1205505
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1205506
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1205507
    invoke-virtual {v1, v0, p2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1205508
    if-eqz v2, :cond_0

    .line 1205509
    const-string p0, "video_home_badge_count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205510
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1205511
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1205512
    if-eqz v2, :cond_1

    .line 1205513
    const-string p0, "video_home_prefetch_unit_count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205514
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1205515
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1205516
    if-eqz v2, :cond_2

    .line 1205517
    const-string p0, "video_home_stale_data_interval_s"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1205518
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1205519
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1205520
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1205500
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel$Serializer;->a(Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
