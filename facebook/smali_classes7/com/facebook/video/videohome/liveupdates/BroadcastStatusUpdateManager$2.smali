.class public final Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7Qb;


# direct methods
.method public constructor <init>(LX/7Qb;)V
    .locals 0

    .prologue
    .line 1204516
    iput-object p1, p0, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$2;->a:LX/7Qb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1204506
    iget-object v0, p0, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$2;->a:LX/7Qb;

    .line 1204507
    invoke-static {v0}, LX/7Qb;->h(LX/7Qb;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1204508
    iget-object v1, v0, LX/7Qb;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v2, "BroadcastStatusUpdateManager.subscribeToSavedVideos()"

    const-string p0, "App is in bg"

    invoke-virtual {v1, v2, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204509
    :cond_0
    :goto_0
    return-void

    .line 1204510
    :cond_1
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1204511
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 1204512
    iget-object v1, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Qa;

    .line 1204513
    invoke-virtual {v1}, LX/7Qa;->b()V

    goto :goto_1

    .line 1204514
    :cond_2
    iget-object v1, v0, LX/7Qb;->h:LX/7QZ;

    iget-object v2, v0, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7QZ;->a(Ljava/util/Collection;)V

    .line 1204515
    iget-object v1, v0, LX/7Qb;->h:LX/7QZ;

    invoke-virtual {v1}, LX/7QZ;->a()V

    goto :goto_0
.end method
