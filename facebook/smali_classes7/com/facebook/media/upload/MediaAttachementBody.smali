.class public Lcom/facebook/media/upload/MediaAttachementBody;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/media/upload/MediaAttachementBody;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:[B

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1309016
    new-instance v0, LX/8BK;

    invoke-direct {v0}, LX/8BK;-><init>()V

    sput-object v0, Lcom/facebook/media/upload/MediaAttachementBody;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1309017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309018
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->a:Ljava/lang/String;

    .line 1309019
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    .line 1309020
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 1309021
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->c:Ljava/lang/String;

    .line 1309022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->d:Ljava/lang/String;

    .line 1309023
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1309024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309025
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attachement must have data"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1309026
    iput-object p1, p0, Lcom/facebook/media/upload/MediaAttachementBody;->a:Ljava/lang/String;

    .line 1309027
    iput-object p2, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    .line 1309028
    iput-object p3, p0, Lcom/facebook/media/upload/MediaAttachementBody;->c:Ljava/lang/String;

    .line 1309029
    iput-object p4, p0, Lcom/facebook/media/upload/MediaAttachementBody;->d:Ljava/lang/String;

    .line 1309030
    return-void

    .line 1309031
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/4cQ;
    .locals 4

    .prologue
    .line 1309032
    new-instance v0, LX/4cq;

    iget-object v1, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    iget-object v2, p0, Lcom/facebook/media/upload/MediaAttachementBody;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/media/upload/MediaAttachementBody;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1309033
    new-instance v1, LX/4cQ;

    iget-object v2, p0, Lcom/facebook/media/upload/MediaAttachementBody;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    return-object v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1309034
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1309035
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309036
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1309037
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1309038
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309039
    iget-object v0, p0, Lcom/facebook/media/upload/MediaAttachementBody;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309040
    return-void
.end method
