.class public final Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public endOffset:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_offset"
    .end annotation
.end field

.field public startOffset:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_offset"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1309184
    const-class v0, Lcom/facebook/media/upload/video/VideoUploadErrorHandler_InvalidOffsetErrorDataDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 1309181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309182
    iput-wide v0, p0, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->startOffset:J

    .line 1309183
    iput-wide v0, p0, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->endOffset:J

    return-void
.end method
