.class public Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/11H;

.field private final c:LX/0So;

.field private final d:LX/8BY;

.field private final e:LX/7z2;

.field private final f:LX/6b7;

.field private final g:LX/6b9;

.field private h:F

.field private i:LX/14U;

.field private j:LX/8Bb;

.field private k:LX/8BR;

.field private l:LX/8BQ;

.field private m:LX/8BO;

.field private n:Ljava/lang/String;

.field public o:Lcom/facebook/photos/base/media/VideoItem;

.field private p:Lcom/facebook/media/upload/MediaUploadParameters;

.field private q:Ljava/lang/String;

.field private r:J

.field private s:LX/8Ba;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1309653
    const-class v0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/0So;LX/7zS;LX/8BR;LX/6b9;LX/6b7;)V
    .locals 1
    .param p6    # LX/6b7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309644
    iput-object p1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->b:LX/11H;

    .line 1309645
    iput-object p2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c:LX/0So;

    .line 1309646
    iget-object v0, p3, LX/7zS;->a:LX/7z2;

    move-object v0, v0

    .line 1309647
    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->e:LX/7z2;

    .line 1309648
    iput-object p4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->k:LX/8BR;

    .line 1309649
    iput-object p5, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->g:LX/6b9;

    .line 1309650
    iput-object p6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->f:LX/6b7;

    .line 1309651
    new-instance v0, LX/8BY;

    invoke-direct {v0}, LX/8BY;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->d:LX/8BY;

    .line 1309652
    return-void
.end method

.method private static a(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;Ljava/lang/String;)LX/8Bd;
    .locals 15

    .prologue
    .line 1309629
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v2, v2, LX/8Ba;->mCurrentEndOffset:J

    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentStartOffset:J

    sub-long v6, v2, v4

    .line 1309630
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->f:LX/6b7;

    iget-object v3, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentStartOffset:J

    move-object/from16 v8, p1

    invoke-virtual/range {v2 .. v8}, LX/6b7;->a(Ljava/lang/String;JJLjava/lang/String;)V

    .line 1309631
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    .line 1309632
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    const-string v3, "Before sending chunk with resumable"

    invoke-virtual {v2, v3}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309633
    invoke-static {p0}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;)Ljava/lang/String;

    move-result-object v14

    .line 1309634
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    const-string v3, "After sending chunk with resumable"

    invoke-virtual {v2, v3}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309635
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->b:LX/11H;

    iget-object v3, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->d:LX/8BY;

    new-instance v8, LX/8BZ;

    iget-object v9, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v10, v10, LX/8Ba;->mCurrentStartOffset:J

    iget v12, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->h:F

    iget-object v13, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->p:Lcom/facebook/media/upload/MediaUploadParameters;

    invoke-direct/range {v8 .. v14}, LX/8BZ;-><init>(Ljava/lang/String;JFLcom/facebook/media/upload/MediaUploadParameters;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->i:LX/14U;

    sget-object v10, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v8, v9, v10}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, LX/8Bd;

    move-object v10, v0

    .line 1309636
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    const-string v3, "After sending result handle"

    invoke-virtual {v2, v3}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309637
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1309638
    long-to-float v8, v6

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-float v2, v2

    div-float v2, v8, v2

    iput v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->h:F

    .line 1309639
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->f:LX/6b7;

    iget-object v3, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentStartOffset:J

    iget-object v8, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    invoke-virtual {v8}, LX/8BQ;->a()I

    move-result v8

    move-object/from16 v9, p1

    invoke-virtual/range {v2 .. v9}, LX/6b7;->a(Ljava/lang/String;JJILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1309640
    return-object v10

    .line 1309641
    :catch_0
    move-exception v2

    .line 1309642
    iget-object v3, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    invoke-virtual {v3, v2}, LX/8BQ;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;Ljava/lang/String;J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1309516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1309517
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309518
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309519
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1309520
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309521
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1309522
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309523
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget v2, v1, LX/8Ba;->mChunkIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v1, LX/8Ba;->mChunkIndex:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1309524
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;)V
    .locals 15

    .prologue
    .line 1309574
    const-string v0, ""

    .line 1309575
    :goto_0
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v2, v1, LX/8Ba;->mCurrentStartOffset:J

    iget-wide v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->r:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1309576
    :try_start_0
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->j:LX/8Bb;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v2, v2, LX/8Ba;->mCurrentStartOffset:J

    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentEndOffset:J

    .line 1309577
    iput-wide v2, v1, LX/8Bb;->c:J

    .line 1309578
    const-wide/16 v12, 0x0

    iput-wide v12, v1, LX/8Bb;->g:J

    .line 1309579
    sub-long v12, v4, v2

    iput-wide v12, v1, LX/8Bb;->f:J

    .line 1309580
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->p:Lcom/facebook/media/upload/MediaUploadParameters;

    .line 1309581
    iget-object v2, v1, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1309582
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v2, v2, LX/8Ba;->mCurrentStartOffset:J

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->a(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 1309583
    invoke-static {p0, v0}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->a(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;Ljava/lang/String;)LX/8Bd;

    move-result-object v1

    .line 1309584
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    .line 1309585
    iget-wide v12, v1, LX/8Bd;->a:J

    move-wide v4, v12

    .line 1309586
    iput-wide v4, v2, LX/8Ba;->mCurrentStartOffset:J

    .line 1309587
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    .line 1309588
    iget-wide v12, v1, LX/8Bd;->b:J

    move-wide v4, v12

    .line 1309589
    iput-wide v4, v2, LX/8Ba;->mCurrentEndOffset:J
    :try_end_0
    .catch LX/8BP; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1309590
    :goto_1
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->g:LX/6b9;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    invoke-virtual {v1, v2, v3}, LX/6b9;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1309591
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    .line 1309592
    invoke-virtual {v1}, LX/8BQ;->b()V

    .line 1309593
    goto :goto_0

    .line 1309594
    :catch_0
    move-exception v1

    .line 1309595
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    .line 1309596
    iget-wide v12, v1, LX/8BP;->mNewStartOffset:J

    move-wide v4, v12

    .line 1309597
    iput-wide v4, v2, LX/8Ba;->mCurrentStartOffset:J

    .line 1309598
    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    .line 1309599
    iget-wide v12, v1, LX/8BP;->mNewEndOffset:J

    move-wide v4, v12

    .line 1309600
    iput-wide v4, v2, LX/8Ba;->mCurrentEndOffset:J

    goto :goto_1

    .line 1309601
    :catch_1
    move-exception v1

    move-object v8, v1

    move-object v7, v0

    .line 1309602
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->f:LX/6b7;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->j:LX/8Bb;

    .line 1309603
    iget-wide v12, v2, LX/8Bb;->g:J

    move-wide v2, v12

    .line 1309604
    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentEndOffset:J

    iget-object v6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v10, v6, LX/8Ba;->mCurrentStartOffset:J

    sub-long/2addr v4, v10

    iget-object v6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    .line 1309605
    iget v9, v6, LX/8BQ;->a:I

    move v6, v9

    .line 1309606
    invoke-static {v0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v9

    .line 1309607
    const-string v10, "upload_session_id"

    invoke-interface {v9, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309608
    const-string v10, "sent_bytes"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309609
    const-string v10, "total_bytes"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309610
    const-string v10, "auto_retry_count"

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309611
    const-string v10, "video_chunk_id"

    invoke-interface {v9, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309612
    sget-object v10, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL:LX/74R;

    invoke-static {v0, v10, v9}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309613
    throw v8

    .line 1309614
    :catch_2
    move-exception v8

    move-object v7, v0

    .line 1309615
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->f:LX/6b7;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->j:LX/8Bb;

    .line 1309616
    iget-wide v12, v2, LX/8Bb;->g:J

    move-wide v2, v12

    .line 1309617
    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v4, v4, LX/8Ba;->mCurrentEndOffset:J

    iget-object v6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v10, v6, LX/8Ba;->mCurrentStartOffset:J

    sub-long/2addr v4, v10

    iget-object v6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    .line 1309618
    iget v9, v6, LX/8BQ;->a:I

    move v6, v9

    .line 1309619
    invoke-static {v0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v9

    .line 1309620
    const-string v10, "upload_session_id"

    invoke-interface {v9, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309621
    const-string v10, "sent_bytes"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309622
    const-string v10, "total_bytes"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309623
    const-string v10, "auto_retry_count"

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309624
    const-string v10, "video_chunk_id"

    invoke-interface {v9, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309625
    invoke-static {v9, v8}, LX/6b7;->a(Ljava/util/Map;Ljava/lang/Exception;)V

    .line 1309626
    sget-object v10, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE:LX/74R;

    invoke-static {v0, v10, v9}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309627
    throw v8

    .line 1309628
    :cond_0
    return-void
.end method

.method private static c(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 1309559
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v0, v0, LX/8Ba;->mCurrentStartOffset:J

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    iget-wide v2, v2, LX/8Ba;->mCurrentEndOffset:J

    .line 1309560
    new-instance v5, LX/7yy;

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sub-long v9, v2, v0

    iget-object v7, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v11

    move-wide v7, v0

    invoke-direct/range {v5 .. v11}, LX/7yy;-><init>(Ljava/io/File;JJLjava/lang/String;)V

    move-object v0, v5

    .line 1309561
    new-instance v1, LX/7yu;

    invoke-direct {v1, v4, v4, v4}, LX/7yu;-><init>(III)V

    .line 1309562
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1309563
    const-string v3, "X_FB_VIDEO_WATERFALL_ID"

    iget-object v4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->p:Lcom/facebook/media/upload/MediaUploadParameters;

    .line 1309564
    iget-object v5, v4, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1309565
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309566
    new-instance v3, LX/7yw;

    sget-object v4, LX/7yt;->FACEBOOK:LX/7yt;

    invoke-direct {v3, v4, v2, v1}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;)V

    .line 1309567
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->e:LX/7z2;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->j:LX/8Bb;

    invoke-virtual {v1, v0, v3, v2}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;

    move-result-object v0

    .line 1309568
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    .line 1309569
    iput-object v0, v1, LX/8BO;->d:LX/7z0;

    .line 1309570
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->e:LX/7z2;

    invoke-virtual {v1, v0}, LX/7z2;->c(LX/7z0;)LX/7zL;

    move-result-object v0

    iget-object v0, v0, LX/7zL;->a:Ljava/lang/String;

    .line 1309571
    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    const/4 v2, 0x0

    .line 1309572
    iput-object v2, v1, LX/8BO;->d:LX/7z0;

    .line 1309573
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/upload/MediaUploadParameters;LX/8Bh;LX/8BO;)V
    .locals 11

    .prologue
    .line 1309525
    iget-boolean v0, p3, LX/8Bh;->mSkipUpload:Z

    move v0, v0

    .line 1309526
    if-eqz v0, :cond_0

    .line 1309527
    :goto_0
    return-void

    .line 1309528
    :cond_0
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    if-nez v0, :cond_2

    .line 1309529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1309530
    iget-object v1, p2, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1309531
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_ReceiveRequestState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->n:Ljava/lang/String;

    .line 1309532
    iput-object p1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    .line 1309533
    iput-object p2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->p:Lcom/facebook/media/upload/MediaUploadParameters;

    .line 1309534
    iget-object v0, p3, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    move-object v0, v0

    .line 1309535
    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->q:Ljava/lang/String;

    .line 1309536
    new-instance v9, Ljava/io/File;

    iget-object v10, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v10}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    move-wide v0, v9

    .line 1309537
    iput-wide v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->r:J

    .line 1309538
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->h:F

    .line 1309539
    iput-object p4, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    .line 1309540
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->k:LX/8BR;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    invoke-virtual {v0, v1}, LX/8BR;->a(LX/8BO;)LX/8BQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    .line 1309541
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->i:LX/14U;

    .line 1309542
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->i:LX/14U;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->m:LX/8BO;

    .line 1309543
    iget-object v2, v1, LX/8BO;->a:LX/4d1;

    move-object v1, v2

    .line 1309544
    iput-object v1, v0, LX/14U;->c:LX/4d1;

    .line 1309545
    new-instance v1, LX/8Bb;

    .line 1309546
    iget-object v0, p2, Lcom/facebook/media/upload/MediaUploadParameters;->h:LX/8Oo;

    move-object v3, v0

    .line 1309547
    iget-wide v9, p3, LX/8Bh;->mStartOffset:J

    move-wide v4, v9

    .line 1309548
    iget-wide v6, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->r:J

    iget-object v8, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->c:LX/0So;

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, LX/8Bb;-><init>(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;LX/8Oo;JJLX/0So;)V

    iput-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->j:LX/8Bb;

    .line 1309549
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->g:LX/6b9;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->n:Ljava/lang/String;

    const-class v2, LX/8Ba;

    invoke-virtual {v0, v1, v2}, LX/6b9;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ba;

    .line 1309550
    if-eqz v0, :cond_1

    iget-object v1, v0, LX/8Ba;->mVideoPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1309551
    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    .line 1309552
    :goto_1
    invoke-static {p0}, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->b(Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;)V

    goto/16 :goto_0

    .line 1309553
    :cond_1
    new-instance v0, LX/8Ba;

    iget-object v1, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    .line 1309554
    iget-wide v9, p3, LX/8Bh;->mStartOffset:J

    move-wide v2, v9

    .line 1309555
    iget-wide v9, p3, LX/8Bh;->mEndOffset:J

    move-wide v4, v9

    .line 1309556
    invoke-direct/range {v0 .. v5}, LX/8Ba;-><init>(Ljava/lang/String;JJ)V

    iput-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->s:LX/8Ba;

    goto :goto_1

    .line 1309557
    :cond_2
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->o:Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Request manager should not be reused for different files."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1309558
    iget-object v0, p0, Lcom/facebook/media/upload/video/receive/VideoUploadReceiveRequestManager;->l:LX/8BQ;

    invoke-virtual {v0}, LX/8BQ;->b()V

    goto :goto_1
.end method
