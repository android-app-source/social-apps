.class public Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/11H;

.field private c:LX/8Be;

.field private d:LX/14U;

.field private e:LX/8BR;

.field private f:LX/6b7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1309711
    const-class v0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/8BR;LX/6b7;)V
    .locals 1
    .param p3    # LX/6b7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309713
    iput-object p1, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->b:LX/11H;

    .line 1309714
    iput-object p2, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->e:LX/8BR;

    .line 1309715
    iput-object p3, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->f:LX/6b7;

    .line 1309716
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->d:LX/14U;

    .line 1309717
    new-instance v0, LX/8Be;

    invoke-direct {v0}, LX/8Be;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->c:LX/8Be;

    .line 1309718
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/upload/MediaUploadParameters;LX/8BO;)LX/8Bh;
    .locals 8

    .prologue
    .line 1309719
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1309720
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    move-wide v0, v6

    .line 1309721
    new-instance v2, LX/8Bf;

    invoke-direct {v2, v0, v1, p2}, LX/8Bf;-><init>(JLcom/facebook/media/upload/MediaUploadParameters;)V

    .line 1309722
    iget-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->e:LX/8BR;

    invoke-virtual {v0, p3}, LX/8BR;->a(LX/8BO;)LX/8BQ;

    move-result-object v1

    .line 1309723
    iget-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->d:LX/14U;

    .line 1309724
    iget-object v3, p3, LX/8BO;->a:LX/4d1;

    move-object v3, v3

    .line 1309725
    iput-object v3, v0, LX/14U;->c:LX/4d1;

    .line 1309726
    iget-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->f:LX/6b7;

    .line 1309727
    invoke-static {v0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v3

    .line 1309728
    sget-object v4, LX/74R;->MEDIA_UPLOAD_INIT_START:LX/74R;

    invoke-static {v0, v4, v3}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309729
    :goto_0
    :try_start_0
    const-string v0, "Before sending start request"

    invoke-virtual {p3, v0}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309730
    iget-object v0, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->b:LX/11H;

    iget-object v3, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->c:LX/8Be;

    iget-object v4, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->d:LX/14U;

    sget-object v5, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v2, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Bh;

    .line 1309731
    invoke-virtual {v1}, LX/8BQ;->b()V

    .line 1309732
    iget-object v3, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->f:LX/6b7;

    .line 1309733
    iget-object v4, v0, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    move-object v4, v4

    .line 1309734
    invoke-static {v3}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v5

    .line 1309735
    const-string v6, "upload_session_id"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309736
    sget-object v6, LX/74R;->MEDIA_UPLOAD_INIT_SUCCESS:LX/74R;

    invoke-static {v3, v6, v5}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1309737
    return-object v0

    .line 1309738
    :catch_0
    move-exception v0

    .line 1309739
    :try_start_1
    invoke-virtual {v1, v0}, LX/8BQ;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 1309740
    :catch_1
    move-exception v0

    .line 1309741
    iget-object v1, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->f:LX/6b7;

    .line 1309742
    invoke-static {v1}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v2

    .line 1309743
    sget-object v3, LX/74R;->MEDIA_UPLOAD_INIT_CANCEL:LX/74R;

    invoke-static {v1, v3, v2}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309744
    throw v0

    .line 1309745
    :catch_2
    move-exception v0

    .line 1309746
    iget-object v1, p0, Lcom/facebook/media/upload/video/start/VideoUploadStartRequestManager;->f:LX/6b7;

    .line 1309747
    invoke-static {v1}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v2

    .line 1309748
    invoke-static {v2, v0}, LX/6b7;->a(Ljava/util/Map;Ljava/lang/Exception;)V

    .line 1309749
    sget-object v3, LX/74R;->MEDIA_UPLOAD_INIT_FAILURE:LX/74R;

    invoke-static {v1, v3, v2}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309750
    throw v0
.end method
