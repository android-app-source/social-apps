.class public Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/11H;

.field private c:LX/8BV;

.field private d:LX/14U;

.field private e:LX/8BR;

.field private f:LX/6b7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1309373
    const-class v0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/8BR;LX/6b7;)V
    .locals 1
    .param p3    # LX/6b7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309375
    iput-object p1, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->b:LX/11H;

    .line 1309376
    iput-object p2, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->e:LX/8BR;

    .line 1309377
    iput-object p3, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->f:LX/6b7;

    .line 1309378
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->d:LX/14U;

    .line 1309379
    new-instance v0, LX/8BV;

    invoke-direct {v0}, LX/8BV;-><init>()V

    iput-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->c:LX/8BV;

    .line 1309380
    return-void
.end method


# virtual methods
.method public final a(LX/8Bh;Lcom/facebook/media/upload/MediaUploadParameters;LX/8BO;)Ljava/lang/Boolean;
    .locals 7

    .prologue
    .line 1309381
    iget-object v0, p1, LX/8Bh;->mSessionFbid:Ljava/lang/String;

    move-object v1, v0

    .line 1309382
    new-instance v2, LX/8BW;

    invoke-direct {v2, v1, p2}, LX/8BW;-><init>(Ljava/lang/String;Lcom/facebook/media/upload/MediaUploadParameters;)V

    .line 1309383
    iget-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->e:LX/8BR;

    invoke-virtual {v0, p3}, LX/8BR;->a(LX/8BO;)LX/8BQ;

    move-result-object v3

    .line 1309384
    iget-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->d:LX/14U;

    .line 1309385
    iget-object v4, p3, LX/8BO;->a:LX/4d1;

    move-object v4, v4

    .line 1309386
    iput-object v4, v0, LX/14U;->c:LX/4d1;

    .line 1309387
    iget-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->f:LX/6b7;

    .line 1309388
    invoke-static {v0}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v4

    .line 1309389
    const-string v5, "upload_session_id"

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309390
    sget-object v5, LX/74R;->MEDIA_POST_START:LX/74R;

    invoke-static {v0, v5, v4}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309391
    :goto_0
    :try_start_0
    const-string v0, "Before sending post request"

    invoke-virtual {p3, v0}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309392
    iget-object v0, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->b:LX/11H;

    iget-object v4, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->c:LX/8BV;

    iget-object v5, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->d:LX/14U;

    sget-object v6, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v2, v5, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1309393
    invoke-virtual {v3}, LX/8BQ;->b()V

    .line 1309394
    iget-object v4, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->f:LX/6b7;

    .line 1309395
    invoke-static {v4}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v5

    .line 1309396
    const-string v6, "upload_session_id"

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309397
    sget-object v6, LX/74R;->MEDIA_POST_SUCCESS:LX/74R;

    invoke-static {v4, v6, v5}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1309398
    return-object v0

    .line 1309399
    :catch_0
    move-exception v0

    .line 1309400
    :try_start_1
    invoke-virtual {v3, v0}, LX/8BQ;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1309401
    :catch_1
    move-exception v0

    .line 1309402
    iget-object v2, p0, Lcom/facebook/media/upload/video/post/VideoUploadPostRequestManager;->f:LX/6b7;

    .line 1309403
    invoke-static {v2}, LX/6b7;->f(LX/6b7;)Ljava/util/Map;

    move-result-object v3

    .line 1309404
    const-string v4, "upload_session_id"

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309405
    invoke-static {v3, v0}, LX/6b7;->a(Ljava/util/Map;Ljava/lang/Exception;)V

    .line 1309406
    sget-object v4, LX/74R;->MEDIA_POST_FAILURE:LX/74R;

    invoke-static {v2, v4, v3}, LX/6b7;->a(LX/6b7;LX/74R;Ljava/util/Map;)V

    .line 1309407
    throw v0
.end method
