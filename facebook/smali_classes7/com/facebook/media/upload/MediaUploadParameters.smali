.class public Lcom/facebook/media/upload/MediaUploadParameters;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/media/upload/MediaUploadParameters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/upload/MediaAttachementBody;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/8Oo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1309105
    new-instance v0, LX/8BL;

    invoke-direct {v0}, LX/8BL;-><init>()V

    sput-object v0, Lcom/facebook/media/upload/MediaUploadParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1309090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309091
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    .line 1309092
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    .line 1309093
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->c:Z

    .line 1309094
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->d:Ljava/lang/String;

    .line 1309095
    invoke-static {p1}, Lcom/facebook/media/upload/MediaUploadParameters;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->e:Ljava/util/List;

    .line 1309096
    invoke-static {p1}, Lcom/facebook/media/upload/MediaUploadParameters;->a(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->f:Ljava/util/List;

    .line 1309097
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1309098
    if-lez v0, :cond_1

    .line 1309099
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    .line 1309100
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    sget-object v1, Lcom/facebook/media/upload/MediaAttachementBody;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1309101
    :goto_1
    iput-object v2, p0, Lcom/facebook/media/upload/MediaUploadParameters;->h:LX/8Oo;

    .line 1309102
    return-void

    .line 1309103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1309104
    :cond_1
    iput-object v2, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;LX/8Oo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/upload/MediaAttachementBody;",
            ">;",
            "Lcom/facebook/media/upload/MediaUploadProgressListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1309044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309045
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309046
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Waterfall ID must be non empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309047
    :cond_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1309048
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target ID must be non empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309049
    :cond_1
    iput-object p1, p0, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    .line 1309050
    iput-object p2, p0, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    .line 1309051
    iput-boolean p3, p0, Lcom/facebook/media/upload/MediaUploadParameters;->c:Z

    .line 1309052
    iput-object p4, p0, Lcom/facebook/media/upload/MediaUploadParameters;->d:Ljava/lang/String;

    .line 1309053
    iput-object p5, p0, Lcom/facebook/media/upload/MediaUploadParameters;->e:Ljava/util/List;

    .line 1309054
    iput-object p6, p0, Lcom/facebook/media/upload/MediaUploadParameters;->f:Ljava/util/List;

    .line 1309055
    iput-object p7, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    .line 1309056
    iput-object p8, p0, Lcom/facebook/media/upload/MediaUploadParameters;->h:LX/8Oo;

    .line 1309057
    return-void
.end method

.method private static a(Landroid/os/Parcel;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1309080
    const/4 v0, 0x0

    .line 1309081
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1309082
    if-lez v2, :cond_0

    .line 1309083
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1309084
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1309085
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1309086
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1309087
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v5, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1309088
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1309089
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/List;Landroid/os/Parcel;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Landroid/os/Parcel;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1309071
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    .line 1309072
    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1309073
    :goto_1
    if-ge v2, v1, :cond_1

    .line 1309074
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 1309075
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309076
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309077
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v2

    .line 1309078
    goto :goto_0

    .line 1309079
    :cond_1
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1309070
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1309058
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309059
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309060
    iget-boolean v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1309061
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1309062
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->e:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/facebook/media/upload/MediaUploadParameters;->a(Ljava/util/List;Landroid/os/Parcel;)V

    .line 1309063
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->f:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/facebook/media/upload/MediaUploadParameters;->a(Ljava/util/List;Landroid/os/Parcel;)V

    .line 1309064
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 1309065
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1309066
    if-lez v1, :cond_1

    .line 1309067
    iget-object v0, p0, Lcom/facebook/media/upload/MediaUploadParameters;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1309068
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1309069
    goto :goto_0
.end method
