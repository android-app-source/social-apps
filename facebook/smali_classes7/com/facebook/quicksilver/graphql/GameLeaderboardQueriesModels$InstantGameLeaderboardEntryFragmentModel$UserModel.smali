.class public final Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6fede403
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349420
    const-class v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349436
    const-class v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1349437
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1349438
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1349443
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349444
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1349445
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1349446
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1349447
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1349448
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1349449
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1349450
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1349451
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349452
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1349439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349440
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349441
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1349442
    new-instance v0, LX/8Tu;

    invoke-direct {v0, p1}, LX/8Tu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349433
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1349434
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1349435
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1349432
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1349429
    new-instance v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;-><init>()V

    .line 1349430
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1349431
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1349428
    const v0, 0x52291734

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1349427
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349425
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->e:Ljava/lang/String;

    .line 1349426
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349423
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->f:Ljava/lang/String;

    .line 1349424
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349421
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->g:Ljava/lang/String;

    .line 1349422
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;->g:Ljava/lang/String;

    return-object v0
.end method
