.class public final Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1351404
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1351405
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1351406
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1351407
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1351408
    const/4 v2, 0x0

    .line 1351409
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1351410
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1351411
    :goto_0
    move v1, v2

    .line 1351412
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1351413
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1351414
    new-instance v1, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;-><init>()V

    .line 1351415
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1351416
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1351417
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1351418
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1351419
    :cond_0
    return-object v1

    .line 1351420
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1351421
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1351422
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1351423
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1351424
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1351425
    const-string v5, "instant_game_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1351426
    const/4 v4, 0x0

    .line 1351427
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_f

    .line 1351428
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1351429
    :goto_2
    move v3, v4

    .line 1351430
    goto :goto_1

    .line 1351431
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1351432
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1351433
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1351434
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1351435
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1351436
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1351437
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1351438
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_e

    .line 1351439
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1351440
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1351441
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v11, :cond_7

    .line 1351442
    const-string p0, "banner_image_uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1351443
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_3

    .line 1351444
    :cond_8
    const-string p0, "game_detailed_description"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1351445
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_3

    .line 1351446
    :cond_9
    const-string p0, "game_orientation"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1351447
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_3

    .line 1351448
    :cond_a
    const-string p0, "game_score_strategy"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 1351449
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_3

    .line 1351450
    :cond_b
    const-string p0, "game_uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1351451
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1351452
    :cond_c
    const-string p0, "icon_uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 1351453
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_3

    .line 1351454
    :cond_d
    const-string p0, "splash_uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1351455
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_3

    .line 1351456
    :cond_e
    const/4 v11, 0x7

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1351457
    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1351458
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1351459
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1351460
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1351461
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1351462
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1351463
    const/4 v4, 0x6

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1351464
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_f
    move v3, v4

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto/16 :goto_3
.end method
