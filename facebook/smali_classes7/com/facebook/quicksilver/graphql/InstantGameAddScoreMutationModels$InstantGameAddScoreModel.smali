.class public final Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47c31fc5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351044
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351045
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1351012
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1351013
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1351048
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1351049
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1351050
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351046
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->e:Ljava/lang/String;

    .line 1351047
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1351014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351015
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1351016
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1351017
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1351018
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1351019
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1351020
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1351021
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1351022
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1351023
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1351024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351025
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1351026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351027
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1351028
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    .line 1351029
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1351030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    .line 1351031
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->f:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    .line 1351032
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1351033
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    .line 1351034
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1351035
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    .line 1351036
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->g:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    .line 1351037
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1351038
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    .line 1351039
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1351040
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    .line 1351041
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->h:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    .line 1351042
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351043
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351005
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->f:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->f:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    .line 1351006
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->f:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1351001
    new-instance v0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;-><init>()V

    .line 1351002
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1351003
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1351004
    const v0, -0x36594d40    # -1365592.0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1351007
    const v0, 0x65c57489

    return v0
.end method

.method public final j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351008
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->g:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->g:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    .line 1351009
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->g:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351010
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->h:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->h:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    .line 1351011
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->h:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    return-object v0
.end method
