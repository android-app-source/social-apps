.class public final Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1351510
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1351511
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1351509
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1351466
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1351467
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1351468
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1351469
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1351470
    if-eqz v2, :cond_7

    .line 1351471
    const-string v3, "instant_game_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351472
    const/4 p2, 0x3

    const/4 p0, 0x2

    .line 1351473
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1351474
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1351475
    if-eqz v3, :cond_0

    .line 1351476
    const-string v4, "banner_image_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351477
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351478
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1351479
    if-eqz v3, :cond_1

    .line 1351480
    const-string v4, "game_detailed_description"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351481
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351482
    :cond_1
    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result v3

    .line 1351483
    if-eqz v3, :cond_2

    .line 1351484
    const-string v3, "game_orientation"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351485
    invoke-virtual {v1, v2, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351486
    :cond_2
    invoke-virtual {v1, v2, p2}, LX/15i;->g(II)I

    move-result v3

    .line 1351487
    if-eqz v3, :cond_3

    .line 1351488
    const-string v3, "game_score_strategy"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351489
    invoke-virtual {v1, v2, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351490
    :cond_3
    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1351491
    if-eqz v3, :cond_4

    .line 1351492
    const-string v4, "game_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351493
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351494
    :cond_4
    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1351495
    if-eqz v3, :cond_5

    .line 1351496
    const-string v4, "icon_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351497
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351498
    :cond_5
    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1351499
    if-eqz v3, :cond_6

    .line 1351500
    const-string v4, "splash_uri"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351501
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351502
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1351503
    :cond_7
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1351504
    if-eqz v2, :cond_8

    .line 1351505
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351506
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351507
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1351508
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1351465
    check-cast p1, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel$Serializer;->a(Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$QuicksilverGameInfoFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
