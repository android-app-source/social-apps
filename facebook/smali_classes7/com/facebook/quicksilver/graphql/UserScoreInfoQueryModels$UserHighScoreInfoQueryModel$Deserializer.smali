.class public final Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1352138
    const-class v0, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1352139
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1352200
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1352140
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1352141
    const/4 v2, 0x0

    .line 1352142
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 1352143
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352144
    :goto_0
    move v1, v2

    .line 1352145
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1352146
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1352147
    new-instance v1, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;-><init>()V

    .line 1352148
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1352149
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1352150
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1352151
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1352152
    :cond_0
    return-object v1

    .line 1352153
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352154
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 1352155
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1352156
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1352157
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1352158
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1352159
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1352160
    :cond_4
    const-string v5, "instant_game_high_scores"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1352161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1352162
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_5

    .line 1352163
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1352164
    const/4 v5, 0x0

    .line 1352165
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_b

    .line 1352166
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352167
    :goto_3
    move v4, v5

    .line 1352168
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1352169
    :cond_5
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1352170
    goto :goto_1

    .line 1352171
    :cond_6
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1352172
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1352173
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1352174
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1352175
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352176
    :cond_9
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_a

    .line 1352177
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1352178
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1352179
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_9

    if-eqz v6, :cond_9

    .line 1352180
    const-string v7, "best_score"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1352181
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1352182
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_10

    .line 1352183
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352184
    :goto_5
    move v4, v6

    .line 1352185
    goto :goto_4

    .line 1352186
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1352187
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1352188
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_3

    :cond_b
    move v4, v5

    goto :goto_4

    .line 1352189
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_e

    .line 1352190
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1352191
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1352192
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v9, :cond_c

    .line 1352193
    const-string p0, "score"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1352194
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v7

    goto :goto_6

    .line 1352195
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1352196
    :cond_e
    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1352197
    if-eqz v4, :cond_f

    .line 1352198
    invoke-virtual {v0, v6, v8, v6}, LX/186;->a(III)V

    .line 1352199
    :cond_f
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_10
    move v4, v6

    move v8, v6

    goto :goto_6
.end method
