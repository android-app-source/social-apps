.class public final Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4163a76c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1350779
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1350778
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1350776
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1350777
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1350770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1350771
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1350772
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1350773
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1350774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1350775
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1350762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1350763
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1350764
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    .line 1350765
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1350766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;

    .line 1350767
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    .line 1350768
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1350769
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1350751
    new-instance v0, LX/8UP;

    invoke-direct {v0, p1}, LX/8UP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1350760
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    .line 1350761
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel$InstantApplicationUserScopeModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1350758
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1350759
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1350757
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1350754
    new-instance v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$InstantApplicationUserScopeFragmentModel;-><init>()V

    .line 1350755
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1350756
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1350753
    const v0, -0x641e9b42

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1350752
    const v0, 0x285feb

    return v0
.end method
