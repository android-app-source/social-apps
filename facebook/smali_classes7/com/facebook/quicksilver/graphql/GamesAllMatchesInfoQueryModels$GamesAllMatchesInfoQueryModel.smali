.class public final Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xd1b21ed
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349947
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349914
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1349945
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1349946
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349942
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1349943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1349944
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1349934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349935
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1349936
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1349937
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1349938
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1349939
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1349940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349941
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInstantGameSuggestedMatches"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1349932
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->f:Ljava/util/List;

    .line 1349933
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1349924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349925
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1349926
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1349927
    if-eqz v1, :cond_0

    .line 1349928
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;

    .line 1349929
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;->f:Ljava/util/List;

    .line 1349930
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349931
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1349923
    new-instance v0, LX/8U5;

    invoke-direct {v0, p1}, LX/8U5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1349921
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1349922
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1349920
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1349917
    new-instance v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;-><init>()V

    .line 1349918
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1349919
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1349916
    const v0, -0x18a14630

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1349915
    const v0, 0x3c2b9d5

    return v0
.end method
