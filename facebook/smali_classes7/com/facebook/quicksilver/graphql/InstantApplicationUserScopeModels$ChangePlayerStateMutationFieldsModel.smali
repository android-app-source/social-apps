.class public final Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x68297ff5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1350527
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1350528
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1350529
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1350530
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1350531
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1350532
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1350533
    return-void
.end method

.method private a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1350534
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    .line 1350535
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1350536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1350537
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1350538
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1350539
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1350540
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1350541
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1350542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1350543
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1350544
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    .line 1350545
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->a()Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1350546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;

    .line 1350547
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;->e:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    .line 1350548
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1350549
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1350550
    new-instance v0, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;-><init>()V

    .line 1350551
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1350552
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1350553
    const v0, 0x7298927

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1350554
    const v0, 0x45b275ad

    return v0
.end method
