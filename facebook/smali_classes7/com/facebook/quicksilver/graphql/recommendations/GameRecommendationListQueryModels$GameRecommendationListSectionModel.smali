.class public final Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x17ee4e65
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352817
    const-class v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352845
    const-class v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1352843
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1352844
    return-void
.end method

.method private a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1352841
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    .line 1352842
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    return-object v0
.end method

.method private j()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1352839
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->g:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->g:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    .line 1352840
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->g:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1352846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352847
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1352848
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->j()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1352849
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1352850
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1352851
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1352852
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1352853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352854
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1352826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352827
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1352828
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    .line 1352829
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1352830
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;

    .line 1352831
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$GamesModel;

    .line 1352832
    :cond_0
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->j()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1352833
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->j()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    .line 1352834
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->j()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1352835
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;

    .line 1352836
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->g:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$TitleModel;

    .line 1352837
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352838
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1352823
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1352824
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;->f:Z

    .line 1352825
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1352820
    new-instance v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;-><init>()V

    .line 1352821
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1352822
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1352819
    const v0, -0x70b2e6b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1352818
    const v0, -0xba78beb

    return v0
.end method
