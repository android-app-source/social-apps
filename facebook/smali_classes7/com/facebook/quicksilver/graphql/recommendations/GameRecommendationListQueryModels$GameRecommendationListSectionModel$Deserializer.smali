.class public final Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1352674
    const-class v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1352675
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1352676
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1352677
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1352678
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1352679
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1352680
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1352681
    :goto_0
    move v1, v2

    .line 1352682
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1352683
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1352684
    new-instance v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListSectionModel;-><init>()V

    .line 1352685
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1352686
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1352687
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1352688
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1352689
    :cond_0
    return-object v1

    .line 1352690
    :cond_1
    const-string p0, "is_badged"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1352691
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    .line 1352692
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 1352693
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1352694
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1352695
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 1352696
    const-string p0, "games"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1352697
    invoke-static {p1, v0}, LX/8VQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1352698
    :cond_3
    const-string p0, "title"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1352699
    invoke-static {p1, v0}, LX/8VR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1352700
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1352701
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1352702
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1352703
    if-eqz v1, :cond_6

    .line 1352704
    invoke-virtual {v0, v3, v5}, LX/186;->a(IZ)V

    .line 1352705
    :cond_6
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1352706
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
