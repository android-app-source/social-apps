.class public final Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e009dc7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352673
    const-class v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352672
    const-class v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1352670
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1352671
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1352664
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352665
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1352666
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1352667
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1352668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352669
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1352649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352650
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1352651
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    .line 1352652
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1352653
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;

    .line 1352654
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    .line 1352655
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352656
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1352662
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    .line 1352663
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;->e:Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel$ItemsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1352659
    new-instance v0, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/recommendations/GameRecommendationListQueryModels$GameRecommendationListQueryModel;-><init>()V

    .line 1352660
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1352661
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1352658
    const v0, -0x7b4b0972

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1352657
    const v0, -0x1c541805

    return v0
.end method
