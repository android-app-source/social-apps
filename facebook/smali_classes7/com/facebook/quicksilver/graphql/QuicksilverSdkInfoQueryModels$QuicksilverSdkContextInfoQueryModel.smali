.class public final Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x48af6044
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351903
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351902
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1351879
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1351880
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351900
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->f:Ljava/lang/String;

    .line 1351901
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1351892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351893
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1351894
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1351895
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1351896
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1351897
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1351898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351899
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1351889
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351890
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351891
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351888
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1351885
    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;-><init>()V

    .line 1351886
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1351887
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1351884
    const v0, -0xc1c2b5b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1351883
    const v0, 0x6d45dbf9

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351881
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->e:Ljava/lang/String;

    .line 1351882
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method
