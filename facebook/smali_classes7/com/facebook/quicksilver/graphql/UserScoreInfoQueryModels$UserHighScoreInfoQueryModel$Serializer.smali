.class public final Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1352203
    const-class v0, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1352204
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1352205
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1352206
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1352207
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 1352208
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1352209
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1352210
    if-eqz v2, :cond_0

    .line 1352211
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352212
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1352213
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1352214
    if-eqz v2, :cond_4

    .line 1352215
    const-string v3, "instant_game_high_scores"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352216
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1352217
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 1352218
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    .line 1352219
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1352220
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1352221
    if-eqz p0, :cond_2

    .line 1352222
    const-string v0, "best_score"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352223
    const/4 v0, 0x0

    .line 1352224
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1352225
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1352226
    if-eqz v0, :cond_1

    .line 1352227
    const-string v4, "score"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1352228
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1352229
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1352230
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1352231
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1352232
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1352233
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1352234
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1352235
    check-cast p1, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel$Serializer;->a(Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
