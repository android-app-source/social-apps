.class public final Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1351877
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1351878
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1351862
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1351864
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1351865
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1351866
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1351867
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1351868
    if-eqz p0, :cond_0

    .line 1351869
    const-string p2, "context_token_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351870
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351871
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1351872
    if-eqz p0, :cond_1

    .line 1351873
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1351874
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1351875
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1351876
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1351863
    check-cast p1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel$Serializer;->a(Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkContextInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
