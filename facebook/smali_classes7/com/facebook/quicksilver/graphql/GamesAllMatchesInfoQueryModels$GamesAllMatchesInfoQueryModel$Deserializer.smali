.class public final Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1349734
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1349735
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1349768
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1349736
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1349737
    const/4 v2, 0x0

    .line 1349738
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 1349739
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1349740
    :goto_0
    move v1, v2

    .line 1349741
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1349742
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1349743
    new-instance v1, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel;-><init>()V

    .line 1349744
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1349745
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1349746
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1349747
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1349748
    :cond_0
    return-object v1

    .line 1349749
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1349750
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_6

    .line 1349751
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1349752
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1349753
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 1349754
    const-string p0, "__type__"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1349755
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1349756
    :cond_4
    const-string p0, "instant_game_suggested_matches"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1349757
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1349758
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_5

    .line 1349759
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_5

    .line 1349760
    invoke-static {p1, v0}, LX/8U8;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1349761
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1349762
    :cond_5
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1349763
    goto :goto_1

    .line 1349764
    :cond_6
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1349765
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1349766
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1349767
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
