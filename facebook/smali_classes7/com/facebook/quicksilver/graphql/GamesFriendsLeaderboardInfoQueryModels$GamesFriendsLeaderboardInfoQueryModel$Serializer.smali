.class public final Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1350273
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1350274
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1350275
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1350276
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1350277
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1350278
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1350279
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1350280
    if-eqz v2, :cond_0

    .line 1350281
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350282
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1350283
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1350284
    if-eqz v2, :cond_1

    .line 1350285
    const-string p0, "friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350286
    invoke-static {v1, v2, p1, p2}, LX/8UI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1350287
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1350288
    if-eqz v2, :cond_2

    .line 1350289
    const-string p0, "instant_game_friend_leaderboard_entries"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350290
    invoke-static {v1, v2, p1, p2}, LX/8Tw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1350291
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1350292
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1350293
    check-cast p1, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel$Serializer;->a(Lcom/facebook/quicksilver/graphql/GamesFriendsLeaderboardInfoQueryModels$GamesFriendsLeaderboardInfoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
