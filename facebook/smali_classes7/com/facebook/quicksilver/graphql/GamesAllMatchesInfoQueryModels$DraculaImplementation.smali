.class public final Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1349723
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1349724
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1349725
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1349726
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1349695
    if-nez p1, :cond_0

    .line 1349696
    :goto_0
    return v0

    .line 1349697
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1349698
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1349699
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1349700
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1349701
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1349702
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1349703
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7b25a9c
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1349727
    new-instance v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1349729
    packed-switch p0, :pswitch_data_0

    .line 1349730
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1349731
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x7b25a9c
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1349728
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1349732
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->b(I)V

    .line 1349733
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1349717
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1349718
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1349719
    :cond_0
    iput-object p1, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1349720
    iput p2, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->b:I

    .line 1349721
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1349722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1349716
    new-instance v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1349713
    iget v0, p0, LX/1vt;->c:I

    .line 1349714
    move v0, v0

    .line 1349715
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1349710
    iget v0, p0, LX/1vt;->c:I

    .line 1349711
    move v0, v0

    .line 1349712
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1349707
    iget v0, p0, LX/1vt;->b:I

    .line 1349708
    move v0, v0

    .line 1349709
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349704
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1349705
    move-object v0, v0

    .line 1349706
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1349686
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1349687
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1349688
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1349689
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1349690
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1349691
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1349692
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1349693
    invoke-static {v3, v9, v2}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1349694
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1349683
    iget v0, p0, LX/1vt;->c:I

    .line 1349684
    move v0, v0

    .line 1349685
    return v0
.end method
