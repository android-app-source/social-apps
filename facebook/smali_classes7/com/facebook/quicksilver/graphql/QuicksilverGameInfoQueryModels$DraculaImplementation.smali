.class public final Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1351397
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1351398
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1351395
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1351396
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 1351367
    if-nez p1, :cond_0

    .line 1351368
    const/4 v0, 0x0

    .line 1351369
    :goto_0
    return v0

    .line 1351370
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1351371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1351372
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1351373
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1351374
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1351375
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1351376
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v2

    .line 1351377
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1351378
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    move-result-object v3

    .line 1351379
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1351380
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1351381
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1351382
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1351383
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1351384
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1351385
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1351386
    const/4 v7, 0x7

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1351387
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 1351388
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1351389
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1351390
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1351391
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1351392
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1351393
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1351394
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x566e262b
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1351366
    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1351363
    packed-switch p0, :pswitch_data_0

    .line 1351364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1351365
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x566e262b
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1351362
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1351360
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->b(I)V

    .line 1351361
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1351399
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1351400
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1351401
    :cond_0
    iput-object p1, p0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1351402
    iput p2, p0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->b:I

    .line 1351403
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1351359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1351358
    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1351355
    iget v0, p0, LX/1vt;->c:I

    .line 1351356
    move v0, v0

    .line 1351357
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1351352
    iget v0, p0, LX/1vt;->c:I

    .line 1351353
    move v0, v0

    .line 1351354
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1351334
    iget v0, p0, LX/1vt;->b:I

    .line 1351335
    move v0, v0

    .line 1351336
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351349
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1351350
    move-object v0, v0

    .line 1351351
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1351340
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1351341
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1351342
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1351343
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1351344
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1351345
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1351346
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1351347
    invoke-static {v3, v9, v2}, Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/quicksilver/graphql/QuicksilverGameInfoQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1351348
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1351337
    iget v0, p0, LX/1vt;->c:I

    .line 1351338
    move v0, v0

    .line 1351339
    return v0
.end method
