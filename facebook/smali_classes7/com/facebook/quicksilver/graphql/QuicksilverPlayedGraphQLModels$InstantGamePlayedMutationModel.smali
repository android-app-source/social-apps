.class public final Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63da7779
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351749
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1351748
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1351746
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1351747
    return-void
.end method

.method private a()Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351744
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->e:Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->e:Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    .line 1351745
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->e:Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1351738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351739
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->a()Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1351740
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1351741
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1351742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351743
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1351730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1351731
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->a()Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1351732
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->a()Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    .line 1351733
    invoke-direct {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->a()Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1351734
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;

    .line 1351735
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;->e:Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel$ApplicationModel;

    .line 1351736
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1351737
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1351727
    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/QuicksilverPlayedGraphQLModels$InstantGamePlayedMutationModel;-><init>()V

    .line 1351728
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1351729
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1351726
    const v0, -0x27e50102

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1351725
    const v0, -0x7a7fbcd9

    return v0
.end method
