.class public final Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6d232ebe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352022
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1352021
    const-class v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1352019
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1352020
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1352011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352012
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1352013
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1352014
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1352015
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1352016
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1352017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352018
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1352003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1352004
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1352005
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    .line 1352006
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1352007
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;

    .line 1352008
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->f:Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    .line 1352009
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1352010
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1352000
    new-instance v0, LX/8V8;

    invoke-direct {v0, p1}, LX/8V8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1352023
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->e:Ljava/lang/String;

    .line 1352024
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1352001
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1352002
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1351999
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1351996
    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;-><init>()V

    .line 1351997
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1351998
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1351995
    const v0, 0x7e89a10d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1351994
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1351992
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->f:Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->f:Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    .line 1351993
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel;->f:Lcom/facebook/quicksilver/graphql/QuicksilverSdkInfoQueryModels$QuicksilverSdkPlayerFragmentModel$ProfilePictureModel;

    return-object v0
.end method
