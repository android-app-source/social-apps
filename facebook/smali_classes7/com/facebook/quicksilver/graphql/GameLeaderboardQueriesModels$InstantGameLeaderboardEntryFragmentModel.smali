.class public final Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x51c0c3d2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349495
    const-class v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349494
    const-class v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1349492
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1349493
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1349481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349482
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1349483
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1349484
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1349485
    iget-boolean v2, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->e:Z

    invoke-virtual {p1, v4, v2}, LX/186;->a(IZ)V

    .line 1349486
    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->f:I

    invoke-virtual {p1, v2, v3, v4}, LX/186;->a(III)V

    .line 1349487
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1349488
    const/4 v0, 0x3

    iget v2, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->h:I

    invoke-virtual {p1, v0, v2, v4}, LX/186;->a(III)V

    .line 1349489
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1349490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349491
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1349473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349474
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1349475
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    .line 1349476
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1349477
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    .line 1349478
    iput-object v0, v1, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->i:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    .line 1349479
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349480
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1349468
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1349469
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->e:Z

    .line 1349470
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->f:I

    .line 1349471
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->h:I

    .line 1349472
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1349453
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1349454
    iget-boolean v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1349465
    new-instance v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;-><init>()V

    .line 1349466
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1349467
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1349464
    const v0, -0x24f10ed9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1349463
    const v0, -0x10aa3598

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1349461
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1349462
    iget v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->f:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349459
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->g:Ljava/lang/String;

    .line 1349460
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1349457
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1349458
    iget v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->h:I

    return v0
.end method

.method public final m()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349455
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->i:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->i:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    .line 1349456
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;->i:Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel$UserModel;

    return-object v0
.end method
