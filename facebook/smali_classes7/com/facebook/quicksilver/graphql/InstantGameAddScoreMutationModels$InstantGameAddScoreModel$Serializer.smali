.class public final Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1350976
    const-class v0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    new-instance v1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1350977
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1350978
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1350979
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1350980
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1350981
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1350982
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1350983
    if-eqz v2, :cond_0

    .line 1350984
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350985
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1350986
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1350987
    if-eqz v2, :cond_1

    .line 1350988
    const-string p0, "current_score"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350989
    invoke-static {v1, v2, p1, p2}, LX/8U0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1350990
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1350991
    if-eqz v2, :cond_2

    .line 1350992
    const-string p0, "friend_leaderboard"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350993
    invoke-static {v1, v2, p1, p2}, LX/8Tw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1350994
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1350995
    if-eqz v2, :cond_3

    .line 1350996
    const-string p0, "leaderboard"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1350997
    invoke-static {v1, v2, p1, p2}, LX/8Ty;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1350998
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1350999
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1351000
    check-cast p1, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel$Serializer;->a(Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;LX/0nX;LX/0my;)V

    return-void
.end method
