.class public final Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:I

.field public final synthetic d:Ljava/util/List;

.field public final synthetic e:LX/8Uv;

.field public final synthetic f:Ljava/util/Map;

.field public final synthetic g:LX/8Uw;


# direct methods
.method public constructor <init>(LX/8Uw;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;LX/8Uv;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1351602
    iput-object p1, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->g:LX/8Uw;

    iput-object p2, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->b:Ljava/lang/String;

    iput p4, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->c:I

    iput-object p5, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->d:Ljava/util/List;

    iput-object p6, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->e:LX/8Uv;

    iput-object p7, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->f:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1351578
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->g:LX/8Uw;

    iget-object v0, v0, LX/8Uw;->b:LX/8Vo;

    iget-object v1, p0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->a:Ljava/lang/String;

    new-instance v2, LX/8Ut;

    invoke-direct {v2, p0}, LX/8Ut;-><init>(Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;)V

    .line 1351579
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1351580
    sget-object v3, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_HANDLE:LX/8TE;

    iget-object v3, v3, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v5, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351581
    if-nez v2, :cond_0

    .line 1351582
    :goto_0
    return-void

    .line 1351583
    :cond_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1351584
    invoke-virtual {v2}, LX/8Ut;->a()V

    goto :goto_0

    .line 1351585
    :cond_1
    :try_start_0
    iget-object v3, v0, LX/8Vo;->d:LX/8Vg;

    iget-object v4, v0, LX/8Vo;->b:Landroid/content/Context;

    invoke-virtual {v3, v4, v1}, LX/8Vg;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 1351586
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1351587
    :cond_2
    invoke-virtual {v2}, LX/8Ut;->a()V
    :try_end_0
    .catch LX/7zB; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1351588
    :catch_0
    move-exception v3

    move-object v4, v3

    .line 1351589
    iget-object v3, v0, LX/8Vo;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8TD;

    sget-object v6, LX/8TE;->SCREENSHOT_UPLOAD:LX/8TE;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v4, v5}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1351590
    invoke-virtual {v2}, LX/8Ut;->a()V

    goto :goto_0

    .line 1351591
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1351592
    const/16 v6, 0x2e

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1351593
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1351594
    new-instance v6, LX/7yy;

    invoke-direct {v6, v3, v4}, LX/7yy;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1351595
    iget-object v3, v0, LX/8Vo;->a:LX/7zS;

    .line 1351596
    iget-object v4, v3, LX/7zS;->a:LX/7z2;

    move-object v3, v4

    .line 1351597
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1351598
    const-string v7, "image_type"

    const-string v8, "FILE_ATTACHMENT"

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351599
    const-string v7, "use_ent_photo"

    const-string v8, "1"

    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351600
    new-instance v7, LX/7yw;

    sget-object v8, LX/7yt;->MESSENGER:LX/7yt;

    new-instance p0, LX/7yu;

    invoke-direct {p0}, LX/7yu;-><init>()V

    invoke-direct {v7, v8, v4, p0}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;)V

    .line 1351601
    new-instance v4, LX/8Vn;

    invoke-direct {v4, v0, v5, v2}, LX/8Vn;-><init>(LX/8Vo;Ljava/util/Map;LX/8Ut;)V

    invoke-virtual {v3, v6, v7, v4}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;
    :try_end_1
    .catch LX/7zB; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
