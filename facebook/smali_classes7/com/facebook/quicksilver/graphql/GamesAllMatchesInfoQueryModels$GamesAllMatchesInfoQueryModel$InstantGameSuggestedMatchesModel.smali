.class public final Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6de43ab3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349890
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1349889
    const-class v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1349887
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1349888
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1349836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349837
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1349838
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1349839
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->l()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1349840
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x7b25a9c

    invoke-static {v4, v3, v5}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1349841
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1349842
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1349843
    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->e:Z

    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 1349844
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1349845
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1349846
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1349847
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1349848
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1349849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1349872
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1349873
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1349874
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1349875
    if-eqz v1, :cond_2

    .line 1349876
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;

    .line 1349877
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 1349878
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1349879
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7b25a9c

    invoke-static {v2, v0, v3}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1349880
    invoke-virtual {p0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1349881
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;

    .line 1349882
    iput v3, v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->i:I

    move-object v1, v0

    .line 1349883
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1349884
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 1349885
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 1349886
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1349868
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1349869
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->e:Z

    .line 1349870
    const/4 v0, 0x4

    const v1, 0x7b25a9c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->i:I

    .line 1349871
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1349866
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1349867
    iget-boolean v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1349863
    new-instance v0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;

    invoke-direct {v0}, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;-><init>()V

    .line 1349864
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1349865
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1349862
    const v0, -0x5070868c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1349861
    const v0, -0x6f80f049

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349859
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->f:Ljava/lang/String;

    .line 1349860
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349857
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->g:Ljava/lang/String;

    .line 1349858
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1349855
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel$ParticipantsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->h:Ljava/util/List;

    .line 1349856
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadKey"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349853
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1349854
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1349851
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->j:Ljava/lang/String;

    .line 1349852
    iget-object v0, p0, Lcom/facebook/quicksilver/graphql/GamesAllMatchesInfoQueryModels$GamesAllMatchesInfoQueryModel$InstantGameSuggestedMatchesModel;->j:Ljava/lang/String;

    return-object v0
.end method
