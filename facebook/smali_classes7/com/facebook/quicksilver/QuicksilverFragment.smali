.class public Lcom/facebook/quicksilver/QuicksilverFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T7;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public a:LX/8VW;

.field public b:LX/8VV;

.field public c:LX/8W7;

.field private d:LX/8Wo;

.field public e:Z

.field public f:LX/8Su;

.field public g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:LX/8Su;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:Lorg/json/JSONObject;

.field private o:J

.field public p:Z

.field private q:LX/8Sg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/8Sy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:LX/8TY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/8Ul;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:LX/8V7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/0W9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:LX/8UX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private z:LX/8Tt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1347814
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1347815
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347816
    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    .line 1347817
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1347818
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1347819
    check-cast p2, Landroid/view/ViewGroup;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p2, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1347820
    :goto_0
    return-void

    .line 1347821
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v1, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    const-string v2, "Cannot add View to Fragment.  Inflated View is not a ViewGroup."

    invoke-virtual {v0, v1, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/quicksilver/QuicksilverFragment;LX/8Sg;LX/8Sy;LX/8TS;LX/8TY;LX/8TD;LX/8Ul;LX/8V7;LX/0W9;LX/8UX;LX/8Tt;LX/0Ot;LX/0So;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/QuicksilverFragment;",
            "LX/8Sg;",
            "LX/8Sy;",
            "LX/8TS;",
            "LX/8TY;",
            "LX/8TD;",
            "LX/8Ul;",
            "LX/8V7;",
            "LX/0W9;",
            "LX/8UX;",
            "LX/8Tt;",
            "LX/0Ot",
            "<",
            "LX/8T7;",
            ">;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1347822
    iput-object p1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->q:LX/8Sg;

    iput-object p2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->r:LX/8Sy;

    iput-object p3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    iput-object p4, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->t:LX/8TY;

    iput-object p5, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    iput-object p6, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    iput-object p7, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->w:LX/8V7;

    iput-object p8, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->x:LX/0W9;

    iput-object p9, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->y:LX/8UX;

    iput-object p10, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->z:LX/8Tt;

    iput-object p11, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->B:LX/0So;

    return-void
.end method

.method public static a(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1347823
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1347824
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347825
    iget-object v2, v1, LX/8TS;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1347826
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    invoke-virtual {v2}, LX/8TS;->c()LX/8Vb;

    move-result-object v2

    iget-object v2, v2, LX/8Vb;->e:Ljava/lang/String;

    .line 1347827
    :try_start_0
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->j:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1347828
    const-string v3, "locale"

    iget-object v4, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347829
    :cond_0
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->h:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1347830
    const-string v3, "playerID"

    iget-object v4, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347831
    :cond_1
    if-eqz v1, :cond_2

    .line 1347832
    const-string v3, "contextID"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347833
    :cond_2
    if-eqz v2, :cond_3

    .line 1347834
    const-string v1, "playerName"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347835
    :cond_3
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1347836
    const-string v1, "playerPhoto"

    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347837
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1347838
    :goto_0
    return-void

    .line 1347839
    :catch_0
    move-exception v0

    .line 1347840
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v3, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1347841
    const-string v0, "Internal error while trying to resolve the promise for sdk initialization."

    invoke-static {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v12}, LX/8Sg;->a(LX/0QB;)LX/8Sg;

    move-result-object v1

    check-cast v1, LX/8Sg;

    invoke-static {v12}, LX/8Sy;->a(LX/0QB;)LX/8Sy;

    move-result-object v2

    check-cast v2, LX/8Sy;

    invoke-static {v12}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v3

    check-cast v3, LX/8TS;

    invoke-static {v12}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v4

    check-cast v4, LX/8TY;

    invoke-static {v12}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v5

    check-cast v5, LX/8TD;

    invoke-static {v12}, LX/8Ul;->a(LX/0QB;)LX/8Ul;

    move-result-object v6

    check-cast v6, LX/8Ul;

    invoke-static {v12}, LX/8V7;->b(LX/0QB;)LX/8V7;

    move-result-object v7

    check-cast v7, LX/8V7;

    invoke-static {v12}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    new-instance v11, LX/8UX;

    invoke-static {v12}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v12}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-direct {v11, v9, v10}, LX/8UX;-><init>(LX/0tX;LX/1Ck;)V

    move-object v9, v11

    check-cast v9, LX/8UX;

    new-instance p0, LX/8Tt;

    invoke-static {v12}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v12}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v10, v11}, LX/8Tt;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    move-object v10, p0

    check-cast v10, LX/8Tt;

    const/16 v11, 0x306e

    invoke-static {v12, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v12}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v12

    check-cast v12, LX/0So;

    invoke-static/range {v0 .. v12}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Lcom/facebook/quicksilver/QuicksilverFragment;LX/8Sg;LX/8Sy;LX/8TS;LX/8TY;LX/8TD;LX/8Ul;LX/8V7;LX/0W9;LX/8UX;LX/8Tt;LX/0Ot;LX/0So;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1347842
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1347843
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1347844
    const-string v2, "message"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347845
    const-string v2, "promiseID"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347846
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347847
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    sget-object v2, LX/8W5;->REJECT_PROMISE:LX/8W5;

    invoke-virtual {v1, v2, v0}, LX/8W7;->a(LX/8W5;Ljava/lang/Object;)V

    .line 1347848
    :goto_0
    return-void

    .line 1347849
    :catch_0
    move-exception v0

    .line 1347850
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v3, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1347851
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1347852
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1347853
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1347854
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1347855
    :catch_0
    move-exception v0

    .line 1347856
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v3, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1347857
    const-string v0, "Internal error while trying to resolve the promise for getting player data."

    invoke-static {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347858
    :goto_1
    return-void

    .line 1347859
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/facebook/quicksilver/QuicksilverFragment;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 1347894
    :try_start_0
    invoke-virtual {p2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347895
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1347896
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1347897
    :catch_0
    move-exception v0

    .line 1347898
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v3, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1347899
    const-string v0, "Internal error while trying to resolve the promise for setting player data."

    invoke-static {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347900
    :goto_1
    return-void

    .line 1347901
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1
.end method

.method public static b(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1347860
    const-string v0, "Failed to fetch required information for sdk initialization."

    invoke-static {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347861
    return-void
.end method

.method private b(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 1347862
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1347863
    :try_start_0
    const-string v1, "promiseID"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347864
    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347865
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    sget-object v2, LX/8W5;->RESOLVE_PROMISE:LX/8W5;

    invoke-virtual {v1, v2, v0}, LX/8W7;->a(LX/8W5;Ljava/lang/Object;)V

    .line 1347866
    :goto_0
    return-void

    .line 1347867
    :catch_0
    move-exception v0

    .line 1347868
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v2, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v3, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v1, v2, v3, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1347869
    const-string v0, "Internal error while trying to resolve the promise."

    invoke-static {p0, p1, v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1347870
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1347871
    if-nez v0, :cond_0

    .line 1347872
    :goto_0
    return-void

    .line 1347873
    :cond_0
    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$12;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverFragment$12;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 3

    .prologue
    .line 1347874
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    check-cast v0, Landroid/view/View;

    .line 1347875
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1347876
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8T7;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    .line 1347877
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    sget-object v1, LX/8TV;->START_SCREEN:LX/8TV;

    invoke-virtual {v0, v1}, LX/8TS;->a(LX/8TV;)V

    .line 1347878
    return-void
.end method

.method public static k$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 3

    .prologue
    .line 1347879
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T7;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8T7;->f(Landroid/view/View;LX/8Sl;)V

    .line 1347880
    return-void
.end method

.method public static l(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 3

    .prologue
    .line 1347881
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T7;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    .line 1347882
    return-void
.end method

.method public static m(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 3

    .prologue
    .line 1347883
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T7;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8T7;->f(Landroid/view/View;LX/8Sl;)V

    .line 1347884
    return-void
.end method

.method public static n(Lcom/facebook/quicksilver/QuicksilverFragment;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 1347790
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1347791
    const/4 v2, 0x0

    .line 1347792
    :try_start_0
    const-string v1, "solo"

    .line 1347793
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347794
    iget-object v4, v3, LX/8TS;->k:Ljava/lang/String;

    move-object v3, v4

    .line 1347795
    if-eqz v3, :cond_0

    .line 1347796
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347797
    iget-object v3, v2, LX/8TS;->k:Ljava/lang/String;

    move-object v2, v3

    .line 1347798
    :cond_0
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347799
    iget-object v4, v3, LX/8TS;->f:LX/8Tb;

    move-object v3, v4

    .line 1347800
    if-eqz v3, :cond_1

    .line 1347801
    iget-object v4, v3, LX/8Tb;->c:LX/8Ta;

    move-object v4, v4

    .line 1347802
    if-eqz v4, :cond_1

    .line 1347803
    sget-object v1, LX/8Si;->a:[I

    .line 1347804
    iget-object v4, v3, LX/8Tb;->c:LX/8Ta;

    move-object v3, v4

    .line 1347805
    invoke-virtual {v3}, LX/8Ta;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 1347806
    const-string v1, "solo"

    .line 1347807
    :cond_1
    :goto_0
    const-string v3, "contextID"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347808
    const-string v2, "contextType"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1347809
    :goto_1
    return-object v0

    .line 1347810
    :pswitch_0
    const-string v1, "thread"

    goto :goto_0

    .line 1347811
    :pswitch_1
    const-string v1, "post"

    goto :goto_0

    .line 1347812
    :pswitch_2
    const-string v1, "group"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1347813
    :catch_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1347885
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347886
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1347887
    iget-object v1, v0, LX/8TO;->d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-object v0, v1

    .line 1347888
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->PORTRAIT:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-ne v0, v1, :cond_0

    .line 1347889
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 1347890
    :goto_0
    return-void

    .line 1347891
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->LANDSCAPE:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    if-ne v0, v1, :cond_1

    .line 1347892
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1347893
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 1347553
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->g:Ljava/util/Set;

    .line 1347554
    sget-object v0, LX/8Su;->NOT_STARTED:LX/8Su;

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    .line 1347555
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->l:Ljava/util/List;

    .line 1347556
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->m:Ljava/util/List;

    .line 1347557
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->q()V

    .line 1347558
    return-void
.end method

.method private q()V
    .locals 5

    .prologue
    .line 1347593
    sget-object v0, LX/8Su;->WAITING:LX/8Su;

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->f:LX/8Su;

    .line 1347594
    new-instance v0, LX/8Ss;

    invoke-direct {v0, p0}, LX/8Ss;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347595
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->w:LX/8V7;

    .line 1347596
    iget-object v2, v1, LX/8V7;->c:LX/8TS;

    .line 1347597
    iget-object v3, v2, LX/8TS;->e:LX/8TO;

    move-object v2, v3

    .line 1347598
    iget-object v3, v2, LX/8TO;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1347599
    new-instance v3, LX/8V3;

    invoke-direct {v3}, LX/8V3;-><init>()V

    move-object v3, v3

    .line 1347600
    const-string v4, "game"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1347601
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1347602
    iget-object v3, v1, LX/8V7;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1347603
    new-instance v3, LX/8V5;

    invoke-direct {v3, v1, v0}, LX/8V5;-><init>(LX/8V7;LX/8Ss;)V

    .line 1347604
    iget-object v4, v1, LX/8V7;->b:LX/1Ck;

    const-string p0, "quicksilver_sdk_player_info_query"

    invoke-virtual {v4, p0, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1347605
    return-void
.end method

.method public static r$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 7

    .prologue
    .line 1347606
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    sget-object v1, LX/8Su;->NOT_STARTED:LX/8Su;

    if-eq v0, v1, :cond_0

    .line 1347607
    :goto_0
    return-void

    .line 1347608
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347609
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1347610
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1347611
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->y:LX/8UX;

    new-instance v2, LX/8St;

    invoke-direct {v2, p0}, LX/8St;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347612
    new-instance v3, LX/8UL;

    invoke-direct {v3}, LX/8UL;-><init>()V

    move-object v3, v3

    .line 1347613
    const-string v4, "game"

    invoke-virtual {v3, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1347614
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1347615
    iget-object v4, v1, LX/8UX;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1347616
    new-instance v4, LX/8UW;

    invoke-direct {v4, v1, v2}, LX/8UW;-><init>(LX/8UX;LX/8St;)V

    .line 1347617
    iget-object v5, v1, LX/8UX;->b:LX/1Ck;

    const-string v6, "instant_application_user_scope_query"

    invoke-virtual {v5, v6, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1347618
    sget-object v0, LX/8Su;->WAITING:LX/8Su;

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    goto :goto_0
.end method

.method public static s(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 14

    .prologue
    .line 1347559
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 1347560
    :goto_0
    return-void

    .line 1347561
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347562
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1347563
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1347564
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->z:LX/8Tt;

    new-instance v2, LX/8Sh;

    invoke-direct {v2, p0}, LX/8Sh;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->n:Lorg/json/JSONObject;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1347565
    new-instance v4, LX/8UK;

    invoke-direct {v4}, LX/8UK;-><init>()V

    move-object v4, v4

    .line 1347566
    new-instance v5, LX/4DH;

    invoke-direct {v5}, LX/4DH;-><init>()V

    .line 1347567
    const-string v6, "app_id"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347568
    const-string v6, "player_state"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347569
    const-string v6, "data"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1347570
    new-instance v5, LX/8UN;

    invoke-direct {v5}, LX/8UN;-><init>()V

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1347571
    new-instance v8, LX/186;

    const/16 v9, 0x80

    invoke-direct {v8, v9}, LX/186;-><init>(I)V

    .line 1347572
    iget-object v9, v5, LX/8UN;->a:Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel$InstantApplicationUserScopeModel;

    invoke-static {v8, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1347573
    invoke-virtual {v8, v12}, LX/186;->c(I)V

    .line 1347574
    invoke-virtual {v8, v11, v9}, LX/186;->b(II)V

    .line 1347575
    invoke-virtual {v8}, LX/186;->d()I

    move-result v9

    .line 1347576
    invoke-virtual {v8, v9}, LX/186;->d(I)V

    .line 1347577
    invoke-virtual {v8}, LX/186;->e()[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 1347578
    invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1347579
    new-instance v8, LX/15i;

    move-object v11, v10

    move-object v13, v10

    invoke-direct/range {v8 .. v13}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1347580
    new-instance v9, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;

    invoke-direct {v9, v8}, Lcom/facebook/quicksilver/graphql/InstantApplicationUserScopeModels$ChangePlayerStateMutationFieldsModel;-><init>(LX/15i;)V

    .line 1347581
    move-object v5, v9

    .line 1347582
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v4

    .line 1347583
    new-instance v5, LX/3G1;

    invoke-direct {v5}, LX/3G1;-><init>()V

    invoke-virtual {v5, v4}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 1347584
    iput-wide v6, v4, LX/3G2;->d:J

    .line 1347585
    move-object v4, v4

    .line 1347586
    const/16 v5, 0x64

    .line 1347587
    iput v5, v4, LX/3G2;->f:I

    .line 1347588
    move-object v4, v4

    .line 1347589
    invoke-virtual {v4}, LX/3G2;->a()LX/3G3;

    move-result-object v4

    check-cast v4, LX/3G4;

    .line 1347590
    iget-object v5, v1, LX/8Tt;->a:LX/0tX;

    sget-object v6, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v5, v4, v6}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1347591
    new-instance v5, LX/8Ts;

    invoke-direct {v5, v1, v2}, LX/8Ts;-><init>(LX/8Tt;LX/8Sh;)V

    iget-object v6, v1, LX/8Tt;->b:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1347592
    goto/16 :goto_0
.end method

.method public static t(Lcom/facebook/quicksilver/QuicksilverFragment;)V
    .locals 2

    .prologue
    .line 1347619
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1347620
    if-nez v0, :cond_0

    .line 1347621
    :goto_0
    return-void

    .line 1347622
    :cond_0
    new-instance v1, Lcom/facebook/quicksilver/QuicksilverFragment$11;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverFragment$11;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1347623
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f082362

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1347624
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1347625
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347626
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1347627
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347628
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347629
    iget-object v1, v0, LX/8TS;->f:LX/8Tb;

    move-object v0, v1

    .line 1347630
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347631
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->o()V

    .line 1347632
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->p()V

    .line 1347633
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347634
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1347635
    iget-object v2, v1, LX/8TO;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1347636
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1347637
    const-string v3, "Referer"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347638
    invoke-virtual {v0, v1, v2}, LX/8W7;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 1347639
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    .line 1347640
    invoke-static {v0}, LX/8Ul;->f(LX/8Ul;)Ljava/lang/String;

    move-result-object v1

    .line 1347641
    new-instance v2, LX/4GW;

    invoke-direct {v2}, LX/4GW;-><init>()V

    .line 1347642
    const-string v3, "app_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347643
    move-object v1, v2

    .line 1347644
    new-instance v2, LX/8Ux;

    invoke-direct {v2}, LX/8Ux;-><init>()V

    move-object v2, v2

    .line 1347645
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1347646
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1347647
    iget-object v2, v0, LX/8Ul;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1347648
    new-instance v2, LX/8Ui;

    invoke-direct {v2, v0}, LX/8Ui;-><init>(LX/8Ul;)V

    iget-object v3, v0, LX/8Ul;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1347649
    return-void
.end method


# virtual methods
.method public final a(LX/8TE;)V
    .locals 4

    .prologue
    .line 1347650
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    .line 1347651
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    iget-object v3, p1, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 1347652
    sget-object v2, LX/8TE;->FUNNEL_CHALLENGE_LIST_SHOWN:LX/8TE;

    invoke-static {v0, v2, v1}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1347653
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    if-eqz v0, :cond_0

    .line 1347654
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    invoke-virtual {v0}, LX/8Wo;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1347655
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->m(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347656
    :cond_0
    :goto_0
    return-void

    .line 1347657
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    .line 1347658
    iget-object v1, v0, LX/8Wo;->d:LX/8Wr;

    invoke-virtual {v1}, LX/8Wr;->a()V

    .line 1347659
    goto :goto_0
.end method

.method public final a(LX/8Vb;LX/8TK;I)V
    .locals 4

    .prologue
    .line 1347660
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    iget-object v1, p2, LX/8TK;->effect:LX/8TJ;

    invoke-virtual {v0, p1, v1}, LX/8TS;->a(LX/8Vb;LX/8TJ;)V

    .line 1347661
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    .line 1347662
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT_ID:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    iget-object v3, v0, LX/8TD;->c:LX/8TS;

    .line 1347663
    iget-object p1, v3, LX/8TS;->j:LX/8TP;

    move-object v3, p1

    .line 1347664
    iget-object p1, v3, LX/8TP;->b:Ljava/lang/String;

    move-object v3, p1

    .line 1347665
    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_CONTEXT:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    iget-object v3, v0, LX/8TD;->c:LX/8TS;

    .line 1347666
    iget-object p1, v3, LX/8TS;->j:LX/8TP;

    move-object v3, p1

    .line 1347667
    iget-object p1, v3, LX/8TP;->a:LX/8Ta;

    move-object v3, p1

    .line 1347668
    invoke-virtual {v3}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_INDEX:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, p3}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v1

    sget-object v2, LX/8TE;->FUNNEL_ACTION_TAG_SOURCE:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    iget-object v3, p2, LX/8TK;->loggingTag:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 1347669
    sget-object v2, LX/8TE;->FUNNEL_GAME_CONTEXT_UPDATE:LX/8TE;

    invoke-static {v0, v2, v1}, LX/8TD;->a(LX/8TD;LX/8TE;LX/1rQ;)V

    .line 1347670
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->m(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347671
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    check-cast v0, Landroid/view/View;

    .line 1347672
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1347673
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8T7;

    new-instance v2, LX/8Sr;

    invoke-direct {v2, p0}, LX/8Sr;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-virtual {v1, v0, v2}, LX/8T7;->b(Landroid/view/View;LX/8Sl;)V

    .line 1347674
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    invoke-interface {v0}, LX/8VW;->c()V

    .line 1347675
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1347676
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    invoke-interface {v0}, LX/8VW;->a()V

    .line 1347677
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->v()V

    .line 1347678
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    check-cast v0, Landroid/view/View;

    .line 1347679
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1347680
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8T7;

    new-instance v2, LX/8Sq;

    invoke-direct {v2, p0}, LX/8Sq;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-virtual {v1, v0, v2}, LX/8T7;->b(Landroid/view/View;LX/8Sl;)V

    .line 1347681
    :cond_0
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->e(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347682
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1347683
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    .line 1347684
    iget-object v1, v0, LX/8Ul;->b:LX/8TS;

    invoke-virtual {v1}, LX/8TS;->i()LX/8TU;

    move-result-object v1

    .line 1347685
    iget-object v2, v1, LX/8TU;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, LX/0Ph;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    move-object v1, v2

    .line 1347686
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1347687
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1347688
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8TP;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v4, 0x1

    invoke-static {v0, v2, v1, v4}, LX/8Ul;->a(LX/8Ul;LX/8TP;IZ)V

    goto :goto_0

    .line 1347689
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1347690
    iput-object v3, v0, LX/8TS;->e:LX/8TO;

    .line 1347691
    iput-object v3, v0, LX/8TS;->f:LX/8Tb;

    .line 1347692
    iput-object v3, v0, LX/8TS;->g:LX/8Vb;

    .line 1347693
    iput-object v3, v0, LX/8TS;->h:LX/8TW;

    .line 1347694
    iput-boolean v4, v0, LX/8TS;->i:Z

    .line 1347695
    new-instance v1, LX/8TP;

    sget-object v2, LX/8Ta;->None:LX/8Ta;

    invoke-direct {v1, v2, v3}, LX/8TP;-><init>(LX/8Ta;Ljava/lang/String;)V

    iput-object v1, v0, LX/8TS;->j:LX/8TP;

    .line 1347696
    iput-object v3, v0, LX/8TS;->k:Ljava/lang/String;

    .line 1347697
    iget-object v1, v0, LX/8TS;->l:LX/8TU;

    if-eqz v1, :cond_2

    .line 1347698
    iget-object v1, v0, LX/8TS;->l:LX/8TU;

    .line 1347699
    iget-object v2, v1, LX/8TU;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 1347700
    iput-object v3, v0, LX/8TS;->l:LX/8TU;

    .line 1347701
    :cond_2
    sget-object v1, LX/8TV;->START_SCREEN:LX/8TV;

    iput-object v1, v0, LX/8TS;->m:LX/8TV;

    .line 1347702
    iput v4, v0, LX/8TS;->n:I

    .line 1347703
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8TS;->q:Ljava/lang/String;

    .line 1347704
    iput-object v3, v0, LX/8TS;->r:Ljava/lang/String;

    .line 1347705
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->p:Z

    .line 1347706
    return-void
.end method

.method public final d()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1347707
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    if-nez v2, :cond_0

    move v0, v1

    .line 1347708
    :goto_0
    return v0

    .line 1347709
    :cond_0
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    invoke-virtual {v2}, LX/8Wo;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1347710
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->m(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    goto :goto_0

    .line 1347711
    :cond_1
    sget-object v2, LX/8Si;->b:[I

    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347712
    iget-object v4, v3, LX/8TS;->m:LX/8TV;

    move-object v3, v4

    .line 1347713
    invoke-virtual {v3}, LX/8TV;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_2
    move v0, v1

    .line 1347714
    goto :goto_0

    .line 1347715
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->B:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1347716
    iget-wide v4, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->o:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x1388

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 1347717
    iput-wide v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->o:J

    .line 1347718
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->u()V

    goto :goto_0

    .line 1347719
    :cond_3
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1347720
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    invoke-interface {v0}, LX/8VV;->b()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1347721
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1347722
    const-class v0, Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1347723
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1347724
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5512e05b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1347725
    const v0, 0x7f0310df

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1347726
    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1347727
    new-instance v1, LX/8W7;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v4, v5}, LX/8W7;-><init>(Landroid/content/Context;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    .line 1347728
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    new-instance v4, LX/8Sj;

    invoke-direct {v4, p0}, LX/8Sj;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-virtual {v1, v4}, LX/8W7;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1347729
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1347730
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    new-instance v1, LX/8Sv;

    invoke-direct {v1, p0}, LX/8Sv;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    const/4 v6, 0x1

    .line 1347731
    new-instance v4, LX/8W3;

    invoke-direct {v4, v0}, LX/8W3;-><init>(LX/8W7;)V

    invoke-virtual {v0, v4}, LX/8W7;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1347732
    iget-object v4, v0, LX/8W7;->b:LX/8Sx;

    new-instance v5, LX/8W4;

    invoke-direct {v5, v0, v1}, LX/8W4;-><init>(LX/8W7;LX/8Sv;)V

    .line 1347733
    new-instance p1, LX/8Sw;

    const/16 v1, 0x3072

    invoke-static {v4, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {p1, v5, v1}, LX/8Sw;-><init>(LX/8W4;LX/0Ot;)V

    .line 1347734
    move-object v4, p1

    .line 1347735
    const-string v5, "QuicksilverAndroid"

    invoke-virtual {v0, v4, v5}, LX/8W7;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1347736
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 1347737
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Landroid/webkit/CookieManager;->setAcceptThirdPartyCookies(Landroid/webkit/WebView;Z)V

    .line 1347738
    :cond_0
    invoke-virtual {v0}, LX/8W7;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 1347739
    invoke-virtual {v0}, LX/8W7;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 1347740
    invoke-virtual {v0}, LX/8W7;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 1347741
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->r:LX/8Sy;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1347742
    iget-object v4, v0, LX/8Sy;->a:LX/8TY;

    invoke-virtual {v4}, LX/8TY;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1347743
    new-instance v4, LX/8Y7;

    invoke-direct {v4, v1}, LX/8Y7;-><init>(Landroid/content/Context;)V

    .line 1347744
    :goto_0
    move-object v1, v4

    .line 1347745
    instance-of v0, v1, LX/8VW;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1347746
    check-cast v0, LX/8VW;

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    .line 1347747
    :goto_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    new-instance v4, LX/8Sk;

    invoke-direct {v4, p0}, LX/8Sk;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-interface {v0, v4}, LX/8VW;->setCallbackDelegate(LX/8Sk;)V

    .line 1347748
    invoke-direct {p0, v1, v3}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1347749
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->q:LX/8Sg;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1347750
    iget-object v4, v0, LX/8Sg;->a:LX/8TY;

    invoke-virtual {v4}, LX/8TY;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1347751
    new-instance v4, LX/8Xa;

    invoke-direct {v4, v1}, LX/8Xa;-><init>(Landroid/content/Context;)V

    .line 1347752
    :goto_2
    move-object v1, v4

    .line 1347753
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1347754
    instance-of v0, v1, LX/8VV;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1347755
    check-cast v0, LX/8VV;

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    .line 1347756
    :goto_3
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    new-instance v4, LX/8Sn;

    invoke-direct {v4, p0}, LX/8Sn;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    invoke-interface {v0, v4}, LX/8VV;->setCallbackDelegate(LX/8Sn;)V

    .line 1347757
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1347758
    invoke-direct {p0, v1, v3}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1347759
    new-instance v0, LX/8Wo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8Wo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    .line 1347760
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    new-instance v1, LX/8So;

    invoke-direct {v1, p0}, LX/8So;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347761
    iput-object v1, v0, LX/8Wo;->e:LX/8So;

    .line 1347762
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/8Wo;->setVisibility(I)V

    .line 1347763
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    invoke-direct {p0, v0, v3}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1347764
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    new-instance v1, LX/8Sp;

    invoke-direct {v1, p0}, LX/8Sp;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347765
    iput-object v1, v0, LX/8Ul;->f:LX/8Sp;

    .line 1347766
    const v0, -0x7f940079

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 1347767
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v4, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    const-string v5, "Invalid QuicksilverLoadingView created by QuicksilverLoadingViewFactory"

    invoke-virtual {v0, v4, v5}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    goto :goto_1

    .line 1347768
    :cond_2
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->u:LX/8TD;

    sget-object v4, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    const-string v5, "Invalid QuicksilverEndgameView created by QuicksilverEndgameViewFactory"

    invoke-virtual {v0, v4, v5}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    new-instance v4, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;

    invoke-direct {v4, v1}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    new-instance v4, LX/8X4;

    invoke-direct {v4, v1}, LX/8X4;-><init>(Landroid/content/Context;)V

    goto :goto_2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x2348cb7a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1347769
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->s(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347770
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1347771
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverFragment;->c()V

    .line 1347772
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->e:Z

    .line 1347773
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    invoke-virtual {v1}, LX/8W7;->destroy()V

    .line 1347774
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    .line 1347775
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->v:LX/8Ul;

    .line 1347776
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->a:LX/8VW;

    .line 1347777
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->b:LX/8VV;

    .line 1347778
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->d:LX/8Wo;

    .line 1347779
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->s:LX/8TS;

    .line 1347780
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->g:Ljava/util/Set;

    .line 1347781
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->l:Ljava/util/List;

    .line 1347782
    iput-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->m:Ljava/util/List;

    .line 1347783
    const/16 v1, 0x2b

    const v2, -0x158bf1b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x60b737d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1347784
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1347785
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    invoke-virtual {v1}, LX/8W7;->onPause()V

    .line 1347786
    const/16 v1, 0x2b

    const v2, 0x6ec28b43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xdb86f07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1347787
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1347788
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment;->c:LX/8W7;

    invoke-virtual {v1}, LX/8W7;->onResume()V

    .line 1347789
    const/16 v1, 0x2b

    const v2, -0x117f15e7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
