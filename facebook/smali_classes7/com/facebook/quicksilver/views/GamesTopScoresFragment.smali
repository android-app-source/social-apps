.class public Lcom/facebook/quicksilver/views/GamesTopScoresFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/8VU;


# instance fields
.field public a:LX/8WB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8TS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/support/v7/widget/RecyclerView;

.field public e:LX/8VT;

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1353651
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1353652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->f:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    new-instance v2, LX/8WB;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, LX/8WB;-><init>(Landroid/content/Context;)V

    move-object v1, v2

    check-cast v1, LX/8WB;

    invoke-static {p0}, LX/8Tk;->b(LX/0QB;)LX/8Tk;

    move-result-object v2

    check-cast v2, LX/8Tk;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object p0

    check-cast p0, LX/8TS;

    iput-object v1, p1, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    iput-object v2, p1, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->b:LX/8Tk;

    iput-object p0, p1, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->c:LX/8TS;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1353646
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->c:LX/8TS;

    .line 1353647
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1353648
    if-eqz v0, :cond_0

    .line 1353649
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->b:LX/8Tk;

    const/16 v1, 0x1e

    new-instance v2, LX/8Vy;

    invoke-direct {v2, p0}, LX/8Vy;-><init>(Lcom/facebook/quicksilver/views/GamesTopScoresFragment;)V

    invoke-virtual {v0, v1, v2}, LX/8Tk;->a(ILX/8Tj;)V

    .line 1353650
    :cond_0
    return-void
.end method

.method public final a(LX/8VT;)V
    .locals 2

    .prologue
    .line 1353653
    iput-object p1, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->e:LX/8VT;

    .line 1353654
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    if-eqz v0, :cond_0

    .line 1353655
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    new-instance v1, LX/8Vz;

    invoke-direct {v1, p0}, LX/8Vz;-><init>(Lcom/facebook/quicksilver/views/GamesTopScoresFragment;)V

    .line 1353656
    iput-object v1, v0, LX/8WB;->e:LX/8Vz;

    .line 1353657
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353642
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1353643
    const-class v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1353644
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->e:LX/8VT;

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a(LX/8VT;)V

    .line 1353645
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1353638
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->f:Z

    .line 1353639
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    if-eqz v0, :cond_0

    .line 1353640
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    invoke-virtual {v0}, LX/8WB;->d()V

    .line 1353641
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xc85105e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1353637
    const v1, 0x7f030785

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2835a297

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353630
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1353631
    const v0, 0x7f0d1413

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->d:Landroid/support/v7/widget/RecyclerView;

    .line 1353632
    new-instance v0, LX/62V;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62V;-><init>(Landroid/content/Context;)V

    .line 1353633
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1353634
    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1353635
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->d:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a:LX/8WB;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1353636
    return-void
.end method
