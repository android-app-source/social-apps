.class public Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8WX;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TY;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/8WQ;

.field public l:LX/8WP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1354069
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1354070
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354067
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354068
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354047
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354048
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354049
    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->h:LX/0Ot;

    .line 1354050
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354051
    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->i:LX/0Ot;

    .line 1354052
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354053
    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->j:LX/0Ot;

    .line 1354054
    sget-object v0, LX/8WQ;->DISABLED:LX/8WQ;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1354055
    const-class v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1354056
    const v0, 0x7f020be8

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setBackgroundResource(I)V

    .line 1354057
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1354058
    new-instance v0, LX/62V;

    invoke-direct {v0, p1}, LX/62V;-><init>(Landroid/content/Context;)V

    .line 1354059
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1354060
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1354061
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0vv;->c(Landroid/view/View;Z)V

    .line 1354062
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8WX;

    new-instance v1, LX/8WO;

    invoke-direct {v1, p0}, LX/8WO;-><init>(Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;)V

    .line 1354063
    iput-object v1, v0, LX/8WX;->d:LX/8WO;

    .line 1354064
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8WX;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8TS;

    invoke-virtual {v1}, LX/8TS;->c()LX/8Vb;

    move-result-object v1

    iget-object v1, v1, LX/8Vb;->a:Ljava/lang/String;

    .line 1354065
    iput-object v1, v0, LX/8WX;->h:Ljava/lang/String;

    .line 1354066
    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;",
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8WX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8TY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1354046
    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->h:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->i:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->j:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/16 v1, 0x3074

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x308b

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3075

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->a(Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;)Z
    .locals 2

    .prologue
    .line 1354045
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    iget-object v0, v0, LX/8Vb;->a:Ljava/lang/String;

    iget-object v1, p1, LX/8Vb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setCallback(LX/8WP;)V
    .locals 0

    .prologue
    .line 1354043
    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    .line 1354044
    return-void
.end method

.method public setChallengeMode(LX/8WQ;)V
    .locals 0

    .prologue
    .line 1354041
    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1354042
    return-void
.end method
