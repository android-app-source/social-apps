.class public Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Wx;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/8Wy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1354478
    const-class v0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1354479
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1354480
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1354481
    packed-switch p2, :pswitch_data_0

    .line 1354482
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1354483
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03076a

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354484
    new-instance v0, LX/8Wt;

    invoke-direct {v0, p0, v1}, LX/8Wt;-><init>(Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 1354485
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030769

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1354486
    new-instance v0, LX/8Wv;

    invoke-direct {v0, p0, v1}, LX/8Wv;-><init>(Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1354487
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1354488
    check-cast p1, LX/8Wv;

    .line 1354489
    if-nez p2, :cond_4

    .line 1354490
    const/4 v0, 0x0

    .line 1354491
    :goto_0
    move-object v0, v0

    .line 1354492
    iget-object v1, p1, LX/8Wv;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1354493
    iget-object v2, v0, LX/8Wx;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1354494
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354495
    iget-object v1, v0, LX/8Wx;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1354496
    if-nez v1, :cond_5

    .line 1354497
    iget-object v1, p1, LX/8Wv;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1354498
    :goto_1
    iget-object v1, v0, LX/8Wx;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1354499
    if-eqz v1, :cond_0

    .line 1354500
    iget-object v1, p1, LX/8Wv;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1354501
    iget-object v2, v0, LX/8Wx;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1354502
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354503
    :cond_0
    iget-object v1, v0, LX/8Wx;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1354504
    if-eqz v1, :cond_1

    .line 1354505
    iget-object v1, p1, LX/8Wv;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 1354506
    iget-object v2, v0, LX/8Wx;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1354507
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354508
    :cond_1
    iget-boolean v1, v0, LX/8Wx;->f:Z

    move v1, v1

    .line 1354509
    if-eqz v1, :cond_2

    .line 1354510
    iget-object v1, p1, LX/8Wv;->s:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1354511
    :cond_2
    new-instance v1, LX/8Wu;

    invoke-direct {v1, p1, v0, p2}, LX/8Wu;-><init>(LX/8Wv;LX/8Wx;I)V

    .line 1354512
    iget-object v2, p1, LX/8Wv;->m:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354513
    iget-object v2, p1, LX/8Wv;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354514
    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->b:Ljava/util/List;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Wx;

    goto :goto_0

    .line 1354515
    :cond_5
    iget-object v1, p1, LX/8Wv;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1354516
    iget-object v2, v0, LX/8Wx;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1354517
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object p0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1354518
    if-nez p1, :cond_0

    .line 1354519
    const/4 v0, 0x1

    .line 1354520
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1354521
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1354522
    const/4 v0, 0x0

    .line 1354523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
