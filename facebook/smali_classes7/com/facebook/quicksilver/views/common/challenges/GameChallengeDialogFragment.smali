.class public Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;
.super Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Landroid/widget/TextView;

.field private o:Lcom/facebook/widget/tiles/ThreadTileView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field public r:LX/8Vb;

.field public s:LX/8WN;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1354286
    invoke-direct {p0}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;-><init>()V

    .line 1354287
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354288
    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->m:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x17d536cd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354289
    invoke-super {p0, p1}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1354290
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;

    const/16 v1, 0x3074

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->m:LX/0Ot;

    .line 1354291
    const/16 v1, 0x2b

    const v2, -0x9e13ca3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x529c4d9b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354292
    const v1, 0x7f030770

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x7254f4d0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1354293
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1354294
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->r:LX/8Vb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->s:LX/8WN;

    if-nez v0, :cond_1

    .line 1354295
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1354296
    :goto_0
    return-void

    .line 1354297
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1354298
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1354299
    const v0, 0x7f0d13db

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->n:Landroid/widget/TextView;

    .line 1354300
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->n:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082368

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->r:LX/8Vb;

    iget-object v4, v4, LX/8Vb;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354301
    const v0, 0x7f0d0946

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1354302
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->o:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->r:LX/8Vb;

    iget-object v1, v1, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1354303
    const v0, 0x7f0d13dc

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->p:Landroid/widget/TextView;

    .line 1354304
    iget-object v1, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082369

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->r:LX/8Vb;

    iget-object v0, v0, LX/8Vb;->c:Ljava/lang/String;

    aput-object v0, v4, v5

    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TS;

    .line 1354305
    iget-object v5, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v5

    .line 1354306
    iget-object v5, v0, LX/8TO;->c:Ljava/lang/String;

    move-object v0, v5

    .line 1354307
    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354308
    const v0, 0x7f0d13dd

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->q:Landroid/view/View;

    .line 1354309
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;->q:Landroid/view/View;

    new-instance v1, LX/8Wd;

    invoke-direct {v1, p0}, LX/8Wd;-><init>(Lcom/facebook/quicksilver/views/common/challenges/GameChallengeDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
