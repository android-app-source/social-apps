.class public Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:LX/8Ws;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/8WZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/8Wr;

.field public d:LX/8Wl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1354367
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1354393
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354394
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1354390
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1354391
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->b()V

    .line 1354392
    return-void
.end method

.method private static a(Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;LX/8Ws;LX/8WZ;)V
    .locals 0

    .prologue
    .line 1354389
    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a:LX/8Ws;

    iput-object p2, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->b:LX/8WZ;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    const-class v0, LX/8Ws;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/8Ws;

    invoke-static {v1}, LX/8WZ;->a(LX/0QB;)LX/8WZ;

    move-result-object v1

    check-cast v1, LX/8WZ;

    invoke-static {p0, v0, v1}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a(Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;LX/8Ws;LX/8WZ;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1354377
    const-class v0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1354378
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030261

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1354379
    const v0, 0x7f0d08fb

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1354380
    new-instance v1, LX/62V;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/62V;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1354381
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;Z)V

    .line 1354382
    iget-object v1, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->a:LX/8Ws;

    invoke-virtual {v1, v0}, LX/8Ws;->a(Landroid/support/v7/widget/RecyclerView;)LX/8Wr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->c:LX/8Wr;

    .line 1354383
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->c:LX/8Wr;

    new-instance v1, LX/8Wk;

    invoke-direct {v1, p0}, LX/8Wk;-><init>(Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;)V

    .line 1354384
    iput-object v1, v0, LX/8Wr;->d:LX/8Wj;

    .line 1354385
    const v0, 0x7f0d08f7

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1354386
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1354387
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->b:LX/8WZ;

    const v1, 0x7f0d08fa

    invoke-virtual {p0, v1}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8WZ;->a(Landroid/view/View;)V

    .line 1354388
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1354375
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->c:LX/8Wr;

    invoke-virtual {v0}, LX/8Wr;->a()V

    .line 1354376
    return-void
.end method

.method public setCallback(LX/8Wl;)V
    .locals 0

    .prologue
    .line 1354373
    iput-object p1, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->d:LX/8Wl;

    .line 1354374
    return-void
.end method

.method public setMaxResults(I)V
    .locals 1

    .prologue
    .line 1354369
    iget-object v0, p0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->c:LX/8Wr;

    .line 1354370
    iget-object p0, v0, LX/8Wr;->a:LX/8Wi;

    .line 1354371
    iput p1, p0, LX/8Wi;->b:I

    .line 1354372
    return-void
.end method
