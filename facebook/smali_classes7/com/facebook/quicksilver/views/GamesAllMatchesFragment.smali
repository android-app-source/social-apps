.class public Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/8VU;


# instance fields
.field public a:LX/8Vt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8Te;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8TS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/support/v7/widget/RecyclerView;

.field public e:LX/8VT;

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1353565
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1353566
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->f:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    new-instance v2, LX/8Vt;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, LX/8Vt;-><init>(Landroid/content/Context;)V

    move-object v1, v2

    check-cast v1, LX/8Vt;

    invoke-static {p0}, LX/8Te;->b(LX/0QB;)LX/8Te;

    move-result-object v2

    check-cast v2, LX/8Te;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object p0

    check-cast p0, LX/8TS;

    iput-object v1, p1, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    iput-object v2, p1, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->b:LX/8Te;

    iput-object p0, p1, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1353567
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    .line 1353568
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1353569
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1353570
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->b:LX/8Te;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    .line 1353571
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1353572
    iget-object v2, v1, LX/8TO;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1353573
    iget-object v2, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    invoke-virtual {v2}, LX/8TS;->c()LX/8Vb;

    move-result-object v2

    iget-object v2, v2, LX/8Vb;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->c:LX/8TS;

    .line 1353574
    iget-object v4, v3, LX/8TS;->j:LX/8TP;

    move-object v3, v4

    .line 1353575
    new-instance v4, LX/8Vw;

    invoke-direct {v4, p0}, LX/8Vw;-><init>(Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8Te;->a(Ljava/lang/String;Ljava/lang/String;LX/8TP;LX/8Td;)V

    .line 1353576
    :cond_0
    return-void
.end method

.method public final a(LX/8VT;)V
    .locals 2

    .prologue
    .line 1353577
    iput-object p1, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->e:LX/8VT;

    .line 1353578
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    if-eqz v0, :cond_0

    .line 1353579
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    new-instance v1, LX/8Vx;

    invoke-direct {v1, p0}, LX/8Vx;-><init>(Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;)V

    .line 1353580
    iput-object v1, v0, LX/8Vt;->e:LX/8Vx;

    .line 1353581
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353582
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1353583
    const-class v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1353584
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->e:LX/8VT;

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a(LX/8VT;)V

    .line 1353585
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1353586
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->f:Z

    .line 1353587
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    if-eqz v0, :cond_0

    .line 1353588
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    invoke-virtual {v0}, LX/8Vt;->d()V

    .line 1353589
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ea36a01

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1353590
    const v1, 0x7f03076e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4a55be6f    # 3501979.8f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353591
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1353592
    const v0, 0x7f0d13d8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->d:Landroid/support/v7/widget/RecyclerView;

    .line 1353593
    new-instance v0, LX/62V;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62V;-><init>(Landroid/content/Context;)V

    .line 1353594
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1353595
    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1353596
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->d:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a:LX/8Vt;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1353597
    return-void
.end method
