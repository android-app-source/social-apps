.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/8Xf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1FZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8T9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/8TS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8Vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/resources/ui/FbFrameLayout;

.field public h:Lcom/facebook/widget/FbImageView;

.field private i:Lcom/facebook/widget/FbImageView;

.field private j:Lcom/facebook/widget/FbImageView;

.field private k:Lcom/facebook/widget/FbImageView;

.field public l:LX/8XK;

.field public m:LX/8Xe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1355183
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1355184
    return-void
.end method

.method public static a(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;Z)V
    .locals 2

    .prologue
    .line 1355185
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->m:LX/8Xe;

    invoke-virtual {v0, p1}, LX/8Xe;->a(Z)V

    .line 1355186
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->l:LX/8XK;

    if-eqz v0, :cond_0

    .line 1355187
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->l:LX/8XK;

    invoke-virtual {v0, p1}, LX/8XK;->a(Z)V

    .line 1355188
    :cond_0
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->k:Lcom/facebook/widget/FbImageView;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1355189
    return-void

    .line 1355190
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V
    .locals 2

    .prologue
    .line 1355177
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->m:LX/8Xe;

    .line 1355178
    iget-boolean v1, v0, LX/8Xe;->g:Z

    move v0, v1

    .line 1355179
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1355180
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->a(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;Z)V

    .line 1355181
    return-void

    .line 1355182
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a98d507

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355176
    const v1, 0x7f030775

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5b570f7d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355159
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1355160
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    const-class v3, LX/8Xf;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8Xf;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v4

    check-cast v4, LX/1FZ;

    invoke-static {v0}, LX/Jbe;->a(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    move-result-object v5

    check-cast v5, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-static {v0}, LX/8T9;->a(LX/0QB;)LX/8T9;

    move-result-object p1

    check-cast p1, LX/8T9;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object p2

    check-cast p2, LX/8TS;

    invoke-static {v0}, LX/8Vg;->a(LX/0QB;)LX/8Vg;

    move-result-object v0

    check-cast v0, LX/8Vg;

    iput-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->a:LX/8Xf;

    iput-object v4, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->b:LX/1FZ;

    iput-object v5, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->c:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iput-object p1, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->d:LX/8T9;

    iput-object p2, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->e:LX/8TS;

    iput-object v0, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->f:LX/8Vg;

    .line 1355161
    const v0, 0x7f0d13ed

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->h:Lcom/facebook/widget/FbImageView;

    .line 1355162
    const v0, 0x7f0d0508

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->k:Lcom/facebook/widget/FbImageView;

    .line 1355163
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->k:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/8XW;

    invoke-direct {v1, p0}, LX/8XW;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355164
    const v0, 0x7f0d13f3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->i:Lcom/facebook/widget/FbImageView;

    .line 1355165
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->i:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/8XX;

    invoke-direct {v1, p0}, LX/8XX;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355166
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->j:Lcom/facebook/widget/FbImageView;

    .line 1355167
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->j:Lcom/facebook/widget/FbImageView;

    new-instance v1, LX/8XY;

    invoke-direct {v1, p0}, LX/8XY;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355168
    const v0, 0x7f0d13ee

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1355169
    const v0, 0x7f0d0803

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    .line 1355170
    iget-object v2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->a:LX/8Xf;

    .line 1355171
    new-instance v4, LX/8Xe;

    invoke-static {v2}, LX/8T7;->b(LX/0QB;)LX/8T7;

    move-result-object v3

    check-cast v3, LX/8T7;

    invoke-direct {v4, v1, v0, v3}, LX/8Xe;-><init>(Landroid/view/View;Lcom/facebook/drawingview/DrawingView;LX/8T7;)V

    .line 1355172
    move-object v0, v4

    .line 1355173
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->m:LX/8Xe;

    .line 1355174
    const v0, 0x7f0d13ec

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->g:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 1355175
    return-void
.end method
