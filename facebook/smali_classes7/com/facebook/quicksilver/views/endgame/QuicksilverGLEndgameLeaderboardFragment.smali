.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/8Wc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8TS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/View;

.field public d:Landroid/widget/TextView;

.field public e:LX/8XI;

.field public f:LX/8Wb;

.field public g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

.field public h:Lcom/facebook/widget/FbScrollView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field public k:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1355131
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1355132
    return-void
.end method

.method public static d(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V
    .locals 2

    .prologue
    .line 1355100
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setVisibility(I)V

    .line 1355101
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->setAlpha(F)V

    .line 1355102
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1355103
    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x72666545

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355129
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    const-class v3, LX/8Wc;

    invoke-interface {v1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8Wc;

    invoke-static {v1}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v1

    check-cast v1, LX/8TS;

    iput-object v3, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->a:LX/8Wc;

    iput-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->b:LX/8TS;

    .line 1355130
    const v1, 0x7f030774

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x68f19a65

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355104
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1355105
    const v0, 0x7f0d13fc

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d:Landroid/widget/TextView;

    .line 1355106
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d:Landroid/widget/TextView;

    const v1, 0x7f082347

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1355107
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d:Landroid/widget/TextView;

    new-instance v1, LX/8XR;

    invoke-direct {v1, p0}, LX/8XR;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355108
    const v0, 0x7f0d13e4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->h:Lcom/facebook/widget/FbScrollView;

    .line 1355109
    const v0, 0x7f0d13e5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->k:Landroid/view/ViewGroup;

    .line 1355110
    const v0, 0x7f0d13e9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1355111
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    sget-object v1, LX/8WQ;->SHOW_CHALLENGE_POPOVER:LX/8WQ;

    .line 1355112
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1355113
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    new-instance v1, LX/8XS;

    invoke-direct {v1, p0}, LX/8XS;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    .line 1355114
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    .line 1355115
    const v0, 0x7f0d13e6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1355116
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->a:LX/8Wc;

    .line 1355117
    new-instance p2, LX/8Wb;

    invoke-static {v1}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v2

    check-cast v2, LX/8TS;

    invoke-direct {p2, v0, v2}, LX/8Wb;-><init>(Landroid/view/View;LX/8TS;)V

    .line 1355118
    invoke-static {v1}, LX/8TA;->a(LX/0QB;)LX/8TA;

    move-result-object v2

    check-cast v2, LX/8TA;

    .line 1355119
    iput-object v2, p2, LX/8Wb;->d:LX/8TA;

    .line 1355120
    move-object v0, p2

    .line 1355121
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->f:LX/8Wb;

    .line 1355122
    const v0, 0x7f0d13ea

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->i:Landroid/view/View;

    .line 1355123
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->i:Landroid/view/View;

    new-instance v1, LX/8XT;

    invoke-direct {v1, p0}, LX/8XT;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355124
    const v0, 0x7f0d13eb

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->j:Landroid/view/View;

    .line 1355125
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->j:Landroid/view/View;

    new-instance v1, LX/8XU;

    invoke-direct {v1, p0}, LX/8XU;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355126
    iput-object p1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->c:Landroid/view/View;

    .line 1355127
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/8XV;

    invoke-direct {v1, p0}, LX/8XV;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1355128
    return-void
.end method
