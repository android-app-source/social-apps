.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/FbImageView;

.field public b:Lcom/facebook/widget/FbImageView;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/resources/ui/FbImageButton;

.field public f:Landroid/graphics/Bitmap;

.field private g:Landroid/view/View;

.field public h:I

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/view/View;

.field public m:LX/8WI;

.field public n:LX/8VT;

.field public o:LX/5OM;

.field public p:LX/8WJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/8Vv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/8TC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/8T7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/8Vg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1354713
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1354714
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354715
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->s:LX/0Ot;

    .line 1354716
    return-void
.end method

.method public static a$redex0(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1354768
    if-eqz p1, :cond_0

    .line 1354769
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->v:LX/8T7;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    .line 1354770
    :goto_0
    return-void

    .line 1354771
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->o:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1354772
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->v:LX/8T7;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, LX/8T7;->f(Landroid/view/View;LX/8Sl;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1354748
    iput p1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    .line 1354749
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->i:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354750
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->t:LX/8TS;

    .line 1354751
    iget-object v1, v0, LX/8TS;->h:LX/8TW;

    move-object v0, v1

    .line 1354752
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1354753
    :goto_0
    if-nez v0, :cond_3

    .line 1354754
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->t:LX/8TS;

    invoke-virtual {v0}, LX/8TS;->c()LX/8Vb;

    move-result-object v0

    .line 1354755
    iget v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    int-to-long v2, v1

    iget-wide v0, v0, LX/8Vb;->m:J

    sub-long v0, v2, v0

    .line 1354756
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 1354757
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082358

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354758
    :goto_1
    return-void

    .line 1354759
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->t:LX/8TS;

    .line 1354760
    iget-object v1, v0, LX/8TS;->h:LX/8TW;

    move-object v0, v1

    .line 1354761
    iget v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/8TW;->a(J)LX/8Vb;

    move-result-object v0

    goto :goto_0

    .line 1354762
    :cond_1
    cmp-long v2, v0, v4

    if-nez v2, :cond_2

    .line 1354763
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082356

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1354764
    :cond_2
    iget-object v2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082355

    new-array v5, v11, [Ljava/lang/Object;

    sub-long v0, v12, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1354765
    :cond_3
    iget-wide v2, v0, LX/8Vb;->l:J

    iget v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_4

    .line 1354766
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082354

    new-array v4, v6, [Ljava/lang/Object;

    iget-wide v6, v0, LX/8Vb;->l:J

    iget v5, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    int-to-long v8, v5

    sub-long/2addr v6, v8

    add-long/2addr v6, v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v0, v0, LX/8Vb;->d:Ljava/lang/String;

    aput-object v0, v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1354767
    :cond_4
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082357

    new-array v4, v6, [Ljava/lang/Object;

    iget v5, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    int-to-long v6, v5

    iget-wide v8, v0, LX/8Vb;->l:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v0, v0, LX/8Vb;->d:Ljava/lang/String;

    aput-object v0, v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354773
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1354774
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    const-class v3, LX/8WJ;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8WJ;

    invoke-static {v0}, LX/8Vv;->a(LX/0QB;)LX/8Vv;

    move-result-object v4

    check-cast v4, LX/8Vv;

    invoke-static {v0}, LX/Jbe;->a(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    move-result-object v5

    check-cast v5, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    const/16 v6, 0x306f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v7

    check-cast v7, LX/8TS;

    invoke-static {v0}, LX/8TC;->a(LX/0QB;)LX/8TC;

    move-result-object v8

    check-cast v8, LX/8TC;

    invoke-static {v0}, LX/8T7;->b(LX/0QB;)LX/8T7;

    move-result-object p1

    check-cast p1, LX/8T7;

    invoke-static {v0}, LX/8Vg;->a(LX/0QB;)LX/8Vg;

    move-result-object v0

    check-cast v0, LX/8Vg;

    iput-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->p:LX/8WJ;

    iput-object v4, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->q:LX/8Vv;

    iput-object v5, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->r:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iput-object v6, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->s:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->t:LX/8TS;

    iput-object v8, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->u:LX/8TC;

    iput-object p1, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->v:LX/8T7;

    iput-object v0, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->w:LX/8Vg;

    .line 1354775
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d09d18c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354747
    const v1, 0x7f0310dc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3e6a943b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1354717
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1354718
    const v0, 0x7f0d2800

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->i:Landroid/widget/TextView;

    .line 1354719
    const v0, 0x7f0d2801

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->j:Landroid/widget/TextView;

    .line 1354720
    invoke-virtual {p0, v2}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a(I)V

    .line 1354721
    const v0, 0x7f0d13f9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->l:Landroid/view/View;

    .line 1354722
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1354723
    const v0, 0x7f0d1409

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->k:Landroid/widget/TextView;

    .line 1354724
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->k:Landroid/widget/TextView;

    const v1, 0x7f082359

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1354725
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1354726
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->k:Landroid/widget/TextView;

    new-instance v1, LX/8XC;

    invoke-direct {v1, p0}, LX/8XC;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354727
    const v0, 0x7f0d2803

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a:Lcom/facebook/widget/FbImageView;

    .line 1354728
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d280c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->b:Lcom/facebook/widget/FbImageView;

    .line 1354729
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d280d

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->e:Lcom/facebook/resources/ui/FbImageButton;

    .line 1354730
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d280b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->g:Landroid/view/View;

    .line 1354731
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->g:Landroid/view/View;

    new-instance v1, LX/8XD;

    invoke-direct {v1, p0}, LX/8XD;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354732
    const v0, 0x7f0d2802

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    .line 1354733
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0d280a

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    .line 1354734
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    new-instance v1, LX/8XE;

    invoke-direct {v1, p0}, LX/8XE;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354735
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->r:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->c(Landroid/app/Activity;)Ljava/util/List;

    move-result-object v0

    .line 1354736
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1354737
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->e:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 1354738
    :goto_0
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->p:LX/8WJ;

    const v0, 0x7f0d13d8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->q:LX/8Vv;

    invoke-virtual {v1, v0, v2}, LX/8WJ;->a(Landroid/support/v7/widget/RecyclerView;LX/8VY;)LX/8WI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->m:LX/8WI;

    .line 1354739
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->m:LX/8WI;

    invoke-virtual {v0}, LX/8WI;->a()V

    .line 1354740
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->m:LX/8WI;

    new-instance v1, LX/8XF;

    invoke-direct {v1, p0}, LX/8XF;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V

    .line 1354741
    iput-object v1, v0, LX/8WI;->f:LX/8WH;

    .line 1354742
    return-void

    .line 1354743
    :cond_0
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->u:LX/8TC;

    iget-object v2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->e:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v1, v2, v0}, LX/8TC;->a(Landroid/view/View;Ljava/util/List;)LX/5OM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->o:LX/5OM;

    .line 1354744
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->o:LX/5OM;

    new-instance v1, LX/8XG;

    invoke-direct {v1, p0}, LX/8XG;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V

    .line 1354745
    iput-object v1, v0, LX/5OM;->p:LX/5OO;

    .line 1354746
    goto :goto_0
.end method
