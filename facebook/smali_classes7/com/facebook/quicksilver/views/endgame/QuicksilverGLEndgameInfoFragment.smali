.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

.field public b:LX/8X1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicksilver/dataloader/QuicksilverComponentDataProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

.field public h:Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

.field public i:Lcom/facebook/widget/FbScrollView;

.field public j:LX/8X0;

.field public k:LX/8XJ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1355066
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1355067
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355068
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->c:LX/0Ot;

    .line 1355069
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355070
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->d:LX/0Ot;

    .line 1355071
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355072
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->e:LX/0Ot;

    .line 1355073
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355074
    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->f:LX/0Ot;

    .line 1355075
    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4299b31d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355064
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v3, p0

    check-cast v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    const-class v5, LX/8X1;

    invoke-interface {p3, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/8X1;

    const/16 v6, 0x3074

    invoke-static {p3, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x307b

    invoke-static {p3, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3071

    invoke-static {p3, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v1, 0x306f

    invoke-static {p3, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v5, v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->b:LX/8X1;

    iput-object v6, v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->c:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->d:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->e:LX/0Ot;

    iput-object p3, v3, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->f:LX/0Ot;

    .line 1355065
    const v1, 0x7f030773

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x73208f19

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355035
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1355036
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

    invoke-direct {v0, p0, p1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

    .line 1355037
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->d:Landroid/widget/TextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1355038
    const v0, 0x7f0d13df

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->i:Lcom/facebook/widget/FbScrollView;

    .line 1355039
    const v0, 0x7f0d13e1

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    .line 1355040
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    sget-object v1, LX/8WQ;->DIRECT_CHALLENGE:LX/8WQ;

    .line 1355041
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->k:LX/8WQ;

    .line 1355042
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->g:Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;

    new-instance v1, LX/8XM;

    invoke-direct {v1, p0}, LX/8XM;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;)V

    .line 1355043
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/GLLeaderboardCardView;->l:LX/8WP;

    .line 1355044
    const v0, 0x7f0d13e0

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->h:Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    .line 1355045
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->h:Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    new-instance v1, LX/8XN;

    invoke-direct {v1, p0}, LX/8XN;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;)V

    .line 1355046
    iput-object v1, v0, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->d:LX/8Wl;

    .line 1355047
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->h:Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/common/challenges/QuicksilverChallengeListCardView;->setMaxResults(I)V

    .line 1355048
    const v0, 0x7f0d13e2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1355049
    new-instance v1, LX/62V;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/62V;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1355050
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;Z)V

    .line 1355051
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->b:LX/8X1;

    .line 1355052
    new-instance v3, LX/8X0;

    .line 1355053
    new-instance v4, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    invoke-direct {v4}, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;-><init>()V

    .line 1355054
    move-object v4, v4

    .line 1355055
    move-object v4, v4

    .line 1355056
    check-cast v4, Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;

    invoke-static {v1}, LX/8Tm;->a(LX/0QB;)LX/8Tm;

    move-result-object v5

    check-cast v5, LX/8Tm;

    invoke-static {v1}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v6

    check-cast v6, LX/8TD;

    invoke-static {v1}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v7

    check-cast v7, LX/8TS;

    move-object v8, v0

    invoke-direct/range {v3 .. v8}, LX/8X0;-><init>(Lcom/facebook/quicksilver/views/common/recommendations/GameRecommendationAdapter;LX/8Tm;LX/8TD;LX/8TS;Landroid/support/v7/widget/RecyclerView;)V

    .line 1355057
    move-object v1, v3

    .line 1355058
    iput-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->j:LX/8X0;

    .line 1355059
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->j:LX/8X0;

    new-instance v2, LX/8XO;

    invoke-direct {v2, p0, v0}, LX/8XO;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;Landroid/support/v7/widget/RecyclerView;)V

    .line 1355060
    iput-object v2, v1, LX/8X0;->e:LX/8XO;

    .line 1355061
    const v0, 0x7f0d13e3

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1355062
    new-instance v1, LX/8XP;

    invoke-direct {v1, p0}, LX/8XP;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355063
    return-void
.end method
