.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private b:Landroid/support/v4/view/ViewPager;

.field private c:Landroid/widget/TextView;

.field public d:LX/8XB;

.field public e:LX/8VT;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1354688
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1354689
    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7aac67be

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354679
    const v1, 0x7f0310dd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4e913159

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354680
    const v0, 0x7f0d2806

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->a:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1354681
    const v0, 0x7f0d2807

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->b:Landroid/support/v4/view/ViewPager;

    .line 1354682
    new-instance v0, LX/8XB;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/8XB;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->d:LX/8XB;

    .line 1354683
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->b:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->d:LX/8XB;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1354684
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->a:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1354685
    const v0, 0x7f0d2805

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->c:Landroid/widget/TextView;

    .line 1354686
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->c:Landroid/widget/TextView;

    new-instance v1, LX/8X8;

    invoke-direct {v1, p0}, LX/8X8;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1354687
    return-void
.end method
