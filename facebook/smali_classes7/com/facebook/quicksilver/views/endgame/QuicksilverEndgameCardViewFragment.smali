.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:LX/8X7;

.field public c:LX/8X2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1354639
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1354640
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1354641
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    invoke-virtual {v0}, LX/8X7;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a(I)V

    .line 1354642
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1354615
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    invoke-virtual {v0}, LX/8X7;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    move-result-object v0

    const/4 p0, 0x0

    .line 1354616
    iput-object p1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    .line 1354617
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1354618
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setAlpha(F)V

    .line 1354619
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1354620
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setAlpha(F)V

    .line 1354621
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1354622
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a:Lcom/facebook/widget/FbImageView;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->b:Lcom/facebook/widget/FbImageView;

    if-eqz v1, :cond_0

    .line 1354623
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a:Lcom/facebook/widget/FbImageView;

    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1354624
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->b:Lcom/facebook/widget/FbImageView;

    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1354625
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->v:LX/8T7;

    iget-object v2, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->c:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/8T7;->e(Landroid/view/View;LX/8Sl;)V

    .line 1354626
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1354636
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 1354637
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1354638
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1354643
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    invoke-virtual {v0}, LX/8X7;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    move-result-object v0

    const/4 v1, 0x0

    .line 1354644
    iget-object p0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->d:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    if-nez p0, :cond_0

    .line 1354645
    invoke-static {v0, v1}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a$redex0(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;Z)V

    .line 1354646
    const/4 v1, 0x1

    .line 1354647
    :cond_0
    move v0, v1

    .line 1354648
    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1354634
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    invoke-virtual {v0}, LX/8X7;->e()V

    .line 1354635
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x55fac90c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354633
    const v1, 0x7f0310da

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1e7a5c7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354627
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1354628
    const v0, 0x7f0d27fa

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a:Landroid/support/v4/view/ViewPager;

    .line 1354629
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a:Landroid/support/v4/view/ViewPager;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1354630
    new-instance v0, LX/8X7;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/8X7;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    .line 1354631
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;->b:LX/8X7;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1354632
    return-void
.end method
