.class public Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/CustomViewPager;

.field private c:LX/8XL;

.field public d:LX/8XH;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1354993
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1354994
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v0

    check-cast v0, LX/8TS;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->a:LX/8TS;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1354958
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-virtual {v0, p1}, LX/8XL;->a(Landroid/graphics/Bitmap;)V

    .line 1354959
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354960
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1354961
    const-class v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1354962
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;",
            "LX/8Vb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1354963
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-virtual {v0, p1, p2, p3}, LX/8XL;->a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V

    .line 1354964
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1354965
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1354966
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-static {v0}, LX/8XL;->g(LX/8XL;)V

    .line 1354967
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1354968
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-static {v0}, LX/8XL;->h(LX/8XL;)V

    .line 1354969
    return-void
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1354970
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    if-eqz v1, :cond_1

    .line 1354971
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 1354972
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1354973
    iget-object v2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-static {v2}, LX/8XL;->f(LX/8XL;)Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;

    move-result-object v2

    .line 1354974
    iget-object v3, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->m:LX/8Xe;

    .line 1354975
    iget-boolean v4, v3, LX/8Xe;->g:Z

    move v3, v4

    .line 1354976
    if-eqz v3, :cond_2

    .line 1354977
    invoke-static {v2}, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;->d(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameScreenshotFragment;)V

    .line 1354978
    const/4 v3, 0x1

    .line 1354979
    :goto_0
    move v2, v3

    .line 1354980
    if-eqz v2, :cond_0

    .line 1354981
    :goto_1
    return v0

    .line 1354982
    :cond_0
    if-eq v1, v0, :cond_1

    .line 1354983
    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1

    .line 1354984
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x778968d8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1354985
    const v1, 0x7f030772

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x192b5932

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1354986
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1354987
    const v0, 0x7f0d13de

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    .line 1354988
    new-instance v0, LX/8XL;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/8XL;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    .line 1354989
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->c:LX/8XL;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1354990
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1354991
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameFragment;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1354992
    return-void
.end method
