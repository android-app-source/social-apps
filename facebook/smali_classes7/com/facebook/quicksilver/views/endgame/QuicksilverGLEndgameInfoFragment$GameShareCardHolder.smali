.class public final Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public final synthetic f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

.field public final g:Lcom/facebook/common/callercontext/CallerContext;

.field public h:LX/5OM;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1355016
    iput-object p1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1355017
    const-class v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1355018
    const v0, 0x7f0d13ce

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355019
    const v0, 0x7f0d13cf    # 1.87524E38f

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355020
    const v0, 0x7f0d13d0

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->c:Landroid/widget/TextView;

    .line 1355021
    const v0, 0x7f0d13d1

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->d:Landroid/widget/TextView;

    .line 1355022
    const v0, 0x7f0d13fc

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->e:Landroid/widget/TextView;

    .line 1355023
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->e:Landroid/widget/TextView;

    const v1, 0x7f082343

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1355024
    const/16 p1, 0x8

    .line 1355025
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1355026
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355027
    :goto_0
    return-void

    .line 1355028
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->c(Landroid/app/Activity;)Ljava/util/List;

    move-result-object v0

    .line 1355029
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1355030
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1355031
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TC;

    iget-object p1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iget-object p2, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->f:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment;

    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Landroid/app/Activity;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/8TC;->a(Landroid/view/View;Ljava/util/List;)LX/5OM;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->h:LX/5OM;

    .line 1355032
    iget-object v0, p0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;->h:LX/5OM;

    new-instance v1, LX/8XQ;

    invoke-direct {v1, p0}, LX/8XQ;-><init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameInfoFragment$GameShareCardHolder;)V

    .line 1355033
    iput-object v1, v0, LX/5OM;->p:LX/5OO;

    .line 1355034
    goto :goto_0
.end method
