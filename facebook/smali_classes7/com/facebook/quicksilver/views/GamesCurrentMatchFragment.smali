.class public Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/8VU;


# instance fields
.field public a:LX/8WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8Vv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/8WI;

.field private d:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1353609
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1353607
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->c:LX/8WI;

    invoke-virtual {v0}, LX/8WI;->b()V

    .line 1353608
    return-void
.end method

.method public final a(LX/8VT;)V
    .locals 0

    .prologue
    .line 1353606
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353610
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1353611
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;

    const-class p1, LX/8WJ;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/8WJ;

    invoke-static {v0}, LX/8Vv;->a(LX/0QB;)LX/8Vv;

    move-result-object v0

    check-cast v0, LX/8Vv;

    iput-object p1, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->a:LX/8WJ;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->b:LX/8Vv;

    .line 1353612
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1353605
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x63296093

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1353604
    const v1, 0x7f030771

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3ae37c45

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1353598
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1353599
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1353600
    const v0, 0x7f0d13d8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->d:Landroid/support/v7/widget/RecyclerView;

    .line 1353601
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->a:LX/8WJ;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->d:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->b:LX/8Vv;

    invoke-virtual {v0, v1, v2}, LX/8WJ;->a(Landroid/support/v7/widget/RecyclerView;LX/8VY;)LX/8WI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->c:LX/8WI;

    .line 1353602
    iget-object v0, p0, Lcom/facebook/quicksilver/views/GamesCurrentMatchFragment;->c:LX/8WI;

    invoke-virtual {v0}, LX/8WI;->a()V

    .line 1353603
    return-void
.end method
