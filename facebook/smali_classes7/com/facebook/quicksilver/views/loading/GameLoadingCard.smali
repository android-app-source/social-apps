.class public Lcom/facebook/quicksilver/views/loading/GameLoadingCard;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1355442
    const-class v0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1355440
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355441
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1355473
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355474
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1355475
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355476
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->b()V

    .line 1355477
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1355464
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030778

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355465
    const v0, 0x7f0d13ce

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355466
    const v0, 0x7f0d13cf    # 1.87524E38f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355467
    const v0, 0x7f0d13f9

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->b:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    .line 1355468
    const v0, 0x7f0d13fa

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->e:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    .line 1355469
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->e:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->setInitialValue(I)V

    .line 1355470
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1355471
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->setProgress(I)V

    .line 1355472
    return-void
.end method

.method public setGameInfo(LX/8TO;)V
    .locals 3

    .prologue
    .line 1355453
    iget-object v0, p1, LX/8TO;->j:Landroid/net/Uri;

    move-object v0, v0

    .line 1355454
    if-eqz v0, :cond_0

    .line 1355455
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355456
    iget-object v1, p1, LX/8TO;->j:Landroid/net/Uri;

    move-object v1, v1

    .line 1355457
    sget-object v2, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1355458
    :cond_0
    iget-object v0, p1, LX/8TO;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1355459
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1355460
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355461
    iget-object v1, p1, LX/8TO;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1355462
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1355463
    :cond_1
    return-void
.end method

.method public setMaxProgress(I)V
    .locals 1

    .prologue
    .line 1355449
    iput p1, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->f:I

    .line 1355450
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->b:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    .line 1355451
    iput p1, v0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    .line 1355452
    return-void
.end method

.method public setProgress(I)V
    .locals 4

    .prologue
    .line 1355443
    const/4 v0, 0x0

    .line 1355444
    iget v1, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->f:I

    if-lez v1, :cond_0

    .line 1355445
    int-to-double v0, p1

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->f:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 1355446
    :cond_0
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->e:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->setProgress(I)V

    .line 1355447
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GameLoadingCard;->b:Lcom/facebook/quicksilver/views/loading/CircularProgressView;

    invoke-virtual {v0, p1}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->setProgress(I)V

    .line 1355448
    return-void
.end method
