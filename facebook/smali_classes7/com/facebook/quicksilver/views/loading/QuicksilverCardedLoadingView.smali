.class public final Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;
.super Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1Ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/8TY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

.field public i:Landroid/widget/ProgressBar;

.field public j:I

.field public k:LX/8Sk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1355732
    const-class v0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1355675
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355676
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1355713
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355714
    const-class p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;

    invoke-static {p1, p0}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1355715
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0310d9

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355716
    const p1, 0x7f0d27f7

    invoke-virtual {p0, p1}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355717
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->j:I

    .line 1355718
    iget-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget p2, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->j:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setY(F)V

    .line 1355719
    const p1, 0x7f0d27f8

    invoke-virtual {p0, p1}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->g:Landroid/view/View;

    .line 1355720
    iget-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->g:Landroid/view/View;

    iget p2, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->j:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    .line 1355721
    const p1, 0x7f0d27f9

    invoke-virtual {p0, p1}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->i:Landroid/widget/ProgressBar;

    .line 1355722
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 1355723
    iget-object p2, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->c:LX/8TS;

    .line 1355724
    iget-object p3, p2, LX/8TS;->j:LX/8TP;

    move-object p2, p3

    .line 1355725
    invoke-virtual {p2}, LX/8TP;->d()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1355726
    new-instance p2, Lcom/facebook/quicksilver/views/loading/GamesMessengerStartScreenCardFragment;

    invoke-direct {p2}, Lcom/facebook/quicksilver/views/loading/GamesMessengerStartScreenCardFragment;-><init>()V

    iput-object p2, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    .line 1355727
    :goto_0
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object p1

    invoke-virtual {p1}, LX/0gc;->a()LX/0hH;

    move-result-object p1

    const p2, 0x7f0d27f8

    iget-object p3, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    invoke-virtual {p1, p2, p3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p1

    invoke-virtual {p1}, LX/0hH;->b()I

    .line 1355728
    iget-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    new-instance p2, LX/8Xu;

    invoke-direct {p2, p0}, LX/8Xu;-><init>(Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;)V

    .line 1355729
    iput-object p2, p1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->l:LX/8VT;

    .line 1355730
    return-void

    .line 1355731
    :cond_0
    new-instance p2, Lcom/facebook/quicksilver/views/loading/GamesFb4aStartScreenCardFragment;

    invoke-direct {p2}, Lcom/facebook/quicksilver/views/loading/GamesFb4aStartScreenCardFragment;-><init>()V

    iput-object p2, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v2

    check-cast v2, LX/8TS;

    invoke-static {p0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object p0

    check-cast p0, LX/8TD;

    iput-object v1, p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->b:LX/1Ad;

    iput-object v2, p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->c:LX/8TS;

    iput-object v3, p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->d:LX/8TY;

    iput-object p0, p1, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->e:LX/8TD;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;LX/8Vb;LX/8TX;I)V
    .locals 4

    .prologue
    .line 1355710
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->j:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/8Xw;

    invoke-direct {v1, p0, p1, p2, p3}, LX/8Xw;-><init>(Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;LX/8Vb;LX/8TX;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1355711
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->j:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 1355712
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1355699
    invoke-super {p0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->a()V

    .line 1355700
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->c:LX/8TS;

    .line 1355701
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355702
    iget-object v1, v0, LX/8TO;->g:Ljava/lang/String;

    move-object v0, v1

    .line 1355703
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1355704
    if-eqz v1, :cond_0

    .line 1355705
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->b:LX/1Ad;

    sget-object v2, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    new-instance v2, LX/8Xv;

    invoke-direct {v2, p0}, LX/8Xv;-><init>(Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;)V

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1355706
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1355707
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->c()V

    .line 1355708
    return-void

    .line 1355709
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->e:LX/8TD;

    sget-object v2, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    const-string v3, "Invalid icon URL received: %s"

    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1355697
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1355698
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1355683
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->d:LX/8TY;

    invoke-virtual {v0}, LX/8TY;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1355684
    :cond_0
    const/4 v0, 0x0

    sget-object v1, LX/8TX;->START_SCREEN:LX/8TX;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->a$redex0(Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;LX/8Vb;LX/8TX;I)V

    .line 1355685
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->i:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    .line 1355686
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1355687
    :cond_2
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    .line 1355688
    invoke-static {v0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->n(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V

    .line 1355689
    iget-object v1, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    if-eqz v1, :cond_3

    .line 1355690
    iget-object v1, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    .line 1355691
    iget-object v3, v1, LX/8Xs;->b:[LX/8VU;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object p0, v3, v2

    .line 1355692
    invoke-interface {p0}, LX/8VU;->b()V

    .line 1355693
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1355694
    :cond_3
    iget-object v1, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->k:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 1355695
    iget-object v1, v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->k:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1355696
    :cond_4
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1355681
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1355682
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1355679
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->h:Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->e()V

    .line 1355680
    return-void
.end method

.method public final setCallbackDelegate(LX/8Sk;)V
    .locals 0

    .prologue
    .line 1355677
    iput-object p1, p0, Lcom/facebook/quicksilver/views/loading/QuicksilverCardedLoadingView;->k:LX/8Sk;

    .line 1355678
    return-void
.end method
