.class public abstract Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8VW;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:[I


# instance fields
.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/animation/AnimatorSet;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TD;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1355323
    const-class v0, LX/8VW;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1355324
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    return-void

    :array_0
    .array-data 4
        -0xdfc8a1
        -0xe2ded7
        -0xbd984e
        -0x8a6938
        -0xe3b074
        -0xbf7f01
        -0xbd984e
        -0xca871b
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1355313
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355314
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355315
    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->e:LX/0Ot;

    .line 1355316
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1355317
    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->f:LX/0Ot;

    .line 1355318
    const-class v0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1355319
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    const p1, 0x7f0310e0

    invoke-static {v0, p1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355320
    const v0, 0x7f0d2810

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355321
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance p1, LX/8Xg;

    invoke-direct {p1, p0}, LX/8Xg;-><init>(Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;)V

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1355322
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;

    const/16 v2, 0x3072

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x3074

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->e:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->f:LX/0Ot;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1355302
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TS;

    .line 1355303
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355304
    iget-object v1, v0, LX/8TO;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1355305
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1355306
    :cond_0
    :goto_0
    return-void

    .line 1355307
    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1355308
    if-eqz v0, :cond_2

    .line 1355309
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V

    .line 1355310
    :goto_1
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1355311
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 1355312
    :cond_2
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->QUICKSILVER_VIEW_ERROR:LX/8TE;

    const-string v3, "Invalid splash URL received: %s"

    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1355299
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1355300
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1355301
    :cond_0
    return-void
.end method
