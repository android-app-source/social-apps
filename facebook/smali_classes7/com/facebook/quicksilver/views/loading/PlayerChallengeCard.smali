.class public Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public a:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;

.field private c:Lcom/facebook/widget/tiles/ThreadTileView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1355614
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355615
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1355616
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355617
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1355618
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355619
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a()V

    .line 1355620
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1355621
    const-class v0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-static {v0, p0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1355622
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030770

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1355623
    const v0, 0x7f0d13db

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->b:Landroid/widget/TextView;

    .line 1355624
    const v0, 0x7f0d0946

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->c:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1355625
    const v0, 0x7f0d13dc

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->d:Landroid/widget/TextView;

    .line 1355626
    const v0, 0x7f0d13dd

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->e:Landroid/view/View;

    .line 1355627
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v0

    check-cast v0, LX/8TS;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a:LX/8TS;

    return-void
.end method


# virtual methods
.method public final a(LX/8Vb;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1355628
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082368

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p1, LX/8Vb;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1355629
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->c:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v1, p1, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1355630
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082369

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, LX/8Vb;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->a:LX/8TS;

    .line 1355631
    iget-object v5, v4, LX/8TS;->e:LX/8TO;

    move-object v4, v5

    .line 1355632
    iget-object v5, v4, LX/8TO;->c:Ljava/lang/String;

    move-object v4, v5

    .line 1355633
    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1355634
    return-void
.end method

.method public setPlayButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1355635
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/PlayerChallengeCard;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355636
    return-void
.end method
