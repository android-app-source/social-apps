.class public Lcom/facebook/quicksilver/views/loading/CircularProgressView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:J

.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:F

.field private final f:Landroid/graphics/RectF;

.field private final g:Landroid/graphics/Paint;

.field private final h:Landroid/graphics/Paint;

.field private i:I

.field public j:I

.field private k:I

.field public l:F

.field public m:F

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/animation/ValueAnimator;

.field public p:F

.field private q:F

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1355392
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1355393
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1355341
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355342
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1355394
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355395
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->a:J

    .line 1355396
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->b:F

    .line 1355397
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->c:F

    .line 1355398
    const/high16 v0, 0x43340000    # 180.0f

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->d:F

    .line 1355399
    const/high16 v0, 0x43200000    # 160.0f

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->e:F

    .line 1355400
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    .line 1355401
    iput v3, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    .line 1355402
    const/16 v0, 0x64

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    .line 1355403
    iput v2, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->l:F

    .line 1355404
    iput v2, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->m:F

    .line 1355405
    iput v2, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    .line 1355406
    iput v2, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->q:F

    .line 1355407
    iput-boolean v3, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->r:Z

    .line 1355408
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->CircularProgressView:[I

    invoke-virtual {v0, p2, v1, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1355409
    :try_start_0
    const/16 v0, 0x6

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1355410
    const/16 v2, 0x5

    const/high16 v3, -0x1000000

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1355411
    const/16 v3, 0x7

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    .line 1355412
    const/16 v4, 0x7

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->getContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x40a00000    # 5.0f

    invoke-static {v5, v6}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1355413
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1355414
    invoke-static {v0, v3}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->a(IF)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->g:Landroid/graphics/Paint;

    .line 1355415
    invoke-static {v2, v3}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->a(IF)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->h:Landroid/graphics/Paint;

    .line 1355416
    return-void

    .line 1355417
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static a(IF)Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 1355434
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 1355435
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1355436
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1355437
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1355438
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1355439
    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    const/4 v3, 0x2

    const/4 v2, -0x1

    .line 1355418
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1355419
    :goto_0
    return-void

    .line 1355420
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    .line 1355421
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1355422
    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355423
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1355424
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1355425
    new-instance v1, LX/8Xk;

    invoke-direct {v1, p0}, LX/8Xk;-><init>(Lcom/facebook/quicksilver/views/loading/CircularProgressView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355426
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1355427
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1355428
    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355429
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1355430
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1355431
    new-instance v1, LX/8Xl;

    invoke-direct {v1, p0}, LX/8Xl;-><init>(Lcom/facebook/quicksilver/views/loading/CircularProgressView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355432
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x43200000    # 160.0f
    .end array-data

    .line 1355433
    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1355373
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1355374
    :cond_0
    return-void

    .line 1355375
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 1355376
    if-eqz p1, :cond_2

    .line 1355377
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 1355378
    :cond_2
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1355379
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->q:F

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    .line 1355380
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->invalidate()V

    .line 1355381
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    if-lez v0, :cond_0

    .line 1355382
    const/high16 v0, 0x43b40000    # 360.0f

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    .line 1355383
    :cond_0
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->q:F

    .line 1355384
    iget v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    .line 1355385
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v0, 0x1

    aput v1, v2, v0

    invoke-static {v2}, Landroid/animation/ObjectAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->o:Landroid/animation/ValueAnimator;

    .line 1355386
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->q:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1355387
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->o:Landroid/animation/ValueAnimator;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355388
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->o:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1355389
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->o:Landroid/animation/ValueAnimator;

    new-instance v1, LX/8Xm;

    invoke-direct {v1, p0}, LX/8Xm;-><init>(Lcom/facebook/quicksilver/views/loading/CircularProgressView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355390
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->o:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1355391
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v7, 0x41a00000    # 20.0f

    const/high16 v6, 0x41200000    # 10.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1355364
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    const/high16 v3, 0x43b40000    # 360.0f

    iget-object v5, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1355365
    iget-boolean v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->r:Z

    if-nez v0, :cond_0

    .line 1355366
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    iget v3, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    iget-object v5, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1355367
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->p:F

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->q:F

    .line 1355368
    :goto_0
    return-void

    .line 1355369
    :cond_0
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->m:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 1355370
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->l:F

    sub-float v2, v0, v6

    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->m:F

    add-float v3, v0, v7

    iget-object v5, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1355371
    :cond_1
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->m:F

    neg-float v0, v0

    .line 1355372
    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    iget v2, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->l:F

    sub-float/2addr v2, v6

    sub-float/2addr v2, v0

    add-float v3, v0, v7

    iget-object v5, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v0, 0x2c

    const v1, -0x31f652a5

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355358
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1355359
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    .line 1355360
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    .line 1355361
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    iget v4, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->k:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1355362
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->f:Landroid/graphics/RectF;

    sub-float v5, v1, v3

    sub-float v6, v2, v3

    add-float/2addr v1, v3

    add-float/2addr v2, v3

    invoke-virtual {v4, v5, v6, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1355363
    const/16 v1, 0x2d

    const v2, 0x1b4b8806

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setIndeterminate(Z)V
    .locals 1

    .prologue
    .line 1355353
    iput-boolean p1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->r:Z

    .line 1355354
    iget-boolean v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->r:Z

    if-eqz v0, :cond_0

    .line 1355355
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->a()V

    .line 1355356
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->r:Z

    invoke-direct {p0, v0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->a(Z)V

    .line 1355357
    return-void
.end method

.method public setMax(I)V
    .locals 0

    .prologue
    .line 1355351
    iput p1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    .line 1355352
    return-void
.end method

.method public setProgress(I)V
    .locals 2

    .prologue
    .line 1355343
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    if-ne p1, v0, :cond_0

    .line 1355344
    :goto_0
    return-void

    .line 1355345
    :cond_0
    iput p1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    .line 1355346
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    if-le v0, v1, :cond_1

    .line 1355347
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->j:I

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    .line 1355348
    :cond_1
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    if-gez v0, :cond_2

    .line 1355349
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->i:I

    .line 1355350
    :cond_2
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/CircularProgressView;->b()V

    goto :goto_0
.end method
