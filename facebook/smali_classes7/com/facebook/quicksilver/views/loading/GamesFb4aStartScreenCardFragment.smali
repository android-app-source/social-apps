.class public Lcom/facebook/quicksilver/views/loading/GamesFb4aStartScreenCardFragment;
.super Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1355553
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()[I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1355554
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->e:LX/8TY;

    invoke-virtual {v0}, LX/8TY;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1355555
    new-array v0, v3, [I

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->c:I

    aput v1, v0, v2

    .line 1355556
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->c:I

    aput v1, v0, v2

    sget v1, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->b:I

    aput v1, v0, v3

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1355557
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->e:LX/8TY;

    invoke-virtual {v0}, LX/8TY;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1355558
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 1355559
    invoke-super {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->e()V

    .line 1355560
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355561
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355562
    if-eqz v0, :cond_0

    .line 1355563
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Tr;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355564
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1355565
    iget-object v2, v1, LX/8TO;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1355566
    new-instance v2, LX/8Xn;

    invoke-direct {v2, p0}, LX/8Xn;-><init>(Lcom/facebook/quicksilver/views/loading/GamesFb4aStartScreenCardFragment;)V

    .line 1355567
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1355568
    :cond_0
    :goto_0
    return-void

    .line 1355569
    :cond_1
    new-instance v3, LX/8VC;

    invoke-direct {v3}, LX/8VC;-><init>()V

    move-object v3, v3

    .line 1355570
    const-string v4, "game_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1355571
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1355572
    iget-object v4, v0, LX/8Tr;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1355573
    new-instance v4, LX/8Tq;

    invoke-direct {v4, v0, v2}, LX/8Tq;-><init>(LX/8Tr;LX/8Xn;)V

    .line 1355574
    iget-object v5, v0, LX/8Tr;->c:LX/1Ck;

    sget-object p0, LX/8TE;->PLAYER_SCORE:LX/8TE;

    iget-object p0, p0, LX/8TE;->value:Ljava/lang/String;

    invoke-virtual {v5, p0, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
