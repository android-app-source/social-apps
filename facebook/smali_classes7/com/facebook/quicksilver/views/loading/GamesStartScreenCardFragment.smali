.class public abstract Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public d:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/8TY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Tr;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private i:Landroid/support/v4/view/ViewPager;

.field public j:LX/8Xs;

.field public k:Landroid/widget/TextView;

.field public l:LX/8VT;

.field public m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Landroid/widget/TextView;

.field public o:[I

.field public p:Landroid/widget/TextView;

.field public q:Landroid/widget/ProgressBar;

.field public r:LX/0wd;

.field public s:LX/0wW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1355549
    const v0, 0x7f08234b

    sput v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->a:I

    .line 1355550
    const v0, 0x7f08234c

    sput v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->b:I

    .line 1355551
    const v0, 0x7f08234d

    sput v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->c:I

    .line 1355552
    const-class v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1355547
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1355548
    return-void
.end method

.method public static n(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V
    .locals 4

    .prologue
    .line 1355543
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->q:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1355544
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355545
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->r:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1355546
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355540
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1355541
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;

    invoke-static {p1}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v2

    check-cast v2, LX/8TS;

    invoke-static {p1}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v3

    check-cast v3, LX/8TY;

    const/16 v0, 0x307c

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p1

    check-cast p1, LX/0wW;

    iput-object v2, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    iput-object v3, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->e:LX/8TY;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->f:LX/0Or;

    iput-object p1, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->s:LX/0wW;

    .line 1355542
    return-void
.end method

.method public abstract b()[I
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public e()V
    .locals 5

    .prologue
    .line 1355481
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    if-eqz v0, :cond_1

    .line 1355482
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    .line 1355483
    iget-object v2, v0, LX/8Xs;->b:[LX/8VU;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1355484
    if-eqz v4, :cond_0

    .line 1355485
    invoke-interface {v4}, LX/8VU;->a()V

    .line 1355486
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1355487
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355488
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355489
    if-eqz v0, :cond_3

    .line 1355490
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355491
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355492
    iget-object v1, v0, LX/8TO;->g:Ljava/lang/String;

    move-object v0, v1

    .line 1355493
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1355494
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355495
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1355496
    iget-object v2, v1, LX/8TO;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1355497
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1355498
    :cond_2
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355499
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1355500
    iget-object v1, v0, LX/8TO;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1355501
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1355502
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355503
    iget-object v2, v1, LX/8TS;->e:LX/8TO;

    move-object v1, v2

    .line 1355504
    iget-object v2, v1, LX/8TO;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1355505
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1355506
    :cond_3
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x64721f4c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1355539
    const v1, 0x7f03076c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x61a07039

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1355507
    const v0, 0x7f0d13d6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->h:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1355508
    const v0, 0x7f0d13d7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->i:Landroid/support/v4/view/ViewPager;

    .line 1355509
    const v0, 0x7f0d13d3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1355510
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->b()[I

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->o:[I

    .line 1355511
    new-instance v0, LX/8Xs;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v3

    invoke-direct {v0, p0, v3}, LX/8Xs;-><init>(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    .line 1355512
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->i:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->j:LX/8Xs;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1355513
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->i:Landroid/support/v4/view/ViewPager;

    const/16 v3, 0x30

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1355514
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->i:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1355515
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->h:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v3, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1355516
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->h:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 1355517
    const v0, 0x7f0d13d2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->k:Landroid/widget/TextView;

    .line 1355518
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1355519
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->k:Landroid/widget/TextView;

    new-instance v1, LX/8Xo;

    invoke-direct {v1, p0}, LX/8Xo;-><init>(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355520
    const v0, 0x7f0d13d4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1355521
    const v0, 0x7f0d13d5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->n:Landroid/widget/TextView;

    .line 1355522
    const v4, 0x7f0d1409

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->p:Landroid/widget/TextView;

    .line 1355523
    const v4, 0x7f0d13f9

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->q:Landroid/widget/ProgressBar;

    .line 1355524
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0705

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1355525
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355526
    iget-object v5, v4, LX/8TS;->j:LX/8TP;

    move-object v4, v5

    .line 1355527
    invoke-virtual {v4}, LX/8TP;->d()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1355528
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->p:Landroid/widget/TextView;

    const v5, 0x7f08234a

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1355529
    :cond_1
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->p:Landroid/widget/TextView;

    new-instance v5, LX/8Xp;

    invoke-direct {v5, p0}, LX/8Xp;-><init>(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1355530
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b17f4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1355531
    iget-object v5, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->s:LX/0wW;

    invoke-virtual {v5}, LX/0wW;->a()LX/0wd;

    move-result-object v5

    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    const-wide/high16 v8, 0x4014000000000000L    # 5.0

    invoke-static {v6, v7, v8, v9}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v5

    int-to-double v6, v4

    invoke-virtual {v5, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->r:LX/0wd;

    .line 1355532
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->r:LX/0wd;

    new-instance v5, LX/8Xq;

    invoke-direct {v5, p0}, LX/8Xq;-><init>(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V

    invoke-virtual {v4, v5}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1355533
    iget-object v4, p0, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->d:LX/8TS;

    .line 1355534
    iget-boolean v5, v4, LX/8TS;->i:Z

    move v4, v5

    .line 1355535
    if-eqz v4, :cond_2

    .line 1355536
    invoke-static {p0}, Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;->n(Lcom/facebook/quicksilver/views/loading/GamesStartScreenCardFragment;)V

    .line 1355537
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 1355538
    goto/16 :goto_0
.end method
