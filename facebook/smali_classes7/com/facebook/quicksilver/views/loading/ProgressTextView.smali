.class public Lcom/facebook/quicksilver/views/loading/ProgressTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:I

.field public b:I

.field private c:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1355641
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1355642
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355652
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1355653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1355650
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1355651
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1355654
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1355655
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1355656
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->a:I

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ObjectAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    .line 1355657
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->a:I

    iget v1, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x14

    mul-long/2addr v0, v2

    .line 1355658
    iget-object v2, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355659
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1355660
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    new-instance v1, LX/8Xt;

    invoke-direct {v1, p0}, LX/8Xt;-><init>(Lcom/facebook/quicksilver/views/loading/ProgressTextView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355661
    iget-object v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1355662
    return-void
.end method


# virtual methods
.method public setInitialValue(I)V
    .locals 5

    .prologue
    .line 1355647
    iput p1, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    .line 1355648
    invoke-virtual {p0}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082366

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1355649
    return-void
.end method

.method public setProgress(I)V
    .locals 1

    .prologue
    .line 1355643
    iget v0, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->a:I

    if-ne p1, v0, :cond_0

    .line 1355644
    :goto_0
    return-void

    .line 1355645
    :cond_0
    iput p1, p0, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->a:I

    .line 1355646
    invoke-direct {p0}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->a()V

    goto :goto_0
.end method
