.class public final Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;
.super Lcom/facebook/quicksilver/QuicksilverFragment$ValidatedRunnable;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/8Sv;


# direct methods
.method public constructor <init>(LX/8Sv;Ljava/lang/String;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 1347476
    iput-object p1, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iput-object p2, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->b:Ljava/util/List;

    iget-object v0, p1, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-direct {p0, v0}, Lcom/facebook/quicksilver/QuicksilverFragment$ValidatedRunnable;-><init>(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1347477
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v0, v0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    sget-object v1, LX/8Su;->READY:LX/8Su;

    if-ne v0, v1, :cond_0

    .line 1347478
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v0, v0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->b:Ljava/util/List;

    .line 1347479
    invoke-static {v0, v1, v2}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/util/List;)V

    .line 1347480
    :goto_0
    return-void

    .line 1347481
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v0, v0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverFragment;->k:LX/8Su;

    sget-object v1, LX/8Su;->FAILED:LX/8Su;

    if-ne v0, v1, :cond_1

    .line 1347482
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v0, v0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch player data"

    .line 1347483
    invoke-static {v0, v1, v2}, Lcom/facebook/quicksilver/QuicksilverFragment;->a$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347484
    goto :goto_0

    .line 1347485
    :cond_1
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1347486
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v1, v1, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverFragment;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347487
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverFragment$WebViewEventListener$7;->c:LX/8Sv;

    iget-object v0, v0, LX/8Sv;->a:Lcom/facebook/quicksilver/QuicksilverFragment;

    .line 1347488
    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->r$redex0(Lcom/facebook/quicksilver/QuicksilverFragment;)V

    .line 1347489
    goto :goto_0
.end method
