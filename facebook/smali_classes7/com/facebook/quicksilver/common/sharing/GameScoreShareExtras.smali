.class public Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;
.super Lcom/facebook/quicksilver/common/sharing/GameShareExtras;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1348427
    new-instance v0, LX/8TH;

    invoke-direct {v0}, LX/8TH;-><init>()V

    sput-object v0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1348428
    invoke-direct {p0, p1}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;-><init>(Landroid/os/Parcel;)V

    .line 1348429
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1348430
    invoke-direct {p0, p1, p2}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348431
    iput-object p3, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->a:Ljava/lang/String;

    .line 1348432
    iput-object p4, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->b:Ljava/lang/String;

    .line 1348433
    return-void
.end method


# virtual methods
.method public final a()LX/8TI;
    .locals 1

    .prologue
    .line 1348434
    sget-object v0, LX/8TI;->SCORE_SHARE:LX/8TI;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1348435
    iget-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1348436
    iget-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1348437
    return-void
.end method

.method public final b(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1348438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->a:Ljava/lang/String;

    .line 1348439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->b:Ljava/lang/String;

    .line 1348440
    return-void
.end method
