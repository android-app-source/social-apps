.class public Lcom/facebook/quicksilver/common/sharing/GameChallengeCreationExtras;
.super Lcom/facebook/quicksilver/common/sharing/GameShareExtras;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/quicksilver/common/sharing/GameChallengeCreationExtras;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1348405
    new-instance v0, LX/8TF;

    invoke-direct {v0}, LX/8TF;-><init>()V

    sput-object v0, Lcom/facebook/quicksilver/common/sharing/GameChallengeCreationExtras;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1348410
    invoke-direct {p0, p1}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;-><init>(Landroid/os/Parcel;)V

    .line 1348411
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1348408
    invoke-direct {p0, p1, p2}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348409
    return-void
.end method


# virtual methods
.method public final a()LX/8TI;
    .locals 1

    .prologue
    .line 1348412
    sget-object v0, LX/8TI;->CHALLENGE_CREATION:LX/8TI;

    return-object v0
.end method

.method public final a(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1348407
    return-void
.end method

.method public final b(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1348406
    return-void
.end method
