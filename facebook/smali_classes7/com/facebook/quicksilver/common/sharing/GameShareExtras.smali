.class public abstract Lcom/facebook/quicksilver/common/sharing/GameShareExtras;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1348391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348392
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    .line 1348393
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->b:Ljava/lang/String;

    .line 1348394
    invoke-virtual {p0, p1}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->b(Landroid/os/Parcel;)V

    .line 1348395
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1348396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348397
    iput-object p1, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    .line 1348398
    iput-object p2, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->b:Ljava/lang/String;

    .line 1348399
    return-void
.end method


# virtual methods
.method public abstract a()LX/8TI;
.end method

.method public abstract a(Landroid/os/Parcel;)V
.end method

.method public abstract b(Landroid/os/Parcel;)V
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1348400
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1348401
    iget-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1348402
    iget-object v0, p0, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1348403
    invoke-virtual {p0, p1}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a(Landroid/os/Parcel;)V

    .line 1348404
    return-void
.end method
