.class public Lcom/facebook/quicksilver/QuicksilverActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;
.implements LX/0f2;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final p:LX/0ih;


# instance fields
.field public A:LX/8TS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private B:LX/8TY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TD;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Vm;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/6LN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/8Se;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private G:LX/6LO;
    .annotation runtime Lcom/facebook/quicksilver/ForQuicksilverBannerNotification;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private H:LX/0s6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/8T7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private J:LX/8Bs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;"
        }
    .end annotation
.end field

.field private L:LX/8Br;

.field public q:Lcom/facebook/quicksilver/QuicksilverFragment;

.field private r:LX/3u1;

.field public s:Landroid/support/v7/widget/RecyclerView;

.field public t:LX/8WF;

.field private u:LX/0ih;

.field private v:LX/0wM;

.field private w:Z

.field private x:LX/67X;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private z:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1347090
    sget-object v0, LX/0ig;->at:LX/0ih;

    sput-object v0, Lcom/facebook/quicksilver/QuicksilverActivity;->p:LX/0ih;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1347091
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1347092
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->w:Z

    .line 1347093
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347094
    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    .line 1347095
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347096
    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->D:LX/0Ot;

    .line 1347097
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1347098
    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    return-void
.end method

.method private static a(Landroid/content/Intent;LX/0if;)LX/0ih;
    .locals 1

    .prologue
    .line 1347099
    const-string v0, "funnel_definition"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 1347100
    if-nez v0, :cond_0

    .line 1347101
    sget-object v0, Lcom/facebook/quicksilver/QuicksilverActivity;->p:LX/0ih;

    .line 1347102
    invoke-virtual {p1, v0}, LX/0if;->a(LX/0ih;)V

    .line 1347103
    :cond_0
    return-object v0
.end method

.method private static a(LX/8TP;)LX/8Tb;
    .locals 4

    .prologue
    .line 1347104
    new-instance v0, LX/8Tb;

    .line 1347105
    iget-object v1, p0, LX/8TP;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1347106
    sget-object v2, LX/8TZ;->GAME_RECOMMENDATION:LX/8TZ;

    .line 1347107
    iget-object v3, p0, LX/8TP;->a:LX/8Ta;

    move-object v3, v3

    .line 1347108
    invoke-direct {v0, v1, v2, v3}, LX/8Tb;-><init>(Ljava/lang/String;LX/8TZ;LX/8Ta;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/quicksilver/QuicksilverActivity;LX/67X;Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;LX/0if;LX/8TS;LX/8TY;LX/0Ot;LX/0Ot;LX/6LN;LX/8Se;LX/6LO;LX/0s6;LX/8T7;LX/8Bs;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicksilver/QuicksilverActivity;",
            "LX/67X;",
            "Lcom/facebook/quicksilver/dataloader/QuicksilverComponentDataProvider;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/8TS;",
            "LX/8TY;",
            "LX/0Ot",
            "<",
            "LX/8TD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Vm;",
            ">;",
            "LX/6LN;",
            "LX/8Se;",
            "LX/6LO;",
            "LX/0s6;",
            "LX/8T7;",
            "LX/8Bs;",
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1347082
    iput-object p1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->x:LX/67X;

    iput-object p2, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    iput-object p3, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->z:LX/0if;

    iput-object p4, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    iput-object p5, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->B:LX/8TY;

    iput-object p6, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->D:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    iput-object p9, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->F:LX/8Se;

    iput-object p10, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->G:LX/6LO;

    iput-object p11, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->H:LX/0s6;

    iput-object p12, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->I:LX/8T7;

    iput-object p13, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->J:LX/8Bs;

    iput-object p14, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v14}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v1

    check-cast v1, LX/67X;

    invoke-static {v14}, LX/Jbe;->a(LX/0QB;)Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    move-result-object v2

    check-cast v2, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-static {v14}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {v14}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v4

    check-cast v4, LX/8TS;

    invoke-static {v14}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v5

    check-cast v5, LX/8TY;

    const/16 v6, 0x3072

    invoke-static {v14, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3086

    invoke-static {v14, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v14}, LX/6LN;->b(LX/0QB;)LX/6LN;

    move-result-object v8

    check-cast v8, LX/6LN;

    invoke-static {v14}, LX/8Se;->b(LX/0QB;)LX/8Se;

    move-result-object v9

    check-cast v9, LX/8Se;

    invoke-static {v14}, LX/8Sf;->a(LX/0QB;)LX/8Sf;

    move-result-object v10

    check-cast v10, LX/6LO;

    invoke-static {v14}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v11

    check-cast v11, LX/0s6;

    invoke-static {v14}, LX/8T7;->b(LX/0QB;)LX/8T7;

    move-result-object v12

    check-cast v12, LX/8T7;

    const-class v13, LX/8Bs;

    invoke-interface {v14, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/8Bs;

    const/16 v15, 0x306f

    invoke-static {v14, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/facebook/quicksilver/QuicksilverActivity;->a(Lcom/facebook/quicksilver/QuicksilverActivity;LX/67X;Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;LX/0if;LX/8TS;LX/8TY;LX/0Ot;LX/0Ot;LX/6LN;LX/8Se;LX/6LO;LX/0s6;LX/8T7;LX/8Bs;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/quicksilver/QuicksilverActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1347109
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    if-nez v0, :cond_1

    .line 1347110
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->z:LX/0if;

    invoke-static {v0, v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->a(Landroid/content/Intent;LX/0if;)LX/0ih;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    .line 1347111
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    .line 1347112
    iput-object v1, v0, LX/8TD;->f:LX/0ih;

    .line 1347113
    :goto_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    .line 1347114
    iget-object v1, v0, LX/8TD;->c:LX/8TS;

    .line 1347115
    iget-object v2, v1, LX/8TS;->f:LX/8Tb;

    move-object v1, v2

    .line 1347116
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347117
    iget-object v1, v0, LX/8TD;->c:LX/8TS;

    .line 1347118
    iget-object v2, v1, LX/8TS;->f:LX/8Tb;

    move-object v1, v2

    .line 1347119
    iget-object v2, v0, LX/8TD;->d:LX/0if;

    iget-object v3, v0, LX/8TD;->f:LX/0ih;

    sget-object v4, LX/8TE;->FUNNEL_TAG_GAME_ID:LX/8TE;

    iget-object v4, v4, LX/8TE;->value:Ljava/lang/String;

    invoke-static {v4, p1}, LX/8TD;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1347120
    iget-object v2, v0, LX/8TD;->d:LX/0if;

    iget-object v3, v0, LX/8TD;->f:LX/0ih;

    sget-object v4, LX/8TE;->FUNNEL_TAG_SOURCE:LX/8TE;

    iget-object v4, v4, LX/8TE;->value:Ljava/lang/String;

    .line 1347121
    iget-object p1, v1, LX/8Tb;->b:LX/8TZ;

    move-object v1, p1

    .line 1347122
    invoke-virtual {v1}, LX/8TZ;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, LX/8TD;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1347123
    iget-object v1, v0, LX/8TD;->c:LX/8TS;

    .line 1347124
    iget-object v2, v1, LX/8TS;->f:LX/8Tb;

    move-object v1, v2

    .line 1347125
    iget-object v2, v1, LX/8Tb;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1347126
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1347127
    const/16 v2, 0x3a

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 1347128
    iget-object v2, v0, LX/8TD;->d:LX/0if;

    iget-object v3, v0, LX/8TD;->f:LX/0ih;

    sget-object v4, LX/8TE;->FUNNEL_TAG_SOURCE_ID:LX/8TE;

    iget-object v4, v4, LX/8TE;->value:Ljava/lang/String;

    invoke-static {v4, v1}, LX/8TD;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1347129
    :cond_0
    iget-object v2, v0, LX/8TD;->d:LX/0if;

    iget-object v3, v0, LX/8TD;->f:LX/0ih;

    sget-object v1, LX/8TE;->FUNNEL_TAG_IS_MVP:LX/8TE;

    iget-object v4, v1, LX/8TE;->value:Ljava/lang/String;

    iget-object v1, v0, LX/8TD;->e:LX/8TY;

    invoke-virtual {v1}, LX/8TY;->b()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, LX/8TD;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1347130
    iget-object v1, v0, LX/8TD;->d:LX/0if;

    iget-object v2, v0, LX/8TD;->f:LX/0ih;

    sget-object v3, LX/8TE;->FUNNEL_TAG_IS_FAST_START:LX/8TE;

    iget-object v3, v3, LX/8TE;->value:Ljava/lang/String;

    iget-object v4, v0, LX/8TD;->e:LX/8TY;

    invoke-virtual {v4}, LX/8TY;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/8TD;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1347131
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_QUICKSILVER_START:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 1347132
    return-void

    .line 1347133
    :cond_1
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->z:LX/0if;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    goto/16 :goto_0

    .line 1347134
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1347135
    const v1, 0x7f08236d

    if-ne p1, v1, :cond_1

    .line 1347136
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v1, :cond_0

    .line 1347137
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    sget-object v2, LX/8TE;->FUNNEL_SOURCE_TOP_BAR:LX/8TE;

    invoke-virtual {v1, v2}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(LX/8TE;)V

    .line 1347138
    :cond_0
    :goto_0
    return v0

    .line 1347139
    :cond_1
    const v1, 0x102002c

    if-ne p1, v1, :cond_2

    .line 1347140
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->onBackPressed()V

    goto :goto_0

    .line 1347141
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)LX/8Tb;
    .locals 8

    .prologue
    .line 1347142
    const-string v0, "source"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LX/8TZ;->fromSerializable(Ljava/io/Serializable;)LX/8TZ;

    move-result-object v0

    .line 1347143
    if-nez v0, :cond_1

    .line 1347144
    sget-object v1, LX/8TZ;->UNKNOWN:LX/8TZ;

    .line 1347145
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->UNKNOWN_SOURCE:LX/8TE;

    .line 1347146
    if-nez p1, :cond_2

    .line 1347147
    const-string v3, ""

    .line 1347148
    :goto_0
    move-object v3, v3

    .line 1347149
    invoke-virtual {v0, v2, v3}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1347150
    :goto_1
    const-string v0, "source_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8Ta;

    .line 1347151
    if-nez v0, :cond_0

    .line 1347152
    sget-object v0, LX/8Ta;->None:LX/8Ta;

    .line 1347153
    :cond_0
    const-string v2, "source_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1347154
    new-instance v3, LX/8Tb;

    invoke-direct {v3, v2, v1, v0}, LX/8Tb;-><init>(Ljava/lang/String;LX/8TZ;LX/8Ta;)V

    return-object v3

    :cond_1
    move-object v1, v0

    goto :goto_1

    .line 1347155
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1347156
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 1347157
    if-eqz v5, :cond_4

    .line 1347158
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 1347159
    const-string v6, " extras { "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347160
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1347161
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p0, "="

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1347162
    :cond_3
    const-string v3, "}"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1347163
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)LX/8Tp;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1347164
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347165
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->CHALLENGE_CREATION_NO_THREAD_ID:LX/8TE;

    const-string v2, "No thread id returned from activity result"

    invoke-virtual {v0, v1, v2}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1347166
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    .line 1347167
    :goto_0
    return-object v0

    .line 1347168
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v0, :cond_1

    .line 1347169
    invoke-static {}, LX/8Vb;->b()LX/8Va;

    move-result-object v0

    .line 1347170
    iput-object p1, v0, LX/8Va;->b:Ljava/lang/String;

    .line 1347171
    move-object v0, v0

    .line 1347172
    iput-object p1, v0, LX/8Va;->a:Ljava/lang/String;

    .line 1347173
    move-object v0, v0

    .line 1347174
    invoke-virtual {v0}, LX/8Va;->a()LX/8Vb;

    move-result-object v0

    .line 1347175
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    sget-object v2, LX/8TK;->CHALLENGE_CREATION:LX/8TK;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/quicksilver/QuicksilverFragment;->a(LX/8Vb;LX/8TK;I)V

    .line 1347176
    sget-object v0, LX/8Tp;->CHALLENGE_CREATED:LX/8Tp;

    goto :goto_0

    .line 1347177
    :cond_1
    sget-object v0, LX/8Tp;->SOMETHING_WENT_WRONG:LX/8Tp;

    goto :goto_0
.end method

.method private o()V
    .locals 4

    .prologue
    const/16 v1, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1347178
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 1347179
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->v:LX/0wM;

    .line 1347180
    new-instance v0, LX/8WF;

    invoke-direct {v0}, LX/8WF;-><init>()V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->t:LX/8WF;

    .line 1347181
    new-instance v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->s:Landroid/support/v7/widget/RecyclerView;

    .line 1347182
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->s:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1347183
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->s:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->t:LX/8WF;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1347184
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->b()LX/3u1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    .line 1347185
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    invoke-virtual {v0, v3}, LX/3u1;->a(Z)V

    .line 1347186
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->s:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, LX/3u1;->a(Landroid/view/View;)V

    .line 1347187
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    invoke-virtual {v0, v3}, LX/3u1;->b(Z)V

    .line 1347188
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->B:LX/8TY;

    invoke-virtual {v0}, LX/8TY;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1347189
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->q()V

    .line 1347190
    :goto_0
    return-void

    .line 1347191
    :cond_0
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->p()V

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 1347192
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    const v1, 0x7f082364

    invoke-virtual {v0, v1}, LX/3u1;->b(I)V

    .line 1347193
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->v:LX/0wM;

    const v2, 0x7f02081a

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3u1;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1347194
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 1347195
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/3u1;->a(Ljava/lang/CharSequence;)V

    .line 1347196
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->r:LX/3u1;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->v:LX/0wM;

    const v2, 0x7f0207e4

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3u1;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1347197
    return-void
.end method

.method public static r(Lcom/facebook/quicksilver/QuicksilverActivity;)LX/8Tb;
    .locals 2

    .prologue
    .line 1347075
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1347076
    iget-object v1, v0, LX/8TS;->f:LX/8Tb;

    move-object v0, v1

    .line 1347077
    if-nez v0, :cond_0

    .line 1347078
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->c(Landroid/content/Intent;)LX/8Tb;

    move-result-object v0

    .line 1347079
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1347080
    iget-object v1, v0, LX/8TS;->j:LX/8TP;

    move-object v0, v1

    .line 1347081
    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->a(LX/8TP;)LX/8Tb;

    move-result-object v0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/quicksilver/QuicksilverActivity;)V
    .locals 2

    .prologue
    .line 1347198
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    if-nez v0, :cond_0

    .line 1347199
    :goto_0
    return-void

    .line 1347200
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_QUICKSILVER_END:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 1347201
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->z:LX/0if;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->u:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    goto :goto_0
.end method

.method private t()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1347202
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->B:LX/8TY;

    .line 1347203
    iget-object v2, v0, LX/8TY;->a:LX/0Uh;

    const/16 v3, 0x4c4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 1347204
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->H:LX/0s6;

    invoke-static {v0}, LX/36d;->a(LX/0s6;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1347205
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->QUICKSILVER_NOT_AVAILABLE:LX/8TE;

    const-string v3, "User failed master switch GK check."

    invoke-virtual {v0, v2, v3}, LX/8TD;->a(LX/8TE;Ljava/lang/String;)V

    .line 1347206
    const v0, 0x7f082365

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1347207
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->finish()V

    move v0, v1

    .line 1347208
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1347209
    const-string v0, "instant_game_player"

    return-object v0
.end method

.method public final a(II)V
    .locals 5

    .prologue
    .line 1347083
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->F:LX/8Se;

    .line 1347084
    iput p1, v0, LX/8Se;->c:I

    .line 1347085
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->F:LX/8Se;

    .line 1347086
    iput p2, v0, LX/8Se;->d:I

    .line 1347087
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->F:LX/8Se;

    invoke-virtual {v0, v1}, LX/6LN;->a(LX/6LI;)V

    .line 1347088
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverActivity$2;

    invoke-direct {v1, p0}, Lcom/facebook/quicksilver/QuicksilverActivity$2;-><init>(Lcom/facebook/quicksilver/QuicksilverActivity;)V

    const-wide/16 v2, 0xbb8

    const v4, -0x556628a1

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1347089
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1346934
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1346935
    invoke-static {p0, p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1346936
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->x:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 1346937
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->J:LX/8Bs;

    sget-object v1, LX/8Bx;->QUICKSILVER_ACTIVITY:LX/8Bx;

    invoke-virtual {v0, v1}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->L:LX/8Br;

    .line 1346938
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->L:LX/8Br;

    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->F:LX/8Se;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->G:LX/6LO;

    invoke-virtual {v0, v1, v2}, LX/6LN;->a(Ljava/util/Set;LX/6LO;)V

    .line 1346939
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    new-instance v1, LX/2th;

    invoke-direct {v1, p0}, LX/2th;-><init>(Lcom/facebook/quicksilver/QuicksilverActivity;)V

    .line 1346940
    iput-object v1, v0, LX/8TS;->p:LX/2th;

    .line 1346941
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 1346942
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 1346943
    instance-of v0, p1, Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v0, :cond_0

    .line 1346944
    check-cast p1, Lcom/facebook/quicksilver/QuicksilverFragment;

    iput-object p1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    .line 1346945
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1346946
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "app_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8TS;->b(Ljava/lang/String;)V

    .line 1346947
    :cond_0
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 1346948
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->x:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1346949
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1346950
    const v0, 0x7f040035

    const v1, 0x7f0400e4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->overridePendingTransition(II)V

    .line 1346951
    const v0, 0x7f0310e1

    invoke-virtual {p0, v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->setContentView(I)V

    .line 1346952
    invoke-direct {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->o()V

    .line 1346953
    const v0, 0x7f0d2811

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1346954
    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    .line 1346955
    iput-object v0, v1, LX/6LN;->h:Landroid/view/ViewGroup;

    .line 1346956
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1346957
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1346958
    iget-object p0, v0, LX/8TS;->e:LX/8TO;

    move-object v0, p0

    .line 1346959
    iget-object p0, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1346960
    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1346961
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1346962
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1346963
    if-eqz v0, :cond_0

    .line 1346964
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1346965
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1346966
    iget-object v1, v0, LX/8TO;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1346967
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1346968
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1346969
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1346970
    if-eqz v0, :cond_0

    .line 1346971
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1346972
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1346973
    iget-object v1, v0, LX/8TO;->g:Ljava/lang/String;

    move-object v0, v1

    .line 1346974
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1346975
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1346976
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1346977
    :cond_0
    :goto_0
    return-void

    .line 1346978
    :cond_1
    const/16 v0, 0x22b3

    if-ne p1, v0, :cond_2

    .line 1346979
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TI;->GAME_SHARE:LX/8TI;

    sget-object v3, LX/8TE;->FACEBOOK:LX/8TE;

    invoke-virtual {v0, v2, v3, v1}, LX/8TD;->a(LX/8TI;LX/8TE;Ljava/lang/String;)V

    .line 1346980
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    sget-object v1, LX/8Tp;->FB_SHARING_SUCCESSFUL:LX/8Tp;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    goto :goto_0

    .line 1346981
    :cond_2
    const/16 v0, 0x22b4

    if-ne p1, v0, :cond_3

    .line 1346982
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TI;->SCORE_SHARE:LX/8TI;

    sget-object v3, LX/8TE;->FACEBOOK:LX/8TE;

    invoke-virtual {v0, v2, v3, v1}, LX/8TD;->a(LX/8TI;LX/8TE;Ljava/lang/String;)V

    .line 1346983
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    sget-object v1, LX/8Tp;->FB_SHARING_SUCCESSFUL:LX/8Tp;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    goto :goto_0

    .line 1346984
    :cond_3
    const/16 v0, 0x22b5

    if-eq p1, v0, :cond_4

    const/16 v0, 0x22b6

    if-ne p1, v0, :cond_5

    .line 1346985
    :cond_4
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    iget-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->D:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Vm;

    invoke-virtual {v1, p3, p0}, LX/8Vm;->a(Landroid/content/Intent;Landroid/app/Activity;)LX/8Tp;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    goto :goto_0

    .line 1346986
    :cond_5
    const/16 v0, 0x22b7

    if-ne p1, v0, :cond_0

    .line 1346987
    if-eqz p3, :cond_6

    const-string v0, "challenge_creation_thread_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1346988
    :cond_6
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    invoke-direct {p0, v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->c(Ljava/lang/String;)LX/8Tp;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1346989
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1346990
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1346991
    :cond_1
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1346992
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    if-nez v0, :cond_0

    move v0, v1

    .line 1346993
    :goto_0
    return v0

    .line 1346994
    :cond_0
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->B:LX/8TY;

    invoke-virtual {v0}, LX/8TY;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1346995
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-virtual {v0, p0}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(Landroid/app/Activity;)Ljava/util/List;

    move-result-object v0

    .line 1346996
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    .line 1346997
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1346998
    iget-object v4, v3, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->d:LX/8TY;

    invoke-virtual {v4}, LX/8TY;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    instance-of v4, p0, Lcom/facebook/quicksilver/QuicksilverActivity;

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    .line 1346999
    :goto_1
    if-eqz v4, :cond_1

    .line 1347000
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f083ac3

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1347001
    new-instance v6, LX/8To;

    const v7, 0x7f083ac3

    const/4 v8, 0x0

    invoke-direct {v6, v7, v4, v8}, LX/8To;-><init>(ILjava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347002
    :cond_1
    move-object v3, v5

    .line 1347003
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1347004
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    .line 1347005
    const v5, 0x7f110001

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1347006
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1347007
    invoke-interface {v4}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1347008
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1347009
    :cond_2
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1347010
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8To;

    .line 1347011
    iget v4, v0, LX/8To;->a:I

    move v4, v4

    .line 1347012
    iget-object v5, v0, LX/8To;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1347013
    invoke-interface {p1, v1, v4, v1, v5}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1347014
    iget-object v5, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v5, v5

    .line 1347015
    if-eqz v5, :cond_3

    .line 1347016
    iget-object v5, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v0, v5

    .line 1347017
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_2

    .line 1347018
    :cond_4
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 1347019
    const v5, 0x7f0a0717

    invoke-static {p0, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    .line 1347020
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1347021
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8To;

    .line 1347022
    new-instance v8, Landroid/text/SpannableString;

    .line 1347023
    iget-object v9, v0, LX/8To;->b:Ljava/lang/String;

    move-object v9, v9

    .line 1347024
    invoke-direct {v8, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1347025
    iget-object v9, v0, LX/8To;->b:Ljava/lang/String;

    move-object v9, v9

    .line 1347026
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v6, v1, v9, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1347027
    invoke-interface {v4}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v9

    .line 1347028
    iget v10, v0, LX/8To;->a:I

    move v10, v10

    .line 1347029
    invoke-interface {v9, v2, v10, v1, v8}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v8

    .line 1347030
    iget-object v9, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v9, v9

    .line 1347031
    if-eqz v9, :cond_5

    .line 1347032
    iget-object v9, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->v:LX/0wM;

    .line 1347033
    iget-object v10, v0, LX/8To;->c:Ljava/lang/Integer;

    move-object v0, v10

    .line 1347034
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, v0, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_3

    .line 1347035
    :cond_6
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 1347036
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x57fea66b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1347037
    invoke-static {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->s(Lcom/facebook/quicksilver/QuicksilverActivity;)V

    .line 1347038
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1347039
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    .line 1347040
    const/16 v1, 0x23

    const v2, -0x77af5cac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1347041
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    if-eqz v0, :cond_0

    .line 1347042
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->y:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(ILandroid/app/Activity;Ljava/util/Map;)LX/8Tp;

    move-result-object v1

    .line 1347043
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    .line 1347044
    sget-object v0, LX/8Tp;->UNHANDLED_EVENT:LX/8Tp;

    invoke-virtual {v0, v1}, LX/8Tp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1347045
    const/4 v0, 0x1

    .line 1347046
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->b(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7aa01bea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1347047
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1347048
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1347049
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    invoke-virtual {v0}, LX/6LN;->b()V

    .line 1347050
    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1347051
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->FUNNEL_GAME_BACKGROUNDED:LX/8TE;

    invoke-virtual {v0, v2}, LX/8TD;->b(LX/8TE;)V

    .line 1347052
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->w:Z

    .line 1347053
    const/16 v0, 0x23

    const v2, -0x16d88495

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1347054
    iget-object v2, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->B:LX/8TY;

    invoke-virtual {v2}, LX/8TY;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1347055
    const v2, 0x7f08236d

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1347056
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1347057
    iget-object v4, v3, LX/8TS;->m:LX/8TV;

    move-object v3, v4

    .line 1347058
    sget-object v4, LX/8TV;->IN_GAME:LX/8TV;

    if-eq v3, v4, :cond_1

    if-nez v2, :cond_1

    .line 1347059
    const v2, 0x7f08236d

    invoke-virtual {p0}, Lcom/facebook/quicksilver/QuicksilverActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08236d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v2, v1, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 1347060
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    .line 1347061
    :cond_1
    iget-object v3, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    .line 1347062
    iget-object v4, v3, LX/8TS;->m:LX/8TV;

    move-object v3, v4

    .line 1347063
    sget-object v4, LX/8TV;->IN_GAME:LX/8TV;

    if-ne v3, v4, :cond_0

    if-eqz v2, :cond_0

    .line 1347064
    const v2, 0x7f08236d

    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1347065
    goto :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4847abed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1347066
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1347067
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1347068
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->E:LX/6LN;

    invoke-virtual {v0}, LX/6LN;->a()V

    .line 1347069
    iget-boolean v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->w:Z

    if-eqz v0, :cond_0

    .line 1347070
    iget-object v0, p0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v2, LX/8TE;->FUNNEL_GAME_FOREGROUNDED:LX/8TE;

    invoke-virtual {v0, v2}, LX/8TD;->b(LX/8TE;)V

    .line 1347071
    :cond_0
    const/16 v0, 0x23

    const v2, 0x6473e84a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2ac9e40b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1347072
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1347073
    const v1, 0x7f0400d2

    const v2, 0x7f0400e5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/quicksilver/QuicksilverActivity;->overridePendingTransition(II)V

    .line 1347074
    const/16 v1, 0x23

    const v2, 0x7b467894

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
