.class public Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public n:LX/78Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field public q:Landroid/widget/TextView;

.field public r:Landroid/widget/TextView;

.field public s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173272
    const-class v0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1173271
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    invoke-static {p0}, LX/78Z;->b(LX/0QB;)LX/78Z;

    move-result-object p0

    check-cast p0, LX/78Z;

    iput-object p0, p1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->n:LX/78Z;

    return-void
.end method

.method public static k(Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;)V
    .locals 2

    .prologue
    .line 1173204
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    const v1, 0x7f0d2849

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->p:Landroid/widget/TextView;

    .line 1173205
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->p:Landroid/widget/TextView;

    .line 1173206
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173207
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1173208
    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->A:Ljava/lang/String;

    move-object p0, v1

    .line 1173209
    move-object v1, p0

    .line 1173210
    move-object v1, v1

    .line 1173211
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173212
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1173262
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1173263
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-eqz v1, :cond_1

    .line 1173264
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1173265
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1173266
    :cond_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1173267
    :cond_1
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1173268
    invoke-virtual {v0, v2}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 1173269
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1173270
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5c9ee1ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173257
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1173258
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_0

    .line 1173259
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1173260
    const/16 v1, 0x2b

    const v2, -0x7c7468c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1173261
    :goto_0
    return-void

    :cond_0
    const v1, 0x4f5f9a26    # 3.75142144E9f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1ca9b653

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173221
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1173222
    const-class v1, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1173223
    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1173224
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1173225
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310f5

    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    .line 1173226
    :goto_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_1

    .line 1173227
    :goto_1
    const v1, 0x97f29c5

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1173228
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310f4

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    goto :goto_0

    .line 1173229
    :cond_1
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->s:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173230
    iget-object v2, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1173231
    :try_start_0
    iget-object v3, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v4, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    invoke-virtual {v4}, LX/31O;->a()Ljava/util/List;

    move-result-object v4

    const/4 v1, 0x0

    .line 1173232
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v5

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-eq v5, p1, :cond_4

    .line 1173233
    :cond_2
    const/4 v5, 0x0

    .line 1173234
    :goto_2
    move-object v3, v5

    .line 1173235
    iput-object v3, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    .line 1173236
    iget-object v3, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1173237
    :goto_3
    move-object v2, v3

    .line 1173238
    move-object v1, v2

    .line 1173239
    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->t:Ljava/util/List;

    .line 1173240
    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->k(Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;)V

    .line 1173241
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    const v2, 0x7f0d284b

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->r:Landroid/widget/TextView;

    .line 1173242
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->t:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 1173243
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->r:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1173244
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a79

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173245
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->r:Landroid/widget/TextView;

    new-instance v2, LX/78d;

    invoke-direct {v2, p0}, LX/78d;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173246
    :goto_4
    new-instance v2, LX/78c;

    invoke-direct {v2, p0}, LX/78c;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;)V

    .line 1173247
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    const v3, 0x7f0d284c

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->q:Landroid/widget/TextView;

    .line 1173248
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->q:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173249
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173250
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1173251
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->o:Landroid/view/View;

    const v3, 0x7f0d2840

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1173252
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173253
    :cond_3
    goto/16 :goto_1

    :catch_0
    const/4 v3, 0x0

    goto :goto_3

    .line 1173254
    :cond_4
    :try_start_1
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    .line 1173255
    invoke-static {v3, v5}, LX/7EJ;->d(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;)Ljava/util/List;

    move-result-object v5

    goto/16 :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1173256
    :cond_5
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackThanksDialogFragment;->r:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a17a67a    # 5.785E-4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173213
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1173214
    if-eqz v1, :cond_0

    .line 1173215
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    move v1, v1

    .line 1173216
    if-eqz v1, :cond_0

    .line 1173217
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1173218
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 1173219
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1173220
    const/16 v1, 0x2b

    const v2, -0x29844322

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
