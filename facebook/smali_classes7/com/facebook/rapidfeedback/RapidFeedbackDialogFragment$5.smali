.class public final Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 0

    .prologue
    .line 1172640
    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v2, -0x2

    const/4 v0, 0x0

    .line 1172641
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1172642
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1172643
    const/16 v2, 0x30

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1172644
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v3, LX/78U;->INTRO_COLLAPSED:LX/78U;

    if-ne v2, v3, :cond_2

    .line 1172645
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v3, v3, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v3}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;I)V

    .line 1172646
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v3, v3, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1172647
    :goto_0
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v2, v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1172648
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->z:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v3, v3, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v4, LX/78U;->EXPANDED:LX/78U;

    if-eq v3, v4, :cond_0

    const/4 v0, 0x1

    .line 1172649
    :cond_0
    iget-object v6, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v6, v2}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a(I)V

    .line 1172650
    iget-object v6, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b112f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int/2addr v8, v9

    iget v9, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    iget v10, v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    move v11, v0

    invoke-virtual/range {v6 .. v11}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a(IIIIZ)V

    .line 1172651
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v1, LX/78U;->INTRO_COLLAPSED:LX/78U;

    if-ne v0, v1, :cond_1

    .line 1172652
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    const/4 v8, 0x0

    .line 1172653
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v7, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    sub-int/2addr v6, v7

    iget v7, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    .line 1172654
    new-instance v7, Landroid/view/animation/TranslateAnimation;

    int-to-float v6, v6

    invoke-direct {v7, v8, v8, v6, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1172655
    const-wide/16 v8, 0xfa

    invoke-virtual {v7, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1172656
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1172657
    const/4 v6, 0x1

    invoke-virtual {v7, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1172658
    new-instance v6, LX/78R;

    invoke-direct {v6, v0}, LX/78R;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v7, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1172659
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v6, v6

    .line 1172660
    invoke-virtual {v6, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1172661
    :cond_1
    return-void

    .line 1172662
    :cond_2
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v3, v3, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    iget-object v4, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    const/16 v5, 0x96

    .line 1172663
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 1172664
    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    .line 1172665
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1172666
    move v6, v6

    .line 1172667
    move v4, v6

    .line 1172668
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1172669
    iput v3, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    .line 1172670
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget v3, v3, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    invoke-static {v2, v3}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;I)V

    .line 1172671
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v3, LX/7FJ;->IMPRESSION:LX/7FJ;

    invoke-virtual {v2, v3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1172672
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0
.end method
