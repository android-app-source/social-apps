.class public final Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$11$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/78M;


# direct methods
.method public constructor <init>(LX/78M;)V
    .locals 0

    .prologue
    .line 1172587
    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$11$1;->a:LX/78M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1172588
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$11$1;->a:LX/78M;

    iget-object v0, v0, LX/78M;->a:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    const/4 p0, 0x0

    .line 1172589
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1172590
    iput p0, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1172591
    iput p0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1172592
    iget-object p0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {p0, v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1172593
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1172594
    const/4 p0, -0x1

    iput p0, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1172595
    iget-object p0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {p0, v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1172596
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object p0, LX/7FJ;->IMPRESSION:LX/7FJ;

    invoke-virtual {v1, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1172597
    sget-object v1, LX/78U;->EXPANDED:LX/78U;

    iput-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    .line 1172598
    invoke-static {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    .line 1172599
    return-void
.end method
