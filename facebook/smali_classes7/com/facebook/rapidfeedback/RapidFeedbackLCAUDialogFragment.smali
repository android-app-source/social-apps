.class public Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public n:LX/78Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/view/View;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field public s:Lcom/facebook/widget/CustomLinearLayout;

.field public t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173176
    const-class v0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1173175
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;Landroid/view/View;LX/7FI;)V
    .locals 1

    .prologue
    .line 1173173
    new-instance v0, LX/78b;

    invoke-direct {v0, p0, p2}, LX/78b;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;LX/7FI;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173174
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-static {p0}, LX/78Z;->b(LX/0QB;)LX/78Z;

    move-result-object v1

    check-cast v1, LX/78Z;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object v1, p1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    iput-object p0, p1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->o:LX/0hB;

    return-void
.end method

.method public static b(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)V
    .locals 2

    .prologue
    .line 1173067
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    const v1, 0x7f0d283c

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->q:Landroid/widget/TextView;

    .line 1173068
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->q:Landroid/widget/TextView;

    .line 1173069
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173070
    iget-object p0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1173071
    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->z:Ljava/lang/String;

    move-object p0, v1

    .line 1173072
    move-object v1, p0

    .line 1173073
    move-object v1, v1

    .line 1173074
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173075
    return-void
.end method

.method public static k(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)V
    .locals 2

    .prologue
    .line 1173167
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    const v1, 0x7f0d283d

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->r:Landroid/widget/TextView;

    .line 1173168
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->u:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FA;

    .line 1173169
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->r:Landroid/widget/TextView;

    .line 1173170
    iget-object p0, v0, LX/7FA;->d:Ljava/lang/String;

    move-object v0, p0

    .line 1173171
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173172
    return-void
.end method

.method public static m(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1173162
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1173163
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1133

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1173164
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1173165
    const v1, 0x7f0a011a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1173166
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1173151
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1173152
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-eqz v1, :cond_1

    .line 1173153
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1173154
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1173155
    :cond_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1173156
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1173157
    :cond_1
    :goto_0
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1173158
    invoke-virtual {v0, v2}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 1173159
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1173160
    return-object v0

    .line 1173161
    :cond_2
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a797eab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173146
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1173147
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_0

    .line 1173148
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1173149
    const/16 v1, 0x2b

    const v2, 0x25895f5c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1173150
    :goto_0
    return-void

    :cond_0
    const v1, 0x59190314

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2635f03b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173084
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1173085
    const-class v1, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1173086
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1173087
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_0

    .line 1173088
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x79baafba

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1173089
    :cond_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1173090
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310f1

    new-instance v4, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    .line 1173091
    :goto_1
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1173092
    iget-object v2, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1173093
    :try_start_0
    iget-object v4, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v5, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    invoke-virtual {v5}, LX/31O;->a()Ljava/util/List;

    move-result-object v5

    .line 1173094
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1173095
    :cond_1
    const/4 v6, 0x0

    .line 1173096
    :goto_2
    move-object v4, v6

    .line 1173097
    iput-object v4, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    .line 1173098
    iget-object v4, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1173099
    :goto_3
    move-object v2, v4

    .line 1173100
    move-object v1, v2

    .line 1173101
    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->u:Ljava/util/List;

    .line 1173102
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->u:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1173103
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0

    .line 1173104
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310f0

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    goto :goto_1

    .line 1173105
    :cond_4
    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->b(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)V

    .line 1173106
    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->k(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)V

    .line 1173107
    const/4 v8, 0x0

    .line 1173108
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    const v2, 0x7f0d283e

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->s:Lcom/facebook/widget/CustomLinearLayout;

    .line 1173109
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7F3;

    .line 1173110
    iget-object v4, v1, LX/7F3;->a:LX/7F7;

    move-object v4, v4

    .line 1173111
    sget-object v5, LX/7F7;->QUESTION:LX/7F7;

    if-eq v4, v5, :cond_5

    .line 1173112
    check-cast v1, LX/7FB;

    .line 1173113
    new-instance v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1173114
    invoke-virtual {v1}, LX/7FB;->a()LX/7EQ;

    move-result-object v5

    .line 1173115
    iget-object v6, v5, LX/7EQ;->c:Ljava/lang/String;

    move-object v5, v6

    .line 1173116
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173117
    iget-object v5, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    invoke-virtual {v5}, LX/78Z;->a()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1173118
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-static {v5, v6}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1173119
    :goto_5
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1173120
    const v5, 0x7f0215a2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1173121
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0060

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0060

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v4, v8, v5, v8, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1173122
    new-instance v5, LX/78a;

    invoke-direct {v5, p0, v1}, LX/78a;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;LX/7FB;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173123
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->s:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->m(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1173124
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->s:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1173125
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0052

    invoke-static {v5, v6}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_5

    .line 1173126
    :cond_7
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1173127
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->s:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->m(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1173128
    :cond_8
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->n:LX/78Z;

    invoke-virtual {v1}, LX/78Z;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1173129
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    const v2, 0x7f0d2840

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1173130
    sget-object v2, LX/7FI;->CLICK_CROSS_OUT:LX/7FI;

    invoke-static {p0, v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->a(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;Landroid/view/View;LX/7FI;)V

    .line 1173131
    :goto_6
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v2, LX/7FJ;->IMPRESSION:LX/7FJ;

    invoke-virtual {v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    goto/16 :goto_0

    :catch_0
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 1173132
    :cond_9
    :try_start_1
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    .line 1173133
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1173134
    iget v7, v4, LX/7EJ;->e:I

    invoke-static {v4, v6, v7}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1173135
    invoke-virtual {v6}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->p()LX/0Px;

    move-result-object v7

    .line 1173136
    if-eqz v7, :cond_a

    .line 1173137
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    .line 1173138
    invoke-virtual {v6}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->q()LX/2uF;

    move-result-object p1

    invoke-static {v4, v7, p1}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;LX/2uF;)LX/7EQ;

    move-result-object v7

    .line 1173139
    new-instance p1, LX/7FB;

    invoke-virtual {v6}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v7, v1}, LX/7FB;-><init>(LX/7EQ;Ljava/lang/String;)V

    .line 1173140
    invoke-interface {v8, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_a
    move-object v6, v8

    .line 1173141
    goto/16 :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1173142
    :cond_b
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->p:Landroid/view/View;

    const v2, 0x7f0d283f

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1173143
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081a78

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1173144
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173145
    sget-object v2, LX/7FI;->CLICK_CLOSE_BUTTON:LX/7FI;

    invoke-static {p0, v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->a(Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;Landroid/view/View;LX/7FI;)V

    goto :goto_6
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x28c5fffc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173076
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1173077
    if-eqz v1, :cond_0

    .line 1173078
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    move v1, v1

    .line 1173079
    if-eqz v1, :cond_0

    .line 1173080
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1173081
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 1173082
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1173083
    const/16 v1, 0x2b

    const v2, 0x3e64e656

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
