.class public Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public n:Landroid/view/View;

.field public o:Landroid/widget/TextView;

.field public p:Landroid/widget/TextView;

.field public q:Lcom/facebook/widget/text/BetterEditTextView;

.field public r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

.field public final s:Landroid/view/View$OnClickListener;

.field public final t:Landroid/view/View$OnClickListener;

.field public u:LX/7F5;

.field public v:LX/7FA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173042
    const-class v0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1173039
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1173040
    new-instance v0, LX/78W;

    invoke-direct {v0, p0}, LX/78W;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->s:Landroid/view/View$OnClickListener;

    .line 1173041
    new-instance v0, LX/78X;

    invoke-direct {v0, p0}, LX/78X;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->t:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static o(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V
    .locals 1

    .prologue
    .line 1172995
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1172996
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k()V

    .line 1172997
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1173030
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1173031
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-eqz v1, :cond_1

    .line 1173032
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1173033
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1173034
    :cond_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1173035
    :cond_1
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1173036
    invoke-virtual {v0, v2}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 1173037
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1173038
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3c585ba0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173025
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1173026
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_0

    .line 1173027
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1173028
    const/16 v1, 0x2b

    const v2, 0x4c7b86c0    # 6.5936128E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1173029
    :goto_0
    return-void

    :cond_0
    const v1, 0xa916b19

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x245e7e49

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1173006
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1173007
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1173008
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310ef

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    .line 1173009
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->r:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-nez v1, :cond_0

    .line 1173010
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x5a5482cb

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1173011
    :cond_0
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    const v2, 0x7f0d2839    # 1.8763E38f

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1173012
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->v:LX/7FA;

    .line 1173013
    iget-object v2, v1, LX/7FA;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1173014
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1173015
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->v:LX/7FA;

    .line 1173016
    iget-object v3, v2, LX/7FA;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1173017
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1173018
    :cond_1
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v2, LX/78Y;

    invoke-direct {v2, p0}, LX/78Y;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1173019
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    const v2, 0x7f0d283a

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->o:Landroid/widget/TextView;

    .line 1173020
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173021
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->o:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173022
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->n:Landroid/view/View;

    const v2, 0x7f0d283b

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    .line 1173023
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackFreeformFragment;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a7d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1173024
    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6a31103c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172998
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1172999
    if-eqz v1, :cond_0

    .line 1173000
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    move v1, v1

    .line 1173001
    if-eqz v1, :cond_0

    .line 1173002
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1173003
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 1173004
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1173005
    const/16 v1, 0x2b

    const v2, -0x4c06dc0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
