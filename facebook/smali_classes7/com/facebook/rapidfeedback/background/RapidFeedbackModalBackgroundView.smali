.class public Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:LX/78g;

.field private f:LX/78g;

.field private g:LX/78i;

.field private h:Landroid/graphics/Rect;

.field private i:Landroid/graphics/Rect;

.field private j:Landroid/graphics/Rect;

.field private k:Z

.field private l:I

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1173419
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1173420
    invoke-direct {p0, p1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a(Landroid/content/Context;)V

    .line 1173421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1173416
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1173417
    invoke-direct {p0, p1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a(Landroid/content/Context;)V

    .line 1173418
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1173422
    new-instance v0, LX/78g;

    invoke-direct {v0, p1}, LX/78g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    .line 1173423
    new-instance v0, LX/78g;

    invoke-direct {v0, p1}, LX/78g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    .line 1173424
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1173402
    new-instance v0, LX/78i;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    iget v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    iget v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    sget-object v6, LX/78h;->COLLAPSE:LX/78h;

    invoke-direct/range {v0 .. v6}, LX/78i;-><init>(LX/78g;LX/78g;IIILX/78h;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    .line 1173403
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/78i;->setDuration(J)V

    .line 1173404
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    invoke-virtual {p0, v0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1173405
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1173406
    invoke-virtual {p0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 1173407
    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    .line 1173408
    iput-object v0, v1, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    .line 1173409
    invoke-virtual {p0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 1173410
    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    .line 1173411
    iput-object v0, v1, LX/78g;->a:Landroid/graphics/drawable/Drawable;

    .line 1173412
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 1173413
    iput p1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    .line 1173414
    iput p2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    .line 1173415
    return-void
.end method

.method public final a(IIIIZ)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 1173337
    iput p1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->c:I

    .line 1173338
    iput p2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    .line 1173339
    iput p3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    .line 1173340
    iput p4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    .line 1173341
    iget v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    .line 1173342
    if-eqz p5, :cond_0

    .line 1173343
    iget v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    iget v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 1173344
    iget v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    iget v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 1173345
    :goto_0
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->c:I

    iget v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    add-int/2addr v5, v1

    invoke-direct {v3, v2, v1, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173346
    new-instance v1, Landroid/graphics/Rect;

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->c:I

    iget v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    invoke-direct {v1, v2, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173347
    iget-object v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    invoke-virtual {v4, v3}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173348
    iget-object v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    .line 1173349
    iput-object v1, v3, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173350
    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    invoke-virtual {p0, v1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->removeView(Landroid/view/View;)V

    .line 1173351
    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2, v3}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1173352
    new-instance v1, Landroid/graphics/Rect;

    iget v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    sub-int v3, v0, v3

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->c:I

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173353
    new-instance v0, Landroid/graphics/Rect;

    iget v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->c:I

    iget v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173354
    iget-object v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    invoke-virtual {v3, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173355
    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    .line 1173356
    iput-object v0, v1, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173357
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    invoke-virtual {p0, v0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->removeView(Landroid/view/View;)V

    .line 1173358
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v2, v1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1173359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->m:Z

    .line 1173360
    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 1173361
    new-instance v0, LX/78i;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    iget v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->a:I

    iget v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b:I

    iget v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->d:I

    sget-object v6, LX/78h;->EXPAND:LX/78h;

    invoke-direct/range {v0 .. v6}, LX/78i;-><init>(LX/78g;LX/78g;IIILX/78h;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    .line 1173362
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/78i;->setDuration(J)V

    .line 1173363
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    new-instance v1, LX/78j;

    invoke-direct {v1, p0, p1}, LX/78j;-><init>(Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, LX/78i;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1173364
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->g:LX/78i;

    invoke-virtual {p0, v0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1173365
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1173366
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->m:Z

    if-nez v0, :cond_0

    .line 1173367
    :goto_0
    return-void

    .line 1173368
    :cond_0
    iput v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->l:I

    .line 1173369
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    .line 1173370
    iput-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173371
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    .line 1173372
    iput-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173373
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173374
    iput-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    .line 1173375
    iput-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    .line 1173376
    iput-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    .line 1173377
    iput-boolean v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->k:Z

    goto :goto_0
.end method

.method public final b(I)V
    .locals 6

    .prologue
    .line 1173378
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->m:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->l:I

    if-ne p1, v0, :cond_1

    .line 1173379
    :cond_0
    :goto_0
    return-void

    .line 1173380
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->k:Z

    if-nez v0, :cond_2

    .line 1173381
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    .line 1173382
    iget-object v1, v0, LX/78g;->c:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1173383
    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    .line 1173384
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    .line 1173385
    iget-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1173386
    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    .line 1173387
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    .line 1173388
    iget-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1173389
    iput-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    .line 1173390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->k:Z

    .line 1173391
    :cond_2
    iput p1, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->l:I

    .line 1173392
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, p1

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173393
    iput-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173394
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, p1

    iget-object v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->i:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1173395
    iput-object v1, v0, LX/78g;->b:Landroid/graphics/Rect;

    .line 1173396
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, p1

    iget-object v4, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, p1

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, LX/78g;->setDrawableBounds(Landroid/graphics/Rect;)V

    .line 1173397
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    .line 1173398
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->f:LX/78g;

    invoke-virtual {v0}, LX/78g;->invalidate()V

    goto :goto_0
.end method

.method public getUpperBound()I
    .locals 1

    .prologue
    .line 1173399
    iget-object v0, p0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->e:LX/78g;

    .line 1173400
    iget-object p0, v0, LX/78g;->c:Landroid/graphics/Rect;

    move-object v0, p0

    .line 1173401
    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method
