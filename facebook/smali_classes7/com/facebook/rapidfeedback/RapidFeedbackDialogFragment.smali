.class public Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/4nF;


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public A:I

.field public B:LX/7EX;

.field private final C:Landroid/view/ViewGroup$LayoutParams;

.field public final D:Landroid/view/View$OnClickListener;

.field public final E:Landroid/view/View$OnClickListener;

.field public final F:Ljava/lang/Runnable;

.field private G:LX/4nG;

.field public H:LX/1wz;

.field private I:Z

.field public J:Z

.field public K:I

.field private final L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public M:Z

.field public n:Landroid/graphics/Rect;

.field public o:LX/1bH;

.field public p:I

.field public q:I

.field public r:LX/78U;

.field public s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

.field public t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

.field public u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

.field public v:Lcom/facebook/structuredsurvey/views/SurveyListView;

.field private w:Landroid/widget/LinearLayout;

.field public x:Lcom/facebook/widget/text/BetterButton;

.field public y:Lcom/facebook/widget/text/BetterButton;

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1172950
    const-class v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1172943
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1172944
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->C:Landroid/view/ViewGroup$LayoutParams;

    .line 1172945
    new-instance v0, LX/78O;

    invoke-direct {v0, p0}, LX/78O;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->D:Landroid/view/View$OnClickListener;

    .line 1172946
    new-instance v0, LX/78P;

    invoke-direct {v0, p0}, LX/78P;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->E:Landroid/view/View$OnClickListener;

    .line 1172947
    new-instance v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$3;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->F:Ljava/lang/Runnable;

    .line 1172948
    new-instance v0, LX/78Q;

    invoke-direct {v0, p0}, LX/78Q;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1172949
    return-void
.end method

.method public static c(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1172927
    if-eqz p1, :cond_3

    .line 1172928
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1172929
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1172930
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->y:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 1172931
    :goto_0
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1172932
    iget-object v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1172933
    iget-object v3, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    const/4 p1, 0x0

    .line 1172934
    iget-object v0, v3, LX/31O;->c:Ljava/lang/String;

    const-string v2, "control_node"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1172935
    :cond_0
    :goto_1
    move v3, p1

    .line 1172936
    move v2, v3

    .line 1172937
    move v0, v2

    .line 1172938
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->f()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1172939
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->x:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 1172940
    :cond_1
    :goto_2
    return-void

    .line 1172941
    :cond_2
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->y:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    goto :goto_0

    .line 1172942
    :cond_3
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget v0, v3, LX/31O;->a:I

    invoke-static {v3}, LX/31O;->g(LX/31O;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_0

    const/4 p1, 0x1

    goto :goto_1
.end method

.method public static d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;I)V
    .locals 2

    .prologue
    .line 1172921
    iput p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    .line 1172922
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1172923
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v0, v1

    .line 1172924
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1172925
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v1, v0

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    div-int/lit8 v1, p1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    .line 1172926
    return-void
.end method

.method public static d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V
    .locals 4

    .prologue
    .line 1172913
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1172914
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1172915
    if-eqz p1, :cond_0

    .line 1172916
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1172917
    :cond_0
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1172918
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setVisibility(I)V

    .line 1172919
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1, v0}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1172920
    return-void
.end method

.method public static m$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 3

    .prologue
    .line 1172907
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1172908
    invoke-virtual {v0}, Landroid/app/Dialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1172909
    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/facebook/widget/text/BetterEditTextView;

    if-eqz v0, :cond_0

    .line 1172910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1172911
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1172912
    :cond_0
    return-void
.end method

.method public static t(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 4

    .prologue
    .line 1172735
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1172736
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1172737
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1172738
    new-instance v1, LX/78T;

    invoke-direct {v1, p0}, LX/78T;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1172739
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    invoke-virtual {v1, v0}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1172740
    return-void
.end method

.method public static u(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V
    .locals 6

    .prologue
    .line 1172852
    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->m$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    .line 1172853
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setVisibility(I)V

    .line 1172854
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h()V

    .line 1172855
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1172856
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d()LX/7EX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    .line 1172857
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    if-nez v0, :cond_1

    .line 1172858
    const/4 v2, 0x0

    .line 1172859
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i()V

    .line 1172860
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v1, LX/7FJ;->COMPLETE:LX/7FJ;

    invoke-virtual {v0, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;)V

    .line 1172861
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->J:Z

    if-eqz v0, :cond_3

    .line 1172862
    sget-object v0, LX/31M;->DOWN:LX/31M;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->a(LX/31M;Z)V

    .line 1172863
    :goto_0
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k()V

    .line 1172864
    :cond_0
    :goto_1
    return-void

    .line 1172865
    :cond_1
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1172866
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->F:Ljava/lang/Runnable;

    .line 1172867
    iput-object v1, v0, LX/7EX;->c:Ljava/lang/Runnable;

    .line 1172868
    :cond_2
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->c(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V

    .line 1172869
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1172870
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->d(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V

    .line 1172871
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    const/4 v2, 0x0

    .line 1172872
    invoke-virtual {v0}, LX/7EX;->getCount()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_6

    .line 1172873
    invoke-virtual {v0, v3}, LX/7EX;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7F3;

    .line 1172874
    iget-object v5, v1, LX/7F3;->a:LX/7F7;

    move-object v1, v5

    .line 1172875
    sget-object v5, LX/7F7;->QUESTION:LX/7F7;

    if-ne v1, v5, :cond_5

    .line 1172876
    add-int/lit8 v1, v3, 0x1

    if-ge v1, v4, :cond_5

    .line 1172877
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, LX/7EX;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7F3;

    .line 1172878
    iget-object v3, v1, LX/7F3;->a:LX/7F7;

    move-object v1, v3

    .line 1172879
    sget-object v3, LX/7F7;->EDITTEXT:LX/7F7;

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    .line 1172880
    :goto_3
    move v0, v1

    .line 1172881
    if-eqz v0, :cond_0

    .line 1172882
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    new-instance v1, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$9;

    invoke-direct {v1, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$9;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 1172883
    :cond_3
    sget-object v0, LX/78U;->OUTRO_COLLAPSED:LX/78U;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    .line 1172884
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/views/SurveyListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1172885
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1172886
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {v1, v0}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1172887
    invoke-static {p0, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->c(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V

    .line 1172888
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1172889
    iget-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1172890
    :try_start_0
    iget-object v2, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    .line 1172891
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1172892
    new-instance v4, LX/7F8;

    iget-object v0, v2, LX/7EJ;->c:Ljava/lang/String;

    invoke-direct {v4, v0}, LX/7F8;-><init>(Ljava/lang/String;)V

    .line 1172893
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1172894
    move-object v2, v3

    .line 1172895
    invoke-static {v1, v2}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/util/List;)LX/7EX;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1172896
    :goto_4
    move-object v1, v2

    .line 1172897
    move-object v0, v1

    .line 1172898
    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    .line 1172899
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 1172900
    :catch_0
    move-exception v2

    .line 1172901
    iget-object v3, v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    sget-object v4, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    const-string v0, "NaRF:Outro Toast Build Failed"

    invoke-virtual {v3, v4, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1172902
    invoke-virtual {v1}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    .line 1172903
    const/4 v2, 0x0

    goto :goto_4

    :cond_4
    move v1, v2

    .line 1172904
    goto :goto_3

    .line 1172905
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_2

    :cond_6
    move v1, v2

    .line 1172906
    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 1172846
    new-instance v0, LX/78V;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->H:LX/1wz;

    invoke-direct {v0, p0, v1}, LX/78V;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;LX/1wz;)V

    .line 1172847
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 1172848
    invoke-virtual {v0}, LX/78V;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->C:Landroid/view/ViewGroup$LayoutParams;

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->C:Landroid/view/ViewGroup$LayoutParams;

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 1172849
    invoke-virtual {v0}, LX/78V;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1172850
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1172851
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1172951
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b()V

    .line 1172952
    return-void
.end method

.method public final a(LX/31M;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1172830
    sget-object v0, LX/78N;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1172831
    :goto_0
    return-void

    .line 1172832
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    neg-int v1, v0

    .line 1172833
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1172834
    :goto_1
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1172835
    if-eqz p2, :cond_0

    .line 1172836
    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1172837
    :cond_0
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1172838
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1172839
    new-instance v1, LX/78L;

    invoke-direct {v1, p0}, LX/78L;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1172840
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1172841
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 1172842
    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1172843
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    .line 1172844
    :pswitch_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->q:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->p:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 1172845
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1172828
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v0, p1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b(I)V

    .line 1172829
    return-void
.end method

.method public final e_(I)V
    .locals 1

    .prologue
    .line 1172826
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    invoke-virtual {v0, p1}, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;->b(I)V

    .line 1172827
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x516e4072

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1172771
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1172772
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->M:Z

    if-nez v0, :cond_0

    .line 1172773
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1172774
    const/16 v0, 0x2b

    const v2, 0x3d931a85

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1172775
    :goto_0
    return-void

    .line 1172776
    :cond_0
    const v0, 0x7f0d2843

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    .line 1172777
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const v2, 0x7f0d2847

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyListView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    .line 1172778
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const v2, 0x7f0d2844

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->w:Landroid/widget/LinearLayout;

    .line 1172779
    const v0, 0x7f0d2842

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->t:Lcom/facebook/rapidfeedback/background/RapidFeedbackModalBackgroundView;

    .line 1172780
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const v2, 0x7f0d2845

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->x:Lcom/facebook/widget/text/BetterButton;

    .line 1172781
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    const v2, 0x7f0d2846

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->y:Lcom/facebook/widget/text/BetterButton;

    .line 1172782
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/views/SurveyListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->L:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1172783
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    iget v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->A:I

    sget-object v3, LX/03r;->RapidFeedback:[I

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1172784
    const/16 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->z:I

    .line 1172785
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1172786
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->z:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 1172787
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    .line 1172788
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1172789
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b112e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1172790
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    iget-object v3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v6

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->setPadding(IIII)V

    .line 1172791
    if-nez p1, :cond_1

    .line 1172792
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->I:Z

    if-eqz v0, :cond_3

    .line 1172793
    sget-object v0, LX/78U;->EXPANDED:LX/78U;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    .line 1172794
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d()LX/7EX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    .line 1172795
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->x:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a78

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 1172796
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->x:Lcom/facebook/widget/text/BetterButton;

    new-instance v2, LX/78S;

    invoke-direct {v2, p0}, LX/78S;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1172797
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->y:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a77

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 1172798
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->y:Lcom/facebook/widget/text/BetterButton;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1172799
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v2, LX/78U;->INTRO_COLLAPSED:LX/78U;

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {p0, v0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->c(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;Z)V

    .line 1172800
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v2, LX/78U;->EXPANDED:LX/78U;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1172801
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->F:Ljava/lang/Runnable;

    .line 1172802
    iput-object v2, v0, LX/7EX;->c:Ljava/lang/Runnable;

    .line 1172803
    :cond_2
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->v:Lcom/facebook/structuredsurvey/views/SurveyListView;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    invoke-virtual {v0, v2}, Lcom/facebook/structuredsurvey/views/SurveyListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1172804
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->s:Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;

    new-instance v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;

    invoke-direct {v2, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment$5;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/rapidfeedback/ui/RapidFeedbackPageView;->post(Ljava/lang/Runnable;)Z

    .line 1172805
    const v0, 0x40dbf405

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto/16 :goto_0

    .line 1172806
    :cond_3
    sget-object v0, LX/78U;->INTRO_COLLAPSED:LX/78U;

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    .line 1172807
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 1172808
    iget-object v2, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 1172809
    :try_start_0
    iget-object v3, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    .line 1172810
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1172811
    new-instance v5, LX/7F6;

    iget-object v6, v3, LX/7EJ;->a:Ljava/lang/String;

    iget-object v0, v3, LX/7EJ;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v0}, LX/7F6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172812
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1172813
    move-object v3, v4

    .line 1172814
    invoke-static {v2, v3}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/util/List;)LX/7EX;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1172815
    :goto_3
    move-object v2, v3

    .line 1172816
    move-object v0, v2

    .line 1172817
    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    .line 1172818
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->B:LX/7EX;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->D:Landroid/view/View$OnClickListener;

    .line 1172819
    iput-object v2, v0, LX/7EX;->b:Landroid/view/View$OnClickListener;

    .line 1172820
    goto/16 :goto_1

    .line 1172821
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 1172822
    :catch_0
    move-exception v3

    .line 1172823
    iget-object v4, v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    sget-object v5, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    const-string v6, "NaRF:Intro Toast Build Failed"

    invoke-virtual {v4, v5, v6, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1172824
    invoke-virtual {v2}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    .line 1172825
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1172767
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onAttach(Landroid/content/Context;)V

    .line 1172768
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->o:LX/1bH;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/1bH;

    if-eqz v0, :cond_0

    .line 1172769
    check-cast p1, LX/1bH;

    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->o:LX/1bH;

    .line 1172770
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x428aa52e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172759
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1172760
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1172761
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1172762
    const-string v2, "survey_theme_arg"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->A:I

    .line 1172763
    iget v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->A:I

    invoke-virtual {p0, v3, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1172764
    const-string v2, "skip_intro_toast_arg"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->I:Z

    .line 1172765
    const-string v2, "skip_outro_toast_arg"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->J:Z

    .line 1172766
    const/16 v1, 0x2b

    const v2, 0x7ed88d92

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x112b5c8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172755
    const v1, 0x7f0310f2

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1172756
    new-instance v2, LX/4nG;

    invoke-direct {v2, v1}, LX/4nG;-><init>(Landroid/view/View;)V

    iput-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->G:LX/4nG;

    .line 1172757
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->G:LX/4nG;

    invoke-virtual {v2, p0}, LX/4nG;->a(LX/4nF;)V

    .line 1172758
    const/16 v2, 0x2b

    const v3, -0x1810b6dd

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x74cda599

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172747
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1172748
    if-eqz v1, :cond_0

    .line 1172749
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mRetainInstance:Z

    move v1, v1

    .line 1172750
    if-eqz v1, :cond_0

    .line 1172751
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1172752
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 1172753
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1172754
    const/16 v1, 0x2b

    const v2, -0x7a24a984

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x739657f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1172741
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->r:LX/78U;

    sget-object v2, LX/78U;->OUTRO_COLLAPSED:LX/78U;

    if-ne v1, v2, :cond_1

    .line 1172742
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    if-eqz v1, :cond_0

    .line 1172743
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k()V

    .line 1172744
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1172745
    :cond_1
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1172746
    const/16 v1, 0x2b

    const v2, -0x7bd08972

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
