.class public Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/FbZeroRequestHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:Landroid/widget/ProgressBar;

.field public t:Landroid/view/View;

.field private u:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/4pL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215356
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1215357
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->b()V

    .line 1215358
    new-instance v1, LX/7Vq;

    invoke-direct {v1, p0}, LX/7Vq;-><init>(Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;)V

    .line 1215359
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/service/FbZeroRequestHandler;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/service/FbZeroRequestHandler;->a(LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1215360
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;

    const/16 v1, 0x13f1

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->p:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;LX/4pL;)V
    .locals 1

    .prologue
    .line 1215361
    if-nez p1, :cond_0

    .line 1215362
    invoke-virtual {p0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->finish()V

    .line 1215363
    :cond_0
    new-instance v0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity$2;-><init>(Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;LX/4pL;)V

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1215364
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1215365
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215366
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->s:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215367
    return-void
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1215368
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215369
    :goto_0
    return-object p0

    .line 1215370
    :cond_0
    const-string v0, "  "

    const-string v1, "\n\n"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1215371
    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1215372
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1215373
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f080e87

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1215374
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 1215375
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->q:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/7Vr;

    invoke-direct {v1, p0}, LX/7Vr;-><init>(Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1215376
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1215377
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215378
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215379
    const v0, 0x7f030baf

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->setContentView(I)V

    .line 1215380
    const v0, 0x7f0d1d0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1215381
    const v0, 0x7f0d1d08

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->s:Landroid/widget/ProgressBar;

    .line 1215382
    const v0, 0x7f0d1d09

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->t:Landroid/view/View;

    .line 1215383
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1215384
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->l()V

    .line 1215385
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->a()V

    .line 1215386
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x65398267

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1215387
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1215388
    iget-object v1, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1215389
    iget-object v1, p0, Lcom/facebook/zero/activity/NativeTermsAndConditionsActivity;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1215390
    :cond_0
    const/16 v1, 0x23

    const v2, 0x2b5b8656

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
