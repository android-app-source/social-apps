.class public Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/ProgressBar;

.field private w:LX/7Yb;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1215549
    const-class v0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;

    const-string v1, "time_based_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215554
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1215550
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215551
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->E:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215552
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->l()V

    .line 1215553
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1215545
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->E:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215546
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215547
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1215548
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1215555
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215556
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->E:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215557
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->m()V

    .line 1215558
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1215496
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Landroid/os/Bundle;)V

    .line 1215497
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1215498
    new-instance v1, LX/7Yb;

    invoke-direct {v1, v0}, LX/7Yb;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1215499
    const-string v2, "subtitle_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7Yb;->b:Ljava/lang/String;

    .line 1215500
    move-object v1, v1

    .line 1215501
    move-object v0, v1

    .line 1215502
    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215503
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215504
    iget-object v1, v0, LX/7YU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1215505
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215506
    const-string v0, "TimeBasedOptinInterstitialActivityNew"

    const-string v1, "Tried to show %s, but didn\'t find a campaign ID"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "TimeBasedOptinInterstitialActivityNew"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1215507
    invoke-virtual {p0}, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->finish()V

    .line 1215508
    :goto_0
    return-void

    .line 1215509
    :cond_0
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->setTheme(I)V

    .line 1215510
    const v0, 0x7f0314bf

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->setContentView(I)V

    .line 1215511
    const v0, 0x7f0d2f0c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->x:Landroid/view/View;

    .line 1215512
    const v0, 0x7f0d2f0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->y:Landroid/widget/TextView;

    .line 1215513
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->y:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215514
    iget-object v2, v1, LX/7YU;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1215515
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215516
    const v0, 0x7f0d2f0e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->z:Landroid/widget/TextView;

    .line 1215517
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->z:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215518
    iget-object v2, v1, LX/7Yb;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1215519
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215520
    const v0, 0x7f0d2f0f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->A:Landroid/widget/TextView;

    .line 1215521
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->A:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215522
    iget-object v2, v1, LX/7YU;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1215523
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215524
    const v0, 0x7f0d2f10

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    .line 1215525
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215526
    iget-object v2, v1, LX/7YU;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1215527
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215528
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215529
    iget-object v1, v0, LX/7YU;->j:Ljava/lang/String;

    move-object v0, v1

    .line 1215530
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1215531
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    new-instance v1, LX/7Vw;

    invoke-direct {v1, p0}, LX/7Vw;-><init>(Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215532
    :cond_1
    const v0, 0x7f0d2f11

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    .line 1215533
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215534
    iget-object v2, v1, LX/7YU;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1215535
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215536
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    new-instance v1, LX/7Vx;

    invoke-direct {v1, p0}, LX/7Vx;-><init>(Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215537
    const v0, 0x7f0d2f12

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    .line 1215538
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    .line 1215539
    iget-object v2, v1, LX/7YU;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1215540
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215541
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    new-instance v1, LX/7Vy;

    invoke-direct {v1, p0}, LX/7Vy;-><init>(Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215542
    const v0, 0x7f0d2f13

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->E:Landroid/widget/ProgressBar;

    .line 1215543
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->E:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215544
    const-string v0, "iorg_optin_interstitial_shown"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1215493
    sget-object v0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final o()LX/7YU;
    .locals 1

    .prologue
    .line 1215495
    iget-object v0, p0, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;->w:LX/7Yb;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
    .end annotation

    .prologue
    .line 1215494
    const-string v0, "timebased"

    return-object v0
.end method
