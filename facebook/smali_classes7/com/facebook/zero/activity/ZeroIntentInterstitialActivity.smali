.class public Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final x:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/121;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/content/Intent;

.field public v:LX/0yY;

.field public w:Z

.field private y:LX/0Yb;

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1215682
    const-class v0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;

    sput-object v0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->x:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215681
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;LX/01T;LX/121;LX/0i4;LX/0Xl;LX/03V;)V
    .locals 0

    .prologue
    .line 1215680
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->q:LX/121;

    iput-object p3, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->r:LX/0i4;

    iput-object p4, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->s:LX/0Xl;

    iput-object p5, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->t:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;

    invoke-static {v5}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v1

    check-cast v1, LX/01T;

    invoke-static {v5}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v2

    check-cast v2, LX/121;

    const-class v3, LX/0i4;

    invoke-interface {v5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {v5}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->a(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;LX/01T;LX/121;LX/0i4;LX/0Xl;LX/03V;)V

    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 1215674
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->q:LX/121;

    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->o()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/7W0;

    invoke-direct {v4, p0}, LX/7W0;-><init>(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 1215675
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    iget-object v0, v0, LX/0yY;->prefString:Ljava/lang/String;

    .line 1215676
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1215677
    if-nez v0, :cond_0

    .line 1215678
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->q:LX/121;

    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 1215679
    :cond_0
    return-void
.end method

.method public static m(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1215656
    new-instance v0, LX/7VX;

    invoke-direct {v0}, LX/7VX;-><init>()V

    .line 1215657
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    .line 1215658
    if-eqz v0, :cond_0

    .line 1215659
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1215660
    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1215661
    const-string v2, "no_warn_external"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1215662
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1215663
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 1215664
    const-string v2, "no_warn_external"

    const-string v5, "1"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1215665
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1215666
    :cond_0
    move-object v0, v0

    .line 1215667
    iget-boolean v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->w:Z

    if-eqz v1, :cond_1

    .line 1215668
    :try_start_0
    iget v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->z:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1215669
    :goto_0
    return-void

    .line 1215670
    :catch_0
    sget-object v1, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->x:Ljava/lang/Class;

    const-string v2, "Activity not found for intent: [%s]"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1215671
    :cond_1
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1215672
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->finish()V

    goto :goto_0

    .line 1215673
    :catch_1
    sget-object v1, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->x:Ljava/lang/Class;

    const-string v2, "Activity not found for intent: [%s]"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1215683
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_0

    .line 1215684
    const v0, 0x7f080e43

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1215685
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f080e0e

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1215635
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_0

    .line 1215636
    const v0, 0x7f080e48

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1215637
    :goto_0
    return-object v0

    .line 1215638
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->CHECKIN_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_1

    .line 1215639
    const v0, 0x7f080e48

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215640
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_2

    .line 1215641
    const v0, 0x7f080e45

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215642
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_3

    .line 1215643
    const v0, 0x7f080e44

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215644
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v1, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    if-ne v0, v1, :cond_4

    .line 1215645
    const v0, 0x7f080e49

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215646
    :cond_4
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_5

    .line 1215647
    const v0, 0x7f080660

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1215648
    :goto_1
    const v1, 0x7f080e0d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1215649
    :cond_5
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_6

    .line 1215650
    const v0, 0x7f080661

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1215651
    :cond_6
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_7

    .line 1215652
    const v0, 0x7f080662

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1215653
    :cond_7
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-ne v0, v1, :cond_8

    .line 1215654
    const v0, 0x7f080663

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1215655
    :cond_8
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Zero-rating isn\'t supported in the product: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->p:LX/01T;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1215631
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215632
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->r:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v3, v1, v2

    new-instance v2, LX/7W1;

    invoke-direct {v2, p0}, LX/7W1;-><init>(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1215633
    :goto_0
    return-void

    .line 1215634
    :cond_0
    invoke-static {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->m(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1215627
    iget-boolean v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->w:Z

    if-eqz v0, :cond_0

    .line 1215628
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->setResult(I)V

    .line 1215629
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->finish()V

    .line 1215630
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1215604
    if-eqz p1, :cond_0

    .line 1215605
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1215606
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215607
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215608
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->s:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    new-instance v2, LX/7Vz;

    invoke-direct {v2, p0}, LX/7Vz;-><init>(Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->y:LX/0Yb;

    .line 1215609
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->y:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1215610
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1215611
    const-string v0, "destination_intent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    .line 1215612
    const-string v0, "zero_feature_key_string"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1215613
    invoke-static {v0}, LX/0yY;->fromString(Ljava/lang/String;)LX/0yY;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    .line 1215614
    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    sget-object v3, LX/0yY;->UNKNOWN:LX/0yY;

    if-ne v2, v3, :cond_1

    .line 1215615
    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->t:LX/03V;

    sget-object v3, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->x:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing feature key extra: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215616
    sget-object v0, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    .line 1215617
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->v:LX/0yY;

    if-nez v0, :cond_4

    .line 1215618
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    if-nez v0, :cond_3

    const-string v0, "destination intent was null"

    .line 1215619
    :goto_0
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->t:LX/03V;

    sget-object v2, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->x:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215620
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->finish()V

    .line 1215621
    :goto_1
    return-void

    .line 1215622
    :cond_3
    const-string v0, "zero feature key was null"

    goto :goto_0

    .line 1215623
    :cond_4
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->u:Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 1215624
    const-string v0, "start_for_result"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->w:Z

    .line 1215625
    const-string v0, "request_code"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->z:I

    .line 1215626
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->l()V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1215599
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1215600
    const/16 v0, 0x1f2b

    if-ne p1, v0, :cond_0

    .line 1215601
    :goto_0
    return-void

    .line 1215602
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->setResult(ILandroid/content/Intent;)V

    .line 1215603
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->finish()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x730f8856

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1215595
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1215596
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->y:LX/0Yb;

    if-eqz v1, :cond_0

    .line 1215597
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroIntentInterstitialActivity;->y:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1215598
    :cond_0
    const/16 v1, 0x23

    const v2, 0x79161c71

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
