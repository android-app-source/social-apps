.class public final Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

.field public b:LX/0yh;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity;LX/0yh;)V
    .locals 0

    .prologue
    .line 1215759
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1215760
    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    .line 1215761
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity;LX/0yh;B)V
    .locals 0

    .prologue
    .line 1215762
    invoke-direct {p0, p1, p2}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;-><init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity;LX/0yh;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1215763
    if-nez p3, :cond_1

    .line 1215764
    :cond_0
    :goto_0
    return-void

    .line 1215765
    :cond_1
    const-string v1, ""

    .line 1215766
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 1215767
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    invoke-static {v4, v0}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1215768
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1215769
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " Rewrite rules: \n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1215770
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1215771
    if-eqz p2, :cond_0

    .line 1215772
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    .line 1215773
    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    invoke-static {v2, p2}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;)Ljava/lang/String;

    move-result-object v2

    .line 1215774
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1215775
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const/high16 v4, -0x10000

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/16 v4, 0x21

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1215776
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v1}, LX/0yh;->getBaseToken()LX/0yi;

    move-result-object v1

    invoke-virtual {v1}, LX/0yi;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TOKEN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 30
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v5, 0x2a

    const v6, 0x99a87df

    invoke-static {v4, v5, v6}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v23

    .line 1215777
    const v4, 0x7f03150e

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v24

    .line 1215778
    const v4, 0x7f0d2f79

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1215779
    const v5, 0x7f0d2f7a

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1215780
    const v6, 0x7f0d2f7b

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1215781
    const v7, 0x7f0d2f7c

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1215782
    const v8, 0x7f0d08c2

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1215783
    const v9, 0x7f0d2f7d

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1215784
    const v10, 0x7f0d2f7e

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1215785
    const v11, 0x7f0d2f7f

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 1215786
    const v12, 0x7f0d2f80

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1215787
    const v13, 0x7f0d2f81

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 1215788
    const v14, 0x7f0d2f82

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 1215789
    const v15, 0x7f0d2f83

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 1215790
    const v16, 0x7f0d2f85

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/EditText;

    .line 1215791
    const v17, 0x7f0d2f86

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 1215792
    const v18, 0x7f0d2f87

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 1215793
    const v19, 0x7f0d2f88

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/EditText;

    .line 1215794
    const v20, 0x7f0d2f89

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 1215795
    const v21, 0x7f0d2f8a

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 1215796
    const v22, 0x7f0d2f84

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 1215797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->q:LX/0Or;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_2

    .line 1215798
    const-string v25, "ACTIVE TOKEN"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215799
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, LX/0yh;->getTokenHashKey()LX/0Tn;

    move-result-object v25

    const-string v26, ""

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-interface {v4, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215800
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "Token Hash: "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getTokenFastHashKey()LX/0Tn;

    move-result-object v5

    const-string v25, ""

    move-object/from16 v0, v25

    invoke-interface {v4, v5, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215802
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "Fast Token Hash: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getTokenRequestTimeKey()LX/0Tn;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 1215804
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Server fetch time: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-long v0, v4

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x3e8

    mul-long v26, v26, v28

    invoke-static/range {v26 .. v27}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215805
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getLastTimeCheckedKey()LX/0Tn;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 1215806
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Client last update time: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215807
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v6, v6, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v7}, LX/0yh;->getTokenTTLKey()LX/0Tn;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1215808
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Client next update time: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v0, v6

    move-wide/from16 v26, v0

    add-long v4, v4, v26

    invoke-static {v4, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215809
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getCampaignIdKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215810
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Campaign ID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215811
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getStatusKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215812
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215813
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getRegistrationStatusKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215814
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Registration status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215815
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v5, v5, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v6}, LX/0yh;->getCarrierNameKey()LX/0Tn;

    move-result-object v6

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1215816
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v6, v6, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v7}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1215817
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Carrier name: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ID: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215818
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v5, v5, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0df;->c:LX/0Tn;

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1215819
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stored MccMnc: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215820
    const-string v5, "registered"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1215821
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getUnregisteredReasonKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215822
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unregistered Reason: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215823
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1215824
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getRewriteRulesKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215825
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1215826
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v5, v5, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->r:LX/1p3;

    invoke-virtual {v5, v4}, LX/1p3;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1215827
    :cond_0
    :goto_2
    new-instance v4, LX/7W2;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2}, LX/7W2;-><init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Landroid/widget/TextView;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1215828
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a$redex0(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0Px;)V

    .line 1215829
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v5}, LX/0yh;->getBackupRewriteRulesKey()LX/0Tn;

    move-result-object v5

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215830
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1215831
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v5, v5, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->r:LX/1p3;

    invoke-virtual {v5, v4}, LX/1p3;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->d:LX/0Px;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1215832
    :cond_1
    :goto_3
    new-instance v4, LX/7W3;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v4, v0, v1, v2}, LX/7W3;-><init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Landroid/widget/TextView;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1215833
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->d:LX/0Px;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1, v4, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a$redex0(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0Px;)V

    .line 1215834
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v5, v5, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->s:LX/0yU;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    invoke-virtual {v6}, LX/0yh;->getBaseToken()LX/0yi;

    move-result-object v6

    invoke-virtual {v6}, LX/0yi;->getUIFeaturesKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0yU;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Ljava/util/Set;)Ljava/util/Set;

    .line 1215835
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v4, v4, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->u:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1215836
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    const-string v5, "Error deserializing ui features"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1215837
    :goto_4
    const v4, 0x45ae6410

    move/from16 v0, v23

    invoke-static {v4, v0}, LX/02F;->f(II)V

    return-object v24

    .line 1215838
    :cond_2
    const-string v25, "INACTIVE TOKEN"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1215839
    :cond_3
    const/16 v4, 0x8

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1215840
    :catch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    const-string v5, "Error deserializing re-write rules"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 1215841
    :catch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    const-string v5, "Error deserializing backup re-write rules"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 1215842
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " Features: \n"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n"

    invoke-static {v5}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a:Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    iget-object v6, v6, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->u:Ljava/util/Set;

    invoke-virtual {v5, v6}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method
