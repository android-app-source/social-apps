.class public Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private am:Lcom/facebook/resources/ui/FbTextView;

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1215212
    const-class v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;

    const-string v1, "messenger_optin_interstitial_old"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215211
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1215194
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->setTheme(I)V

    .line 1215195
    const v0, 0x7f030afc

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->setContentView(I)V

    .line 1215196
    const v0, 0x7f0d0c72

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->H:Landroid/widget/ProgressBar;

    .line 1215197
    const v0, 0x7f0d0c7b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1215198
    const v0, 0x7f0d0c73

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->K:Landroid/view/ViewGroup;

    .line 1215199
    const v0, 0x7f0d0c74

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->L:Lcom/facebook/resources/ui/FbTextView;

    .line 1215200
    const v0, 0x7f0d0c75

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1215201
    const v0, 0x7f0d0c76

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->M:Lcom/facebook/resources/ui/FbTextView;

    .line 1215202
    const v0, 0x7f0d1bc7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->R:Lcom/facebook/resources/ui/FbTextView;

    .line 1215203
    const v0, 0x7f0d0c78

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->I:Landroid/widget/LinearLayout;

    .line 1215204
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215205
    const v0, 0x7f0d0c7c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->J:Lcom/facebook/resources/ui/FbButton;

    .line 1215206
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1215207
    const-string v1, "ref"

    const-string v2, "dialtone_optin_screen"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215208
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/7Vf;

    invoke-direct {v2, p0, v0}, LX/7Vf;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215209
    const v0, 0x7f0d0c7d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    .line 1215210
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1215149
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->b()V

    .line 1215150
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1215151
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1215152
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1215153
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1215154
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    move v0, v1

    .line 1215155
    :cond_0
    iget-object v3, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215156
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1215157
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215158
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215159
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1215160
    :cond_1
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215161
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1215162
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215163
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215164
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/7Vg;

    invoke-direct {v3, p0}, LX/7Vg;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215165
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1215166
    :cond_2
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1215167
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1215168
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1215169
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215170
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1215171
    :goto_1
    if-eqz v1, :cond_4

    .line 1215172
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1215173
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 1215174
    goto/16 :goto_0

    .line 1215175
    :cond_4
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1215191
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->b(Landroid/os/Bundle;)V

    .line 1215192
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215193
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 1215190
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1215179
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215180
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215181
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1215182
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215183
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215184
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/7Vh;

    invoke-direct {v2, p0}, LX/7Vh;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215185
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->am:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215186
    const/4 v0, 0x1

    .line 1215187
    :goto_0
    if-eqz v0, :cond_0

    .line 1215188
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215189
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1215178
    sget-object v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1215176
    sget-object v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityOld;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1215177
    return-void
.end method
