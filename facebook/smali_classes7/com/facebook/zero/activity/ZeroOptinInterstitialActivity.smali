.class public Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final s:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0yV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Ux;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Or;
    .annotation runtime Lcom/facebook/zero/capping/IsMessageCapEligibleGK;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Landroid/widget/ProgressBar;

.field public I:Landroid/widget/LinearLayout;

.field public J:Lcom/facebook/resources/ui/FbButton;

.field public K:Landroid/view/ViewGroup;

.field public L:Lcom/facebook/resources/ui/FbTextView;

.field public M:Lcom/facebook/resources/ui/FbTextView;

.field public N:Landroid/widget/ScrollView;

.field public O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public P:Lcom/facebook/resources/ui/FbTextView;

.field public Q:Lcom/facebook/fbui/facepile/FacepileView;

.field public R:Lcom/facebook/resources/ui/FbTextView;

.field public S:Ljava/lang/String;

.field public T:Ljava/lang/String;

.field public U:Ljava/lang/String;

.field public V:Landroid/net/Uri;

.field public W:Z

.field public X:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public Y:Ljava/lang/String;

.field public Z:Ljava/lang/String;

.field public aa:Ljava/lang/String;

.field public ab:Landroid/net/Uri;

.field public ac:Ljava/lang/String;

.field public ad:Ljava/lang/String;

.field public ae:Ljava/lang/String;

.field public af:Ljava/lang/String;

.field public ag:Ljava/lang/String;

.field public ah:Ljava/lang/String;

.field public ai:Ljava/lang/String;

.field public aj:Ljava/lang/String;

.field public ak:Z

.field public al:Ljava/lang/String;

.field private p:Lcom/facebook/resources/ui/FbButton;

.field public t:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0yP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/64L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1214204
    const-class v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    const-string v1, "zero_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1214301
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/7YV;)V
    .locals 1

    .prologue
    .line 1214302
    iget-object v0, p1, LX/7YU;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1214303
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->S:Ljava/lang/String;

    .line 1214304
    iget-object v0, p1, LX/7YV;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1214305
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    .line 1214306
    iget-object v0, p1, LX/7YU;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1214307
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->U:Ljava/lang/String;

    .line 1214308
    iget-object v0, p1, LX/7YV;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1214309
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    .line 1214310
    iget-boolean v0, p1, LX/7YV;->e:Z

    move v0, v0

    .line 1214311
    iput-boolean v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->W:Z

    .line 1214312
    iget-object v0, p1, LX/7YV;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1214313
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    .line 1214314
    iget-object v0, p1, LX/7YV;->g:LX/0Px;

    move-object v0, v0

    .line 1214315
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->X:LX/0Px;

    .line 1214316
    iget-object v0, p1, LX/7YU;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1214317
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Z:Ljava/lang/String;

    .line 1214318
    iget-object v0, p1, LX/7YU;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1214319
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    .line 1214320
    iget-object v0, p1, LX/7YU;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1214321
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ab:Landroid/net/Uri;

    .line 1214322
    iget-object v0, p1, LX/7YU;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1214323
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    .line 1214324
    iget-object v0, p1, LX/7YV;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1214325
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ad:Ljava/lang/String;

    .line 1214326
    iget-object v0, p1, LX/7YV;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1214327
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ae:Ljava/lang/String;

    .line 1214328
    iget-object v0, p1, LX/7YU;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1214329
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->af:Ljava/lang/String;

    .line 1214330
    iget-object v0, p1, LX/7YU;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1214331
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    .line 1214332
    iget-object v0, p1, LX/7YV;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1214333
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ah:Ljava/lang/String;

    .line 1214334
    iget-object v0, p1, LX/7YV;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1214335
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ai:Ljava/lang/String;

    .line 1214336
    iget-object v0, p1, LX/7YU;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1214337
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aj:Ljava/lang/String;

    .line 1214338
    iget-boolean v0, p1, LX/7YV;->l:Z

    move v0, v0

    .line 1214339
    iput-boolean v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ak:Z

    .line 1214340
    iget-object v0, p1, LX/7YU;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1214341
    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->al:Ljava/lang/String;

    .line 1214342
    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;LX/0Xl;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Lcom/facebook/content/SecureContextHelper;LX/0yP;LX/0yI;LX/64L;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;LX/0Ot;LX/0Or;LX/0Ot;LX/0Uh;LX/17Y;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;",
            "LX/0Xl;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            "LX/0yI;",
            "LX/64L;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0yV;",
            "LX/0Ot",
            "<",
            "LX/7Ux;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1214343
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->t:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->u:LX/0Xl;

    iput-object p3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->v:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p4, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->w:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->x:LX/0yP;

    iput-object p6, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->y:LX/0yI;

    iput-object p7, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->z:LX/64L;

    iput-object p8, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->B:LX/0yV;

    iput-object p10, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->C:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->D:LX/0Or;

    iput-object p12, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->E:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->F:LX/0Uh;

    iput-object p14, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->G:LX/17Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    invoke-static {v14}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {v14}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-static {v14}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v14}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v14}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v5

    check-cast v5, LX/0yP;

    invoke-static {v14}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v6

    check-cast v6, LX/0yI;

    invoke-static {v14}, LX/64L;->b(LX/0QB;)LX/64L;

    move-result-object v7

    check-cast v7, LX/64L;

    invoke-static {v14}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v14}, LX/0yV;->b(LX/0QB;)LX/0yV;

    move-result-object v9

    check-cast v9, LX/0yV;

    const/16 v10, 0x38d9

    invoke-static {v14, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x15b0

    invoke-static {v14, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0xbc

    invoke-static {v14, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v14}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {v14}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v14

    check-cast v14, LX/17Y;

    invoke-static/range {v0 .. v14}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;LX/0Xl;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;Lcom/facebook/content/SecureContextHelper;LX/0yP;LX/0yI;LX/64L;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;LX/0Ot;LX/0Or;LX/0Ot;LX/0Uh;LX/17Y;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214344
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->q()V

    .line 1214345
    new-instance v0, LX/7WA;

    invoke-direct {v0, p0, p5, p3, p4}, LX/7WA;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214346
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->z:LX/64L;

    invoke-virtual {v1, v0}, LX/64L;->a(LX/64J;)V

    .line 1214347
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->z:LX/64L;

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, LX/64L;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1214348
    return-void
.end method

.method public static a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;)V
    .locals 4

    .prologue
    .line 1214349
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1214350
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "title_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214351
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "subtitle_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214352
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "description_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214353
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "image_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214354
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "should_use_default_image_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->e()Z

    move-result v2

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 1214355
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "facepile_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214356
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "terms_and_conditions_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214357
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "clickable_link_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->i()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214358
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "clickable_link_url_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->j()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214359
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "primary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214360
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "primary_button_intent_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214361
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "primary_button_step_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214362
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "primary_button_action_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214363
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "secondary_button_text_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214364
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "secondary_button_intent_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->p()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214365
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "secondary_button_step_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->q()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214366
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "secondary_button_action_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214367
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "secondary_button_override_back_only_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->s()Z

    move-result v2

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 1214368
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "campaign_token_to_refresh_type_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->t()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1214369
    :try_start_0
    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "facepile_profile_picture_urls_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->B:LX/0yV;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->g()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0yV;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1214370
    :goto_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1214371
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->B:LX/0yV;

    invoke-static {v0, v1}, LX/7YV;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)LX/7YV;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(LX/7YV;)V

    .line 1214372
    return-void

    .line 1214373
    :catch_0
    move-exception v0

    .line 1214374
    const-string v2, "ZeroOptinInterstitialActivity"

    const-string v3, "Failed to write zero optin facepile URLs to shared prefs"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p0    # Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214375
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214376
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->G:LX/17Y;

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1214377
    if-nez v0, :cond_0

    .line 1214378
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1214379
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214380
    :cond_0
    if-eqz p2, :cond_1

    .line 1214381
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1214382
    :cond_1
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1214383
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->w:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1214384
    :cond_2
    return-void
.end method

.method public static s(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1214385
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->H:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214386
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->o()V

    .line 1214387
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->b()V

    .line 1214388
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->l()V

    .line 1214389
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->m()V

    .line 1214390
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    if-nez v0, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    if-nez v0, :cond_6

    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    if-nez v0, :cond_6

    .line 1214391
    :cond_5
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/7YV;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1214392
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->finish()V

    .line 1214393
    :goto_0
    return-void

    .line 1214394
    :cond_6
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "iorg_optin_interstitial_shown"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1214395
    const-string v0, "caller_context"

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->n()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1214396
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method private t()V
    .locals 5

    .prologue
    .line 1214397
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;

    invoke-direct {v1, p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1214398
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1214399
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->setTheme(I)V

    .line 1214400
    const v0, 0x7f031668

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->setContentView(I)V

    .line 1214401
    const v0, 0x7f0d0c72

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->H:Landroid/widget/ProgressBar;

    .line 1214402
    const v0, 0x7f0d0c73

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    .line 1214403
    const v0, 0x7f0d0c74

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    .line 1214404
    const v0, 0x7f0d0c76

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    .line 1214405
    const v0, 0x7f0d0c77

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    .line 1214406
    const v0, 0x7f0d0c7b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1214407
    const v0, 0x7f0d0c7a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    .line 1214408
    const v0, 0x7f0d0c79

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1214409
    const v0, 0x7f0d2f0b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    .line 1214410
    const v0, 0x7f0d0c78

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    .line 1214411
    const v0, 0x7f0d31da    # 1.8768E38f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    .line 1214412
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7W5;

    invoke-direct {v1, p0}, LX/7W5;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214413
    const v0, 0x7f0d0c7c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    .line 1214414
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7W6;

    invoke-direct {v1, p0}, LX/7W6;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214415
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 1214283
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "optin_interstitial_back_pressed"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1214284
    const-string v0, "caller_context"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1214285
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1214286
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 1214287
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1214288
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214289
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->S:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214290
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->S:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214291
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->S:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214292
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1214293
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214294
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->U:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1214295
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214296
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214297
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214298
    :goto_1
    if-eqz v1, :cond_0

    .line 1214299
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1214300
    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1214275
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1214276
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1214277
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a()V

    .line 1214278
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->F:LX/0Uh;

    const/16 v1, 0x2e2

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1214279
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->A:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->B:LX/0yV;

    invoke-static {v0, v1}, LX/7YV;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)LX/7YV;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(LX/7YV;)V

    .line 1214280
    invoke-static {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    .line 1214281
    :goto_0
    return-void

    .line 1214282
    :cond_0
    const-string v1, "0"

    const-string v2, ""

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214273
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ad:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ae:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->af:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->al:Ljava/lang/String;

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1214274
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214271
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ah:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ai:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aj:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->al:Ljava/lang/String;

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1214272
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 1214268
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->t()V

    .line 1214269
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1214270
    return-void
.end method

.method public l()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1214240
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1214241
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1214242
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1214243
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214244
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    move v0, v1

    .line 1214245
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214246
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1214247
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214248
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214249
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1214250
    :cond_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v3, v5}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1214251
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->X:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1214252
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->X:LX/0Px;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1214253
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    move v0, v1

    .line 1214254
    :cond_1
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214255
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Z:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1214256
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Z:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214257
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Z:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214258
    new-instance v0, LX/7WB;

    invoke-direct {v0, p0}, LX/7WB;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V

    .line 1214259
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-static {v3, v4, v6, v6, v0}, LX/7Gw;->a(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    .line 1214260
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->R:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1214261
    :cond_2
    if-eqz v0, :cond_3

    .line 1214262
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1214263
    :cond_3
    return-void

    .line 1214264
    :cond_4
    iget-boolean v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->W:Z

    if-eqz v0, :cond_5

    .line 1214265
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v6, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214266
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    move v0, v1

    .line 1214267
    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method public m()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 1214226
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1214227
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1214228
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214229
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1214230
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214231
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    move v0, v1

    .line 1214232
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1214233
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ak:Z

    if-nez v3, :cond_0

    .line 1214234
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1214235
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214236
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    move v0, v1

    .line 1214237
    :cond_0
    if-eqz v0, :cond_1

    .line 1214238
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1214239
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1214225
    sget-object v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 1214224
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1214219
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->n()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214220
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ak:Z

    if-eqz v0, :cond_2

    .line 1214221
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->e(Landroid/os/Bundle;)V

    .line 1214222
    :goto_0
    return-void

    .line 1214223
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->finish()V

    goto :goto_0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 1214218
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1214209
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1214210
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1214211
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1214212
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1214213
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    .line 1214214
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1214215
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->p()V

    .line 1214216
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->H:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214217
    return-void
.end method

.method public final r()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1214205
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1214206
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ab:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214207
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1214208
    return-object v0
.end method
