.class public final Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;)V
    .locals 0

    .prologue
    .line 1215917
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;->a:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1215918
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;->a:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->t:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->OPTIN:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1215919
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity$5;->a:Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->u:LX/0Xl;

    const-string v1, "com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 1215920
    return-void
.end method
