.class public Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private am:Lcom/facebook/resources/ui/FbTextView;

.field private an:Lcom/facebook/resources/ui/FbTextView;

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1214466
    const-class v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;

    const-string v1, "dialtone_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1214467
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1214465
    iput-object p1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->r:Landroid/content/res/Resources;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->a(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1214468
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->setTheme(I)V

    .line 1214469
    const v0, 0x7f030411

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->setContentView(I)V

    .line 1214470
    const v0, 0x7f0d0c72

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->H:Landroid/widget/ProgressBar;

    .line 1214471
    const v0, 0x7f0d0c73

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    .line 1214472
    const v0, 0x7f0d0c74

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->L:Lcom/facebook/resources/ui/FbTextView;

    .line 1214473
    const v0, 0x7f0d0c75

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->am:Lcom/facebook/resources/ui/FbTextView;

    .line 1214474
    const v0, 0x7f0d0c76

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->M:Lcom/facebook/resources/ui/FbTextView;

    .line 1214475
    const v0, 0x7f0d0c77

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    .line 1214476
    const v0, 0x7f0d0c7a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    .line 1214477
    const v0, 0x7f0d0c79

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1214478
    const v0, 0x7f0d0c7b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1214479
    const v0, 0x7f0d0c78

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    .line 1214480
    const v0, 0x7f0d0c7c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    .line 1214481
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1214482
    const-string v1, "ref"

    const-string v2, "dialtone_optin_screen"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214483
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/7VH;

    invoke-direct {v2, p0, v0}, LX/7VH;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214484
    const v0, 0x7f0d0c7d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    .line 1214485
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1214453
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->b()V

    .line 1214454
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1214455
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->am:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214456
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1214457
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->am:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214458
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->am:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->T:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214459
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->am:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214460
    :goto_1
    if-eqz v1, :cond_1

    .line 1214461
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1214462
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 1214463
    goto :goto_0

    .line 1214464
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1214450
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->b(Landroid/os/Bundle;)V

    .line 1214451
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1214452
    return-void
.end method

.method public final l()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1214432
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0, v6}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1214433
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214434
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1214435
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/47x;

    iget-object v4, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->r:Landroid/content/res/Resources;

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    iget-object v4, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v3

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v5}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->aa:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v3

    invoke-virtual {v3}, LX/47x;->a()LX/47x;

    move-result-object v3

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214436
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214437
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/7VI;

    invoke-direct {v3, p0}, LX/7VI;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214438
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->P:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    move v0, v1

    .line 1214439
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1214440
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1214441
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->V:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214442
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->O:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    move v0, v1

    .line 1214443
    :cond_0
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v3, v6}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1214444
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->X:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1214445
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->X:LX/0Px;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1214446
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->Q:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1214447
    :goto_1
    if-eqz v1, :cond_1

    .line 1214448
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->N:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1214449
    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final m()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 1214416
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1214417
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1214418
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214419
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1214420
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214421
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->J:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    move v0, v1

    .line 1214422
    :goto_0
    iget-object v3, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214423
    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1214424
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214425
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->ag:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214426
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/7VJ;

    invoke-direct {v3, p0}, LX/7VJ;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214427
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->an:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1214428
    :goto_1
    if-eqz v1, :cond_0

    .line 1214429
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;->I:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1214430
    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1214431
    sget-object v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method
