.class public Lcom/facebook/zero/activity/ZeroInternStatusActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1p3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0yU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Landroid/support/v4/view/ViewPager;

.field public u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215871
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1215872
    return-void
.end method

.method public static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1215868
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1215869
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1215870
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd/MM/yyyy hh:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1215865
    invoke-static {p1}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1215867
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Matcher:  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nReplacer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 1215873
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->u:Ljava/util/Set;

    return-object p1
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1p3;LX/0yU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/activity/ZeroInternStatusActivity;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;",
            "LX/1p3;",
            "LX/0yU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1215866
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->q:LX/0Or;

    iput-object p3, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->r:LX/1p3;

    iput-object p4, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->s:LX/0yU;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v1, 0x13c1

    invoke-static {v2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, LX/1p3;->b(LX/0QB;)LX/1p3;

    move-result-object v1

    check-cast v1, LX/1p3;

    invoke-static {v2}, LX/0yU;->b(LX/0QB;)LX/0yU;

    move-result-object v2

    check-cast v2, LX/0yU;

    invoke-static {p0, v0, v3, v1, v2}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Lcom/facebook/zero/activity/ZeroInternStatusActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1p3;LX/0yU;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1215850
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215851
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215852
    const v0, 0x7f031665

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->setContentView(I)V

    .line 1215853
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1215854
    invoke-static {}, LX/0yh;->values()[LX/0yh;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1215855
    new-instance v6, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    invoke-direct {v6, p0, v5}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;-><init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity;LX/0yh;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1215856
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1215857
    :cond_0
    const v0, 0x7f0d15b3

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->t:Landroid/support/v4/view/ViewPager;

    .line 1215858
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->t:Landroid/support/v4/view/ViewPager;

    new-instance v3, LX/7W4;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    invoke-direct {v3, p0, v4, v2}, LX/7W4;-><init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity;LX/0gc;Ljava/util/List;)V

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1215859
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1215860
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    .line 1215861
    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->b:LX/0yh;

    iget-object v3, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->q:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    if-ne v0, v3, :cond_1

    .line 1215862
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternStatusActivity;->t:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1215863
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1215864
    :cond_2
    return-void
.end method
