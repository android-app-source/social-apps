.class public abstract Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/7VG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1214555
    const-class v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1214556
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1214557
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1214558
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214559
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 1214560
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1214561
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1214562
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1214563
    :goto_0
    return-void

    .line 1214564
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7VG;LX/0Sh;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/03V;)V
    .locals 0

    .prologue
    .line 1214565
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r:LX/7VG;

    iput-object p3, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->s:LX/0Sh;

    iput-object p4, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->u:LX/0Zb;

    iput-object p6, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->v:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;

    invoke-static {v6}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v6}, LX/7VG;->a(LX/0QB;)LX/7VG;

    move-result-object v2

    check-cast v2, LX/7VG;

    invoke-static {v6}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {v0 .. v6}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7VG;LX/0Sh;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/03V;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ZeroOptinStateValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214566
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->o()LX/7YU;

    move-result-object v0

    .line 1214567
    iget-object v1, v0, LX/7YU;->p:Ljava/lang/String;

    move-object v0, v1

    .line 1214568
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r:LX/7VG;

    new-instance v2, LX/7WC;

    invoke-direct {v2, p0, p3, p4}, LX/7WC;-><init>(Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214569
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 1214570
    :cond_0
    sget-object v4, LX/7VG;->a:Ljava/lang/Class;

    const-string p0, "NOT setting optin state: provided %s was null"

    const/4 v3, 0x1

    new-array p3, v3, [Ljava/lang/Object;

    const/4 p4, 0x0

    if-nez p1, :cond_1

    const-string v3, "optin flow type"

    :goto_0
    aput-object v3, p3, p4

    invoke-static {v4, p0, p3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214571
    :goto_1
    return-void

    .line 1214572
    :cond_1
    const-string v3, "optin state"

    goto :goto_0

    .line 1214573
    :cond_2
    iget-object v3, v1, LX/7VG;->b:LX/1pA;

    invoke-virtual {v3}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v3

    .line 1214574
    new-instance v4, LX/4KV;

    invoke-direct {v4}, LX/4KV;-><init>()V

    .line 1214575
    const-string p0, "optin_flow_type"

    invoke-virtual {v4, p0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214576
    move-object v4, v4

    .line 1214577
    const-string p0, "optin_state"

    invoke-virtual {v4, p0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214578
    move-object v4, v4

    .line 1214579
    iget-object p0, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object p0, p0

    .line 1214580
    iget-object p3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object p0, p3

    .line 1214581
    const-string p3, "carrier_mcc"

    invoke-virtual {v4, p3, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214582
    move-object v4, v4

    .line 1214583
    iget-object p0, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object p0, p0

    .line 1214584
    iget-object p3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object p0, p3

    .line 1214585
    const-string p3, "carrier_mnc"

    invoke-virtual {v4, p3, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214586
    move-object v4, v4

    .line 1214587
    iget-object p0, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object p0, p0

    .line 1214588
    iget-object p3, p0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object p0, p3

    .line 1214589
    const-string p3, "sim_mcc"

    invoke-virtual {v4, p3, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214590
    move-object v4, v4

    .line 1214591
    iget-object p0, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v3, p0

    .line 1214592
    iget-object p0, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v3, p0

    .line 1214593
    const-string p0, "sim_mnc"

    invoke-virtual {v4, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214594
    move-object v3, v4

    .line 1214595
    iget-object v4, v1, LX/7VG;->b:LX/1pA;

    invoke-virtual {v4}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v4

    .line 1214596
    const-string p0, "network_interface"

    invoke-virtual {v3, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214597
    move-object v3, v3

    .line 1214598
    new-instance v4, LX/7XG;

    invoke-direct {v4}, LX/7XG;-><init>()V

    move-object v4, v4

    .line 1214599
    const-string p0, "input"

    invoke-virtual {v4, p0, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1214600
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1214601
    iget-object v4, v1, LX/7VG;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1214602
    new-instance v4, LX/7VF;

    invoke-direct {v4, v1, v0, v2}, LX/7VF;-><init>(LX/7VG;Ljava/lang/String;LX/7WC;)V

    move-object v4, v4

    .line 1214603
    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method


# virtual methods
.method public abstract a()V
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214604
    return-void
.end method

.method public abstract b()V
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1214552
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1214553
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1214554
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1214550
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->u:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "caller_context"

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->n()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1214551
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214541
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1214542
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1214543
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214544
    if-eqz p2, :cond_0

    .line 1214545
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1214546
    :cond_0
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1214547
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1214548
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->finish()V

    .line 1214549
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    .line 1214535
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->o()LX/7YU;

    move-result-object v0

    .line 1214536
    iget-object v1, v0, LX/7YU;->l:Ljava/lang/String;

    move-object v0, v1

    .line 1214537
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1214538
    const-string v2, "ref"

    const-string v3, "dialtone_optin_screen"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214539
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p()Ljava/lang/String;

    move-result-object v2

    const-string v3, "in"

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214540
    return-void
.end method

.method public m()V
    .locals 4

    .prologue
    .line 1214531
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->o()LX/7YU;

    move-result-object v0

    .line 1214532
    iget-object v1, v0, LX/7YU;->n:Ljava/lang/String;

    move-object v0, v1

    .line 1214533
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p()Ljava/lang/String;

    move-result-object v1

    const-string v2, "out"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214534
    return-void
.end method

.method public abstract n()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public abstract o()LX/7YU;
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    .line 1214508
    const-string v0, "optin_interstitial_back_pressed"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    .line 1214509
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->o()LX/7YU;

    move-result-object v0

    .line 1214510
    iget-object v1, v0, LX/7YU;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1214511
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1214512
    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->v:LX/03V;

    sget-object v3, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Encountered "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " back_button_behavior string in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->n()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 1214513
    iget-object v5, v4, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v4, v5

    .line 1214514
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214515
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1214516
    :cond_0
    invoke-static {v1}, LX/7WD;->fromString(Ljava/lang/String;)LX/7WD;

    move-result-object v0

    .line 1214517
    if-nez v0, :cond_3

    .line 1214518
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1214519
    :cond_1
    :goto_1
    return-void

    .line 1214520
    :cond_2
    const-string v0, "empty"

    goto :goto_0

    .line 1214521
    :cond_3
    sget-object v1, LX/7WD;->CLOSE_OPTIN:LX/7WD;

    if-ne v0, v1, :cond_4

    .line 1214522
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->finish()V

    goto :goto_1

    .line 1214523
    :cond_4
    sget-object v1, LX/7WD;->DO_NOTHING:LX/7WD;

    if-eq v0, v1, :cond_1

    .line 1214524
    sget-object v1, LX/7WD;->PRIMARY_BUTTON_ACTION:LX/7WD;

    if-ne v0, v1, :cond_5

    .line 1214525
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a()V

    goto :goto_1

    .line 1214526
    :cond_5
    sget-object v1, LX/7WD;->SECONDARY_BUTTON_ACTION:LX/7WD;

    if-ne v0, v1, :cond_6

    .line 1214527
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b()V

    goto :goto_1

    .line 1214528
    :cond_6
    sget-object v1, LX/7WD;->DEFAULT_BEHAVIOR:LX/7WD;

    if-ne v0, v1, :cond_7

    .line 1214529
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_1

    .line 1214530
    :cond_7
    sget-object v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p:Ljava/lang/String;

    const-string v1, "Encountered a totally unexpected ZeroOptinInterstitialActivityBase.BackButtonBehavior"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public abstract p()Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
    .end annotation
.end method

.method public final q()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1214502
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1214503
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->o()LX/7YU;

    move-result-object v1

    .line 1214504
    iget-object p0, v1, LX/7YU;->j:Ljava/lang/String;

    move-object v1, p0

    .line 1214505
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214506
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1214507
    return-object v0
.end method

.method public final r()V
    .locals 0

    .prologue
    .line 1214500
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1214501
    return-void
.end method
