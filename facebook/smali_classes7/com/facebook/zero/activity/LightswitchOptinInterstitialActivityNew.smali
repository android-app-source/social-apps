.class public Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private w:LX/7YZ;

.field private x:LX/2EJ;

.field private y:Landroid/view/View;

.field private z:Landroid/widget/ProgressBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1214949
    const-class v0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;

    const-string v1, "lightswitch_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1214947
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;-><init>()V

    .line 1214948
    return-void
.end method

.method public static t(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V
    .locals 2

    .prologue
    .line 1214943
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->y:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1214944
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->x:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1214945
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->z:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214946
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1214940
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->z:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214941
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1214942
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1214937
    invoke-static {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    .line 1214938
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->l()V

    .line 1214939
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214950
    invoke-direct {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->u()V

    .line 1214951
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214952
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1214930
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214931
    iget-boolean v1, v0, LX/7YZ;->c:Z

    move v0, v1

    .line 1214932
    if-eqz v0, :cond_0

    .line 1214933
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->x:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1214934
    :goto_0
    return-void

    .line 1214935
    :cond_0
    invoke-static {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    .line 1214936
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->m()V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1214865
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Landroid/os/Bundle;)V

    .line 1214866
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1214867
    new-instance v1, LX/7YZ;

    invoke-direct {v1, v0}, LX/7YZ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1214868
    const-string v2, "image_url_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->b:Ljava/lang/String;

    .line 1214869
    const-string v2, "should_show_confirmation_key"

    const/4 v0, 0x1

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, LX/7YZ;->c:Z

    .line 1214870
    const-string v2, "confirmation_title_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->d:Ljava/lang/String;

    .line 1214871
    const-string v2, "confirmation_description_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->e:Ljava/lang/String;

    .line 1214872
    const-string v2, "confirmation_primary_button_text_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->f:Ljava/lang/String;

    .line 1214873
    const-string v2, "confirmation_secondary_button_text_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->g:Ljava/lang/String;

    .line 1214874
    const-string v2, "confirmation_back_button_behavior_key"

    const-string v0, ""

    invoke-virtual {v1, v2, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/7YZ;->h:Ljava/lang/String;

    .line 1214875
    move-object v1, v1

    .line 1214876
    move-object v0, v1

    .line 1214877
    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214878
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214879
    iget-object v1, v0, LX/7YU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1214880
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1214881
    const-string v0, "LightswitchOptinInterstitialActivityNew"

    const-string v1, "Tried to show %s, but didn\'t find a campaign ID"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "LightswitchOptinInterstitialActivityNew"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214882
    invoke-virtual {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->finish()V

    .line 1214883
    :goto_0
    return-void

    .line 1214884
    :cond_0
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->setTheme(I)V

    .line 1214885
    const v0, 0x7f0309e3

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->setContentView(I)V

    .line 1214886
    const v0, 0x7f0d1916

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->y:Landroid/view/View;

    .line 1214887
    const v0, 0x7f0d191d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->z:Landroid/widget/ProgressBar;

    .line 1214888
    const v0, 0x7f0d1917

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1214889
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214890
    iget-object v1, v0, LX/7YZ;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1214891
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214892
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214893
    iget-object v2, v1, LX/7YZ;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1214894
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214895
    :goto_1
    const v0, 0x7f0d1918

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    .line 1214896
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214897
    iget-object v2, v1, LX/7YU;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1214898
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214899
    const v0, 0x7f0d1919

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    .line 1214900
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214901
    iget-object v2, v1, LX/7YU;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1214902
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214903
    const v0, 0x7f0d191a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    .line 1214904
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214905
    iget-object v2, v1, LX/7YU;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1214906
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214907
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1214908
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    new-instance v1, LX/7VS;

    invoke-direct {v1, p0}, LX/7VS;-><init>(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214909
    :cond_1
    const v0, 0x7f0d191b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    .line 1214910
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214911
    iget-object v2, v1, LX/7YU;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1214912
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214913
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    new-instance v1, LX/7VT;

    invoke-direct {v1, p0}, LX/7VT;-><init>(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214914
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214915
    iget-object v2, v1, LX/7YZ;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1214916
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214917
    iget-object v2, v1, LX/7YZ;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1214918
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214919
    iget-object v2, v1, LX/7YZ;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1214920
    new-instance v2, LX/7VU;

    invoke-direct {v2, p0}, LX/7VU;-><init>(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214921
    iget-object v2, v1, LX/7YZ;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1214922
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->x:LX/2EJ;

    .line 1214923
    const v0, 0x7f0d191c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    .line 1214924
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214925
    iget-object v2, v1, LX/7YU;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1214926
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214927
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    new-instance v1, LX/7VV;

    invoke-direct {v1, p0}, LX/7VV;-><init>(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214928
    const-string v0, "iorg_optin_interstitial_shown"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1214929
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1214864
    sget-object v0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final o()LX/7YU;
    .locals 1

    .prologue
    .line 1214863
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1214843
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->x:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214844
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->onBackPressed()V

    .line 1214845
    :goto_0
    :pswitch_0
    return-void

    .line 1214846
    :cond_0
    const-string v0, "optin_interstitial_back_pressed"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    .line 1214847
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->w:LX/7YZ;

    .line 1214848
    iget-object v1, v0, LX/7YZ;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1214849
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1214850
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->v:LX/03V;

    const-string v2, "LightswitchOptinInterstitialActivityNew"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Encountered "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " back_button_behavior string in LightswitchOptinInterstitialActivityNew"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214851
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    .line 1214852
    :cond_1
    const-string v0, "empty"

    goto :goto_1

    .line 1214853
    :cond_2
    invoke-static {v0}, LX/7WD;->fromString(Ljava/lang/String;)LX/7WD;

    move-result-object v0

    .line 1214854
    if-nez v0, :cond_3

    .line 1214855
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    .line 1214856
    :cond_3
    sget-object v1, LX/7VW;->a:[I

    invoke-virtual {v0}, LX/7WD;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1214857
    const-string v0, "LightswitchOptinInterstitialActivityNew"

    const-string v1, "Encountered a totally unexpected ZeroOptinInterstitialActivityBase.BackButtonBehavior"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1214858
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->finish()V

    goto :goto_0

    .line 1214859
    :pswitch_2
    invoke-static {p0}, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;)V

    .line 1214860
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->m()V

    goto :goto_0

    .line 1214861
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;->x:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    goto :goto_0

    .line 1214862
    :pswitch_4
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
    .end annotation

    .prologue
    .line 1214842
    const-string v0, "free_facebook"

    return-object v0
.end method
