.class public Lcom/facebook/zero/activity/ZeroInternSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/7WY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7Wa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7Wc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7We;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7Wi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7Wk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7Wm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/7Wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/7Wq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/7Ws;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/7Wu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/7Wz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/7X1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/7X4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215720
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroInternSettingsActivity;LX/7WY;LX/7Wa;LX/7Wc;LX/7We;LX/7Wi;LX/7Wk;LX/7Wm;LX/7Wo;LX/7Wq;LX/7Ws;LX/7Wu;LX/7Wz;LX/7X1;LX/7X4;)V
    .locals 0

    .prologue
    .line 1215719
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->a:LX/7WY;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->b:LX/7Wa;

    iput-object p3, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->c:LX/7Wc;

    iput-object p4, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->d:LX/7We;

    iput-object p5, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->e:LX/7Wi;

    iput-object p6, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->f:LX/7Wk;

    iput-object p7, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->g:LX/7Wm;

    iput-object p8, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->h:LX/7Wo;

    iput-object p9, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->i:LX/7Wq;

    iput-object p10, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->j:LX/7Ws;

    iput-object p11, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->k:LX/7Wu;

    iput-object p12, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->l:LX/7Wz;

    iput-object p13, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->m:LX/7X1;

    iput-object p14, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;

    invoke-static {v14}, LX/7WY;->b(LX/0QB;)LX/7WY;

    move-result-object v1

    check-cast v1, LX/7WY;

    invoke-static {v14}, LX/7Wa;->b(LX/0QB;)LX/7Wa;

    move-result-object v2

    check-cast v2, LX/7Wa;

    invoke-static {v14}, LX/7Wc;->b(LX/0QB;)LX/7Wc;

    move-result-object v3

    check-cast v3, LX/7Wc;

    invoke-static {v14}, LX/7We;->b(LX/0QB;)LX/7We;

    move-result-object v4

    check-cast v4, LX/7We;

    invoke-static {v14}, LX/7Wi;->b(LX/0QB;)LX/7Wi;

    move-result-object v5

    check-cast v5, LX/7Wi;

    invoke-static {v14}, LX/7Wk;->b(LX/0QB;)LX/7Wk;

    move-result-object v6

    check-cast v6, LX/7Wk;

    invoke-static {v14}, LX/7Wm;->b(LX/0QB;)LX/7Wm;

    move-result-object v7

    check-cast v7, LX/7Wm;

    invoke-static {v14}, LX/7Wo;->b(LX/0QB;)LX/7Wo;

    move-result-object v8

    check-cast v8, LX/7Wo;

    invoke-static {v14}, LX/7Wq;->b(LX/0QB;)LX/7Wq;

    move-result-object v9

    check-cast v9, LX/7Wq;

    invoke-static {v14}, LX/7Ws;->b(LX/0QB;)LX/7Ws;

    move-result-object v10

    check-cast v10, LX/7Ws;

    invoke-static {v14}, LX/7Wu;->b(LX/0QB;)LX/7Wu;

    move-result-object v11

    check-cast v11, LX/7Wu;

    invoke-static {v14}, LX/7Wz;->b(LX/0QB;)LX/7Wz;

    move-result-object v12

    check-cast v12, LX/7Wz;

    invoke-static {v14}, LX/7X1;->b(LX/0QB;)LX/7X1;

    move-result-object v13

    check-cast v13, LX/7X1;

    invoke-static {v14}, LX/7X4;->b(LX/0QB;)LX/7X4;

    move-result-object v14

    check-cast v14, LX/7X4;

    invoke-static/range {v0 .. v14}, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->a(Lcom/facebook/zero/activity/ZeroInternSettingsActivity;LX/7WY;LX/7Wa;LX/7Wc;LX/7We;LX/7Wi;LX/7Wk;LX/7Wm;LX/7Wo;LX/7Wq;LX/7Ws;LX/7Wu;LX/7Wz;LX/7X1;LX/7X4;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1215696
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->b(Landroid/os/Bundle;)V

    .line 1215697
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215698
    const-string v0, "Zero Rating Settings"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1215699
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1215700
    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1215701
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1215702
    const v2, 0x7f080e8d

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 1215703
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1215704
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215705
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->m:LX/7X1;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215706
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->f:LX/7Wk;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215707
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->e:LX/7Wi;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215708
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->b:LX/7Wa;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215709
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->a:LX/7WY;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215710
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->d:LX/7We;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215711
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->l:LX/7Wz;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215712
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->h:LX/7Wo;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215713
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->k:LX/7Wu;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215714
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->j:LX/7Ws;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215715
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->i:LX/7Wq;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215716
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->c:LX/7Wc;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215717
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->g:LX/7Wm;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1215718
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1215691
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onAttachedToWindow()V

    .line 1215692
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    if-eqz v0, :cond_0

    .line 1215693
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    .line 1215694
    iget-object p0, v0, LX/7X4;->b:LX/0Yb;

    invoke-virtual {p0}, LX/0Yb;->b()V

    .line 1215695
    :cond_0
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1215686
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDetachedFromWindow()V

    .line 1215687
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    if-eqz v0, :cond_0

    .line 1215688
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroInternSettingsActivity;->n:LX/7X4;

    .line 1215689
    iget-object p0, v0, LX/7X4;->b:LX/0Yb;

    invoke-virtual {p0}, LX/0Yb;->c()V

    .line 1215690
    :cond_0
    return-void
.end method
