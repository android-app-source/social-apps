.class public Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Lcom/facebook/fbui/facepile/FacepileView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/ProgressBar;

.field private w:LX/0yV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/7YW;

.field private z:LX/2EJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1214741
    const-class v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;

    const-string v1, "dialtone_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1214739
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;-><init>()V

    .line 1214740
    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;LX/0yV;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;",
            "LX/0yV;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1214738
    iput-object p1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->w:LX/0yV;

    iput-object p2, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->x:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;

    invoke-static {v1}, LX/0yV;->b(LX/0QB;)LX/0yV;

    move-result-object v0

    check-cast v0, LX/0yV;

    const/16 v2, 0x2e3

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->a(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;LX/0yV;LX/0Ot;)V

    return-void
.end method

.method public static t(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V
    .locals 2

    .prologue
    .line 1214734
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->A:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1214735
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->z:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1214736
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->J:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214737
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1214731
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->J:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1214732
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->A:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1214733
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1214728
    invoke-static {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    .line 1214729
    invoke-virtual {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->l()V

    .line 1214730
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1214742
    invoke-direct {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->u()V

    .line 1214743
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1214744
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1214721
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214722
    iget-boolean v1, v0, LX/7YW;->f:Z

    move v0, v1

    .line 1214723
    if-eqz v0, :cond_0

    .line 1214724
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->z:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1214725
    :goto_0
    return-void

    .line 1214726
    :cond_0
    invoke-static {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    .line 1214727
    invoke-virtual {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->m()V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    .line 1214634
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Landroid/os/Bundle;)V

    .line 1214635
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1214636
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->w:LX/0yV;

    .line 1214637
    new-instance v3, LX/7YW;

    invoke-direct {v3, v0, v1}, LX/7YW;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0yV;)V

    .line 1214638
    const-string v4, "image_url_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->c:Ljava/lang/String;

    .line 1214639
    const-string v4, "facepile_text_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->d:Ljava/lang/String;

    .line 1214640
    const-string v4, "should_show_confirmation_key"

    const/4 v0, 0x1

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v3, LX/7YW;->f:Z

    .line 1214641
    const-string v4, "confirmation_title_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->g:Ljava/lang/String;

    .line 1214642
    const-string v4, "confirmation_description_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->h:Ljava/lang/String;

    .line 1214643
    const-string v4, "confirmation_primary_button_text_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->i:Ljava/lang/String;

    .line 1214644
    const-string v4, "confirmation_secondary_button_text_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->j:Ljava/lang/String;

    .line 1214645
    const-string v4, "confirmation_back_button_behavior_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->k:Ljava/lang/String;

    .line 1214646
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1214647
    iput-object v4, v3, LX/7YW;->e:LX/0Px;

    .line 1214648
    const-string v4, "facepile_profile_picture_urls_key"

    const-string v0, ""

    invoke-virtual {v3, v4, v0}, LX/7YU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1214649
    :try_start_0
    iget-object v0, v3, LX/7YW;->l:LX/0yV;

    invoke-virtual {v0, v4}, LX/0yV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/7YW;->e:LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1214650
    :goto_0
    move-object v3, v3

    .line 1214651
    move-object v0, v3

    .line 1214652
    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214653
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214654
    iget-object v1, v0, LX/7YU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1214655
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1214656
    const-string v0, "DialtoneOptinInterstitialActivityNew"

    const-string v1, "Tried to show %s, but didn\'t find a campaign ID"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "DialtoneOptinInterstitialActivityNew"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214657
    invoke-virtual {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->finish()V

    .line 1214658
    :goto_1
    return-void

    .line 1214659
    :cond_0
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->setTheme(I)V

    .line 1214660
    const v0, 0x7f030412

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->setContentView(I)V

    .line 1214661
    const v0, 0x7f0d0c7e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->A:Landroid/view/View;

    .line 1214662
    const v0, 0x7f0d0c7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    .line 1214663
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214664
    iget-object v3, v1, LX/7YU;->f:Ljava/lang/String;

    move-object v1, v3

    .line 1214665
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214666
    const v0, 0x7f0d0c80

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    .line 1214667
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214668
    iget-object v3, v1, LX/7YU;->g:Ljava/lang/String;

    move-object v1, v3

    .line 1214669
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214670
    const v0, 0x7f0d0c81

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->D:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1214671
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214672
    iget-object v1, v0, LX/7YW;->e:LX/0Px;

    move-object v0, v1

    .line 1214673
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1214674
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->D:Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214675
    iget-object v3, v1, LX/7YW;->e:LX/0Px;

    move-object v1, v3

    .line 1214676
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1214677
    :goto_2
    const v0, 0x7f0d0c82

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    .line 1214678
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214679
    iget-object v3, v1, LX/7YW;->d:Ljava/lang/String;

    move-object v1, v3

    .line 1214680
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214681
    const v0, 0x7f0d0c83

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    .line 1214682
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214683
    iget-object v3, v1, LX/7YU;->h:Ljava/lang/String;

    move-object v1, v3

    .line 1214684
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214685
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214686
    iget-object v1, v0, LX/7YU;->j:Ljava/lang/String;

    move-object v0, v1

    .line 1214687
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1214688
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    new-instance v1, LX/7VK;

    invoke-direct {v1, p0}, LX/7VK;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214689
    :cond_1
    const v0, 0x7f0d0c84

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1214690
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214691
    iget-object v1, v0, LX/7YW;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1214692
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1214693
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214694
    iget-object v2, v1, LX/7YW;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1214695
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1214696
    :goto_3
    const v0, 0x7f0d0c85

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->H:Landroid/widget/TextView;

    .line 1214697
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->H:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214698
    iget-object v2, v1, LX/7YU;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1214699
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214700
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->H:Landroid/widget/TextView;

    new-instance v1, LX/7VL;

    invoke-direct {v1, p0}, LX/7VL;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214701
    const v0, 0x7f0d0c86

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->I:Landroid/widget/TextView;

    .line 1214702
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->I:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214703
    iget-object v2, v1, LX/7YU;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1214704
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1214705
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->I:Landroid/widget/TextView;

    new-instance v1, LX/7VM;

    invoke-direct {v1, p0}, LX/7VM;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214706
    const v0, 0x7f0d0c87

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->J:Landroid/widget/ProgressBar;

    .line 1214707
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214708
    iget-object v2, v1, LX/7YW;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1214709
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214710
    iget-object v2, v1, LX/7YW;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1214711
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214712
    iget-object v2, v1, LX/7YW;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1214713
    new-instance v2, LX/7VN;

    invoke-direct {v2, p0}, LX/7VN;-><init>(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214714
    iget-object v2, v1, LX/7YW;->j:Ljava/lang/String;

    move-object v1, v2

    .line 1214715
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->z:LX/2EJ;

    .line 1214716
    const-string v0, "iorg_optin_interstitial_shown"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1214717
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->D:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1214718
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_3

    .line 1214719
    :catch_0
    move-exception v4

    .line 1214720
    sget-object v0, LX/7YW;->b:Ljava/lang/Class;

    const-string v1, "Failed to read zero optin facepile URLs from shared prefs"

    invoke-static {v0, v1, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public final l()V
    .locals 4

    .prologue
    .line 1214631
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->l()V

    .line 1214632
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->M:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1214633
    return-void
.end method

.method public final m()V
    .locals 6

    .prologue
    .line 1214628
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->m()V

    .line 1214629
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0dQ;->M:LX/0Tn;

    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1214630
    return-void
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1214627
    sget-object v0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final o()LX/7YU;
    .locals 1

    .prologue
    .line 1214626
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1214606
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->z:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1214607
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->onBackPressed()V

    .line 1214608
    :goto_0
    :pswitch_0
    return-void

    .line 1214609
    :cond_0
    const-string v0, "optin_interstitial_back_pressed"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    .line 1214610
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->y:LX/7YW;

    .line 1214611
    iget-object v1, v0, LX/7YW;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1214612
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1214613
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->v:LX/03V;

    const-string v2, "DialtoneOptinInterstitialActivityNew"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Encountered "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " back_button_behavior string in DialtoneOptinInterstitialActivityNew"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214614
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    .line 1214615
    :cond_1
    const-string v0, "empty"

    goto :goto_1

    .line 1214616
    :cond_2
    invoke-static {v0}, LX/7WD;->fromString(Ljava/lang/String;)LX/7WD;

    move-result-object v0

    .line 1214617
    if-nez v0, :cond_3

    .line 1214618
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    .line 1214619
    :cond_3
    sget-object v1, LX/7VO;->a:[I

    invoke-virtual {v0}, LX/7WD;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1214620
    const-string v0, "DialtoneOptinInterstitialActivityNew"

    const-string v1, "Encountered a totally unexpected ZeroOptinInterstitialActivityBase.BackButtonBehavior"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1214621
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->finish()V

    goto :goto_0

    .line 1214622
    :pswitch_2
    invoke-static {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->t(Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;)V

    .line 1214623
    invoke-virtual {p0}, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->m()V

    goto :goto_0

    .line 1214624
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;->z:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    goto :goto_0

    .line 1214625
    :pswitch_4
    invoke-super {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->r()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
    .end annotation

    .prologue
    .line 1214605
    const-string v0, "dialtone"

    return-object v0
.end method
