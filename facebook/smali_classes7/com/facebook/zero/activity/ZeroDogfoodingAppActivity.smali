.class public Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/common/shortcuts/InstallShortcutHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215559
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 1215560
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->p:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021b0f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v7, 0x0

    .line 1215561
    invoke-virtual {v0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a()I

    move-result v2

    .line 1215562
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1215563
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1215564
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1215565
    invoke-virtual {v1, v7, v7, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1215566
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1215567
    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1215568
    move-object v3, v3

    .line 1215569
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1215570
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1215571
    const-string v0, "https://m.facebook.com/zero/dogfooding"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1215572
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->p:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    const-string v2, "Iorg Dogfooding"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    .line 1215573
    return-void
.end method

.method private static a(Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/48V;)V
    .locals 0

    .prologue
    .line 1215574
    iput-object p1, p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->p:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    iput-object p2, p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->q:LX/48V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;

    invoke-static {v1}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static {v1}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v1

    check-cast v1, LX/48V;

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->a(Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/48V;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1215575
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215576
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215577
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->a()V

    .line 1215578
    new-instance v0, LX/0D4;

    invoke-direct {v0, p0}, LX/0D4;-><init>(Landroid/content/Context;)V

    .line 1215579
    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->setContentView(Landroid/view/View;)V

    .line 1215580
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroDogfoodingAppActivity;->q:LX/48V;

    const-string v2, "https://m.facebook.com/zero/dogfooding"

    invoke-virtual {v1, v0, v2}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1215581
    return-void
.end method
