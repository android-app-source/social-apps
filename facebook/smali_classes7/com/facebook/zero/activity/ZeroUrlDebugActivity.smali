.class public Lcom/facebook/zero/activity/ZeroUrlDebugActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/1hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215950
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1215951
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    .line 1215952
    iget-object v1, p0, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->p:LX/1hx;

    .line 1215953
    iget-object v2, v1, LX/1hx;->b:Ljava/util/List;

    move-object v1, v2

    .line 1215954
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1215955
    const/4 v2, 0x0

    .line 1215956
    :try_start_0
    new-instance v5, Ljava/net/URI;

    invoke-direct {v5, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 1215957
    sget-object v6, LX/7WM;->a:[Ljava/lang/String;

    array-length v7, v6

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_1

    aget-object v8, v6, v4

    .line 1215958
    invoke-virtual {v5}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_3

    .line 1215959
    const/4 v2, 0x1

    .line 1215960
    :cond_1
    :goto_1
    move v2, v2

    .line 1215961
    if-eqz v2, :cond_0

    .line 1215962
    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1215963
    :goto_2
    if-ltz v2, :cond_0

    .line 1215964
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const/16 v5, 0xff

    const/16 v6, 0x57

    const/16 v7, 0x19

    const/16 v8, 0x9f

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v2

    const/16 v6, 0x21

    invoke-interface {v0, v4, v2, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1215965
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    goto :goto_2

    .line 1215966
    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215967
    return-void

    .line 1215968
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1215969
    :catch_0
    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;

    invoke-static {v0}, LX/1hx;->a(LX/0QB;)LX/1hx;

    move-result-object v0

    check-cast v0, LX/1hx;

    iput-object v0, p0, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->p:LX/1hx;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1215970
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215971
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215972
    const v0, 0x7f031663

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->setContentView(I)V

    .line 1215973
    const v0, 0x7f0d2f87

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1215974
    const-string v1, ""

    .line 1215975
    iget-object v2, p0, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->p:LX/1hx;

    .line 1215976
    iget-object v3, v2, LX/1hx;->b:Ljava/util/List;

    move-object v2, v3

    .line 1215977
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1215978
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 1215979
    goto :goto_0

    .line 1215980
    :cond_0
    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1215981
    invoke-direct {p0, v0, v2}, Lcom/facebook/zero/activity/ZeroUrlDebugActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215982
    return-void
.end method
