.class public Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;
.super Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private w:LX/7YX;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/ProgressBar;

.field private z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1215136
    const-class v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;

    const-string v1, "messenger_optin_interstitial_new"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215138
    invoke-direct {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;-><init>()V

    .line 1215139
    return-void
.end method

.method private t()LX/7Ve;
    .locals 2

    .prologue
    .line 1215137
    invoke-virtual {p0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "zero_messenger_type_extra_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7Ve;->fromString(Ljava/lang/String;)LX/7Ve;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1215128
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215129
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->y:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215130
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->l()V

    .line 1215131
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1215132
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->y:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215133
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215134
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1215135
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1215124
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1215125
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->y:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215126
    invoke-virtual {p0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->m()V

    .line 1215127
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1215064
    invoke-super {p0, p1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Landroid/os/Bundle;)V

    .line 1215065
    invoke-direct {p0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->t()LX/7Ve;

    move-result-object v0

    sget-object v1, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1215066
    new-instance v1, LX/7Ya;

    invoke-direct {v1, v0}, LX/7Ya;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1215067
    invoke-virtual {v1}, LX/7YX;->a()V

    .line 1215068
    move-object v0, v1

    .line 1215069
    :goto_0
    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215070
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215071
    iget-object v1, v0, LX/7YU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1215072
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1215073
    const-string v0, "MessengerOptinInterstitialActivityNew"

    const-string v1, "Tried to show %s, but didn\'t find a campaign ID"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "MessengerOptinInterstitialActivityNew"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1215074
    invoke-virtual {p0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->finish()V

    .line 1215075
    :goto_1
    return-void

    .line 1215076
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1215077
    new-instance v1, LX/7YY;

    invoke-direct {v1, v0}, LX/7YY;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1215078
    invoke-virtual {v1}, LX/7YX;->a()V

    .line 1215079
    move-object v0, v1

    .line 1215080
    goto :goto_0

    .line 1215081
    :cond_1
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->setTheme(I)V

    .line 1215082
    const v0, 0x7f030afb

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->setContentView(I)V

    .line 1215083
    const v0, 0x7f0d1bbe

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->x:Landroid/view/View;

    .line 1215084
    const v0, 0x7f0d1bc6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->y:Landroid/widget/ProgressBar;

    .line 1215085
    const v0, 0x7f0d1bbf

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1215086
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215087
    iget-object v1, v0, LX/7YX;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1215088
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1215089
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215090
    iget-object v2, v1, LX/7YX;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1215091
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1215092
    :goto_2
    const v0, 0x7f0d1bc0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->A:Landroid/widget/TextView;

    .line 1215093
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->A:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215094
    iget-object v2, v1, LX/7YU;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1215095
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215096
    const v0, 0x7f0d1bc1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    .line 1215097
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215098
    iget-object v2, v1, LX/7YX;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1215099
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215100
    const v0, 0x7f0d1bc2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    .line 1215101
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215102
    iget-object v2, v1, LX/7YU;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1215103
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215104
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->C:Landroid/widget/TextView;

    new-instance v1, LX/7Vb;

    invoke-direct {v1, p0}, LX/7Vb;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215105
    const v0, 0x7f0d1bc3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    .line 1215106
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215107
    iget-object v2, v1, LX/7YU;->m:Ljava/lang/String;

    move-object v1, v2

    .line 1215108
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215109
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->D:Landroid/widget/TextView;

    new-instance v1, LX/7Vc;

    invoke-direct {v1, p0}, LX/7Vc;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215110
    const v0, 0x7f0d1bc4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    .line 1215111
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->E:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215112
    iget-object v2, v1, LX/7YU;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1215113
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215114
    const v0, 0x7f0d1bc5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    .line 1215115
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215116
    iget-object v2, v1, LX/7YU;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1215117
    invoke-static {v0, v1}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1215118
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    .line 1215119
    iget-object v1, v0, LX/7YU;->j:Ljava/lang/String;

    move-object v0, v1

    .line 1215120
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1215121
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->F:Landroid/widget/TextView;

    new-instance v1, LX/7Vd;

    invoke-direct {v1, p0}, LX/7Vd;-><init>(Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215122
    :cond_2
    const-string v0, "iorg_optin_interstitial_shown"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1215123
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public final n()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1215063
    sget-object v0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final o()LX/7YU;
    .locals 1

    .prologue
    .line 1215062
    iget-object v0, p0, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->w:LX/7YX;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/ZeroOptinFlowTypeValue;
    .end annotation

    .prologue
    .line 1215061
    invoke-direct {p0}, Lcom/facebook/zero/activity/MessengerOptinInterstitialActivityNew;->t()LX/7Ve;

    move-result-object v0

    sget-object v1, LX/7Ve;->MESSAGE_CAPPING:LX/7Ve;

    if-ne v0, v1, :cond_0

    const-string v0, "message_capping"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "free_messenger"

    goto :goto_0
.end method
