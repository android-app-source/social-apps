.class public Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/resources/ui/FbTextView;

.field private B:LX/0aG;

.field private C:LX/0Sh;

.field private D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/0Xl;

.field public F:LX/0Xl;

.field private G:Ljava/util/concurrent/ScheduledExecutorService;

.field private H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field public I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

.field private q:Landroid/widget/ProgressBar;

.field private r:Landroid/widget/LinearLayout;

.field private s:Lcom/facebook/resources/ui/FbButton;

.field private t:Lcom/facebook/resources/ui/FbButton;

.field private u:Landroid/widget/ScrollView;

.field private v:Lcom/facebook/resources/ui/FbTextView;

.field private w:Lcom/facebook/resources/ui/FbTextView;

.field private x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private y:Lcom/facebook/resources/ui/FbTextView;

.field private z:Lcom/facebook/fbui/facepile/FacepileView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1215319
    const-class v0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    const-string v1, "zero_optin_interstitial"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1215320
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1215321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    .line 1215322
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->n()V

    .line 1215323
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->p()Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    move-result-object v1

    new-instance v2, LX/7Vk;

    invoke-direct {v2, p0}, LX/7Vk;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-interface {v0, v1, v2}, LX/33e;->a(Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1215324
    return-void
.end method

.method private a(LX/0aG;LX/0Sh;LX/0Ot;LX/0Xl;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Xl;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1215331
    iput-object p1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->B:LX/0aG;

    .line 1215332
    iput-object p2, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->C:LX/0Sh;

    .line 1215333
    iput-object p3, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    .line 1215334
    iput-object p4, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->E:LX/0Xl;

    .line 1215335
    iput-object p5, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->F:LX/0Xl;

    .line 1215336
    iput-object p6, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->G:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1215337
    iput-object p7, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->H:LX/0Ot;

    .line 1215338
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-static {v7}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v7}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    const/16 v3, 0x13ec

    invoke-static {v7, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v7}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v7}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v7}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v8, 0x13f1

    invoke-static {v7, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->a(LX/0aG;LX/0Sh;LX/0Ot;LX/0Xl;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 3

    .prologue
    .line 1215325
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->n()V

    .line 1215326
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->q()Lcom/facebook/zero/sdk/request/ZeroOptinParams;

    move-result-object v1

    new-instance v2, LX/7Vl;

    invoke-direct {v2, p0}, LX/7Vl;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-interface {v0, v1, v2}, LX/33e;->a(Lcom/facebook/zero/sdk/request/ZeroOptinParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1215327
    return-void
.end method

.method public static l(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 3

    .prologue
    .line 1215328
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->n()V

    .line 1215329
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->r()Lcom/facebook/zero/sdk/request/ZeroOptoutParams;

    move-result-object v1

    new-instance v2, LX/7Vm;

    invoke-direct {v2, p0}, LX/7Vm;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-interface {v0, v1, v2}, LX/33e;->a(Lcom/facebook/zero/sdk/request/ZeroOptoutParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1215330
    return-void
.end method

.method public static m(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1215287
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    if-nez v0, :cond_0

    .line 1215288
    invoke-virtual {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->finish()V

    .line 1215289
    :cond_0
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215290
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->s:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1215291
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->s:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215292
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->t:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1215293
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->t:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215294
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->v:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215295
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->v:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215296
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215297
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215298
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1215299
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->c()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1215300
    :cond_1
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215301
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1215302
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->y:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215303
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->y:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215304
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1215305
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->z:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1215306
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1215307
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->z:Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1215308
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->z:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1215309
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->A:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215310
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->A:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1215311
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->s()V

    .line 1215312
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215313
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->u:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1215314
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1215315
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1215316
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->u:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1215317
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->q:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1215318
    return-void
.end method

.method public static o(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 3

    .prologue
    .line 1215284
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    if-nez v0, :cond_0

    .line 1215285
    :goto_0
    return-void

    .line 1215286
    :cond_0
    new-instance v0, LX/31Y;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, LX/31Y;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/7Vo;

    invoke-direct {v2, p0}, LX/7Vo;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->l()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/7Vn;

    invoke-direct {v2, p0}, LX/7Vn;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method

.method private p()Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;
    .locals 4

    .prologue
    .line 1215283
    new-instance v1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private q()Lcom/facebook/zero/sdk/request/ZeroOptinParams;
    .locals 4

    .prologue
    .line 1215282
    new-instance v1, Lcom/facebook/zero/sdk/request/ZeroOptinParams;

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/zero/sdk/request/ZeroOptinParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private r()Lcom/facebook/zero/sdk/request/ZeroOptoutParams;
    .locals 3

    .prologue
    .line 1215281
    new-instance v1, Lcom/facebook/zero/sdk/request/ZeroOptoutParams;

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/facebook/zero/sdk/request/ZeroOptoutParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;)V

    return-object v1
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1215278
    new-instance v0, LX/7Vp;

    invoke-direct {v0, p0}, LX/7Vp;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    .line 1215279
    iget-object v1, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->A:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->I:Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    invoke-virtual {v2}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-static {v1, v2, v3, v3, v0}, LX/7Gw;->a(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    .line 1215280
    return-void
.end method

.method public static t(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 5

    .prologue
    .line 1215276
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->G:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity$9;

    invoke-direct {v1, p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity$9;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    const-wide/16 v2, 0x3a98

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1215277
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1215257
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1215258
    invoke-static {p0, p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1215259
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->setTheme(I)V

    .line 1215260
    const v0, 0x7f030baa

    invoke-virtual {p0, v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->setContentView(I)V

    .line 1215261
    const v0, 0x7f0d0c72

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->q:Landroid/widget/ProgressBar;

    .line 1215262
    const v0, 0x7f0d1cf4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->u:Landroid/widget/ScrollView;

    .line 1215263
    const v0, 0x7f0d0c74

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 1215264
    const v0, 0x7f0d0c76

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 1215265
    const v0, 0x7f0d1cf5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->x:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1215266
    const v0, 0x7f0d1cf6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->y:Lcom/facebook/resources/ui/FbTextView;

    .line 1215267
    const v0, 0x7f0d0c79

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->z:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1215268
    const v0, 0x7f0d1cf7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->A:Lcom/facebook/resources/ui/FbTextView;

    .line 1215269
    const v0, 0x7f0d0c78

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->r:Landroid/widget/LinearLayout;

    .line 1215270
    const v0, 0x7f0d1cf2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->s:Lcom/facebook/resources/ui/FbButton;

    .line 1215271
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->s:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7Vi;

    invoke-direct {v1, p0}, LX/7Vi;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215272
    const v0, 0x7f0d1cf3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->t:Lcom/facebook/resources/ui/FbButton;

    .line 1215273
    iget-object v0, p0, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->t:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7Vj;

    invoke-direct {v1, p0}, LX/7Vj;-><init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1215274
    invoke-direct {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->a()V

    .line 1215275
    return-void
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 1215255
    invoke-static {p0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->o(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    .line 1215256
    return-void
.end method
