.class public Lcom/facebook/zero/server/SendZeroHeaderRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/server/SendZeroHeaderRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1218467
    new-instance v0, LX/7XW;

    invoke-direct {v0}, LX/7XW;-><init>()V

    sput-object v0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1218461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218462
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    .line 1218463
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    .line 1218464
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    .line 1218465
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    .line 1218466
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1218455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218456
    iput-object p1, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    .line 1218457
    iput-object p2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    .line 1218458
    iput-object p3, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    .line 1218459
    iput-object p4, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    .line 1218460
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1218468
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1218443
    instance-of v1, p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    if-nez v1, :cond_1

    .line 1218444
    :cond_0
    :goto_0
    return v0

    .line 1218445
    :cond_1
    check-cast p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    .line 1218446
    iget-object v1, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    .line 1218447
    iget-object v2, p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1218448
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    .line 1218449
    iget-object v2, p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1218450
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    .line 1218451
    iget-object v2, p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1218452
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    .line 1218453
    iget-object v2, p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1218454
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1218442
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1218441
    const-class v0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "encryptedUId"

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "nonce"

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "headerAppId"

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "encryptedMachineId"

    iget-object v2, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1218436
    iget-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1218437
    iget-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1218438
    iget-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1218439
    iget-object v0, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1218440
    return-void
.end method
