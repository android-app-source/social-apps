.class public Lcom/facebook/zero/notification/ZeroPersistentNotificationService;
.super LX/0te;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/app/Notification;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1216334
    const-class v0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;

    sput-object v0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1216335
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 1216336
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->d:Ljava/lang/String;

    .line 1216337
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->e:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1216338
    iget-object v0, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->a:LX/0Zb;

    const-string v1, "messenger_free_data_notification_click"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1216339
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1216340
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1216341
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1216342
    const-string v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "text"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, LX/7WQ;->a()LX/0Px;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1216343
    const-string v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1216344
    const-string v3, "text"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1216345
    iget-object v4, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->c:Landroid/app/Notification;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->d:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->e:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1216346
    :goto_0
    return v0

    .line 1216347
    :cond_0
    iput-object v2, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->d:Ljava/lang/String;

    .line 1216348
    iput-object v3, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->e:Ljava/lang/String;

    .line 1216349
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1216350
    const-string v5, "com.facebook.zero.notification.ZeroPersistentNotificationService.TAP"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1216351
    const/high16 v5, 0x8000000

    invoke-static {p0, v1, v4, v5}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1216352
    new-instance v4, LX/2HB;

    invoke-direct {v4, p0}, LX/2HB;-><init>(Landroid/content/Context;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, LX/2HB;->a(J)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v4

    const v5, 0x7f020b55

    invoke-virtual {v4, v5}, LX/2HB;->a(I)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 1216353
    iput-boolean v0, v2, LX/2HB;->v:Z

    .line 1216354
    move-object v2, v2

    .line 1216355
    iput-object v1, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1216356
    move-object v1, v2

    .line 1216357
    const/4 v2, 0x2

    .line 1216358
    iput v2, v1, LX/2HB;->j:I

    .line 1216359
    move-object v1, v1

    .line 1216360
    invoke-virtual {v1, v0}, LX/2HB;->b(Z)LX/2HB;

    move-result-object v1

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->c:Landroid/app/Notification;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1216361
    goto :goto_0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1216362
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x1d0cada0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1216363
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 1216364
    invoke-virtual {p0}, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1216365
    const/16 v1, 0x25

    const v2, -0x102cb558

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x24

    const v4, -0x5ac3633

    invoke-static {v2, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1216366
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1216367
    if-nez p1, :cond_0

    .line 1216368
    invoke-static {}, LX/7WQ;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_0
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1216369
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1216370
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1216371
    :cond_0
    monitor-enter p0

    .line 1216372
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 1216373
    const-string v5, "com.facebook.zero.notification.ZeroPersistentNotificationService.SHOW"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1216374
    invoke-direct {p0, p1}, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1216375
    const-string v2, "id"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->c:Landroid/app/Notification;

    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1216376
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v0, -0x501c6594

    invoke-static {v0, v4}, LX/02F;->d(II)V

    move v0, v1

    .line 1216377
    :goto_1
    return v0

    .line 1216378
    :cond_1
    :try_start_1
    const-string v5, "com.facebook.zero.notification.ZeroPersistentNotificationService.TAP"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1216379
    invoke-direct {p0}, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->a()V

    .line 1216380
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v0, 0x57c915a4

    invoke-static {v0, v4}, LX/02F;->d(II)V

    move v0, v1

    goto :goto_1

    .line 1216381
    :cond_2
    :try_start_2
    const-string v1, "com.facebook.zero.notification.ZeroPersistentNotificationService.HIDE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1216382
    const-string v1, "id"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 1216383
    const-string v1, "id"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1216384
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1216385
    :cond_4
    invoke-virtual {p0, p3}, Lcom/facebook/zero/notification/ZeroPersistentNotificationService;->stopSelf(I)V

    .line 1216386
    const v0, 0x39246d80

    invoke-static {v0, v4}, LX/02F;->d(II)V

    move v0, v2

    goto :goto_1

    .line 1216387
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const v1, 0x5a974ef9

    invoke-static {v1, v4}, LX/02F;->d(II)V

    throw v0
.end method
