.class public final Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic c:LX/1lF;

.field public final synthetic d:LX/7YR;


# direct methods
.method public constructor <init>(LX/7YR;ZLandroid/support/v4/app/FragmentActivity;LX/1lF;)V
    .locals 0

    .prologue
    .line 1219866
    iput-object p1, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->d:LX/7YR;

    iput-boolean p2, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->a:Z

    iput-object p3, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->b:Landroid/support/v4/app/FragmentActivity;

    iput-object p4, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->c:LX/1lF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1219867
    iget-boolean v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->a:Z

    if-eqz v0, :cond_2

    .line 1219868
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->d:LX/7YR;

    iget-object v1, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1219869
    iget-object v2, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1219870
    iget-object v3, v2, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v2, v3

    .line 1219871
    sget-object v3, LX/0yY;->DIALTONE_PHOTO_INTERSTITIAL:LX/0yY;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1219872
    iget-object v3, v2, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v2, v3

    .line 1219873
    sget-object v3, LX/0yY;->DIALTONE_VIDEO_INTERSTITIAL:LX/0yY;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1219874
    iget-object v3, v2, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v2, v3

    .line 1219875
    sget-object v3, LX/0yY;->DIALTONE_PHOTO:LX/0yY;

    if-ne v2, v3, :cond_1

    .line 1219876
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, v0, LX/7YR;->d:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-char v5, LX/49i;->b:C

    const-string p0, ""

    invoke-interface {v3, v4, v5, p0}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0a0097

    const p0, 0x7f0a0093

    invoke-static {v2, v3, v4, v5, p0}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v2

    invoke-virtual {v2}, LX/4nm;->a()V

    .line 1219877
    :cond_1
    iget-object v2, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->j()V

    .line 1219878
    :goto_0
    return-void

    .line 1219879
    :cond_2
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->c:LX/1lF;

    sget-object v1, LX/1lF;->UPSELL_FLOW_STARTING:LX/1lF;

    if-ne v0, v1, :cond_3

    .line 1219880
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->d:LX/7YR;

    .line 1219881
    iget-object v1, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 1219882
    new-instance v2, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;

    invoke-direct {v2}, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;-><init>()V

    .line 1219883
    iput-object v0, v2, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;->k:LX/7YR;

    .line 1219884
    const-string v3, "webview_upsell_dialog"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1219885
    goto :goto_0

    .line 1219886
    :cond_3
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->d:LX/7YR;

    iget-object v1, p0, Lcom/facebook/zero/upsell/ui/screencontroller/FbZeroBalanceSpinnerController$1;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1219887
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, v0, LX/7YR;->d:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-char v5, LX/49i;->d:C

    const-string p0, ""

    invoke-interface {v3, v4, v5, p0}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0a0097

    const p0, 0x7f0a0095

    invoke-static {v2, v3, v4, v5, p0}, LX/4nm;->a(Landroid/view/View;Ljava/lang/String;III)LX/4nm;

    move-result-object v2

    invoke-virtual {v2}, LX/4nm;->a()V

    .line 1219888
    iget-object v2, v0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->k()V

    .line 1219889
    goto :goto_0
.end method
