.class public Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# static fields
.field public static final j:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public k:LX/7YR;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1219963
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;->j:LX/2QP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1219964
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x56a77e08

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1219965
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1219966
    const/4 v1, 0x1

    const v2, 0x1030005

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1219967
    const/16 v1, 0x2b

    const v2, 0x63ac5df5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.webkit.WebView.loadUrl"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/16 v0, 0x2a

    const v1, 0x28e0023f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1219968
    new-instance v1, LX/0D4;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0D4;-><init>(Landroid/content/Context;)V

    .line 1219969
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1219970
    invoke-virtual {v1, v2}, LX/0D4;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1219971
    new-instance v2, LX/7YT;

    invoke-direct {v2, p0}, LX/7YT;-><init>(Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;)V

    invoke-virtual {v1, v2}, LX/0D4;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1219972
    const-string v2, "http://pcautivo.telcel.com:8080/captivefb/?interface=native"

    invoke-virtual {v1, v2}, LX/0D4;->loadUrl(Ljava/lang/String;)V

    .line 1219973
    const/16 v2, 0x2b

    const v3, 0x8a68cb1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1219974
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1219975
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;->k:LX/7YR;

    if-eqz v0, :cond_0

    .line 1219976
    iget-object v0, p0, Lcom/facebook/zero/upsell/ui/screencontroller/ZeroBalanceWebviewFragment;->k:LX/7YR;

    .line 1219977
    iget-object p0, v0, LX/7YR;->c:LX/0yW;

    sget-object p1, LX/1lF;->UPSELL_FLOW_FINISHING:LX/1lF;

    invoke-virtual {p0, p1}, LX/0yW;->a(LX/1lF;)V

    .line 1219978
    :cond_0
    return-void
.end method
