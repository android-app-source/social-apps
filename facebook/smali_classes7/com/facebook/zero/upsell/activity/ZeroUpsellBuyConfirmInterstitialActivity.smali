.class public Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final r:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:LX/128;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1219026
    const-class v0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;

    sput-object v0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->r:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1219025
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1219018
    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1219019
    if-nez v0, :cond_0

    .line 1219020
    const/4 v0, 0x0

    .line 1219021
    :goto_0
    return-object v0

    .line 1219022
    :cond_0
    :try_start_0
    sget-object v1, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1219023
    :catch_0
    move-exception v1

    .line 1219024
    iget-object v2, p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->q:LX/03V;

    sget-object v3, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->r:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error decoding query param "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V
    .locals 10

    .prologue
    .line 1219011
    iget-object v0, p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->p:LX/128;

    sget-object v1, LX/0yY;->BUY_CONFIRM_INTERSTITIAL:LX/0yY;

    const/4 v2, 0x0

    new-instance v3, LX/7Y9;

    invoke-direct {v3, p0}, LX/7Y9;-><init>(Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1219012
    iget-object v0, p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->p:LX/128;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    sget-object v2, LX/0yY;->BUY_CONFIRM_INTERSTITIAL:LX/0yY;

    .line 1219013
    invoke-virtual {v0}, LX/128;->a()V

    .line 1219014
    invoke-static {v1, v2}, LX/121;->a(LX/0gc;LX/0yY;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1219015
    sget-object v6, LX/6YN;->BUY_CONFIRM:LX/6YN;

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    move-object v4, v2

    move-object v5, p1

    invoke-static/range {v4 .. v9}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/0yY;Ljava/lang/Object;LX/6YN;ILjava/lang/Object;LX/4g1;)Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    move-result-object v4

    .line 1219016
    iget-object v5, v2, LX/0yY;->prefString:Ljava/lang/String;

    invoke-virtual {v4, v1, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1219017
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;LX/128;LX/03V;)V
    .locals 0

    .prologue
    .line 1219010
    iput-object p1, p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->p:LX/128;

    iput-object p2, p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->q:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;

    invoke-static {v1}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v0

    check-cast v0, LX/128;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;LX/128;LX/03V;)V

    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1219008
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1219009
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1219005
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->setResult(I)V

    .line 1219006
    invoke-virtual {p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->finish()V

    .line 1219007
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1219002
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->setResult(I)V

    .line 1219003
    invoke-virtual {p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->finish()V

    .line 1219004
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1218979
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1218980
    invoke-static {p0, p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1218981
    if-nez p1, :cond_0

    .line 1218982
    invoke-virtual {p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1218983
    if-nez v1, :cond_1

    .line 1218984
    invoke-direct {p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->l()V

    .line 1218985
    :cond_0
    :goto_0
    return-void

    .line 1218986
    :cond_1
    const-string v0, "promo_data_model"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1218987
    if-nez v0, :cond_3

    .line 1218988
    const-string v0, "extra_launch_uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1218989
    if-nez v0, :cond_2

    .line 1218990
    invoke-direct {p0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->l()V

    goto :goto_0

    .line 1218991
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1218992
    const-string v1, "promo_id"

    invoke-direct {p0, v0, v1}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1218993
    const-string v2, "title"

    invoke-direct {p0, v0, v2}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1218994
    const-string v3, "top_message"

    invoke-direct {p0, v0, v3}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1218995
    const-string v4, "promo_name"

    invoke-direct {p0, v0, v4}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1218996
    const-string v5, "promo_price"

    invoke-direct {p0, v0, v5}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1218997
    const-string v6, "message"

    invoke-direct {p0, v0, v6}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1218998
    const-string v7, "button_text"

    invoke-direct {p0, v0, v7}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1218999
    const-string v8, "extra_text"

    invoke-direct {p0, v0, v8}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1219000
    new-instance v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    sget-object v9, LX/6Y4;->MEGAPHONE:LX/6Y4;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Y4;)V

    .line 1219001
    :cond_3
    invoke-direct {p0, v0}, Lcom/facebook/zero/upsell/activity/ZeroUpsellBuyConfirmInterstitialActivity;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    goto :goto_0
.end method
