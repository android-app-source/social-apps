.class public Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/7YJ;

.field public final d:LX/7YK;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1219820
    const-class v0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/7YJ;LX/7YK;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/annotations/DisableZeroTokenBootstrapGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/7YJ;",
            "LX/7YK;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219822
    iput-object p1, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->b:LX/0Or;

    .line 1219823
    iput-object p2, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->c:LX/7YJ;

    .line 1219824
    iput-object p3, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->d:LX/7YK;

    .line 1219825
    iput-object p4, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->e:LX/0Or;

    .line 1219826
    return-void
.end method

.method public static a(Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # LX/0e6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;TPARAMS;)TRESU",
            "LT;"
        }
    .end annotation

    .prologue
    .line 1219827
    iget-object v0, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 1219828
    new-instance v2, LX/14U;

    invoke-direct {v2}, LX/14U;-><init>()V

    .line 1219829
    iget-object v1, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219830
    sget-object v1, LX/14V;->DEFAULT:LX/14V;

    invoke-virtual {v2, v1}, LX/14U;->a(LX/14V;)V

    .line 1219831
    :goto_0
    move-object v1, v2

    .line 1219832
    sget-object v2, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, p2, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 1219833
    :cond_0
    sget-object v1, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v2, v1}, LX/14U;->a(LX/14V;)V

    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 1219834
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1219835
    const-string v1, "zero_buy_promo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219836
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1219837
    const-string v1, "zeroBuyPromoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/model/ZeroPromoParams;

    .line 1219838
    iget-object v1, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->c:LX/7YJ;

    invoke-static {p0, v1, v0}, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->a(Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    .line 1219839
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1219840
    :goto_0
    return-object v0

    .line 1219841
    :cond_0
    const-string v1, "zero_get_recommended_promo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219842
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1219843
    const-string v1, "zeroBuyPromoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;

    .line 1219844
    iget-object v1, p0, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->d:LX/7YK;

    invoke-static {p0, v1, v0}, Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;->a(Lcom/facebook/zero/upsell/service/UpsellPromoServiceHandler;LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    .line 1219845
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1219846
    goto :goto_0

    .line 1219847
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
