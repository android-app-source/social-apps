.class public final Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1219089
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1219090
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1219085
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1219086
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1219091
    if-nez p1, :cond_0

    move v0, v1

    .line 1219092
    :goto_0
    return v0

    .line 1219093
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1219094
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1219095
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1219096
    const v2, 0x531f345a

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1219097
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 1219098
    const v3, 0x638f46eb

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1219099
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1219100
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1219101
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1219102
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1219103
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219104
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1219105
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 1219106
    const v3, -0x443ad4c4

    const/4 v5, 0x0

    .line 1219107
    if-nez v2, :cond_1

    move v4, v5

    .line 1219108
    :goto_1
    move v2, v4

    .line 1219109
    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1219110
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219111
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1219112
    invoke-virtual {p0, p1, v11}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1219113
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1219114
    const/4 v6, 0x5

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1219115
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1219116
    const/4 v7, 0x6

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1219117
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1219118
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1219119
    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    .line 1219120
    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    .line 1219121
    invoke-virtual {p3, v11, v5}, LX/186;->b(II)V

    .line 1219122
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1219123
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1219124
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219125
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1219126
    const-class v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    .line 1219127
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1219128
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1219129
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1219130
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 1219131
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1219132
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 1219133
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 1219134
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1219135
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219136
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1219137
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1219138
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1219139
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1219140
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1219141
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1219142
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1219143
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v6

    .line 1219144
    if-nez v6, :cond_2

    const/4 v4, 0x0

    .line 1219145
    :goto_2
    if-ge v5, v6, :cond_3

    .line 1219146
    invoke-virtual {p0, v2, v5}, LX/15i;->q(II)I

    move-result v7

    .line 1219147
    invoke-static {p0, v7, v3, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v4, v5

    .line 1219148
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1219149
    :cond_2
    new-array v4, v6, [I

    goto :goto_2

    .line 1219150
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x443ad4c4 -> :sswitch_2
        -0x11fa58a4 -> :sswitch_0
        0x531f345a -> :sswitch_1
        0x638f46eb -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1219151
    new-instance v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1219152
    if-eqz p0, :cond_0

    .line 1219153
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1219154
    if-eq v0, p0, :cond_0

    .line 1219155
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1219156
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1219157
    sparse-switch p2, :sswitch_data_0

    .line 1219158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1219159
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1219160
    const v1, 0x531f345a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1219161
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1219162
    const v1, 0x638f46eb

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1219163
    :goto_0
    :sswitch_1
    return-void

    .line 1219164
    :sswitch_2
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1219165
    const v1, -0x443ad4c4

    .line 1219166
    if-eqz v0, :cond_0

    .line 1219167
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1219168
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1219169
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1219170
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1219171
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1219172
    :cond_0
    goto :goto_0

    .line 1219173
    :sswitch_3
    const-class v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$MobileCarrierAccountModel$CarrierAccountUpsellProductsModel$EdgesModel$NodeModel;

    .line 1219174
    invoke-static {v0, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x443ad4c4 -> :sswitch_3
        -0x11fa58a4 -> :sswitch_0
        0x531f345a -> :sswitch_2
        0x638f46eb -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1219175
    if-eqz p1, :cond_0

    .line 1219176
    invoke-static {p0, p1, p2}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1219177
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;

    .line 1219178
    if-eq v0, v1, :cond_0

    .line 1219179
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1219180
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1219181
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1219087
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1219088
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1219080
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1219081
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1219082
    :cond_0
    iput-object p1, p0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1219083
    iput p2, p0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->b:I

    .line 1219084
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1219079
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1219078
    new-instance v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1219075
    iget v0, p0, LX/1vt;->c:I

    .line 1219076
    move v0, v0

    .line 1219077
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1219072
    iget v0, p0, LX/1vt;->c:I

    .line 1219073
    move v0, v0

    .line 1219074
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1219069
    iget v0, p0, LX/1vt;->b:I

    .line 1219070
    move v0, v0

    .line 1219071
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1219066
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1219067
    move-object v0, v0

    .line 1219068
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1219054
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1219055
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1219056
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1219057
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1219058
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1219059
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1219060
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1219061
    invoke-static {v3, v9, v2}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1219062
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1219063
    iget v0, p0, LX/1vt;->c:I

    .line 1219064
    move v0, v0

    .line 1219065
    return v0
.end method
