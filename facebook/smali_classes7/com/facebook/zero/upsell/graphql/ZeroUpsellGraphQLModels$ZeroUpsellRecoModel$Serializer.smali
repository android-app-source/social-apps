.class public final Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1219336
    const-class v0, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;

    new-instance v1, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1219337
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1219335
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1219264
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1219265
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1219266
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1219267
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1219268
    if-eqz v2, :cond_e

    .line 1219269
    const-string v3, "mobile_carrier_account"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219270
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1219271
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1219272
    if-eqz v3, :cond_a

    .line 1219273
    const-string v4, "carrier_account_upsell_products"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219274
    const/4 v6, 0x0

    .line 1219275
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1219276
    invoke-virtual {v1, v3, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219277
    if-eqz v4, :cond_0

    .line 1219278
    const-string v5, "clickable_buy_text"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219279
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219280
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1219281
    if-eqz v4, :cond_5

    .line 1219282
    const-string v5, "edges"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219283
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1219284
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v7

    if-ge v5, v7, :cond_4

    .line 1219285
    invoke-virtual {v1, v4, v5}, LX/15i;->q(II)I

    move-result v7

    .line 1219286
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1219287
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1219288
    if-eqz p0, :cond_1

    .line 1219289
    const-string v0, "formatted_price"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219290
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219291
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1219292
    if-eqz p0, :cond_2

    .line 1219293
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219294
    invoke-static {v1, p0, p1}, LX/7YE;->a(LX/15i;ILX/0nX;)V

    .line 1219295
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1219296
    if-eqz p0, :cond_3

    .line 1219297
    const-string v0, "purchase_button_text"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219298
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219299
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1219300
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1219301
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1219302
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4, v6}, LX/15i;->a(III)I

    move-result v4

    .line 1219303
    if-eqz v4, :cond_6

    .line 1219304
    const-string v5, "max_age_sec"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219305
    invoke-virtual {p1, v4}, LX/0nX;->b(I)V

    .line 1219306
    :cond_6
    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219307
    if-eqz v4, :cond_7

    .line 1219308
    const-string v5, "promo_screen_title"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219309
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219310
    :cond_7
    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219311
    if-eqz v4, :cond_8

    .line 1219312
    const-string v5, "title"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219313
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219314
    :cond_8
    const/4 v4, 0x5

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219315
    if-eqz v4, :cond_9

    .line 1219316
    const-string v5, "top_message"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219317
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219318
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1219319
    :cond_a
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1219320
    if-eqz v3, :cond_d

    .line 1219321
    const-string v4, "mobile_carrier"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219322
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1219323
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219324
    if-eqz v4, :cond_b

    .line 1219325
    const-string v5, "short_name"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219326
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219327
    :cond_b
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1219328
    if-eqz v4, :cond_c

    .line 1219329
    const-string v5, "upsell_logo"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219330
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219331
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1219332
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1219333
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1219334
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1219263
    check-cast p1, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel$Serializer;->a(Lcom/facebook/zero/upsell/graphql/ZeroUpsellGraphQLModels$ZeroUpsellRecoModel;LX/0nX;LX/0my;)V

    return-void
.end method
