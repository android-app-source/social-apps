.class public final Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1217800
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;

    new-instance v1, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1217801
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1217799
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1217753
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1217754
    const/4 v2, 0x0

    .line 1217755
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1217756
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1217757
    :goto_0
    move v1, v2

    .line 1217758
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1217759
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1217760
    new-instance v1, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;

    invoke-direct {v1}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;-><init>()V

    .line 1217761
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1217762
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1217763
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1217764
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1217765
    :cond_0
    return-object v1

    .line 1217766
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1217767
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1217768
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1217769
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1217770
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1217771
    const-string v4, "zero_terms_conditions"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1217772
    const/4 v3, 0x0

    .line 1217773
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1217774
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1217775
    :goto_2
    move v1, v3

    .line 1217776
    goto :goto_1

    .line 1217777
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1217778
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1217779
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1217780
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1217781
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_a

    .line 1217782
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1217783
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1217784
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v7, :cond_6

    .line 1217785
    const-string p0, "clickable_link_text"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1217786
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1217787
    :cond_7
    const-string p0, "clickable_link_url"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1217788
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1217789
    :cond_8
    const-string p0, "custom_legal_terms"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1217790
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1217791
    :cond_9
    const-string p0, "description_text"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1217792
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1217793
    :cond_a
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1217794
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1217795
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1217796
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1217797
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1217798
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_b
    move v1, v3

    move v4, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method
