.class public final Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x37bfdc6f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1217029
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1217030
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1217031
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1217032
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1217033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1217034
    invoke-virtual {p0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->a()Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1217035
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1217036
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1217037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1217038
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1217039
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1217040
    invoke-virtual {p0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->a()Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1217041
    invoke-virtual {p0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->a()Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    .line 1217042
    invoke-virtual {p0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->a()Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1217043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;

    .line 1217044
    iput-object v0, v1, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->e:Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    .line 1217045
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1217046
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1217047
    iget-object v0, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->e:Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    iput-object v0, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->e:Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    .line 1217048
    iget-object v0, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;->e:Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1217049
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;

    invoke-direct {v0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel;-><init>()V

    .line 1217050
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1217051
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1217052
    const v0, -0x2bc11082

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1217053
    const v0, -0x6747e1ce

    return v0
.end method
