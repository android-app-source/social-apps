.class public final Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1218059
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;

    new-instance v1, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1218060
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1218061
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1218062
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1218063
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1218064
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1218065
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1218066
    if-eqz v2, :cond_3

    .line 1218067
    const-string v3, "zero_carrier"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218068
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1218069
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1218070
    if-eqz v3, :cond_0

    .line 1218071
    const-string p0, "carrier_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218072
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218073
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1218074
    if-eqz v3, :cond_1

    .line 1218075
    const-string p0, "logo_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218076
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218077
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1218078
    if-eqz v3, :cond_2

    .line 1218079
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218080
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218081
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1218082
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1218083
    if-eqz v2, :cond_4

    .line 1218084
    const-string v3, "zero_token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218085
    invoke-static {v1, v2, p1, p2}, LX/7XS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1218086
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1218087
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1218088
    check-cast p1, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel$Serializer;->a(Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$FetchZeroTokenQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
