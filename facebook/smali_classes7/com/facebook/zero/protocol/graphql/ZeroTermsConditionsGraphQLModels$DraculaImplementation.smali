.class public final Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1217725
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1217726
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1217746
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1217747
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1217728
    if-nez p1, :cond_0

    .line 1217729
    :goto_0
    return v0

    .line 1217730
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1217731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1217732
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1217733
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1217734
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1217735
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1217736
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1217737
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1217738
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1217739
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1217740
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1217741
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1217742
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1217743
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1217744
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1217745
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x713b4467
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1217727
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1217722
    packed-switch p0, :pswitch_data_0

    .line 1217723
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1217724
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x713b4467
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1217721
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1217719
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->b(I)V

    .line 1217720
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1217748
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1217749
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1217750
    :cond_0
    iput-object p1, p0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1217751
    iput p2, p0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->b:I

    .line 1217752
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1217718
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1217717
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1217714
    iget v0, p0, LX/1vt;->c:I

    .line 1217715
    move v0, v0

    .line 1217716
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1217711
    iget v0, p0, LX/1vt;->c:I

    .line 1217712
    move v0, v0

    .line 1217713
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1217693
    iget v0, p0, LX/1vt;->b:I

    .line 1217694
    move v0, v0

    .line 1217695
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1217708
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1217709
    move-object v0, v0

    .line 1217710
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1217699
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1217700
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1217701
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1217702
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1217703
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1217704
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1217705
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1217706
    invoke-static {v3, v9, v2}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1217707
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1217696
    iget v0, p0, LX/1vt;->c:I

    .line 1217697
    move v0, v0

    .line 1217698
    return v0
.end method
