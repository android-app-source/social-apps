.class public final Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1217927
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1217928
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1217929
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1217930
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const v3, -0x601ce3f2

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 1217931
    if-nez p1, :cond_0

    .line 1217932
    :goto_0
    return v0

    .line 1217933
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1217934
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1217935
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1217936
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1217937
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1217938
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1217939
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1217940
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1217941
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1217942
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1217943
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1217944
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 1217945
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1217946
    :sswitch_1
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1217947
    const v1, 0x52759548

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1217948
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v1

    .line 1217949
    invoke-static {p0, v1, v3, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1217950
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 1217951
    invoke-static {p0, v2, v3, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1217952
    const/4 v3, 0x4

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1217953
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1217954
    invoke-virtual {p3, v5, v1}, LX/186;->b(II)V

    .line 1217955
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1217956
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1217957
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1217958
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1217959
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1217960
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1217961
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1217962
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1217963
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1217964
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1217965
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1217966
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 1217967
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1217968
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1217969
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1217970
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1217971
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1217972
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1217973
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1217974
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1217975
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x601ce3f2 -> :sswitch_3
        -0x39369940 -> :sswitch_0
        0x52759548 -> :sswitch_2
        0x548640fc -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1217976
    if-nez p0, :cond_0

    move v0, v1

    .line 1217977
    :goto_0
    return v0

    .line 1217978
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1217979
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1217980
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1217981
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1217982
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1217983
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1217984
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1218003
    const/4 v7, 0x0

    .line 1218004
    const/4 v1, 0x0

    .line 1218005
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1218006
    invoke-static {v2, v3, v0}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1218007
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1218008
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1218009
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1217985
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const v2, -0x601ce3f2

    .line 1217887
    sparse-switch p2, :sswitch_data_0

    .line 1217888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1217889
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1217890
    const v1, 0x52759548

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1217891
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1217892
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1217893
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1217894
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1217895
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x601ce3f2 -> :sswitch_1
        -0x39369940 -> :sswitch_1
        0x52759548 -> :sswitch_1
        0x548640fc -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1217986
    if-nez p1, :cond_0

    move v0, v1

    .line 1217987
    :goto_0
    return v0

    .line 1217988
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1217989
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1217990
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1217991
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1217992
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1217993
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1217994
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1217995
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1217996
    if-eqz p1, :cond_0

    .line 1217997
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1217998
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1217999
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1218000
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1218001
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1218002
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1217920
    if-eqz p1, :cond_0

    .line 1217921
    invoke-static {p0, p1, p2}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1217922
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    .line 1217923
    if-eq v0, v1, :cond_0

    .line 1217924
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1217925
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1217926
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1217878
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1217879
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1217880
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1217881
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1217882
    :cond_0
    iput-object p1, p0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1217883
    iput p2, p0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->b:I

    .line 1217884
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1217885
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1217886
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1217896
    iget v0, p0, LX/1vt;->c:I

    .line 1217897
    move v0, v0

    .line 1217898
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1217899
    iget v0, p0, LX/1vt;->c:I

    .line 1217900
    move v0, v0

    .line 1217901
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1217902
    iget v0, p0, LX/1vt;->b:I

    .line 1217903
    move v0, v0

    .line 1217904
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1217905
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1217906
    move-object v0, v0

    .line 1217907
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1217908
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1217909
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1217910
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1217911
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1217912
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1217913
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1217914
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1217915
    invoke-static {v3, v9, v2}, Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/zero/protocol/graphql/ZeroTokenGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1217916
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1217917
    iget v0, p0, LX/1vt;->c:I

    .line 1217918
    move v0, v0

    .line 1217919
    return v0
.end method
