.class public final Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x89e4b2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1217025
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1217024
    const-class v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1217022
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1217023
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1217017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1217018
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1217019
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1217020
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1217021
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1217026
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1217027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1217028
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1217014
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1217015
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;->e:Z

    .line 1217016
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1217012
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1217013
    iget-boolean v0, p0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1217009
    new-instance v0, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;

    invoke-direct {v0}, Lcom/facebook/zero/protocol/graphql/ZeroIncentivesGraphQLModels$FetchZeroIncentivesQueryModel$ZeroIncentivesModel;-><init>()V

    .line 1217010
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1217011
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1217008
    const v0, 0x69ef1dcc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1217007
    const v0, -0x353f9f56    # -6303829.0f

    return v0
.end method
