.class public Lcom/facebook/zero/ui/ExtraChargesDialogFragment;
.super Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;
.source ""


# instance fields
.field public t:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/6YV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1218874
    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;-><init>()V

    .line 1218875
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->x:Z

    .line 1218876
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0x1483

    invoke-static {v3, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v2

    check-cast v2, LX/0yH;

    invoke-static {v3}, LX/7YQ;->b(LX/0QB;)LX/7YQ;

    move-result-object v3

    check-cast v3, LX/6YV;

    iput-object v1, p1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->u:LX/0Or;

    iput-object v2, p1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->v:LX/0yH;

    iput-object v3, p1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->w:LX/6YV;

    return-void
.end method

.method public static b(LX/0yY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;LX/4g1;)Lcom/facebook/zero/ui/ExtraChargesDialogFragment;
    .locals 8
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1218877
    new-instance v0, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;

    invoke-direct {v0}, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;-><init>()V

    .line 1218878
    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v2 .. v7}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;LX/4g1;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)Landroid/os/Bundle;

    move-result-object v2

    move-object v1, v2

    .line 1218879
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1218880
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1218881
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1218882
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1218883
    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1218884
    const-string v0, "zero_extra_charges_dialog_open"

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1218885
    const-string v0, "zero_extra_charges_dialog_confirm"

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4a3f489b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1218886
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1218887
    const-class v1, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1218888
    const/4 v1, 0x0

    const v2, 0x7f0e03f3

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1218889
    const/16 v1, 0x2b

    const v2, -0x2f326da3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x73fc4b7f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1218890
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1218891
    new-instance v0, LX/7Xx;

    invoke-direct {v0, p0}, LX/7Xx;-><init>(Lcom/facebook/zero/ui/ExtraChargesDialogFragment;)V

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1218892
    new-instance v0, LX/6Y5;

    invoke-direct {v0}, LX/6Y5;-><init>()V

    iget-object v3, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->o:Ljava/lang/String;

    .line 1218893
    iput-object v3, v0, LX/6Y5;->c:Ljava/lang/String;

    .line 1218894
    move-object v0, v0

    .line 1218895
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/6Y5;->a(Ljava/lang/Boolean;)LX/6Y5;

    move-result-object v0

    const v3, 0x7f080017

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/7Xz;

    invoke-direct {v4, p0}, LX/7Xz;-><init>(Lcom/facebook/zero/ui/ExtraChargesDialogFragment;)V

    invoke-virtual {v0, v3, v4}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    sget-object v4, LX/4g1;->UPSELL_WITH_SMS:LX/4g1;

    if-ne v0, v4, :cond_2

    const v0, 0x7f080016

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/7Xy;

    invoke-direct {v4, p0}, LX/7Xy;-><init>(Lcom/facebook/zero/ui/ExtraChargesDialogFragment;)V

    invoke-virtual {v3, v0, v4}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    .line 1218896
    iget-object v3, p0, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->w:LX/6YV;

    iget-object v4, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    invoke-interface {v3, v4}, LX/6YV;->b(LX/0yY;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    sget-object v4, LX/4g1;->UPSELL_WITH_SMS:LX/4g1;

    if-ne v3, v4, :cond_1

    .line 1218897
    :cond_0
    new-instance v3, LX/7Y0;

    invoke-direct {v3, p0}, LX/7Y0;-><init>(Lcom/facebook/zero/ui/ExtraChargesDialogFragment;)V

    .line 1218898
    iput-object v3, v0, LX/6Y5;->p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1218899
    iget-object v3, p0, Lcom/facebook/zero/ui/ExtraChargesDialogFragment;->w:LX/6YV;

    invoke-interface {v3}, LX/6YV;->a()Z

    move-result v3

    .line 1218900
    iput-boolean v3, v0, LX/6Y5;->q:Z

    .line 1218901
    :cond_1
    new-instance v3, LX/6YP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1218902
    invoke-virtual {v3, v0}, LX/6YP;->a(LX/6Y5;)V

    .line 1218903
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1218904
    const v0, -0x242e78eb

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 1218905
    :cond_2
    const v0, 0x7f080e56

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1218906
    const-string v0, "zero_extra_charges_dialog_cancel"

    return-object v0
.end method
