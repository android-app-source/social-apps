.class public Lcom/facebook/zero/service/ZeroUpdateStatusManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile g:Lcom/facebook/zero/service/ZeroUpdateStatusManager;


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1218702
    const-class v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    sput-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->a:Ljava/lang/Class;

    .line 1218703
    const-class v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1218705
    iput-object p1, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->c:LX/0Ot;

    .line 1218706
    iput-object p2, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->d:LX/0Ot;

    .line 1218707
    iput-object p3, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->e:LX/0Ot;

    .line 1218708
    iput-object p5, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->f:LX/0Ot;

    .line 1218709
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/zero/service/ZeroUpdateStatusManager;
    .locals 9

    .prologue
    .line 1218710
    sget-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->g:Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    if-nez v0, :cond_1

    .line 1218711
    const-class v1, Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    monitor-enter v1

    .line 1218712
    :try_start_0
    sget-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->g:Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1218713
    if-eqz v2, :cond_0

    .line 1218714
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1218715
    new-instance v3, Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    const/16 v4, 0x542

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13ec

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1c6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x140f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/zero/service/ZeroUpdateStatusManager;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1218716
    move-object v0, v3

    .line 1218717
    sput-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->g:Lcom/facebook/zero/service/ZeroUpdateStatusManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1218719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1218720
    :cond_1
    sget-object v0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->g:Lcom/facebook/zero/service/ZeroUpdateStatusManager;

    return-object v0

    .line 1218721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1218722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1218723
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1218724
    new-instance v5, Lcom/facebook/zero/server/ZeroUpdateStatusParams;

    iget-object v3, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1pA;

    invoke-virtual {v3}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v6

    iget-object v3, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1pA;

    invoke-virtual {v3}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v3, p1}, Lcom/facebook/zero/server/ZeroUpdateStatusParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;)V

    .line 1218725
    const-string v3, "zeroUpdateStatusParams"

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1218726
    const-string v3, "zero_update_status"

    .line 1218727
    iget-object v7, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v11, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v12, -0x333d800b

    move-object v8, v3

    move-object v9, v4

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    .line 1218728
    sget-object v8, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v7, v8}, LX/1ML;->updatePriority(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 1218729
    move-object v3, v7

    .line 1218730
    move-object v1, v3

    .line 1218731
    new-instance v2, LX/7Xn;

    invoke-direct {v2, p0, p1}, LX/7Xn;-><init>(Lcom/facebook/zero/service/ZeroUpdateStatusManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroUpdateStatusManager;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1218732
    return-void
.end method
