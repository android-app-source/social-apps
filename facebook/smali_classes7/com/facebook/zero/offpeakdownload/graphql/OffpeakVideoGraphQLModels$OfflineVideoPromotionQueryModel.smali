.class public final Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x331e57ab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1216573
    const-class v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1216572
    const-class v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1216570
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1216571
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1216564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1216565
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1216566
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1216567
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1216568
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1216569
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1216556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1216557
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1216558
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    .line 1216559
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1216560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    .line 1216561
    iput-object v0, v1, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->e:Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    .line 1216562
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1216563
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216554
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->e:Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->e:Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    .line 1216555
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;->e:Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1216551
    new-instance v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;

    invoke-direct {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel;-><init>()V

    .line 1216552
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1216553
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1216549
    const v0, -0x20e98f55

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1216550
    const v0, -0x6747e1ce

    return v0
.end method
