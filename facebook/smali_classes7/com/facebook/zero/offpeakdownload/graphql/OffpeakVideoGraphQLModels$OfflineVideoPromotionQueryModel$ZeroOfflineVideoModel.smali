.class public final Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6dd5b3b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1216506
    const-class v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1216507
    const-class v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1216508
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1216509
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1216510
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1216511
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1216512
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1216513
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1216514
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1216515
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1216516
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1216517
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1216518
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1216519
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->s()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1216520
    invoke-virtual {p0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->t()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1216521
    const/16 v10, 0xc

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1216522
    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1216523
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1216524
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1216525
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1216526
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1216527
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1216528
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1216529
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1216530
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->m:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1216531
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->n:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1216532
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1216533
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1216534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1216535
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1216546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1216547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1216548
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216536
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->e:Ljava/lang/String;

    .line 1216537
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1216538
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1216539
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->m:I

    .line 1216540
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->n:I

    .line 1216541
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1216542
    new-instance v0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;

    invoke-direct {v0}, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;-><init>()V

    .line 1216543
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1216544
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1216545
    const v0, 0x5be383fd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1216503
    const v0, 0x66d18a9a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216504
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->f:Ljava/lang/String;

    .line 1216505
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216501
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->g:Ljava/lang/String;

    .line 1216502
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216499
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->h:Ljava/lang/String;

    .line 1216500
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216497
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->i:Ljava/lang/String;

    .line 1216498
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216495
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->j:Ljava/lang/String;

    .line 1216496
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216493
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->k:Ljava/lang/String;

    .line 1216494
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216491
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->l:Ljava/lang/String;

    .line 1216492
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 1216489
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1216490
    iget v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->m:I

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1216483
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1216484
    iget v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->n:I

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216487
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->o:Ljava/lang/String;

    .line 1216488
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1216485
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->p:Ljava/lang/String;

    .line 1216486
    iget-object v0, p0, Lcom/facebook/zero/offpeakdownload/graphql/OffpeakVideoGraphQLModels$OfflineVideoPromotionQueryModel$ZeroOfflineVideoModel;->p:Ljava/lang/String;

    return-object v0
.end method
