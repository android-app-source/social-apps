.class public final Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/1Be;


# direct methods
.method public constructor <init>(LX/1Be;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1226751
    iput-object p1, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->c:LX/1Be;

    iput-object p2, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1226752
    iget-object v0, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bn;

    iget-object v1, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->a:Ljava/lang/String;

    const/4 v2, 0x0

    .line 1226753
    iget-object v3, v0, LX/1Bn;->i:LX/3n3;

    if-eqz v3, :cond_0

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1226754
    :cond_0
    :goto_0
    move-object v0, v2

    .line 1226755
    if-nez v0, :cond_1

    .line 1226756
    :goto_1
    return-void

    .line 1226757
    :cond_1
    iget-object v1, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->c:LX/1Be;

    new-instance v2, LX/1Bt;

    iget-object v3, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->b:Ljava/util/List;

    .line 1226758
    iget v4, v0, LX/1By;->f:I

    move v0, v4

    .line 1226759
    iget-object v4, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v5, v4}, LX/1Bt;-><init>(Ljava/util/List;IZLjava/lang/String;)V

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    invoke-virtual {v1, v2, v0}, LX/1Be;->a(LX/1Bt;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;)V

    goto :goto_1

    :cond_2
    invoke-static {v1}, LX/047;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3, v5}, LX/1Bn;->a(LX/1Bn;Ljava/lang/String;Ljava/lang/String;Z)LX/1By;

    move-result-object v2

    goto :goto_0
.end method
