.class public final Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:LX/1Be;


# direct methods
.method public constructor <init>(LX/1Be;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1226760
    iput-object p1, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->d:LX/1Be;

    iput-object p2, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1226761
    iget-object v0, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->d:LX/1Be;

    iget-object v1, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/browser/prefetch/BrowserPrefetcher$2;->c:Z

    const/4 v9, 0x1

    .line 1226762
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 1226763
    iget-object v4, v0, LX/1Be;->m:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->d()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1226764
    :cond_0
    :goto_0
    return-void

    .line 1226765
    :cond_1
    invoke-static {v0}, LX/1Be;->p(LX/1Be;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1226766
    iget-object v4, v0, LX/1Be;->h:Ljava/util/concurrent/ExecutorService;

    .line 1226767
    new-instance v5, LX/7hx;

    invoke-direct {v5, v0, v1, v3, v2}, LX/7hx;-><init>(LX/1Be;Ljava/lang/String;ZLjava/lang/String;)V

    move-object v5, v5

    .line 1226768
    const v6, 0x10bb59a7

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v4

    .line 1226769
    const-wide/16 v6, 0x4e20

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v8, -0x2482db28

    invoke-static {v4, v6, v7, v5, v8}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 1226770
    :catch_0
    invoke-interface {v4, v9}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 1226771
    :catch_1
    invoke-interface {v4, v9}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    .line 1226772
    :catch_2
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    aput-object v2, v4, v9

    goto :goto_0
.end method
