.class public Lcom/facebook/facerec/job/TagSuggestFetchJob;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LX/7ya;

.field private final b:LX/0Sh;

.field private final c:LX/03V;

.field private final d:LX/18V;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final g:LX/7yd;

.field private final h:Ljava/lang/String;

.field private final i:J

.field private j:LX/4d1;

.field private volatile k:Z

.field private l:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method public constructor <init>(LX/7ya;LX/0Sh;LX/03V;LX/18V;Ljava/util/List;Lcom/facebook/performancelogger/PerformanceLogger;LX/7yd;Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facerec/manager/FaceRecManager$TagSuggestFetchCompletedListener;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/7yd;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1279389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279390
    iput-object p1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a:LX/7ya;

    .line 1279391
    iput-object p2, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->b:LX/0Sh;

    .line 1279392
    iput-object p3, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->c:LX/03V;

    .line 1279393
    iput-object p4, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->d:LX/18V;

    .line 1279394
    iput-object p5, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->e:Ljava/util/List;

    .line 1279395
    iput-object p6, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1279396
    iput-object p7, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->g:LX/7yd;

    .line 1279397
    iput-object p8, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->h:Ljava/lang/String;

    .line 1279398
    iput-wide p9, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->i:J

    .line 1279399
    new-instance v0, LX/4d1;

    invoke-direct {v0}, LX/4d1;-><init>()V

    iput-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->j:LX/4d1;

    .line 1279400
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->k:Z

    .line 1279401
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    .line 1279402
    return-void
.end method

.method private a(LX/7yj;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yj;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1279372
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1279373
    iget-object v0, p1, LX/7yj;->b:Ljava/util/Map;

    move-object v3, v0

    .line 1279374
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279375
    iget-object v1, v0, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279376
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1279377
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1279378
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1279379
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1279380
    :cond_0
    new-instance v5, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279381
    iget-object p0, v0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    move-object v0, p0

    .line 1279382
    invoke-direct {v5, v0, v1, v6}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;Z)V

    .line 1279383
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279384
    :cond_1
    new-instance v1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279385
    iget-object v5, v0, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    move-object v0, v5

    .line 1279386
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v0, v5, v6}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;Z)V

    .line 1279387
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279388
    :cond_2
    return-object v2
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1279367
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279368
    const/4 v2, 0x0

    .line 1279369
    iput-object v2, v0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    .line 1279370
    goto :goto_0

    .line 1279371
    :cond_0
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    .line 1279357
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a:LX/7ya;

    .line 1279358
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1279359
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279360
    new-instance v4, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279361
    iget-object v5, v1, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    move-object v1, v5

    .line 1279362
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    invoke-direct {v4, v1, v5, v6}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;Z)V

    .line 1279363
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279364
    :cond_0
    move-object v1, v2

    .line 1279365
    invoke-virtual {v0, v1}, LX/7ya;->a(Ljava/util/List;)V

    .line 1279366
    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 4

    .prologue
    .line 1279289
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1279290
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    .line 1279291
    if-eqz v0, :cond_0

    .line 1279292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->k:Z

    .line 1279293
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "marking "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to no-op"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279294
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1279295
    :goto_0
    return-void

    .line 1279296
    :cond_0
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->j:LX/4d1;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1279297
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->j:LX/4d1;

    invoke-virtual {v0}, LX/4d1;->a()Z

    .line 1279298
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a:LX/7ya;

    .line 1279299
    iget-object v1, v0, LX/7ya;->a:LX/7yc;

    iget-object v1, v1, LX/7yc;->d:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facerec/manager/FaceRecManager$TagSuggestFetchCompletedListenerImpl$2;

    invoke-direct {v2, v0, p2}, Lcom/facebook/facerec/manager/FaceRecManager$TagSuggestFetchCompletedListenerImpl$2;-><init>(LX/7ya;Z)V

    const v3, -0x74756bae

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1279300
    goto :goto_0

    .line 1279301
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": network request in progress, ignoring abort request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 1279302
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1279303
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1279304
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->k:Z

    if-eqz v0, :cond_0

    .line 1279305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": abort request, I\'m going home"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279306
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a()V

    .line 1279307
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1279308
    :goto_0
    return-void

    .line 1279309
    :cond_0
    :try_start_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1279310
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279311
    iget-object v3, v0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v3, v3

    .line 1279312
    if-eqz v3, :cond_1

    .line 1279313
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1279314
    :cond_2
    move-object v1, v1

    .line 1279315
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1279316
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279317
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a()V

    .line 1279318
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1279319
    :cond_3
    :try_start_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279320
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1279321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": suggestions already present, aborting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1279322
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a()V

    .line 1279323
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1279324
    :cond_5
    :try_start_3
    new-instance v2, LX/14U;

    invoke-direct {v2}, LX/14U;-><init>()V

    .line 1279325
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->j:LX/4d1;

    .line 1279326
    iput-object v0, v2, LX/14U;->c:LX/4d1;

    .line 1279327
    new-instance v3, LX/7yi;

    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->c:LX/03V;

    iget-object v4, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->h:Ljava/lang/String;

    invoke-direct {v3, v0, v1, v4}, LX/7yi;-><init>(LX/03V;Ljava/util/List;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1279328
    :try_start_4
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x3b0001

    const-string v5, "FaceRecServerCommunication"

    invoke-interface {v0, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 1279329
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Sending "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " crops for photo "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->h:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279330
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279331
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Crop with width: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279332
    iget v5, v0, Lcom/facebook/photos/base/tagging/FaceBox;->j:I

    move v5, v5

    .line 1279333
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " height: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1279334
    iget v5, v0, Lcom/facebook/photos/base/tagging/FaceBox;->k:I

    move v5, v5

    .line 1279335
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " byte size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1279336
    iget-object v5, v0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v5, v5

    .line 1279337
    if-eqz v5, :cond_6

    .line 1279338
    iget-object v5, v0, Lcom/facebook/photos/base/tagging/FaceBox;->i:[B

    move-object v0, v5

    .line 1279339
    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 1279340
    :catch_0
    move-exception v0

    .line 1279341
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1279342
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a()V

    .line 1279343
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 1279344
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 1279345
    :cond_7
    :try_start_6
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->d:LX/18V;

    new-instance v1, LX/7yh;

    iget-object v4, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->g:LX/7yd;

    iget-object v5, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v1, v4, v5}, LX/7yh;-><init>(LX/7yd;Lcom/facebook/performancelogger/PerformanceLogger;)V

    invoke-virtual {v0, v1, v3, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yj;

    .line 1279346
    invoke-direct {p0, v0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a(LX/7yj;)Ljava/util/List;

    move-result-object v0

    .line 1279347
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x3b0001

    const-string v3, "FaceRecServerCommunication"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1279348
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a:LX/7ya;

    invoke-virtual {v1, v0}, LX/7ya;->a(Ljava/util/List;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1279349
    :goto_4
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->a()V

    .line 1279350
    iget-object v0, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->l:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 1279351
    :catch_1
    move-exception v0

    .line 1279352
    :try_start_7
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x3b0001

    const-string v3, "FaceRecServerCommunication"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 1279353
    iget-boolean v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->k:Z

    if-nez v1, :cond_8

    .line 1279354
    const-string v1, "TagSuggestFetchJob"

    const-string v2, "aborting"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1279355
    iget-object v1, p0, Lcom/facebook/facerec/job/TagSuggestFetchJob;->c:LX/03V;

    const-string v2, "TagSuggestFetchJob"

    const-string v3, "FaceRecMethod threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1279356
    :cond_8
    invoke-direct {p0}, Lcom/facebook/facerec/job/TagSuggestFetchJob;->c()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4
.end method
