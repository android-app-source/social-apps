.class public final Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7ye;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;


# direct methods
.method public constructor <init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1279429
    iput-object p1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iput-object p2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->a:LX/7ye;

    iput-object p3, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1279430
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1279431
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->a:LX/7ye;

    invoke-virtual {v0, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d(LX/7ye;)LX/7ye;

    move-result-object v2

    .line 1279432
    iget-object v3, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v4, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->a:LX/7ye;

    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v5, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->a:LX/7ye;

    invoke-virtual {v0, v5}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/7ye;Z)V

    .line 1279433
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-boolean v0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c:Z

    if-nez v0, :cond_2

    .line 1279434
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->b:Ljava/util/List;

    invoke-virtual {v2}, LX/7ye;->c()Ljava/util/List;

    move-result-object v3

    .line 1279435
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1279436
    :cond_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->b:Ljava/util/List;

    .line 1279437
    iget-object v3, v2, LX/7ye;->f:LX/75F;

    iget-object v4, v2, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v3, v4, v0}, LX/75F;->a(LX/74x;Ljava/util/List;)V

    .line 1279438
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    .line 1279439
    iget-object v6, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    if-eqz v6, :cond_1

    .line 1279440
    iget-object v6, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    invoke-interface {v6, v2}, LX/7yZ;->a(LX/7ye;)V

    .line 1279441
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Got face boxes for photo "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/7ye;->g()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1279442
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b(LX/7ye;)V

    .line 1279443
    :cond_2
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iput-boolean v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b:Z

    .line 1279444
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer$3;->c:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h()V

    .line 1279445
    return-void

    :cond_3
    move v0, v1

    .line 1279446
    goto :goto_0

    .line 1279447
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279448
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/base/tagging/Tag;

    .line 1279449
    iget-object v8, v5, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v8, v8

    .line 1279450
    instance-of v8, v8, Lcom/facebook/photos/base/tagging/FaceBoxStub;

    if-nez v8, :cond_7

    .line 1279451
    iget-object v8, v5, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v8, v8

    .line 1279452
    instance-of v8, v8, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v8, :cond_6

    .line 1279453
    :cond_7
    iget-object v8, v5, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v8, v8

    .line 1279454
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1279455
    invoke-interface {v8}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v11

    .line 1279456
    invoke-interface {v4}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v12

    .line 1279457
    if-eqz v11, :cond_8

    if-nez v12, :cond_9

    .line 1279458
    :cond_8
    :goto_2
    move v8, v9

    .line 1279459
    if-eqz v8, :cond_6

    .line 1279460
    iput-object v4, v5, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1279461
    const/4 v5, 0x1

    .line 1279462
    iput-boolean v5, v4, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1279463
    goto :goto_1

    .line 1279464
    :cond_9
    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v13

    if-nez v13, :cond_a

    invoke-virtual {v12, v11}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v13

    if-eqz v13, :cond_b

    :cond_a
    move v9, v10

    .line 1279465
    goto :goto_2

    .line 1279466
    :cond_b
    invoke-static {v11, v12}, Landroid/graphics/RectF;->intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 1279467
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13, v11}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1279468
    invoke-virtual {v13, v12}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    .line 1279469
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v11}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1279470
    invoke-virtual {v0, v12}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 1279471
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v11

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v12

    mul-float/2addr v11, v12

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v12

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v13

    mul-float/2addr v12, v13

    div-float/2addr v11, v12

    const/high16 v12, 0x3f000000    # 0.5f

    cmpl-float v11, v11, v12

    if-ltz v11, :cond_8

    move v9, v10

    goto :goto_2
.end method
