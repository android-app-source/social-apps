.class public final Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7yd;


# direct methods
.method public constructor <init>(LX/7yd;)V
    .locals 0

    .prologue
    .line 1279820
    iput-object p1, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1279821
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1279822
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->g:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1279823
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot initialize the local suggestions twice"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1279824
    :cond_0
    new-instance v12, LX/0Pz;

    invoke-direct {v12}, LX/0Pz;-><init>()V

    .line 1279825
    const-wide/16 v4, 0x0

    .line 1279826
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->b:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1279827
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->b:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v6

    .line 1279828
    iget-object v0, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v0

    .line 1279829
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1279830
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->d:LX/3iT;

    .line 1279831
    iget-object v0, v6, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v0

    .line 1279832
    invoke-virtual {v6}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/7Gr;->SELF:LX/7Gr;

    invoke-virtual/range {v2 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v2

    .line 1279833
    invoke-virtual {v12, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_1
    move-wide v10, v4

    .line 1279834
    const/4 v3, 0x0

    .line 1279835
    :try_start_0
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->f:LX/2RQ;

    sget-object v4, LX/2RU;->FACEBOOK_FRIENDS_TYPES:LX/0Px;

    const/16 v5, 0x14

    invoke-virtual {v2, v4, v5}, LX/2RQ;->a(Ljava/util/Collection;I)LX/2RR;

    move-result-object v2

    .line 1279836
    iget-object v4, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v4, v4, LX/7yd;->c:LX/3LP;

    invoke-virtual {v4, v2}, LX/3LP;->a(LX/2RR;)LX/3On;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v9, v2

    .line 1279837
    :goto_0
    if-eqz v9, :cond_4

    .line 1279838
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v9}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/user/model/User;

    move-object v8, v0

    if-eqz v8, :cond_3

    .line 1279839
    iget-object v0, v8, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v0

    .line 1279840
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1279841
    cmp-long v2, v4, v10

    if-eqz v2, :cond_2

    .line 1279842
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v2, v2, LX/7yd;->d:LX/3iT;

    .line 1279843
    iget-object v0, v8, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, v0

    .line 1279844
    invoke-virtual {v8}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/7Gr;->USER:LX/7Gr;

    invoke-virtual/range {v2 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v2

    .line 1279845
    invoke-virtual {v12, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1279846
    iget-object v3, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v3, v3, LX/7yd;->h:Ljava/util/Map;

    .line 1279847
    iget-object v0, v8, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v0

    .line 1279848
    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1279849
    :catchall_0
    move-exception v2

    invoke-interface {v9}, LX/3On;->close()V

    throw v2

    .line 1279850
    :catch_0
    move-exception v2

    .line 1279851
    iget-object v4, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    iget-object v4, v4, LX/7yd;->e:LX/03V;

    const-string v5, "LocalSuggestionsStore"

    const-string v6, "Failed to query top friends; no suggestions"

    invoke-virtual {v4, v5, v6, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v3

    goto :goto_0

    .line 1279852
    :cond_3
    invoke-interface {v9}, LX/3On;->close()V

    .line 1279853
    :cond_4
    iget-object v2, p0, Lcom/facebook/facerec/manager/LocalSuggestionsStore$LocalSuggestionsInitRunnable;->a:LX/7yd;

    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1279854
    iput-object v3, v2, LX/7yd;->g:LX/0Px;

    .line 1279855
    return-void
.end method
