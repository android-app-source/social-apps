.class public Lcom/facebook/facerec/manager/FaceBoxPrioritizer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7ye;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8HH;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/03V;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7yc;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/747;

.field public final j:LX/0Sh;

.field public final k:Ljava/util/concurrent/Executor;

.field public final l:Ljava/util/concurrent/ExecutorService;

.field public m:LX/1HI;

.field public n:LX/7yZ;

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field private final q:LX/75F;

.field public r:I

.field private s:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/7ye;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7ye;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1279729
    const-class v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/747;LX/03V;LX/0Ot;LX/0Ot;LX/75F;LX/1HI;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/747;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/8HH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7yc;",
            ">;",
            "LX/75F;",
            "LX/1HI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279714
    iput-object p4, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->i:LX/747;

    .line 1279715
    iput-object p5, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->g:LX/03V;

    .line 1279716
    iput-object p7, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h:LX/0Ot;

    .line 1279717
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    .line 1279718
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    .line 1279719
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279720
    iput-object p6, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->f:LX/0Ot;

    .line 1279721
    iput-object p8, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->q:LX/75F;

    .line 1279722
    iput-object p1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->j:LX/0Sh;

    .line 1279723
    iput-object p2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->k:Ljava/util/concurrent/Executor;

    .line 1279724
    iput-object p3, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->l:Ljava/util/concurrent/ExecutorService;

    .line 1279725
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    .line 1279726
    iput-object p9, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->m:LX/1HI;

    .line 1279727
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8HH;

    new-instance p1, LX/7yY;

    invoke-direct {p1, p0}, LX/7yY;-><init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;)V

    invoke-virtual {v0, p1}, LX/8HH;->a(LX/7yX;)V

    .line 1279728
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/facerec/manager/FaceBoxPrioritizer;
    .locals 1

    .prologue
    .line 1279712
    invoke-static {p0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b(LX/0QB;)Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/photos/base/tagging/FaceBox;LX/7ye;)Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1279676
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 1279677
    iget-boolean v0, p1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v0, v0

    .line 1279678
    if-nez v0, :cond_0

    .line 1279679
    iget-object v0, p2, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279680
    if-eqz v0, :cond_0

    .line 1279681
    iget-object v0, p2, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279682
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1279683
    if-eqz v0, :cond_0

    .line 1279684
    iget-object v0, p2, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279685
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1279686
    instance-of v0, v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v0, :cond_0

    .line 1279687
    iget-object v0, p2, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279688
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1279689
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279690
    iget-boolean v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v0, v1

    .line 1279691
    if-eqz v0, :cond_1

    :cond_0
    move v6, v2

    .line 1279692
    :goto_0
    return v6

    .line 1279693
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->p:Z

    if-eqz v0, :cond_2

    .line 1279694
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->i:LX/747;

    .line 1279695
    sget-object v1, LX/746;->PREFILLED_TAG_SKIPPED:LX/746;

    invoke-static {v1}, LX/747;->a(LX/746;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/747;->a(LX/747;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1279696
    move v6, v2

    .line 1279697
    goto :goto_0

    .line 1279698
    :cond_2
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->i:LX/747;

    .line 1279699
    sget-object v1, LX/746;->PREFILLED_TAG_CREATED:LX/746;

    invoke-static {v1}, LX/747;->a(LX/746;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/747;->a(LX/747;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1279700
    new-instance v1, Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279701
    iget-object v3, v0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 1279702
    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279703
    iget-wide v9, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v9

    .line 1279704
    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279705
    iget-object v2, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v7, v2

    .line 1279706
    move-object v2, p1

    move v8, v6

    invoke-direct/range {v1 .. v8}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;Z)V

    .line 1279707
    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->i()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/photos/base/tagging/Tag;->a(Ljava/util/Map;)V

    .line 1279708
    iput-boolean v6, p1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1279709
    invoke-virtual {p2}, LX/7ye;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1279710
    iget-object v0, p2, LX/7ye;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1279711
    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/facerec/manager/FaceBoxPrioritizer;
    .locals 10

    .prologue
    .line 1279674
    new-instance v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/747;->b(LX/0QB;)LX/747;

    move-result-object v4

    check-cast v4, LX/747;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x2e50

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1c2e

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v8

    check-cast v8, LX/75F;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v9

    check-cast v9, LX/1HI;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;-><init>(LX/0Sh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/747;LX/03V;LX/0Ot;LX/0Ot;LX/75F;LX/1HI;)V

    .line 1279675
    return-object v0
.end method

.method public static b(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;Ljava/util/List;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1279657
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    if-nez v0, :cond_0

    .line 1279658
    :goto_0
    return v1

    .line 1279659
    :cond_0
    const/4 v2, 0x1

    .line 1279660
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v3, v1

    .line 1279661
    :goto_1
    if-ge v3, v4, :cond_6

    .line 1279662
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1279663
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->g:LX/03V;

    const-string v5, "FaceBoxPrioritizer: suggestions has null"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mDefaultTagSuggestions is null at index "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279664
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1279665
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->g:LX/03V;

    const-string v5, "FaceBoxPrioritizer: suggestions has null"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "suggestions is null at index "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279666
    :cond_2
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1279667
    :cond_3
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279668
    iget-wide v10, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v6, v10

    .line 1279669
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279670
    iget-wide v10, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v8, v10

    .line 1279671
    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    :goto_2
    move v1, v0

    .line 1279672
    goto/16 :goto_0

    .line 1279673
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method private static c(LX/7ye;Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7ye;",
            "Ljava/util/List",
            "<",
            "LX/7ye;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1279648
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279649
    iget-object v0, p0, LX/7ye;->a:Ljava/lang/String;

    move-object v2, v0

    .line 1279650
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1279651
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ye;

    .line 1279652
    iget-object p0, v0, LX/7ye;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1279653
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1279654
    :goto_1
    return v1

    .line 1279655
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1279656
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private g(LX/7ye;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1279627
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279628
    invoke-virtual {p1}, LX/7ye;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1279629
    iget-object v0, p1, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279630
    if-eqz v0, :cond_0

    .line 1279631
    iget-object v0, p1, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279632
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1279633
    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->r:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 1279634
    :cond_0
    :goto_0
    return v2

    .line 1279635
    :cond_1
    iget-object v0, p1, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279636
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1279637
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279638
    iget-boolean v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v1, v1

    .line 1279639
    if-nez v1, :cond_0

    .line 1279640
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->q:LX/75F;

    invoke-virtual {v1, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    .line 1279641
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279642
    invoke-direct {p0, v0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(Lcom/facebook/photos/base/tagging/FaceBox;LX/7ye;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 1279643
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1279644
    goto :goto_2

    .line 1279645
    :cond_4
    if-eqz v1, :cond_5

    .line 1279646
    iget v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->r:I

    :cond_5
    move v2, v1

    .line 1279647
    goto :goto_0
.end method

.method private h(LX/7ye;)V
    .locals 4

    .prologue
    .line 1279623
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    if-eqz v0, :cond_0

    .line 1279624
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    invoke-interface {v0, p1}, LX/7yZ;->b(LX/7ye;)V

    .line 1279625
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got tag suggestions for photo "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1279626
    return-void
.end method

.method private static i(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;)Z
    .locals 2

    .prologue
    .line 1279482
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279483
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;Ljava/util/List;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/7ye;)V
    .locals 2

    .prologue
    .line 1279613
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279614
    invoke-static {p0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->i(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1279615
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1279616
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279617
    iget-object v1, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279618
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279619
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-static {p1, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;Ljava/util/List;)I

    move-result v0

    .line 1279620
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1279621
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1279622
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/7ye;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7ye;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1279584
    invoke-virtual {p1}, LX/7ye;->b()Ljava/util/List;

    move-result-object v0

    .line 1279585
    if-eqz v0, :cond_7

    .line 1279586
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1279587
    if-eqz p2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v3, v4

    :goto_0
    move v5, v1

    .line 1279588
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_6

    .line 1279589
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279590
    if-eqz v3, :cond_5

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1279591
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Tag suggestions for face box "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279592
    iget-object v6, v1, Lcom/facebook/photos/base/tagging/FaceBox;->b:Landroid/graphics/RectF;

    move-object v6, v6

    .line 1279593
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ":"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279594
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1279595
    iget-object v7, v2, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v2, v7

    .line 1279596
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    goto :goto_2

    :cond_0
    move v3, v1

    .line 1279597
    goto :goto_0

    .line 1279598
    :cond_1
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v4, :cond_2

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {p0, v2}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1279599
    :cond_2
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1279600
    iput-object v2, v1, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 1279601
    :cond_3
    :goto_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 1279602
    :cond_4
    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    if-eqz v2, :cond_3

    .line 1279603
    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    .line 1279604
    iput-object v2, v1, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 1279605
    goto :goto_3

    .line 1279606
    :cond_5
    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    if-eqz v2, :cond_3

    .line 1279607
    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->o:LX/0Px;

    .line 1279608
    iput-object v2, v1, Lcom/facebook/photos/base/tagging/FaceBox;->l:Ljava/util/List;

    .line 1279609
    goto :goto_3

    .line 1279610
    :cond_6
    invoke-direct {p0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->g(LX/7ye;)Z

    .line 1279611
    invoke-direct {p0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h(LX/7ye;)V

    .line 1279612
    :cond_7
    return-void
.end method

.method public final declared-synchronized a(LX/7ye;Z)V
    .locals 2

    .prologue
    .line 1279575
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;Ljava/util/List;)I

    move-result v0

    .line 1279576
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1279577
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1279578
    :cond_0
    if-eqz p2, :cond_1

    .line 1279579
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279580
    iget-object v1, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279581
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279582
    :cond_1
    monitor-exit p0

    return-void

    .line 1279583
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7ye;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1279545
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1279546
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1279547
    :cond_1
    const/4 v2, 0x0

    .line 1279548
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_4

    .line 1279549
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ye;

    .line 1279550
    invoke-virtual {v0}, LX/7ye;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/7ye;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1279551
    iget-object v1, v0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v1, v1

    .line 1279552
    if-eqz v1, :cond_5

    .line 1279553
    iget-object v1, v0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v1, v1

    .line 1279554
    iget-object v4, v1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v4

    .line 1279555
    instance-of v1, v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v1, :cond_5

    .line 1279556
    iget-object v1, v0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v1, v1

    .line 1279557
    iget-object v4, v1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v4

    .line 1279558
    check-cast v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279559
    iget-boolean v4, v1, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v1, v4

    .line 1279560
    if-nez v1, :cond_5

    invoke-direct {p0, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->g(LX/7ye;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1279561
    invoke-direct {p0, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h(LX/7ye;)V

    move v0, v2

    .line 1279562
    :goto_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 1279563
    :cond_2
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279564
    iget-object v4, v0, LX/7ye;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1279565
    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279566
    invoke-static {p0, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->i(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1279567
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-static {v0, v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;Ljava/util/List;)I

    move-result v1

    .line 1279568
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 1279569
    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v2, v1}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    .line 1279570
    :cond_3
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1279571
    const/4 v0, 0x1

    goto :goto_2

    .line 1279572
    :cond_4
    if-eqz v2, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b:Z

    if-nez v0, :cond_0

    .line 1279573
    invoke-virtual {p0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1279574
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1279537
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 1279538
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1279539
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1279540
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b:Z

    .line 1279541
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1279542
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279543
    :cond_0
    monitor-exit p0

    return-void

    .line 1279544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/7ye;)V
    .locals 5

    .prologue
    .line 1279530
    invoke-virtual {p0, p1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c(LX/7ye;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1279531
    :goto_0
    return-void

    .line 1279532
    :cond_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1279533
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yc;

    invoke-virtual {p1}, LX/7ye;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, LX/7ye;->g()J

    move-result-wide v2

    .line 1279534
    iget-object v4, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1279535
    invoke-virtual {v0, v1, v2, v3, v4}, LX/7yc;->a(Ljava/util/List;JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1279536
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/7yW;

    invoke-direct {v1, p0, p1}, LX/7yW;-><init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/7ye;)V

    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->k:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final c(LX/7ye;)Z
    .locals 2

    .prologue
    .line 1279523
    iget-object v0, p1, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279524
    if-nez v0, :cond_0

    .line 1279525
    const/4 v0, 0x0

    .line 1279526
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->q:LX/75F;

    .line 1279527
    iget-object v0, p1, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v0

    .line 1279528
    iget-object p0, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, p0

    .line 1279529
    check-cast v0, LX/74x;

    invoke-virtual {v1, v0}, LX/75F;->b(LX/74x;)Z

    move-result v0

    goto :goto_0
.end method

.method public final declared-synchronized d(LX/7ye;)LX/7ye;
    .locals 2

    .prologue
    .line 1279512
    monitor-enter p0

    .line 1279513
    :try_start_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279514
    iget-object v1, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279515
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279516
    iget-object v1, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279517
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 1279518
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->t:Ljava/util/Map;

    .line 1279519
    iget-object v1, p1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1279520
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ye;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, v0

    .line 1279521
    :cond_0
    monitor-exit p0

    return-object p1

    .line 1279522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1279510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->p:Z

    .line 1279511
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1279508
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->p:Z

    .line 1279509
    return-void
.end method

.method public final declared-synchronized h()V
    .locals 7

    .prologue
    .line 1279484
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1279485
    :goto_0
    monitor-exit p0

    return-void

    .line 1279486
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ye;

    .line 1279487
    invoke-virtual {v0}, LX/7ye;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1279488
    const/4 v5, 0x1

    .line 1279489
    iput-boolean v5, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b:Z

    .line 1279490
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c:Z

    .line 1279491
    iget-object v1, v0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v1, v1

    .line 1279492
    iget-object v2, v1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v2

    .line 1279493
    check-cast v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279494
    iget-object v3, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->m:LX/1HI;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "file://"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279495
    iget-object v4, v1, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1279496
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    iget-object v2, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7yc;

    .line 1279497
    iget-object v6, v2, LX/7yc;->h:LX/1o9;

    move-object v2, v6

    .line 1279498
    iput-object v2, v4, LX/1bX;->c:LX/1o9;

    .line 1279499
    move-object v2, v4

    .line 1279500
    invoke-virtual {v2, v5}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    sget-object v4, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v2

    .line 1279501
    new-instance v3, LX/7yV;

    invoke-direct {v3, p0, v1, v0}, LX/7yV;-><init>(Lcom/facebook/facerec/manager/FaceBoxPrioritizer;Lcom/facebook/photos/base/photos/LocalPhoto;LX/7ye;)V

    .line 1279502
    iget-object v1, p0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v3, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279503
    goto :goto_0

    .line 1279504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1279505
    :cond_1
    :try_start_2
    invoke-virtual {v0}, LX/7ye;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/7ye;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1279506
    invoke-virtual {p0, v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b(LX/7ye;)V

    .line 1279507
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->h()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
