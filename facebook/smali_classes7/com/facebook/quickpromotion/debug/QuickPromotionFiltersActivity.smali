.class public Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/77A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:[LX/2g6;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1171196
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1171197
    invoke-static {}, LX/2g6;->values()[LX/2g6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->c:[LX/2g6;

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/77A;)V
    .locals 0

    .prologue
    .line 1171195
    iput-object p1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->b:LX/77A;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/Jyr;->a(LX/0QB;)LX/Jyr;

    move-result-object v1

    check-cast v1, LX/77A;

    invoke-static {p0, v0, v1}, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->a(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/77A;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;)V
    .locals 3

    .prologue
    .line 1171184
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1171185
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171186
    const-string v2, "Reset All Overrides to Default"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171187
    new-instance v2, LX/76c;

    invoke-direct {v2, p0}, LX/76c;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171188
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171189
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1171190
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171191
    const-string v2, "Filters"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171192
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->b:LX/77A;

    invoke-interface {v1, p0, v0}, LX/77A;->a(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;Landroid/preference/PreferenceScreen;)V

    .line 1171193
    invoke-virtual {p0, v0}, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1171194
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1171174
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1171175
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1171176
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->c:[LX/2g6;

    array-length v1, v1

    new-array v3, v1, [Ljava/lang/CharSequence;

    .line 1171177
    iget-object v4, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->c:[LX/2g6;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 1171178
    invoke-virtual {v6}, LX/2g6;->getFilterStateCaption()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v1

    .line 1171179
    add-int/lit8 v1, v1, 0x1

    .line 1171180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1171181
    :cond_0
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/2fw;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)LX/0Tn;

    move-result-object v1

    sget-object v4, LX/2g6;->DEFAULT:LX/2g6;

    invoke-virtual {v4}, LX/2g6;->ordinal()I

    move-result v4

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    new-instance v1, LX/76d;

    invoke-direct {v1, p0, v3, p1}, LX/76d;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;[Ljava/lang/CharSequence;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;)V

    invoke-virtual {v2, v3, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171182
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1171183
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1171170
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1171171
    invoke-static {p0, p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1171172
    invoke-static {p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;->a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;)V

    .line 1171173
    return-void
.end method
