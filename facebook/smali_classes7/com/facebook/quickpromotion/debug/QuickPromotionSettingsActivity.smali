.class public Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/13H;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/DefinitionValidator;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/13H;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ContextualFilterValidator;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/13H;
    .annotation runtime Lcom/facebook/quickpromotion/annotations/ActionLimitValidator;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/13N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/117;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2g2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:[LX/2fx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1171418
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1171419
    invoke-static {}, LX/2fx;->values()[LX/2fx;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->m:[LX/2fx;

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;LX/0iA;LX/13H;LX/13H;LX/13H;LX/0aG;LX/13N;LX/0lC;Ljava/util/concurrent/Executor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/117;LX/2g2;)V
    .locals 0

    .prologue
    .line 1171420
    iput-object p1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a:LX/0iA;

    iput-object p2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->b:LX/13H;

    iput-object p3, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->c:LX/13H;

    iput-object p4, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->d:LX/13H;

    iput-object p5, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->e:LX/0aG;

    iput-object p6, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->f:LX/13N;

    iput-object p7, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->g:LX/0lC;

    iput-object p8, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->h:Ljava/util/concurrent/Executor;

    iput-object p9, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p10, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->j:LX/117;

    iput-object p11, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->k:LX/2g2;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    invoke-static {v11}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {v11}, LX/13L;->b(LX/0QB;)LX/13L;

    move-result-object v2

    check-cast v2, LX/13H;

    invoke-static {v11}, LX/2g1;->a(LX/0QB;)LX/2g1;

    move-result-object v3

    check-cast v3, LX/13H;

    invoke-static {v11}, LX/13M;->b(LX/0QB;)LX/13M;

    move-result-object v4

    check-cast v4, LX/13H;

    invoke-static {v11}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {v11}, LX/13N;->a(LX/0QB;)LX/13N;

    move-result-object v6

    check-cast v6, LX/13N;

    invoke-static {v11}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v11}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v11}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v11}, LX/116;->b(LX/0QB;)LX/116;

    move-result-object v10

    check-cast v10, LX/117;

    invoke-static {v11}, LX/2g2;->b(LX/0QB;)LX/2g2;

    move-result-object v11

    check-cast v11, LX/2g2;

    invoke-static/range {v0 .. v11}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;LX/0iA;LX/13H;LX/13H;LX/13H;LX/0aG;LX/13N;LX/0lC;Ljava/util/concurrent/Executor;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/117;LX/2g2;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v13, 0x0

    const/4 v4, 0x0

    .line 1171328
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 1171329
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1171330
    sget-object v1, LX/2fw;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1171331
    const-string v1, "Enable Dev Mode"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171332
    const-string v1, "Disables hardcoded interstitial delays"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171333
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1171334
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171335
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171336
    const-string v1, "Global Filter Options"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171337
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/quickpromotion/debug/QuickPromotionFiltersActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1171338
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171339
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171340
    const-string v1, "Triggers Firing Page"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171341
    const-string v1, "Tapping a trigger will show the eligible QP Interstitial"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171342
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/quickpromotion/debug/QuickPromotionTriggersActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1171343
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171344
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1171345
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171346
    const-string v1, "Refresh & Reset"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171347
    invoke-direct {p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->b()Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171348
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171349
    const-string v1, "Reset Interstitial and Action Delays"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171350
    new-instance v1, LX/76i;

    invoke-direct {v1, p0}, LX/76i;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171351
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171352
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171353
    const-string v1, "Reset All Force Modes to Default"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171354
    new-instance v1, LX/76j;

    invoke-direct {v1, p0}, LX/76j;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171355
    invoke-virtual {v5, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171356
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1171357
    new-instance v7, Landroid/preference/PreferenceCategory;

    invoke-direct {v7, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1171358
    invoke-virtual {v5, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171359
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v7, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171360
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a:LX/0iA;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13D;

    .line 1171361
    if-eqz v0, :cond_0

    .line 1171362
    invoke-virtual {v0}, LX/13D;->i()Ljava/lang/Iterable;

    move-result-object v1

    .line 1171363
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1171364
    new-instance v9, Landroid/preference/Preference;

    invoke-direct {v9, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171365
    iget-object v2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v10, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v10}, LX/2fw;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v10

    sget-object v11, LX/2fx;->DEFAULT:LX/2fx;

    invoke-virtual {v11}, LX/2fx;->ordinal()I

    move-result v11

    invoke-interface {v2, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 1171366
    iget-object v10, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->m:[LX/2fx;

    aget-object v2, v10, v2

    invoke-virtual {v2}, LX/2fx;->getStatusCaption()Ljava/lang/String;

    move-result-object v2

    .line 1171367
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171368
    iget-object v2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->c:LX/13H;

    invoke-interface {v2, v1, v13}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    iget-boolean v2, v2, LX/1Y7;->c:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->d:LX/13H;

    invoke-interface {v2, v1, v13}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    iget-boolean v2, v2, LX/1Y7;->c:Z

    if-eqz v2, :cond_1

    iget-boolean v2, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    if-nez v2, :cond_1

    move v2, v3

    .line 1171369
    :goto_1
    const-string v10, "Title: %s\nContent: %s\nEligible?: %s"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    iget-object v12, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    aput-object v12, v11, v4

    iget-object v12, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    aput-object v12, v11, v3

    const/4 v12, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-static {v10, v11}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171370
    new-instance v2, LX/76k;

    invoke-direct {v2, p0, v1}, LX/76k;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v9, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171371
    invoke-virtual {v7, v9}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_1
    move v2, v4

    .line 1171372
    goto :goto_1

    .line 1171373
    :cond_2
    invoke-virtual {v0}, LX/13D;->j()Ljava/lang/Iterable;

    move-result-object v1

    .line 1171374
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1171375
    new-instance v9, Landroid/preference/Preference;

    invoke-direct {v9, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171376
    iget-object v2, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v9, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171377
    iget-object v2, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->b:LX/13H;

    invoke-interface {v2, v1, v13}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v2

    .line 1171378
    iget-boolean v10, v2, LX/1Y7;->c:Z

    if-eqz v10, :cond_4

    .line 1171379
    invoke-virtual {v0, v1, v13}, LX/13D;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v1

    .line 1171380
    :goto_3
    const-string v2, "Invalid: %s"

    new-array v10, v3, [Ljava/lang/Object;

    iget-object v1, v1, LX/1Y7;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v10, v4

    invoke-static {v2, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171381
    invoke-virtual {v7, v9}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 1171382
    :cond_3
    invoke-virtual {p0, v5}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1171383
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1171384
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1171385
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v1}, LX/2fw;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    sget-object v3, LX/2fx;->DEFAULT:LX/2fx;

    invoke-virtual {v3}, LX/2fx;->ordinal()I

    move-result v3

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 1171386
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->m:[LX/2fx;

    aget-object v0, v1, v0

    invoke-virtual {v0}, LX/2fx;->getStatusCaption()Ljava/lang/String;

    move-result-object v0

    .line 1171387
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1171388
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "[\n"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1171389
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 1171390
    const-string v4, "{type: %s, value: %s}\n"

    new-array v5, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    aput-object v0, v5, v9

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1171391
    :cond_0
    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1171392
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->c:LX/13H;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v1

    .line 1171393
    const-string v0, "false"

    .line 1171394
    iget-boolean v4, v1, LX/1Y7;->c:Z

    if-eqz v4, :cond_4

    .line 1171395
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->d:LX/13H;

    const/4 v4, 0x0

    invoke-interface {v1, p1, v4}, LX/13H;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;

    move-result-object v1

    .line 1171396
    iget-boolean v4, v1, LX/1Y7;->c:Z

    if-eqz v4, :cond_3

    .line 1171397
    iget-boolean v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    if-eqz v0, :cond_2

    .line 1171398
    const-string v0, "false. Is in exposure holdout."

    .line 1171399
    :cond_1
    :goto_1
    const-string v4, "Title: %s\n\nContent: %s\n\nMax Impressions: %s\nLocal Impressions: %s\n\nPrimary Action Limit: %s\nLocal Count: %s\n\nSecondary Action Limit: %s\nLocal Count: %s\n\nPriority: %s\n\nSocial Context:%s\n\nEligible?: %s\n\nTriggers: %s\n\nFilters: %s\n\nImage: %s"

    const/16 v1, 0xe

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    aput-object v1, v5, v8

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    aput-object v1, v5, v9

    iget v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v10

    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->f:LX/13N;

    sget-object v6, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-virtual {v1, p1, v6}, LX/13N;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v11

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-nez v1, :cond_7

    const-string v1, "null"

    :goto_2
    aput-object v1, v5, v12

    const/4 v1, 0x5

    iget-object v6, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->f:LX/13N;

    sget-object v7, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    invoke-virtual {v6, p1, v7}, LX/13N;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x6

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-nez v1, :cond_8

    const-string v1, "null"

    :goto_3
    aput-object v1, v5, v6

    const/4 v1, 0x7

    iget-object v6, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->f:LX/13N;

    sget-object v7, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    invoke-virtual {v6, p1, v7}, LX/13N;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/16 v1, 0x8

    iget-wide v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->priority:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    const/16 v6, 0x9

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v1, :cond_9

    const-string v1, "null"

    :goto_4
    aput-object v1, v5, v6

    const/16 v1, 0xa

    aput-object v0, v5, v1

    const/16 v0, 0xb

    const-string v1, ","

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/16 v0, 0xc

    aput-object v3, v5, v0

    const/16 v1, 0xd

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    if-nez v0, :cond_a

    const-string v0, "null"

    :goto_5
    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1171400
    const-string v0, "Reset Counters"

    new-instance v1, LX/76l;

    invoke-direct {v1, p0, p1}, LX/76l;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v2, v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171401
    const-string v0, "JSON"

    new-instance v1, LX/76m;

    invoke-direct {v1, p0, p1}, LX/76m;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v2, v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171402
    const-string v0, "Force Mode Options"

    new-instance v1, LX/76n;

    invoke-direct {v1, p0, p1}, LX/76n;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v2, v0, v1}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171403
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1171404
    return-void

    .line 1171405
    :cond_2
    const-string v0, "true"

    goto/16 :goto_1

    .line 1171406
    :cond_3
    iget-object v4, v1, LX/1Y7;->e:LX/0am;

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1171407
    const-string v4, "false.\nFailed Counter: %s"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v0, v1, LX/1Y7;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fy;

    invoke-virtual {v0}, LX/2fy;->getReadableName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1171408
    :cond_4
    iget-object v4, v1, LX/1Y7;->d:LX/0am;

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1171409
    const-string v4, "false.\nFailed filter: %s, value: %s"

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v0, v1, LX/1Y7;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v0

    aput-object v0, v5, v8

    iget-object v0, v1, LX/1Y7;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    aput-object v0, v5, v9

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1171410
    :cond_5
    iget-object v4, v1, LX/1Y7;->f:LX/0am;

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1171411
    iget-object v4, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->k:LX/2g2;

    iget-object v0, v1, LX/1Y7;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    invoke-virtual {v4, p1, v0}, LX/2g2;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;)Ljava/util/Map;

    move-result-object v0

    .line 1171412
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "false.\nFailed filter clause. Contextual Filter Results:\n"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1171413
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1171414
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 1171415
    const-string v6, "result: %b, filter: %s, value: %s \n"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v0

    aput-object v0, v7, v9

    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    aput-object v0, v7, v10

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1171416
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1171417
    :cond_7
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_2

    :cond_8
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_3

    :cond_9
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    goto/16 :goto_4

    :cond_a
    const-string v0, "{\n height: %d,\n width %d,\n scale: %f,\n name: %s,\n url: %s\n}"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->height:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v8

    iget-object v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->width:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v9

    iget-object v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v3, v10

    iget-object v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->name:Ljava/lang/String;

    aput-object v6, v3, v11

    iget-object v6, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    iget-object v6, v6, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    aput-object v6, v3, v12

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5
.end method

.method private b()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 1171324
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171325
    new-instance v1, LX/76h;

    invoke-direct {v1, p0}, LX/76h;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171326
    const-string v1, "Refresh Quick Promotion Interstitial Data"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171327
    return-object v0
.end method

.method public static b$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1171314
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1171315
    const-string v1, "Force Mode Options"

    invoke-virtual {v2, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1171316
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->m:[LX/2fx;

    array-length v1, v1

    new-array v3, v1, [Ljava/lang/CharSequence;

    .line 1171317
    iget-object v4, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->m:[LX/2fx;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 1171318
    invoke-virtual {v6}, LX/2fx;->getActionCaption()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v1

    .line 1171319
    add-int/lit8 v1, v1, 0x1

    .line 1171320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1171321
    :cond_0
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {v1}, LX/2fw;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    sget-object v4, LX/2fx;->DEFAULT:LX/2fx;

    invoke-virtual {v4}, LX/2fx;->ordinal()I

    move-result v4

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    new-instance v1, LX/76o;

    invoke-direct {v1, p0, v3, p1}, LX/76o;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;[Ljava/lang/CharSequence;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v2, v3, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171322
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1171323
    return-void
.end method

.method public static c$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 8

    .prologue
    .line 1171297
    new-instance v1, LX/0ju;

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1171298
    const-string v0, "Reset Counters"

    invoke-virtual {v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1171299
    invoke-static {}, LX/2fy;->values()[LX/2fy;

    move-result-object v0

    array-length v2, v0

    .line 1171300
    new-array v3, v2, [Z

    .line 1171301
    new-array v4, v2, [Ljava/lang/CharSequence;

    .line 1171302
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1171303
    invoke-static {}, LX/2fy;->values()[LX/2fy;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-virtual {v5}, LX/2fy;->getReadableName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 1171304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1171305
    :cond_0
    new-array v0, v2, [Z

    .line 1171306
    new-instance v5, LX/76p;

    invoke-direct {v5, p0, v3}, LX/76p;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;[Z)V

    .line 1171307
    iget-object v6, v1, LX/0ju;->a:LX/31a;

    iput-object v4, v6, LX/31a;->u:[Ljava/lang/CharSequence;

    .line 1171308
    iget-object v6, v1, LX/0ju;->a:LX/31a;

    iput-object v5, v6, LX/31a;->I:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    .line 1171309
    iget-object v6, v1, LX/0ju;->a:LX/31a;

    iput-object v0, v6, LX/31a;->E:[Z

    .line 1171310
    iget-object v6, v1, LX/0ju;->a:LX/31a;

    const/4 v7, 0x1

    iput-boolean v7, v6, LX/31a;->F:Z

    .line 1171311
    const-string v0, "GO!"

    new-instance v4, LX/76q;

    invoke-direct {v4, p0, v2, v3, p1}, LX/76q;-><init>(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;I[ZLcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V

    invoke-virtual {v1, v0, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1171312
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1171313
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1171286
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1171287
    invoke-static {p0, p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1171288
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1171289
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->j:LX/117;

    invoke-virtual {v0}, LX/117;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1171290
    iget-object v3, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->j:LX/117;

    invoke-virtual {v3, v0}, LX/117;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v0

    .line 1171291
    instance-of v3, v0, LX/13D;

    if-eqz v3, :cond_0

    .line 1171292
    check-cast v0, LX/13D;

    .line 1171293
    invoke-virtual {v0}, LX/13D;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1171294
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->l:Ljava/util/Map;

    .line 1171295
    invoke-static {p0}, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;->a$redex0(Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;)V

    .line 1171296
    return-void
.end method
