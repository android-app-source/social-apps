.class public Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/preference/PreferenceCategory;

.field private d:LX/0dN;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1171548
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1171549
    new-instance v0, LX/76t;

    invoke-direct {v0, p0}, LX/76t;-><init>(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;)V

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->d:LX/0dN;

    .line 1171550
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1171545
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "No filter applied"

    :goto_0
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171546
    return-void

    .line 1171547
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Filtered by: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 1171496
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 1171497
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4om;->setText(Ljava/lang/String;)V

    .line 1171498
    const-string v1, "Launch segue"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171499
    const-string v1, "Launch a user defined segue"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171500
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "fb://"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1171501
    new-instance v1, LX/76u;

    invoke-direct {v1, p0}, LX/76u;-><init>(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1171502
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171503
    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;LX/17W;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1171544
    iput-object p1, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a:LX/17W;

    iput-object p2, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0, v0, v1}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;LX/17W;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1171525
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 1171526
    const-class v0, LX/0ax;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 1171527
    if-nez v3, :cond_1

    .line 1171528
    :cond_0
    return-void

    .line 1171529
    :cond_1
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fw;->c:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 1171530
    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v0, v3, v2

    .line 1171531
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v0, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1171532
    const-string v0, "%s"

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "="

    invoke-virtual {v6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    .line 1171533
    :goto_1
    const-string v7, "^fb://.*$"

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1171534
    if-nez v4, :cond_3

    if-nez v0, :cond_4

    .line 1171535
    :cond_3
    iget-object v7, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->e:Ljava/lang/String;

    invoke-static {v7}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1171536
    invoke-static {v6}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1171537
    new-instance v7, Landroid/preference/Preference;

    invoke-direct {v7, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171538
    invoke-virtual {v7, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171539
    new-instance v8, LX/76x;

    invoke-direct {v8, p0, v0, v6}, LX/76x;-><init>(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;ZLjava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1171540
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1171541
    :cond_4
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1171542
    goto :goto_1

    .line 1171543
    :catch_0
    goto :goto_2
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 1171551
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 1171552
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4om;->setText(Ljava/lang/String;)V

    .line 1171553
    const-string v1, "Filter segues"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171554
    invoke-virtual {v0}, LX/4om;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a(Landroid/preference/Preference;Ljava/lang/String;)V

    .line 1171555
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 1171556
    new-instance v1, LX/76v;

    invoke-direct {v1, p0}, LX/76v;-><init>(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1171557
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171558
    return-void
.end method

.method private c(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1171518
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1171519
    sget-object v1, LX/2fw;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1171520
    const-string v1, "Show all segues"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171521
    const-string v1, "Show all segues including parameterized segues."

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171522
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1171523
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171524
    return-void
.end method

.method private d(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1171513
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    .line 1171514
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    const-string v1, "Segues"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171515
    iget-object v0, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1171516
    invoke-static {p0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a$redex0(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;)V

    .line 1171517
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1171504
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1171505
    invoke-static {p0, p0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1171506
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1171507
    invoke-direct {p0, v0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 1171508
    invoke-direct {p0, v0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 1171509
    invoke-direct {p0, v0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->c(Landroid/preference/PreferenceScreen;)V

    .line 1171510
    invoke-direct {p0, v0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->d(Landroid/preference/PreferenceScreen;)V

    .line 1171511
    invoke-virtual {p0, v0}, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1171512
    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x700d375d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1171493
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onPause()V

    .line 1171494
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fw;->c:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->d:LX/0dN;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 1171495
    const/16 v1, 0x23

    const v2, 0x49d89ecb

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x4548b1c3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1171490
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 1171491
    iget-object v1, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fw;->c:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->d:LX/0dN;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 1171492
    const/16 v1, 0x23

    const v2, -0x1d728cec

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
