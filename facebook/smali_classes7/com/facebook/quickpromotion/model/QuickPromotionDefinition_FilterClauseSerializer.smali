.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_FilterClauseSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172156
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_FilterClauseSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_FilterClauseSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172157
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172158
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172159
    if-nez p0, :cond_0

    .line 1172160
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172161
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172162
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_FilterClauseSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;LX/0nX;LX/0my;)V

    .line 1172163
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172164
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172165
    const-string v0, "type"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->type:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause$BooleanType;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172166
    const-string v0, "filters"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->filters:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172167
    const-string v0, "clauses"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;->clauses:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172168
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172169
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_FilterClauseSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$FilterClause;LX/0nX;LX/0my;)V

    return-void
.end method
