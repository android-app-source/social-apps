.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_CreativeSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172154
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_CreativeSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_CreativeSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172155
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172153
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172147
    if-nez p0, :cond_0

    .line 1172148
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172149
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172150
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_CreativeSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/0nX;LX/0my;)V

    .line 1172151
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172152
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172134
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172135
    const-string v0, "content"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172136
    const-string v0, "image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172137
    const-string v0, "animated_image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172138
    const-string v0, "primary_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172139
    const-string v0, "secondary_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172140
    const-string v0, "dismiss_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172141
    const-string v0, "social_context"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172142
    const-string v0, "footer"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->footer:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172143
    const-string v0, "template"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->template:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172144
    const-string v0, "template_parameters"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->templateParameters:LX/0P1;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172145
    const-string v0, "branding_image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172146
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172133
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_CreativeSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/0nX;LX/0my;)V

    return-void
.end method
