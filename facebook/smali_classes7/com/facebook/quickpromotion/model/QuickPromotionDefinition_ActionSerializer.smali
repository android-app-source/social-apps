.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ActionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172116
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ActionSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ActionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172117
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172102
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172103
    if-nez p0, :cond_0

    .line 1172104
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172105
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172106
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ActionSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/0nX;LX/0my;)V

    .line 1172107
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172108
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172109
    const-string v0, "style"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action$Style;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172110
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172111
    const-string v0, "url"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172112
    const-string v0, "limit"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->limit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172113
    const-string v0, "dismiss_promotion"

    iget-boolean v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1172114
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172115
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ActionSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/0nX;LX/0my;)V

    return-void
.end method
