.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ContextualFilterSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172118
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ContextualFilterSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ContextualFilterSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172119
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172120
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172121
    if-nez p0, :cond_0

    .line 1172122
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172123
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172124
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ContextualFilterSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;LX/0nX;LX/0my;)V

    .line 1172125
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172126
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172127
    const-string v0, "type"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172128
    const-string v0, "passes_if_not_client_supported"

    iget-boolean v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->passIfNotSupported:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1172129
    const-string v0, "value"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172130
    const-string v0, "extra_data"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172131
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172132
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ContextualFilterSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;LX/0nX;LX/0my;)V

    return-void
.end method
