.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172099
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172101
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172093
    if-nez p0, :cond_0

    .line 1172094
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172095
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172096
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/0nX;LX/0my;)V

    .line 1172097
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172098
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1172063
    const-string v0, "promotion_id"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172064
    const-string v0, "triggers"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172065
    const-string v0, "creatives"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->creatives:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172066
    const-string v0, "contextual_filters"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->d()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172067
    const-string v0, "boolean_filter_root"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->booleanFilter:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$BooleanFilter;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172068
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172069
    const-string v0, "content"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172070
    const-string v0, "image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->imageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172071
    const-string v0, "animated_image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->animatedImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172072
    const-string v0, "primary_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172073
    const-string v0, "secondary_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172074
    const-string v0, "dismiss_action"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172075
    const-string v0, "social_context"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172076
    const-string v0, "footer"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->footer:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172077
    const-string v0, "template"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172078
    const-string v0, "template_parameters"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->templateParameters:LX/0P1;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172079
    const-string v0, "priority"

    iget-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->priority:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1172080
    const-string v0, "max_impressions"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->maxImpressions:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172081
    const-string v0, "viewer_impressions"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->viewerImpressions:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172082
    const-string v0, "start_time"

    iget-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->startTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1172083
    const-string v0, "end_time"

    iget-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->endTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1172084
    const-string v0, "client_ttl_seconds"

    iget-wide v2, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->clientTtlSeconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1172085
    const-string v0, "instance_log_data"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172086
    const-string v0, "is_exposure_holdout"

    iget-boolean v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1172087
    const-string v0, "log_eligibility_waterfall"

    iget-boolean v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->logEligibilityWaterfall:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1172088
    const-string v0, "branding_image"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->brandingImageParams:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172089
    const-string v0, "custom_renderer_type"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172090
    const-string v0, "custom_renderer_params"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1172091
    const-string v0, "attributes"

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->getAttributesList()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172092
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172062
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinitionSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/0nX;LX/0my;)V

    return-void
.end method
