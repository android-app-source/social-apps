.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ImageParametersSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172170
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ImageParametersSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ImageParametersSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172171
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172172
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172173
    if-nez p0, :cond_0

    .line 1172174
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172175
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172176
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ImageParametersSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;LX/0nX;LX/0my;)V

    .line 1172177
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172178
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172179
    const-string v0, "uri"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172180
    const-string v0, "width"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->width:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172181
    const-string v0, "height"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->height:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172182
    const-string v0, "scale"

    iget v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->scale:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1172183
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172184
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172185
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_ImageParametersSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;LX/0nX;LX/0my;)V

    return-void
.end method
