.class public Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_SocialContextSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172186
    const-class v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    new-instance v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_SocialContextSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_SocialContextSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1172187
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172192
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172193
    if-nez p0, :cond_0

    .line 1172194
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1172195
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1172196
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_SocialContextSerializer;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;LX/0nX;LX/0my;)V

    .line 1172197
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1172198
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1172189
    const-string v0, "text"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172190
    const-string v0, "friend_ids"

    iget-object v1, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1172191
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1172188
    check-cast p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition_SocialContextSerializer;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;LX/0nX;LX/0my;)V

    return-void
.end method
