.class public Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0gr;


# instance fields
.field public p:LX/2hf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172318
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1172298
    iget-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->p:LX/2hf;

    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v0

    .line 1172299
    if-nez v0, :cond_0

    .line 1172300
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->finish()V

    .line 1172301
    :goto_0
    return-void

    .line 1172302
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1172303
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;

    invoke-static {v0}, LX/2hf;->a(LX/0QB;)LX/2hf;

    move-result-object v0

    check-cast v0, LX/2hf;

    iput-object v0, p0, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->p:LX/2hf;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1172314
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 1172315
    invoke-virtual {p0, p1}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->setIntent(Landroid/content/Intent;)V

    .line 1172316
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->a()V

    .line 1172317
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1172311
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1172312
    invoke-static {p0, p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1172313
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 1172308
    if-eqz p1, :cond_0

    .line 1172309
    invoke-virtual {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->finish()V

    .line 1172310
    :cond_0
    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1172304
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 1172305
    if-nez p1, :cond_0

    .line 1172306
    invoke-direct {p0}, Lcom/facebook/quickpromotion/ui/QuickPromotionInterstitialActivity;->a()V

    .line 1172307
    :cond_0
    return-void
.end method
