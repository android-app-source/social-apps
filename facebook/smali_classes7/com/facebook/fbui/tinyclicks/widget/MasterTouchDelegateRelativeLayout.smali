.class public Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/7yo;


# instance fields
.field private a:LX/7yl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1280362
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1280363
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a(Landroid/content/Context;)V

    .line 1280364
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1280359
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1280360
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a(Landroid/content/Context;)V

    .line 1280361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1280342
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1280343
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a(Landroid/content/Context;)V

    .line 1280344
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1280355
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 1280356
    invoke-static {v0}, LX/7yl;->b(LX/0QB;)LX/7yl;

    move-result-object v0

    check-cast v0, LX/7yl;

    iput-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    .line 1280357
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    invoke-virtual {v0, p0}, LX/7yl;->a(LX/7yo;)V

    .line 1280358
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1280352
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1280353
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/graphics/Canvas;)V

    .line 1280354
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x51515abb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1280349
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 1280350
    iget-object v1, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->a()V

    .line 1280351
    const/16 v1, 0x2d

    const v2, -0x2c649c4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3ece934b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1280346
    iget-object v1, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->b()V

    .line 1280347
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 1280348
    const/16 v1, 0x2d

    const v2, -0x6ce18bd6    # -1.9999677E-27f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1280345
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateRelativeLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
