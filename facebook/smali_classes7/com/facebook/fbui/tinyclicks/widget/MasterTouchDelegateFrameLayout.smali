.class public Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/7yo;


# instance fields
.field public a:LX/7yl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1280319
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1280320
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a(Landroid/content/Context;)V

    .line 1280321
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1280322
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1280323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1280324
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1280325
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a(Landroid/content/Context;)V

    .line 1280326
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1280327
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 1280328
    invoke-static {v0}, LX/7yl;->b(LX/0QB;)LX/7yl;

    move-result-object v0

    check-cast v0, LX/7yl;

    iput-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    .line 1280329
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    invoke-virtual {v0, p0}, LX/7yl;->a(LX/7yo;)V

    .line 1280330
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1280331
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1280332
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/graphics/Canvas;)V

    .line 1280333
    return-void
.end method

.method public getMasterTouchDelegate()LX/7yl;
    .locals 1

    .prologue
    .line 1280334
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x67a13399

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1280335
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1280336
    iget-object v1, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->a()V

    .line 1280337
    const/16 v1, 0x2d

    const v2, -0x52625a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xff27da1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1280338
    iget-object v1, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->b()V

    .line 1280339
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1280340
    const/16 v1, 0x2d

    const v2, 0x4a5c5b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1280341
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
