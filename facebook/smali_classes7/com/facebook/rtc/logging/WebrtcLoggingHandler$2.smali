.class public final Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/2S7;


# direct methods
.method public constructor <init>(LX/2S7;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1174001
    iput-object p1, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->c:LX/2S7;

    iput-wide p2, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->a:J

    iput-object p4, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1174002
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->c:LX/2S7;

    iget-wide v4, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->a:J

    invoke-static {v0, v4, v5}, LX/2S7;->b(LX/2S7;J)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1174003
    :try_start_1
    iget-object v1, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->c:LX/2S7;

    iget-wide v4, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->a:J

    iget-object v3, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v5, v3}, LX/2S7;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 1174004
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1174005
    :try_start_2
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1174006
    :try_start_3
    invoke-virtual {v1, v4}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1174007
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 1174008
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 1174009
    :goto_0
    return-void

    .line 1174010
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v2, :cond_0

    .line 1174011
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1174012
    :cond_0
    if-eqz v1, :cond_1

    .line 1174013
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    :cond_1
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 1174014
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1174015
    iget-object v2, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;->c:LX/2S7;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Unable to save call summary: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2S7;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1174016
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method
