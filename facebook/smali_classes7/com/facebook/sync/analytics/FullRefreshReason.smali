.class public Lcom/facebook/sync/analytics/FullRefreshReason;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/sync/analytics/FullRefreshReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final b:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final c:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final d:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final e:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final f:Lcom/facebook/sync/analytics/FullRefreshReason;

.field public static final g:Lcom/facebook/sync/analytics/FullRefreshReason;


# instance fields
.field public final h:LX/7Fz;

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1188598
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->NO_EXISTING_SYNC_TOKEN:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->a:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188599
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->NO_EXISTING_SEQUENCE_ID:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->b:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188600
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->USER_REQUESTED:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->c:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188601
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->RECOVERY_FROM_UNCAUGHT_EXCEPTION:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->d:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188602
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->MISSED_DELTA:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->e:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188603
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->GATEKEEPER_CHANGED:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->f:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188604
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->NONE:LX/7Fz;

    invoke-direct {v0, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;)V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->g:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188605
    new-instance v0, LX/7Fy;

    invoke-direct {v0}, LX/7Fy;-><init>()V

    sput-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/7Fz;)V
    .locals 1

    .prologue
    .line 1188606
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;Ljava/lang/String;)V

    .line 1188607
    return-void
.end method

.method public constructor <init>(LX/7Fz;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1188608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188609
    iput-object p1, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->h:LX/7Fz;

    .line 1188610
    iput-object p2, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    .line 1188611
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1188612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188613
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7Fz;

    iput-object v0, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->h:LX/7Fz;

    .line 1188614
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    .line 1188615
    return-void
.end method

.method public static a(J)Lcom/facebook/sync/analytics/FullRefreshReason;
    .locals 4

    .prologue
    .line 1188616
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    sget-object v1, LX/7Fz;->DELTA_FORCED_FETCH_NO_ARGS:LX/7Fz;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "firstDeltaSequenceId = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/sync/analytics/FullRefreshReason;
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1188617
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1188618
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->g:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 1188619
    :goto_0
    return-object v0

    .line 1188620
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 1188621
    array-length v0, v1

    if-eq v0, v2, :cond_1

    .line 1188622
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->g:Lcom/facebook/sync/analytics/FullRefreshReason;

    goto :goto_0

    .line 1188623
    :cond_1
    :try_start_0
    new-instance v0, Lcom/facebook/sync/analytics/FullRefreshReason;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, LX/7Fz;->valueOf(Ljava/lang/String;)LX/7Fz;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-direct {v0, v2, v1}, Lcom/facebook/sync/analytics/FullRefreshReason;-><init>(LX/7Fz;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1188624
    :catch_0
    sget-object v0, Lcom/facebook/sync/analytics/FullRefreshReason;->g:Lcom/facebook/sync/analytics/FullRefreshReason;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1188625
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1188626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->h:LX/7Fz;

    invoke-virtual {v1}, LX/7Fz;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1188627
    iget-object v1, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1188628
    iget-object v0, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->h:LX/7Fz;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1188629
    iget-object v0, p0, Lcom/facebook/sync/analytics/FullRefreshReason;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1188630
    return-void
.end method
