.class public Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/61B;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/5Pb;

.field private final c:LX/1HI;

.field private final d:LX/5PR;

.field private e:LX/5Pf;

.field private f:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/5Pg;

.field public h:Lcom/facebook/videocodec/effects/model/OverlayData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1208452
    const-class v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;Landroid/net/Uri;)V
    .locals 2
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1208491
    new-instance v0, Lcom/facebook/videocodec/effects/model/OverlayData;

    const/16 v1, 0x8

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, p2, v1}, Lcom/facebook/videocodec/effects/model/OverlayData;-><init>(Landroid/net/Uri;[F)V

    invoke-direct {p0, p1, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;-><init>(LX/1HI;Lcom/facebook/videocodec/effects/model/OverlayData;)V

    .line 1208492
    return-void

    .line 1208493
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>(LX/1HI;Lcom/facebook/videocodec/effects/model/OverlayData;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1208480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208481
    iput-object p1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->c:LX/1HI;

    .line 1208482
    iput-object p2, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208483
    new-instance v0, LX/5Pg;

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208484
    iget-object v2, v1, Lcom/facebook/videocodec/effects/model/OverlayData;->c:[F

    move-object v1, v2

    .line 1208485
    invoke-direct {v0, v1, v4}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->g:LX/5Pg;

    .line 1208486
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1208487
    iput v1, v0, LX/5PQ;->a:I

    .line 1208488
    move-object v0, v0

    .line 1208489
    const-string v1, "aPosition"

    iget-object v2, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->g:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    new-instance v2, LX/5Pg;

    const/16 v3, 0x8

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d:LX/5PR;

    .line 1208490
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1208453
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    if-eqz v0, :cond_1

    .line 1208454
    :cond_0
    :goto_0
    return-void

    .line 1208455
    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208456
    iget-object v2, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    move-object v0, v2

    .line 1208457
    if-eqz v0, :cond_3

    .line 1208458
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1208459
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 1208460
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208461
    iget-object v1, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    move-object v0, v1

    .line 1208462
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1208463
    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->c:LX/1HI;

    sget-object v2, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 1208464
    const v1, -0x75623b86

    :try_start_0
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    .line 1208465
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1208466
    instance-of v1, v0, LX/1lm;

    if-eqz v1, :cond_2

    .line 1208467
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1208468
    invoke-direct {p0, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1208469
    :catch_0
    move-exception v0

    .line 1208470
    :goto_1
    const-string v1, "OverlayRenderer"

    const-string v2, "Failed to retrieve overlay image"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1208471
    :cond_2
    :try_start_1
    const-string v1, "OverlayRenderer"

    const-string v2, "Retrieved overlay image was not a bitmap: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208472
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1208473
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1208474
    :cond_3
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208475
    iget-object v1, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1208476
    if-eqz v0, :cond_0

    .line 1208477
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208478
    iget-object v1, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1208479
    invoke-direct {p0, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const v3, 0x812f

    const/16 v2, 0x2601

    .line 1208410
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2802

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2803

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    .line 1208411
    iput-object p1, v0, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 1208412
    move-object v0, v0

    .line 1208413
    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    .line 1208414
    return-void
.end method

.method public static d(Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1208445
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1208446
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1208447
    iput-object v1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    .line 1208448
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1208449
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1208450
    iput-object v1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    .line 1208451
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208494
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1208441
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d(Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;)V

    .line 1208442
    const v0, 0x7f070049

    const v1, 0x7f070048

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b:LX/5Pb;

    .line 1208443
    invoke-direct {p0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a()V

    .line 1208444
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1208437
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d(Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;)V

    .line 1208438
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208439
    iput-object p1, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->a:Landroid/net/Uri;

    .line 1208440
    return-void
.end method

.method public final a([F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1208430
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Positional data must contain 8 elements"

    invoke-static {v0, v2}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1208431
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    .line 1208432
    iput-object p1, v0, Lcom/facebook/videocodec/effects/model/OverlayData;->c:[F

    .line 1208433
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->g:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 1208434
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->g:LX/5Pg;

    iget-object v0, v0, LX/5Pg;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 1208435
    return-void

    :cond_0
    move v0, v1

    .line 1208436
    goto :goto_0
.end method

.method public final a([F[F[FJ)V
    .locals 3

    .prologue
    .line 1208418
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/OverlayData;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208419
    invoke-direct {p0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a()V

    .line 1208420
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    if-eqz v0, :cond_1

    .line 1208421
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1208422
    const-string v0, "GL_BLEND"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208423
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 1208424
    const-string v0, "blendFunc"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208425
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    const-string v1, "uSceneMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "sOverlay"

    iget-object v2, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->e:LX/5Pf;

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208426
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1208427
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1208428
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->f:LX/1FJ;

    .line 1208429
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1208416
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->d(Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;)V

    .line 1208417
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208415
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->h:Lcom/facebook/videocodec/effects/model/OverlayData;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/OverlayData;->c()Z

    move-result v0

    return v0
.end method
