.class public Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/61B;
.implements LX/6Jv;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:[F


# instance fields
.field private c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private final e:LX/5PR;

.field public f:LX/5Pc;

.field public g:LX/5Pb;

.field public h:Z

.field public i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public j:I

.field public k:I

.field public l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

.field private m:LX/6Js;

.field public n:LX/1HI;

.field public o:I

.field public final p:Ljava/util/concurrent/Executor;

.field public final q:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/5Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field private final t:J

.field private final u:[I

.field public v:LX/0lB;

.field public w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

.field public final x:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/7SI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1208664
    const-class v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1208665
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 1208666
    sput-object v0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->b:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1208667
    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/Executor;LX/0lB;Landroid/content/Context;)V
    .locals 5
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1208668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208669
    iput v2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->j:I

    .line 1208670
    iput v2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->k:I

    .line 1208671
    iput v2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    .line 1208672
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    .line 1208673
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    .line 1208674
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    .line 1208675
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->t:J

    .line 1208676
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->u:[I

    .line 1208677
    new-instance v0, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    invoke-direct {v0}, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    .line 1208678
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    .line 1208679
    iput-object p1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->n:LX/1HI;

    .line 1208680
    iput-object p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->v:LX/0lB;

    .line 1208681
    iput-boolean v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->h:Z

    .line 1208682
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1208683
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1208684
    iput v1, v0, LX/5PQ;->a:I

    .line 1208685
    move-object v0, v0

    .line 1208686
    const-string v1, "position"

    new-instance v2, LX/5Pg;

    const/16 v3, 0x8

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->e:LX/5PR;

    .line 1208687
    const v0, 0x7f070054

    invoke-static {p4, v0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->d:Ljava/lang/String;

    .line 1208688
    const v0, 0x7f070053

    invoke-static {p4, v0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->c:Ljava/lang/String;

    .line 1208689
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->c:Ljava/lang/String;

    .line 1208690
    iput-object p2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->p:Ljava/util/concurrent/Executor;

    .line 1208691
    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1208716
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 1208717
    :try_start_0
    invoke-static {v1}, LX/2Eb;->a(Ljava/lang/Readable;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1208718
    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    return-object v0

    .line 1208719
    :catch_0
    move-exception v0

    .line 1208720
    :try_start_1
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1208721
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1md;->a(Ljava/io/Reader;)V

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 1208692
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#extension GL_OES_EGL_image_external : require\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1208693
    const-string v1, "uniform sampler2D inputImage;"

    const-string v2, "uniform samplerExternalOES inputImage;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1208694
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1208695
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1208696
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 1208697
    const-string v5, "inputImageY"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v6, :cond_0

    const-string v5, "inputImageUV"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v6, :cond_0

    .line 1208698
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1208699
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1208700
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1208701
    return-object v0
.end method

.method public static d(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V
    .locals 5

    .prologue
    .line 1208702
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1208703
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    goto :goto_0

    .line 1208704
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1208705
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1208706
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->close()V

    goto :goto_1

    .line 1208707
    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1208708
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1208709
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SI;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1208710
    iget-object v2, v0, LX/7SI;->a:[I

    invoke-static {v4, v2, v3}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    .line 1208711
    iget-object v2, v0, LX/7SI;->c:[LX/5Pf;

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/5Pf;->a()V

    .line 1208712
    iget-object v2, v0, LX/7SI;->c:[LX/5Pf;

    aget-object v2, v2, v4

    invoke-virtual {v2}, LX/5Pf;->a()V

    .line 1208713
    goto :goto_2

    .line 1208714
    :cond_2
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1208715
    return-void
.end method

.method public static g(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V
    .locals 7

    .prologue
    .line 1208648
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1208649
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1208650
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1208651
    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ln;

    .line 1208652
    check-cast v2, LX/1lm;

    invoke-virtual {v2}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1208653
    new-instance v4, LX/5Pe;

    invoke-direct {v4}, LX/5Pe;-><init>()V

    const/16 v5, 0x2801

    const/16 v6, 0x2601

    invoke-virtual {v4, v5, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v4

    const/16 v5, 0x2800

    const/16 v6, 0x2601

    invoke-virtual {v4, v5, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v4

    const/16 v5, 0x2802

    const v6, 0x812f

    invoke-virtual {v4, v5, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v4

    const/16 v5, 0x2803

    const v6, 0x812f

    invoke-virtual {v4, v5, v6}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v4

    .line 1208654
    iput-object v2, v4, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 1208655
    move-object v2, v4

    .line 1208656
    invoke-virtual {v2}, LX/5Pe;->a()LX/5Pf;

    move-result-object v2

    .line 1208657
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208658
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1

    .line 1208659
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1208660
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208661
    iput p1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->j:I

    .line 1208662
    iput p2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->k:I

    .line 1208663
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1208645
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208646
    iput-object p1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->f:LX/5Pc;

    .line 1208647
    return-void
.end method

.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1208641
    iput-object p1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->m:LX/6Js;

    .line 1208642
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->m:LX/6Js;

    if-eqz v0, :cond_0

    .line 1208643
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->m:LX/6Js;

    sget-object v1, LX/7Sc;->SHADER_FILTER:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1208644
    :cond_0
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 2

    .prologue
    .line 1208634
    sget-object v0, LX/7SJ;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1208635
    :goto_0
    return-void

    .line 1208636
    :pswitch_0
    check-cast p1, LX/7Sn;

    .line 1208637
    iget-object v0, p1, LX/7Sn;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-object v0, v0

    .line 1208638
    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1208639
    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208640
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a([F[F[FJ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1208532
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->assetPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1208533
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 1208534
    if-eqz v0, :cond_1

    .line 1208535
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g:LX/5Pb;

    .line 1208536
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->d(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V

    .line 1208537
    :cond_1
    :goto_0
    return-void

    .line 1208538
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->t:J

    sub-long/2addr v0, v4

    .line 1208539
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1208540
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->d(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V

    .line 1208541
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-virtual {v3}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->assetPath()Ljava/lang/String;

    move-result-object v5

    .line 1208542
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/config.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/7ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1208543
    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 1208544
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->v:LX/0lB;

    const-class v6, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    invoke-virtual {v4, v3, v6}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iput-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    .line 1208545
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v3, v3, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->buffers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1208546
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1208547
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1208548
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v4, v4, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->buffers:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    const-string v7, "size"

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 1208549
    iget-object v7, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    new-instance v8, LX/7SI;

    iget p2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->j:I

    int-to-float p2, p2

    mul-float/2addr p2, v4

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    iget p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->k:I

    int-to-float p3, p3

    mul-float/2addr v4, p3

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v8, p2, v4}, LX/7SI;-><init>(II)V

    invoke-virtual {v7, v3, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1208550
    :catch_0
    move-exception v3

    .line 1208551
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1208552
    const-string v4, "ShaderRenderer"

    const-string v5, "Exception loading shader files"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1208553
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g:LX/5Pb;

    if-eqz v3, :cond_1

    .line 1208554
    iget v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    if-nez v3, :cond_16

    .line 1208555
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->s:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1208556
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V

    .line 1208557
    :cond_4
    const/4 v3, 0x1

    .line 1208558
    :goto_3
    move v3, v3

    .line 1208559
    if-eqz v3, :cond_1

    .line 1208560
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g:LX/5Pb;

    invoke-virtual {v3}, LX/5Pb;->a()LX/5Pa;

    move-result-object v3

    .line 1208561
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    const-string v5, "uTime"

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1208562
    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 1208563
    const-string v1, "uTime"

    invoke-virtual {v3, v1, v0}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    .line 1208564
    :cond_5
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    const-string v1, "uRenderSize"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1208565
    const-string v0, "uRenderSize"

    iget v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->j:I

    int-to-float v1, v1

    iget v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->k:I

    int-to-float v4, v4

    invoke-virtual {v3, v0, v1, v4}, LX/5Pa;->a(Ljava/lang/String;FF)LX/5Pa;

    .line 1208566
    :cond_6
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    const-string v1, "inputIsYUV"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1208567
    const-string v0, "inputIsYUV"

    invoke-virtual {v3, v0, v2}, LX/5Pa;->a(Ljava/lang/String;Z)LX/5Pa;

    .line 1208568
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->h:Z

    if-eqz v0, :cond_8

    .line 1208569
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1208570
    const-string v0, "__EXTERNAL_SAMPLER__OES__"

    invoke-static {v3, v0}, LX/5Pa;->c(LX/5Pa;Ljava/lang/String;)I

    move-result v5

    .line 1208571
    if-nez v5, :cond_17

    move v0, v1

    :goto_4
    const-string v6, "Reserved external sampler unit should be zero but is %s.  Ensure reserveTextureUnit0 is called before any other textures are set."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v6, v1}, LX/64O;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1208572
    :cond_8
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1208573
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Pf;

    invoke-virtual {v3, v1, v0}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    goto :goto_5

    .line 1208574
    :cond_9
    const-string v0, "uSurfaceTransformMatrix"

    invoke-virtual {v3, v0, p1}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    .line 1208575
    const v0, 0x8ca6

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->u:[I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 1208576
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->pass_targets:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 1208577
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->pass_targets:[Ljava/lang/String;

    aget-object v4, v0, v1

    .line 1208578
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SI;

    invoke-virtual {v0}, LX/7SI;->a()LX/5Pf;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    .line 1208579
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1208580
    :cond_a
    move v1, v2

    .line 1208581
    :goto_7
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->pass_targets:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 1208582
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v0, v0, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->pass_targets:[Ljava/lang/String;

    aget-object v4, v0, v1

    .line 1208583
    const-string v0, "passIndex"

    invoke-virtual {v3, v0, v1}, LX/5Pa;->a(Ljava/lang/String;I)LX/5Pa;

    .line 1208584
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->x:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SI;

    .line 1208585
    const p1, 0x8d40

    const/4 v8, 0x0

    .line 1208586
    iget-object v5, v0, LX/7SI;->a:[I

    aget v5, v5, v8

    invoke-static {p1, v5}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1208587
    const v5, 0x8ce0

    const/16 v6, 0xde1

    .line 1208588
    iget-boolean v7, v0, LX/7SI;->b:Z

    if-eqz v7, :cond_18

    iget-object v7, v0, LX/7SI;->c:[LX/5Pf;

    const/4 p2, 0x0

    aget-object v7, v7, p2

    :goto_8
    move-object v7, v7

    .line 1208589
    iget v7, v7, LX/5Pf;->b:I

    invoke-static {p1, v5, v6, v7, v8}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 1208590
    iget v5, v0, LX/7SI;->d:I

    iget v6, v0, LX/7SI;->e:I

    invoke-static {v8, v8, v5, v6}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1208591
    iget-object v5, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->e:LX/5PR;

    invoke-virtual {v3, v5}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208592
    iget-boolean v5, v0, LX/7SI;->b:Z

    if-nez v5, :cond_19

    const/4 v5, 0x1

    :goto_9
    iput-boolean v5, v0, LX/7SI;->b:Z

    .line 1208593
    invoke-virtual {v0}, LX/7SI;->a()LX/5Pf;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    .line 1208594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1208595
    :cond_b
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    const-string v1, "passIndex"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1208596
    const-string v0, "passIndex"

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    iget-object v1, v1, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;->pass_targets:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {v3, v0, v1}, LX/5Pa;->a(Ljava/lang/String;I)LX/5Pa;

    .line 1208597
    :cond_c
    const v0, 0x8d40

    iget-object v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->u:[I

    aget v1, v1, v2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1208598
    iget v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->k:I

    iget v1, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->j:I

    invoke-static {v2, v2, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1208599
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->e:LX/5PR;

    invoke-virtual {v3, v0}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    goto/16 :goto_0

    .line 1208600
    :cond_d
    :try_start_1
    new-instance v3, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    invoke-direct {v3}, Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;-><init>()V

    iput-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->w:Lcom/facebook/videocodec/effects/renderers/ShaderRendererFilterConfig;

    .line 1208601
    :cond_e
    iget-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->d:Ljava/lang/String;

    .line 1208602
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/compiled.fs"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/7ew;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1208603
    const-string v6, "NEEDS_SURFACE_TRANSFORM"

    .line 1208604
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "#define "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " 1\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v4, v7

    .line 1208605
    invoke-static {v4}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1208606
    iget-object v6, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->f:LX/5Pc;

    iget-boolean v7, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->h:Z

    invoke-interface {v6, v3, v4, v7}, LX/5Pc;->a(Ljava/lang/String;Ljava/lang/String;Z)LX/5Pb;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g:LX/5Pb;

    .line 1208607
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 p5, -0x1

    const/4 v5, 0x0

    .line 1208608
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 1208609
    if-nez v6, :cond_12
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1208610
    :cond_f
    const/4 v8, 0x4

    const/4 v3, 0x0

    .line 1208611
    iget-object v4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    .line 1208612
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "uTime"

    aput-object v5, v4, v3

    const/4 v5, 0x1

    const-string v6, "uRenderSize"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "inputIsYUV"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "passIndex"

    aput-object v6, v4, v5

    .line 1208613
    iget-object v5, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->g:LX/5Pb;

    invoke-virtual {v5}, LX/5Pb;->a()LX/5Pa;

    move-result-object v5

    .line 1208614
    :goto_a
    if-ge v3, v8, :cond_11

    aget-object v6, v4, v3

    .line 1208615
    iget-object v7, v5, LX/5Pa;->a:LX/5Pb;

    iget v7, v7, LX/5Pb;->a:I

    invoke-static {v7, v6}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    .line 1208616
    const/4 p2, -0x1

    if-eq v7, p2, :cond_15

    const/4 v7, 0x1

    :goto_b
    move v7, v7

    .line 1208617
    if-eqz v7, :cond_10

    .line 1208618
    iget-object v7, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->q:Ljava/util/HashSet;

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1208619
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 1208620
    :cond_11
    goto/16 :goto_2

    .line 1208621
    :cond_12
    :try_start_2
    array-length v7, v6

    move v4, v5

    :goto_c
    if-ge v4, v7, :cond_f

    aget-object v8, v6, v4

    .line 1208622
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    const-string p3, ".png"

    invoke-virtual {p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p2

    if-ne p2, p5, :cond_13

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    const-string p3, ".jpg"

    invoke-virtual {p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p2

    if-eq p2, p5, :cond_14

    .line 1208623
    :cond_13
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    const-string p3, "\\."

    invoke-virtual {p2, p3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    aget-object p2, p2, v5

    .line 1208624
    iget-object p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->r:Ljava/util/HashMap;

    const/4 p4, 0x0

    invoke-virtual {p3, p2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208625
    new-instance p3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1208626
    iput-boolean v5, p3, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 1208627
    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v8

    invoke-virtual {v8}, LX/1bX;->n()LX/1bf;

    move-result-object v8

    .line 1208628
    iget-object p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->n:LX/1HI;

    sget-object p4, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v8, p4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v8

    invoke-static {v8}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v8

    .line 1208629
    iget p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    add-int/lit8 p3, p3, 0x1

    iput p3, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->o:I

    .line 1208630
    new-instance p3, LX/7SK;

    iget-object p4, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-direct {p3, p0, p2, p4}, LX/7SK;-><init>(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V

    iget-object p2, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->p:Ljava/util/concurrent/Executor;

    invoke-static {v8, p3, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1208631
    :cond_14
    add-int/lit8 v4, v4, 0x1

    goto :goto_c
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_15
    const/4 v7, 0x0

    goto :goto_b

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_17
    move v0, v4

    .line 1208632
    goto/16 :goto_4

    :cond_18
    iget-object v7, v0, LX/7SI;->c:[LX/5Pf;

    const/4 p2, 0x1

    aget-object v7, v7, p2

    goto/16 :goto_8

    .line 1208633
    :cond_19
    const/4 v5, 0x0

    goto/16 :goto_9
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1208530
    invoke-static {p0}, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->d(Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;)V

    .line 1208531
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208529
    iget-object v0, p0, Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;->l:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
