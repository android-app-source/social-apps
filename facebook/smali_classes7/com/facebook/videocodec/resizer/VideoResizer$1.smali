.class public final Lcom/facebook/videocodec/resizer/VideoResizer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7TH;

.field public final synthetic b:LX/7TF;

.field public final synthetic c:LX/7TG;


# direct methods
.method public constructor <init>(LX/7TG;LX/7TH;LX/7TF;)V
    .locals 0

    .prologue
    .line 1210227
    iput-object p1, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->c:LX/7TG;

    iput-object p2, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->a:LX/7TH;

    iput-object p3, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->b:LX/7TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1210228
    iget-object v0, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->c:LX/7TG;

    iget-object v0, v0, LX/7TG;->b:LX/0Sj;

    const-string v1, "VideoResizer"

    const-string v2, "start"

    invoke-interface {v0, v1, v2}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v1

    .line 1210229
    if-eqz v1, :cond_0

    .line 1210230
    invoke-interface {v1}, LX/0cV;->a()V

    .line 1210231
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->c:LX/7TG;

    iget-object v0, v0, LX/7TG;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7TC;

    .line 1210232
    iget-object v2, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->a:LX/7TH;

    invoke-virtual {v0, v2}, LX/7TC;->a(LX/7TH;)LX/7TD;

    move-result-object v0

    .line 1210233
    iget-object v2, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->b:LX/7TF;

    invoke-virtual {v2, v0}, LX/7TF;->a(LX/7TD;)Z

    .line 1210234
    iget-object v0, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->b:LX/7TF;

    const/4 v2, 0x0

    .line 1210235
    iput-object v2, v0, LX/7TF;->b:Ljava/lang/Thread;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1210236
    if-eqz v1, :cond_1

    .line 1210237
    invoke-interface {v1}, LX/0cV;->b()V

    .line 1210238
    :cond_1
    :goto_0
    return-void

    .line 1210239
    :catch_0
    move-exception v0

    .line 1210240
    :try_start_1
    iget-object v2, p0, Lcom/facebook/videocodec/resizer/VideoResizer$1;->b:LX/7TF;

    invoke-virtual {v2, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210241
    if-eqz v1, :cond_1

    .line 1210242
    invoke-interface {v1}, LX/0cV;->b()V

    goto :goto_0

    .line 1210243
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 1210244
    invoke-interface {v1}, LX/0cV;->b()V

    :cond_2
    throw v0
.end method
