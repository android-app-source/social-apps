.class public final enum Lcom/facebook/events/model/PrivacyKind;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/events/model/PrivacyKind;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/events/model/PrivacyKind;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/PrivacyKind;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PRIVATE:Lcom/facebook/events/model/PrivacyKind;

.field public static final enum PUBLIC:Lcom/facebook/events/model/PrivacyKind;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1274489
    new-instance v0, Lcom/facebook/events/model/PrivacyKind;

    const-string v1, "PRIVATE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/events/model/PrivacyKind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyKind;->PRIVATE:Lcom/facebook/events/model/PrivacyKind;

    .line 1274490
    new-instance v0, Lcom/facebook/events/model/PrivacyKind;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v3}, Lcom/facebook/events/model/PrivacyKind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyKind;->PUBLIC:Lcom/facebook/events/model/PrivacyKind;

    .line 1274491
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/events/model/PrivacyKind;

    sget-object v1, Lcom/facebook/events/model/PrivacyKind;->PRIVATE:Lcom/facebook/events/model/PrivacyKind;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/events/model/PrivacyKind;->PUBLIC:Lcom/facebook/events/model/PrivacyKind;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/events/model/PrivacyKind;->$VALUES:[Lcom/facebook/events/model/PrivacyKind;

    .line 1274492
    new-instance v0, LX/7vM;

    invoke-direct {v0}, LX/7vM;-><init>()V

    sput-object v0, Lcom/facebook/events/model/PrivacyKind;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1274493
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyKind;
    .locals 1

    .prologue
    .line 1274494
    const-class v0, Lcom/facebook/events/model/PrivacyKind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyKind;

    return-object v0
.end method

.method public static values()[Lcom/facebook/events/model/PrivacyKind;
    .locals 1

    .prologue
    .line 1274495
    sget-object v0, Lcom/facebook/events/model/PrivacyKind;->$VALUES:[Lcom/facebook/events/model/PrivacyKind;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/events/model/PrivacyKind;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1274496
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1274497
    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyKind;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274498
    return-void
.end method
