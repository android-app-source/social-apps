.class public final enum Lcom/facebook/events/model/EventType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/events/model/EventType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/events/model/EventType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/EventType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NORMAL:Lcom/facebook/events/model/EventType;

.field public static final enum QUICK_INVITE:Lcom/facebook/events/model/EventType;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1274346
    new-instance v0, Lcom/facebook/events/model/EventType;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/facebook/events/model/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/EventType;->NORMAL:Lcom/facebook/events/model/EventType;

    .line 1274347
    new-instance v0, Lcom/facebook/events/model/EventType;

    const-string v1, "QUICK_INVITE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/events/model/EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/EventType;->QUICK_INVITE:Lcom/facebook/events/model/EventType;

    .line 1274348
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/events/model/EventType;

    sget-object v1, Lcom/facebook/events/model/EventType;->NORMAL:Lcom/facebook/events/model/EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/events/model/EventType;->QUICK_INVITE:Lcom/facebook/events/model/EventType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/events/model/EventType;->$VALUES:[Lcom/facebook/events/model/EventType;

    .line 1274349
    new-instance v0, LX/7vG;

    invoke-direct {v0}, LX/7vG;-><init>()V

    sput-object v0, Lcom/facebook/events/model/EventType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1274350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/events/model/EventType;
    .locals 1

    .prologue
    .line 1274351
    const-class v0, Lcom/facebook/events/model/EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/events/model/EventType;
    .locals 1

    .prologue
    .line 1274352
    sget-object v0, Lcom/facebook/events/model/EventType;->$VALUES:[Lcom/facebook/events/model/EventType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/events/model/EventType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1274353
    const/4 v0, 0x0

    return v0
.end method

.method public final getContentValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1274354
    invoke-virtual {p0}, Lcom/facebook/events/model/EventType;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1274355
    invoke-virtual {p0}, Lcom/facebook/events/model/EventType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274356
    return-void
.end method
