.class public Lcom/facebook/events/model/EventArtist;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/EventArtist;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:I

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1274222
    new-instance v0, LX/7vD;

    invoke-direct {v0}, LX/7vD;-><init>()V

    sput-object v0, Lcom/facebook/events/model/EventArtist;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7vE;)V
    .locals 1

    .prologue
    .line 1274223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274224
    iget-object v0, p1, LX/7vE;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    .line 1274225
    iget-object v0, p1, LX/7vE;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventArtist;->a:Ljava/lang/String;

    .line 1274226
    iget-object v0, p1, LX/7vE;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventArtist;->c:Ljava/lang/String;

    .line 1274227
    iget-boolean v0, p1, LX/7vE;->d:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/EventArtist;->d:Z

    .line 1274228
    iget v0, p1, LX/7vE;->e:I

    iput v0, p0, Lcom/facebook/events/model/EventArtist;->e:I

    .line 1274229
    iget-object v0, p1, LX/7vE;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventArtist;->f:Ljava/lang/String;

    .line 1274230
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1274231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/model/EventArtist;->a:Ljava/lang/String;

    .line 1274233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    .line 1274234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/model/EventArtist;->c:Ljava/lang/String;

    .line 1274235
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/model/EventArtist;->d:Z

    .line 1274236
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/EventArtist;->e:I

    .line 1274237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventArtist;->f:Ljava/lang/String;

    .line 1274238
    return-void

    .line 1274239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1274240
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1274241
    iget-object v0, p0, Lcom/facebook/events/model/EventArtist;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274242
    iget-object v0, p0, Lcom/facebook/events/model/EventArtist;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274243
    iget-object v0, p0, Lcom/facebook/events/model/EventArtist;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274244
    iget-boolean v0, p0, Lcom/facebook/events/model/EventArtist;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1274245
    iget v0, p0, Lcom/facebook/events/model/EventArtist;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1274246
    iget-object v0, p0, Lcom/facebook/events/model/EventArtist;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274247
    return-void

    .line 1274248
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
