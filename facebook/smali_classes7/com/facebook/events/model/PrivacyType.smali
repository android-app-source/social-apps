.class public final enum Lcom/facebook/events/model/PrivacyType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/events/model/PrivacyType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/events/model/PrivacyType;

.field public static final enum COMMUNITY:Lcom/facebook/events/model/PrivacyType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/PrivacyType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FRIENDS_OF_FRIENDS:Lcom/facebook/events/model/PrivacyType;

.field public static final enum FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

.field public static final enum GROUP:Lcom/facebook/events/model/PrivacyType;

.field public static final enum INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

.field public static final enum PAGE:Lcom/facebook/events/model/PrivacyType;

.field public static SELECTABLE_PRIVACY_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/PrivacyType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1274503
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "FRIENDS_OF_FRIENDS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_FRIENDS:Lcom/facebook/events/model/PrivacyType;

    .line 1274504
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "FRIENDS_OF_GUESTS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    .line 1274505
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    .line 1274506
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "INVITE_ONLY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    .line 1274507
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    .line 1274508
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "USER_PUBLIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    .line 1274509
    new-instance v0, Lcom/facebook/events/model/PrivacyType;

    const-string v1, "COMMUNITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/events/model/PrivacyType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    .line 1274510
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/events/model/PrivacyType;

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_FRIENDS:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->$VALUES:[Lcom/facebook/events/model/PrivacyType;

    .line 1274511
    new-array v0, v6, [Lcom/facebook/events/model/PrivacyType;

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    aput-object v1, v0, v5

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->SELECTABLE_PRIVACY_TYPES:Ljava/util/List;

    .line 1274512
    new-instance v0, LX/7vN;

    invoke-direct {v0}, LX/7vN;-><init>()V

    sput-object v0, Lcom/facebook/events/model/PrivacyType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1274528
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1274529
    return-void
.end method

.method public static getPrivacyTypeForEventEditInputData(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventPrivacyEnum;
    .end annotation

    .prologue
    .line 1274518
    sget-object v0, LX/7vO;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1274519
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type does not support setting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1274520
    :pswitch_0
    const-string v0, "FRIENDS_OF_FRIENDS"

    .line 1274521
    :goto_0
    return-object v0

    .line 1274522
    :pswitch_1
    const-string v0, "FRIENDS_OF_GUESTS"

    goto :goto_0

    .line 1274523
    :pswitch_2
    const-string v0, "INVITE_ONLY"

    goto :goto_0

    .line 1274524
    :pswitch_3
    const-string v0, "PAGE"

    goto :goto_0

    .line 1274525
    :pswitch_4
    const-string v0, "USER_PUBLIC"

    goto :goto_0

    .line 1274526
    :pswitch_5
    const-string v0, "GROUP"

    goto :goto_0

    .line 1274527
    :pswitch_6
    const-string v0, "COMMUNITY"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyType;
    .locals 1

    .prologue
    .line 1274517
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/events/model/PrivacyType;
    .locals 1

    .prologue
    .line 1274530
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->$VALUES:[Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/events/model/PrivacyType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1274516
    const/4 v0, 0x0

    return v0
.end method

.method public final getContentValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1274515
    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1274513
    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274514
    return-void
.end method
