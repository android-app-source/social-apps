.class public Lcom/facebook/events/model/EventUser;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/7vJ;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:I

.field public final h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

.field public final j:Z

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1274431
    new-instance v0, LX/7vH;

    invoke-direct {v0}, LX/7vH;-><init>()V

    sput-object v0, Lcom/facebook/events/model/EventUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7vI;)V
    .locals 1

    .prologue
    .line 1274419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274420
    iget-object v0, p1, LX/7vI;->a:LX/7vJ;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    .line 1274421
    iget-object v0, p1, LX/7vI;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    .line 1274422
    iget-object v0, p1, LX/7vI;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    .line 1274423
    iget-object v0, p1, LX/7vI;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    .line 1274424
    iget-object v0, p1, LX/7vI;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    .line 1274425
    iget-object v0, p1, LX/7vI;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->f:Ljava/lang/String;

    .line 1274426
    iget v0, p1, LX/7vI;->g:I

    iput v0, p0, Lcom/facebook/events/model/EventUser;->g:I

    .line 1274427
    iget-object v0, p1, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1274428
    iget-object v0, p1, LX/7vI;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1274429
    iget-boolean v0, p1, LX/7vI;->j:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/EventUser;->j:Z

    .line 1274430
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1274404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274405
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1274406
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    .line 1274407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    .line 1274408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    .line 1274409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    .line 1274410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    .line 1274411
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->f:Ljava/lang/String;

    .line 1274412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/EventUser;->g:I

    .line 1274413
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1274414
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1274415
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/model/EventUser;->j:Z

    .line 1274416
    return-void

    .line 1274417
    :cond_0
    invoke-static {v0}, LX/7vJ;->valueOf(Ljava/lang/String;)LX/7vJ;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1274418
    const/4 v0, 0x0

    return v0
.end method

.method public final i()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1274389
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1274390
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274391
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274392
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274393
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274394
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274395
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274396
    iget v0, p0, Lcom/facebook/events/model/EventUser;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1274397
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274398
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1274399
    iget-boolean v0, p0, Lcom/facebook/events/model/EventUser;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1274400
    return-void

    .line 1274401
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    invoke-virtual {v0}, LX/7vJ;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1274402
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1274403
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
