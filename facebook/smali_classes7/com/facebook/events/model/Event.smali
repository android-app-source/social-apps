.class public Lcom/facebook/events/model/Event;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/facebook/events/model/Event;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:J

.field public B:Z

.field private C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Z

.field private I:Ljava/util/Date;

.field private J:J

.field private K:Ljava/util/Date;

.field private L:J

.field public M:Ljava/util/TimeZone;

.field public N:Z

.field public O:LX/7vL;

.field public P:J

.field public Q:Ljava/lang/String;

.field public R:Ljava/lang/String;

.field public S:Ljava/util/TimeZone;

.field public T:I

.field public U:Ljava/lang/String;

.field public V:Landroid/net/Uri;

.field public W:Ljava/lang/String;

.field public X:Landroid/net/Uri;

.field public Y:Landroid/net/Uri;

.field public Z:Landroid/net/Uri;

.field public a:Ljava/lang/String;

.field public aa:Ljava/lang/String;

.field private ab:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;"
        }
    .end annotation
.end field

.field public ac:Z

.field public ad:Ljava/lang/String;

.field public ae:I

.field public af:Ljava/lang/String;

.field public ag:I

.field public ah:Ljava/lang/String;

.field public ai:I

.field public aj:Lcom/facebook/events/model/EventUser;

.field public ak:Ljava/lang/String;

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public an:Ljava/lang/String;

.field public ao:Ljava/lang/String;

.field public ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

.field public aq:I

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

.field public d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public e:Lcom/facebook/events/model/EventType;

.field public f:Lcom/facebook/events/model/PrivacyType;

.field public g:Lcom/facebook/events/model/PrivacyKind;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:LX/03R;

.field public l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public o:Ljava/lang/String;

.field public p:I

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1274017
    new-instance v0, LX/7vA;

    invoke-direct {v0}, LX/7vA;-><init>()V

    sput-object v0, Lcom/facebook/events/model/Event;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7vC;)V
    .locals 2

    .prologue
    .line 1274018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274019
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    .line 1274020
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->B:Z

    .line 1274021
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274022
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/events/model/Event;->P:J

    .line 1274023
    iget-object v0, p1, LX/7vC;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    .line 1274024
    iget-object v0, p1, LX/7vC;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    .line 1274025
    iget-object v0, p1, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1274026
    iget-object v0, p1, LX/7vC;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1274027
    iget-object v0, p1, LX/7vC;->e:Lcom/facebook/events/model/EventType;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    .line 1274028
    iget-object v0, p1, LX/7vC;->f:Lcom/facebook/events/model/PrivacyKind;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    .line 1274029
    iget-object v0, p1, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    .line 1274030
    iget-boolean v0, p1, LX/7vC;->h:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->h:Z

    .line 1274031
    iget-boolean v0, p1, LX/7vC;->i:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->i:Z

    .line 1274032
    iget-boolean v0, p1, LX/7vC;->j:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->j:Z

    .line 1274033
    iget-object v0, p1, LX/7vC;->k:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    .line 1274034
    iget-boolean v0, p1, LX/7vC;->l:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->l:Z

    .line 1274035
    iget-object v0, p1, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1274036
    iget-object v0, p1, LX/7vC;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1274037
    iget-object v0, p1, LX/7vC;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    .line 1274038
    iget v0, p1, LX/7vC;->p:I

    iput v0, p0, Lcom/facebook/events/model/Event;->p:I

    .line 1274039
    iget-object v0, p1, LX/7vC;->q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    .line 1274040
    iget-object v0, p1, LX/7vC;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    .line 1274041
    iget-object v0, p1, LX/7vC;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    .line 1274042
    iget-object v0, p1, LX/7vC;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    .line 1274043
    iget-object v0, p1, LX/7vC;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    .line 1274044
    iget-object v0, p1, LX/7vC;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    .line 1274045
    iget-object v0, p1, LX/7vC;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    .line 1274046
    iget-object v0, p1, LX/7vC;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    .line 1274047
    iget-boolean v0, p1, LX/7vC;->y:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->y:Z

    .line 1274048
    iget-boolean v0, p1, LX/7vC;->z:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->z:Z

    .line 1274049
    iget-wide v0, p1, LX/7vC;->A:J

    iput-wide v0, p0, Lcom/facebook/events/model/Event;->A:J

    .line 1274050
    iget-boolean v0, p1, LX/7vC;->B:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->B:Z

    .line 1274051
    iget-object v0, p1, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274052
    iget-object v0, p1, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274053
    iget-boolean v0, p1, LX/7vC;->E:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->E:Z

    .line 1274054
    iget-object v0, p1, LX/7vC;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    .line 1274055
    iget-object v0, p1, LX/7vC;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    .line 1274056
    iget-boolean v0, p1, LX/7vC;->H:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->H:Z

    .line 1274057
    iget-object v0, p1, LX/7vC;->I:Ljava/util/Date;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    .line 1274058
    iget-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 1274059
    iget-object v0, p1, LX/7vC;->I:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/model/Event;->J:J

    .line 1274060
    :cond_0
    iget-object v0, p1, LX/7vC;->J:Ljava/util/Date;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    .line 1274061
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    if-eqz v0, :cond_1

    .line 1274062
    iget-object v0, p1, LX/7vC;->J:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/model/Event;->L:J

    .line 1274063
    :cond_1
    iget-object v0, p1, LX/7vC;->K:Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    .line 1274064
    iget-boolean v0, p1, LX/7vC;->L:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->N:Z

    .line 1274065
    iget-object v0, p1, LX/7vC;->M:LX/7vL;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1274066
    iget-wide v0, p1, LX/7vC;->N:J

    iput-wide v0, p0, Lcom/facebook/events/model/Event;->P:J

    .line 1274067
    iget-object v0, p1, LX/7vC;->O:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    .line 1274068
    iget-object v0, p1, LX/7vC;->S:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    .line 1274069
    iget-object v0, p1, LX/7vC;->P:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    .line 1274070
    iget-object v0, p1, LX/7vC;->Q:Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    .line 1274071
    iget v0, p1, LX/7vC;->R:I

    iput v0, p0, Lcom/facebook/events/model/Event;->T:I

    .line 1274072
    iget-object v0, p1, LX/7vC;->T:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    .line 1274073
    iget-object v0, p1, LX/7vC;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    .line 1274074
    iget-object v0, p1, LX/7vC;->V:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    .line 1274075
    iget-object v0, p1, LX/7vC;->W:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    .line 1274076
    iget-object v0, p1, LX/7vC;->X:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    .line 1274077
    iget-object v0, p1, LX/7vC;->Y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    .line 1274078
    iget-object v0, p1, LX/7vC;->Z:Ljava/util/EnumSet;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ab:Ljava/util/EnumSet;

    .line 1274079
    iget-boolean v0, p1, LX/7vC;->aa:Z

    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->ac:Z

    .line 1274080
    iget-object v0, p1, LX/7vC;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    .line 1274081
    iget v0, p1, LX/7vC;->ac:I

    iput v0, p0, Lcom/facebook/events/model/Event;->ae:I

    .line 1274082
    iget-object v0, p1, LX/7vC;->ad:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    .line 1274083
    iget v0, p1, LX/7vC;->ae:I

    iput v0, p0, Lcom/facebook/events/model/Event;->ag:I

    .line 1274084
    iget-object v0, p1, LX/7vC;->af:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    .line 1274085
    iget v0, p1, LX/7vC;->ag:I

    iput v0, p0, Lcom/facebook/events/model/Event;->ai:I

    .line 1274086
    iget-object v0, p1, LX/7vC;->ah:Lcom/facebook/events/model/EventUser;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    .line 1274087
    iget-object v0, p1, LX/7vC;->ai:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    .line 1274088
    iget-object v0, p1, LX/7vC;->aj:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->al:Ljava/lang/String;

    .line 1274089
    iget-object v0, p1, LX/7vC;->ak:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    .line 1274090
    iget-object v0, p1, LX/7vC;->al:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    .line 1274091
    iget-object v0, p1, LX/7vC;->am:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    .line 1274092
    iget-object v0, p1, LX/7vC;->an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1274093
    iget v0, p1, LX/7vC;->ao:I

    iput v0, p0, Lcom/facebook/events/model/Event;->aq:I

    .line 1274094
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1274095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274096
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    .line 1274097
    iput-boolean v1, p0, Lcom/facebook/events/model/Event;->B:Z

    .line 1274098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274099
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/facebook/events/model/Event;->P:J

    .line 1274100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    .line 1274101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    .line 1274102
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1274103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1274104
    const-class v0, Lcom/facebook/events/model/EventType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventType;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    .line 1274105
    const-class v0, Lcom/facebook/events/model/PrivacyKind;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyKind;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    .line 1274106
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    .line 1274107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->h:Z

    .line 1274108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->i:Z

    .line 1274109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->j:Z

    .line 1274110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    .line 1274111
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->l:Z

    .line 1274112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1274113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1274114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    .line 1274115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->p:I

    .line 1274116
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1274117
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1274118
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1274119
    iput-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    .line 1274120
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    .line 1274121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    .line 1274122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    .line 1274123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    .line 1274124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    .line 1274125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    .line 1274126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    .line 1274127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->y:Z

    .line 1274128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->z:Z

    .line 1274129
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/events/model/Event;->A:J

    .line 1274130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->B:Z

    .line 1274131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1274133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->E:Z

    .line 1274134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    .line 1274135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    .line 1274136
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_d

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->H:Z

    .line 1274137
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/events/model/Event;->J:J

    .line 1274138
    new-instance v0, Ljava/util/Date;

    iget-wide v4, p0, Lcom/facebook/events/model/Event;->J:J

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    .line 1274139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1274140
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/events/model/Event;->L:J

    .line 1274141
    new-instance v0, Ljava/util/Date;

    iget-wide v4, p0, Lcom/facebook/events/model/Event;->L:J

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    .line 1274142
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1274143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    .line 1274144
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_e

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/facebook/events/model/Event;->N:Z

    .line 1274145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    .line 1274146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1274147
    new-instance v0, LX/7vL;

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    invoke-direct {v0, v4, v5, v6, v7}, LX/7vL;-><init>(DD)V

    iput-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1274148
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/events/model/Event;->P:J

    .line 1274149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    .line 1274150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1274151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    .line 1274152
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->T:I

    .line 1274153
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    .line 1274154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    .line 1274155
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    .line 1274156
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    .line 1274157
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    .line 1274158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    .line 1274159
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/7vK;->deserializeCapabilities(J)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ab:Ljava/util/EnumSet;

    .line 1274160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_f

    :goto_b
    iput-boolean v1, p0, Lcom/facebook/events/model/Event;->ac:Z

    .line 1274161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    .line 1274162
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->ae:I

    .line 1274163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    .line 1274164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->ag:I

    .line 1274165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    .line 1274166
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->ai:I

    .line 1274167
    const-class v0, Lcom/facebook/events/model/EventUser;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    .line 1274168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    .line 1274169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->al:Ljava/lang/String;

    .line 1274170
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    .line 1274171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    .line 1274172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    .line 1274173
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1274174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/model/Event;->aq:I

    .line 1274175
    return-void

    :cond_4
    move v0, v2

    .line 1274176
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 1274177
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 1274178
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 1274179
    goto/16 :goto_3

    .line 1274180
    :cond_8
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    goto/16 :goto_4

    :cond_9
    move v0, v2

    .line 1274181
    goto/16 :goto_5

    :cond_a
    move v0, v2

    .line 1274182
    goto/16 :goto_6

    :cond_b
    move v0, v2

    .line 1274183
    goto/16 :goto_7

    :cond_c
    move v0, v2

    .line 1274184
    goto/16 :goto_8

    :cond_d
    move v0, v2

    .line 1274185
    goto/16 :goto_9

    :cond_e
    move v0, v2

    .line 1274186
    goto/16 :goto_a

    :cond_f
    move v1, v2

    .line 1274187
    goto/16 :goto_b
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 2

    .prologue
    .line 1274005
    sget-object v0, LX/7vB;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1274006
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    :goto_0
    return-object v0

    .line 1274007
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 1274008
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 1274009
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 1274010
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/events/model/Event;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1274188
    if-nez p0, :cond_1

    .line 1274189
    :cond_0
    :goto_0
    return v0

    .line 1274190
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 1274191
    if-eqz v1, :cond_0

    .line 1274192
    iget-object v1, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 1274193
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1274194
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z
    .locals 1

    .prologue
    .line 1274195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(J)Z
    .locals 2

    .prologue
    .line 1274196
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z
    .locals 1

    .prologue
    .line 1274217
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PUBLIC_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 1

    .prologue
    .line 1274197
    iget-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1274198
    iget-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final L()Ljava/util/Date;
    .locals 4

    .prologue
    .line 1274199
    iget-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1274200
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Event ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has null start date, which is not allowed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1274201
    :cond_0
    iget-wide v0, p0, Lcom/facebook/events/model/Event;->J:J

    iget-object v2, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1274202
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/facebook/events/model/Event;->J:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    .line 1274203
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    return-object v0
.end method

.method public final M()J
    .locals 3

    .prologue
    .line 1274204
    iget-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1274205
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Event ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has null start date, which is not allowed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1274206
    :cond_0
    iget-wide v0, p0, Lcom/facebook/events/model/Event;->J:J

    return-wide v0
.end method

.method public final N()Ljava/util/Date;
    .locals 4

    .prologue
    .line 1274207
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1274208
    const/4 v0, 0x0

    .line 1274209
    :goto_0
    return-object v0

    .line 1274210
    :cond_0
    iget-wide v0, p0, Lcom/facebook/events/model/Event;->L:J

    iget-object v2, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1274211
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/facebook/events/model/Event;->L:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    .line 1274212
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    goto :goto_0
.end method

.method public final O()J
    .locals 3

    .prologue
    .line 1274213
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1274214
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Event ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has null end date, unexpected method call."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1274215
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 1274216
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final S()D
    .locals 4

    .prologue
    .line 1274011
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1274012
    iget-wide v2, v0, LX/7vL;->a:D

    move-wide v0, v2

    .line 1274013
    return-wide v0
.end method

.method public final T()D
    .locals 4

    .prologue
    .line 1274014
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1274015
    iget-wide v2, v0, LX/7vL;->b:D

    move-wide v0, v2

    .line 1274016
    return-wide v0
.end method

.method public final a(J)J
    .locals 1

    .prologue
    .line 1273897
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    if-nez v0, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide p1

    goto :goto_0
.end method

.method public final a(LX/7vK;)Z
    .locals 1

    .prologue
    .line 1273896
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ab:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1273890
    iget-object v0, p0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 1273891
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v1, v2

    .line 1273892
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1273893
    iget-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    .line 1273894
    goto :goto_0

    .line 1273895
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final ag()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1273889
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ab:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clone()Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final ap()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1273879
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    .line 1273880
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v1

    .line 1273881
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    .line 1273882
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    .line 1273883
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1273884
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final av()Z
    .locals 11

    .prologue
    .line 1273876
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->N:Z

    iget-wide v2, p0, Lcom/facebook/events/model/Event;->J:J

    iget-wide v4, p0, Lcom/facebook/events/model/Event;->L:J

    .line 1273877
    if-eqz v0, :cond_0

    const-wide/32 v6, 0x5265c00

    add-long/2addr v6, v2

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    const/4 v6, 0x1

    :goto_1
    move v0, v6

    .line 1273878
    return v0

    :cond_0
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1273875
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 1273885
    check-cast p1, Lcom/facebook/events/model/Event;

    .line 1273886
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    .line 1273887
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v2

    .line 1273888
    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1273898
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1273899
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    check-cast p1, Lcom/facebook/events/model/Event;

    iget-object v1, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1273900
    iget-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1273901
    iget-object v0, p0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1273902
    iget-object v0, p0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1273903
    iget-object v0, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273904
    iget-object v0, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273905
    iget-object v0, p0, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1273906
    iget-object v0, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273907
    iget-object v0, p0, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273908
    iget-object v0, p0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273909
    iget-object v0, p0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273910
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->h:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273911
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->i:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273912
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->j:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273913
    iget-object v0, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273914
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->l:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273915
    iget-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273916
    iget-object v0, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273917
    iget-object v0, p0, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273918
    iget v0, p0, Lcom/facebook/events/model/Event;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273919
    iget-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1273920
    iget-object v0, p0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273921
    iget-object v0, p0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273922
    iget-object v0, p0, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273923
    iget-object v0, p0, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273924
    iget-object v0, p0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273925
    iget-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273926
    iget-object v0, p0, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273927
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->y:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273928
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->z:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273929
    iget-wide v4, p0, Lcom/facebook/events/model/Event;->A:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1273930
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->B:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273931
    iget-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-nez v0, :cond_a

    move-object v0, v3

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273932
    iget-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-nez v0, :cond_b

    :goto_b
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273933
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->E:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273934
    iget-object v0, p0, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273935
    iget-object v0, p0, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273936
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->H:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273937
    iget-object v0, p0, Lcom/facebook/events/model/Event;->I:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1273938
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    if-nez v0, :cond_e

    .line 1273939
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273940
    :goto_e
    iget-object v0, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    if-nez v0, :cond_f

    .line 1273941
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273942
    :goto_f
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->N:Z

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273943
    iget-object v0, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273944
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    if-nez v0, :cond_11

    .line 1273945
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273946
    :goto_11
    iget-wide v4, p0, Lcom/facebook/events/model/Event;->P:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1273947
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273948
    iget-object v0, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    if-nez v0, :cond_12

    .line 1273949
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273950
    :goto_12
    iget v0, p0, Lcom/facebook/events/model/Event;->T:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273951
    iget-object v0, p0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273952
    iget-object v0, p0, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273953
    iget-object v0, p0, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273954
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273955
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273956
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273957
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ab:Ljava/util/EnumSet;

    invoke-static {v0}, LX/7vK;->serializeCapabilities(Ljava/util/EnumSet;)J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1273958
    iget-boolean v0, p0, Lcom/facebook/events/model/Event;->ac:Z

    if-eqz v0, :cond_13

    :goto_13
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273959
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273960
    iget v0, p0, Lcom/facebook/events/model/Event;->ae:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273961
    iget-object v0, p0, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273962
    iget v0, p0, Lcom/facebook/events/model/Event;->ag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273963
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273964
    iget v0, p0, Lcom/facebook/events/model/Event;->ai:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273965
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273966
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273967
    iget-object v0, p0, Lcom/facebook/events/model/Event;->al:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273968
    iget-object v0, p0, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273969
    iget-object v0, p0, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273970
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273971
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1273972
    iget v0, p0, Lcom/facebook/events/model/Event;->aq:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273973
    return-void

    .line 1273974
    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1273975
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1273976
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 1273977
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 1273978
    goto/16 :goto_4

    .line 1273979
    :cond_5
    const-string v0, ""

    goto/16 :goto_5

    .line 1273980
    :cond_6
    const-string v0, ""

    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 1273981
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 1273982
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 1273983
    goto/16 :goto_9

    .line 1273984
    :cond_a
    iget-object v0, p0, Lcom/facebook/events/model/Event;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a

    .line 1273985
    :cond_b
    iget-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 1273986
    goto/16 :goto_c

    :cond_d
    move v0, v2

    .line 1273987
    goto/16 :goto_d

    .line 1273988
    :cond_e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273989
    iget-object v0, p0, Lcom/facebook/events/model/Event;->K:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_e

    .line 1273990
    :cond_f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273991
    iget-object v0, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_10
    move v0, v2

    .line 1273992
    goto/16 :goto_10

    .line 1273993
    :cond_11
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273994
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1273995
    iget-wide v6, v0, LX/7vL;->a:D

    move-wide v4, v6

    .line 1273996
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1273997
    iget-object v0, p0, Lcom/facebook/events/model/Event;->O:LX/7vL;

    .line 1273998
    iget-wide v6, v0, LX/7vL;->b:D

    move-wide v4, v6

    .line 1273999
    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    goto/16 :goto_11

    .line 1274000
    :cond_12
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1274001
    iget-object v0, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_12

    :cond_13
    move v1, v2

    .line 1274002
    goto/16 :goto_13
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1274003
    iget-object v0, p0, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1274004
    iget-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    return-object v0
.end method
