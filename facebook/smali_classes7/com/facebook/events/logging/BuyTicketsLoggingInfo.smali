.class public Lcom/facebook/events/logging/BuyTicketsLoggingInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/logging/BuyTicketsLoggingInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/events/common/ActionMechanism;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1273709
    new-instance v0, LX/7v9;

    invoke-direct {v0}, LX/7v9;-><init>()V

    sput-object v0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1273691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1273692
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    .line 1273693
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    .line 1273694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->c:Ljava/lang/String;

    .line 1273695
    const-class v0, Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->d:Lcom/facebook/events/common/ActionMechanism;

    .line 1273696
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 0

    .prologue
    .line 1273697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1273698
    iput-object p1, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    .line 1273699
    iput-object p2, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    .line 1273700
    iput-object p3, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->c:Ljava/lang/String;

    .line 1273701
    iput-object p4, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->d:Lcom/facebook/events/common/ActionMechanism;

    .line 1273702
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1273703
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1273704
    iget-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273705
    iget-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273706
    iget-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1273707
    iget-object v0, p0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->d:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1273708
    return-void
.end method
