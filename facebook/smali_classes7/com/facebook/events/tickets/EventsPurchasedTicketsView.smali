.class public Lcom/facebook/events/tickets/EventsPurchasedTicketsView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/fig/button/FigButton;

.field private c:Landroid/view/View$OnClickListener;

.field public d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1275056
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1275057
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1275058
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1275059
    invoke-direct {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->b()V

    .line 1275060
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275061
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1275062
    invoke-direct {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->b()V

    .line 1275063
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1275064
    const-class v0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1275065
    const v0, 0x7f030502

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1275066
    const v0, 0x7f0d0e43

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->b:Lcom/facebook/fig/button/FigButton;

    .line 1275067
    new-instance v0, LX/7vj;

    invoke-direct {v0, p0}, LX/7vj;-><init>(Lcom/facebook/events/tickets/EventsPurchasedTicketsView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->c:Landroid/view/View$OnClickListener;

    .line 1275068
    iget-object v0, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->b:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1275069
    return-void
.end method

.method public static e(Lcom/facebook/events/tickets/EventsPurchasedTicketsView;)V
    .locals 3

    .prologue
    .line 1275070
    invoke-virtual {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1275071
    iget-object v1, p0, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1275072
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1275073
    invoke-virtual {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1440

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1440

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/events/tickets/EventsPurchasedTicketsView;->setPadding(IIII)V

    .line 1275074
    return-void
.end method
