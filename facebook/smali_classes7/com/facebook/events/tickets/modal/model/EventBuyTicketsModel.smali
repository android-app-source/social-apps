.class public Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:Ljava/lang/String;

.field public final C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final D:Ljava/lang/String;

.field public final E:I

.field public final F:Ljava/lang/String;

.field public final G:Ljava/lang/String;

.field public final H:Z

.field public final I:Z

.field public final J:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final N:LX/7wp;

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/net/Uri;

.field public final g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

.field public final p:Z

.field public final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

.field public final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketingInfo$RegistrationSettings$Nodes$ScreenElements$;",
            ">;"
        }
    .end annotation
.end field

.field public final w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1276295
    new-instance v0, LX/7wn;

    invoke-direct {v0}, LX/7wn;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1276296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276297
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    .line 1276298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    .line 1276299
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->c:I

    .line 1276300
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    .line 1276301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    .line 1276302
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->f:Landroid/net/Uri;

    .line 1276303
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276304
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276305
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276306
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276307
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->k:Ljava/lang/String;

    .line 1276308
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->l:Ljava/lang/String;

    .line 1276309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->m:Ljava/lang/String;

    .line 1276310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->n:Ljava/lang/String;

    .line 1276311
    const-class v0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276312
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    .line 1276313
    const-class v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    .line 1276314
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    .line 1276315
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    .line 1276316
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->t:LX/0Px;

    .line 1276317
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1276318
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    .line 1276319
    const-class v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1276320
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    .line 1276321
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1276322
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    .line 1276324
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    .line 1276325
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    .line 1276326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    .line 1276327
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->values()[Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1276328
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1276329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->F:Ljava/lang/String;

    .line 1276330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->G:Ljava/lang/String;

    .line 1276331
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->H:Z

    .line 1276332
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->I:Z

    .line 1276333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->K:Ljava/lang/String;

    .line 1276334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->L:Ljava/lang/String;

    .line 1276335
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276336
    invoke-static {}, LX/7wp;->values()[LX/7wp;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    .line 1276337
    const-class v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->J:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1276338
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;ZLX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;LX/0Px;LX/0Px;Ljava/util/Map;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;ILjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;LX/7wp;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/events/logging/BuyTicketsLoggingInfo;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketingInfo$RegistrationSettings$Nodes$ScreenElements$;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;",
            "LX/7wp;",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1276339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276340
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    .line 1276341
    iput-object p2, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    .line 1276342
    iput p3, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->c:I

    .line 1276343
    iput-object p4, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    .line 1276344
    iput-object p5, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    .line 1276345
    iput-object p6, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->f:Landroid/net/Uri;

    .line 1276346
    iput-object p9, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276347
    iput-object p10, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276348
    iput-object p11, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->k:Ljava/lang/String;

    .line 1276349
    iput-object p12, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->l:Ljava/lang/String;

    .line 1276350
    iput-object p13, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->m:Ljava/lang/String;

    .line 1276351
    iput-object p14, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->n:Ljava/lang/String;

    .line 1276352
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276353
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    .line 1276354
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    .line 1276355
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    .line 1276356
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    .line 1276357
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->t:LX/0Px;

    .line 1276358
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1276359
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    .line 1276360
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1276361
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    .line 1276362
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276363
    iput-object p7, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276364
    iput-object p8, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276365
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    .line 1276366
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    .line 1276367
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    .line 1276368
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    .line 1276369
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1276370
    move/from16 v0, p31

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1276371
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->F:Ljava/lang/String;

    .line 1276372
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->G:Ljava/lang/String;

    .line 1276373
    move/from16 v0, p34

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->H:Z

    .line 1276374
    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->I:Z

    .line 1276375
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->K:Ljava/lang/String;

    .line 1276376
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->L:Ljava/lang/String;

    .line 1276377
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276378
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    .line 1276379
    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->J:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1276380
    return-void
.end method


# virtual methods
.method public final a()LX/7wo;
    .locals 2

    .prologue
    .line 1276381
    new-instance v0, LX/7wq;

    invoke-direct {v0}, LX/7wq;-><init>()V

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    .line 1276382
    iput-object v1, v0, LX/7wq;->a:Ljava/lang/String;

    .line 1276383
    move-object v0, v0

    .line 1276384
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    .line 1276385
    iput-object v1, v0, LX/7wq;->b:Ljava/lang/String;

    .line 1276386
    move-object v0, v0

    .line 1276387
    iget v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->c:I

    .line 1276388
    iput v1, v0, LX/7wq;->c:I

    .line 1276389
    move-object v0, v0

    .line 1276390
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    .line 1276391
    iput-object v1, v0, LX/7wq;->d:Ljava/lang/String;

    .line 1276392
    move-object v0, v0

    .line 1276393
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    .line 1276394
    iput-object v1, v0, LX/7wq;->e:Ljava/lang/String;

    .line 1276395
    move-object v0, v0

    .line 1276396
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->f:Landroid/net/Uri;

    .line 1276397
    iput-object v1, v0, LX/7wq;->f:Landroid/net/Uri;

    .line 1276398
    move-object v0, v0

    .line 1276399
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276400
    iput-object v1, v0, LX/7wq;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276401
    move-object v0, v0

    .line 1276402
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276403
    iput-object v1, v0, LX/7wq;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276404
    move-object v0, v0

    .line 1276405
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276406
    iput-object v1, v0, LX/7wq;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276407
    move-object v0, v0

    .line 1276408
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276409
    iput-object v1, v0, LX/7wq;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276410
    move-object v0, v0

    .line 1276411
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->k:Ljava/lang/String;

    .line 1276412
    iput-object v1, v0, LX/7wq;->k:Ljava/lang/String;

    .line 1276413
    move-object v0, v0

    .line 1276414
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->l:Ljava/lang/String;

    .line 1276415
    iput-object v1, v0, LX/7wq;->l:Ljava/lang/String;

    .line 1276416
    move-object v0, v0

    .line 1276417
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->m:Ljava/lang/String;

    .line 1276418
    iput-object v1, v0, LX/7wq;->m:Ljava/lang/String;

    .line 1276419
    move-object v0, v0

    .line 1276420
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->n:Ljava/lang/String;

    .line 1276421
    iput-object v1, v0, LX/7wq;->n:Ljava/lang/String;

    .line 1276422
    move-object v0, v0

    .line 1276423
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276424
    iput-object v1, v0, LX/7wq;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276425
    move-object v0, v0

    .line 1276426
    iget-boolean v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    .line 1276427
    iput-boolean v1, v0, LX/7wq;->p:Z

    .line 1276428
    move-object v0, v0

    .line 1276429
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    .line 1276430
    iput-object v1, v0, LX/7wq;->q:LX/0Px;

    .line 1276431
    move-object v0, v0

    .line 1276432
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    .line 1276433
    iput-object v1, v0, LX/7wq;->r:Ljava/lang/String;

    .line 1276434
    move-object v0, v0

    .line 1276435
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    .line 1276436
    iput-object v1, v0, LX/7wq;->s:Ljava/lang/String;

    .line 1276437
    move-object v0, v0

    .line 1276438
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->t:LX/0Px;

    .line 1276439
    iput-object v1, v0, LX/7wq;->t:LX/0Px;

    .line 1276440
    move-object v0, v0

    .line 1276441
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1276442
    iput-object v1, v0, LX/7wq;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1276443
    move-object v0, v0

    .line 1276444
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    .line 1276445
    iput-object v1, v0, LX/7wq;->v:LX/0Px;

    .line 1276446
    move-object v0, v0

    .line 1276447
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1276448
    iput-object v1, v0, LX/7wq;->w:LX/0Px;

    .line 1276449
    move-object v0, v0

    .line 1276450
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    .line 1276451
    iput-object v1, v0, LX/7wq;->x:Ljava/util/Map;

    .line 1276452
    move-object v0, v0

    .line 1276453
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276454
    iput-object v1, v0, LX/7wq;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276455
    move-object v0, v0

    .line 1276456
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    .line 1276457
    iput-object v1, v0, LX/7wq;->A:Ljava/lang/String;

    .line 1276458
    move-object v0, v0

    .line 1276459
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    .line 1276460
    iput-object v1, v0, LX/7wq;->B:Ljava/lang/String;

    .line 1276461
    move-object v0, v0

    .line 1276462
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    .line 1276463
    iput-object v1, v0, LX/7wq;->C:Ljava/lang/String;

    .line 1276464
    move-object v0, v0

    .line 1276465
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    .line 1276466
    iput-object v1, v0, LX/7wq;->D:Ljava/lang/String;

    .line 1276467
    move-object v0, v0

    .line 1276468
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1276469
    iput-object v1, v0, LX/7wq;->E:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1276470
    move-object v0, v0

    .line 1276471
    iget v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    .line 1276472
    iput v1, v0, LX/7wq;->I:I

    .line 1276473
    move-object v0, v0

    .line 1276474
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->F:Ljava/lang/String;

    .line 1276475
    iput-object v1, v0, LX/7wq;->J:Ljava/lang/String;

    .line 1276476
    move-object v0, v0

    .line 1276477
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->G:Ljava/lang/String;

    .line 1276478
    iput-object v1, v0, LX/7wq;->K:Ljava/lang/String;

    .line 1276479
    move-object v0, v0

    .line 1276480
    iget-boolean v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->H:Z

    .line 1276481
    iput-boolean v1, v0, LX/7wq;->L:Z

    .line 1276482
    move-object v0, v0

    .line 1276483
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->K:Ljava/lang/String;

    .line 1276484
    iput-object v1, v0, LX/7wq;->F:Ljava/lang/String;

    .line 1276485
    move-object v0, v0

    .line 1276486
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->L:Ljava/lang/String;

    .line 1276487
    iput-object v1, v0, LX/7wq;->G:Ljava/lang/String;

    .line 1276488
    move-object v0, v0

    .line 1276489
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276490
    iput-object v1, v0, LX/7wq;->H:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276491
    move-object v0, v0

    .line 1276492
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    .line 1276493
    iput-object v1, v0, LX/7wq;->N:LX/7wp;

    .line 1276494
    move-object v0, v0

    .line 1276495
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1276496
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1276497
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276498
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276499
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276500
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276501
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276502
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1276503
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276504
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276505
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276506
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276507
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276508
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276509
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276510
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276511
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1276512
    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1276513
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    .line 1276514
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1276515
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276516
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276517
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->t:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1276518
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1276519
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1276520
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1276521
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1276522
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1276523
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276524
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276525
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276526
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276527
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276528
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->C:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276529
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276530
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276531
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276532
    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->H:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1276533
    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->I:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1276534
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276535
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276536
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->M:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1276537
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    invoke-virtual {v0}, LX/7wp;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276538
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->J:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1276539
    return-void
.end method
