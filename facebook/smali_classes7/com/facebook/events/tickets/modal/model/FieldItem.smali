.class public Lcom/facebook/events/tickets/modal/model/FieldItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/modal/model/FieldItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/7wx;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Boolean;

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1276808
    new-instance v0, LX/7wv;

    invoke-direct {v0}, LX/7wv;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/modal/model/FieldItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1276809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276810
    sget-object v0, LX/7wx;->COMPOUND_MAP:LX/7wx;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    .line 1276811
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    .line 1276812
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    .line 1276813
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    .line 1276814
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    .line 1276815
    return-void
.end method

.method public constructor <init>(LX/0Rf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1276816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276817
    sget-object v0, LX/7wx;->STRING_SET:LX/7wx;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    .line 1276818
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    .line 1276819
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    .line 1276820
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    .line 1276821
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    .line 1276822
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1276823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276824
    const-class v0, LX/7wx;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7wx;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    .line 1276825
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    .line 1276826
    invoke-static {p1}, LX/46R;->e(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    .line 1276827
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    .line 1276828
    invoke-static {p1}, LX/46R;->h(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    .line 1276829
    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    .line 1276830
    return-void

    .line 1276831
    :cond_0
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1276832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276833
    sget-object v0, LX/7wx;->STRING:LX/7wx;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    .line 1276834
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    .line 1276835
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    .line 1276836
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    .line 1276837
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    .line 1276838
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1276839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276840
    sget-object v0, LX/7wx;->BOOLEAN:LX/7wx;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    .line 1276841
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    .line 1276842
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    .line 1276843
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    .line 1276844
    iput-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    .line 1276845
    return-void
.end method

.method public static a(Lorg/json/JSONException;)V
    .locals 3

    .prologue
    .line 1276846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error when creating JSON object, %s"

    invoke-virtual {p0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1276847
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1276848
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1276849
    sget-object v1, LX/7ww;->a:[I

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    invoke-virtual {v2}, LX/7wx;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1276850
    :cond_0
    :goto_0
    return v0

    .line 1276851
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 1276852
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 1276853
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    goto :goto_0

    .line 1276854
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1276855
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "address2"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1276856
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, LX/7xD;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1276857
    :cond_2
    const/4 v0, 0x1

    .line 1276858
    :goto_1
    move v0, v0

    .line 1276859
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1276860
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1276861
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276862
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    .line 1276863
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1276864
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1276865
    return-void
.end method
