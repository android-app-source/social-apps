.class public Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/tickets/modal/model/FieldItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1276712
    new-instance v0, LX/7wr;

    invoke-direct {v0}, LX/7wr;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1276705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276706
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    .line 1276707
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->a:Ljava/lang/String;

    .line 1276708
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->b:Ljava/lang/String;

    .line 1276709
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    const-class v1, Lcom/facebook/events/tickets/modal/model/FieldItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1276710
    invoke-static {p1}, LX/46R;->b(Landroid/os/Parcel;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    .line 1276711
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1276664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276665
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    .line 1276666
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->a:Ljava/lang/String;

    .line 1276667
    iput-object p2, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->b:Ljava/lang/String;

    .line 1276668
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    .line 1276669
    return-void
.end method

.method private static a(Lorg/json/JSONException;)V
    .locals 3

    .prologue
    .line 1276704
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Error when creating JSON object, %s"

    invoke-virtual {p0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final b()Lorg/json/JSONObject;
    .locals 12

    .prologue
    .line 1276676
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1276677
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1276678
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 1276679
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1276680
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 1276681
    const-string v7, "form_field_id"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1276682
    const-string v7, "value"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/tickets/modal/model/FieldItem;

    .line 1276683
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1276684
    :try_start_1
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 1276685
    sget-object v9, LX/7ww;->a:[I

    iget-object v10, v1, Lcom/facebook/events/tickets/modal/model/FieldItem;->a:LX/7wx;

    invoke-virtual {v10}, LX/7wx;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1276686
    :cond_0
    :goto_2
    move-object v8, v11
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1276687
    :goto_3
    :try_start_2
    move-object v1, v8

    .line 1276688
    invoke-virtual {v6, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1276689
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1276690
    :catch_0
    move-exception v0

    .line 1276691
    invoke-static {v0}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->a(Lorg/json/JSONException;)V

    .line 1276692
    :cond_1
    return-object v2

    .line 1276693
    :cond_2
    :try_start_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 1276694
    :catch_1
    :try_start_4
    move-exception v9

    .line 1276695
    invoke-static {v9}, Lcom/facebook/events/tickets/modal/model/FieldItem;->a(Lorg/json/JSONException;)V

    goto :goto_3
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1276696
    :pswitch_0
    const-string v9, "default_field"

    iget-object v10, v1, Lcom/facebook/events/tickets/modal/model/FieldItem;->b:Ljava/lang/String;

    invoke-virtual {v11, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 1276697
    :pswitch_1
    const-string v9, "default_field"

    iget-object v10, v1, Lcom/facebook/events/tickets/modal/model/FieldItem;->c:Ljava/lang/Boolean;

    invoke-virtual {v11, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 1276698
    :pswitch_2
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 1276699
    iget-object v9, v1, Lcom/facebook/events/tickets/modal/model/FieldItem;->d:LX/0Rf;

    invoke-virtual {v9}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1276700
    invoke-virtual {v10, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_4

    .line 1276701
    :cond_3
    const-string v9, "default_field"

    invoke-virtual {v11, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 1276702
    :pswitch_3
    iget-object v9, v1, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    invoke-virtual {v9}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v9

    invoke-virtual {v9}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 1276703
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v11, v10, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1276675
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1276670
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276671
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276672
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1276673
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1276674
    return-void
.end method
