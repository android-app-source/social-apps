.class public Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;"
        }
    .end annotation
.end field

.field public static a:I


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

.field public final e:J

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public l:I

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1276794
    const/4 v0, -0x1

    sput v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->a:I

    .line 1276795
    new-instance v0, LX/7wt;

    invoke-direct {v0}, LX/7wt;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1276780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276781
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    .line 1276782
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    .line 1276783
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    .line 1276784
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->e:J

    .line 1276785
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->f:J

    .line 1276786
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    .line 1276787
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    .line 1276788
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1276789
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    .line 1276790
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->k:Ljava/lang/String;

    .line 1276791
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    .line 1276792
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->m:LX/0Px;

    .line 1276793
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;JJIILcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;Ljava/lang/String;ILX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;",
            "JJII",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1276752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276753
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    .line 1276754
    iput-object p2, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    .line 1276755
    iput-object p3, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    .line 1276756
    iput-wide p4, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->e:J

    .line 1276757
    iput-wide p6, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->f:J

    .line 1276758
    iput p8, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    .line 1276759
    iput p9, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    .line 1276760
    iput-object p10, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1276761
    iput-object p11, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    .line 1276762
    iput-object p12, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->k:Ljava/lang/String;

    .line 1276763
    iput p13, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    .line 1276764
    iput-object p14, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->m:LX/0Px;

    .line 1276765
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1276779
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1276766
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276767
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276768
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1276769
    iget-wide v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1276770
    iget-wide v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1276771
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276772
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276773
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1276774
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276775
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1276776
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276777
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->m:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1276778
    return-void
.end method
