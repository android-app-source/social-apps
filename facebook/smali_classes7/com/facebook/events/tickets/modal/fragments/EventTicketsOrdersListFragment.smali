.class public Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;
.super Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;
.source ""


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel;",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Landroid/view/View;

.field private d:LX/0h5;

.field public e:LX/1P1;

.field public f:LX/7x3;

.field public g:LX/7wI;

.field public h:LX/1ON;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ON",
            "<",
            "LX/7wN;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:LX/7x9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/7wJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/7x4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1276246
    new-instance v0, LX/7wk;

    invoke-direct {v0}, LX/7wk;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1276247
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    invoke-static {v3}, LX/7x9;->b(LX/0QB;)LX/7x9;

    move-result-object v1

    check-cast v1, LX/7x9;

    const-class v2, LX/7wJ;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/7wJ;

    const-class p0, LX/7x4;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/7x4;

    iput-object v1, p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->j:LX/7x9;

    iput-object v2, p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->k:LX/7wJ;

    iput-object v3, p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->l:LX/7x4;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276248
    invoke-super {p0, p1}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 1276249
    const-class v0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1276250
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1276251
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->i:Ljava/lang/String;

    .line 1276252
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276253
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4f4f45c0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1276254
    const v1, 0x7f03053e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x13397a36    # -1.9199929E27f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onHiddenChanged(Z)V
    .locals 2

    .prologue
    .line 1276255
    invoke-super {p0, p1}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->onHiddenChanged(Z)V

    .line 1276256
    if-nez p1, :cond_0

    .line 1276257
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->d:LX/0h5;

    new-instance v1, LX/7wl;

    invoke-direct {v1, p0}, LX/7wl;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1276258
    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276259
    invoke-super {p0, p1, p2}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1276260
    const v0, 0x7f0d0ec6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1276261
    const v0, 0x7f0d0ec7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->c:Landroid/view/View;

    .line 1276262
    const-class v0, LX/7wM;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7wM;

    .line 1276263
    if-eqz v0, :cond_0

    .line 1276264
    invoke-interface {v0}, LX/7wM;->a()LX/0h5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->d:LX/0h5;

    .line 1276265
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1276266
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1276267
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->e:LX/1P1;

    .line 1276268
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->e:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1276269
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->k:LX/7wJ;

    .line 1276270
    new-instance p2, LX/7wI;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object p1

    check-cast p1, LX/11T;

    invoke-direct {p2, p0, v1, p1}, LX/7wI;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;Landroid/content/Context;LX/11T;)V

    .line 1276271
    move-object v0, p2

    .line 1276272
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->g:LX/7wI;

    .line 1276273
    new-instance v0, LX/1ON;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->g:LX/7wI;

    invoke-direct {v0, v1}, LX/1ON;-><init>(LX/1OO;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->h:LX/1ON;

    .line 1276274
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->h:LX/1ON;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1276275
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/7wm;

    invoke-direct {v1, p0}, LX/7wm;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1276276
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->l:LX/7x4;

    .line 1276277
    new-instance p2, LX/7x3;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-direct {p2, p0, v1, v2, p1}, LX/7x3;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;Landroid/content/Context;LX/0tX;LX/1Ck;)V

    .line 1276278
    move-object v0, p2

    .line 1276279
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->f:LX/7x3;

    .line 1276280
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->f:LX/7x3;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/7x3;->a(Ljava/lang/String;)V

    .line 1276281
    return-void
.end method
