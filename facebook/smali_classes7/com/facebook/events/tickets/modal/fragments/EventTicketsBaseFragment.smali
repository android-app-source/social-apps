.class public abstract Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1276177
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276178
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1276179
    if-eqz p1, :cond_0

    .line 1276180
    const-string v0, "is_hidden"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a:Z

    .line 1276181
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a:Z

    if-eqz v0, :cond_1

    .line 1276182
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1276183
    :cond_1
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 1276184
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 1276185
    iput-boolean p1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a:Z

    .line 1276186
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1276187
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1276188
    const-string v0, "is_hidden"

    iget-boolean v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1276189
    return-void
.end method
