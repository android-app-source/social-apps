.class public Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private b:Landroid/content/Context;

.field public c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public d:LX/7xy;

.field private e:LX/1P1;

.field public f:LX/7wG;

.field private g:I

.field public h:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:LX/7wQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:LX/1OX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1275887
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1275888
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->i:Z

    .line 1275889
    new-instance v0, LX/7wV;

    invoke-direct {v0, p0}, LX/7wV;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->k:Landroid/view/View$OnClickListener;

    .line 1275890
    new-instance v0, LX/7wW;

    invoke-direct {v0, p0}, LX/7wW;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->l:LX/1OX;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-static {p0}, LX/7wQ;->a(LX/0QB;)LX/7wQ;

    move-result-object p0

    check-cast p0, LX/7wQ;

    iput-object p0, p1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->j:LX/7wQ;

    return-void
.end method

.method public static b(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V
    .locals 1

    .prologue
    .line 1275891
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1275892
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->clearFocus()V

    .line 1275893
    return-void
.end method

.method public static d(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;I)V
    .locals 3

    .prologue
    .line 1275894
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1275895
    if-eqz v0, :cond_0

    .line 1275896
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1275897
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)I
    .locals 3

    .prologue
    .line 1275898
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1275899
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1275900
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1275901
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1275902
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->g:I

    sub-int/2addr v0, v2

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1275903
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    .line 1275904
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    invoke-interface {p0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/events/tickets/modal/model/FieldItem;

    :goto_0
    move-object v0, p0

    .line 1275905
    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;I)V
    .locals 3

    .prologue
    .line 1275906
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, LX/7wo;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;)LX/7wo;

    move-result-object v0

    invoke-interface {v0}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275907
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->j:LX/7wQ;

    new-instance v1, LX/7wS;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-direct {v1, v2}, LX/7wS;-><init>(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1275908
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment$4;

    invoke-direct {v1, p0, p5}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment$4;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;I)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 1275909
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->d:LX/7xy;

    .line 1275910
    iget-object v1, v0, LX/7xy;->a:Ljava/util/List;

    invoke-interface {v1, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ws;

    .line 1275911
    sget-object v2, LX/7xx;->a:[I

    .line 1275912
    iget-object v0, v1, LX/7ws;->c:LX/7wO;

    move-object v1, v0

    .line 1275913
    invoke-virtual {v1}, LX/7wO;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1275914
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 1275915
    if-eqz v0, :cond_0

    .line 1275916
    invoke-static {p0}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->b(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V

    .line 1275917
    :cond_0
    return-void

    .line 1275918
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275919
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1275920
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->b:Landroid/content/Context;

    .line 1275921
    const-class v0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->b:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1275922
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1275923
    const-string v1, "extra_event_ticketing"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275924
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1275925
    const-string v1, "extra_modal_height"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->g:I

    .line 1275926
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275927
    sget-object v2, LX/7xF;->a:[I

    iget-object v3, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1275928
    const/4 v4, 0x0

    .line 1275929
    iget-object v2, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    if-eqz v2, :cond_4

    .line 1275930
    iget-object v2, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    .line 1275931
    :goto_0
    move-object v2, v2

    .line 1275932
    :goto_1
    move-object v1, v2

    .line 1275933
    invoke-interface {v0, v1}, LX/7wo;->a(LX/0Px;)LX/7wo;

    move-result-object v0

    invoke-interface {v0}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275934
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1275935
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v3, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1275936
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    if-ne v5, v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->b()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1275937
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1275938
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1275939
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 1275940
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->h:LX/0Rf;

    .line 1275941
    const-class v0, LX/7wG;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7wG;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->f:LX/7wG;

    .line 1275942
    return-void

    .line 1275943
    :pswitch_0
    const/4 v4, 0x0

    .line 1275944
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1275945
    iget-object v8, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    move v6, v4

    :goto_3
    if-ge v5, v9, :cond_3

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    move v3, v4

    .line 1275946
    :goto_4
    iget v10, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    if-ge v3, v10, :cond_2

    .line 1275947
    iget-object v10, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->w:LX/0Px;

    iget-object v11, v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->x:Ljava/util/Map;

    .line 1275948
    if-nez v10, :cond_5

    .line 1275949
    new-instance v12, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    iget-object v13, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    iget-object p1, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-direct {v12, v13, p1}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275950
    :goto_5
    move-object v10, v12

    .line 1275951
    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275952
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1275953
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v3

    iget-object v10, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    invoke-interface {v3, v10, v6}, LX/7wo;->a(Ljava/lang/String;I)LX/7wo;

    move-result-object v3

    invoke-interface {v3}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v1

    .line 1275954
    iget v2, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    add-int v3, v6, v2

    .line 1275955
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v6, v3

    goto :goto_3

    .line 1275956
    :cond_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 1275957
    goto/16 :goto_1

    .line 1275958
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1275959
    new-instance v3, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    invoke-direct {v3, v4, v4}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1275960
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_0

    .line 1275961
    :cond_5
    iget-object v12, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    add-int v13, v12, v3

    .line 1275962
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v12

    if-ge v13, v12, :cond_6

    iget-object p1, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    invoke-virtual {v10, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    .line 1275963
    iget-object v11, v12, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->a:Ljava/lang/String;

    move-object v12, v11

    .line 1275964
    invoke-virtual {p1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 1275965
    :cond_6
    new-instance v12, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    iget-object v13, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->b:Ljava/lang/String;

    iget-object p1, v2, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-direct {v12, v13, p1}, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1275966
    :cond_7
    invoke-virtual {v10, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 1275967
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1275968
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateAnimation(IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1275969
    :goto_0
    return-object v0

    .line 1275970
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1275971
    new-instance v1, LX/7wX;

    invoke-direct {v1, p0}, LX/7wX;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1d0a3926

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1275972
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->b:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03051c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2447b42a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x23730a7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1275973
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1275974
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->i:Z

    .line 1275975
    const/16 v1, 0x2b

    const v2, -0xf9163af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275976
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1275977
    const v0, 0x7f0d0e89

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1275978
    new-instance v0, LX/7xy;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->k:Landroid/view/View$OnClickListener;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->c:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-direct {v0, v1, p0, p0, v2}, LX/7xy;-><init>(Landroid/view/View$OnClickListener;Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->d:LX/7xy;

    .line 1275979
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->d:LX/7xy;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1275980
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->e:LX/1P1;

    .line 1275981
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->e:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1275982
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->l:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1275983
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1275984
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 1275985
    const/4 v1, 0x0

    .line 1275986
    iput-boolean v1, v0, LX/1Of;->g:Z

    .line 1275987
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1275988
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1275989
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1275990
    check-cast v2, Landroid/view/ViewGroup;

    new-instance v3, LX/7wY;

    invoke-direct {v3, p0, v0}, LX/7wY;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;Landroid/app/Activity;)V

    sget-object v0, LX/73i;->PAYMENTS_WHITE:LX/73i;

    sget-object p1, LX/73h;->BACK_ARROW:LX/73h;

    invoke-virtual {v1, v2, v3, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1275991
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f081f9f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/73i;->PAYMENTS_WHITE:LX/73i;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/String;LX/73i;)V

    .line 1275992
    if-nez p2, :cond_0

    .line 1275993
    iget v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->g:I

    invoke-static {p0, v0}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->d(Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;I)V

    .line 1275994
    :cond_0
    return-void
.end method
