.class public Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;
.super Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->EVENT_TICKET_ORDER_DETAIL_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ScrollView;

.field private d:Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

.field private e:LX/7wz;

.field private f:Ljava/lang/String;

.field public g:LX/7x0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/7x9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1276221
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;
    .locals 3

    .prologue
    .line 1276216
    new-instance v0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    invoke-direct {v0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;-><init>()V

    .line 1276217
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1276218
    const-string v2, "order_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1276219
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1276220
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    const-class v1, LX/7x0;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7x0;

    invoke-static {p0}, LX/7x9;->b(LX/0QB;)LX/7x9;

    move-result-object p0

    check-cast p0, LX/7x9;

    iput-object v1, p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->g:LX/7x0;

    iput-object p0, p1, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->h:LX/7x9;

    return-void
.end method


# virtual methods
.method public final a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V
    .locals 7

    .prologue
    .line 1276222
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->d:Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1276223
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1276224
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->c:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1276225
    if-eqz p7, :cond_0

    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1276226
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->h:LX/7x9;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a:Landroid/view/View;

    invoke-virtual {v0, v1, p7}, LX/7x9;->a(Landroid/view/View;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V

    .line 1276227
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276211
    invoke-super {p0, p1}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->a(Landroid/os/Bundle;)V

    .line 1276212
    const-class v0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1276213
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1276214
    const-string v1, "order_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->f:Ljava/lang/String;

    .line 1276215
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4ec36cbd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1276210
    const v1, 0x7f030524

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x10889f2e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276190
    invoke-super {p0, p1, p2}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1276191
    const v0, 0x7f0d0e99

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a:Landroid/view/View;

    .line 1276192
    const v0, 0x7f0d0e9c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->b:Landroid/view/View;

    .line 1276193
    const v0, 0x7f0d0e9b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->d:Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

    .line 1276194
    const v0, 0x7f0d0e9a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->c:Landroid/widget/ScrollView;

    .line 1276195
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->c:Landroid/widget/ScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1276196
    const-class v0, LX/7wM;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 1276197
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->g:LX/7x0;

    .line 1276198
    new-instance v2, LX/7wz;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object v7

    check-cast v7, LX/7xH;

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, LX/7wz;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;Landroid/content/Context;LX/0tX;LX/1Ck;LX/7xH;)V

    .line 1276199
    move-object v0, v2

    .line 1276200
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->e:LX/7wz;

    .line 1276201
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1276202
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->e:LX/7wz;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->f:Ljava/lang/String;

    .line 1276203
    iget-object v2, v0, LX/7wz;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1475

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1276204
    new-instance v3, LX/7oB;

    invoke-direct {v3}, LX/7oB;-><init>()V

    move-object v3, v3

    .line 1276205
    const-string v4, "order_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1276206
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1276207
    iget-object v3, v0, LX/7wz;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1276208
    iget-object v3, v0, LX/7wz;->d:LX/1Ck;

    const-string v4, "initial_fetch"

    new-instance v5, LX/7wy;

    invoke-direct {v5, v0}, LX/7wy;-><init>(LX/7wz;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1276209
    :cond_0
    return-void
.end method
