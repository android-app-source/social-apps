.class public Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7x6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7wQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7xk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/7xQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

.field private j:Lcom/facebook/events/common/EventAnalyticsParams;

.field private k:Landroid/content/Context;

.field private l:LX/1OM;

.field public m:LX/7wG;

.field private final n:LX/7wi;

.field public o:Z

.field public p:Z

.field public q:Lcom/facebook/widget/CustomLinearLayout;

.field public r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

.field private t:Landroid/view/View;

.field private u:Lcom/facebook/widget/error/GenericErrorView;

.field private final v:Landroid/view/View$OnClickListener;

.field private final w:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1276055
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1276056
    new-instance v0, LX/7wi;

    invoke-direct {v0, p0}, LX/7wi;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->n:LX/7wi;

    .line 1276057
    new-instance v0, LX/7wb;

    invoke-direct {v0, p0}, LX/7wb;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->v:Landroid/view/View$OnClickListener;

    .line 1276058
    new-instance v0, LX/7wc;

    invoke-direct {v0, p0}, LX/7wc;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->w:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1276059
    return-void
.end method

.method public static a(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Z)V
    .locals 9

    .prologue
    .line 1276155
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->f:LX/7xQ;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    sget-object v2, LX/7xO;->GENERATE_SUBTOTAL:LX/7xO;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/7xQ;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Ljava/util/List;)LX/7xP;

    move-result-object v8

    .line 1276156
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iget-boolean v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->p:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v3, v3, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->m:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-boolean v5, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->p:Z

    if-eqz v5, :cond_1

    iget-object v5, v8, LX/7xP;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v5}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v6, 0x1

    :goto_1
    iget-object v7, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->v:Landroid/view/View$OnClickListener;

    move v5, p1

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;ZZLandroid/view/View$OnClickListener;LX/7xP;)V

    .line 1276157
    return-void

    .line 1276158
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->d:LX/7xH;

    iget-object v2, v8, LX/7xP;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, LX/7xH;->b(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 14

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    invoke-static {v6}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    new-instance v7, LX/7x6;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v6}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v10

    check-cast v10, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v7 .. v13}, LX/7x6;-><init>(Landroid/content/res/Resources;LX/0tX;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Ljava/util/concurrent/Executor;LX/03V;LX/0ad;)V

    move-object v2, v7

    check-cast v2, LX/7x6;

    invoke-static {v6}, LX/7wQ;->a(LX/0QB;)LX/7wQ;

    move-result-object v3

    check-cast v3, LX/7wQ;

    invoke-static {v6}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object v4

    check-cast v4, LX/7xH;

    const-class v5, LX/7xk;

    invoke-interface {v6, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/7xk;

    invoke-static {v6}, LX/7xQ;->b(LX/0QB;)LX/7xQ;

    move-result-object v6

    check-cast v6, LX/7xQ;

    iput-object v1, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a:LX/1nQ;

    iput-object v2, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->b:LX/7x6;

    iput-object v3, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->c:LX/7wQ;

    iput-object v4, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->d:LX/7xH;

    iput-object v5, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->e:LX/7xk;

    iput-object v6, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->f:LX/7xQ;

    return-void
.end method

.method public static b(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;Lcom/facebook/payments/auth/pin/model/PaymentPin;)Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;
    .locals 17

    .prologue
    .line 1276144
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v12

    .line 1276145
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;

    .line 1276146
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v13

    .line 1276147
    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    const/4 v2, 0x1

    move v10, v2

    .line 1276148
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;

    move-object v11, v2

    .line 1276149
    :goto_1
    invoke-virtual {v9}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->b()LX/1vs;

    move-result-object v2

    iget-object v14, v2, LX/1vs;->a:LX/15i;

    iget v15, v2, LX/1vs;->b:I

    .line 1276150
    new-instance v2, LX/7wq;

    invoke-direct {v2}, LX/7wq;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    invoke-virtual {v2, v3}, LX/7wq;->a(Lcom/facebook/events/logging/BuyTicketsLoggingInfo;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->p()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/7wq;->b(Z)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->m(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->eR_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->n(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->p(Ljava/lang/String;)LX/7wq;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->d:LX/7xH;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->eQ_()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->j()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->b()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, LX/7xH;->a(ZJJLcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/7wq;->o(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->f(Ljava/lang/String;)LX/7wq;

    move-result-object v3

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, LX/7wq;->a(Landroid/net/Uri;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->q()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->b(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->c(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->d(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->g(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->h(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->i(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->j(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->g(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual {v2, v12}, LX/7wq;->b(LX/0Px;)LX/7wq;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/7wq;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;)LX/7wq;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v3, v2}, LX/7wq;->l(Ljava/lang/String;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;->a()LX/2uF;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->a(LX/2uF;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;->a()LX/2uF;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->b(LX/2uF;)LX/7wq;

    move-result-object v2

    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->p()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->f(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->t()I

    move-result v3

    invoke-virtual {v2, v3}, LX/7wq;->a(I)LX/7wq;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v14, v15, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->e(Ljava/lang/String;)LX/7wq;

    move-result-object v3

    if-eqz v10, :cond_4

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v3, v2}, LX/7wq;->b(I)LX/7wq;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7wq;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7wq;

    move-result-object v2

    sget-object v3, LX/7wp;->SELECT:LX/7wp;

    invoke-virtual {v2, v3}, LX/7wq;->b(LX/7wp;)LX/7wq;

    move-result-object v3

    if-nez v11, :cond_5

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    :goto_5
    invoke-virtual {v3, v2}, LX/7wq;->f(LX/0Px;)LX/7wq;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/7wq;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)LX/7wq;

    move-result-object v2

    .line 1276151
    invoke-virtual {v2}, LX/7wq;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v2

    return-object v2

    .line 1276152
    :cond_0
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_0

    .line 1276153
    :cond_1
    const/4 v2, 0x0

    move-object v11, v2

    goto/16 :goto_1

    .line 1276154
    :cond_2
    invoke-virtual {v13}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_2

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_4
    invoke-virtual {v9}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->c()I

    move-result v2

    goto :goto_4

    :cond_5
    invoke-virtual {v11}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    goto :goto_5
.end method

.method public static k(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)Z
    .locals 1

    .prologue
    .line 1276143
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->E:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 5

    .prologue
    .line 1276130
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-boolean v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    if-eqz v0, :cond_1

    new-instance v0, LX/7xV;

    .line 1276131
    new-instance v1, LX/7we;

    invoke-direct {v1, p0}, LX/7we;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    move-object v1, v1

    .line 1276132
    invoke-direct {v0, v1}, LX/7xV;-><init>(LX/7we;)V

    :goto_0
    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->l:LX/1OM;

    .line 1276133
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->l:LX/1OM;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1276134
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->ERROR:LX/7wp;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1276135
    if-eqz v0, :cond_0

    .line 1276136
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->l:LX/1OM;

    check-cast v0, LX/7xU;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-interface {v0, v1}, LX/7xU;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1276137
    :cond_0
    return-void

    .line 1276138
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->e:LX/7xk;

    .line 1276139
    new-instance v1, LX/7wd;

    invoke-direct {v1, p0}, LX/7wd;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    move-object v1, v1

    .line 1276140
    new-instance v4, LX/7xj;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v2

    check-cast v2, LX/0iA;

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v3

    check-cast v3, LX/3kp;

    invoke-direct {v4, v1, v2, v3}, LX/7xj;-><init>(LX/7wd;LX/0iA;LX/3kp;)V

    .line 1276141
    move-object v0, v4

    .line 1276142
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static r(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1276113
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_0

    .line 1276114
    :goto_0
    return-void

    .line 1276115
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    if-ne v0, v1, :cond_1

    .line 1276116
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1276117
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->setVisibility(I)V

    .line 1276118
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1276119
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    goto :goto_0

    .line 1276120
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->ERROR:LX/7wp;

    if-ne v0, v1, :cond_2

    .line 1276121
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1276122
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->setVisibility(I)V

    .line 1276123
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1276124
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 1276125
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    goto :goto_0

    .line 1276126
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1276127
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-virtual {v0, v3}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->setVisibility(I)V

    .line 1276128
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1276129
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276159
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1276160
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->k:Landroid/content/Context;

    .line 1276161
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->k:Landroid/content/Context;

    invoke-static {p0, v1}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1276162
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1276163
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->h:Ljava/lang/String;

    .line 1276164
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1276165
    const-string v1, "ticketing_flow_logging_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276166
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1276167
    const-string v1, "extras_event_analytics_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1276168
    const-class v0, LX/7wG;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7wG;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->m:LX/7wG;

    .line 1276169
    if-nez p1, :cond_0

    .line 1276170
    new-instance v0, LX/7wq;

    invoke-direct {v0}, LX/7wq;-><init>()V

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    .line 1276171
    iput-object v1, v0, LX/7wq;->N:LX/7wp;

    .line 1276172
    move-object v0, v0

    .line 1276173
    invoke-virtual {v0}, LX/7wq;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1276174
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->c:LX/7wQ;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->n:LX/7wi;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1276175
    return-void

    .line 1276176
    :cond_0
    const-string v0, "state_event_ticketing_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7002fa4b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1276112
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->k:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03051f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0xc6c3a77

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5ef39c3d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1276109
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->c:LX/7wQ;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->n:LX/7wi;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1276110
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1276111
    const/16 v1, 0x2b

    const v2, 0x361d6002

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 1

    .prologue
    .line 1276105
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onHiddenChanged(Z)V

    .line 1276106
    if-nez p1, :cond_0

    .line 1276107
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1276108
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b9d4078

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1276102
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1276103
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->o:Z

    .line 1276104
    const/16 v1, 0x2b

    const v2, -0x162b6421

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1276099
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1276100
    const-string v0, "state_event_ticketing_model"

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1276101
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276060
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1276061
    const v0, 0x7f0d0e8c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 1276062
    const v0, 0x7f0d0e8d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1276063
    const v0, 0x7f0d0e8e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->s:Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    .line 1276064
    const v0, 0x7f0d0e8f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->t:Landroid/view/View;

    .line 1276065
    const v0, 0x7f0d0e90

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    .line 1276066
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->u:Lcom/facebook/widget/error/GenericErrorView;

    .line 1276067
    new-instance v1, LX/7wf;

    invoke-direct {v1, p0}, LX/7wf;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    move-object v1, v1

    .line 1276068
    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1276069
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62V;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/62V;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1276070
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1276071
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 1276072
    const/4 v1, 0x0

    .line 1276073
    iput-boolean v1, v0, LX/1Of;->g:Z

    .line 1276074
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1276075
    new-instance v1, LX/62X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a0117

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b146f

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-direct {v1, v2, p1}, LX/62X;-><init>(II)V

    .line 1276076
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b1474

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1276077
    iput v2, v1, LX/62X;->b:I

    .line 1276078
    iput v2, v1, LX/62X;->c:I

    .line 1276079
    move-object v1, v1

    .line 1276080
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1276081
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1276082
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1276083
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1276084
    check-cast v2, Landroid/view/ViewGroup;

    new-instance p1, LX/7wg;

    invoke-direct {p1, p0, v0}, LX/7wg;-><init>(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Landroid/app/Activity;)V

    sget-object v0, LX/73i;->PAYMENTS_WHITE:LX/73i;

    sget-object p2, LX/73h;->CROSS:LX/73h;

    invoke-virtual {v1, v2, p1, v0, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1276085
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f081f9e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/73i;->PAYMENTS_WHITE:LX/73i;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/String;LX/73i;)V

    .line 1276086
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    if-ne v0, v1, :cond_0

    .line 1276087
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->b:LX/7x6;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->h:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b1475

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, p0}, LX/7x6;->a(Ljava/lang/String;ILcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276088
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->ERROR:LX/7wp;

    if-ne v0, v1, :cond_1

    .line 1276089
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v0

    sget-object v1, LX/7wp;->FETCH:LX/7wp;

    invoke-interface {v0, v1}, LX/7wo;->a(LX/7wp;)LX/7wo;

    move-result-object v0

    invoke-interface {v0}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1276090
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->b:LX/7x6;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->h:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b1475

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, p0}, LX/7x6;->a(Ljava/lang/String;ILcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276091
    :cond_1
    invoke-static {p0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->r(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276092
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->N:LX/7wp;

    sget-object v1, LX/7wp;->SELECT:LX/7wp;

    if-ne v0, v1, :cond_2

    .line 1276093
    invoke-static {p0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->o(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)V

    .line 1276094
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-boolean v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->p:Z

    if-eqz v0, :cond_2

    .line 1276095
    invoke-static {p0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->k(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;)Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Z)V

    .line 1276096
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1276097
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->w:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1276098
    return-void
.end method
