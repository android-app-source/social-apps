.class public Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;
.super Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;
.source ""


# instance fields
.field private b:Lcom/facebook/widget/CustomLinearLayout;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/widget/text/BetterButton;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/view/ViewGroup;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

.field public j:LX/7xP;

.field public k:Z

.field private l:I

.field private m:I

.field private final n:Landroid/view/View$OnClickListener;

.field private final o:Landroid/view/View$OnClickListener;

.field public p:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1277626
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1277627
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->k:Z

    .line 1277628
    new-instance v0, LX/7xZ;

    invoke-direct {v0, p0}, LX/7xZ;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->n:Landroid/view/View$OnClickListener;

    .line 1277629
    new-instance v0, LX/7xa;

    invoke-direct {v0, p0}, LX/7xa;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->o:Landroid/view/View$OnClickListener;

    .line 1277630
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a()V

    .line 1277631
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277620
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1277621
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->k:Z

    .line 1277622
    new-instance v0, LX/7xZ;

    invoke-direct {v0, p0}, LX/7xZ;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->n:Landroid/view/View$OnClickListener;

    .line 1277623
    new-instance v0, LX/7xa;

    invoke-direct {v0, p0}, LX/7xa;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->o:Landroid/view/View$OnClickListener;

    .line 1277624
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a()V

    .line 1277625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277614
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1277615
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->k:Z

    .line 1277616
    new-instance v0, LX/7xZ;

    invoke-direct {v0, p0}, LX/7xZ;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->n:Landroid/view/View$OnClickListener;

    .line 1277617
    new-instance v0, LX/7xa;

    invoke-direct {v0, p0}, LX/7xa;-><init>(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->o:Landroid/view/View$OnClickListener;

    .line 1277618
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a()V

    .line 1277619
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .prologue
    .line 1277602
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1277603
    const v0, 0x7f030514

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1277604
    const v0, 0x7f0d0e73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b:Lcom/facebook/widget/CustomLinearLayout;

    .line 1277605
    const v0, 0x7f0d0e74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1277606
    const v0, 0x7f0d0e75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1277607
    const v0, 0x7f0d0e76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->e:Lcom/facebook/widget/text/BetterButton;

    .line 1277608
    const v0, 0x7f0d0e77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1277609
    const v0, 0x7f0d0e72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    .line 1277610
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->m:I

    .line 1277611
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->l:I

    .line 1277612
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->setOrientation(I)V

    .line 1277613
    return-void
.end method

.method private static a(Landroid/view/View;LX/73b;)V
    .locals 2

    .prologue
    .line 1277632
    const v0, 0x7f0d0e78

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p1, LX/73b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277633
    const v0, 0x7f0d0e79

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p1, LX/73b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277634
    return-void
.end method

.method private static a(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;LX/1nQ;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1277601
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->p:LX/1nQ;

    iput-object p2, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;

    invoke-static {v1}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v0

    check-cast v0, LX/1nQ;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;LX/1nQ;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;LX/7xP;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1277585
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->j:LX/7xP;

    .line 1277586
    iget-object v0, p1, LX/7xP;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 1277587
    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->k:Z

    if-nez v0, :cond_4

    .line 1277588
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1277589
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277590
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_2

    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->m:I

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1277591
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/7xu;

    iput-boolean v2, v0, LX/7xu;->a:Z

    .line 1277592
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277593
    :goto_3
    return-void

    :cond_1
    move v1, v3

    .line 1277594
    goto :goto_0

    .line 1277595
    :cond_2
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->l:I

    goto :goto_1

    .line 1277596
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->o:Landroid/view/View$OnClickListener;

    goto :goto_2

    .line 1277597
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/7xu;

    iput-boolean v3, v0, LX/7xu;->a:Z

    .line 1277598
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277599
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1277600
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b()V

    goto :goto_3
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1277574
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v1, v2

    .line 1277575
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->j:LX/7xP;

    iget-object v0, v0, LX/7xP;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1277576
    if-lt v1, v3, :cond_0

    .line 1277577
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f030515

    iget-object v5, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1277578
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1277579
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->j:LX/7xP;

    iget-object v0, v0, LX/7xP;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73b;

    invoke-static {v4, v0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a(Landroid/view/View;LX/73b;)V

    .line 1277580
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1277581
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1277582
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1277583
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1277584
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;ZZLandroid/view/View$OnClickListener;LX/7xP;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277551
    iput-object p2, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->h:Ljava/lang/String;

    .line 1277552
    iput-object p4, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->i:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1277553
    if-nez p2, :cond_1

    .line 1277554
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277555
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277556
    :goto_0
    if-eqz p3, :cond_2

    .line 1277557
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277558
    :goto_1
    if-eqz p1, :cond_0

    .line 1277559
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277560
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->e:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p7}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277561
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->e:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p5}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 1277562
    if-eqz p6, :cond_3

    .line 1277563
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1277564
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277565
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1277566
    :goto_2
    return-void

    .line 1277567
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277568
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f98

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277569
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setAllCaps(Z)V

    .line 1277570
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1277571
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081f97

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1

    .line 1277572
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->b:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1277573
    invoke-static {p0, p8}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;->a$redex0(Lcom/facebook/events/tickets/modal/views/EventSelectTicketsFooterView;LX/7xP;)V

    goto :goto_2
.end method
