.class public Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphButton;

.field private b:Lcom/facebook/fbui/glyph/GlyphButton;

.field private c:Landroid/widget/TextView;

.field private d:I

.field public e:I

.field private f:I

.field private g:I

.field private h:Ljava/text/NumberFormat;

.field public i:LX/7xW;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1278286
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1278287
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    .line 1278288
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    .line 1278289
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    .line 1278290
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278291
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278292
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278279
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278280
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    .line 1278281
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    .line 1278282
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    .line 1278283
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278284
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278272
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1278273
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    .line 1278274
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    .line 1278275
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    .line 1278276
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278277
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278278
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1278258
    const v0, 0x7f0304a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1278259
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->h:Ljava/text/NumberFormat;

    .line 1278260
    const v0, 0x7f0d0d9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1278261
    const v0, 0x7f0d0d98

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1278262
    const v0, 0x7f0d0d99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->c:Landroid/widget/TextView;

    .line 1278263
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/7xv;

    invoke-direct {v1, p0}, LX/7xv;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1278264
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/7xw;

    invoke-direct {v1, p0}, LX/7xw;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1278265
    sget-object v0, LX/03r;->EventTicketingQuantityPicker:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1278266
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1278267
    if-eqz v1, :cond_0

    .line 1278268
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1278269
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1278270
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1278271
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;)V
    .locals 2

    .prologue
    .line 1278254
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    if-ge v0, v1, :cond_0

    .line 1278255
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->c()V

    .line 1278256
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f()V

    .line 1278257
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;)V
    .locals 2

    .prologue
    .line 1278250
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    if-le v0, v1, :cond_0

    .line 1278251
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d()V

    .line 1278252
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f()V

    .line 1278253
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1278215
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    if-ge v0, v1, :cond_0

    .line 1278216
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    .line 1278217
    :goto_0
    return-void

    .line 1278218
    :cond_0
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1278246
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    if-le v0, v1, :cond_0

    .line 1278247
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    .line 1278248
    :goto_0
    return-void

    .line 1278249
    :cond_0
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1278240
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->h:Ljava/text/NumberFormat;

    iget v4, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278241
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v4, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    if-le v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1278242
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    iget v4, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1278243
    return-void

    :cond_0
    move v0, v2

    .line 1278244
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1278245
    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1278237
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->i:LX/7xW;

    if-eqz v0, :cond_0

    .line 1278238
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->i:LX/7xW;

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    invoke-interface {v0, v1}, LX/7xW;->a(I)V

    .line 1278239
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentQuantity()I
    .locals 1

    .prologue
    .line 1278236
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    return v0
.end method

.method public setCurrentQuantity(I)V
    .locals 0

    .prologue
    .line 1278233
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278234
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e()V

    .line 1278235
    return-void
.end method

.method public setListener(LX/7xW;)V
    .locals 0

    .prologue
    .line 1278231
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->i:LX/7xW;

    .line 1278232
    return-void
.end method

.method public setMaximumQuantity(I)V
    .locals 1

    .prologue
    .line 1278226
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->f:I

    .line 1278227
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    if-le v0, p1, :cond_0

    .line 1278228
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278229
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e()V

    .line 1278230
    :cond_0
    return-void
.end method

.method public setMinimumQuantity(I)V
    .locals 1

    .prologue
    .line 1278221
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->d:I

    .line 1278222
    iget v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    if-ge v0, p1, :cond_0

    .line 1278223
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->g:I

    .line 1278224
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e()V

    .line 1278225
    :cond_0
    return-void
.end method

.method public setMinimumSelectedQuantity(I)V
    .locals 0

    .prologue
    .line 1278219
    iput p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->e:I

    .line 1278220
    return-void
.end method
