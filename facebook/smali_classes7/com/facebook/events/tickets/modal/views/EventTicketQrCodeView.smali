.class public Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

.field public c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:Lcom/facebook/fbui/glyph/GlyphView;

.field private h:I

.field private i:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/25T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/7xt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1278109
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

    const-string v1, "event_ticketing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1278106
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1278107
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a()V

    .line 1278108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1278103
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278104
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a()V

    .line 1278105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1278100
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1278101
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a()V

    .line 1278102
    return-void
.end method

.method private static a(LX/7og;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7og;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketOrder$EventTickets$Nodes$Fbqrcode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1278091
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1278092
    invoke-interface {p0}, LX/7of;->e()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;

    move-result-object v0

    .line 1278093
    if-eqz v0, :cond_1

    .line 1278094
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;

    .line 1278095
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    move-result-object v0

    .line 1278096
    invoke-static {v0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1278097
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1278098
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1278099
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1278075
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1278076
    const v0, 0x7f03052b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1278077
    const v0, 0x7f0d0eae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->b:Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    .line 1278078
    const v0, 0x7f0d0eaf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1278079
    const v0, 0x7f0d0eb0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    .line 1278080
    const v0, 0x7f0d0ead

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1278081
    const v0, 0x7f0d0eb1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1278082
    const v0, 0x7f0d0eb2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1278083
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 1278084
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setVisibility(I)V

    .line 1278085
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1278086
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1278087
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1278088
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->j:LX/25T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1278089
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->h:I

    .line 1278090
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 1278065
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const/16 v1, 0xa

    .line 1278066
    iput v1, v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 1278067
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const/high16 v1, 0x40a00000    # 5.0f

    .line 1278068
    iput v1, v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    .line 1278069
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCount(I)V

    .line 1278070
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCurrentItem(I)V

    .line 1278071
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/7xq;

    invoke-direct {v1, p0, p2}, LX/7xq;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;I)V

    .line 1278072
    iput-object v1, v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 1278073
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->d:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setVisibility(I)V

    .line 1278074
    return-void
.end method

.method private a(LX/7og;LX/0Px;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7og;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketOrder$EventTickets$Nodes$Fbqrcode;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1278041
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->k:LX/7xt;

    .line 1278042
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278043
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278044
    invoke-interface {p1}, LX/7of;->e()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278045
    invoke-interface {p1}, LX/7of;->e()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278046
    invoke-interface {p1}, LX/7of;->e()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/7xt;->a:LX/0Px;

    .line 1278047
    iput-object p2, v0, LX/7xt;->b:Ljava/util/List;

    .line 1278048
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1278049
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->k:LX/7xt;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1278050
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->j:LX/25T;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1278051
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->b:Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    invoke-virtual {v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->d()V

    .line 1278052
    new-instance v0, LX/7x7;

    const/16 v1, 0x14

    const/high16 v2, 0x66000000

    invoke-direct {v0, v1, v5, v2}, LX/7x7;-><init>(III)V

    .line 1278053
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    .line 1278054
    iput-object v0, v1, LX/1bX;->j:LX/33B;

    .line 1278055
    move-object v0, v1

    .line 1278056
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1278057
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->i:LX/1Ad;

    sget-object v3, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1278058
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1278059
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    .line 1278060
    if-le v0, v5, :cond_0

    .line 1278061
    invoke-direct {p0, v4, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->b(II)V

    .line 1278062
    invoke-direct {p0, v4, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(II)V

    .line 1278063
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    .line 1278064
    return-void
.end method

.method private static a(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;LX/1Ad;LX/25T;LX/7xt;)V
    .locals 0

    .prologue
    .line 1278110
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->i:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->j:LX/25T;

    iput-object p3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->k:LX/7xt;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/25T;->a(LX/0QB;)LX/25T;

    move-result-object v1

    check-cast v1, LX/25T;

    new-instance p1, LX/7xt;

    const-class v3, Landroid/content/Context;

    invoke-interface {v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v2}, LX/19B;->a(LX/0QB;)LX/19B;

    move-result-object v4

    check-cast v4, LX/19B;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-direct {p1, v3, v4, v5}, LX/7xt;-><init>(Landroid/content/Context;LX/19B;LX/1Ad;)V

    move-object v2, p1

    check-cast v2, LX/7xt;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;LX/1Ad;LX/25T;LX/7xt;)V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;)Z
    .locals 1

    .prologue
    .line 1278040
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;->b()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 1278036
    invoke-static {p0, p1, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;II)V

    .line 1278037
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/7xr;

    invoke-direct {v1, p0}, LX/7xr;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1278038
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->g:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/7xs;

    invoke-direct {v1, p0}, LX/7xs;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1278039
    return-void
.end method

.method public static c(Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;II)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1278031
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1278032
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->g:Lcom/facebook/fbui/glyph/GlyphView;

    add-int/lit8 v3, p2, -0x1

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1278033
    return-void

    :cond_0
    move v0, v2

    .line 1278034
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1278035
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1278026
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278027
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->b:Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278028
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(LX/7og;)LX/0Px;

    move-result-object v0

    .line 1278029
    invoke-direct {p0, p1, v0, p5}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(LX/7og;LX/0Px;Ljava/lang/String;)V

    .line 1278030
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1278017
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1278018
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->k:LX/7xt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-nez v0, :cond_1

    .line 1278019
    :cond_0
    :goto_0
    return-void

    .line 1278020
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->h:I

    if-eq v0, v1, :cond_0

    .line 1278021
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1278022
    iget v1, v0, LX/25R;->i:I

    move v0, v1

    .line 1278023
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->k:LX/7xt;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1278024
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->c:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setCurrentPosition(I)V

    .line 1278025
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->h:I

    goto :goto_0
.end method
