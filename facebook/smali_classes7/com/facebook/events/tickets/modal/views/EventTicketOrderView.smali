.class public Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

.field private c:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1277997
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1277998
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a()V

    .line 1277999
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277994
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1277995
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a()V

    .line 1277996
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277991
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1277992
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a()V

    .line 1277993
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1278000
    const v0, 0x7f030529

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1278001
    const v0, 0x7f0d0eaa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1278002
    const v0, 0x7f0d0eac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    .line 1278003
    const v0, 0x7f0d0eab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    .line 1278004
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1278005
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->setOrientation(I)V

    .line 1278006
    return-void
.end method

.method private a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1277986
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1277987
    :goto_0
    return-void

    .line 1277988
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    const v1, 0x7f03052d

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1277989
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1277990
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeView;->a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1277981
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1277982
    :goto_0
    return-void

    .line 1277983
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    const v1, 0x7f0304a4

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1277984
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    .line 1277985
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(LX/7og;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1277973
    invoke-interface {p0}, LX/7of;->e()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;

    move-result-object v0

    .line 1277974
    if-nez v0, :cond_0

    move v0, v1

    .line 1277975
    :goto_0
    return v0

    .line 1277976
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;

    .line 1277977
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1277978
    const/4 v0, 0x1

    goto :goto_0

    .line 1277979
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1277980
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1277964
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1277965
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a(LX/7og;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277966
    invoke-direct/range {p0 .. p5}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a(LX/7og;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1277967
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a(LX/7og;)V

    .line 1277968
    return-void

    .line 1277969
    :cond_0
    invoke-static {p6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1277970
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277971
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1277972
    :cond_1
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
