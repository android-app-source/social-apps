.class public Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;
.super LX/62U;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field public final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public final q:Lcom/facebook/resources/ui/FbTextView;

.field public final r:Lcom/facebook/resources/ui/FbRadioButton;

.field public final s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final t:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

.field public u:LX/7xg;

.field public v:I

.field public final w:Landroid/view/View$OnClickListener;

.field public x:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1277849
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    const-string v1, "event_ticketing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1277850
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277851
    new-instance v0, LX/7xl;

    invoke-direct {v0, p0}, LX/7xl;-><init>(Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->w:Landroid/view/View$OnClickListener;

    .line 1277852
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1277853
    const v0, 0x7f0d0e7b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1277854
    const v0, 0x7f0d0e7c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1277855
    const v0, 0x7f0d0e7d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1277856
    const v0, 0x7f0d0e80

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1277857
    const v0, 0x7f0d0e7a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->p:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1277858
    const v0, 0x7f0d0e7f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->t:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 1277859
    const v0, 0x7f0d0e7e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1277860
    const v1, 0x7f03049e

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1277861
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    .line 1277862
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setClickable(Z)V

    .line 1277863
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    invoke-static {p0}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object p0

    check-cast p0, LX/7xH;

    iput-object p0, p1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->x:LX/7xH;

    return-void
.end method
