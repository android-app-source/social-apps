.class public Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/5O6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1277548
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1277549
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a()V

    .line 1277550
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277545
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1277546
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a()V

    .line 1277547
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1277542
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1277543
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a()V

    .line 1277544
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)LX/7xu;
    .locals 2

    .prologue
    .line 1277541
    new-instance v0, LX/7xu;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/7xu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/7xu;
    .locals 1

    .prologue
    .line 1277540
    new-instance v0, LX/7xu;

    invoke-direct {v0, p0}, LX/7xu;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1277536
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->setOrientation(I)V

    .line 1277537
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->setWillNotDraw(Z)V

    .line 1277538
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a:LX/5O6;

    .line 1277539
    return-void
.end method

.method private static b()LX/7xu;
    .locals 3

    .prologue
    .line 1277535
    new-instance v0, LX/7xu;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/7xu;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1277534
    instance-of v0, p1, LX/7xu;

    return v0
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    .prologue
    .line 1277521
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 1277522
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/7xu;

    .line 1277523
    invoke-static {v0}, LX/7xu;->a(LX/7xu;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1277524
    :goto_0
    return v0

    .line 1277525
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    .line 1277526
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 1277527
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v0, LX/7xu;->c:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v6, v0, LX/7xu;->c:I

    sub-int/2addr v5, v6

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v3, v4, v5, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 1277528
    iget-boolean v2, v0, LX/7xu;->a:Z

    if-eqz v2, :cond_1

    .line 1277529
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a:LX/5O6;

    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v3

    invoke-virtual {v2, p1, v3}, LX/5O6;->a(Landroid/graphics/Canvas;F)V

    .line 1277530
    :cond_1
    iget-boolean v0, v0, LX/7xu;->b:Z

    if-eqz v0, :cond_2

    .line 1277531
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a:LX/5O6;

    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {v0, p1, v2}, LX/5O6;->a(Landroid/graphics/Canvas;F)V

    .line 1277532
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    move v0, v1

    .line 1277533
    goto :goto_0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1277515
    invoke-static {}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->b()LX/7xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1277520
    invoke-static {}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->b()LX/7xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1277519
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a(Landroid/util/AttributeSet;)LX/7xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1277518
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/7xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1277517
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a(Landroid/util/AttributeSet;)LX/7xu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1277516
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/7xu;

    move-result-object v0

    return-object v0
.end method
