.class public Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;
.super Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;
.source ""


# instance fields
.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/widget/CustomLinearLayout;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/fig/button/FigButton;

.field private i:LX/11T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/7xH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1277954
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1277955
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a()V

    .line 1277956
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1277951
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1277952
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a()V

    .line 1277953
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1277948
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/tickets/modal/views/EventTicketingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1277949
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a()V

    .line 1277950
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1277938
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1277939
    const v0, 0x7f030526

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1277940
    const v0, 0x7f0d0ea0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 1277941
    const v0, 0x7f0d0ea1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1277942
    const v0, 0x7f0d0ea2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1277943
    const v0, 0x7f0d0ea3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1277944
    const v0, 0x7f0d0ea4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1277945
    const v0, 0x7f0d0ea5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1277946
    const v0, 0x7f0d0ea6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h:Lcom/facebook/fig/button/FigButton;

    .line 1277947
    return-void
.end method

.method private static a(Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;LX/11T;LX/1nQ;LX/7xH;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1277937
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->i:LX/11T;

    iput-object p2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->j:LX/1nQ;

    iput-object p3, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->k:LX/7xH;

    iput-object p4, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->l:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    invoke-static {v3}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v0

    check-cast v0, LX/11T;

    invoke-static {v3}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {v3}, LX/7xH;->a(LX/0QB;)LX/7xH;

    move-result-object v2

    check-cast v2, LX/7xH;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a(Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;LX/11T;LX/1nQ;LX/7xH;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1277957
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030525

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->c:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1277958
    const v0, 0x7f0d0e9e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1277959
    const v1, 0x7f0d0e9f

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1277960
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277961
    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277962
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1277963
    return-void
.end method

.method private b(LX/7og;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1277930
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->d(LX/7og;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277931
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277932
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277933
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p1}, LX/7of;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277934
    :goto_0
    return-void

    .line 1277935
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277936
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private c(LX/7og;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1277923
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->e(LX/7og;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277924
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277925
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277926
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p1}, LX/7of;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277927
    :goto_0
    return-void

    .line 1277928
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277929
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static d(LX/7og;)Z
    .locals 1

    .prologue
    .line 1277922
    invoke-interface {p0}, LX/7of;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(LX/7og;)Z
    .locals 1

    .prologue
    .line 1277921
    invoke-interface {p0}, LX/7of;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(LX/7og;)V
    .locals 4

    .prologue
    .line 1277915
    invoke-interface {p1}, LX/7og;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1277916
    invoke-interface {p1}, LX/7og;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h:Lcom/facebook/fig/button/FigButton;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1277917
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/7xp;

    invoke-direct {v1, p0, p1}, LX/7xp;-><init>(Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;LX/7og;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277918
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1277919
    :goto_0
    return-void

    .line 1277920
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private g(LX/7og;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1277906
    invoke-interface {p1}, LX/7og;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1277907
    invoke-interface {p1}, LX/7og;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1277908
    invoke-virtual {v1, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v9}, LX/15i;->j(II)I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v11}, LX/47f;->a(Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v0

    .line 1277909
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->i:LX/11T;

    invoke-virtual {v1}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-interface {p1}, LX/7of;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1277910
    invoke-interface {p1}, LX/7og;->eX_()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1277911
    invoke-interface {p1}, LX/7og;->eX_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1277912
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081fbf

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v9

    invoke-virtual {v3, v2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v10

    invoke-virtual {v5, v4, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v11

    const/4 v0, 0x3

    aput-object v1, v8, v0

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1277913
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277914
    return-void
.end method

.method private h(LX/7og;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1277897
    invoke-interface {p1}, LX/7og;->k()LX/0Px;

    move-result-object v3

    .line 1277898
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;

    .line 1277899
    iget-object v5, p0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->k:LX/7xH;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;->d()I

    move-result v7

    invoke-virtual {v5, v6, v7}, LX/7xH;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 1277900
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1277901
    :goto_1
    invoke-direct {p0, v5, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277902
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1277903
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$TicketOrderItemsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1277904
    invoke-virtual {v6, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1277905
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/7og;)V
    .locals 0

    .prologue
    .line 1277890
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1277891
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->h(LX/7og;)V

    .line 1277892
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->b(LX/7og;)V

    .line 1277893
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->c(LX/7og;)V

    .line 1277894
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->f(LX/7og;)V

    .line 1277895
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->g(LX/7og;)V

    .line 1277896
    return-void
.end method
