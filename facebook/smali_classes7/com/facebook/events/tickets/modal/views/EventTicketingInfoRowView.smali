.class public Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field private l:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1278140
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1278141
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->e()V

    .line 1278142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278143
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1278144
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->e()V

    .line 1278145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278146
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1278147
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->e()V

    .line 1278148
    return-void
.end method

.method private static a(Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278149
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1278150
    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278151
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1278152
    :goto_0
    return-void

    .line 1278153
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1278154
    const v0, 0x7f0304a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1278155
    const v0, 0x7f0d0d9d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 1278156
    const v0, 0x7f0d0d9e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 1278157
    const v0, 0x7f0d0d9f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1278158
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1475

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1278159
    invoke-virtual {p0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1278160
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1278161
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setBackgroundResource(I)V

    .line 1278162
    return-void
.end method

.method private setTextAppearance(Lcom/facebook/resources/ui/FbTextView;)V
    .locals 2

    .prologue
    .line 1278163
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 1278164
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e084c

    invoke-virtual {p1, v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1278165
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 1278166
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1278167
    return-void

    .line 1278168
    :cond_0
    const v0, 0x7f0e084c

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 4

    .prologue
    .line 1278169
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278170
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278171
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278172
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, p2}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;)V

    .line 1278173
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081fa3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Lcom/facebook/resources/ui/FbTextView;Ljava/lang/String;)V

    .line 1278174
    if-eqz p4, :cond_0

    .line 1278175
    invoke-virtual {p0, p4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1278176
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setPadding(IIII)V

    .line 1278177
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1278178
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b147c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1278179
    invoke-virtual {p0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1278180
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setTextAppearance(Lcom/facebook/resources/ui/FbTextView;)V

    .line 1278181
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setTextAppearance(Lcom/facebook/resources/ui/FbTextView;)V

    .line 1278182
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setTextAppearance(Lcom/facebook/resources/ui/FbTextView;)V

    .line 1278183
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->setBackgroundResource(I)V

    .line 1278184
    return-void
.end method
