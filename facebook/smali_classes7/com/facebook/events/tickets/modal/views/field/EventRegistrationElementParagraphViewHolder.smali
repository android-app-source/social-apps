.class public Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;
.super LX/62U;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final n:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field public final o:Landroid/content/res/Resources;

.field public p:LX/1vv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1278701
    const-class v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1278696
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1278697
    const-class v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    move-object v0, p1

    .line 1278698
    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->n:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1278699
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->o:Landroid/content/res/Resources;

    .line 1278700
    return-void
.end method

.method public static a(LX/15i;ILandroid/text/SpannableStringBuilder;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const v2, -0x20b478b7

    const/4 v3, 0x0

    .line 1278683
    const v0, -0x7d3e8e01

    invoke-static {p0, p1, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1278684
    invoke-static {p0, p1, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278685
    invoke-static {p0, p1, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    move-object v1, v0

    :goto_1
    move v2, v3

    .line 1278686
    :goto_2
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1278687
    invoke-virtual {v1, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1278688
    sget-object v6, LX/7y0;->a:[I

    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v0, v7}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    .line 1278689
    :cond_0
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1278690
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1278691
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1278692
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1278693
    :pswitch_0
    invoke-virtual {v4, v5, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1278694
    new-instance v0, Landroid/text/style/URLSpan;

    invoke-virtual {v4, v5, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v9}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v4, v5, v9}, LX/15i;->j(II)I

    move-result v7

    const/4 v8, 0x1

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v4

    add-int/2addr v4, v7

    const/16 v5, 0x11

    invoke-static {p2, v0, v6, v4, v5}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto :goto_3

    .line 1278695
    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V
    .locals 1

    .prologue
    .line 1278679
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1278680
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    if-lt p3, p2, :cond_0

    if-le p3, v0, :cond_1

    .line 1278681
    :cond_0
    :goto_0
    return-void

    .line 1278682
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;

    invoke-static {p0}, LX/1vv;->a(LX/0QB;)LX/1vv;

    move-result-object p0

    check-cast p0, LX/1vv;

    iput-object p0, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->p:LX/1vv;

    return-void
.end method

.method public static b(LX/15i;ILandroid/text/SpannableStringBuilder;)V
    .locals 11

    .prologue
    const v2, -0x14729352

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 1278648
    const v0, -0x7d3e8e01

    invoke-static {p0, p1, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1278649
    invoke-static {p0, p1, v9, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278650
    invoke-static {p0, p1, v9, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    move-object v1, v0

    :goto_1
    move v2, v3

    .line 1278651
    :goto_2
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1278652
    invoke-virtual {v1, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1278653
    sget-object v6, LX/7y0;->b:[I

    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v4, v5, v9, v0, v10}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    .line 1278654
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1278655
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1278656
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1278657
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1278658
    :pswitch_0
    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v0

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v7

    add-int/2addr v7, v0

    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v4, v5, v9, v0, v10}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 1278659
    sget-object v4, LX/7y0;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1278660
    const v4, -0x6f6b64

    .line 1278661
    :goto_4
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0x11

    invoke-static {p2, v5, v6, v7, v4}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    .line 1278662
    goto :goto_3

    .line 1278663
    :pswitch_1
    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v0

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v7

    add-int/2addr v7, v0

    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v4, v5, v9, v0, v10}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 1278664
    sget-object v4, LX/7y0;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_2

    .line 1278665
    const/4 v4, 0x1

    .line 1278666
    :goto_5
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x11

    invoke-static {p2, v5, v6, v7, v4}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    .line 1278667
    goto :goto_3

    .line 1278668
    :pswitch_2
    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v7

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v4

    add-int/2addr v4, v7

    const/16 v5, 0x11

    invoke-static {p2, v0, v6, v4, v5}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 1278669
    :pswitch_3
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v4, v5, v8}, LX/15i;->j(II)I

    move-result v7

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v4

    add-int/2addr v4, v7

    const/16 v5, 0x11

    invoke-static {p2, v0, v6, v4, v5}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 1278670
    :cond_2
    return-void

    .line 1278671
    :pswitch_4
    const v4, -0x5c1c2

    .line 1278672
    goto :goto_4

    .line 1278673
    :pswitch_5
    const v4, -0xbd48d6

    .line 1278674
    goto :goto_4

    .line 1278675
    :pswitch_6
    const v4, -0xbd984e

    .line 1278676
    goto :goto_4

    .line 1278677
    :pswitch_7
    const/4 v4, 0x2

    .line 1278678
    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V
    .locals 14

    .prologue
    .line 1278618
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1278619
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1278620
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->n:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1278621
    const v3, -0x7d3e8e01

    invoke-static {v1, v0, v3}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1278622
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1278623
    const/4 p1, 0x3

    invoke-virtual {v1, v0, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1278624
    invoke-static {v1, v0, v3}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->b(LX/15i;ILandroid/text/SpannableStringBuilder;)V

    .line 1278625
    invoke-static {v1, v0, v3}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->a(LX/15i;ILandroid/text/SpannableStringBuilder;)V

    .line 1278626
    move-object v3, v3

    .line 1278627
    invoke-virtual {v2, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278628
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->n:Lcom/facebook/drawee/span/DraweeSpanTextView;

    const/4 p1, 0x0

    const/4 v5, 0x1

    .line 1278629
    const-class v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v1, v0, v5, v3, p1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    if-eqz v3, :cond_0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v1, v0, v5, v3, p1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    if-ne v3, v4, :cond_5

    .line 1278630
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->n:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1278631
    iget-object v2, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->n:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1278632
    new-instance v5, LX/2nQ;

    invoke-virtual {v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v5, v4}, LX/2nQ;-><init>(Ljava/lang/CharSequence;)V

    .line 1278633
    new-instance v6, Ljava/util/TreeSet;

    sget-object v4, LX/1vv;->a:Ljava/util/Comparator;

    invoke-direct {v6, v4}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 1278634
    const/4 v4, 0x0

    const v7, -0x20b478b7

    invoke-static {v1, v0, v4, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v4}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v4

    :goto_1
    invoke-virtual {v4}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 1278635
    const/4 v9, 0x3

    invoke-virtual {v8, v7, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    const/4 v9, 0x0

    const-class v10, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const/4 v11, 0x0

    invoke-virtual {v8, v7, v9, v10, v11}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    if-ne v9, v10, :cond_1

    .line 1278636
    new-instance v9, LX/34R;

    const/4 v10, 0x3

    invoke-virtual {v8, v7, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    new-instance v11, LX/1yN;

    const/4 v12, 0x2

    invoke-virtual {v8, v7, v12}, LX/15i;->j(II)I

    move-result v12

    const/4 v13, 0x1

    invoke-virtual {v8, v7, v13}, LX/15i;->j(II)I

    move-result v7

    invoke-direct {v11, v12, v7}, LX/1yN;-><init>(II)V

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-direct {v9, v10, v11, v7, v8}, LX/34R;-><init>(Landroid/net/Uri;LX/1yN;II)V

    invoke-virtual {v6, v9}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1278637
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v4

    goto :goto_1

    .line 1278638
    :cond_3
    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, LX/34R;

    .line 1278639
    iget-object v4, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->p:LX/1vv;

    iget-object v6, v9, LX/34R;->a:Landroid/net/Uri;

    const/4 v7, -0x1

    invoke-virtual {v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getLineHeight()I

    move-result v8

    iget-object v9, v9, LX/34R;->b:LX/1yN;

    const/4 v10, 0x2

    invoke-static {v10}, LX/34T;->a(I)I

    move-result v10

    sget-object v11, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v4 .. v11}, LX/1vv;->a(LX/2nQ;Landroid/net/Uri;IILX/1yN;ILcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_3

    .line 1278640
    :cond_4
    invoke-virtual {v2, v5}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setDraweeSpanStringBuilder(LX/2nQ;)V

    .line 1278641
    return-void

    .line 1278642
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1278643
    :cond_5
    sget-object v4, LX/7y0;->c:[I

    const-class v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v1, v0, v5, v3, p1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_0

    .line 1278644
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->o:Landroid/content/res/Resources;

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1278645
    :goto_4
    const/4 v4, 0x0

    int-to-float v3, v3

    invoke-virtual {v2, v4, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 1278646
    :pswitch_1
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->o:Landroid/content/res/Resources;

    const v4, 0x7f0b004b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto :goto_4

    .line 1278647
    :pswitch_2
    iget-object v3, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementParagraphViewHolder;->o:Landroid/content/res/Resources;

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
