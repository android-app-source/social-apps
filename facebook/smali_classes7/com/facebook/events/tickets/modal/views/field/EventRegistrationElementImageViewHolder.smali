.class public Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;
.super LX/62U;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final o:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/1Ai;

.field public n:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1278605
    const-class v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;

    const-string v1, "event_ticketing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->o:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1278606
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1278607
    new-instance v0, LX/7xz;

    invoke-direct {v0, p0}, LX/7xz;-><init>(Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;)V

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->m:LX/1Ai;

    .line 1278608
    const-class v0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1278609
    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1278610
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->n:LX/1Ad;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;LX/1ln;)V
    .locals 2
    .param p0    # Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1278611
    if-eqz p1, :cond_0

    .line 1278612
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1278613
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/field/EventRegistrationElementImageViewHolder;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1278614
    :cond_0
    return-void
.end method
