.class public Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private m:Lcom/facebook/widget/text/BetterTextView;

.field private n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1278111
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventTicketOrderView;

    const-string v1, "event_ticketing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1278112
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1278113
    const v0, 0x7f0d0eb3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1278114
    const v0, 0x7f0d0eb4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1278115
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/1Ad;)V
    .locals 2

    .prologue
    .line 1278116
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1278117
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/events/tickets/modal/views/EventTicketQrCodeViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1278118
    return-void
.end method
