.class public Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;
.super LX/62U;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1277651
    const-class v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;

    const-string v1, "event_ticketing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1277652
    invoke-direct {p0, p1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 1277653
    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1277654
    return-void
.end method
