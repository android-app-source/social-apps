.class public Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/7wG;


# instance fields
.field private p:Lcom/facebook/events/common/EventAnalyticsParams;

.field private q:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1275640
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/base/fragment/FbFragment;)I
    .locals 1

    .prologue
    .line 1275638
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1275639
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1275633
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1275634
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1275635
    const-string v1, "ticketing_flow_logging_info"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1275636
    const-string v1, "extras_event_analytics_params"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1275637
    return-object v0
.end method

.method private a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 3

    .prologue
    .line 1275625
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 1275626
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v1

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v1

    instance-of v2, v0, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    .line 1275627
    iput-boolean v2, v1, LX/6wu;->d:Z

    .line 1275628
    move-object v1, v1

    .line 1275629
    invoke-static {v0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a(Lcom/facebook/base/fragment/FbFragment;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1275630
    iput-object v0, v1, LX/6wu;->c:LX/0am;

    .line 1275631
    move-object v0, v1

    .line 1275632
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;LX/6wr;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1275624
    iput-object p1, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->q:LX/6wr;

    iput-object p2, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->r:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;

    invoke-static {v1}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a(Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;LX/6wr;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private c(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)Landroid/content/Intent;
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 1275595
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->d(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)LX/0Rf;

    move-result-object v0

    .line 1275596
    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    iget-object v2, v2, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    .line 1275597
    iput-object v2, v1, LX/6xd;->b:Ljava/lang/String;

    .line 1275598
    move-object v1, v1

    .line 1275599
    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v1

    invoke-virtual {v1}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    .line 1275600
    new-instance v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    new-instance v3, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    iget-object v4, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->A:Ljava/lang/String;

    const v5, 0x7f081d50

    new-array v6, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->D:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->B:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;-><init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V

    .line 1275601
    sget-object v3, LX/6qw;->EVENT_TICKETING:LX/6qw;

    sget-object v4, LX/6xg;->MOR_EVENT_TICKETING:LX/6xg;

    invoke-static {v3, v4, v0, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v0

    .line 1275602
    iput-object v2, v0, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1275603
    move-object v0, v0

    .line 1275604
    sget-object v1, LX/6vb;->EMAIL:LX/6vb;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 1275605
    iput-object v1, v0, LX/6qZ;->t:LX/0Rf;

    .line 1275606
    move-object v0, v0

    .line 1275607
    invoke-direct {p0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    .line 1275608
    iput-object v1, v0, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1275609
    move-object v0, v0

    .line 1275610
    iput-boolean v9, v0, LX/6qZ;->o:Z

    .line 1275611
    move-object v0, v0

    .line 1275612
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a:Ljava/lang/String;

    .line 1275613
    iput-object v1, v0, LX/6qZ;->q:Ljava/lang/String;

    .line 1275614
    move-object v0, v0

    .line 1275615
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->z:Ljava/lang/String;

    .line 1275616
    iput-object v1, v0, LX/6qZ;->r:Ljava/lang/String;

    .line 1275617
    move-object v0, v0

    .line 1275618
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->e:Ljava/lang/String;

    .line 1275619
    iput-object v1, v0, LX/6qZ;->s:Ljava/lang/String;

    .line 1275620
    move-object v0, v0

    .line 1275621
    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    .line 1275622
    new-instance v1, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v1, v0, p1, v2}, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;-><init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 1275623
    invoke-static {p0, v1}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;",
            ")",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1275555
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    sget-object v1, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    sget-object v1, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    .line 1275556
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iget-object v2, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/7xE;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1275557
    invoke-virtual {v1}, Lcom/facebook/payments/currency/CurrencyAmount;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1275558
    sget-object v1, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1275559
    sget-object v1, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1275560
    :cond_0
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1275590
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1275591
    invoke-static {p0, p0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1275592
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->q:LX/6wr;

    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1275593
    const/4 p1, 0x0

    invoke-virtual {v0, p0, p1, v1}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1275594
    return-void
.end method

.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 3

    .prologue
    .line 1275588
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->c(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1275589
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275578
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1275579
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->setContentView(I)V

    .line 1275580
    const/4 v0, 0x0

    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    invoke-static {p0, v0, v1}, LX/6wr;->b(Landroid/app/Activity;ZLX/73i;)V

    .line 1275581
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extras_event_analytics_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1275582
    if-nez p1, :cond_0

    .line 1275583
    new-instance v0, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    invoke-direct {v0}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;-><init>()V

    .line 1275584
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1275585
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1275586
    :cond_0
    sget-object v0, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1275587
    return-void
.end method

.method public final b(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 7

    .prologue
    .line 1275568
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 1275569
    invoke-static {v0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a(Lcom/facebook/base/fragment/FbFragment;)I

    move-result v1

    .line 1275570
    new-instance v2, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-direct {v2}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;-><init>()V

    .line 1275571
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1275572
    const-string v4, "extra_event_ticketing"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1275573
    const-string v4, "extra_modal_height"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1275574
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1275575
    move-object v1, v2

    .line 1275576
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f040056

    const v4, 0x7f040092

    const v5, 0x7f040055

    const v6, 0x7f040093

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const-class v1, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1275577
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1275565
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1275566
    sget-object v0, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1275567
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1275561
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1275562
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->finish()V

    .line 1275563
    :goto_0
    return-void

    .line 1275564
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method
