.class public Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/7wM;


# instance fields
.field private p:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1275729
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1275726
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1275727
    const-string v1, "order_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1275728
    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1275687
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1275688
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1275689
    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1275719
    new-instance v0, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    invoke-direct {v0}, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;-><init>()V

    .line 1275720
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1275721
    const-string v2, "event_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275722
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1275723
    move-object v0, v0

    .line 1275724
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1275725
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1275715
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 1275716
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a(Ljava/lang/String;)Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    move-result-object v1

    .line 1275717
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f040056

    const v4, 0x7f040092

    const v5, 0x7f040055

    const v6, 0x7f040093

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const-class v1, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1275718
    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1275712
    invoke-static {p1}, Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;->a(Ljava/lang/String;)Lcom/facebook/events/tickets/modal/fragments/EventTicketOrderDetailFragment;

    move-result-object v0

    .line 1275713
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1275714
    return-void
.end method


# virtual methods
.method public final a()LX/0h5;
    .locals 1

    .prologue
    .line 1275711
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->p:LX/0h5;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275695
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1275696
    const v0, 0x7f040011

    const v1, 0x7f040010

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->overridePendingTransition(II)V

    .line 1275697
    const v0, 0x7f03052a

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->setContentView(I)V

    .line 1275698
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1275699
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->p:LX/0h5;

    .line 1275700
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->p:LX/0h5;

    const v1, 0x7f081fb9

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1275701
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "order_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1275702
    invoke-virtual {p0}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "event_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1275703
    if-nez p1, :cond_1

    .line 1275704
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1275705
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Event ID and order ID don\'t exist."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1275706
    :cond_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1275707
    invoke-direct {p0, v0}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->f(Ljava/lang/String;)V

    .line 1275708
    :cond_1
    :goto_0
    return-void

    .line 1275709
    :cond_2
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1275710
    invoke-direct {p0, v1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1275692
    iget-object v0, p0, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->p:LX/0h5;

    new-instance v1, LX/7wL;

    invoke-direct {v1, p0}, LX/7wL;-><init>(Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1275693
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->e(Ljava/lang/String;)V

    .line 1275694
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1275690
    invoke-direct {p0, p1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->f(Ljava/lang/String;)V

    .line 1275691
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1275684
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1275685
    const v0, 0x7f040010

    const v1, 0x7f040012

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->overridePendingTransition(II)V

    .line 1275686
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0xba3b3dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1275681
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1275682
    iget-object v1, p0, Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;->p:LX/0h5;

    new-instance v2, LX/7wK;

    invoke-direct {v2, p0}, LX/7wK;-><init>(Lcom/facebook/events/tickets/modal/EventTicketsOrdersActivity;)V

    invoke-interface {v1, v2}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1275683
    const/16 v1, 0x23

    const v2, -0xcb7f6c8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
