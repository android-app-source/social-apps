.class public Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;
.super LX/6E7;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1275349
    const-class v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1275350
    const-class v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1275374
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1275375
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->a()V

    .line 1275376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1275371
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1275372
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->a()V

    .line 1275373
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275368
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1275369
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->a()V

    .line 1275370
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1275363
    const v0, 0x7f03052e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1275364
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->setOrientation(I)V

    .line 1275365
    const v0, 0x7f0d0eb5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1275366
    const v0, 0x7f0d0eb6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1275367
    return-void
.end method


# virtual methods
.method public setConfirmationText(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V
    .locals 3

    .prologue
    .line 1275356
    if-eqz p1, :cond_0

    .line 1275357
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V

    .line 1275358
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 1275359
    :goto_0
    return-void

    .line 1275360
    :catch_0
    move-exception v0

    .line 1275361
    sget-object v1, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->b:Ljava/lang/String;

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1275362
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setLogoUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1275351
    if-eqz p1, :cond_0

    .line 1275352
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1275353
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1275354
    :goto_0
    return-void

    .line 1275355
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationTextWithLogoRowView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
