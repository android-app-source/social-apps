.class public Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7vv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/fig/listitem/FigListItem;

.field private d:Lcom/facebook/widget/SwitchCompat;

.field public e:LX/0hs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1275447
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1275448
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a()V

    .line 1275449
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1275444
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1275445
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a()V

    .line 1275446
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275441
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1275442
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a()V

    .line 1275443
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1275433
    const-class v0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1275434
    const v0, 0x7f030531

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1275435
    const v0, 0x7f0d0eb7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    .line 1275436
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    .line 1275437
    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v1

    .line 1275438
    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->d:Lcom/facebook/widget/SwitchCompat;

    .line 1275439
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getBodyTextWithGlyph()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 1275440
    return-void
.end method

.method private static a(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;LX/0wM;LX/7vv;)V
    .locals 0

    .prologue
    .line 1275432
    iput-object p1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->b:LX/7vv;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/7vv;->a(LX/0QB;)LX/7vv;

    move-result-object v1

    check-cast v1, LX/7vv;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;LX/0wM;LX/7vv;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1275423
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    .line 1275424
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    const v1, 0x7f081fb1

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1275425
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1275426
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    iget-object v1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a:LX/0wM;

    const v2, 0x7f0208ed

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1275427
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    .line 1275428
    iput v4, v0, LX/0hs;->t:I

    .line 1275429
    invoke-virtual {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v1}, Lcom/facebook/fig/listitem/FigListItem;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v2, v0, v1

    .line 1275430
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->e:LX/0hs;

    iget-object v1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 1275431
    return-void
.end method

.method private getBodyTextWithGlyph()Landroid/text/SpannableString;
    .locals 6

    .prologue
    const/16 v5, 0x21

    const/4 v4, 0x0

    .line 1275413
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081fb0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1275414
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1275415
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->a:LX/0wM;

    const v2, 0x7f0208b3

    const v3, -0x958e80

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1275416
    if-eqz v0, :cond_0

    .line 1275417
    invoke-virtual {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1275418
    invoke-virtual {v0, v4, v4, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1275419
    new-instance v2, LX/34T;

    const/4 v3, 0x2

    invoke-direct {v2, v0, v3}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1275420
    const/4 v0, 0x1

    invoke-virtual {v1, v2, v4, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1275421
    :cond_0
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e013e

    invoke-direct {v0, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v1, v0, v4, v2, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1275422
    return-object v1
.end method


# virtual methods
.method public final a(LX/7w4;)V
    .locals 2

    .prologue
    .line 1275409
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->d:Lcom/facebook/widget/SwitchCompat;

    iget-boolean v1, p1, LX/7w4;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1275410
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->d:Lcom/facebook/widget/SwitchCompat;

    new-instance v1, LX/7w5;

    invoke-direct {v1, p0, p1}, LX/7w5;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;LX/7w4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1275411
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;->c:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v1, LX/7w6;

    invoke-direct {v1, p0}, LX/7w6;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingGoingOptionRowView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1275412
    return-void
.end method
