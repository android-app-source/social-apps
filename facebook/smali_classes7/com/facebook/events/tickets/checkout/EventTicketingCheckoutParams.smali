.class public Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/CheckoutParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

.field public final b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public final c:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1275101
    new-instance v0, LX/7vm;

    invoke-direct {v0}, LX/7vm;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1275113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275114
    const-class v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1275115
    const-class v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275116
    const-class v0, Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1275117
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1275108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275109
    iput-object p1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1275110
    iput-object p2, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275111
    iput-object p3, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1275112
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 1275118
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 3

    .prologue
    .line 1275107
    new-instance v0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;

    iget-object v1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v2, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v0, p1, v1, v2}, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;-><init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1275106
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1275102
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->a:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275103
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275104
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingCheckoutParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275105
    return-void
.end method
