.class public Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;
.super LX/6E7;
.source ""


# instance fields
.field public a:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1275515
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1275516
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a()V

    .line 1275517
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275518
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1275519
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a()V

    .line 1275520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1275521
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1275522
    invoke-direct {p0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a()V

    .line 1275523
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1275524
    const-class v0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;

    invoke-static {v0, p0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1275525
    const v0, 0x7f03053c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1275526
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->setOrientation(I)V

    .line 1275527
    const v0, 0x7f0d0ebb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1275528
    const v0, 0x7f0d0ebc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1275529
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v0

    check-cast v0, LX/1nG;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->a:LX/1nG;

    return-void
.end method


# virtual methods
.method public setInfoMessage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1275530
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275531
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1275532
    :goto_0
    return-void

    .line 1275533
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1275534
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTermsAndPolicies(LX/3Ab;)V
    .locals 2

    .prologue
    .line 1275535
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v1, LX/7wD;

    invoke-direct {v1, p0}, LX/7wD;-><init>(Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 1275536
    return-void
.end method
