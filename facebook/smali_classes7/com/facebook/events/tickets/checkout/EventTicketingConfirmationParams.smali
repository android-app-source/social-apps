.class public Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/confirmation/ConfirmationParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

.field public final b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public final c:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1275288
    new-instance v0, LX/7vw;

    invoke-direct {v0}, LX/7vw;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1275283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275284
    const-class v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    .line 1275285
    const-class v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275286
    const-class v0, Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1275287
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/confirmation/ConfirmationCommonParams;Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1275278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275279
    iput-object p1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    .line 1275280
    iput-object p2, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1275281
    iput-object p3, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1275282
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;
    .locals 1

    .prologue
    .line 1275272
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1275277
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1275273
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275274
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->b:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275275
    iget-object v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingConfirmationParams;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1275276
    return-void
.end method
