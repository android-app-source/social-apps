.class public Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/confirmation/ProductConfirmationData;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1275491
    new-instance v0, LX/7wA;

    invoke-direct {v0}, LX/7wA;-><init>()V

    sput-object v0, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1275492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275493
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->a:Z

    .line 1275494
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1275495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1275496
    iput-boolean p1, p0, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->a:Z

    .line 1275497
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1275498
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1275499
    iget-boolean v0, p0, Lcom/facebook/events/tickets/checkout/EventTicketingProductConfirmationData;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1275500
    return-void
.end method
