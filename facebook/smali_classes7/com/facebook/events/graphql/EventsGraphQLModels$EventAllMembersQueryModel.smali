.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e865a03
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242047
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242046
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1242044
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242045
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1242038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242039
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1242040
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1242041
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1242042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242043
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1242030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242031
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1242032
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    .line 1242033
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1242034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;

    .line 1242035
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    .line 1242036
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242037
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1242048
    new-instance v0, LX/7ou;

    invoke-direct {v0, p1}, LX/7ou;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllEventMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242028
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    .line 1242029
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1242026
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1242027
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1242025
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1242022
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;-><init>()V

    .line 1242023
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242024
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1242021
    const v0, 0x135a00f8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1242020
    const v0, 0x403827a

    return v0
.end method
