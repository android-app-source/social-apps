.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oc;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250080
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250053
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1250078
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250079
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1250075
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250076
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250077
    return-void
.end method

.method public static a(LX/7oc;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .locals 8

    .prologue
    .line 1250056
    if-nez p0, :cond_0

    .line 1250057
    const/4 p0, 0x0

    .line 1250058
    :goto_0
    return-object p0

    .line 1250059
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    if-eqz v0, :cond_1

    .line 1250060
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    goto :goto_0

    .line 1250061
    :cond_1
    new-instance v0, LX/7pt;

    invoke-direct {v0}, LX/7pt;-><init>()V

    .line 1250062
    invoke-interface {p0}, LX/7oc;->a()I

    move-result v1

    iput v1, v0, LX/7pt;->a:I

    .line 1250063
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1250064
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1250065
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1250066
    iget v3, v0, LX/7pt;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1250067
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1250068
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1250069
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1250070
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1250071
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1250072
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;-><init>(LX/15i;)V

    .line 1250073
    move-object p0, v3

    .line 1250074
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1250054
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1250055
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1250081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250082
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1250083
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1250084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250085
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1250042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250044
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1250036
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->e:I

    .line 1250037
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1250038
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1250039
    if-eqz v0, :cond_0

    .line 1250040
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1250041
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1250045
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1250046
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->e:I

    .line 1250047
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1250048
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;-><init>()V

    .line 1250049
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250050
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1250051
    const v0, 0x3e948e47

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1250052
    const v0, 0x34854315

    return v0
.end method
