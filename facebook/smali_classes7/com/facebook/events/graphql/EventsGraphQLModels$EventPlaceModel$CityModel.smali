.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1cca35f3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248856
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248861
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1248862
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248863
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1248890
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248891
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248892
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
    .locals 8

    .prologue
    .line 1248864
    if-nez p0, :cond_0

    .line 1248865
    const/4 p0, 0x0

    .line 1248866
    :goto_0
    return-object p0

    .line 1248867
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    if-eqz v0, :cond_1

    .line 1248868
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    goto :goto_0

    .line 1248869
    :cond_1
    new-instance v0, LX/7pk;

    invoke-direct {v0}, LX/7pk;-><init>()V

    .line 1248870
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pk;->a:Ljava/lang/String;

    .line 1248871
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1248872
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1248873
    iget-object v3, v0, LX/7pk;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1248874
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1248875
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1248876
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1248877
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1248878
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1248879
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1248880
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1248881
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;-><init>(LX/15i;)V

    .line 1248882
    move-object p0, v3

    .line 1248883
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1248884
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248885
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1248886
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1248887
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1248888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248889
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1248857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248859
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1248860
    new-instance v0, LX/7pl;

    invoke-direct {v0, p1}, LX/7pl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248846
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->e:Ljava/lang/String;

    .line 1248847
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1248848
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1248849
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1248850
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1248851
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;-><init>()V

    .line 1248852
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248853
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1248854
    const v0, -0x6ef899be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1248855
    const v0, 0x25d6af

    return v0
.end method
