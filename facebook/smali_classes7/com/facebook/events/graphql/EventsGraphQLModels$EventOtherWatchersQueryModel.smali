.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x204d84e4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248322
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248321
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1248319
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248320
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1248313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248314
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1248315
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1248316
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1248317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1248304
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248305
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1248306
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    .line 1248307
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1248308
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;

    .line 1248309
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    .line 1248310
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248311
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1248312
    new-instance v0, LX/7pe;

    invoke-direct {v0, p1}, LX/7pe;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOtherEventWatchers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248323
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    .line 1248324
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1248297
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1248298
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1248296
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1248299
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;-><init>()V

    .line 1248300
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248301
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1248302
    const v0, -0x6baf9035

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1248303
    const v0, 0x403827a

    return v0
.end method
