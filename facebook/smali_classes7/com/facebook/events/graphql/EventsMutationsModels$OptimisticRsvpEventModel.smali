.class public final Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x68085b86
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272774
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272766
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1272769
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1272770
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1272771
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1272772
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1272773
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1272759
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1272760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1272761
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1272762
    if-eqz v0, :cond_0

    .line 1272763
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1272764
    :cond_0
    return-void

    .line 1272765
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1272775
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1272776
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1272777
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1272778
    if-eqz v0, :cond_0

    .line 1272779
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1272780
    :cond_0
    return-void

    .line 1272781
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1272792
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k:Z

    .line 1272793
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1272794
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1272795
    if-eqz v0, :cond_0

    .line 1272796
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1272797
    :cond_0
    return-void
.end method

.method private l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272782
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1272783
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272790
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1272791
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    return-object v0
.end method

.method private n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272788
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1272789
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    return-object v0
.end method

.method private o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272786
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1272787
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    return-object v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 1272784
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1272785
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k:Z

    return v0
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272767
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1272768
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1272626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272627
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1272628
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1272629
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1272630
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1272631
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1272632
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1272633
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->q()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1272634
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1272635
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1272636
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1272637
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1272638
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1272639
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1272640
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1272641
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1272642
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1272643
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272644
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1272645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272646
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1272647
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1272648
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1272649
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    .line 1272650
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1272651
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1272652
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1272653
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1272654
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    .line 1272655
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1272656
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1272657
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1272658
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1272659
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    .line 1272660
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1272661
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1272662
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1272663
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1272664
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    .line 1272665
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1272666
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272667
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1272668
    new-instance v0, LX/7ul;

    invoke-direct {v0, p1}, LX/7ul;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272669
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1272670
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1272671
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k:Z

    .line 1272672
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1272673
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1272674
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    .line 1272675
    if-eqz v0, :cond_6

    .line 1272676
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272677
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272678
    iput v2, p2, LX/18L;->c:I

    .line 1272679
    :goto_0
    return-void

    .line 1272680
    :cond_0
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1272681
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v0

    .line 1272682
    if-eqz v0, :cond_6

    .line 1272683
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272684
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272685
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1272686
    :cond_1
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1272687
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    .line 1272688
    if-eqz v0, :cond_6

    .line 1272689
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272690
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272691
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1272692
    :cond_2
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1272693
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v0

    .line 1272694
    if-eqz v0, :cond_6

    .line 1272695
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272696
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272697
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1272698
    :cond_3
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1272699
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->k()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272700
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272701
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1272702
    :cond_4
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1272703
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->p()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272705
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1272706
    :cond_5
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1272707
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->q()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1272708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1272709
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1272710
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1272711
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1272712
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    .line 1272713
    if-eqz v0, :cond_0

    .line 1272714
    if-eqz p3, :cond_1

    .line 1272715
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1272716
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;->a(I)V

    .line 1272717
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1272718
    :cond_0
    :goto_0
    return-void

    .line 1272719
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;->a(I)V

    goto :goto_0

    .line 1272720
    :cond_2
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1272721
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v0

    .line 1272722
    if-eqz v0, :cond_0

    .line 1272723
    if-eqz p3, :cond_3

    .line 1272724
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1272725
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;->a(I)V

    .line 1272726
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    goto :goto_0

    .line 1272727
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1272728
    :cond_4
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1272729
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->n()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    .line 1272730
    if-eqz v0, :cond_0

    .line 1272731
    if-eqz p3, :cond_5

    .line 1272732
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1272733
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;->a(I)V

    .line 1272734
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    goto :goto_0

    .line 1272735
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;->a(I)V

    goto :goto_0

    .line 1272736
    :cond_6
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1272737
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->o()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    move-result-object v0

    .line 1272738
    if-eqz v0, :cond_0

    .line 1272739
    if-eqz p3, :cond_7

    .line 1272740
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    .line 1272741
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;->a(I)V

    .line 1272742
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;

    goto/16 :goto_0

    .line 1272743
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventWatchersModel;->a(I)V

    goto/16 :goto_0

    .line 1272744
    :cond_8
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1272745
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 1272746
    :cond_9
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1272747
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->a(Z)V

    goto/16 :goto_0

    .line 1272748
    :cond_a
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1272749
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1272750
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;-><init>()V

    .line 1272751
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1272752
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1272753
    const v0, -0x459e83b0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1272754
    const v0, 0x403827a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272755
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->i:Ljava/lang/String;

    .line 1272756
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272757
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1272758
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;->j:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method
