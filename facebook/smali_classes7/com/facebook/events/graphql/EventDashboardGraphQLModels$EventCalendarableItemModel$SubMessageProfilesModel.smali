.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4ad7ddcf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237795
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237794
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1237792
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1237793
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237789
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1237790
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1237791
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1237779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237780
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1237781
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1237782
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1237783
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1237784
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1237785
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1237786
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1237787
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237788
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1237776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237778
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1237775
    new-instance v0, LX/7n5;

    invoke-direct {v0, p1}, LX/7n5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237796
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1237773
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1237774
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1237772
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1237763
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;-><init>()V

    .line 1237764
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1237765
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237770
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->f:Ljava/lang/String;

    .line 1237771
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237768
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->g:Ljava/lang/String;

    .line 1237769
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1237767
    const v0, -0x3cf4d840

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1237766
    const v0, 0x50c72189

    return v0
.end method
