.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7om;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7b28942d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I

.field private t:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253692
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253691
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1253693
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1253694
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253695
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1253696
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1253697
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableTimeSlots"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253698
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0x1d39ca05

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->g:LX/3Sb;

    .line 1253699
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253700
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->h:Ljava/util/List;

    .line 1253701
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253702
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253703
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    return-object v0
.end method

.method private p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253704
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253705
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253706
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l:Ljava/util/List;

    .line 1253707
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253708
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m:Ljava/lang/String;

    .line 1253709
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253710
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->n:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->n:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 1253711
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->n:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method private t()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrefillValues"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253712
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0xf

    const v4, 0x5886ddaf

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t:LX/3Sb;

    .line 1253713
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253687
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 1253688
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253689
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    .line 1253690
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->v:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    return-object v0
.end method

.method private w()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeEnd"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 1253685
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253686
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeSelected"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253683
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253684
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeStart"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253681
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253682
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 1253640
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253641
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1253642
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l()LX/2uF;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v4

    .line 1253643
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1253644
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1253645
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x79daef33

    invoke-static {v8, v7, v9}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1253646
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->q()LX/0Px;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 1253647
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->r()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1253648
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1253649
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->a()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1253650
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j()LX/0Px;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v12

    .line 1253651
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t()LX/2uF;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v13

    .line 1253652
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->u()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1253653
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->v()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1253654
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w()LX/1vs;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget v0, v0, LX/1vs;->b:I

    move/from16 v16, v0

    const v18, 0x7094ead2

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1253655
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, 0x7550ba30

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1253656
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y()LX/1vs;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, LX/1vs;->b:I

    move/from16 v18, v0

    const v20, -0x67018e1c

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1253657
    const/16 v19, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1253658
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1253659
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->f:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1253660
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1253661
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1253662
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1253663
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1253664
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253665
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1253666
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1253667
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1253668
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1253669
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253670
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253671
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1253672
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->s:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1253673
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1253674
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1253675
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1253676
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253677
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253678
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253679
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253680
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1253587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253588
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1253589
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->l()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1253590
    if-eqz v1, :cond_0

    .line 1253591
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253592
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->g:LX/3Sb;

    .line 1253593
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1253594
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1253595
    if-eqz v1, :cond_1

    .line 1253596
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253597
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->h:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 1253598
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1253599
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253600
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1253601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253602
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253603
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1253604
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x79daef33

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1253605
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1253606
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253607
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j:I

    move-object v1, v0

    .line 1253608
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1253609
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1253610
    if-eqz v2, :cond_4

    .line 1253611
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253612
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->r:Ljava/util/List;

    move-object v1, v0

    .line 1253613
    :cond_4
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1253614
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1253615
    if-eqz v2, :cond_5

    .line 1253616
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253617
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->t:LX/3Sb;

    move-object v1, v0

    .line 1253618
    :cond_5
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1253619
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7094ead2

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1253620
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1253621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253622
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w:I

    move-object v1, v0

    .line 1253623
    :cond_6
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 1253624
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7550ba30

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1253625
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1253626
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253627
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x:I

    move-object v1, v0

    .line 1253628
    :cond_7
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1253629
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x67018e1c

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1253630
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1253631
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    .line 1253632
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y:I

    move-object v1, v0

    .line 1253633
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253634
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    .line 1253635
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 1253636
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 1253637
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 1253638
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :cond_9
    move-object p0, v1

    .line 1253639
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253585
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o:Ljava/lang/String;

    .line 1253586
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1253574
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1253575
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->f:Z

    .line 1253576
    const/4 v0, 0x5

    const v1, 0x79daef33

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->j:I

    .line 1253577
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->k:Z

    .line 1253578
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p:Z

    .line 1253579
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->q:Z

    .line 1253580
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->s:I

    .line 1253581
    const/16 v0, 0x12

    const v1, 0x7094ead2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->w:I

    .line 1253582
    const/16 v0, 0x13

    const v1, 0x7550ba30

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->x:I

    .line 1253583
    const/16 v0, 0x14

    const v1, -0x67018e1c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->y:I

    .line 1253584
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1253571
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;-><init>()V

    .line 1253572
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1253573
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1253569
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253570
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->p:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1253568
    const v0, -0x3b2f22e3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1253567
    const v0, -0xcee8c5a

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253563
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->r:Ljava/util/List;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->r:Ljava/util/List;

    .line 1253564
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 1253565
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253566
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;->s:I

    return v0
.end method
