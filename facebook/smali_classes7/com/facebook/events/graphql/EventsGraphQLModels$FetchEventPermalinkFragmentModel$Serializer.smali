.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1256736
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1256737
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1256735
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 1256410
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1256411
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v9, 0x23

    const/16 v8, 0x1f

    const/16 v4, 0xa

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 1256412
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1256413
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1256414
    if-eqz v2, :cond_0

    .line 1256415
    const-string v2, "action_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256416
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256417
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256418
    if-eqz v2, :cond_1

    .line 1256419
    const-string v3, "admin_setting"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256420
    invoke-static {v1, v2, p1}, LX/7r1;->a(LX/15i;ILX/0nX;)V

    .line 1256421
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256422
    if-eqz v2, :cond_2

    .line 1256423
    const-string v3, "album"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256424
    invoke-static {v1, v2, p1}, LX/7tO;->a(LX/15i;ILX/0nX;)V

    .line 1256425
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256426
    if-eqz v2, :cond_3

    .line 1256427
    const-string v3, "attending_activity"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256428
    invoke-static {v1, v2, p1, p2}, LX/7qh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256429
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256430
    if-eqz v2, :cond_4

    .line 1256431
    const-string v3, "can_guests_invite_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256432
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256433
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256434
    if-eqz v2, :cond_5

    .line 1256435
    const-string v3, "can_post_be_moderated"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256436
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256437
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256438
    if-eqz v2, :cond_6

    .line 1256439
    const-string v3, "can_view_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256440
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256441
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256442
    if-eqz v2, :cond_7

    .line 1256443
    const-string v3, "can_viewer_change_guest_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256444
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256445
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256446
    if-eqz v2, :cond_8

    .line 1256447
    const-string v3, "can_viewer_create_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256448
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256449
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256450
    if-eqz v2, :cond_9

    .line 1256451
    const-string v3, "can_viewer_purchase_onsite_tickets"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256452
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256453
    :cond_9
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1256454
    if-eqz v2, :cond_a

    .line 1256455
    const-string v2, "connection_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256456
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256457
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256458
    if-eqz v2, :cond_b

    .line 1256459
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256460
    invoke-static {v1, v2, p1, p2}, LX/7qx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256461
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256462
    if-eqz v2, :cond_c

    .line 1256463
    const-string v3, "created_for_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256464
    invoke-static {v1, v2, p1}, LX/7r2;->a(LX/15i;ILX/0nX;)V

    .line 1256465
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1256466
    cmp-long v4, v2, v6

    if-eqz v4, :cond_d

    .line 1256467
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256468
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1256469
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1256470
    cmp-long v4, v2, v6

    if-eqz v4, :cond_e

    .line 1256471
    const-string v4, "end_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256472
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1256473
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256474
    if-eqz v2, :cond_f

    .line 1256475
    const-string v3, "eventCategoryLabel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256476
    invoke-static {v1, v2, p1}, LX/7r3;->a(LX/15i;ILX/0nX;)V

    .line 1256477
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256478
    if-eqz v2, :cond_10

    .line 1256479
    const-string v3, "eventProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256480
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1256481
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256482
    if-eqz v2, :cond_11

    .line 1256483
    const-string v3, "eventUrl"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256484
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256485
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1256486
    if-eqz v2, :cond_12

    .line 1256487
    const-string v3, "event_activity_unseen_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256488
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1256489
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256490
    if-eqz v2, :cond_13

    .line 1256491
    const-string v3, "event_buy_ticket_display_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256492
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256493
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256494
    if-eqz v2, :cond_14

    .line 1256495
    const-string v3, "event_buy_ticket_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256496
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256497
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256498
    if-eqz v2, :cond_15

    .line 1256499
    const-string v3, "event_category_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256500
    invoke-static {v1, v2, p1, p2}, LX/7r0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256501
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256502
    if-eqz v2, :cond_16

    .line 1256503
    const-string v3, "event_creator"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256504
    invoke-static {v1, v2, p1, p2}, LX/7qi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256505
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256506
    if-eqz v2, :cond_17

    .line 1256507
    const-string v3, "event_declines"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256508
    invoke-static {v1, v2, p1}, LX/7tP;->a(LX/15i;ILX/0nX;)V

    .line 1256509
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256510
    if-eqz v2, :cond_18

    .line 1256511
    const-string v3, "event_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256512
    invoke-static {v1, v2, p1, p2}, LX/7rA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256513
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256514
    if-eqz v2, :cond_19

    .line 1256515
    const-string v3, "event_email_associates"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256516
    invoke-static {v1, v2, p1}, LX/7tQ;->a(LX/15i;ILX/0nX;)V

    .line 1256517
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256518
    if-eqz v2, :cond_1a

    .line 1256519
    const-string v3, "event_email_declines"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256520
    invoke-static {v1, v2, p1}, LX/7tR;->a(LX/15i;ILX/0nX;)V

    .line 1256521
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256522
    if-eqz v2, :cond_1b

    .line 1256523
    const-string v3, "event_email_invitees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256524
    invoke-static {v1, v2, p1}, LX/7tS;->a(LX/15i;ILX/0nX;)V

    .line 1256525
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256526
    if-eqz v2, :cond_1c

    .line 1256527
    const-string v3, "event_email_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256528
    invoke-static {v1, v2, p1}, LX/7tT;->a(LX/15i;ILX/0nX;)V

    .line 1256529
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256530
    if-eqz v2, :cond_1d

    .line 1256531
    const-string v3, "event_hosts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256532
    invoke-static {v1, v2, p1, p2}, LX/7r5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256533
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256534
    if-eqz v2, :cond_1e

    .line 1256535
    const-string v3, "event_invitees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256536
    invoke-static {v1, v2, p1}, LX/7tU;->a(LX/15i;ILX/0nX;)V

    .line 1256537
    :cond_1e
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 1256538
    if-eqz v2, :cond_1f

    .line 1256539
    const-string v2, "event_kind"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256540
    invoke-virtual {v1, v0, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256541
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256542
    if-eqz v2, :cond_20

    .line 1256543
    const-string v3, "event_maybes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256544
    invoke-static {v1, v2, p1}, LX/7tV;->a(LX/15i;ILX/0nX;)V

    .line 1256545
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256546
    if-eqz v2, :cond_21

    .line 1256547
    const-string v3, "event_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256548
    invoke-static {v1, v2, p1}, LX/7tW;->a(LX/15i;ILX/0nX;)V

    .line 1256549
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256550
    if-eqz v2, :cond_22

    .line 1256551
    const-string v3, "event_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256552
    invoke-static {v1, v2, p1, p2}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256553
    :cond_22
    invoke-virtual {v1, v0, v9}, LX/15i;->g(II)I

    move-result v2

    .line 1256554
    if-eqz v2, :cond_23

    .line 1256555
    const-string v2, "event_privacy_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256556
    invoke-virtual {v1, v0, v9}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256557
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256558
    if-eqz v2, :cond_24

    .line 1256559
    const-string v2, "event_promotion_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256560
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256561
    :cond_24
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256562
    if-eqz v2, :cond_25

    .line 1256563
    const-string v3, "event_sms_associates"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256564
    invoke-static {v1, v2, p1}, LX/7tX;->a(LX/15i;ILX/0nX;)V

    .line 1256565
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256566
    if-eqz v2, :cond_26

    .line 1256567
    const-string v3, "event_sms_declines"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256568
    invoke-static {v1, v2, p1}, LX/7tY;->a(LX/15i;ILX/0nX;)V

    .line 1256569
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256570
    if-eqz v2, :cond_27

    .line 1256571
    const-string v3, "event_sms_invitees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256572
    invoke-static {v1, v2, p1}, LX/7tZ;->a(LX/15i;ILX/0nX;)V

    .line 1256573
    :cond_27
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256574
    if-eqz v2, :cond_28

    .line 1256575
    const-string v3, "event_sms_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256576
    invoke-static {v1, v2, p1}, LX/7ta;->a(LX/15i;ILX/0nX;)V

    .line 1256577
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256578
    if-eqz v2, :cond_29

    .line 1256579
    const-string v3, "event_ticket_provider_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256580
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256581
    :cond_29
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256582
    if-eqz v2, :cond_2a

    .line 1256583
    const-string v2, "event_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256584
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256585
    :cond_2a
    const/16 v2, 0x2b

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1256586
    if-eqz v2, :cond_2b

    .line 1256587
    const-string v3, "event_user_location_shares_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256588
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1256589
    :cond_2b
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1256590
    if-eqz v2, :cond_2c

    .line 1256591
    const-string v3, "event_user_location_shares_start_interval"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256592
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1256593
    :cond_2c
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256594
    if-eqz v2, :cond_2d

    .line 1256595
    const-string v3, "event_viewer_capability"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256596
    invoke-static {v1, v2, p1}, LX/7tF;->a(LX/15i;ILX/0nX;)V

    .line 1256597
    :cond_2d
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256598
    if-eqz v2, :cond_2e

    .line 1256599
    const-string v2, "event_visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256600
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256601
    :cond_2e
    const/16 v2, 0x2f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256602
    if-eqz v2, :cond_2f

    .line 1256603
    const-string v3, "event_watchers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256604
    invoke-static {v1, v2, p1}, LX/7tb;->a(LX/15i;ILX/0nX;)V

    .line 1256605
    :cond_2f
    const/16 v2, 0x30

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256606
    if-eqz v2, :cond_30

    .line 1256607
    const-string v3, "friendEventInviteesFirst5"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256608
    invoke-static {v1, v2, p1, p2}, LX/7td;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256609
    :cond_30
    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256610
    if-eqz v2, :cond_31

    .line 1256611
    const-string v3, "friendEventMaybesFirst5"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256612
    invoke-static {v1, v2, p1, p2}, LX/7s8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256613
    :cond_31
    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256614
    if-eqz v2, :cond_32

    .line 1256615
    const-string v3, "friendEventMembersFirst5"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256616
    invoke-static {v1, v2, p1, p2}, LX/7sA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256617
    :cond_32
    const/16 v2, 0x33

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256618
    if-eqz v2, :cond_33

    .line 1256619
    const-string v3, "friendEventWatchersFirst5"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256620
    invoke-static {v1, v2, p1, p2}, LX/7sC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256621
    :cond_33
    const/16 v2, 0x34

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256622
    if-eqz v2, :cond_34

    .line 1256623
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256624
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256625
    :cond_34
    const/16 v2, 0x35

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256626
    if-eqz v2, :cond_35

    .line 1256627
    const-string v3, "is_all_day"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256628
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256629
    :cond_35
    const/16 v2, 0x36

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256630
    if-eqz v2, :cond_36

    .line 1256631
    const-string v3, "is_canceled"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256632
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256633
    :cond_36
    const/16 v2, 0x37

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256634
    if-eqz v2, :cond_37

    .line 1256635
    const-string v3, "is_event_draft"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256636
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256637
    :cond_37
    const/16 v2, 0x38

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256638
    if-eqz v2, :cond_38

    .line 1256639
    const-string v3, "is_official"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256640
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256641
    :cond_38
    const/16 v2, 0x39

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256642
    if-eqz v2, :cond_39

    .line 1256643
    const-string v3, "is_privacy_locked"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256644
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256645
    :cond_39
    const/16 v2, 0x3a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256646
    if-eqz v2, :cond_3a

    .line 1256647
    const-string v3, "live_permalink_time_range_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256648
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256649
    :cond_3a
    const/16 v2, 0x3b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256650
    if-eqz v2, :cond_3b

    .line 1256651
    const-string v3, "max_ticket_price"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256652
    invoke-static {v1, v2, p1}, LX/7sp;->a(LX/15i;ILX/0nX;)V

    .line 1256653
    :cond_3b
    const/16 v2, 0x3c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256654
    if-eqz v2, :cond_3c

    .line 1256655
    const-string v3, "min_ticket_price"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256656
    invoke-static {v1, v2, p1}, LX/7sq;->a(LX/15i;ILX/0nX;)V

    .line 1256657
    :cond_3c
    const/16 v2, 0x3d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256658
    if-eqz v2, :cond_3d

    .line 1256659
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256660
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256661
    :cond_3d
    const/16 v2, 0x3e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256662
    if-eqz v2, :cond_3e

    .line 1256663
    const-string v3, "parent_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256664
    invoke-static {v1, v2, p1}, LX/7r6;->a(LX/15i;ILX/0nX;)V

    .line 1256665
    :cond_3e
    const/16 v2, 0x3f

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1256666
    if-eqz v2, :cond_3f

    .line 1256667
    const-string v3, "pending_post_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256668
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1256669
    :cond_3f
    const/16 v2, 0x40

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256670
    if-eqz v2, :cond_40

    .line 1256671
    const-string v3, "post_approval_required"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256672
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256673
    :cond_40
    const/16 v2, 0x41

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256674
    if-eqz v2, :cond_41

    .line 1256675
    const-string v3, "posted_item_privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256676
    invoke-static {v1, v2, p1, p2}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256677
    :cond_41
    const/16 v2, 0x42

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256678
    if-eqz v2, :cond_42

    .line 1256679
    const-string v3, "saved_collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256680
    invoke-static {v1, v2, p1, p2}, LX/40j;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256681
    :cond_42
    const/16 v2, 0x43

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1256682
    cmp-long v4, v2, v6

    if-eqz v4, :cond_43

    .line 1256683
    const-string v4, "scheduled_publish_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256684
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1256685
    :cond_43
    const/16 v2, 0x44

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1256686
    cmp-long v4, v2, v6

    if-eqz v4, :cond_44

    .line 1256687
    const-string v4, "start_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256688
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1256689
    :cond_44
    const/16 v2, 0x45

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256690
    if-eqz v2, :cond_45

    .line 1256691
    const-string v3, "suggested_event_context_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256692
    invoke-static {v1, v2, p1}, LX/7sD;->a(LX/15i;ILX/0nX;)V

    .line 1256693
    :cond_45
    const/16 v2, 0x46

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256694
    if-eqz v2, :cond_46

    .line 1256695
    const-string v3, "supported_taggable_activities"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256696
    invoke-static {v1, v2, p1, p2}, LX/7te;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256697
    :cond_46
    const/16 v2, 0x47

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256698
    if-eqz v2, :cond_47

    .line 1256699
    const-string v3, "supports_event_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256700
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256701
    :cond_47
    const/16 v2, 0x48

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256702
    if-eqz v2, :cond_48

    .line 1256703
    const-string v3, "ticket_tiers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256704
    invoke-static {v1, v2, p1, p2}, LX/7ss;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256705
    :cond_48
    const/16 v2, 0x49

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1256706
    if-eqz v2, :cond_49

    .line 1256707
    const-string v3, "timezone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256708
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256709
    :cond_49
    const/16 v2, 0x4a

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1256710
    if-eqz v2, :cond_4a

    .line 1256711
    const-string v3, "total_purchased_tickets"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256712
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1256713
    :cond_4a
    const/16 v2, 0x4b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256714
    if-eqz v2, :cond_4b

    .line 1256715
    const-string v2, "viewer_guest_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256716
    const/16 v2, 0x4b

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256717
    :cond_4b
    const/16 v2, 0x4c

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1256718
    if-eqz v2, :cond_4c

    .line 1256719
    const-string v3, "viewer_has_pending_invite"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256720
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1256721
    :cond_4c
    const/16 v2, 0x4d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256722
    if-eqz v2, :cond_4d

    .line 1256723
    const-string v3, "viewer_inviters"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256724
    invoke-static {v1, v2, p1, p2}, LX/7u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1256725
    :cond_4d
    const/16 v2, 0x4e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256726
    if-eqz v2, :cond_4e

    .line 1256727
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256728
    const/16 v2, 0x4e

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256729
    :cond_4e
    const/16 v2, 0x4f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1256730
    if-eqz v2, :cond_4f

    .line 1256731
    const-string v2, "viewer_watch_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1256732
    const/16 v2, 0x4f

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1256733
    :cond_4f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1256734
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1256409
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
