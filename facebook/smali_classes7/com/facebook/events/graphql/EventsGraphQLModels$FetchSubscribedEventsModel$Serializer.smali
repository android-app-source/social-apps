.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1258251
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1258252
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1258230
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1258232
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1258233
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1258234
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1258235
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1258236
    if-eqz v2, :cond_1

    .line 1258237
    const-string v3, "subscribed_calendar_profiles"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1258238
    const/4 v3, 0x0

    .line 1258239
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1258240
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1258241
    if-eqz v3, :cond_0

    .line 1258242
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1258243
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1258244
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1258245
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1258246
    if-eqz v2, :cond_2

    .line 1258247
    const-string v3, "subscribed_profile_calendar_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1258248
    invoke-static {v1, v2, p1, p2}, LX/7to;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1258249
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1258250
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1258231
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;LX/0nX;LX/0my;)V

    return-void
.end method
