.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    const-wide/16 v4, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v1

    :cond_0
    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    invoke-virtual {p0, p1, v8, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, 0x3d76496b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, 0x3d76496b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, 0x3d76496b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v0

    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v1

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x4

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x6

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x7

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    invoke-virtual {p3, v8, v0}, LX/186;->a(IZ)V

    invoke-virtual {p3, v9, v1}, LX/186;->a(IZ)V

    invoke-virtual {p3, v10, v2}, LX/186;->b(II)V

    const/4 v0, 0x4

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    const/4 v0, 0x5

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    const/4 v0, 0x6

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, 0x328e79d9

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, 0x719af298

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x4

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v1}, LX/186;->b(II)V

    invoke-virtual {p3, v10, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_c
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    invoke-virtual {p0, p1, v8, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_17
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v2, -0x20b478b7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v3

    const v4, -0x14729352

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v3

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v2

    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v2

    invoke-virtual {p0, p1, v9}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    const v3, -0x502a23c1

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v3

    const v4, 0x3ebacdf3

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    const v3, -0x32767df3    # -2.883752E8f

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_21
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v1}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_22
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_23
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_24
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_25
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v10, v4}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_26
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v2

    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v8, v2, v1}, LX/186;->a(III)V

    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_27
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x481ebddc

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v1}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_28
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    invoke-virtual {p3, v8, v6}, LX/186;->b(II)V

    invoke-virtual {p3, v9, v7}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_29
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_2a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_2b
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_2c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_2d
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_2e
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d3e8e01 -> :sswitch_18
        -0x7b94614d -> :sswitch_2a
        -0x76b8f127 -> :sswitch_24
        -0x7337c51a -> :sswitch_22
        -0x6ccc86d3 -> :sswitch_16
        -0x67018e1c -> :sswitch_13
        -0x60e548fa -> :sswitch_4
        -0x60673e54 -> :sswitch_1
        -0x502a23c1 -> :sswitch_1e
        -0x5021254b -> :sswitch_25
        -0x481ebddc -> :sswitch_28
        -0x44babd00 -> :sswitch_5
        -0x4130883f -> :sswitch_23
        -0x3bc85c65 -> :sswitch_c
        -0x32767df3 -> :sswitch_1f
        -0x2229e847 -> :sswitch_1b
        -0x21196ea6 -> :sswitch_26
        -0x20b478b7 -> :sswitch_19
        -0x1b60c62d -> :sswitch_20
        -0x14729352 -> :sswitch_1a
        -0x138d5a56 -> :sswitch_2
        -0x4d8b640 -> :sswitch_d
        0x73f2b0 -> :sswitch_27
        0x77f9e45 -> :sswitch_2c
        0x112f0644 -> :sswitch_e
        0x1490253d -> :sswitch_b
        0x190f5b39 -> :sswitch_2d
        0x1d39ca05 -> :sswitch_1d
        0x24510586 -> :sswitch_3
        0x298c90b0 -> :sswitch_21
        0x316fe152 -> :sswitch_f
        0x328e79d9 -> :sswitch_8
        0x3d76496b -> :sswitch_6
        0x3e343ca2 -> :sswitch_10
        0x3ebacdf3 -> :sswitch_14
        0x5159bf9b -> :sswitch_17
        0x5886ddaf -> :sswitch_15
        0x5f0ceed9 -> :sswitch_2e
        0x6d9a73ae -> :sswitch_2b
        0x6db3f1df -> :sswitch_29
        0x6e394281 -> :sswitch_7
        0x7094ead2 -> :sswitch_11
        0x719af298 -> :sswitch_a
        0x7372fd38 -> :sswitch_0
        0x7550ba30 -> :sswitch_12
        0x757c2504 -> :sswitch_9
        0x79daef33 -> :sswitch_1c
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    const/4 v7, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    invoke-static {v2, v3, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;
    .locals 1

    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    if-eqz p0, :cond_0

    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 5

    const/4 v4, 0x2

    const v3, 0x3d76496b

    const/4 v2, 0x1

    const/4 v1, 0x0

    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :sswitch_0
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    :goto_0
    :sswitch_1
    return-void

    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v1, 0x328e79d9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v1, 0x719af298

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_7
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_8
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_9
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x20b478b7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x14729352

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x502a23c1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    const v1, 0x3ebacdf3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x32767df3    # -2.883752E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    :sswitch_d
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    const v1, -0x481ebddc

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    :sswitch_f
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    invoke-static {v0, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d3e8e01 -> :sswitch_a
        -0x7b94614d -> :sswitch_1
        -0x76b8f127 -> :sswitch_d
        -0x7337c51a -> :sswitch_1
        -0x6ccc86d3 -> :sswitch_1
        -0x67018e1c -> :sswitch_1
        -0x60e548fa -> :sswitch_3
        -0x60673e54 -> :sswitch_0
        -0x502a23c1 -> :sswitch_c
        -0x5021254b -> :sswitch_1
        -0x481ebddc -> :sswitch_f
        -0x44babd00 -> :sswitch_4
        -0x4130883f -> :sswitch_1
        -0x3bc85c65 -> :sswitch_7
        -0x32767df3 -> :sswitch_1
        -0x2229e847 -> :sswitch_1
        -0x21196ea6 -> :sswitch_1
        -0x20b478b7 -> :sswitch_1
        -0x1b60c62d -> :sswitch_1
        -0x14729352 -> :sswitch_1
        -0x138d5a56 -> :sswitch_2
        -0x4d8b640 -> :sswitch_1
        0x73f2b0 -> :sswitch_e
        0x77f9e45 -> :sswitch_1
        0x112f0644 -> :sswitch_1
        0x1490253d -> :sswitch_1
        0x190f5b39 -> :sswitch_1
        0x1d39ca05 -> :sswitch_b
        0x24510586 -> :sswitch_1
        0x298c90b0 -> :sswitch_1
        0x316fe152 -> :sswitch_8
        0x328e79d9 -> :sswitch_1
        0x3d76496b -> :sswitch_1
        0x3e343ca2 -> :sswitch_1
        0x3ebacdf3 -> :sswitch_1
        0x5159bf9b -> :sswitch_9
        0x5886ddaf -> :sswitch_1
        0x5f0ceed9 -> :sswitch_1
        0x6d9a73ae -> :sswitch_1
        0x6db3f1df -> :sswitch_1
        0x6e394281 -> :sswitch_5
        0x7094ead2 -> :sswitch_1
        0x719af298 -> :sswitch_1
        0x7372fd38 -> :sswitch_1
        0x7550ba30 -> :sswitch_1
        0x757c2504 -> :sswitch_6
        0x79daef33 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p0, p1, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a:LX/15i;

    iput p2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->b:I

    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    iget v0, p0, LX/1vt;->c:I

    move v0, v0

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, LX/1vt;->c:I

    move v0, v0

    return v0
.end method

.method public final o_()I
    .locals 1

    iget v0, p0, LX/1vt;->b:I

    move v0, v0

    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, LX/1vt;->a:LX/15i;

    move-object v0, v0

    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    invoke-virtual {v3, v9}, LX/186;->d(I)V

    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    invoke-static {v3, v9, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    return-object v0
.end method

.method public final u_()I
    .locals 1

    iget v0, p0, LX/1vt;->c:I

    move v0, v0

    return v0
.end method
