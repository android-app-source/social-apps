.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249665
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1249666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249623
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1249624
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1249625
    const/4 v2, 0x0

    .line 1249626
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1249627
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249628
    :goto_0
    move v1, v2

    .line 1249629
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1249630
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1249631
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;-><init>()V

    .line 1249632
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1249633
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1249634
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1249635
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1249636
    :cond_0
    return-object v1

    .line 1249637
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249638
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1249639
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1249640
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1249642
    const-string v4, "event_sms_invitees"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1249643
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1249644
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_a

    .line 1249645
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249646
    :goto_2
    move v1, v3

    .line 1249647
    goto :goto_1

    .line 1249648
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1249649
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1249650
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1249651
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 1249652
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1249653
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249654
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v7, :cond_5

    .line 1249655
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1249656
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v4

    goto :goto_3

    .line 1249657
    :cond_6
    const-string p0, "nodes"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1249658
    invoke-static {p1, v0}, LX/7s1;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 1249659
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1249660
    :cond_8
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1249661
    if-eqz v1, :cond_9

    .line 1249662
    invoke-virtual {v0, v3, v6, v3}, LX/186;->a(III)V

    .line 1249663
    :cond_9
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1249664
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v1, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method
