.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1251196
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1251197
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1251175
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1251177
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1251178
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1251179
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1251180
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1251181
    if-eqz v2, :cond_1

    .line 1251182
    const-string v3, "event_viewer_capability"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1251183
    const/4 v3, 0x0

    .line 1251184
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1251185
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1251186
    if-eqz v3, :cond_0

    .line 1251187
    const-string p0, "remaining_invites"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1251188
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1251189
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1251190
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1251191
    if-eqz v2, :cond_2

    .line 1251192
    const-string v3, "uninvitable_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1251193
    invoke-static {v1, v2, p1, p2}, LX/7sJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1251194
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1251195
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1251176
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;LX/0nX;LX/0my;)V

    return-void
.end method
