.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249176
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1249177
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249178
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1249179
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1249180
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1249181
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249182
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1249183
    if-eqz v2, :cond_7

    .line 1249184
    const-string v3, "invitable_entries_token_query"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249185
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249186
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1249187
    if-eqz v3, :cond_5

    .line 1249188
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249189
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1249190
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_4

    .line 1249191
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    const/4 p2, 0x0

    .line 1249192
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249193
    invoke-virtual {v1, p0, p2}, LX/15i;->g(II)I

    move-result v0

    .line 1249194
    if-eqz v0, :cond_0

    .line 1249195
    const-string v0, "__type__"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249196
    invoke-static {v1, p0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1249197
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1249198
    if-eqz v0, :cond_1

    .line 1249199
    const-string p2, "photo"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249200
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1249201
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1249202
    if-eqz v0, :cond_2

    .line 1249203
    const-string p2, "title"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249204
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1249205
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1249206
    if-eqz v0, :cond_3

    .line 1249207
    const-string p2, "token"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249208
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1249209
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249210
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1249211
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1249212
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1249213
    if-eqz v3, :cond_6

    .line 1249214
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249215
    invoke-static {v1, v3, p1}, LX/4aB;->a(LX/15i;ILX/0nX;)V

    .line 1249216
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249217
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249218
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1249219
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
