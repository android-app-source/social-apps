.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7ebb5d84
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242205
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242204
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1242202
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242203
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1242194
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242195
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1242196
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1242197
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1242198
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1242199
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1242200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242201
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1242186
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242187
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1242188
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    .line 1242189
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1242190
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;

    .line 1242191
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    .line 1242192
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242193
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242184
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->e:Ljava/lang/String;

    .line 1242185
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1242181
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;-><init>()V

    .line 1242182
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242183
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1242177
    const v0, 0x6d4f4145

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1242180
    const v0, 0x7bb566fd

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242178
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    .line 1242179
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel$PageInfoSectionsModel$FieldsModel$ValueModel;

    return-object v0
.end method
