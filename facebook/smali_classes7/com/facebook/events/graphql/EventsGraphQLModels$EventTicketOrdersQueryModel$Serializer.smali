.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1251986
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1251987
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1251988
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1251989
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1251990
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    .line 1251991
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1251992
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1251993
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 1251994
    const-string v4, "end_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1251995
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1251996
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1251997
    if-eqz v2, :cond_1

    .line 1251998
    const-string v3, "eventProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1251999
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1252000
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252001
    if-eqz v2, :cond_2

    .line 1252002
    const-string v3, "event_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252003
    invoke-static {v1, v2, p1, p2}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252004
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252005
    if-eqz v2, :cond_3

    .line 1252006
    const-string v3, "event_ticket_provider"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252007
    invoke-static {v1, v2, p1, p2}, LX/7sa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252008
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1252009
    if-eqz v2, :cond_4

    .line 1252010
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252011
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1252012
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1252013
    if-eqz v2, :cond_5

    .line 1252014
    const-string v3, "is_all_day"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252015
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1252016
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1252017
    if-eqz v2, :cond_6

    .line 1252018
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252019
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1252020
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252021
    if-eqz v2, :cond_a

    .line 1252022
    const-string v3, "purchased_ticket_orders"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252023
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1252024
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1252025
    if-eqz v3, :cond_8

    .line 1252026
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252027
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1252028
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 1252029
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1, p2}, LX/7sc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252030
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1252031
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1252032
    :cond_8
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1252033
    if-eqz v3, :cond_9

    .line 1252034
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252035
    invoke-static {v1, v3, p1}, LX/4aB;->a(LX/15i;ILX/0nX;)V

    .line 1252036
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1252037
    :cond_a
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1252038
    cmp-long v4, v2, v6

    if-eqz v4, :cond_b

    .line 1252039
    const-string v4, "start_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252040
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1252041
    :cond_b
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1252042
    if-eqz v2, :cond_c

    .line 1252043
    const-string v3, "timezone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252044
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1252045
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1252046
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1252047
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
