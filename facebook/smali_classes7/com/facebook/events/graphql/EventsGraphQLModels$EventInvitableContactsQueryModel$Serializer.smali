.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1247207
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1247208
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1247206
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1247178
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1247179
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1247180
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1247181
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1247182
    if-eqz v2, :cond_2

    .line 1247183
    const-string v3, "event_invitable_contacts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1247184
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1247185
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1247186
    if-eqz v3, :cond_0

    .line 1247187
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1247188
    invoke-static {v1, v3, p1, p2}, LX/7rc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1247189
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1247190
    if-eqz v3, :cond_1

    .line 1247191
    const-string p0, "page_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1247192
    invoke-static {v1, v3, p1}, LX/4aB;->a(LX/15i;ILX/0nX;)V

    .line 1247193
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1247194
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1247195
    if-eqz v2, :cond_4

    .line 1247196
    const-string v3, "event_viewer_capability"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1247197
    const/4 v3, 0x0

    .line 1247198
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1247199
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1247200
    if-eqz v3, :cond_3

    .line 1247201
    const-string p0, "remaining_invites"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1247202
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1247203
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1247204
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1247205
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1247177
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
