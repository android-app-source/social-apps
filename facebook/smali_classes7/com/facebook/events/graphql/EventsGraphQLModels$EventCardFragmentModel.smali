.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oY;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1128b125
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:J

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242797
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242798
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1242765
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242766
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1242799
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242800
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242801
    return-void
.end method

.method public static a(LX/7oY;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;
    .locals 4

    .prologue
    .line 1242802
    if-nez p0, :cond_0

    .line 1242803
    const/4 p0, 0x0

    .line 1242804
    :goto_0
    return-object p0

    .line 1242805
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    if-eqz v0, :cond_1

    .line 1242806
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    goto :goto_0

    .line 1242807
    :cond_1
    new-instance v0, LX/7ow;

    invoke-direct {v0}, LX/7ow;-><init>()V

    .line 1242808
    invoke-interface {p0}, LX/7oY;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/7ow;->a:Z

    .line 1242809
    invoke-interface {p0}, LX/7oY;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->b:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1242810
    invoke-interface {p0}, LX/7oY;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1242811
    invoke-interface {p0}, LX/7oY;->b()J

    move-result-wide v2

    iput-wide v2, v0, LX/7ow;->d:J

    .line 1242812
    invoke-interface {p0}, LX/7oY;->c()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242813
    invoke-interface {p0}, LX/7oY;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1242814
    invoke-interface {p0}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->g:Ljava/lang/String;

    .line 1242815
    invoke-interface {p0}, LX/7oY;->eQ_()Z

    move-result v1

    iput-boolean v1, v0, LX/7ow;->h:Z

    .line 1242816
    invoke-interface {p0}, LX/7oY;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/7ow;->i:Z

    .line 1242817
    invoke-interface {p0}, LX/7oY;->eR_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->j:Ljava/lang/String;

    .line 1242818
    invoke-interface {p0}, LX/7oY;->o()J

    move-result-wide v2

    iput-wide v2, v0, LX/7ow;->k:J

    .line 1242819
    invoke-interface {p0}, LX/7oY;->j()J

    move-result-wide v2

    iput-wide v2, v0, LX/7ow;->l:J

    .line 1242820
    invoke-interface {p0}, LX/7oY;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->m:Ljava/lang/String;

    .line 1242821
    invoke-interface {p0}, LX/7oY;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->n:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1242822
    invoke-interface {p0}, LX/7oY;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/7ow;->o:Z

    .line 1242823
    invoke-interface {p0}, LX/7oY;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/7ow;->p:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1242824
    invoke-virtual {v0}, LX/7ow;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1242825
    iput-wide p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->o:J

    .line 1242826
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242827
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242828
    if-eqz v0, :cond_0

    .line 1242829
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1242830
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1242831
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1242832
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242833
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242834
    if-eqz v0, :cond_0

    .line 1242835
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xd

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1242836
    :cond_0
    return-void

    .line 1242837
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1242838
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1242839
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242840
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242841
    if-eqz v0, :cond_0

    .line 1242842
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0xf

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1242843
    :cond_0
    return-void

    .line 1242844
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1242845
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->n:Ljava/lang/String;

    .line 1242846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242847
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242848
    if-eqz v0, :cond_0

    .line 1242849
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1242850
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1242851
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e:Z

    .line 1242852
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242853
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242854
    if-eqz v0, :cond_0

    .line 1242855
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1242856
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1242894
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->m:Z

    .line 1242895
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242896
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242897
    if-eqz v0, :cond_0

    .line 1242898
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1242899
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1242857
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s:Z

    .line 1242858
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1242859
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1242860
    if-eqz v0, :cond_0

    .line 1242861
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1242862
    :cond_0
    return-void
.end method

.method private t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242900
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1242901
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242931
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242932
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242892
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1242893
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    .line 1242902
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242903
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1242904
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1242905
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1242906
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1242907
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1242908
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1242909
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1242910
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1242911
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1242912
    const/16 v2, 0x10

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1242913
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1242914
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1242915
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1242916
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->h:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242917
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1242918
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1242919
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1242920
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1242921
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1242922
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1242923
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->o:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242924
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->p:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242925
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1242926
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1242927
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1242928
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1242929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242930
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1242874
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242875
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1242876
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1242877
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1242878
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    .line 1242879
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1242880
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1242881
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242882
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1242883
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    .line 1242884
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242885
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1242886
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1242887
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1242888
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    .line 1242889
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1242890
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242891
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1242873
    new-instance v0, LX/7p0;

    invoke-direct {v0, p1}, LX/7p0;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242872
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1242863
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1242864
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e:Z

    .line 1242865
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->h:J

    .line 1242866
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->l:Z

    .line 1242867
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->m:Z

    .line 1242868
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->o:J

    .line 1242869
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->p:J

    .line 1242870
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s:Z

    .line 1242871
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1242767
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242768
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242770
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1242771
    :goto_0
    return-void

    .line 1242772
    :cond_0
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1242773
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242775
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1242776
    :cond_1
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1242777
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242779
    const/16 v0, 0x9

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1242780
    :cond_2
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1242781
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242783
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1242784
    :cond_3
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1242785
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242786
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242787
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1242788
    :cond_4
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1242789
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242791
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1242792
    :cond_5
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1242793
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1242794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1242795
    const/16 v0, 0xf

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1242796
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1242716
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1242717
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->a(Z)V

    .line 1242718
    :cond_0
    :goto_0
    return-void

    .line 1242719
    :cond_1
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1242720
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->b(Z)V

    goto :goto_0

    .line 1242721
    :cond_2
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1242722
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1242723
    :cond_3
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1242724
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->a(J)V

    goto :goto_0

    .line 1242725
    :cond_4
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1242726
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto :goto_0

    .line 1242727
    :cond_5
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1242728
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->c(Z)V

    goto :goto_0

    .line 1242729
    :cond_6
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242730
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1242733
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242734
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->h:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1242735
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;-><init>()V

    .line 1242736
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242737
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242738
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242739
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1242740
    const v0, 0x784e9a44

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242741
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->k:Ljava/lang/String;

    .line 1242742
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1242743
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242744
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->l:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242745
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->n:Ljava/lang/String;

    .line 1242746
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1242747
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1242748
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242749
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->p:J

    return-wide v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1242750
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242751
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->e:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242752
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1242753
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242754
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1242755
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242756
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->m:Z

    return v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 1242757
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242758
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->o:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242731
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->q:Ljava/lang/String;

    .line 1242732
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242759
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1242760
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->r:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1242761
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1242762
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->s:Z

    return v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242763
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1242764
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;->t:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method
