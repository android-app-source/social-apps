.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x435932a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1243123
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1243097
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1243126
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1243127
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243124
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->e:Ljava/lang/String;

    .line 1243125
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1243105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1243106
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1243107
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1243108
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1243109
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1243110
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1243111
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1243112
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1243113
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1243114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1243115
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1243116
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1243117
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1243118
    if-eqz v1, :cond_0

    .line 1243119
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    .line 1243120
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->g:Ljava/util/List;

    .line 1243121
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1243122
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243103
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->f:Ljava/lang/String;

    .line 1243104
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1243100
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;-><init>()V

    .line 1243101
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1243102
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1243099
    const v0, 0x5734aa8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1243098
    const v0, 0x6e6d6162

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1243095
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->g:Ljava/util/List;

    .line 1243096
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
