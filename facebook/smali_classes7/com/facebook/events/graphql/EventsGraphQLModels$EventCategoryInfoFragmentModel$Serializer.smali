.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1243130
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1243131
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1243132
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1243133
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1243134
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1243135
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1243136
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1243137
    if-eqz v2, :cond_0

    .line 1243138
    const-string p0, "event_category_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1243139
    invoke-static {v1, v2, p1, p2}, LX/7r0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1243140
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1243141
    if-eqz v2, :cond_1

    .line 1243142
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1243143
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1243144
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1243145
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1243146
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
