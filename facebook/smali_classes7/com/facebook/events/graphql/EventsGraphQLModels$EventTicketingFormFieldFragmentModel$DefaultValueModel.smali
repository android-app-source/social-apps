.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7on;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4c11943a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253456
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253455
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1253453
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1253454
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253451
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->e:Ljava/lang/String;

    .line 1253452
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253449
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->f:Ljava/lang/String;

    .line 1253450
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253447
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->g:Ljava/lang/String;

    .line 1253448
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253445
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->h:Ljava/lang/String;

    .line 1253446
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253443
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->i:Ljava/lang/String;

    .line 1253444
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253397
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->k:Ljava/lang/String;

    .line 1253398
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253441
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->l:Ljava/lang/String;

    .line 1253442
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253439
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->m:Ljava/lang/String;

    .line 1253440
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253437
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->o:Ljava/lang/String;

    .line 1253438
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->o:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 1253411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253412
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1253413
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1253414
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1253415
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1253416
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1253417
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1253418
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1253419
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1253420
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1253421
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1253422
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->r()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1253423
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1253424
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1253425
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1253426
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1253427
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1253428
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1253429
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1253430
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1253431
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1253432
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1253433
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1253434
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1253435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253436
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1253408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253410
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253406
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->j:Ljava/lang/String;

    .line 1253407
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1253403
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;-><init>()V

    .line 1253404
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1253405
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253401
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->n:Ljava/lang/String;

    .line 1253402
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1253400
    const v0, 0x69f707b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1253399
    const v0, 0x67c6efbd

    return v0
.end method
