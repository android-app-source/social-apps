.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oa;
.implements LX/7oj;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x60c6e45b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:I

.field private W:I

.field private X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aA:I

.field private aB:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aC:Z

.field private aD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aF:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ae:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:Z

.field private ak:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private an:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ao:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:I

.field private aq:Z

.field private ar:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:J

.field private au:J

.field private av:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aw:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ax:Z

.field private ay:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:J

.field private s:J

.field private t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1257004
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1256991
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1256989
    const/16 v0, 0x50

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1256990
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1256986
    const/16 v0, 0x50

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1256987
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1256988
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1256980
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA:I

    .line 1256981
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256982
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256983
    if-eqz v0, :cond_0

    .line 1256984
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1256985
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1256974
    iput-wide p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at:J

    .line 1256975
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256976
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256977
    if-eqz v0, :cond_0

    .line 1256978
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x43

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1256979
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1256967
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1256968
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256969
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256970
    if-eqz v0, :cond_0

    .line 1256971
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x4b

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1256972
    :cond_0
    return-void

    .line 1256973
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1256960
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1256961
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256962
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256963
    if-eqz v0, :cond_0

    .line 1256964
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x4f

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1256965
    :cond_0
    return-void

    .line 1256966
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1256954
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an:Ljava/lang/String;

    .line 1256955
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256956
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256957
    if-eqz v0, :cond_0

    .line 1256958
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x3d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1256959
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1256948
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l:Z

    .line 1256949
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256950
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256951
    if-eqz v0, :cond_0

    .line 1256952
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1256953
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1256942
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ag:Z

    .line 1256943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256944
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256945
    if-eqz v0, :cond_0

    .line 1256946
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1256947
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1256936
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah:Z

    .line 1256937
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256938
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256939
    if-eqz v0, :cond_0

    .line 1256940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1256941
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1256930
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC:Z

    .line 1256931
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1256932
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1256933
    if-eqz v0, :cond_0

    .line 1256934
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x4c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1256935
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256929
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256927
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->v:Ljava/lang/String;

    .line 1256928
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()LX/7oW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256903
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()LX/7oc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256924
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256923
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256922
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256920
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1256921
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final synthetic H()LX/7od;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256919
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic I()LX/7oe;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256918
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256916
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->N:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->N:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1256917
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->N:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256914
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1256915
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->O:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256912
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->U:Lcom/facebook/graphql/enums/GraphQLEventType;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->U:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 1256913
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->U:Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method

.method public final synthetic M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256911
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256909
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 1256910
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method

.method public final synthetic O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256908
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256907
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256906
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 1256904
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256905
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ag:Z

    return v0
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 1256996
    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256997
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj:Z

    return v0
.end method

.method public final synthetic T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257027
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final U()Z
    .locals 2

    .prologue
    .line 1257028
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1257029
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq:Z

    return v0
.end method

.method public final synthetic V()LX/1oP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257030
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257031
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final X()I
    .locals 2

    .prologue
    .line 1257032
    const/16 v0, 0x9

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1257033
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA:I

    return v0
.end method

.method public final Y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1257025
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD:Ljava/util/List;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD:Ljava/util/List;

    .line 1257026
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257036
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1257037
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 60

    .prologue
    .line 1257038
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1257039
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1257040
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1257041
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1257042
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1257043
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1257044
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1257045
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1257046
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1257047
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1257048
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1257049
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1257050
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1257051
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1257052
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1257053
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1257054
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1257055
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1257056
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1257057
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1257058
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1257059
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1257060
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1257061
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1257062
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1257063
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1257064
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1257065
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v28

    .line 1257066
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    .line 1257067
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1257068
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1257069
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1257070
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1257071
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 1257072
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    .line 1257073
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 1257074
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v37

    .line 1257075
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1257076
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1257077
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1257078
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1257079
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1257080
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 1257081
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aS()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1257082
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1257083
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1257084
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 1257085
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 1257086
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 1257087
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1257088
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1257089
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1257090
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1257091
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 1257092
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v55

    .line 1257093
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y()LX/0Px;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v56

    .line 1257094
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v57

    .line 1257095
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v58

    .line 1257096
    const/16 v59, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1257097
    const/16 v59, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1257098
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1257099
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1257100
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1257101
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257102
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257103
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257104
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257105
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->m:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257106
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257107
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1257108
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1257109
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1257110
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1257111
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1257112
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1257113
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1257114
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1257115
    const/16 v2, 0x12

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->w:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1257116
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1257117
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1257118
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1257119
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1257120
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257121
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257122
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257123
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257124
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257125
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257126
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257127
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257128
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257129
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257130
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257131
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257132
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257133
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257134
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257135
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257136
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257137
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257138
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257139
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257140
    const/16 v2, 0x2b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->V:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1257141
    const/16 v2, 0x2c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->W:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1257142
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257143
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257144
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257145
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257146
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257147
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257148
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257149
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257150
    const/16 v2, 0x35

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->af:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257151
    const/16 v2, 0x36

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ag:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257152
    const/16 v2, 0x37

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257153
    const/16 v2, 0x38

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ai:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257154
    const/16 v2, 0x39

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257155
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257156
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257157
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257158
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257159
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257160
    const/16 v2, 0x3f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1257161
    const/16 v2, 0x40

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257162
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257163
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257164
    const/16 v3, 0x43

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1257165
    const/16 v3, 0x44

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1257166
    const/16 v2, 0x45

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257167
    const/16 v2, 0x46

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257168
    const/16 v2, 0x47

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257169
    const/16 v2, 0x48

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257170
    const/16 v2, 0x49

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257171
    const/16 v2, 0x4a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1257172
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257173
    const/16 v2, 0x4c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1257174
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257175
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257176
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1257177
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1257178
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1257179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1257180
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1257181
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1257182
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1257183
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257184
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1257185
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1257186
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    .line 1257187
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1257188
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257189
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    .line 1257190
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1257191
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 1257192
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1257193
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257194
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 1257195
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1257196
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1257197
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1257198
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257199
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1257200
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1257201
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1257202
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1257203
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257204
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1257205
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1257206
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1257207
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1257208
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257209
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1257210
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1257211
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1257212
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1257213
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257214
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1257215
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1257216
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    .line 1257217
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1257218
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257219
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    .line 1257220
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1257221
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1257222
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1257223
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257224
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1257225
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1257226
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    .line 1257227
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1257228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257229
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    .line 1257230
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1257231
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1257232
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1257233
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257234
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1257235
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1257236
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    .line 1257237
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1257238
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257239
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    .line 1257240
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1257241
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    .line 1257242
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1257243
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257244
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    .line 1257245
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1257246
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    .line 1257247
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1257248
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257249
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    .line 1257250
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1257251
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    .line 1257252
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1257253
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257254
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    .line 1257255
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1257256
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1257257
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1257258
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257259
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1257260
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1257261
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    .line 1257262
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1257263
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257264
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    .line 1257265
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1257266
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    .line 1257267
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1257268
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257269
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    .line 1257270
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1257271
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    .line 1257272
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1257273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257274
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    .line 1257275
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1257276
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1257277
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1257278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257279
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1257280
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1257281
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    .line 1257282
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 1257283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257284
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    .line 1257285
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1257286
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    .line 1257287
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 1257288
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257289
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    .line 1257290
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 1257291
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    .line 1257292
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 1257293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257294
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    .line 1257295
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 1257296
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    .line 1257297
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 1257298
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257299
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->S:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    .line 1257300
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1257301
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1257302
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 1257303
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257304
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1257305
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 1257306
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    .line 1257307
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 1257308
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257309
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    .line 1257310
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 1257311
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    .line 1257312
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 1257313
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257314
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    .line 1257315
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 1257316
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1257317
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 1257318
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257319
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1257320
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 1257321
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1257322
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 1257323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257324
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1257325
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 1257326
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1257327
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 1257328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257329
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1257330
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 1257331
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1257332
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 1257333
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257334
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1257335
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 1257336
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1257337
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 1257338
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257339
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1257340
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 1257341
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1257342
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 1257343
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257344
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1257345
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 1257346
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1257347
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 1257348
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257349
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1257350
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 1257351
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1257352
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 1257353
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257354
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1257355
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 1257356
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1257357
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 1257358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257359
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1257360
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 1257361
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    .line 1257362
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 1257363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257364
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    .line 1257365
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 1257366
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1257367
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 1257368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257369
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1257370
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 1257371
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1257372
    if-eqz v2, :cond_26

    .line 1257373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1257374
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD:Ljava/util/List;

    move-object v1, v0

    .line 1257375
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1257376
    if-nez v1, :cond_27

    :goto_0
    return-object p0

    :cond_27
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1257377
    new-instance v0, LX/7qO;

    invoke-direct {v0, p1}, LX/7qO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257378
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1257379
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1257380
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->i:Z

    .line 1257381
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->j:Z

    .line 1257382
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->k:Z

    .line 1257383
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l:Z

    .line 1257384
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->m:Z

    .line 1257385
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->n:Z

    .line 1257386
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r:J

    .line 1257387
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s:J

    .line 1257388
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->w:I

    .line 1257389
    const/16 v0, 0x2b

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->V:I

    .line 1257390
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->W:I

    .line 1257391
    const/16 v0, 0x35

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->af:Z

    .line 1257392
    const/16 v0, 0x36

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ag:Z

    .line 1257393
    const/16 v0, 0x37

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah:Z

    .line 1257394
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ai:Z

    .line 1257395
    const/16 v0, 0x39

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj:Z

    .line 1257396
    const/16 v0, 0x3f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap:I

    .line 1257397
    const/16 v0, 0x40

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq:Z

    .line 1257398
    const/16 v0, 0x43

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at:J

    .line 1257399
    const/16 v0, 0x44

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au:J

    .line 1257400
    const/16 v0, 0x47

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax:Z

    .line 1257401
    const/16 v0, 0x4a

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA:I

    .line 1257402
    const/16 v0, 0x4c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC:Z

    .line 1257403
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1257404
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1257405
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257407
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    .line 1257408
    :goto_0
    return-void

    .line 1257409
    :cond_0
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1257410
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v0

    .line 1257411
    if-eqz v0, :cond_d

    .line 1257412
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257413
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257414
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1257415
    :cond_1
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1257416
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    .line 1257417
    if-eqz v0, :cond_d

    .line 1257418
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257419
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257420
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1257421
    :cond_2
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1257422
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    .line 1257423
    if-eqz v0, :cond_d

    .line 1257424
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257425
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257426
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1257427
    :cond_3
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1257428
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    .line 1257429
    if-eqz v0, :cond_d

    .line 1257430
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257431
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257432
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257433
    :cond_4
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1257434
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    .line 1257435
    if-eqz v0, :cond_d

    .line 1257436
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257437
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257438
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257439
    :cond_5
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1257440
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257442
    const/16 v0, 0x36

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257443
    :cond_6
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1257444
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257446
    const/16 v0, 0x37

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257447
    :cond_7
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1257448
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257449
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257450
    const/16 v0, 0x3d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257451
    :cond_8
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1257452
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257454
    const/16 v0, 0x43

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257455
    :cond_9
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1257456
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257458
    const/16 v0, 0x4a

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257459
    :cond_a
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1257460
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257462
    const/16 v0, 0x4b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257463
    :cond_b
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1257464
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257466
    const/16 v0, 0x4c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257467
    :cond_c
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1257468
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1257469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1257470
    const/16 v0, 0x4f

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1257471
    :cond_d
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1257472
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1257473
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(Z)V

    .line 1257474
    :cond_0
    :goto_0
    return-void

    .line 1257475
    :cond_1
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1257476
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v0

    .line 1257477
    if-eqz v0, :cond_0

    .line 1257478
    if-eqz p3, :cond_2

    .line 1257479
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    .line 1257480
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;->a(I)V

    .line 1257481
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    goto :goto_0

    .line 1257482
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1257483
    :cond_3
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1257484
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    .line 1257485
    if-eqz v0, :cond_0

    .line 1257486
    if-eqz p3, :cond_4

    .line 1257487
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    .line 1257488
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a(I)V

    .line 1257489
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    goto :goto_0

    .line 1257490
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a(I)V

    goto :goto_0

    .line 1257491
    :cond_5
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1257492
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v0

    .line 1257493
    if-eqz v0, :cond_0

    .line 1257494
    if-eqz p3, :cond_6

    .line 1257495
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    .line 1257496
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a(I)V

    .line 1257497
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    goto :goto_0

    .line 1257498
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a(I)V

    goto/16 :goto_0

    .line 1257499
    :cond_7
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1257500
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    .line 1257501
    if-eqz v0, :cond_0

    .line 1257502
    if-eqz p3, :cond_8

    .line 1257503
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    .line 1257504
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a(I)V

    .line 1257505
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    goto/16 :goto_0

    .line 1257506
    :cond_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a(I)V

    goto/16 :goto_0

    .line 1257507
    :cond_9
    const-string v0, "event_watchers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1257508
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v0

    .line 1257509
    if-eqz v0, :cond_0

    .line 1257510
    if-eqz p3, :cond_a

    .line 1257511
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    .line 1257512
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;->a(I)V

    .line 1257513
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    goto/16 :goto_0

    .line 1257514
    :cond_a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;->a(I)V

    goto/16 :goto_0

    .line 1257515
    :cond_b
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1257516
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->b(Z)V

    goto/16 :goto_0

    .line 1257517
    :cond_c
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1257518
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->c(Z)V

    goto/16 :goto_0

    .line 1257519
    :cond_d
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1257520
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1257521
    :cond_e
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1257522
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(J)V

    goto/16 :goto_0

    .line 1257523
    :cond_f
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1257524
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(I)V

    goto/16 :goto_0

    .line 1257525
    :cond_10
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1257526
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 1257527
    :cond_11
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1257528
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->d(Z)V

    goto/16 :goto_0

    .line 1257529
    :cond_12
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1257530
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257034
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    .line 1257035
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    return-object v0
.end method

.method public final aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257023
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    .line 1257024
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    return-object v0
.end method

.method public final aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257021
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1257022
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    return-object v0
.end method

.method public final aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256994
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    .line 1256995
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    return-object v0
.end method

.method public final aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257019
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    .line 1257020
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    return-object v0
.end method

.method public final aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257017
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    .line 1257018
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    return-object v0
.end method

.method public final aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257015
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1257016
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method public final aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257013
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    .line 1257014
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    return-object v0
.end method

.method public final aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257011
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    .line 1257012
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    return-object v0
.end method

.method public final aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257009
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    .line 1257010
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    return-object v0
.end method

.method public final aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257007
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->S:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->S:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    .line 1257008
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->S:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    return-object v0
.end method

.method public final aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257005
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1257006
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    return-object v0
.end method

.method public final aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257002
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    .line 1257003
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    return-object v0
.end method

.method public final aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257000
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    .line 1257001
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aa:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    return-object v0
.end method

.method public final aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256998
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1256999
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    return-object v0
.end method

.method public final aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256925
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1256926
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    return-object v0
.end method

.method public final aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256992
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    const/16 v1, 0x33

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1256993
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    return-object v0
.end method

.method public final aR()Z
    .locals 2

    .prologue
    .line 1256845
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256846
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ai:Z

    return v0
.end method

.method public final aS()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256843
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak:Ljava/lang/String;

    const/16 v1, 0x3a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak:Ljava/lang/String;

    .line 1256844
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak:Ljava/lang/String;

    return-object v0
.end method

.method public final aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256841
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1256842
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    return-object v0
.end method

.method public final aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256839
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1256840
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    return-object v0
.end method

.method public final aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256837
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    const/16 v1, 0x3e

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1256838
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    return-object v0
.end method

.method public final aW()I
    .locals 1

    .prologue
    const/4 v0, 0x7

    .line 1256849
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256850
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap:I

    return v0
.end method

.method public final aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256835
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1256836
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    return-object v0
.end method

.method public final aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256833
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1256834
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    return-object v0
.end method

.method public final aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256831
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    const/16 v1, 0x45

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1256832
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    return-object v0
.end method

.method public final synthetic aa()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256830
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v0

    return-object v0
.end method

.method public final ab()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1256828
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256829
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->n:Z

    return v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256826
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->x:Ljava/lang/String;

    .line 1256827
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final ad()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256824
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->T:Ljava/lang/String;

    .line 1256825
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final ae()I
    .locals 2

    .prologue
    .line 1256822
    const/4 v0, 0x5

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256823
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->V:I

    return v0
.end method

.method public final af()I
    .locals 2

    .prologue
    .line 1256820
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256821
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->W:I

    return v0
.end method

.method public final synthetic ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256819
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ah()LX/2rX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256818
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final ai()Z
    .locals 2

    .prologue
    .line 1256787
    const/16 v0, 0x8

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256788
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax:Z

    return v0
.end method

.method public final synthetic aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256799
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    return-object v0
.end method

.method public final ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256797
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1256798
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    return-object v0
.end method

.method public final al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256795
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    .line 1256796
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    return-object v0
.end method

.method public final am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256789
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 1256790
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    return-object v0
.end method

.method public final an()Z
    .locals 2

    .prologue
    .line 1256791
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256792
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->k:Z

    return v0
.end method

.method public final ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256793
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1256794
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    return-object v0
.end method

.method public final ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256802
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1256803
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    return-object v0
.end method

.method public final aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256804
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1256805
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    return-object v0
.end method

.method public final ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256806
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1256807
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->u:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final as()I
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 1256808
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256809
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->w:I

    return v0
.end method

.method public final at()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256810
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->y:Ljava/lang/String;

    .line 1256811
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256812
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    .line 1256813
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    return-object v0
.end method

.method public final av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256814
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1256815
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    return-object v0
.end method

.method public final aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256800
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    .line 1256801
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    return-object v0
.end method

.method public final ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256876
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1256877
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    return-object v0
.end method

.method public final ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256901
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    .line 1256902
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    return-object v0
.end method

.method public final az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256899
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    .line 1256900
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1256897
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256898
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1256894
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;-><init>()V

    .line 1256895
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1256896
    return-object v0
.end method

.method public final ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256892
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    const/16 v1, 0x46

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    .line 1256893
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    return-object v0
.end method

.method public final bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256890
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    const/16 v1, 0x48

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1256891
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256889
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256888
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1256887
    const v0, 0x63b4a2c4

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256885
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ae:Ljava/lang/String;

    const/16 v1, 0x34

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ae:Ljava/lang/String;

    .line 1256886
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1256883
    const/4 v0, 0x6

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256884
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->af:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256881
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an:Ljava/lang/String;

    const/16 v1, 0x3d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an:Ljava/lang/String;

    .line 1256882
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1256880
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1256878
    const/16 v0, 0x8

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256879
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au:J

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1256847
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256848
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256874
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1256875
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256873
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1256871
    const/4 v0, 0x6

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256872
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ah:Z

    return v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 1256869
    const/16 v0, 0x8

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256870
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256816
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az:Ljava/lang/String;

    const/16 v1, 0x49

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az:Ljava/lang/String;

    .line 1256817
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256867
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x4b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1256868
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1256865
    const/16 v0, 0x9

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256866
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC:Z

    return v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256863
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1256864
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256861
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1256862
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256860
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 1256858
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256859
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->i:Z

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1256856
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256857
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->j:Z

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 1256854
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256855
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->m:Z

    return v0
.end method

.method public final synthetic y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256853
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()J
    .locals 2

    .prologue
    .line 1256851
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1256852
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r:J

    return-wide v0
.end method
