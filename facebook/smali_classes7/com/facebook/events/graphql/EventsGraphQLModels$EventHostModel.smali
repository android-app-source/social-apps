.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7ob;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xf26c296
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1247030
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1247031
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1247032
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1247033
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1247034
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1247035
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1247036
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;
    .locals 13

    .prologue
    .line 1247037
    if-nez p0, :cond_0

    .line 1247038
    const/4 p0, 0x0

    .line 1247039
    :goto_0
    return-object p0

    .line 1247040
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    if-eqz v0, :cond_1

    .line 1247041
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    goto :goto_0

    .line 1247042
    :cond_1
    new-instance v0, LX/7pV;

    invoke-direct {v0}, LX/7pV;-><init>()V

    .line 1247043
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1247044
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1247045
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->c:Ljava/lang/String;

    .line 1247046
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->d:Ljava/lang/String;

    .line 1247047
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1247048
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->eP_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pV;->f:Ljava/lang/String;

    .line 1247049
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/7pV;->g:Z

    .line 1247050
    const/4 v6, 0x1

    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 1247051
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1247052
    iget-object v3, v0, LX/7pV;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1247053
    iget-object v5, v0, LX/7pV;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1247054
    iget-object v7, v0, LX/7pV;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1247055
    iget-object v8, v0, LX/7pV;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1247056
    iget-object v9, v0, LX/7pV;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1247057
    iget-object v10, v0, LX/7pV;->f:Ljava/lang/String;

    invoke-virtual {v2, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1247058
    const/4 v11, 0x7

    invoke-virtual {v2, v11}, LX/186;->c(I)V

    .line 1247059
    invoke-virtual {v2, v12, v3}, LX/186;->b(II)V

    .line 1247060
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1247061
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1247062
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1247063
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1247064
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1247065
    const/4 v3, 0x6

    iget-boolean v5, v0, LX/7pV;->g:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 1247066
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1247067
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1247068
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1247069
    invoke-virtual {v3, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1247070
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1247071
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;-><init>(LX/15i;)V

    .line 1247072
    move-object p0, v3

    .line 1247073
    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1247074
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1247075
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1247076
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1247077
    if-eqz v0, :cond_0

    .line 1247078
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1247079
    :cond_0
    return-void

    .line 1247080
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247003
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1247004
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1247081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1247082
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1247083
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1247084
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1247085
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1247086
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1247087
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->eP_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1247088
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1247089
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1247090
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1247091
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1247092
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1247093
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1247094
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1247095
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1247096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1247097
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1247098
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1247099
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1247100
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1247101
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1247102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    .line 1247103
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1247104
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1247105
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1247106
    new-instance v0, LX/7pW;

    invoke-direct {v0, p1}, LX/7pW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247107
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1247027
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1247028
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k:Z

    .line 1247029
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1247108
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1247109
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1247110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1247111
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1247112
    :goto_0
    return-void

    .line 1247113
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1247024
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1247025
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1247026
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247021
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1247022
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1247023
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1247018
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;-><init>()V

    .line 1247019
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1247020
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247016
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->g:Ljava/lang/String;

    .line 1247017
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247014
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->h:Ljava/lang/String;

    .line 1247015
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1247013
    const v0, -0x2d91744e

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247012
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final eP_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247010
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->j:Ljava/lang/String;

    .line 1247011
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final eU_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247008
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1247009
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1247007
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1247005
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1247006
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventHostModel;->k:Z

    return v0
.end method
