.class public final Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1239214
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1239215
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1239216
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1239217
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1239218
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1239219
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1239220
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239221
    if-eqz v2, :cond_0

    .line 1239222
    const-string p0, "eventProfilePicture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239223
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1239224
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239225
    if-eqz v2, :cond_1

    .line 1239226
    const-string p0, "event_place"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239227
    invoke-static {v1, v2, p1}, LX/7nV;->a(LX/15i;ILX/0nX;)V

    .line 1239228
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239229
    if-eqz v2, :cond_2

    .line 1239230
    const-string p0, "friends_going"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239231
    invoke-static {v1, v2, p1, p2}, LX/7nS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1239232
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239233
    if-eqz v2, :cond_3

    .line 1239234
    const-string p0, "friends_interested"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239235
    invoke-static {v1, v2, p1, p2}, LX/7nT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1239236
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239237
    if-eqz v2, :cond_4

    .line 1239238
    const-string p0, "friends_invited"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239239
    invoke-static {v1, v2, p1, p2}, LX/7nU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1239240
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239241
    if-eqz v2, :cond_5

    .line 1239242
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239243
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239244
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239245
    if-eqz v2, :cond_6

    .line 1239246
    const-string p0, "live_permalink_time_range_sentence"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239247
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239248
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239249
    if-eqz v2, :cond_7

    .line 1239250
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239251
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239252
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1239253
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1239254
    check-cast p1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
