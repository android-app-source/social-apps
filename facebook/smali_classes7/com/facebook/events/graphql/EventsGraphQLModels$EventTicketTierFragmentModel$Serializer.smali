.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1252357
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1252358
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1252360
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1252361
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1252362
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1252363
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1252364
    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1252365
    if-eqz v2, :cond_0

    .line 1252366
    const-string v3, "event_buy_ticket_display_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252367
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1252368
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252369
    if-eqz v2, :cond_1

    .line 1252370
    const-string v3, "event_ticket_provider"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252371
    invoke-static {v1, v2, p1, p2}, LX/7sf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252372
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1252373
    if-eqz v2, :cond_2

    .line 1252374
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252375
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1252376
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1252377
    if-eqz v2, :cond_3

    .line 1252378
    const-string v3, "is_official"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252379
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1252380
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1252381
    if-eqz v2, :cond_4

    .line 1252382
    const-string v3, "supports_multi_tier_purchase"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252383
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1252384
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252385
    if-eqz v2, :cond_5

    .line 1252386
    const-string v3, "ticket_seat_selection_note"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252387
    invoke-static {v1, v2, p1}, LX/7sg;->a(LX/15i;ILX/0nX;)V

    .line 1252388
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252389
    if-eqz v2, :cond_6

    .line 1252390
    const-string v3, "ticket_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252391
    invoke-static {v1, v2, p1, p2}, LX/7sj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252392
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1252393
    if-eqz v2, :cond_7

    .line 1252394
    const-string v3, "ticket_tiers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252395
    invoke-static {v1, v2, p1, p2}, LX/7sn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1252396
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1252397
    if-eqz v2, :cond_8

    .line 1252398
    const-string v3, "total_purchased_tickets"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1252399
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1252400
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1252401
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1252359
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
