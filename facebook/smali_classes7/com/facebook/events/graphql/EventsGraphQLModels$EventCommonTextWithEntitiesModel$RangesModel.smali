.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/1W5;
.implements LX/0jT;
.implements LX/1eF;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x563cf7a0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244577
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244631
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1244629
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244630
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1244626
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244627
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244628
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;
    .locals 2

    .prologue
    .line 1244616
    if-nez p0, :cond_0

    .line 1244617
    const/4 p0, 0x0

    .line 1244618
    :goto_0
    return-object p0

    .line 1244619
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    if-eqz v0, :cond_1

    .line 1244620
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    goto :goto_0

    .line 1244621
    :cond_1
    new-instance v0, LX/7pD;

    invoke-direct {v0}, LX/7pD;-><init>()V

    .line 1244622
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v1

    iput-object v1, v0, LX/7pD;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244623
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->b()I

    move-result v1

    iput v1, v0, LX/7pD;->b:I

    .line 1244624
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->c()I

    move-result v1

    iput v1, v0, LX/7pD;->c:I

    .line 1244625
    invoke-virtual {v0}, LX/7pD;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    move-result-object p0

    goto :goto_0
.end method

.method private l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244614
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244615
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1244606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244607
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1244608
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1244609
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1244610
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1244611
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1244612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244613
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1244598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244599
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1244600
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244601
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1244602
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    .line 1244603
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244604
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244605
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/171;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244597
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1244593
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1244594
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->f:I

    .line 1244595
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->g:I

    .line 1244596
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1244591
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244592
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1244588
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;-><init>()V

    .line 1244589
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244590
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1244586
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244587
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->g:I

    return v0
.end method

.method public final synthetic d()LX/1y9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244585
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1244584
    const v0, 0x73d8a811

    return v0
.end method

.method public final synthetic e()LX/1yC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244583
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eL_()LX/1yH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244582
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eM_()LX/1yI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244581
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1244580
    const v0, -0x3d10ccb9

    return v0
.end method

.method public final synthetic j()LX/1yK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244579
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244578
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method
