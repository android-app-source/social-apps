.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249669
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1249670
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249671
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1249672
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1249673
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1249674
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249675
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1249676
    if-eqz v2, :cond_2

    .line 1249677
    const-string p0, "event_sms_invitees"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249678
    const/4 p0, 0x0

    .line 1249679
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249680
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1249681
    if-eqz p0, :cond_0

    .line 1249682
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249683
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1249684
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1249685
    if-eqz p0, :cond_1

    .line 1249686
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249687
    invoke-static {v1, p0, p1, p2}, LX/7s1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1249688
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249689
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249690
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1249691
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
