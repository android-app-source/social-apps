.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c488d66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250529
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250528
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1250526
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250527
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1250523
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250524
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250525
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 10

    .prologue
    .line 1250497
    if-nez p0, :cond_0

    .line 1250498
    const/4 p0, 0x0

    .line 1250499
    :goto_0
    return-object p0

    .line 1250500
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    if-eqz v0, :cond_1

    .line 1250501
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    goto :goto_0

    .line 1250502
    :cond_1
    new-instance v2, LX/7py;

    invoke-direct {v2}, LX/7py;-><init>()V

    .line 1250503
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1250504
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1250505
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1250506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1250507
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/7py;->a:LX/0Px;

    .line 1250508
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v0

    iput v0, v2, LX/7py;->b:I

    .line 1250509
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 1250510
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1250511
    iget-object v5, v2, LX/7py;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1250512
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 1250513
    invoke-virtual {v4, v9, v5}, LX/186;->b(II)V

    .line 1250514
    iget v5, v2, LX/7py;->b:I

    invoke-virtual {v4, v8, v5, v9}, LX/186;->a(III)V

    .line 1250515
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1250516
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1250517
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1250518
    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1250519
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1250520
    new-instance v5, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-direct {v5, v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;-><init>(LX/15i;)V

    .line 1250521
    move-object p0, v5

    .line 1250522
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1250490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250491
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1250492
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1250493
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1250494
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1250495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250496
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1250470
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->e:Ljava/util/List;

    .line 1250471
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1250482
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250483
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1250484
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1250485
    if-eqz v1, :cond_0

    .line 1250486
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1250487
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->e:Ljava/util/List;

    .line 1250488
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250489
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1250479
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1250480
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->f:I

    .line 1250481
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1250477
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1250478
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1250474
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;-><init>()V

    .line 1250475
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250476
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1250473
    const v0, -0xb4d0131

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1250472
    const v0, -0x6e31e683

    return v0
.end method
