.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249319
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249318
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1249316
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1249317
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1249307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249308
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1249309
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1249310
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1249311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249312
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1249313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249315
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249320
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1249300
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;-><init>()V

    .line 1249301
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1249302
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1249303
    const v0, 0x3c68ba7e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1249304
    const v0, -0x66364096

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249305
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->e:Ljava/lang/String;

    .line 1249306
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$EventTicketsModel$NodesModel$FbqrcodeModel;->e:Ljava/lang/String;

    return-object v0
.end method
