.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6738dcef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1246953
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1246952
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1246950
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1246951
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246948
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->i:Ljava/lang/String;

    .line 1246949
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1246934
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1246935
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1246936
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1246937
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1246938
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1246939
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1246940
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1246941
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1246942
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1246943
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1246944
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1246945
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1246946
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1246947
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1246909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1246910
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1246911
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    .line 1246912
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1246913
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    .line 1246914
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    .line 1246915
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1246916
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    .line 1246917
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1246918
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    .line 1246919
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    .line 1246920
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1246921
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    .line 1246922
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1246923
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    .line 1246924
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    .line 1246925
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1246926
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    .line 1246927
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1246928
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    .line 1246929
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    .line 1246930
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1246931
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1246933
    new-instance v0, LX/7pU;

    invoke-direct {v0, p1}, LX/7pU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246932
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1246954
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1246955
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v0

    .line 1246956
    if-eqz v0, :cond_3

    .line 1246957
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1246958
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1246959
    iput v2, p2, LX/18L;->c:I

    .line 1246960
    :goto_0
    return-void

    .line 1246961
    :cond_0
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1246962
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v0

    .line 1246963
    if-eqz v0, :cond_3

    .line 1246964
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1246965
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1246966
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1246967
    :cond_1
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1246968
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v0

    .line 1246969
    if-eqz v0, :cond_3

    .line 1246970
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1246971
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1246972
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1246973
    :cond_2
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1246974
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v0

    .line 1246975
    if-eqz v0, :cond_3

    .line 1246976
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1246977
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1246978
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1246979
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1246863
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1246864
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    move-result-object v0

    .line 1246865
    if-eqz v0, :cond_0

    .line 1246866
    if-eqz p3, :cond_1

    .line 1246867
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    .line 1246868
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;->a(I)V

    .line 1246869
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    .line 1246870
    :cond_0
    :goto_0
    return-void

    .line 1246871
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1246872
    :cond_2
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1246873
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    move-result-object v0

    .line 1246874
    if-eqz v0, :cond_0

    .line 1246875
    if-eqz p3, :cond_3

    .line 1246876
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    .line 1246877
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;->a(I)V

    .line 1246878
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    goto :goto_0

    .line 1246879
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;->a(I)V

    goto :goto_0

    .line 1246880
    :cond_4
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1246881
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    move-result-object v0

    .line 1246882
    if-eqz v0, :cond_0

    .line 1246883
    if-eqz p3, :cond_5

    .line 1246884
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    .line 1246885
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;->a(I)V

    .line 1246886
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    goto :goto_0

    .line 1246887
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1246888
    :cond_6
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1246889
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    move-result-object v0

    .line 1246890
    if-eqz v0, :cond_0

    .line 1246891
    if-eqz p3, :cond_7

    .line 1246892
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    .line 1246893
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;->a(I)V

    .line 1246894
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    goto/16 :goto_0

    .line 1246895
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1246896
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;-><init>()V

    .line 1246897
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1246898
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1246899
    const v0, -0x22cfabee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1246900
    const v0, 0x403827a

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246901
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    .line 1246902
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventDeclinesModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246903
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    .line 1246904
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventInviteesModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246905
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    .line 1246906
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMaybesModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246907
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    .line 1246908
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$EventMembersModel;

    return-object v0
.end method
