.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249968
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1249969
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1250014
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1249970
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1249971
    const/4 v2, 0x0

    .line 1249972
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 1249973
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249974
    :goto_0
    move v1, v2

    .line 1249975
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1249976
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1249977
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;-><init>()V

    .line 1249978
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1249979
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1249980
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1249981
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1249982
    :cond_0
    return-object v1

    .line 1249983
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249984
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_a

    .line 1249985
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1249986
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249987
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1249988
    const-string p0, "event_declines"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1249989
    invoke-static {p1, v0}, LX/7s4;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1249990
    :cond_3
    const-string p0, "event_maybes"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1249991
    invoke-static {p1, v0}, LX/7s5;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1249992
    :cond_4
    const-string p0, "event_members"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1249993
    invoke-static {p1, v0}, LX/7s6;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1249994
    :cond_5
    const-string p0, "friendEventMaybesFirst5"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1249995
    invoke-static {p1, v0}, LX/7s8;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1249996
    :cond_6
    const-string p0, "friendEventMembersFirst5"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1249997
    invoke-static {p1, v0}, LX/7sA;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1249998
    :cond_7
    const-string p0, "friendEventWatchersFirst5"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1249999
    invoke-static {p1, v0}, LX/7sC;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1250000
    :cond_8
    const-string p0, "suggested_event_context_sentence"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1250001
    invoke-static {p1, v0}, LX/7sD;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1250002
    :cond_9
    const-string p0, "viewer_inviters"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1250003
    invoke-static {p1, v0}, LX/7u6;->b(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1250004
    :cond_a
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1250005
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1250006
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1250007
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1250008
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1250009
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1250010
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1250011
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1250012
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1250013
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
