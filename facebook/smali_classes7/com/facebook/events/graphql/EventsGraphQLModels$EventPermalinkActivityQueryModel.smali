.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x78b4bb8c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248729
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248728
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1248726
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248727
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1248720
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->g:Ljava/lang/String;

    .line 1248721
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1248722
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1248723
    if-eqz v0, :cond_0

    .line 1248724
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1248725
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248718
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->f:Ljava/lang/String;

    .line 1248719
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248716
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->g:Ljava/lang/String;

    .line 1248717
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1248680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248681
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1248682
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1248683
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1248684
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1248685
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1248686
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1248687
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1248688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1248708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248709
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1248710
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    .line 1248711
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1248712
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    .line 1248713
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    .line 1248714
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248715
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1248707
    new-instance v0, LX/7ph;

    invoke-direct {v0, p1}, LX/7ph;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248706
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1248700
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1248701
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1248702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1248703
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1248704
    :goto_0
    return-void

    .line 1248705
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1248697
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1248698
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->a(Ljava/lang/String;)V

    .line 1248699
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1248694
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;-><init>()V

    .line 1248695
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248696
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1248693
    const v0, -0x7e249e88

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1248692
    const v0, 0x403827a

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventActivityList"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248690
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    .line 1248691
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel;

    return-object v0
.end method
