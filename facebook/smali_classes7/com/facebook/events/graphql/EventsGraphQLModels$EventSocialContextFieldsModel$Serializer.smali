.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1250683
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1250684
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1250685
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1250686
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1250687
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1250688
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1250689
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250690
    if-eqz v2, :cond_0

    .line 1250691
    const-string p0, "event_declines"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250692
    invoke-static {v1, v2, p1}, LX/7s4;->a(LX/15i;ILX/0nX;)V

    .line 1250693
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250694
    if-eqz v2, :cond_1

    .line 1250695
    const-string p0, "event_maybes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250696
    invoke-static {v1, v2, p1}, LX/7s5;->a(LX/15i;ILX/0nX;)V

    .line 1250697
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250698
    if-eqz v2, :cond_2

    .line 1250699
    const-string p0, "event_members"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250700
    invoke-static {v1, v2, p1}, LX/7s6;->a(LX/15i;ILX/0nX;)V

    .line 1250701
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250702
    if-eqz v2, :cond_3

    .line 1250703
    const-string p0, "friendEventMaybesFirst5"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250704
    invoke-static {v1, v2, p1, p2}, LX/7s8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1250705
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250706
    if-eqz v2, :cond_4

    .line 1250707
    const-string p0, "friendEventMembersFirst5"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250708
    invoke-static {v1, v2, p1, p2}, LX/7sA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1250709
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250710
    if-eqz v2, :cond_5

    .line 1250711
    const-string p0, "friendEventWatchersFirst5"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250712
    invoke-static {v1, v2, p1, p2}, LX/7sC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1250713
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250714
    if-eqz v2, :cond_6

    .line 1250715
    const-string p0, "suggested_event_context_sentence"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250716
    invoke-static {v1, v2, p1}, LX/7sD;->a(LX/15i;ILX/0nX;)V

    .line 1250717
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1250718
    if-eqz v2, :cond_7

    .line 1250719
    const-string p0, "viewer_inviters"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1250720
    invoke-static {v1, v2, p1, p2}, LX/7u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1250721
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1250722
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1250723
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
