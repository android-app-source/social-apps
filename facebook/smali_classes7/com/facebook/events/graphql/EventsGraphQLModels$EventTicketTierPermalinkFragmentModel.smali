.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oj;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3a9b2f75
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253177
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253190
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1253188
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1253189
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1253182
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j:I

    .line 1253183
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1253184
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1253185
    if-eqz v0, :cond_0

    .line 1253186
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1253187
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253180
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->e:Ljava/lang/String;

    .line 1253181
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253178
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1253179
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    return-object v0
.end method

.method private l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253133
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1253134
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253175
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1253176
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    return-object v0
.end method

.method private n()I
    .locals 2

    .prologue
    .line 1253173
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253174
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1253191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253192
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1253193
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1253194
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1253195
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1253196
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1253197
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1253198
    const/4 v0, 0x1

    iget-boolean v4, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->f:Z

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1253199
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1253200
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1253201
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1253202
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 1253203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253204
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1253155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253156
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1253157
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1253158
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1253159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1253160
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 1253161
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1253162
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1253163
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1253164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1253165
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 1253166
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1253167
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1253168
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1253169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1253170
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 1253171
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253172
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1253154
    new-instance v0, LX/7qE;

    invoke-direct {v0, p1}, LX/7qE;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253153
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1253149
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1253150
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->f:Z

    .line 1253151
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->j:I

    .line 1253152
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1253143
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1253144
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1253145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1253146
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1253147
    :goto_0
    return-void

    .line 1253148
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1253140
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1253141
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;->a(I)V

    .line 1253142
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1253137
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;-><init>()V

    .line 1253138
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1253139
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1253136
    const v0, -0x17316435

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1253135
    const v0, 0x403827a

    return v0
.end method
