.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5d8fb525
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1243005
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1243004
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1243002
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1243003
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1242994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242995
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1242996
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1242997
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1242998
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1242999
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1243000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1243001
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1242986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242987
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1242988
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1242989
    if-eqz v1, :cond_0

    .line 1242990
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;

    .line 1242991
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->f:Ljava/util/List;

    .line 1242992
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242993
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243006
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->e:Ljava/lang/String;

    .line 1243007
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1242983
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;-><init>()V

    .line 1242984
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242985
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1242982
    const v0, 0x3aae84c6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1242981
    const v0, 0x6e1e805d

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1242979
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventArtistPageDetailsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->f:Ljava/util/List;

    .line 1242980
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryEdgesFragmentModel$RoleAssociatedEdgesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
