.class public final Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7nK;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7b1c3ab4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239283
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239284
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1239285
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1239286
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1239287
    iput-object p1, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l:Ljava/lang/String;

    .line 1239288
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1239289
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1239290
    if-eqz v0, :cond_0

    .line 1239291
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1239292
    :cond_0
    return-void
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239293
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239294
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239295
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239296
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1239297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239298
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1239299
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1239300
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1239301
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1239302
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1239303
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1239304
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1239305
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->eN_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1239306
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1239307
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1239308
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1239309
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1239310
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1239311
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1239312
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1239313
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1239314
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1239315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239316
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1239317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239318
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1239319
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239320
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1239321
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 1239322
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239323
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1239324
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239325
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1239326
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 1239327
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239328
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1239329
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    .line 1239330
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1239331
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 1239332
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->g:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    .line 1239333
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1239334
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    .line 1239335
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1239336
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 1239337
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->h:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    .line 1239338
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1239339
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    .line 1239340
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1239341
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 1239342
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->i:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    .line 1239343
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239344
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1239345
    new-instance v0, LX/7nN;

    invoke-direct {v0, p1}, LX/7nN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239346
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1239274
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239275
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->eN_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1239276
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1239277
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    .line 1239278
    :goto_0
    return-void

    .line 1239279
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1239280
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239281
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->a(Ljava/lang/String;)V

    .line 1239282
    :cond_0
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239255
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1239257
    new-instance v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;-><init>()V

    .line 1239258
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1239259
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239260
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->n()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239261
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j:Ljava/lang/String;

    .line 1239262
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1239256
    const v0, 0x59ad2a7a

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239263
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k:Ljava/lang/String;

    .line 1239264
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final eN_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239265
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l:Ljava/lang/String;

    .line 1239266
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1239267
    const v0, 0x403827a

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239268
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->g:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->g:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    .line 1239269
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->g:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239270
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->h:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->h:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    .line 1239271
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->h:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239272
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->i:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->i:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    .line 1239273
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->i:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    return-object v0
.end method
