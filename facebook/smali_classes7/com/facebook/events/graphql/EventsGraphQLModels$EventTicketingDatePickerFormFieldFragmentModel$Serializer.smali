.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1253283
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1253284
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1253285
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1253286
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1253287
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1253288
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1253289
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253290
    if-eqz v2, :cond_0

    .line 1253291
    const-string p0, "available_times"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253292
    invoke-static {v1, v2, p1, p2}, LX/7su;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1253293
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1253294
    if-eqz v2, :cond_1

    .line 1253295
    const-string p0, "form_field_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253296
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253297
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253298
    if-eqz v2, :cond_2

    .line 1253299
    const-string p0, "time_end"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253300
    invoke-static {v1, v2, p1}, LX/7sv;->a(LX/15i;ILX/0nX;)V

    .line 1253301
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253302
    if-eqz v2, :cond_3

    .line 1253303
    const-string p0, "time_selected"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253304
    invoke-static {v1, v2, p1}, LX/7sw;->a(LX/15i;ILX/0nX;)V

    .line 1253305
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253306
    if-eqz v2, :cond_4

    .line 1253307
    const-string p0, "time_start"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253308
    invoke-static {v1, v2, p1}, LX/7sx;->a(LX/15i;ILX/0nX;)V

    .line 1253309
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1253310
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1253311
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
