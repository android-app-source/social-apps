.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1258135
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1258136
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1258137
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1258138
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1258139
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1258140
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1258141
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1258142
    if-eqz v2, :cond_0

    .line 1258143
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1258144
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1258145
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1258146
    if-eqz v2, :cond_1

    .line 1258147
    const-string p0, "all_events"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1258148
    invoke-static {v1, v2, p1}, LX/7tn;->a(LX/15i;ILX/0nX;)V

    .line 1258149
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1258150
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1258151
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
