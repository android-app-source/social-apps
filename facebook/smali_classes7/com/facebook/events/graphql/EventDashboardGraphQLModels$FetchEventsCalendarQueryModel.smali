.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x347405bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238331
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238330
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1238328
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1238329
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1238322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238323
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1238324
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1238325
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1238326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238327
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1238307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238308
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1238309
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    .line 1238310
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1238311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;

    .line 1238312
    iput-object v0, v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->e:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    .line 1238313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238314
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238320
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->e:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->e:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    .line 1238321
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;->e:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1238317
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;-><init>()V

    .line 1238318
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1238319
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1238316
    const v0, 0x728b9ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1238315
    const v0, -0x6747e1ce

    return v0
.end method
