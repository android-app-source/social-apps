.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oi;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29c84e76
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252872
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252806
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1252870
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1252871
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1252875
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m:I

    .line 1252876
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1252877
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1252878
    if-eqz v0, :cond_0

    .line 1252879
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1252880
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252881
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->e:Ljava/lang/String;

    .line 1252882
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252883
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252884
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252885
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->g:Ljava/lang/String;

    .line 1252886
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252887
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1252888
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    return-object v0
.end method

.method private n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252889
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1252890
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTicketTiers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252891
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1252892
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    return-object v0
.end method

.method private p()I
    .locals 2

    .prologue
    .line 1252873
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1252874
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1252851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252852
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1252853
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1252854
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1252855
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1252856
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1252857
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1252858
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1252859
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1252860
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1252861
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1252862
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1252863
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1252864
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1252865
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1252866
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1252867
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1252868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1252869
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1252828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252829
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1252830
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252831
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1252832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    .line 1252833
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252834
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1252835
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1252836
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1252837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    .line 1252838
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1252839
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1252840
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1252841
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1252842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    .line 1252843
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1252844
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1252845
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1252846
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1252847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    .line 1252848
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1252849
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1252850
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1252827
    new-instance v0, LX/7qC;

    invoke-direct {v0, p1}, LX/7qC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252826
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1252821
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1252822
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->h:Z

    .line 1252823
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->i:Z

    .line 1252824
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->m:I

    .line 1252825
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1252815
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1252816
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1252817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1252818
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 1252819
    :goto_0
    return-void

    .line 1252820
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1252812
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1252813
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;->a(I)V

    .line 1252814
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1252809
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel;-><init>()V

    .line 1252810
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1252811
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1252808
    const v0, -0x60377eed

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1252807
    const v0, 0x403827a

    return v0
.end method
