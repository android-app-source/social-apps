.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7e05e4eb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271583
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271550
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1271581
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1271582
    return-void
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getClaimActionLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1271579
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1271580
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventTickets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271577
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    .line 1271578
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271575
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->g:Ljava/lang/String;

    .line 1271576
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1271584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271585
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x3928ad92

    invoke-static {v1, v0, v2}, Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1271586
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1271587
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1271588
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1271589
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1271590
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1271591
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1271592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271593
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1271560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271561
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1271562
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3928ad92

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1271563
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1271564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    .line 1271565
    iput v3, v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->e:I

    move-object v1, v0

    .line 1271566
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1271567
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    .line 1271568
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1271569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    .line 1271570
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    .line 1271571
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271572
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1271573
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1271574
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271559
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1271556
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1271557
    const/4 v0, 0x0

    const v1, 0x3928ad92

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;->e:I

    .line 1271558
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1271553
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;-><init>()V

    .line 1271554
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1271555
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1271552
    const v0, -0x3087fd6b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1271551
    const v0, -0x3b2b1738

    return v0
.end method
