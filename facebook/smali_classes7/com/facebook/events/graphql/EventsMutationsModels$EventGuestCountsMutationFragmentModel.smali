.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x25a783ae
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271074
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271073
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1271071
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1271072
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1271068
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1271069
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1271070
    return-void
.end method

.method private j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271066
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 1271067
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271064
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    .line 1271065
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    return-object v0
.end method

.method private l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270955
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    .line 1270956
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271062
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    .line 1271063
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271060
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->i:Ljava/lang/String;

    .line 1271061
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1271046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271047
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1271048
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1271049
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1271050
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1271051
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1271052
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1271053
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1271054
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1271055
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1271056
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1271057
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1271058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271059
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1271023
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271024
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1271025
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 1271026
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1271027
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    .line 1271028
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 1271029
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1271030
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    .line 1271031
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1271032
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    .line 1271033
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    .line 1271034
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1271035
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    .line 1271036
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1271037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    .line 1271038
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    .line 1271039
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1271040
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    .line 1271041
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1271042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    .line 1271043
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    .line 1271044
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271045
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1271022
    new-instance v0, LX/7uY;

    invoke-direct {v0, p1}, LX/7uY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271021
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1270995
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1270996
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v0

    .line 1270997
    if-eqz v0, :cond_3

    .line 1270998
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1270999
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1271000
    iput v2, p2, LX/18L;->c:I

    .line 1271001
    :goto_0
    return-void

    .line 1271002
    :cond_0
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1271003
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v0

    .line 1271004
    if-eqz v0, :cond_3

    .line 1271005
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1271006
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1271007
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1271008
    :cond_1
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1271009
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v0

    .line 1271010
    if-eqz v0, :cond_3

    .line 1271011
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1271012
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1271013
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1271014
    :cond_2
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1271015
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v0

    .line 1271016
    if-eqz v0, :cond_3

    .line 1271017
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1271018
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1271019
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1271020
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1270962
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1270963
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    move-result-object v0

    .line 1270964
    if-eqz v0, :cond_0

    .line 1270965
    if-eqz p3, :cond_1

    .line 1270966
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 1270967
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;->a(I)V

    .line 1270968
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;

    .line 1270969
    :cond_0
    :goto_0
    return-void

    .line 1270970
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1270971
    :cond_2
    const-string v0, "event_invitees.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1270972
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->k()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    move-result-object v0

    .line 1270973
    if-eqz v0, :cond_0

    .line 1270974
    if-eqz p3, :cond_3

    .line 1270975
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    .line 1270976
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;->a(I)V

    .line 1270977
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;

    goto :goto_0

    .line 1270978
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventInviteesModel;->a(I)V

    goto :goto_0

    .line 1270979
    :cond_4
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1270980
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    move-result-object v0

    .line 1270981
    if-eqz v0, :cond_0

    .line 1270982
    if-eqz p3, :cond_5

    .line 1270983
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    .line 1270984
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;->a(I)V

    .line 1270985
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->g:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;

    goto :goto_0

    .line 1270986
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1270987
    :cond_6
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1270988
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->m()Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    move-result-object v0

    .line 1270989
    if-eqz v0, :cond_0

    .line 1270990
    if-eqz p3, :cond_7

    .line 1270991
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    .line 1270992
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;->a(I)V

    .line 1270993
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;

    goto/16 :goto_0

    .line 1270994
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel$EventMembersModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1270959
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;-><init>()V

    .line 1270960
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1270961
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1270958
    const v0, 0x1b0af63a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1270957
    const v0, 0x403827a

    return v0
.end method
