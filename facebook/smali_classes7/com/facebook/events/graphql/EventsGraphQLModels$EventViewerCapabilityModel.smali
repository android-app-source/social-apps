.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7a197ffd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254742
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254743
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1254744
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1254745
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1254746
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1254747
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1254748
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 9

    .prologue
    .line 1254749
    if-nez p0, :cond_0

    .line 1254750
    const/4 p0, 0x0

    .line 1254751
    :goto_0
    return-object p0

    .line 1254752
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    if-eqz v0, :cond_1

    .line 1254753
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    goto :goto_0

    .line 1254754
    :cond_1
    new-instance v0, LX/7qI;

    invoke-direct {v0}, LX/7qI;-><init>()V

    .line 1254755
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->a:Z

    .line 1254756
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->b:Z

    .line 1254757
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->c:Z

    .line 1254758
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->d:Z

    .line 1254759
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->e:Z

    .line 1254760
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->fb_()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->f:Z

    .line 1254761
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->fc_()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->g:Z

    .line 1254762
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->h:Z

    .line 1254763
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->i:Z

    .line 1254764
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->j:Z

    .line 1254765
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->k:Z

    .line 1254766
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->l:Z

    .line 1254767
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->m:Z

    .line 1254768
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->n:Z

    .line 1254769
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/7qI;->o:Z

    .line 1254770
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    iput-object v1, v0, LX/7qI;->p:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1254771
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1254772
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1254773
    iget-object v3, v0, LX/7qI;->p:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1254774
    const/16 v5, 0x10

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 1254775
    iget-boolean v5, v0, LX/7qI;->a:Z

    invoke-virtual {v2, v8, v5}, LX/186;->a(IZ)V

    .line 1254776
    iget-boolean v5, v0, LX/7qI;->b:Z

    invoke-virtual {v2, v6, v5}, LX/186;->a(IZ)V

    .line 1254777
    const/4 v5, 0x2

    iget-boolean v7, v0, LX/7qI;->c:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254778
    const/4 v5, 0x3

    iget-boolean v7, v0, LX/7qI;->d:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254779
    const/4 v5, 0x4

    iget-boolean v7, v0, LX/7qI;->e:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254780
    const/4 v5, 0x5

    iget-boolean v7, v0, LX/7qI;->f:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254781
    const/4 v5, 0x6

    iget-boolean v7, v0, LX/7qI;->g:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254782
    const/4 v5, 0x7

    iget-boolean v7, v0, LX/7qI;->h:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254783
    const/16 v5, 0x8

    iget-boolean v7, v0, LX/7qI;->i:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254784
    const/16 v5, 0x9

    iget-boolean v7, v0, LX/7qI;->j:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254785
    const/16 v5, 0xa

    iget-boolean v7, v0, LX/7qI;->k:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254786
    const/16 v5, 0xb

    iget-boolean v7, v0, LX/7qI;->l:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254787
    const/16 v5, 0xc

    iget-boolean v7, v0, LX/7qI;->m:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254788
    const/16 v5, 0xd

    iget-boolean v7, v0, LX/7qI;->n:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254789
    const/16 v5, 0xe

    iget-boolean v7, v0, LX/7qI;->o:Z

    invoke-virtual {v2, v5, v7}, LX/186;->a(IZ)V

    .line 1254790
    const/16 v5, 0xf

    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1254791
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1254792
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1254793
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1254794
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1254795
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1254796
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;-><init>(LX/15i;)V

    .line 1254797
    move-object p0, v3

    .line 1254798
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1254799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254800
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1254801
    const/16 v1, 0x10

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1254802
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254803
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->f:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254804
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->g:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254805
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->h:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254806
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->i:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254807
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->j:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254808
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->k:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254809
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->l:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254810
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->m:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254811
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->n:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254812
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->o:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254813
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->p:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254814
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254815
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254816
    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->s:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1254817
    const/16 v1, 0xf

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1254818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1254847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254849
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1254820
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1254821
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->e:Z

    .line 1254822
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->f:Z

    .line 1254823
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->g:Z

    .line 1254824
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->h:Z

    .line 1254825
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->i:Z

    .line 1254826
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->j:Z

    .line 1254827
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->k:Z

    .line 1254828
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->l:Z

    .line 1254829
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->m:Z

    .line 1254830
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->n:Z

    .line 1254831
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->o:Z

    .line 1254832
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->p:Z

    .line 1254833
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q:Z

    .line 1254834
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r:Z

    .line 1254835
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->s:Z

    .line 1254836
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1254837
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254838
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1254839
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;-><init>()V

    .line 1254840
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1254841
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1254842
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254843
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1254844
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254845
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1254740
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254741
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1254846
    const v0, 0x5740fa03

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1254715
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254716
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->i:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1254717
    const v0, -0x71db289c

    return v0
.end method

.method public final fb_()Z
    .locals 2

    .prologue
    .line 1254718
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254719
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->j:Z

    return v0
.end method

.method public final fc_()Z
    .locals 2

    .prologue
    .line 1254720
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254721
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->k:Z

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1254722
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254723
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->l:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1254724
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254725
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->m:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1254726
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254727
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->n:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1254728
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254729
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->o:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1254730
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254731
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->p:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1254732
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254733
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q:Z

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1254734
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254735
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 1254736
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254737
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->s:Z

    return v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254738
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->t:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->t:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1254739
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->t:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method
