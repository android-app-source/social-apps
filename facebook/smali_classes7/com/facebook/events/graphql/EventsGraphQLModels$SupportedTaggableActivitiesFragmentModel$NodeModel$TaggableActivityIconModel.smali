.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4249f99b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259849
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259852
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1259850
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1259851
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1259840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259841
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1259842
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1259843
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1259844
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1259845
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1259846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259847
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1259832
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259833
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1259834
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    .line 1259835
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1259836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    .line 1259837
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    .line 1259838
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259839
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259848
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1259823
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;-><init>()V

    .line 1259824
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1259825
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1259826
    const v0, 0x58a94caa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1259827
    const v0, 0x2615e4cf

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259828
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    .line 1259829
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259830
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    .line 1259831
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel$ImageModel;

    return-object v0
.end method
