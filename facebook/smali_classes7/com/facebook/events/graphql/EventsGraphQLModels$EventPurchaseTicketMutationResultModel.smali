.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1536fff2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249431
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249374
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1249429
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1249430
    return-void
.end method

.method private p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249427
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1249428
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1249409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249410
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x1490253d

    invoke-static {v1, v0, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1249411
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1249412
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x3bc85c65

    invoke-static {v3, v2, v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1249413
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1249414
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1249415
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1249416
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1249417
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1249418
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1249419
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1249420
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1249421
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1249422
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1249423
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1249424
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1249425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249426
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1249383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249384
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1249385
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1490253d

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1249386
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1249387
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    .line 1249388
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->e:I

    move-object v1, v0

    .line 1249389
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1249390
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1249391
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1249392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    .line 1249393
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1249394
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1249395
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3bc85c65

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1249396
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1249397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    .line 1249398
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->g:I

    move-object v1, v0

    .line 1249399
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1249400
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1249401
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1249402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    .line 1249403
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1249404
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249405
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1249406
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1249407
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 1249408
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249382
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1249378
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1249379
    const/4 v0, 0x0

    const v1, 0x1490253d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->e:I

    .line 1249380
    const/4 v0, 0x2

    const v1, -0x3bc85c65

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->g:I

    .line 1249381
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1249375
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;-><init>()V

    .line 1249376
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1249377
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1249360
    const v0, -0x7aaa7799

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1249361
    const v0, -0x3b2b1738

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getClaimActionLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1249362
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1249363
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventTickets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249364
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1249365
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249366
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->h:Ljava/lang/String;

    .line 1249367
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249368
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1249369
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249370
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 1249371
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->j:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249372
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k:Ljava/lang/String;

    .line 1249373
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPurchaseTicketMutationResultModel;->k:Ljava/lang/String;

    return-object v0
.end method
