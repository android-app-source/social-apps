.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x644dbcee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241864
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241863
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241861
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241862
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1241855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241856
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1241857
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1241858
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1241859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241860
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241848
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241849
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    .line 1241850
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241851
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;

    .line 1241852
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    .line 1241853
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241854
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1241865
    new-instance v0, LX/7ot;

    invoke-direct {v0, p1}, LX/7ot;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllEventMaybes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241845
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    .line 1241846
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1241843
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1241844
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1241842
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241839
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;-><init>()V

    .line 1241840
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241841
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241838
    const v0, -0x28cfe9bd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241837
    const v0, 0x403827a

    return v0
.end method
