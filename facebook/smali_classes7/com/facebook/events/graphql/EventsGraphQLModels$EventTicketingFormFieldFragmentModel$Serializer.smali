.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1253561
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1253562
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1253471
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1253472
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1253473
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x11

    const/16 v7, 0x10

    const/16 v6, 0x9

    const/4 v5, 0x7

    const/4 v4, 0x0

    .line 1253474
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1253475
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1253476
    if-eqz v2, :cond_0

    .line 1253477
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253478
    invoke-static {v1, v0, v4, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1253479
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1253480
    if-eqz v2, :cond_1

    .line 1253481
    const-string v3, "allow_multi_select"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253482
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1253483
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253484
    if-eqz v2, :cond_2

    .line 1253485
    const-string v3, "available_time_slots"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253486
    invoke-static {v1, v2, p1, p2}, LX/7tC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1253487
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253488
    if-eqz v2, :cond_3

    .line 1253489
    const-string v3, "available_times"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253490
    invoke-static {v1, v2, p1, p2}, LX/7su;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1253491
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253492
    if-eqz v2, :cond_4

    .line 1253493
    const-string v3, "default_value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253494
    invoke-static {v1, v2, p1}, LX/7sz;->a(LX/15i;ILX/0nX;)V

    .line 1253495
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253496
    if-eqz v2, :cond_5

    .line 1253497
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253498
    invoke-static {v1, v2, p1}, LX/7tB;->a(LX/15i;ILX/0nX;)V

    .line 1253499
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1253500
    if-eqz v2, :cond_6

    .line 1253501
    const-string v3, "disable_autofill"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253502
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1253503
    :cond_6
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1253504
    if-eqz v2, :cond_7

    .line 1253505
    const-string v2, "fields"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253506
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1253507
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1253508
    if-eqz v2, :cond_8

    .line 1253509
    const-string v3, "form_field_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253510
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253511
    :cond_8
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1253512
    if-eqz v2, :cond_9

    .line 1253513
    const-string v2, "form_field_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253514
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253515
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1253516
    if-eqz v2, :cond_a

    .line 1253517
    const-string v3, "heading"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253518
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253519
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1253520
    if-eqz v2, :cond_b

    .line 1253521
    const-string v3, "is_optional"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253522
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1253523
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1253524
    if-eqz v2, :cond_c

    .line 1253525
    const-string v3, "is_weekly_view"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253526
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1253527
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253528
    if-eqz v2, :cond_d

    .line 1253529
    const-string v3, "items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253530
    invoke-static {v1, v2, p1, p2}, LX/7tA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1253531
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1253532
    if-eqz v2, :cond_e

    .line 1253533
    const-string v3, "max_selected"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253534
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1253535
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253536
    if-eqz v2, :cond_f

    .line 1253537
    const-string v3, "prefill_values"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253538
    invoke-static {v1, v2, p1, p2}, LX/7t0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1253539
    :cond_f
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1253540
    if-eqz v2, :cond_10

    .line 1253541
    const-string v2, "semantic_tag"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253542
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253543
    :cond_10
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1253544
    if-eqz v2, :cond_11

    .line 1253545
    const-string v2, "style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253546
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1253547
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253548
    if-eqz v2, :cond_12

    .line 1253549
    const-string v3, "time_end"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253550
    invoke-static {v1, v2, p1}, LX/7sv;->a(LX/15i;ILX/0nX;)V

    .line 1253551
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253552
    if-eqz v2, :cond_13

    .line 1253553
    const-string v3, "time_selected"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253554
    invoke-static {v1, v2, p1}, LX/7sw;->a(LX/15i;ILX/0nX;)V

    .line 1253555
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1253556
    if-eqz v2, :cond_14

    .line 1253557
    const-string v3, "time_start"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1253558
    invoke-static {v1, v2, p1}, LX/7sx;->a(LX/15i;ILX/0nX;)V

    .line 1253559
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1253560
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1253470
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
