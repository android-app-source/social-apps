.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1248678
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1248679
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1248677
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1248660
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1248661
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1248662
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1248663
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1248664
    if-eqz v2, :cond_0

    .line 1248665
    const-string p0, "event_activity_list"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1248666
    invoke-static {v1, v2, p1, p2}, LX/7rs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1248667
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1248668
    if-eqz v2, :cond_1

    .line 1248669
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1248670
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1248671
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1248672
    if-eqz v2, :cond_2

    .line 1248673
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1248674
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1248675
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1248676
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1248659
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
