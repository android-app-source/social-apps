.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7372381
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271542
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271518
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1271540
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1271541
    return-void
.end method

.method private a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1271538
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x33102346

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->e:LX/3Sb;

    .line 1271539
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1271532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271533
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1271534
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1271535
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1271536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271537
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1271524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271525
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1271526
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/events/graphql/EventsMutationsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1271527
    if-eqz v1, :cond_0

    .line 1271528
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    .line 1271529
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;->e:LX/3Sb;

    .line 1271530
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271531
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1271521
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel$EventTicketsModel;-><init>()V

    .line 1271522
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1271523
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1271520
    const v0, 0x4baabf44    # 2.2380168E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1271519
    const v0, 0x576007a3

    return v0
.end method
