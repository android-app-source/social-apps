.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf35cdca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237950
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237949
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1237947
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1237948
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237945
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->f:Ljava/lang/String;

    .line 1237946
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237943
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->i:Ljava/lang/String;

    .line 1237944
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1237929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237930
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1237931
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1237932
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x323d8ce6

    invoke-static {v3, v2, v4}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventDashboardGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1237933
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1237934
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1237935
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1237936
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1237937
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1237938
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1237939
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1237940
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1237941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1237919
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237920
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1237921
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x323d8ce6

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventDashboardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1237922
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1237923
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;

    .line 1237924
    iput v3, v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->g:I

    .line 1237925
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237926
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1237927
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1237928
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237951
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->e:Ljava/lang/String;

    .line 1237952
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1237916
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1237917
    const/4 v0, 0x2

    const v1, 0x323d8ce6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->g:I

    .line 1237918
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1237913
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;-><init>()V

    .line 1237914
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1237915
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1237912
    const v0, -0x16f09a2f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1237911
    const v0, -0x496b7e3e

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237909
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1237910
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237907
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->h:Ljava/lang/String;

    .line 1237908
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->h:Ljava/lang/String;

    return-object v0
.end method
