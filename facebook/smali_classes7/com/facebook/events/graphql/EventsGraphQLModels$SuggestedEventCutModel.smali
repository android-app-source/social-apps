.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4c1c2dbf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259640
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259641
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1259642
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1259643
    return-void
.end method

.method private j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259644
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    .line 1259645
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1259646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259647
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->a()Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1259648
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1259649
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1259650
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1259651
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1259652
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1259653
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1259654
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259655
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1259656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259657
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1259658
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    .line 1259659
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1259660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 1259661
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    .line 1259662
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259663
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259664
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->e:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->e:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    .line 1259665
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->e:Lcom/facebook/graphql/enums/GraphQLEventSuggestionCutType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1259666
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;-><init>()V

    .line 1259667
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1259668
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259669
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->f:Ljava/lang/String;

    .line 1259670
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259671
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1259672
    const v0, -0x4e6a5ddf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1259673
    const v0, 0x223d544

    return v0
.end method
