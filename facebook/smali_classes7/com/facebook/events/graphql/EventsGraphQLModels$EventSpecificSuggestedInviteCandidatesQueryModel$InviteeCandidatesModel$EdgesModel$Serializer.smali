.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1251021
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1251022
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1251023
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1251024
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1251025
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/7sF;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1251026
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1251027
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;LX/0nX;LX/0my;)V

    return-void
.end method
