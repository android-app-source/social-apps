.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1247114
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1247115
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1247116
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1247117
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1247118
    const/4 v2, 0x0

    .line 1247119
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1247120
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247121
    :goto_0
    move v1, v2

    .line 1247122
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1247123
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1247124
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;-><init>()V

    .line 1247125
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1247126
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1247127
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1247128
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1247129
    :cond_0
    return-object v1

    .line 1247130
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247131
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1247132
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1247133
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1247134
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1247135
    const-string v5, "event_invitable_contacts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1247136
    const/4 v4, 0x0

    .line 1247137
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 1247138
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247139
    :goto_2
    move v3, v4

    .line 1247140
    goto :goto_1

    .line 1247141
    :cond_3
    const-string v5, "event_viewer_capability"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1247142
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1247143
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_f

    .line 1247144
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247145
    :goto_3
    move v1, v4

    .line 1247146
    goto :goto_1

    .line 1247147
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1247148
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1247149
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1247150
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1247151
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247152
    :cond_7
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1247153
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1247154
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1247155
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, p0, :cond_7

    if-eqz v6, :cond_7

    .line 1247156
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1247157
    invoke-static {p1, v0}, LX/7rc;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_4

    .line 1247158
    :cond_8
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1247159
    invoke-static {p1, v0}, LX/4aB;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_4

    .line 1247160
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1247161
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1247162
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1247163
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_4

    .line 1247164
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_d

    .line 1247165
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1247166
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1247167
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v7, :cond_b

    .line 1247168
    const-string p0, "remaining_invites"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1247169
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    goto :goto_5

    .line 1247170
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1247171
    :cond_d
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1247172
    if-eqz v1, :cond_e

    .line 1247173
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1247174
    :cond_e
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_f
    move v1, v4

    move v6, v4

    goto :goto_5
.end method
