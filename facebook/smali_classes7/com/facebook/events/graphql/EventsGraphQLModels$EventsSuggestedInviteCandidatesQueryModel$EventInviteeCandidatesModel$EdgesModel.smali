.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x50b14764
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254972
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254971
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1254969
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1254970
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1254963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254964
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1254965
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1254966
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1254967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1254973
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254974
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1254975
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1254976
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1254977
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;

    .line 1254978
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1254979
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254980
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254961
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1254962
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1254958
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;-><init>()V

    .line 1254959
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1254960
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1254957
    const v0, -0x497f0d8e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1254956
    const v0, 0x53a4486f

    return v0
.end method
