.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249816
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1249817
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249805
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1249807
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1249808
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1249809
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1249810
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1249811
    if-eqz v2, :cond_0

    .line 1249812
    const-string p0, "event_sms_associates"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1249813
    invoke-static {v1, v2, p1, p2}, LX/7s2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1249814
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1249815
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1249806
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
