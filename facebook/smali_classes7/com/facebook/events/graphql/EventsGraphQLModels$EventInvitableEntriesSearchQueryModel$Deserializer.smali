.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1247254
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1247255
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1247256
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1247257
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1247258
    const/4 v2, 0x0

    .line 1247259
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1247260
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247261
    :goto_0
    move v1, v2

    .line 1247262
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1247263
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1247264
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;-><init>()V

    .line 1247265
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1247266
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1247267
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1247268
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1247269
    :cond_0
    return-object v1

    .line 1247270
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247271
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1247272
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1247273
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1247274
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1247275
    const-string v4, "invitable_entries_search"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1247276
    const/4 v3, 0x0

    .line 1247277
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1247278
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247279
    :goto_2
    move v1, v3

    .line 1247280
    goto :goto_1

    .line 1247281
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1247282
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1247283
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1247284
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1247285
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1247286
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1247287
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1247288
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1247289
    const-string p0, "nodes"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1247290
    invoke-static {p1, v0}, LX/7rc;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 1247291
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1247292
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1247293
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3
.end method
