.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x53b9411d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251752
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251751
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;->f:I

    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketPriceModel;-><init>()V

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    const v0, 0x611176cf

    return v0
.end method

.method public final f()I
    .locals 1

    const v0, 0x2cee5bdc

    return v0
.end method
