.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x42c9bd69
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245332
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245331
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245329
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245330
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1245320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245321
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1245322
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1245323
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1245324
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1245325
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1245326
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1245327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245328
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEdges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1245318
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->e:Ljava/util/List;

    .line 1245319
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1245333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245334
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1245335
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1245336
    if-eqz v1, :cond_2

    .line 1245337
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;

    .line 1245338
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1245339
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245340
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1245341
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1245342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;

    .line 1245343
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1245344
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245345
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1245308
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1245309
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->g:I

    .line 1245310
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245315
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;-><init>()V

    .line 1245316
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245317
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245314
    const v0, 0x4ecc97e6    # 1.71625344E9f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245313
    const v0, 0x34854315

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245311
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1245312
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    return-object v0
.end method
