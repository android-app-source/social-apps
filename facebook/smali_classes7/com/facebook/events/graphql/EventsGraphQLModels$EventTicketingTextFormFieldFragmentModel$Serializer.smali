.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1254479
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1254480
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1254481
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1254482
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1254483
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x3

    .line 1254484
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1254485
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254486
    if-eqz v2, :cond_0

    .line 1254487
    const-string v3, "default_value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254488
    invoke-static {v1, v2, p1}, LX/7tA;->a(LX/15i;ILX/0nX;)V

    .line 1254489
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254490
    if-eqz v2, :cond_1

    .line 1254491
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254492
    invoke-static {v1, v2, p1}, LX/7tB;->a(LX/15i;ILX/0nX;)V

    .line 1254493
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1254494
    if-eqz v2, :cond_2

    .line 1254495
    const-string v3, "disable_autofill"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254496
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1254497
    :cond_2
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1254498
    if-eqz v2, :cond_3

    .line 1254499
    const-string v2, "fields"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254500
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1254501
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254502
    if-eqz v2, :cond_4

    .line 1254503
    const-string v3, "form_field_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254504
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254505
    :cond_4
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1254506
    if-eqz v2, :cond_5

    .line 1254507
    const-string v2, "form_field_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254508
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254509
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254510
    if-eqz v2, :cond_6

    .line 1254511
    const-string v3, "heading"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254512
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254513
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1254514
    if-eqz v2, :cond_7

    .line 1254515
    const-string v3, "is_optional"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254516
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1254517
    :cond_7
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1254518
    if-eqz v2, :cond_8

    .line 1254519
    const-string v2, "semantic_tag"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254520
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254521
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1254522
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1254523
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
