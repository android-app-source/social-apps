.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64f7de3a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272306
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272307
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1272308
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1272309
    return-void
.end method

.method private a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272310
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    .line 1272311
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1272312
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272313
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1272314
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1272315
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1272316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272317
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1272318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272319
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1272320
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    .line 1272321
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1272322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;

    .line 1272323
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel$UserModel;

    .line 1272324
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272325
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1272326
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;-><init>()V

    .line 1272327
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1272328
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1272329
    const v0, 0x144be426

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1272330
    const v0, 0xa5096c5

    return v0
.end method
