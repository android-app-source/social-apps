.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x137f2816
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254571
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254584
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1254582
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1254583
    return-void
.end method

.method private a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254580
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    .line 1254581
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254578
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254579
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1254576
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->h:Ljava/util/List;

    .line 1254577
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254574
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->i:Ljava/lang/String;

    .line 1254575
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254572
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 1254573
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254585
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->k:Ljava/lang/String;

    .line 1254586
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254524
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 1254525
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1254526
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254527
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1254528
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x79daef33

    invoke-static {v2, v1, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1254529
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1254530
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1254531
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1254532
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1254533
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1254534
    const/16 v7, 0x9

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1254535
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1254536
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1254537
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1254538
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1254539
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1254540
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1254541
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1254542
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1254543
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1254544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254545
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1254546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254547
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1254548
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    .line 1254549
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1254550
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    .line 1254551
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    .line 1254552
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1254553
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x79daef33

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1254554
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1254555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    .line 1254556
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->f:I

    move-object v1, v0

    .line 1254557
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254558
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1254559
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1254560
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1254561
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1254562
    const/4 v0, 0x1

    const v1, 0x79daef33

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->f:I

    .line 1254563
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->g:Z

    .line 1254564
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;->l:Z

    .line 1254565
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1254566
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;-><init>()V

    .line 1254567
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1254568
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1254569
    const v0, -0x4ecad13e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1254570
    const v0, -0x55857437

    return v0
.end method
