.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oe;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250178
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1250227
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1250225
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250226
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1250222
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1250223
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250224
    return-void
.end method

.method public static a(LX/7oe;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .locals 8

    .prologue
    .line 1250203
    if-nez p0, :cond_0

    .line 1250204
    const/4 p0, 0x0

    .line 1250205
    :goto_0
    return-object p0

    .line 1250206
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    if-eqz v0, :cond_1

    .line 1250207
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    goto :goto_0

    .line 1250208
    :cond_1
    new-instance v0, LX/7pv;

    invoke-direct {v0}, LX/7pv;-><init>()V

    .line 1250209
    invoke-interface {p0}, LX/7oe;->a()I

    move-result v1

    iput v1, v0, LX/7pv;->a:I

    .line 1250210
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1250211
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1250212
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1250213
    iget v3, v0, LX/7pv;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1250214
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1250215
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1250216
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1250217
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1250218
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1250219
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;-><init>(LX/15i;)V

    .line 1250220
    move-object p0, v3

    .line 1250221
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1250201
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1250202
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1250196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250197
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1250198
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1250199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250200
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1250193
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1250194
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1250195
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1250187
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->e:I

    .line 1250188
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1250189
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1250190
    if-eqz v0, :cond_0

    .line 1250191
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1250192
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1250184
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1250185
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->e:I

    .line 1250186
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1250181
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;-><init>()V

    .line 1250182
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1250183
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1250180
    const v0, 0xef53544

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1250179
    const v0, -0x6e31e683

    return v0
.end method
