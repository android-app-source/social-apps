.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255889
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255890
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1255891
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1255892
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1255893
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1255894
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1255895
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255896
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1255897
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1255898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255899
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1255900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255902
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1255903
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1255904
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;->e:I

    .line 1255905
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1255906
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;-><init>()V

    .line 1255907
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1255908
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1255909
    const v0, -0x2be0cc7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1255910
    const v0, 0xed9d497

    return v0
.end method
