.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5096c497
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241389
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241388
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241386
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241387
    return-void
.end method

.method private j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241384
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241385
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241382
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    .line 1241383
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1241372
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241373
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1241374
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1241375
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1241376
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1241377
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1241378
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1241379
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1241380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241381
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241350
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241351
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241352
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241353
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 1241354
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241355
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1241356
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    .line 1241357
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1241358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 1241359
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    .line 1241360
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241361
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241371
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241368
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;-><init>()V

    .line 1241369
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241370
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241366
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->e:Ljava/lang/String;

    .line 1241367
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241365
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241364
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241363
    const v0, 0x3bf823dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241362
    const v0, 0x4cff1ce8    # 1.3375264E8f

    return v0
.end method
