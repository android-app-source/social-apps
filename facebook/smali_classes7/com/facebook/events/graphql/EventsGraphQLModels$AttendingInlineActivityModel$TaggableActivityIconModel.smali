.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52f4560a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241199
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241228
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241226
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241227
    return-void
.end method

.method private j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241224
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    .line 1241225
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1241216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241217
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1241218
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1241219
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1241220
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1241221
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1241222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241223
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241208
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241209
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241210
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    .line 1241211
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241212
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    .line 1241213
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    .line 1241214
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241215
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241229
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241205
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;-><init>()V

    .line 1241206
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241207
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241203
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    .line 1241204
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241202
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241201
    const v0, -0x764ed065

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241200
    const v0, 0x2615e4cf

    return v0
.end method
