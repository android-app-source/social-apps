.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x694e5dfd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258934
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258933
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1258935
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1258936
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1258937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258938
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1258939
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1258940
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1258941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1258924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258925
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1258926
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    .line 1258927
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1258928
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;

    .line 1258929
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    .line 1258930
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258931
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1258932
    new-instance v0, LX/7qW;

    invoke-direct {v0, p1}, LX/7qW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1258922
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    .line 1258923
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1258920
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1258921
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1258919
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1258916
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;-><init>()V

    .line 1258917
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1258918
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1258914
    const v0, 0x41576e10

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1258915
    const v0, 0x41e065f

    return v0
.end method
