.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x75f6e539
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258307
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258308
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1258309
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1258310
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1258311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258312
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x190f5b39

    invoke-static {v1, v0, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1258313
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1258314
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1258315
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1258316
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1258317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1258319
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258320
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1258321
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x190f5b39

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1258322
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1258323
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    .line 1258324
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->e:I

    move-object v1, v0

    .line 1258325
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1258326
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    .line 1258327
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1258328
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    .line 1258329
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    .line 1258330
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258331
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1258332
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1258333
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubscribedCalendarProfiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1258334
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1258335
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1258336
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1258337
    const/4 v0, 0x0

    const v1, 0x190f5b39

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->e:I

    .line 1258338
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1258339
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;-><init>()V

    .line 1258340
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1258341
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1258342
    const v0, 0x58b8dd5a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1258343
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1258344
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    .line 1258345
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    return-object v0
.end method
