.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7a4b6a38
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241750
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241749
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241747
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241748
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1241738
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241739
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1241740
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1241741
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1241742
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1241743
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1241744
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1241745
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241746
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241731
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241732
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241733
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241734
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;

    .line 1241735
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241736
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241737
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241725
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241726
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1241727
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1241728
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->f:J

    .line 1241729
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241716
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;-><init>()V

    .line 1241717
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241718
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241719
    const v0, -0x4e7f7ef9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241720
    const v0, -0x1794ec7e

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1241721
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1241722
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->f:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241723
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1241724
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method
