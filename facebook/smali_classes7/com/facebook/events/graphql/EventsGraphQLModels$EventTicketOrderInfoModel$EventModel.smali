.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oX;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x49448e5c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:J

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251559
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251558
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251556
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251557
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1251550
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k:Ljava/lang/String;

    .line 1251551
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1251552
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1251553
    if-eqz v0, :cond_0

    .line 1251554
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1251555
    :cond_0
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251548
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m:Ljava/lang/String;

    .line 1251549
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 1251529
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251530
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1251531
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1251532
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1251533
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1251534
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->eR_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1251535
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1251536
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1251537
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1251538
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1251539
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1251540
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1251541
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1251542
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1251543
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1251544
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1251545
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1251546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251547
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1251511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251512
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1251513
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1251514
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1251515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    .line 1251516
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1251517
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1251518
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1251519
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1251520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    .line 1251521
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1251522
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1251523
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    .line 1251524
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1251525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    .line 1251526
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    .line 1251527
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251528
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1251510
    new-instance v0, LX/7q8;

    invoke-direct {v0, p1}, LX/7q8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251509
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1251504
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1251505
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->e:J

    .line 1251506
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->j:Z

    .line 1251507
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l:J

    .line 1251508
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1251498
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251499
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1251500
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1251501
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    .line 1251502
    :goto_0
    return-void

    .line 1251503
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1251495
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251496
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->a(Ljava/lang/String;)V

    .line 1251497
    :cond_0
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1251560
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251561
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->e:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251474
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;-><init>()V

    .line 1251475
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251476
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251477
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251478
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1251479
    const v0, -0x227928d6

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251480
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->i:Ljava/lang/String;

    .line 1251481
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1251482
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251483
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->j:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251484
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k:Ljava/lang/String;

    .line 1251485
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1251486
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1251487
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251488
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->l:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251489
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1251490
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251491
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1251492
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251493
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    .line 1251494
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderInfoModel$EventModel$EventTicketProviderModel;

    return-object v0
.end method
