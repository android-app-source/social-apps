.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18a8047a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245211
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245210
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245208
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245209
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1245202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245203
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1245204
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1245205
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1245206
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245207
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1245194
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245195
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245196
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    .line 1245197
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1245198
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;

    .line 1245199
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    .line 1245200
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245201
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1245212
    new-instance v0, LX/7pJ;

    invoke-direct {v0, p1}, LX/7pJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventEmailMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245192
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    .line 1245193
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1245190
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1245191
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1245189
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245186
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;-><init>()V

    .line 1245187
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245188
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245185
    const v0, -0x451cd13f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245184
    const v0, 0x403827a

    return v0
.end method
