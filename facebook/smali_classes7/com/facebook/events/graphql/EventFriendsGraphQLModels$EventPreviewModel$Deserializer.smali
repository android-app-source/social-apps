.class public final Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1239347
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    new-instance v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1239348
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1239349
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1239350
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1239351
    const/4 v2, 0x0

    .line 1239352
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1239353
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1239354
    :goto_0
    move v1, v2

    .line 1239355
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1239356
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1239357
    new-instance v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;-><init>()V

    .line 1239358
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1239359
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1239360
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1239361
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1239362
    :cond_0
    return-object v1

    .line 1239363
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1239364
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_7

    .line 1239365
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1239366
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1239367
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 1239368
    const-string p0, "eventProfilePicture"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1239369
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1239370
    :cond_3
    const-string p0, "event_place"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1239371
    invoke-static {p1, v0}, LX/7nV;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1239372
    :cond_4
    const-string p0, "id"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1239373
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1239374
    :cond_5
    const-string p0, "live_permalink_time_range_sentence"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1239375
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1239376
    :cond_6
    const-string p0, "name"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1239377
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1239378
    :cond_7
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1239379
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1239380
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1239381
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1239382
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1239383
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1239384
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1
.end method
