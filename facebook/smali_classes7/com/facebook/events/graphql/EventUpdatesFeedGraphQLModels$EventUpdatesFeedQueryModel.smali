.class public final Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xae9fdbf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1240108
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1240107
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1240105
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1240106
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1240099
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1240100
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1240101
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1240102
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1240103
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1240104
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1240091
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1240092
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1240093
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    .line 1240094
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1240095
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;

    .line 1240096
    iput-object v0, v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->e:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    .line 1240097
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1240098
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActor"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1240084
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->e:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->e:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    .line 1240085
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;->e:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1240088
    new-instance v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel;-><init>()V

    .line 1240089
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1240090
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1240087
    const v0, -0x3c7f7454

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1240086
    const v0, -0x6747e1ce

    return v0
.end method
