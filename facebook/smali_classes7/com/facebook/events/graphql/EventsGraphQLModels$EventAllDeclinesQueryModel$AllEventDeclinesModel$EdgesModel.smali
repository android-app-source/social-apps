.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7515ccea
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241565
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241564
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241562
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241563
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1241552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241553
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1241554
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->l()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1241555
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1241556
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1241557
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1241558
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1241559
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1241560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241561
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241545
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241546
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241547
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241548
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;

    .line 1241549
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241550
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241551
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1241540
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1241541
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->e:Z

    .line 1241542
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->g:J

    .line 1241543
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1241566
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1241567
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241537
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;-><init>()V

    .line 1241538
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241539
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241536
    const v0, 0x1e1fb43b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241535
    const v0, 0x23096734

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241533
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1241534
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 1241531
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1241532
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->g:J

    return-wide v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241529
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->h:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->h:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1241530
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel$EdgesModel;->h:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method
