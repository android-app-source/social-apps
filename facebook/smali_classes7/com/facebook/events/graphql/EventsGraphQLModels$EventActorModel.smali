.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oW;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x131672c2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241493
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241492
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241490
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241491
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1241487
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241488
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241489
    return-void
.end method

.method public static a(LX/7oW;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .locals 12

    .prologue
    .line 1241455
    if-nez p0, :cond_0

    .line 1241456
    const/4 p0, 0x0

    .line 1241457
    :goto_0
    return-object p0

    .line 1241458
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    if-eqz v0, :cond_1

    .line 1241459
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    goto :goto_0

    .line 1241460
    :cond_1
    new-instance v0, LX/7oq;

    invoke-direct {v0}, LX/7oq;-><init>()V

    .line 1241461
    invoke-interface {p0}, LX/7oW;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/7oq;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1241462
    invoke-interface {p0}, LX/7oW;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7oq;->b:Ljava/lang/String;

    .line 1241463
    invoke-interface {p0}, LX/7oW;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7oq;->c:Ljava/lang/String;

    .line 1241464
    invoke-interface {p0}, LX/7oW;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7oq;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1241465
    invoke-interface {p0}, LX/7oW;->eP_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7oq;->e:Ljava/lang/String;

    .line 1241466
    const/4 v6, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 1241467
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1241468
    iget-object v3, v0, LX/7oq;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1241469
    iget-object v5, v0, LX/7oq;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1241470
    iget-object v7, v0, LX/7oq;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1241471
    iget-object v8, v0, LX/7oq;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1241472
    iget-object v9, v0, LX/7oq;->e:Ljava/lang/String;

    invoke-virtual {v2, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1241473
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, LX/186;->c(I)V

    .line 1241474
    invoke-virtual {v2, v11, v3}, LX/186;->b(II)V

    .line 1241475
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1241476
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1241477
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1241478
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1241479
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1241480
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1241481
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1241482
    invoke-virtual {v3, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1241483
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1241484
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;-><init>(LX/15i;)V

    .line 1241485
    move-object p0, v3

    .line 1241486
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241453
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1241454
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1241439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241440
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1241441
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1241442
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1241443
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1241444
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->eP_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1241445
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1241446
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1241447
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1241448
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1241449
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1241450
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1241451
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241452
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241430
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241431
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1241432
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1241434
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1241435
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241436
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1241438
    new-instance v0, LX/7or;

    invoke-direct {v0, p1}, LX/7or;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241437
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1241494
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1241495
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1241415
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241416
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1241417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1241418
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241419
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;-><init>()V

    .line 1241420
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241421
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241422
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->f:Ljava/lang/String;

    .line 1241423
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241424
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->g:Ljava/lang/String;

    .line 1241425
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241426
    const v0, -0x76eec06a

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241427
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final eP_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241413
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->i:Ljava/lang/String;

    .line 1241414
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241428
    const v0, 0x3c2b9d5

    return v0
.end method
