.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x25520f23
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255189
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255161
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1255187
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1255188
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1255179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255180
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1255181
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1255182
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1255183
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1255184
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1255185
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255186
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1255171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255172
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1255173
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    .line 1255174
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1255175
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;

    .line 1255176
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    .line 1255177
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255178
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1255169
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    .line 1255170
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1255166
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;-><init>()V

    .line 1255167
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1255168
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1255165
    const v0, 0x730daefd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1255164
    const v0, -0x667851f1

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1255162
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 1255163
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    return-object v0
.end method
