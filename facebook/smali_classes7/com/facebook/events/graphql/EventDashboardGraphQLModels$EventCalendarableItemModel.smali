.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2a9e3c90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237830
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1237831
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1237832
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1237833
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1237852
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1237853
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1237854
    return-void
.end method

.method private j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237834
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1237835
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237806
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->k:Ljava/lang/String;

    .line 1237807
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    .line 1237836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237837
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1237838
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1237839
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1237840
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1237841
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1237842
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1237843
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1237844
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1237845
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->g:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1237846
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1237847
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1237848
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1237849
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1237850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237851
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1237815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1237816
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1237817
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1237818
    if-eqz v1, :cond_2

    .line 1237819
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 1237820
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->h:Ljava/util/List;

    move-object v1, v0

    .line 1237821
    :goto_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1237822
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1237823
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1237824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 1237825
    iput-object v0, v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1237826
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1237827
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237828
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->f:Ljava/lang/String;

    .line 1237829
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1237797
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1237798
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e:J

    .line 1237799
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->g:J

    .line 1237800
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1237801
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1237802
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->g:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1237803
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;-><init>()V

    .line 1237804
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1237805
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1237808
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel$SubMessageProfilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->h:Ljava/util/List;

    .line 1237809
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic d()LX/7oa;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237810
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1237811
    const v0, 0x38e80f12

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237812
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j:Ljava/lang/String;

    .line 1237813
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1237814
    const v0, 0x62c36625

    return v0
.end method
