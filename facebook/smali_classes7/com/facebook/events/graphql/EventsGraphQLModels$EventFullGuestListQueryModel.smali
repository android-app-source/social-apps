.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1a96dd20
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1246601
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1246600
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1246598
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1246599
    return-void
.end method

.method private a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246596
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    .line 1246597
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    return-object v0
.end method

.method private j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246594
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    .line 1246595
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246592
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    .line 1246593
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    return-object v0
.end method

.method private l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1246546
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    .line 1246547
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1246580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1246581
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1246582
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1246583
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1246584
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1246585
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1246586
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1246587
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1246588
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1246589
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1246590
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1246591
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1246557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1246558
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1246559
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    .line 1246560
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1246561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    .line 1246562
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventDeclinesModel;

    .line 1246563
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1246564
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    .line 1246565
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1246566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    .line 1246567
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventInviteesModel;

    .line 1246568
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1246569
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    .line 1246570
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1246571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    .line 1246572
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMaybesModel;

    .line 1246573
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1246574
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    .line 1246575
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1246576
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    .line 1246577
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$EventMembersModel;

    .line 1246578
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1246579
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1246556
    new-instance v0, LX/7pT;

    invoke-direct {v0, p1}, LX/7pT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1246554
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1246555
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1246553
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1246550
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;-><init>()V

    .line 1246551
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1246552
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1246549
    const v0, -0x3efa8081

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1246548
    const v0, 0x403827a

    return v0
.end method
