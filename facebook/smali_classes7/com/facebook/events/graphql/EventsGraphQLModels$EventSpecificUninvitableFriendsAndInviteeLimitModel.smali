.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5288d9b6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251381
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251380
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251378
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251379
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1251370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251371
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x3e343ca2

    invoke-static {v1, v0, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1251372
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1251373
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1251374
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1251375
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1251376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251377
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1251355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251356
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1251357
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3e343ca2

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1251358
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1251359
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    .line 1251360
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->e:I

    move-object v1, v0

    .line 1251361
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1251362
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    .line 1251363
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1251364
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    .line 1251365
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    .line 1251366
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251367
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1251368
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1251369
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventViewerCapability"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1251353
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251354
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1251352
    new-instance v0, LX/7q6;

    invoke-direct {v0, p1}, LX/7q6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1251339
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1251340
    const/4 v0, 0x0

    const v1, 0x3e343ca2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->e:I

    .line 1251341
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1251350
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1251351
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1251349
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251346
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;-><init>()V

    .line 1251347
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251348
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1251345
    const v0, 0x7673d9b1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1251344
    const v0, 0x403827a

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251342
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    .line 1251343
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    return-object v0
.end method
