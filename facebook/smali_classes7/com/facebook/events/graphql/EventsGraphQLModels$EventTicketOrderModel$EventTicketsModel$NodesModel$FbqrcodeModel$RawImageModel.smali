.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6bed54b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251747
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251734
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251745
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251746
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251743
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->e:Ljava/lang/String;

    .line 1251744
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251741
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1251742
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251740
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251739
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251736
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$FbqrcodeModel$RawImageModel;-><init>()V

    .line 1251737
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251738
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1251735
    const v0, -0x144f7dc0

    return v0
.end method

.method public final f()I
    .locals 1

    const v0, 0x4984e12

    return v0
.end method
