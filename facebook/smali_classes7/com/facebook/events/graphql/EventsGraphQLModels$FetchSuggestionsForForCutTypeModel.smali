.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4131fd5c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258472
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1258471
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1258469
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1258470
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1258439
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1258440
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1258441
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1258461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258462
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1258463
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1258464
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1258465
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1258466
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1258467
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258468
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1258453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1258454
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1258455
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    .line 1258456
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1258457
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;

    .line 1258458
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    .line 1258459
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1258460
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1258452
    new-instance v0, LX/7qS;

    invoke-direct {v0, p1}, LX/7qS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1258450
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    .line 1258451
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1258448
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1258449
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1258447
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1258444
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;-><init>()V

    .line 1258445
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1258446
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1258443
    const v0, -0x6aef8844

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1258442
    const v0, 0x3c2b9d5

    return v0
.end method
