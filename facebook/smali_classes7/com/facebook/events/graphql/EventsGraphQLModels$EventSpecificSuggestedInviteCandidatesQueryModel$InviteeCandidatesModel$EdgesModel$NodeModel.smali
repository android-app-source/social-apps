.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251019
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251018
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251016
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251017
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1251010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251011
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1251012
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1251013
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1251014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251015
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1251007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251009
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1251006
    new-instance v0, LX/7q4;

    invoke-direct {v0, p1}, LX/7q4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251020
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1251004
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1251005
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1251003
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251000
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;-><init>()V

    .line 1251001
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251002
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1250999
    const v0, 0x3b7f9c3a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1250998
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1250996
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1250997
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method
