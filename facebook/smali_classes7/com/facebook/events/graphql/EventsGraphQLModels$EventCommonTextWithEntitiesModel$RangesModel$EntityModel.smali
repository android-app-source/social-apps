.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/171;
.implements LX/16i;
.implements LX/16f;
.implements LX/1y9;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1db40f0a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244526
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244454
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1244455
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244456
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1244457
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244458
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244459
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1244460
    if-nez p0, :cond_0

    .line 1244461
    const/4 p0, 0x0

    .line 1244462
    :goto_0
    return-object p0

    .line 1244463
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    if-eqz v0, :cond_1

    .line 1244464
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    goto :goto_0

    .line 1244465
    :cond_1
    new-instance v2, LX/7pE;

    invoke-direct {v2}, LX/7pE;-><init>()V

    .line 1244466
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1244467
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1244468
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1244469
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1244470
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1244471
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->b:LX/0Px;

    .line 1244472
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1244473
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;->a(Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;)Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1244474
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1244475
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->f:Ljava/lang/String;

    .line 1244476
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->r()Z

    move-result v0

    iput-boolean v0, v2, LX/7pE;->g:Z

    .line 1244477
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->k()Z

    move-result v0

    iput-boolean v0, v2, LX/7pE;->h:Z

    .line 1244478
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->l()Z

    move-result v0

    iput-boolean v0, v2, LX/7pE;->i:Z

    .line 1244479
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->j:Ljava/lang/String;

    .line 1244480
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->s()LX/1yO;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;->a(LX/1yO;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->k:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 1244481
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->u()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1244482
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1244483
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1244484
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-static {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1244485
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1244486
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->m:LX/0Px;

    .line 1244487
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->n:Ljava/lang/String;

    .line 1244488
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7pE;->o:Ljava/lang/String;

    .line 1244489
    invoke-virtual {v2}, LX/7pE;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244490
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1244491
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    return-object v0
.end method

.method private w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244492
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1244493
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    return-object v0
.end method

.method private x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244494
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 1244495
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    return-object v0
.end method

.method private y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244496
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1244497
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    .line 1244539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244540
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1244541
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1244542
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1244543
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1244544
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1244545
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1244546
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1244547
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1244548
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1244549
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 1244550
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1244551
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1244552
    const/16 v12, 0xf

    invoke-virtual {p1, v12}, LX/186;->c(I)V

    .line 1244553
    const/4 v12, 0x0

    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 1244554
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1244555
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1244556
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1244557
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1244558
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1244559
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1244560
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1244561
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1244562
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1244563
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1244564
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1244565
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1244566
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1244567
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1244568
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244569
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1244498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244499
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1244500
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1244501
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1244502
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244503
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->g:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1244504
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1244505
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1244506
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1244507
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244508
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->h:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1244509
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1244510
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 1244511
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1244512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244513
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    .line 1244514
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1244515
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1244516
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1244517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244518
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->p:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    .line 1244519
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1244520
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1244521
    if-eqz v2, :cond_4

    .line 1244522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1244523
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->q:Ljava/util/List;

    move-object v1, v0

    .line 1244524
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244525
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1244423
    new-instance v0, LX/7pF;

    invoke-direct {v0, p1}, LX/7pF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244527
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1244528
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1244529
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->k:Z

    .line 1244530
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->l:Z

    .line 1244531
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->m:Z

    .line 1244532
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1244533
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1244534
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1244535
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244536
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1244537
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1244538
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1244449
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;-><init>()V

    .line 1244450
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244451
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1244452
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/util/List;

    .line 1244453
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244421
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1244422
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->i:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1244424
    const v0, 0x598c527c

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244425
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    .line 1244426
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1244427
    const v0, 0x7c02d003

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244428
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    .line 1244429
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1244430
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244431
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->l:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1244432
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244433
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->m:Z

    return v0
.end method

.method public final synthetic m()LX/3Bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244434
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()LX/1yP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244435
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1244436
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->q:Ljava/util/List;

    .line 1244437
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic p()LX/6R4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244438
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244439
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->w()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1244440
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244441
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->k:Z

    return v0
.end method

.method public final synthetic s()LX/1yO;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244442
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->x()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$PageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244443
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->v()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244444
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->y()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244445
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    .line 1244446
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244447
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    .line 1244448
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;->r:Ljava/lang/String;

    return-object v0
.end method
