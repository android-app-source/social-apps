.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1270544
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1270545
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1270546
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1270547
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1270548
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1270549
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1270550
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1270551
    if-eqz v2, :cond_0

    .line 1270552
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1270553
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1270554
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1270555
    if-eqz v2, :cond_1

    .line 1270556
    const-string p0, "event"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1270557
    invoke-static {v1, v2, p1, p2}, LX/7us;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1270558
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1270559
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1270560
    check-cast p1, Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel$Serializer;->a(Lcom/facebook/events/graphql/EventsMutationsModels$EventAdminRemoveGuestMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
