.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2794649e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1257923
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1257924
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1257921
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1257922
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257918
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1257919
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1257920
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1257910
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1257911
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1257912
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1257913
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1257914
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1257915
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1257916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1257917
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1257902
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1257903
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1257904
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    .line 1257905
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1257906
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;

    .line 1257907
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    .line 1257908
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1257909
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1257925
    new-instance v0, LX/7qP;

    invoke-direct {v0, p1}, LX/7qP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1257900
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    .line 1257901
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel$FriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1257898
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1257899
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1257897
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1257894
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPagedUpcomingBirthdaysQueryModel;-><init>()V

    .line 1257895
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1257896
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1257892
    const v0, 0x5e883435

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1257893
    const v0, 0x3c2b9d5

    return v0
.end method
