.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1259720
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1259721
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1259715
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1259716
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1259717
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/7u0;->a(LX/15i;ILX/0nX;)V

    .line 1259718
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1259719
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;LX/0nX;LX/0my;)V

    return-void
.end method
