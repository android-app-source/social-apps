.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5ce6b293
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253073
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253074
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1253075
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1253076
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1253077
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->e:I

    .line 1253078
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1253079
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1253080
    if-eqz v0, :cond_0

    .line 1253081
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1253082
    :cond_0
    return-void
.end method

.method private j()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1253099
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253100
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->e:I

    return v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253083
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->f:Ljava/lang/String;

    .line 1253084
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253085
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->g:Ljava/lang/String;

    .line 1253086
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1253087
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253088
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1253089
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1253090
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1253091
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1253092
    iget v3, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->e:I

    invoke-virtual {p1, v4, v3, v4}, LX/186;->a(III)V

    .line 1253093
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1253094
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1253095
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1253096
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->i:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1253097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1253069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253070
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253071
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1253072
    new-instance v0, LX/7qF;

    invoke-direct {v0, p1}, LX/7qF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253046
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1253047
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1253048
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->e:I

    .line 1253049
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->i:J

    .line 1253050
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1253051
    const-string v0, "available_inventory"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1253052
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1253053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1253054
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1253055
    :goto_0
    return-void

    .line 1253056
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1253057
    const-string v0, "available_inventory"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1253058
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->a(I)V

    .line 1253059
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1253060
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;-><init>()V

    .line 1253061
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1253062
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253063
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->h:Ljava/lang/String;

    .line 1253064
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1253065
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253066
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->i:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1253067
    const v0, 0x684c26b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1253068
    const v0, 0x2763d928

    return v0
.end method
