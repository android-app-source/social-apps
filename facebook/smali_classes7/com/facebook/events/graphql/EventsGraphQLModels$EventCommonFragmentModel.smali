.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oa;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3cb46292
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Z

.field private Q:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:J

.field private S:J

.field private T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:I

.field private W:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Z

.field private Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:J

.field private p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244031
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244030
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1244028
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244029
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1244025
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244026
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244027
    return-void
.end method

.method public static a(LX/7oa;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 4

    .prologue
    .line 1243965
    if-nez p0, :cond_0

    .line 1243966
    const/4 p0, 0x0

    .line 1243967
    :goto_0
    return-object p0

    .line 1243968
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    if-eqz v0, :cond_1

    .line 1243969
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    goto :goto_0

    .line 1243970
    :cond_1
    new-instance v2, LX/7p3;

    invoke-direct {v2}, LX/7p3;-><init>()V

    .line 1243971
    invoke-interface {p0}, LX/7oa;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1243972
    invoke-interface {p0}, LX/7oa;->u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1243973
    invoke-interface {p0}, LX/7oa;->v()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->c:Z

    .line 1243974
    invoke-interface {p0}, LX/7oa;->w()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->d:Z

    .line 1243975
    invoke-interface {p0}, LX/7oa;->k()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->e:Z

    .line 1243976
    invoke-interface {p0}, LX/7oa;->x()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->f:Z

    .line 1243977
    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1243978
    invoke-interface {p0}, LX/7oa;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1243979
    invoke-interface {p0}, LX/7oa;->y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1243980
    invoke-interface {p0}, LX/7oa;->z()J

    move-result-wide v0

    iput-wide v0, v2, LX/7p3;->j:J

    .line 1243981
    invoke-interface {p0}, LX/7oa;->b()J

    move-result-wide v0

    iput-wide v0, v2, LX/7p3;->k:J

    .line 1243982
    invoke-interface {p0}, LX/7oa;->A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1243983
    invoke-interface {p0}, LX/7oa;->c()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1243984
    invoke-interface {p0}, LX/7oa;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->n:Ljava/lang/String;

    .line 1243985
    invoke-interface {p0}, LX/7oa;->C()LX/7oW;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;->a(LX/7oW;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1243986
    invoke-interface {p0}, LX/7oa;->D()LX/7oc;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(LX/7oc;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1243987
    invoke-interface {p0}, LX/7oa;->E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1243988
    invoke-interface {p0}, LX/7oa;->F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1243989
    invoke-interface {p0}, LX/7oa;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->s:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1243990
    invoke-interface {p0}, LX/7oa;->H()LX/7od;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(LX/7od;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1243991
    invoke-interface {p0}, LX/7oa;->I()LX/7oe;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(LX/7oe;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1243992
    invoke-interface {p0}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1243993
    invoke-interface {p0}, LX/7oa;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1243994
    invoke-interface {p0}, LX/7oa;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->x:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1243995
    invoke-interface {p0}, LX/7oa;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->y:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 1243996
    invoke-interface {p0}, LX/7oa;->M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1243997
    invoke-interface {p0}, LX/7oa;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->A:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 1243998
    invoke-interface {p0}, LX/7oa;->O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1243999
    invoke-interface {p0}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1244000
    invoke-interface {p0}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1244001
    invoke-interface {p0}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->E:Ljava/lang/String;

    .line 1244002
    invoke-interface {p0}, LX/7oa;->eQ_()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->F:Z

    .line 1244003
    invoke-interface {p0}, LX/7oa;->R()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->G:Z

    .line 1244004
    invoke-interface {p0}, LX/7oa;->n()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->H:Z

    .line 1244005
    invoke-interface {p0}, LX/7oa;->S()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->I:Z

    .line 1244006
    invoke-interface {p0}, LX/7oa;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->J:Ljava/lang/String;

    .line 1244007
    invoke-interface {p0}, LX/7oa;->T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1244008
    invoke-interface {p0}, LX/7oa;->U()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->L:Z

    .line 1244009
    invoke-interface {p0}, LX/7oa;->V()LX/1oP;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;->a(LX/1oP;)Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->M:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1244010
    invoke-interface {p0}, LX/7oa;->o()J

    move-result-wide v0

    iput-wide v0, v2, LX/7p3;->N:J

    .line 1244011
    invoke-interface {p0}, LX/7oa;->j()J

    move-result-wide v0

    iput-wide v0, v2, LX/7p3;->O:J

    .line 1244012
    invoke-interface {p0}, LX/7oa;->W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1244013
    invoke-interface {p0}, LX/7oa;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->Q:Ljava/lang/String;

    .line 1244014
    invoke-interface {p0}, LX/7oa;->X()I

    move-result v0

    iput v0, v2, LX/7p3;->R:I

    .line 1244015
    invoke-interface {p0}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1244016
    invoke-interface {p0}, LX/7oa;->r()Z

    move-result v0

    iput-boolean v0, v2, LX/7p3;->T:Z

    .line 1244017
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1244018
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1244019
    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    invoke-static {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;->a(LX/7ob;)Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1244020
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1244021
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->U:LX/0Px;

    .line 1244022
    invoke-interface {p0}, LX/7oa;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1244023
    invoke-interface {p0}, LX/7oa;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, v2, LX/7p3;->W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1244024
    invoke-virtual {v2}, LX/7p3;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1243959
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->V:I

    .line 1243960
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243961
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243962
    if-eqz v0, :cond_0

    .line 1243963
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1243964
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1243953
    iput-wide p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R:J

    .line 1243954
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243955
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243956
    if-eqz v0, :cond_0

    .line 1243957
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x27

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1243958
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1243946
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->W:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1243947
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243948
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243949
    if-eqz v0, :cond_0

    .line 1243950
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x2c

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1243951
    :cond_0
    return-void

    .line 1243952
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1243939
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1243940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243941
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243942
    if-eqz v0, :cond_0

    .line 1243943
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x30

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1243944
    :cond_0
    return-void

    .line 1243945
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1243933
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N:Ljava/lang/String;

    .line 1243934
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243935
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243936
    if-eqz v0, :cond_0

    .line 1243937
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1243938
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1243882
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->i:Z

    .line 1243883
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243884
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243885
    if-eqz v0, :cond_0

    .line 1243886
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1243887
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1243910
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K:Z

    .line 1243911
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243912
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243913
    if-eqz v0, :cond_0

    .line 1243914
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1243915
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1243904
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L:Z

    .line 1243905
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243906
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243907
    if-eqz v0, :cond_0

    .line 1243908
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1243909
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1243898
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X:Z

    .line 1243899
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1243900
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1243901
    if-eqz v0, :cond_0

    .line 1243902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1243903
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243897
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243895
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r:Ljava/lang/String;

    .line 1243896
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()LX/7oW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243894
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()LX/7oc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243893
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243892
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243891
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243889
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1243890
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final synthetic H()LX/7od;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243888
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic I()LX/7oe;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244090
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244088
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1244089
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244295
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1244296
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244297
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 1244298
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method

.method public final synthetic M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244299
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244300
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->E:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->E:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 1244301
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->E:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method

.method public final synthetic O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244302
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244303
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244304
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final R()Z
    .locals 2

    .prologue
    .line 1244305
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244306
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K:Z

    return v0
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 1244293
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244294
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->M:Z

    return v0
.end method

.method public final synthetic T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244307
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final U()Z
    .locals 2

    .prologue
    .line 1244291
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244292
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->P:Z

    return v0
.end method

.method public final synthetic V()LX/1oP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244290
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244289
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final X()I
    .locals 2

    .prologue
    .line 1244287
    const/4 v0, 0x5

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244288
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->V:I

    return v0
.end method

.method public final Y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1244285
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y:Ljava/util/List;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y:Ljava/util/List;

    .line 1244286
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244283
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Z:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Z:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1244284
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Z:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 37

    .prologue
    .line 1244196
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244197
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1244198
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1244199
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1244200
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1244201
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1244202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1244203
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1244204
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->B()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1244205
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1244206
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1244207
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1244208
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1244209
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1244210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1244211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1244212
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1244213
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1244214
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1244215
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1244216
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1244217
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    .line 1244218
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1244219
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1244220
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1244221
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1244222
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1244223
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1244224
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1244225
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1244226
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 1244227
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1244228
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v34

    .line 1244229
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    .line 1244230
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1244231
    const/16 v7, 0x31

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1244232
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 1244233
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1244234
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244235
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244236
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244237
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244238
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1244239
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1244240
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1244241
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->n:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1244242
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->o:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1244243
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1244244
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1244245
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1244246
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1244247
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1244248
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1244249
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1244250
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1244251
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244252
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244253
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244254
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244255
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244256
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244257
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244258
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244259
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244260
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244261
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244262
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244263
    const/16 v2, 0x1f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->J:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244264
    const/16 v2, 0x20

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244265
    const/16 v2, 0x21

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244266
    const/16 v2, 0x22

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->M:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244267
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244268
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244269
    const/16 v2, 0x25

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->P:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244270
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244271
    const/16 v3, 0x27

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1244272
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->S:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1244273
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244274
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244275
    const/16 v2, 0x2b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->V:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1244276
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244277
    const/16 v2, 0x2d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1244278
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244279
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244280
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1244281
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244282
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1244093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244094
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1244095
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1244096
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1244097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244098
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1244099
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1244100
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1244101
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1244102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244103
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1244104
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1244105
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1244106
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1244107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244108
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1244109
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1244110
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1244111
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1244112
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244113
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1244114
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1244115
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1244116
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1244117
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244118
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1244119
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1244120
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1244121
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1244122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244123
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1244124
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1244125
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1244126
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1244127
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244128
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1244129
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1244130
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1244131
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1244132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244133
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1244134
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1244135
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1244136
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1244137
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244138
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1244139
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1244140
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1244141
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1244142
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244143
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1244144
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1244145
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1244146
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1244147
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244148
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1244149
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1244150
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1244151
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1244152
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244153
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1244154
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1244155
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1244156
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1244157
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244158
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1244159
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1244160
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1244161
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1244162
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244163
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1244164
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1244165
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1244166
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1244167
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244168
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1244169
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1244170
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1244171
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1244172
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244173
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1244174
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1244175
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1244176
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1244177
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244178
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1244179
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1244180
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1244181
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1244182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244183
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Q:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1244184
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1244185
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1244186
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1244187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244188
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1244189
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1244190
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1244191
    if-eqz v2, :cond_13

    .line 1244192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1244193
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Y:Ljava/util/List;

    move-object v1, v0

    .line 1244194
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244195
    if-nez v1, :cond_14

    :goto_0
    return-object p0

    :cond_14
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1244092
    new-instance v0, LX/7p9;

    invoke-direct {v0, p1}, LX/7p9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1244091
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1243916
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1243917
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->g:Z

    .line 1243918
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->h:Z

    .line 1243919
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->i:Z

    .line 1243920
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j:Z

    .line 1243921
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->n:J

    .line 1243922
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->o:J

    .line 1243923
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->J:Z

    .line 1243924
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->K:Z

    .line 1243925
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L:Z

    .line 1243926
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->M:Z

    .line 1243927
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->P:Z

    .line 1243928
    const/16 v0, 0x27

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R:J

    .line 1243929
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->S:J

    .line 1243930
    const/16 v0, 0x2b

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->V:I

    .line 1243931
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X:Z

    .line 1243932
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1244032
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244033
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244035
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1244036
    :goto_0
    return-void

    .line 1244037
    :cond_0
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1244038
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1244039
    if-eqz v0, :cond_b

    .line 1244040
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244041
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244042
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1244043
    :cond_1
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1244044
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1244045
    if-eqz v0, :cond_b

    .line 1244046
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244047
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244048
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1244049
    :cond_2
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1244050
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1244051
    if-eqz v0, :cond_b

    .line 1244052
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244053
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244054
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1244055
    :cond_3
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1244056
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244057
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244058
    const/16 v0, 0x20

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1244059
    :cond_4
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1244060
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244062
    const/16 v0, 0x21

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244063
    :cond_5
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1244064
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244065
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244066
    const/16 v0, 0x23

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244067
    :cond_6
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1244068
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244070
    const/16 v0, 0x27

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244071
    :cond_7
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1244072
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244074
    const/16 v0, 0x2b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244075
    :cond_8
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1244076
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244077
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244078
    const/16 v0, 0x2c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244079
    :cond_9
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1244080
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244082
    const/16 v0, 0x2d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244083
    :cond_a
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1244084
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1244085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1244086
    const/16 v0, 0x30

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1244087
    :cond_b
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1243756
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1243757
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(Z)V

    .line 1243758
    :cond_0
    :goto_0
    return-void

    .line 1243759
    :cond_1
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1243760
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1243761
    if-eqz v0, :cond_0

    .line 1243762
    if-eqz p3, :cond_2

    .line 1243763
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1243764
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    .line 1243765
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    goto :goto_0

    .line 1243766
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1243767
    :cond_3
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1243768
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1243769
    if-eqz v0, :cond_0

    .line 1243770
    if-eqz p3, :cond_4

    .line 1243771
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1243772
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    .line 1243773
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    goto :goto_0

    .line 1243774
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1243775
    :cond_5
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1243776
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1243777
    if-eqz v0, :cond_0

    .line 1243778
    if-eqz p3, :cond_6

    .line 1243779
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1243780
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    .line 1243781
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    goto :goto_0

    .line 1243782
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    goto/16 :goto_0

    .line 1243783
    :cond_7
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1243784
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->b(Z)V

    goto/16 :goto_0

    .line 1243785
    :cond_8
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1243786
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->c(Z)V

    goto/16 :goto_0

    .line 1243787
    :cond_9
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1243788
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1243789
    :cond_a
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1243790
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(J)V

    goto/16 :goto_0

    .line 1243791
    :cond_b
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1243792
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(I)V

    goto/16 :goto_0

    .line 1243793
    :cond_c
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1243794
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 1243795
    :cond_d
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1243796
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->d(Z)V

    goto/16 :goto_0

    .line 1243797
    :cond_e
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1243798
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243840
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1243841
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    return-object v0
.end method

.method public final ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243838
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1243839
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    return-object v0
.end method

.method public final ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243836
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1243837
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    return-object v0
.end method

.method public final ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243834
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1243835
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    return-object v0
.end method

.method public final ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243832
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1243833
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final af()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243830
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1243831
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    return-object v0
.end method

.method public final ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243828
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1243829
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    return-object v0
.end method

.method public final ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243826
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1243827
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    return-object v0
.end method

.method public final ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243824
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1243825
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    return-object v0
.end method

.method public final aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243822
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1243823
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    return-object v0
.end method

.method public final ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243820
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1243821
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    return-object v0
.end method

.method public final al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243818
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1243819
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method public final am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243816
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1243817
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    return-object v0
.end method

.method public final an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243814
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1243815
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->F:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    return-object v0
.end method

.method public final ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243812
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1243813
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    return-object v0
.end method

.method public final ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243810
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1243811
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    return-object v0
.end method

.method public final aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243808
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1243809
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    return-object v0
.end method

.method public final ar()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243806
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Q:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Q:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1243807
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->Q:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    return-object v0
.end method

.method public final as()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243804
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1243805
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1243802
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243803
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->o:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1243799
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;-><init>()V

    .line 1243800
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1243801
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243755
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ae()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243864
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1243881
    const v0, -0x6fa539e

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243879
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->I:Ljava/lang/String;

    .line 1243880
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1243877
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243878
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->J:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243875
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N:Ljava/lang/String;

    .line 1243876
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1243874
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1243872
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243873
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->S:J

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1243870
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243871
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->i:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243868
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1243869
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243867
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1243865
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243866
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->L:Z

    return v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 1243842
    const/4 v0, 0x4

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243843
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->R:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243862
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->U:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->U:Ljava/lang/String;

    .line 1243863
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243860
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->W:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->W:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1243861
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->W:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 1243858
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243859
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->X:Z

    return v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243856
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1243857
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243854
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1243855
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243853
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 1243851
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243852
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->g:Z

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1243849
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243850
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->h:Z

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 1243847
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243848
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->j:Z

    return v0
.end method

.method public final synthetic y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1243846
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()J
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1243844
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1243845
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->n:J

    return-wide v0
.end method
