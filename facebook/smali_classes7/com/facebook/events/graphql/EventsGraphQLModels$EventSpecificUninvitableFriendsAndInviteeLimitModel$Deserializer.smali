.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1251129
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1251130
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1251131
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1251132
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1251133
    const/4 v2, 0x0

    .line 1251134
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1251135
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1251136
    :goto_0
    move v1, v2

    .line 1251137
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1251138
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1251139
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;-><init>()V

    .line 1251140
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1251141
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1251142
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1251143
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1251144
    :cond_0
    return-object v1

    .line 1251145
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1251146
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1251147
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1251148
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1251149
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1251150
    const-string v5, "event_viewer_capability"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1251151
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1251152
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 1251153
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1251154
    :goto_2
    move v3, v4

    .line 1251155
    goto :goto_1

    .line 1251156
    :cond_3
    const-string v5, "uninvitable_friends"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1251157
    invoke-static {p1, v0}, LX/7sJ;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1251158
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1251159
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1251160
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1251161
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1251162
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 1251163
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1251164
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1251165
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v7, :cond_6

    .line 1251166
    const-string p0, "remaining_invites"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1251167
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v5

    goto :goto_3

    .line 1251168
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1251169
    :cond_8
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1251170
    if-eqz v3, :cond_9

    .line 1251171
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1251172
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v6, v4

    goto :goto_3
.end method
