.class public final Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x64712e22
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1270375
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1270385
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1270383
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1270384
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270380
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1270381
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1270382
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270378
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 1270379
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270376
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->h:Ljava/lang/String;

    .line 1270377
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1270349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1270350
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1270351
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->k()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1270352
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1270353
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1270354
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1270355
    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->f:I

    invoke-virtual {p1, v0, v3, v4}, LX/186;->a(III)V

    .line 1270356
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1270357
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1270358
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1270359
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1270372
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1270373
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1270374
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1270386
    new-instance v0, LX/7uS;

    invoke-direct {v0, p1}, LX/7uS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270371
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1270368
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1270369
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;->f:I

    .line 1270370
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1270366
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1270367
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1270365
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1270362
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;-><init>()V

    .line 1270363
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1270364
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1270361
    const v0, -0x693688dd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1270360
    const v0, 0x50c72189

    return v0
.end method
