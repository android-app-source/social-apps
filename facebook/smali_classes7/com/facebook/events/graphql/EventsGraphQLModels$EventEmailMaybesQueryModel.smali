.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52590d69
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245091
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245090
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245088
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245089
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1245082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245083
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1245084
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1245085
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1245086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245087
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1245074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245075
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245076
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    .line 1245077
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1245078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;

    .line 1245079
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    .line 1245080
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245081
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1245073
    new-instance v0, LX/7pI;

    invoke-direct {v0, p1}, LX/7pI;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventEmailAssociates"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245063
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    .line 1245064
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1245071
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1245072
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1245070
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245067
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;-><init>()V

    .line 1245068
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245069
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245066
    const v0, -0x4aa5082c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245065
    const v0, 0x403827a

    return v0
.end method
