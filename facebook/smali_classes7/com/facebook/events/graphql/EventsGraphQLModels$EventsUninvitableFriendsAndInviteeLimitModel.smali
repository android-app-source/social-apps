.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x78afd248
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255261
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255260
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1255258
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1255259
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1255256
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1255257
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1255249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255250
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1255251
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1255252
    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1255253
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1255254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255255
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1255262
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255263
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1255264
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    .line 1255265
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1255266
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;

    .line 1255267
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    .line 1255268
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255269
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1255246
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1255247
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->e:I

    .line 1255248
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1255243
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;-><init>()V

    .line 1255244
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1255245
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1255242
    const v0, -0x2fd76a8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1255239
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1255240
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    .line 1255241
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    return-object v0
.end method
