.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251754
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251753
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    new-instance v0, LX/7q9;

    invoke-direct {v0, p1}, LX/7q9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    invoke-virtual {p2}, LX/18L;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;-><init>()V

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$EventTicketsModel$NodesModel$TicketTierModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    const v0, 0x73b1755a

    return v0
.end method

.method public final f()I
    .locals 1

    const v0, 0x2763d928

    return v0
.end method
