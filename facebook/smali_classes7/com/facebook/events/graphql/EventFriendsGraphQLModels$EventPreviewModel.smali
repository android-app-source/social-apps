.class public final Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7nK;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x140b18ea
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239501
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239503
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1239504
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1239505
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1239506
    iput-object p1, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->i:Ljava/lang/String;

    .line 1239507
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1239508
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1239509
    if-eqz v0, :cond_0

    .line 1239510
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1239511
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239512
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239513
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239514
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239515
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1239516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239517
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1239518
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1239519
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1239520
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1239521
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->eN_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1239522
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1239523
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1239524
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1239525
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1239526
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1239527
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1239528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239529
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1239530
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239531
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1239532
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239533
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1239534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    .line 1239535
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1239536
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1239537
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239538
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1239539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    .line 1239540
    iput-object v0, v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->f:Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    .line 1239541
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239542
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1239543
    new-instance v0, LX/7nP;

    invoke-direct {v0, p1}, LX/7nP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239502
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1239479
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239480
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->eN_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1239481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1239482
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1239483
    :goto_0
    return-void

    .line 1239484
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1239486
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239487
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->a(Ljava/lang/String;)V

    .line 1239488
    :cond_0
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239489
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1239490
    new-instance v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;-><init>()V

    .line 1239491
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1239492
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239493
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239494
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->g:Ljava/lang/String;

    .line 1239495
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1239496
    const v0, -0x65492d40

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239497
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->h:Ljava/lang/String;

    .line 1239498
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final eN_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239499
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->i:Ljava/lang/String;

    .line 1239500
    iget-object v0, p0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1239485
    const v0, 0x403827a

    return v0
.end method
