.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel;LX/0nX;LX/0my;)V
    .locals 8

    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, LX/0nX;->f()V

    invoke-virtual {v1, v0, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "buyer_email"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "buyer_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_3

    const-string v3, "credit_card_used"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-static {v1, v2, p1}, LX/7sP;->a(LX/15i;ILX/0nX;)V

    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_4

    const-string v3, "event_tickets"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-static {v1, v2, p1, p2}, LX/7sV;->a(LX/15i;ILX/0nX;LX/0my;)V

    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_6

    const-string v3, "order_action_link"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-static {v1, v2, p1}, LX/7sW;->a(LX/15i;ILX/0nX;)V

    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_7

    const-string v3, "ticket_order_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-static {v1, v2, p1, p2}, LX/7sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    if-eqz v2, :cond_8

    const-string v3, "tickets_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_9

    const-string v3, "total_order_price"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    invoke-static {v1, v2, p1}, LX/7sY;->a(LX/15i;ILX/0nX;)V

    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderModel;LX/0nX;LX/0my;)V

    return-void
.end method
