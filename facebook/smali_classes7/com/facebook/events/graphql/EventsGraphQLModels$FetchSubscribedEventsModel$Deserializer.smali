.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1258186
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1258187
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1258188
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1258189
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1258190
    const/4 v2, 0x0

    .line 1258191
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1258192
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1258193
    :goto_0
    move v1, v2

    .line 1258194
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1258195
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1258196
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;-><init>()V

    .line 1258197
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1258198
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1258199
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1258200
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1258201
    :cond_0
    return-object v1

    .line 1258202
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1258203
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1258204
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1258205
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1258206
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1258207
    const-string v5, "subscribed_calendar_profiles"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1258208
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1258209
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 1258210
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1258211
    :goto_2
    move v3, v4

    .line 1258212
    goto :goto_1

    .line 1258213
    :cond_3
    const-string v5, "subscribed_profile_calendar_events"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1258214
    invoke-static {p1, v0}, LX/7to;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1258215
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1258216
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1258217
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1258218
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1258219
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_8

    .line 1258220
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1258221
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1258222
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v7, :cond_6

    .line 1258223
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1258224
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v5

    goto :goto_3

    .line 1258225
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1258226
    :cond_8
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1258227
    if-eqz v3, :cond_9

    .line 1258228
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1258229
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v6, v4

    goto :goto_3
.end method
