.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16393afd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245646
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245645
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245643
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245644
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1245635
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245636
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1245637
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1245638
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1245639
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1245640
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1245641
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245642
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1245647
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245648
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245649
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1245650
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1245651
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;

    .line 1245652
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1245653
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245654
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245633
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1245634
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245630
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;-><init>()V

    .line 1245631
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245632
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245629
    const v0, -0x66ff9916

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245628
    const v0, -0x1794ec7e

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245626
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1245627
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method
