.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4297d97b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245993
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245996
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245994
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245995
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1245987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245988
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1245989
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1245990
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1245991
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245992
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1245997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245998
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245999
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1246000
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1246001
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;

    .line 1246002
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1246003
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1246004
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245985
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1245986
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245980
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;-><init>()V

    .line 1245981
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245982
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245984
    const v0, -0x157ceeb5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245983
    const v0, 0x36543c2e

    return v0
.end method
