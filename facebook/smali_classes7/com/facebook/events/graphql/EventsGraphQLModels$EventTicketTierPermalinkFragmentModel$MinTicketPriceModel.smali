.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x475a35
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252990
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252993
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1252991
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1252992
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252988
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->f:Ljava/lang/String;

    .line 1252989
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1252994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252995
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1252996
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1252997
    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1252998
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1252999
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1253000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253001
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1252985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1252987
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1252981
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1252982
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->e:I

    .line 1252983
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;->g:I

    .line 1252984
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1252976
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;-><init>()V

    .line 1252977
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1252978
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1252980
    const v0, -0x28835a8b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1252979
    const v0, 0x2cee5bdc

    return v0
.end method
