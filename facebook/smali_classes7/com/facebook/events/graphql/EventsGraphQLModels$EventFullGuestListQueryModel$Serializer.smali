.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1246521
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1246522
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1246523
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1246524
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1246525
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1246526
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1246527
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246528
    if-eqz v2, :cond_0

    .line 1246529
    const-string p0, "event_declines"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246530
    invoke-static {v1, v2, p1, p2}, LX/7rQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1246531
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246532
    if-eqz v2, :cond_1

    .line 1246533
    const-string p0, "event_invitees"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246534
    invoke-static {v1, v2, p1, p2}, LX/7rS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1246535
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246536
    if-eqz v2, :cond_2

    .line 1246537
    const-string p0, "event_maybes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246538
    invoke-static {v1, v2, p1, p2}, LX/7rU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1246539
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246540
    if-eqz v2, :cond_3

    .line 1246541
    const-string p0, "event_members"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246542
    invoke-static {v1, v2, p1, p2}, LX/7rW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1246543
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1246544
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1246545
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventFullGuestListQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
