.class public final Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1270265
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1270266
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1270277
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1270268
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1270269
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1270270
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1270271
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1270272
    if-eqz p0, :cond_0

    .line 1270273
    const-string p2, "canceled_event_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1270274
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1270275
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1270276
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1270267
    check-cast p1, Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel$Serializer;->a(Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
