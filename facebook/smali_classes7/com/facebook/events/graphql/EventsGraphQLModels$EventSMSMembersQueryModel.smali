.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64fafa41
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249939
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249940
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1249941
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1249942
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1249943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249944
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1249945
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1249946
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1249947
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249948
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1249949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249950
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1249951
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    .line 1249952
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1249953
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;

    .line 1249954
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    .line 1249955
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249956
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1249957
    new-instance v0, LX/7ps;

    invoke-direct {v0, p1}, LX/7ps;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventSmsMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249958
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    .line 1249959
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1249960
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1249961
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1249962
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1249963
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;-><init>()V

    .line 1249964
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1249965
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1249966
    const v0, 0x172e115d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1249967
    const v0, 0x403827a

    return v0
.end method
