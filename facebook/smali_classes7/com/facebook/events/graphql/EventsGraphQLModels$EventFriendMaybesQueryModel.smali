.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x620840cd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245741
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1245715
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1245739
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1245740
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1245733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245734
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1245735
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1245736
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1245737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1245725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1245726
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1245727
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    .line 1245728
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1245729
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;

    .line 1245730
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    .line 1245731
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1245732
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1245724
    new-instance v0, LX/7pM;

    invoke-direct {v0, p1}, LX/7pM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendEventMaybes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1245742
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    .line 1245743
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1245722
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1245723
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1245721
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1245718
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;-><init>()V

    .line 1245719
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1245720
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1245717
    const v0, 0x20013bd7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1245716
    const v0, 0x403827a

    return v0
.end method
