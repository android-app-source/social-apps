.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x79a0ef70
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238074
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238073
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1238071
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1238072
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1238061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238062
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1238063
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1238064
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1238065
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1238066
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1238067
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1238068
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1238069
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238070
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1238058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238060
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238056
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->e:Ljava/lang/String;

    .line 1238057
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1238047
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;-><init>()V

    .line 1238048
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1238049
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1238055
    const v0, -0x75f78772

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1238054
    const v0, 0x1d5f48f2

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238052
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->f:Ljava/lang/String;

    .line 1238053
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238050
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->g:Ljava/lang/String;

    .line 1238051
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->g:Ljava/lang/String;

    return-object v0
.end method
