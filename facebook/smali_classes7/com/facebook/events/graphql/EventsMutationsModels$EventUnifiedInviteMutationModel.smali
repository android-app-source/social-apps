.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5ab198ed
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272213
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1272212
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1272210
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1272211
    return-void
.end method

.method private a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1272208
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    .line 1272209
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1272214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272215
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1272216
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1272217
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1272218
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272219
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1272200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1272201
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1272202
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    .line 1272203
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1272204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;

    .line 1272205
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;->e:Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel$EventModel;

    .line 1272206
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1272207
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1272197
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventUnifiedInviteMutationModel;-><init>()V

    .line 1272198
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1272199
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1272196
    const v0, -0x20e4769c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1272195
    const v0, -0xa462714

    return v0
.end method
