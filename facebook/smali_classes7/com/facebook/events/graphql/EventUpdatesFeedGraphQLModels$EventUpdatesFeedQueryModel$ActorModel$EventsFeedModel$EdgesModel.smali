.class public final Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7a53241e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239942
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1239941
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1239939
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1239940
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239937
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->e:Ljava/lang/String;

    .line 1239938
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1239943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239944
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1239945
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-virtual {p1, v1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v1

    .line 1239946
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1239947
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1239948
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1239949
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239950
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1239929
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1239930
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1239931
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1239932
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1239933
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;

    .line 1239934
    iput-object v0, v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1239935
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1239936
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1239927
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v1, 0x1

    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILX/16a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    .line 1239928
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->f:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1239924
    new-instance v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;-><init>()V

    .line 1239925
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1239926
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1239922
    const v0, -0x70a39a8d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1239923
    const v0, 0x2eb2d42b

    return v0
.end method
