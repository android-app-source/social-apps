.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oa;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x504af6c3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Z

.field private R:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:J

.field private T:J

.field private U:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:I

.field private X:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:Z

.field private Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:J

.field private p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259517
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259516
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1259514
    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1259515
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1259508
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->W:I

    .line 1259509
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259510
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259511
    if-eqz v0, :cond_0

    .line 1259512
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1259513
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1259502
    iput-wide p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->S:J

    .line 1259503
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259504
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259505
    if-eqz v0, :cond_0

    .line 1259506
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1259507
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1259495
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->X:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1259496
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259497
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259498
    if-eqz v0, :cond_0

    .line 1259499
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x2d

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1259500
    :cond_0
    return-void

    .line 1259501
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1259488
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1259489
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259490
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259491
    if-eqz v0, :cond_0

    .line 1259492
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x31

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1259493
    :cond_0
    return-void

    .line 1259494
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1259482
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->O:Ljava/lang/String;

    .line 1259483
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259484
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259485
    if-eqz v0, :cond_0

    .line 1259486
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1259487
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1259476
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->i:Z

    .line 1259477
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259478
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259479
    if-eqz v0, :cond_0

    .line 1259480
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1259481
    :cond_0
    return-void
.end method

.method private ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259474
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1259475
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    return-object v0
.end method

.method private ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259449
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1259450
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    return-object v0
.end method

.method private ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259471
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1259472
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    return-object v0
.end method

.method private af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259469
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1259470
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259467
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1259468
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    return-object v0
.end method

.method private ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259465
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1259466
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    return-object v0
.end method

.method private ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259463
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1259464
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    return-object v0
.end method

.method private aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259461
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1259462
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    return-object v0
.end method

.method private ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259459
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1259460
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    return-object v0
.end method

.method private al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259457
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1259458
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    return-object v0
.end method

.method private am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259455
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1259456
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method private an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259453
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1259454
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    return-object v0
.end method

.method private ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259451
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1259452
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    return-object v0
.end method

.method private ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259519
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1259520
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    return-object v0
.end method

.method private aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259537
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1259538
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    return-object v0
.end method

.method private ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259068
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1259069
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    return-object v0
.end method

.method private as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259539
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->R:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->R:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1259540
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->R:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    return-object v0
.end method

.method private at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259535
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->U:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->U:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1259536
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->U:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1259542
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->L:Z

    .line 1259543
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259544
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259545
    if-eqz v0, :cond_0

    .line 1259546
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1259547
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1259548
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->M:Z

    .line 1259549
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259550
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259551
    if-eqz v0, :cond_0

    .line 1259552
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1259553
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1259554
    iput-boolean p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y:Z

    .line 1259555
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1259556
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1259557
    if-eqz v0, :cond_0

    .line 1259558
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1259559
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259560
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259561
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->r:Ljava/lang/String;

    .line 1259562
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic C()LX/7oW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259563
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()LX/7oc;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259564
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259541
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259534
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259532
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1259533
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final synthetic H()LX/7od;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259531
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic I()LX/7oe;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259518
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259529
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1259530
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->A:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259527
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 1259528
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->B:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259525
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 1259526
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->C:Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method

.method public final synthetic M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259524
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259522
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->F:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->F:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 1259523
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->F:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method

.method public final synthetic O()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259473
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259521
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259067
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    return-object v0
.end method

.method public final R()Z
    .locals 2

    .prologue
    .line 1259407
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259408
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->L:Z

    return v0
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 1259405
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259406
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->N:Z

    return v0
.end method

.method public final synthetic T()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259404
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final U()Z
    .locals 2

    .prologue
    .line 1259402
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259403
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Q:Z

    return v0
.end method

.method public final synthetic V()LX/1oP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259401
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259400
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final X()I
    .locals 2

    .prologue
    .line 1259398
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259399
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->W:I

    return v0
.end method

.method public final Y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1259396
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z:Ljava/util/List;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z:Ljava/util/List;

    .line 1259397
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final Z()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259394
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1259395
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 38

    .prologue
    .line 1259305
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259306
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1259307
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1259308
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1259309
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1259310
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1259311
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1259312
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1259313
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->B()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1259314
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1259315
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1259316
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1259317
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1259318
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1259319
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1259320
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1259321
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1259322
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1259323
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1259324
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1259325
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1259326
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1259327
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1259328
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1259329
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1259330
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1259331
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1259332
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->eR_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 1259333
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1259334
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1259335
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1259336
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 1259337
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    .line 1259338
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v35

    .line 1259339
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1259340
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v37

    .line 1259341
    const/16 v7, 0x32

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1259342
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2}, LX/186;->b(II)V

    .line 1259343
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1259344
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259345
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259346
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259347
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259348
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1259349
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1259350
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1259351
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->n:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1259352
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->o:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1259353
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1259354
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1259355
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1259356
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1259357
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1259358
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1259359
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1259360
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1259361
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259362
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259363
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259364
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259365
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259366
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259367
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259368
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259369
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259370
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259371
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259372
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259373
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259374
    const/16 v2, 0x20

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259375
    const/16 v2, 0x21

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->L:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259376
    const/16 v2, 0x22

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->M:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259377
    const/16 v2, 0x23

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->N:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259378
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259379
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259380
    const/16 v2, 0x26

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259381
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259382
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->S:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1259383
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->T:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1259384
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259385
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259386
    const/16 v2, 0x2c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->W:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1259387
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259388
    const/16 v2, 0x2e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1259389
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259390
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259391
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1259392
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259393
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1259197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259198
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1259199
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1259200
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1259201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259202
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 1259203
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1259204
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1259205
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1259206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259207
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1259208
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1259209
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1259210
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1259211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259212
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 1259213
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1259214
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1259215
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ae()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1259216
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259217
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 1259218
    :cond_3
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1259219
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1259220
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1259221
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259222
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1259223
    :cond_4
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1259224
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1259225
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ag()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1259226
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259227
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 1259228
    :cond_5
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1259229
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1259230
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1259231
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259232
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1259233
    :cond_6
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1259234
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1259235
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ai()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1259236
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259237
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1259238
    :cond_7
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1259239
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1259240
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1259241
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259242
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 1259243
    :cond_8
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1259244
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1259245
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1259246
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259247
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1259248
    :cond_9
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1259249
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1259250
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1259251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259252
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1259253
    :cond_a
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1259254
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1259255
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1259256
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259257
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1259258
    :cond_b
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1259259
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1259260
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->an()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1259261
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259262
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 1259263
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1259264
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    .line 1259265
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1259266
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259267
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    .line 1259268
    :cond_d
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1259269
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1259270
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1259271
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259272
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1259273
    :cond_e
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1259274
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1259275
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1259276
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259277
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1259278
    :cond_f
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1259279
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1259280
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1259281
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259282
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1259283
    :cond_10
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1259284
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1259285
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ar()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1259286
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259287
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 1259288
    :cond_11
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1259289
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1259290
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->as()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1259291
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259292
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->R:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 1259293
    :cond_12
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1259294
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1259295
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->at()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1259296
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259297
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->U:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1259298
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1259299
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1259300
    if-eqz v2, :cond_14

    .line 1259301
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 1259302
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z:Ljava/util/List;

    move-object v1, v0

    .line 1259303
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259304
    if-nez v1, :cond_15

    :goto_0
    return-object p0

    :cond_15
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1259196
    new-instance v0, LX/7qY;

    invoke-direct {v0, p1}, LX/7qY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259195
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1259178
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1259179
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->g:Z

    .line 1259180
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->h:Z

    .line 1259181
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->i:Z

    .line 1259182
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->j:Z

    .line 1259183
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->n:J

    .line 1259184
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->o:J

    .line 1259185
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->K:Z

    .line 1259186
    const/16 v0, 0x21

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->L:Z

    .line 1259187
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->M:Z

    .line 1259188
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->N:Z

    .line 1259189
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Q:Z

    .line 1259190
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->S:J

    .line 1259191
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->T:J

    .line 1259192
    const/16 v0, 0x2c

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->W:I

    .line 1259193
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y:Z

    .line 1259194
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1259122
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1259123
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259125
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1259126
    :goto_0
    return-void

    .line 1259127
    :cond_0
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1259128
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1259129
    if-eqz v0, :cond_b

    .line 1259130
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259131
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259132
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1259133
    :cond_1
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1259134
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1259135
    if-eqz v0, :cond_b

    .line 1259136
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259137
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259138
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1259139
    :cond_2
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1259140
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1259141
    if-eqz v0, :cond_b

    .line 1259142
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259143
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259144
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1259145
    :cond_3
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1259146
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->R()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259148
    const/16 v0, 0x21

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1259149
    :cond_4
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1259150
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259151
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259152
    const/16 v0, 0x22

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259153
    :cond_5
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1259154
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259156
    const/16 v0, 0x24

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259157
    :cond_6
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1259158
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259159
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259160
    const/16 v0, 0x28

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259161
    :cond_7
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1259162
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->X()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259163
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259164
    const/16 v0, 0x2c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259165
    :cond_8
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1259166
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259167
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259168
    const/16 v0, 0x2d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259169
    :cond_9
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1259170
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259171
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259172
    const/16 v0, 0x2e

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259173
    :cond_a
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1259174
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1259175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1259176
    const/16 v0, 0x31

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1259177
    :cond_b
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1259079
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1259080
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(Z)V

    .line 1259081
    :cond_0
    :goto_0
    return-void

    .line 1259082
    :cond_1
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1259083
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ah()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1259084
    if-eqz v0, :cond_0

    .line 1259085
    if-eqz p3, :cond_2

    .line 1259086
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1259087
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    .line 1259088
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    goto :goto_0

    .line 1259089
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1259090
    :cond_3
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1259091
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1259092
    if-eqz v0, :cond_0

    .line 1259093
    if-eqz p3, :cond_4

    .line 1259094
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1259095
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    .line 1259096
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    goto :goto_0

    .line 1259097
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1259098
    :cond_5
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1259099
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1259100
    if-eqz v0, :cond_0

    .line 1259101
    if-eqz p3, :cond_6

    .line 1259102
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1259103
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    .line 1259104
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    goto :goto_0

    .line 1259105
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    goto/16 :goto_0

    .line 1259106
    :cond_7
    const-string v0, "is_canceled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1259107
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->b(Z)V

    goto/16 :goto_0

    .line 1259108
    :cond_8
    const-string v0, "is_event_draft"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1259109
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->c(Z)V

    goto/16 :goto_0

    .line 1259110
    :cond_9
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1259111
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1259112
    :cond_a
    const-string v0, "scheduled_publish_timestamp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1259113
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(J)V

    goto/16 :goto_0

    .line 1259114
    :cond_b
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1259115
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(I)V

    goto/16 :goto_0

    .line 1259116
    :cond_c
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1259117
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 1259118
    :cond_d
    const-string v0, "viewer_has_pending_invite"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1259119
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->d(Z)V

    goto/16 :goto_0

    .line 1259120
    :cond_e
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1259121
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259077
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 1259078
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    return-object v0
.end method

.method public final ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259075
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    .line 1259076
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1259073
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259074
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->o:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1259070
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;-><init>()V

    .line 1259071
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1259072
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259066
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->af()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259431
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1259448
    const v0, -0x65901297    # -4.961582E-23f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259446
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->J:Ljava/lang/String;

    .line 1259447
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1259444
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259445
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->K:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259442
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->O:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->O:Ljava/lang/String;

    .line 1259443
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1259441
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1259439
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259440
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->T:J

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1259437
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259438
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->i:Z

    return v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259435
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1259436
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259434
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1259432
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259433
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->M:Z

    return v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 1259409
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259410
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->S:J

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259429
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->V:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->V:Ljava/lang/String;

    .line 1259430
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259427
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->X:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->X:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1259428
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->X:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1259425
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259426
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Y:Z

    return v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259423
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1259424
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259421
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1259422
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259420
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ac()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 1259418
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259419
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->g:Z

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1259416
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259417
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->h:Z

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 1259414
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259415
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->j:Z

    return v0
.end method

.method public final synthetic y()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259413
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ad()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()J
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1259411
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1259412
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->n:J

    return-wide v0
.end method
