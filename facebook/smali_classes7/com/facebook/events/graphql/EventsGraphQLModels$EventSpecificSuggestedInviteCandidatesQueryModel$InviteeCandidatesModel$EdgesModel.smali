.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47f145e9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251052
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251051
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251049
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251050
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1251028
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251029
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1251030
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1251031
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1251032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251033
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1251041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251042
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1251043
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1251044
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1251045
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;

    .line 1251046
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1251047
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251048
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251039
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    .line 1251040
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251036
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificSuggestedInviteCandidatesQueryModel$InviteeCandidatesModel$EdgesModel;-><init>()V

    .line 1251037
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251038
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1251035
    const v0, 0x273a2a4b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1251034
    const v0, 0x6eadcf4

    return v0
.end method
