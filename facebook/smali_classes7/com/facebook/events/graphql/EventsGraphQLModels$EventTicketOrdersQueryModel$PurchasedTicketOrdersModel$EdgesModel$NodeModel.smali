.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2de5b398
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251953
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1251952
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1251950
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1251951
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1251942
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251943
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1251944
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1251945
    iget-wide v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1251946
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1251947
    const/4 v0, 0x2

    iget v2, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->g:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1251948
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251949
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1251939
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1251940
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1251941
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251938
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1251934
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1251935
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->e:J

    .line 1251936
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->g:I

    .line 1251937
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1251931
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;-><init>()V

    .line 1251932
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1251933
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1251930
    const v0, 0x2ccdb33b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1251929
    const v0, -0x3b2b1738

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1251923
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251924
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->e:J

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1251927
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1251928
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1251925
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1251926
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->g:I

    return v0
.end method
