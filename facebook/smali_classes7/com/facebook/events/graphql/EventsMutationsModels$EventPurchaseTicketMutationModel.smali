.class public final Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58273e7e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271671
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1271670
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1271668
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1271669
    return-void
.end method

.method private a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271666
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1271667
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271664
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->f:Ljava/lang/String;

    .line 1271665
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271672
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1271673
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    return-object v0
.end method

.method private l()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOrder"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271662
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    .line 1271663
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1271660
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->i:Ljava/lang/String;

    .line 1271661
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1271646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271647
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1271648
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1271649
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1271650
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1271651
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1271652
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1271653
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1271654
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1271655
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1271656
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1271657
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1271658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1271628
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1271629
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1271630
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1271631
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1271632
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;

    .line 1271633
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1271634
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1271635
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1271636
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1271637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;

    .line 1271638
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel;

    .line 1271639
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1271640
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    .line 1271641
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->l()Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1271642
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;

    .line 1271643
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;->h:Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel$OrderModel;

    .line 1271644
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1271645
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1271625
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPurchaseTicketMutationModel;-><init>()V

    .line 1271626
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1271627
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1271624
    const v0, 0x6b2f319

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1271623
    const v0, 0x599d07a1

    return v0
.end method
