.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1250158
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1250159
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1250160
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1250161
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1250162
    invoke-static {p1, v0}, LX/7s6;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1250163
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1250164
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1250165
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;-><init>()V

    .line 1250166
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1250167
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1250168
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1250169
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1250170
    :cond_0
    return-object v1
.end method
