.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1254132
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1254133
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1254131
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 1254053
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1254054
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v8, 0x11

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 1254055
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1254056
    invoke-virtual {v1, v0, v5, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1254057
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 1254058
    const-string v4, "end_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254059
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1254060
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254061
    if-eqz v2, :cond_1

    .line 1254062
    const-string v3, "eventProfilePicture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254063
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1254064
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254065
    if-eqz v2, :cond_2

    .line 1254066
    const-string v3, "event_buy_ticket_display_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254067
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254068
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254069
    if-eqz v2, :cond_3

    .line 1254070
    const-string v3, "event_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254071
    invoke-static {v1, v2, p1, p2}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254072
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254073
    if-eqz v2, :cond_4

    .line 1254074
    const-string v3, "event_ticket_provider"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254075
    invoke-static {v1, v2, p1, p2}, LX/7sf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254076
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254077
    if-eqz v2, :cond_5

    .line 1254078
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254079
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254080
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1254081
    if-eqz v2, :cond_6

    .line 1254082
    const-string v3, "is_all_day"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254083
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1254084
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1254085
    if-eqz v2, :cond_7

    .line 1254086
    const-string v3, "is_official"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254087
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1254088
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254089
    if-eqz v2, :cond_8

    .line 1254090
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254091
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254092
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254093
    if-eqz v2, :cond_9

    .line 1254094
    const-string v3, "registration_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254095
    invoke-static {v1, v2, p1, p2}, LX/7t4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254096
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1254097
    cmp-long v4, v2, v6

    if-eqz v4, :cond_a

    .line 1254098
    const-string v4, "start_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254099
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1254100
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1254101
    if-eqz v2, :cond_b

    .line 1254102
    const-string v3, "supports_multi_tier_purchase"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254103
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1254104
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254105
    if-eqz v2, :cond_c

    .line 1254106
    const-string v3, "ticket_seat_selection_note"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254107
    invoke-static {v1, v2, p1}, LX/7sg;->a(LX/15i;ILX/0nX;)V

    .line 1254108
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254109
    if-eqz v2, :cond_d

    .line 1254110
    const-string v3, "ticket_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254111
    invoke-static {v1, v2, p1, p2}, LX/7sj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254112
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1254113
    if-eqz v2, :cond_e

    .line 1254114
    const-string v3, "ticket_tiers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254115
    invoke-static {v1, v2, p1, p2}, LX/7sn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254116
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1254117
    if-eqz v2, :cond_f

    .line 1254118
    const-string v3, "timezone"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254119
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254120
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1254121
    if-eqz v2, :cond_10

    .line 1254122
    const-string v3, "total_purchased_tickets"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254123
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1254124
    :cond_10
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 1254125
    if-eqz v2, :cond_11

    .line 1254126
    const-string v2, "viewer_watch_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1254127
    invoke-virtual {v1, v0, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1254128
    :cond_11
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1254129
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1254130
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;LX/0nX;LX/0my;)V

    return-void
.end method
