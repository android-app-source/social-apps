.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1255222
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1255223
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1255224
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1255225
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1255226
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v2, 0x0

    .line 1255227
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1255228
    invoke-virtual {v1, v0, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 1255229
    if-eqz v2, :cond_0

    .line 1255230
    const-string p0, "event_invitee_limit"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1255231
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1255232
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1255233
    if-eqz v2, :cond_1

    .line 1255234
    const-string p0, "event_uninvitable_friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1255235
    invoke-static {v1, v2, p1, p2}, LX/7tL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1255236
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1255237
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1255238
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;LX/0nX;LX/0my;)V

    return-void
.end method
