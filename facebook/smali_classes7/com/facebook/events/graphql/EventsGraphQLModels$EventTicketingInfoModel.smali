.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oX;
.implements LX/7oi;
.implements LX/7oh;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1ef719a6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:J

.field private p:Z

.field private q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I

.field private v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254167
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1254168
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1254169
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1254170
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1254171
    iput p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u:I

    .line 1254172
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1254173
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1254174
    if-eqz v0, :cond_0

    .line 1254175
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1254176
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1254177
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1254178
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1254179
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1254180
    if-eqz v0, :cond_0

    .line 1254181
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x11

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1254182
    :cond_0
    return-void

    .line 1254183
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1254184
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m:Ljava/lang/String;

    .line 1254185
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1254186
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1254187
    if-eqz v0, :cond_0

    .line 1254188
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1254189
    :cond_0
    return-void
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254190
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->t:Ljava/lang/String;

    .line 1254191
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 20

    .prologue
    .line 1254192
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254193
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1254194
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->l()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1254195
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1254196
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1254197
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1254198
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->eR_()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1254199
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1254200
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1254201
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1254202
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1254203
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->v()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1254204
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1254205
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1254206
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1254207
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1254208
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1254209
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1254210
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1254211
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1254212
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1254213
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1254214
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1254215
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1254216
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1254217
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1254218
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1254219
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1254220
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1254221
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1254222
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1254223
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1254224
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254225
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1254226
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1254227
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1254228
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1254229
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1254230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254231
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1254232
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1254233
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1254234
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1254235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254236
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1254237
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1254238
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1254239
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1254240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254241
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1254242
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1254243
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    .line 1254244
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1254245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254246
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    .line 1254247
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1254248
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1254249
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1254250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254251
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1254252
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1254253
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1254254
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1254255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254256
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1254257
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1254258
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1254259
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1254260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    .line 1254261
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1254262
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254263
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1254300
    new-instance v0, LX/7qG;

    invoke-direct {v0, p1}, LX/7qG;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254264
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1254265
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1254266
    invoke-virtual {p1, p2, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e:J

    .line 1254267
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k:Z

    .line 1254268
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->l:Z

    .line 1254269
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o:J

    .line 1254270
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->p:Z

    .line 1254271
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u:I

    .line 1254272
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1254273
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1254274
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1254275
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1254276
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    .line 1254277
    :goto_0
    return-void

    .line 1254278
    :cond_0
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1254279
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->t()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1254280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1254281
    const/16 v0, 0x10

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1254282
    :cond_1
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1254283
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1254284
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1254285
    const/16 v0, 0x11

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1254286
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1254288
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1254289
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->a(Ljava/lang/String;)V

    .line 1254290
    :cond_0
    :goto_0
    return-void

    .line 1254291
    :cond_1
    const-string v0, "total_purchased_tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1254292
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->a(I)V

    goto :goto_0

    .line 1254293
    :cond_2
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1254294
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1254295
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254296
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->e:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1254297
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;-><init>()V

    .line 1254298
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1254299
    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254166
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254287
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1254134
    const v0, -0x3db0a82f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254136
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->j:Ljava/lang/String;

    .line 1254137
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final eQ_()Z
    .locals 2

    .prologue
    .line 1254138
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254139
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->k:Z

    return v0
.end method

.method public final eR_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254140
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m:Ljava/lang/String;

    .line 1254141
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1254135
    const v0, 0x403827a

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1254142
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254143
    iget-wide v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->o:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254144
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1254145
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254146
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->g:Ljava/lang/String;

    .line 1254147
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254148
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1254149
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254150
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1254151
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRegistrationSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254152
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    .line 1254153
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel;

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1254154
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254155
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->p:Z

    return v0
.end method

.method public final q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254156
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    .line 1254157
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSeatSelectionNoteModel;

    return-object v0
.end method

.method public final r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254158
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    .line 1254159
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTicketTiers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254160
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    .line 1254161
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->s:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel;

    return-object v0
.end method

.method public final t()I
    .locals 2

    .prologue
    .line 1254162
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1254163
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->u:I

    return v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1254164
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1254165
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method
