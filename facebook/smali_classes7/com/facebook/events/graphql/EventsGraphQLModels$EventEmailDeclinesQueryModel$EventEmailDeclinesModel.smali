.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x565e911
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244749
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1244752
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1244750
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1244751
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1244730
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1244731
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1244742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244743
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1244744
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1244745
    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1244746
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1244747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244748
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1244753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1244754
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1244755
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1244756
    if-eqz v1, :cond_0

    .line 1244757
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;

    .line 1244758
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->f:Ljava/util/List;

    .line 1244759
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1244760
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1244739
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1244740
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->e:I

    .line 1244741
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1244736
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;-><init>()V

    .line 1244737
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1244738
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1244735
    const v0, -0x7bc1eb91

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1244734
    const v0, 0x5fd9f3dd

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1244732
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->f:Ljava/util/List;

    .line 1244733
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
