.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1246834
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1246835
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1246836
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1246837
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1246838
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1246839
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1246840
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246841
    if-eqz v2, :cond_0

    .line 1246842
    const-string p0, "event_declines"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246843
    invoke-static {v1, v2, p1}, LX/7rX;->a(LX/15i;ILX/0nX;)V

    .line 1246844
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246845
    if-eqz v2, :cond_1

    .line 1246846
    const-string p0, "event_invitees"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246847
    invoke-static {v1, v2, p1}, LX/7rY;->a(LX/15i;ILX/0nX;)V

    .line 1246848
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246849
    if-eqz v2, :cond_2

    .line 1246850
    const-string p0, "event_maybes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246851
    invoke-static {v1, v2, p1}, LX/7rZ;->a(LX/15i;ILX/0nX;)V

    .line 1246852
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1246853
    if-eqz v2, :cond_3

    .line 1246854
    const-string p0, "event_members"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246855
    invoke-static {v1, v2, p1}, LX/7ra;->a(LX/15i;ILX/0nX;)V

    .line 1246856
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1246857
    if-eqz v2, :cond_4

    .line 1246858
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1246859
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1246860
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1246861
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1246862
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventGuestCountsModel;LX/0nX;LX/0my;)V

    return-void
.end method
