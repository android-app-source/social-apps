.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1255118
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1255119
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1255117
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1255107
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1255108
    invoke-static {p1, v0}, LX/7tJ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1255109
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1255110
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1255111
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;-><init>()V

    .line 1255112
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1255113
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1255114
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1255115
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1255116
    :cond_0
    return-object v1
.end method
