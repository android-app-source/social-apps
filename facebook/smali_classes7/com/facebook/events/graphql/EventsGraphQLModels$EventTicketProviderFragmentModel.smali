.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x48f35d92
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252341
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1252340
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1252338
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1252339
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252304
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->e:Ljava/lang/String;

    .line 1252305
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252336
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252337
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252334
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->g:Ljava/lang/String;

    .line 1252335
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1252324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252325
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1252326
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1252327
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1252328
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1252329
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1252330
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1252331
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1252332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1252333
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1252316
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1252317
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1252318
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252319
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1252320
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;

    .line 1252321
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel$EventTicketProviderModel;

    .line 1252322
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1252323
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1252315
    new-instance v0, LX/7qB;

    invoke-direct {v0, p1}, LX/7qB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1252314
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1252312
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1252313
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1252311
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1252308
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketProviderFragmentModel;-><init>()V

    .line 1252309
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1252310
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1252307
    const v0, 0xb3511af

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1252306
    const v0, 0x403827a

    return v0
.end method
