.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5LF;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x733b0348
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259894
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259895
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1259896
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1259897
    return-void
.end method

.method private o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259898
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->h:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->h:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259899
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->h:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259900
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259901
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259902
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259903
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259904
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259905
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259906
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259907
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 1259908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259909
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1259910
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1259911
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1259912
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1259913
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1259914
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1259915
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1259916
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1259917
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1259918
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->n()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1259919
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1259920
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1259921
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1259922
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1259923
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1259924
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1259925
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1259926
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1259927
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1259928
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1259929
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1259930
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259931
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1259932
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259933
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1259934
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259935
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1259936
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259937
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->h:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259938
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1259939
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259940
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1259941
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259942
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259943
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1259944
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259945
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1259946
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259947
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259948
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1259949
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259950
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1259951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259952
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259953
    :cond_3
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1259954
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259955
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1259956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259957
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259958
    :cond_4
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1259959
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259960
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1259961
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259962
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259963
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259964
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259965
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1259890
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;-><init>()V

    .line 1259891
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1259892
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1259893
    const v0, -0x7bafe372

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1259873
    const v0, -0xe40ca

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259874
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->e:Ljava/lang/String;

    .line 1259875
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259876
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->f:Ljava/lang/String;

    .line 1259877
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259878
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->g:Ljava/lang/String;

    .line 1259879
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259880
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1259881
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259882
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->n:Ljava/lang/String;

    .line 1259883
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259884
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259885
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->r()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259886
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->q()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259887
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->p()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259888
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259889
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method
