.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x61b144f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242622
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1242621
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1242619
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242620
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1242616
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1242617
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242618
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;
    .locals 14

    .prologue
    .line 1242578
    if-nez p0, :cond_0

    .line 1242579
    const/4 p0, 0x0

    .line 1242580
    :goto_0
    return-object p0

    .line 1242581
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    if-eqz v0, :cond_1

    .line 1242582
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    goto :goto_0

    .line 1242583
    :cond_1
    new-instance v0, LX/7oz;

    invoke-direct {v0}, LX/7oz;-><init>()V

    .line 1242584
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->a:Ljava/lang/String;

    .line 1242585
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    .line 1242586
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->c:Ljava/lang/String;

    .line 1242587
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242588
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eT_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242589
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->eS_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242590
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/7oz;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242591
    const/4 v6, 0x1

    const/4 v13, 0x0

    const/4 v4, 0x0

    .line 1242592
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1242593
    iget-object v3, v0, LX/7oz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1242594
    iget-object v5, v0, LX/7oz;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1242595
    iget-object v7, v0, LX/7oz;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1242596
    iget-object v8, v0, LX/7oz;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1242597
    iget-object v9, v0, LX/7oz;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1242598
    iget-object v10, v0, LX/7oz;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1242599
    iget-object v11, v0, LX/7oz;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1242600
    const/4 v12, 0x7

    invoke-virtual {v2, v12}, LX/186;->c(I)V

    .line 1242601
    invoke-virtual {v2, v13, v3}, LX/186;->b(II)V

    .line 1242602
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1242603
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1242604
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1242605
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1242606
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1242607
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1242608
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1242609
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1242610
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1242611
    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1242612
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1242613
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;-><init>(LX/15i;)V

    .line 1242614
    move-object p0, v3

    .line 1242615
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1242560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242561
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1242562
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1242563
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1242564
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1242565
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1242566
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1242567
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1242568
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1242569
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1242570
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1242571
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1242572
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1242573
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1242574
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1242575
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1242576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242577
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1242525
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1242526
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1242527
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    .line 1242528
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1242529
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1242530
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    .line 1242531
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1242532
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242533
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1242534
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1242535
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242536
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1242537
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242538
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1242539
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1242540
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242541
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1242542
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242543
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1242544
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1242545
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242546
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1242547
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242548
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1242549
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1242550
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242551
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1242552
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242559
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1242556
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;-><init>()V

    .line 1242557
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1242558
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242554
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    .line 1242555
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242553
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242623
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->g:Ljava/lang/String;

    .line 1242624
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1242511
    const v0, -0x477d7742

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242512
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eS_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242513
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eT_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242514
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1242515
    const v0, 0x4984e12

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242516
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242517
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    .line 1242518
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel$AlbumModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242509
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242510
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242519
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242520
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242521
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242522
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1242523
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1242524
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
