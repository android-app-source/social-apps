.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xec47674
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248762
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248763
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1248764
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248765
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1248766
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248767
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248768
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
    .locals 2

    .prologue
    .line 1248769
    if-nez p0, :cond_0

    .line 1248770
    const/4 p0, 0x0

    .line 1248771
    :goto_0
    return-object p0

    .line 1248772
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    if-eqz v0, :cond_1

    .line 1248773
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    goto :goto_0

    .line 1248774
    :cond_1
    new-instance v0, LX/7pi;

    invoke-direct {v0}, LX/7pi;-><init>()V

    .line 1248775
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pi;->a:Ljava/lang/String;

    .line 1248776
    invoke-virtual {v0}, LX/7pi;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1248777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248778
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1248779
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1248780
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1248781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1248783
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248784
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248785
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248786
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->e:Ljava/lang/String;

    .line 1248787
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1248788
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->e:Ljava/lang/String;

    .line 1248789
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1248790
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1248791
    if-eqz v0, :cond_0

    .line 1248792
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1248793
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1248794
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;-><init>()V

    .line 1248795
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248796
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1248797
    const v0, 0x47ede625

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1248798
    const v0, 0x2fa39a51

    return v0
.end method
