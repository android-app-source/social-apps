.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1254414
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1254415
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1254416
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1254417
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1254418
    const/4 v11, 0x0

    .line 1254419
    const/4 v10, 0x0

    .line 1254420
    const/4 v9, 0x0

    .line 1254421
    const/4 v8, 0x0

    .line 1254422
    const/4 v7, 0x0

    .line 1254423
    const/4 v6, 0x0

    .line 1254424
    const/4 v5, 0x0

    .line 1254425
    const/4 v4, 0x0

    .line 1254426
    const/4 v3, 0x0

    .line 1254427
    const/4 v2, 0x0

    .line 1254428
    const/4 v1, 0x0

    .line 1254429
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1254430
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1254431
    const/4 v1, 0x0

    .line 1254432
    :goto_0
    move v1, v1

    .line 1254433
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1254434
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1254435
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingTextFormFieldFragmentModel;-><init>()V

    .line 1254436
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1254437
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1254438
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1254439
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1254440
    :cond_0
    return-object v1

    .line 1254441
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1254442
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 1254443
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1254444
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1254445
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1254446
    const-string p0, "default_value"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1254447
    invoke-static {p1, v0}, LX/7tA;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1254448
    :cond_3
    const-string p0, "description"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1254449
    invoke-static {p1, v0}, LX/7tB;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1254450
    :cond_4
    const-string p0, "disable_autofill"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1254451
    const/4 v2, 0x1

    .line 1254452
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1254453
    :cond_5
    const-string p0, "fields"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1254454
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1254455
    :cond_6
    const-string p0, "form_field_id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1254456
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1254457
    :cond_7
    const-string p0, "form_field_type"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1254458
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1254459
    :cond_8
    const-string p0, "heading"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1254460
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1254461
    :cond_9
    const-string p0, "is_optional"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1254462
    const/4 v1, 0x1

    .line 1254463
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 1254464
    :cond_a
    const-string p0, "semantic_tag"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1254465
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1254466
    :cond_b
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1254467
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1254468
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1254469
    if-eqz v2, :cond_c

    .line 1254470
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1254471
    :cond_c
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1254472
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1254473
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1254474
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1254475
    if-eqz v1, :cond_d

    .line 1254476
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1254477
    :cond_d
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1254478
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
