.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xe697505
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241681
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241680
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241678
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241679
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1241672
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241673
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1241674
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1241675
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1241676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241677
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241664
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241665
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241666
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    .line 1241667
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241668
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;

    .line 1241669
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    .line 1241670
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241671
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1241682
    new-instance v0, LX/7os;

    invoke-direct {v0, p1}, LX/7os;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllEventDeclines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241662
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    .line 1241663
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1241660
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1241661
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1241659
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241656
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;-><init>()V

    .line 1241657
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241658
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241655
    const v0, -0x2fa4ef67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241654
    const v0, 0x403827a

    return v0
.end method
