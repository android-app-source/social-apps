.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1249172
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1249173
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1249103
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1249104
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1249105
    const/4 v2, 0x0

    .line 1249106
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1249107
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249108
    :goto_0
    move v1, v2

    .line 1249109
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1249110
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1249111
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;-><init>()V

    .line 1249112
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1249113
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1249114
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1249115
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1249116
    :cond_0
    return-object v1

    .line 1249117
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249118
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1249119
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1249120
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249121
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1249122
    const-string v4, "invitable_entries_token_query"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1249123
    const/4 v3, 0x0

    .line 1249124
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1249125
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249126
    :goto_2
    move v1, v3

    .line 1249127
    goto :goto_1

    .line 1249128
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1249129
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1249130
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1249131
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249132
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1249133
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1249134
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249135
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 1249136
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1249137
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1249138
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_7

    .line 1249139
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1249140
    const/4 v6, 0x0

    .line 1249141
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_12

    .line 1249142
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249143
    :goto_5
    move v5, v6

    .line 1249144
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1249145
    :cond_7
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1249146
    goto :goto_3

    .line 1249147
    :cond_8
    const-string v6, "page_info"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1249148
    invoke-static {p1, v0}, LX/4aB;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 1249149
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1249150
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1249151
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1249152
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 1249153
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1249154
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_11

    .line 1249155
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1249156
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1249157
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v10, :cond_c

    .line 1249158
    const-string p0, "__type__"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_d

    const-string p0, "__typename"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 1249159
    :cond_d
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_6

    .line 1249160
    :cond_e
    const-string p0, "photo"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 1249161
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_6

    .line 1249162
    :cond_f
    const-string p0, "title"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_10

    .line 1249163
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 1249164
    :cond_10
    const-string p0, "token"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1249165
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 1249166
    :cond_11
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1249167
    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 1249168
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 1249169
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1249170
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1249171
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_12
    move v5, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto/16 :goto_6
.end method
