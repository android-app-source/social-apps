.class public final Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2609e5b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1270432
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1270431
    const-class v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1270429
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1270430
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270427
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->e:Ljava/lang/String;

    .line 1270428
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1270404
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    .line 1270405
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1270419
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1270420
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1270421
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1270422
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1270423
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1270424
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1270425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1270426
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1270411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1270412
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1270413
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    .line 1270414
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->j()Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1270415
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;

    .line 1270416
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;->f:Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel$ProfileModel;

    .line 1270417
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1270418
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1270408
    new-instance v0, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsMutationsModels$ChangeSubscriptionMutationModel;-><init>()V

    .line 1270409
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1270410
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1270407
    const v0, 0x5b525978

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1270406
    const v0, 0x1f223a8e

    return v0
.end method
