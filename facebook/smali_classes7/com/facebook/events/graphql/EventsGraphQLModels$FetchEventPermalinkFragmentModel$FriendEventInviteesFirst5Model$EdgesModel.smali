.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4c680d08
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1256370
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1256369
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1256367
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1256368
    return-void
.end method

.method private a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256346
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    .line 1256347
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1256361
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1256362
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1256363
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1256364
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1256365
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1256366
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1256353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1256354
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1256355
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    .line 1256356
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1256357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;

    .line 1256358
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    .line 1256359
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1256360
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1256350
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model$EdgesModel;-><init>()V

    .line 1256351
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1256352
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1256349
    const v0, 0x746398d6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1256348
    const v0, -0x7b392492

    return v0
.end method
