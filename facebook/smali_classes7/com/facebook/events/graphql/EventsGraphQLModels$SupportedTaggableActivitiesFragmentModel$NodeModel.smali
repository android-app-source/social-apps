.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x93d50a8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1259966
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1260008
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1260006
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1260007
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1259996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259997
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1259998
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1259999
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1260000
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1260001
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1260002
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1260003
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1260004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1260005
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1259978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1259979
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1259980
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    .line 1259981
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1259982
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;

    .line 1259983
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    .line 1259984
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1259985
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259986
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1259987
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;

    .line 1259988
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259989
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1259990
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    .line 1259991
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1259992
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;

    .line 1259993
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    .line 1259994
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1259995
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259976
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    .line 1259977
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$ObjectModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1259973
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;-><init>()V

    .line 1259974
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1259975
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1259972
    const v0, 0xf056837

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1259971
    const v0, 0x4cff1ce8    # 1.3375264E8f

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259969
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    .line 1259970
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1259967
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    .line 1259968
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$SupportedTaggableActivitiesFragmentModel$NodeModel$TaggableActivityIconModel;

    return-object v0
.end method
