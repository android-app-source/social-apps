.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1254981
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1254982
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1254983
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1254984
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1254985
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/7tI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1254986
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1254987
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;LX/0nX;LX/0my;)V

    return-void
.end method
