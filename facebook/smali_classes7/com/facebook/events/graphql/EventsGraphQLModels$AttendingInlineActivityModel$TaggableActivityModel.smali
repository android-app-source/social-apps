.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5LF;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x79da5f16
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241283
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1241276
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1241277
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1241278
    return-void
.end method

.method private j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241279
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241280
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241281
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241282
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241268
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241269
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241284
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241285
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241286
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241287
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method

.method private o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241288
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241289
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1241290
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241291
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1241292
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1241293
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1241294
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1241295
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1241296
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1241297
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1241298
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1241299
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1241300
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->eO_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1241301
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1241302
    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1241303
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1241304
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->g:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1241305
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1241306
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1241307
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1241308
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1241309
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1241310
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1241311
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1241312
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1241313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241314
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1241315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1241316
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241317
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241318
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1241319
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241320
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241321
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1241322
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241323
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1241324
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241325
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241326
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1241327
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241328
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1241329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241330
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241331
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1241332
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241333
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1241334
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241335
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241336
    :cond_3
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1241337
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241338
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1241339
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241340
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241341
    :cond_4
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1241342
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241343
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1241344
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    .line 1241345
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1241346
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1241347
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241348
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1241270
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1241271
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->g:I

    .line 1241272
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1241273
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;-><init>()V

    .line 1241274
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1241275
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241250
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->e:Ljava/lang/String;

    .line 1241251
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241252
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->f:Ljava/lang/String;

    .line 1241253
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1241254
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1241255
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1241256
    const v0, 0x74e37748

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241257
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->h:Ljava/lang/String;

    .line 1241258
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final eO_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241259
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o:Ljava/lang/String;

    .line 1241260
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1241261
    const v0, -0xe40ca

    return v0
.end method

.method public final synthetic t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241262
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->o()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241263
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241264
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241265
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241266
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1241267
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    return-object v0
.end method
