.class public final Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1239450
    const-class v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    new-instance v1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1239451
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1239452
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1239453
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1239454
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1239455
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1239456
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239457
    if-eqz v2, :cond_0

    .line 1239458
    const-string p0, "eventProfilePicture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239459
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1239460
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1239461
    if-eqz v2, :cond_1

    .line 1239462
    const-string p0, "event_place"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239463
    invoke-static {v1, v2, p1}, LX/7nV;->a(LX/15i;ILX/0nX;)V

    .line 1239464
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239465
    if-eqz v2, :cond_2

    .line 1239466
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239467
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239468
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239469
    if-eqz v2, :cond_3

    .line 1239470
    const-string p0, "live_permalink_time_range_sentence"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239471
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239472
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1239473
    if-eqz v2, :cond_4

    .line 1239474
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1239475
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1239476
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1239477
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1239478
    check-cast p1, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel$Serializer;->a(Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventPreviewModel;LX/0nX;LX/0my;)V

    return-void
.end method
