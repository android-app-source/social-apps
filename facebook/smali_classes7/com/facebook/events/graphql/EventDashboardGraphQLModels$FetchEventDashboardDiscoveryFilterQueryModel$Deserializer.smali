.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1237855
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1237856
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1237857
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1237858
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1237859
    const/4 v2, 0x0

    .line 1237860
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1237861
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1237862
    :goto_0
    move v1, v2

    .line 1237863
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1237864
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1237865
    new-instance v1, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;-><init>()V

    .line 1237866
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1237867
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1237868
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1237869
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1237870
    :cond_0
    return-object v1

    .line 1237871
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1237872
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_4

    .line 1237873
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1237874
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1237875
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 1237876
    const-string p0, "event_category_list"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1237877
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1237878
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, p0, :cond_3

    .line 1237879
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, p0, :cond_3

    .line 1237880
    invoke-static {p1, v0}, LX/7n9;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1237881
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1237882
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1237883
    goto :goto_1

    .line 1237884
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1237885
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1237886
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method
