.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x64d6490b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255476
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1255479
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1255477
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1255478
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1255455
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1255456
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->g:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1255468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255469
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1255470
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->e:I

    invoke-virtual {p1, v2, v0, v2}, LX/186;->a(III)V

    .line 1255471
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1255472
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1255473
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->h:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1255474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255475
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1255480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1255481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1255482
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1255462
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1255463
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->e:I

    .line 1255464
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->f:I

    .line 1255465
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->g:I

    .line 1255466
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;->h:I

    .line 1255467
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1255459
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;-><init>()V

    .line 1255460
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1255461
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1255458
    const v0, -0x47c55bb5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1255457
    const v0, -0x6514abc0

    return v0
.end method
