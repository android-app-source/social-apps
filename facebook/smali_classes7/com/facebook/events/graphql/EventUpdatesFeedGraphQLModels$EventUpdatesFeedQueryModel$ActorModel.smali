.class public final Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x25a70459
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1240029
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1240028
    const-class v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1240006
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1240007
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1240032
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1240033
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1240034
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1240030
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->g:Ljava/lang/String;

    .line 1240031
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1240035
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1240036
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1240037
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1240038
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1240039
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1240040
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1240041
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1240042
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1240043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1240044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1240019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1240020
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1240021
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    .line 1240022
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1240023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    .line 1240024
    iput-object v0, v1, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->f:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    .line 1240025
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1240026
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1240027
    new-instance v0, LX/7nZ;

    invoke-direct {v0, p1}, LX/7nZ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1240018
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1240016
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1240017
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1240015
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1240012
    new-instance v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;-><init>()V

    .line 1240013
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1240014
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1240011
    const v0, -0x5ae2ddbf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1240010
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventsFeed"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1240008
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->f:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->f:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    .line 1240009
    iget-object v0, p0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel;->f:Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel;

    return-object v0
.end method
