.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x597182bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1247618
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1247617
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1247590
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1247591
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1247611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1247612
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1247613
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1247614
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1247615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1247616
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1247603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1247604
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1247605
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    .line 1247606
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1247607
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;

    .line 1247608
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    .line 1247609
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1247610
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1247602
    new-instance v0, LX/7pa;

    invoke-direct {v0, p1}, LX/7pa;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOtherEventDeclines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1247600
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    .line 1247601
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1247598
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1247599
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1247597
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1247594
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;-><init>()V

    .line 1247595
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1247596
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1247593
    const v0, 0x3f3d98e1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1247592
    const v0, 0x403827a

    return v0
.end method
