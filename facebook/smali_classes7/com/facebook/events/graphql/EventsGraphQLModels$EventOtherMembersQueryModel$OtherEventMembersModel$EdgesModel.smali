.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16393afd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248051
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1248054
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1248052
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1248053
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1248043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248044
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1248045
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1248046
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1248047
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1248048
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1248049
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248050
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1248055
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1248056
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1248057
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1248058
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1248059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;

    .line 1248060
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1248061
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1248062
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248041
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    .line 1248042
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1248038
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;-><init>()V

    .line 1248039
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1248040
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1248034
    const v0, -0x36e7b1d0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1248037
    const v0, 0x3ad2b9c

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1248035
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    .line 1248036
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->f:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    return-object v0
.end method
