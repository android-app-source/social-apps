.class public final Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x61a38660
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238210
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1238215
    const-class v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1238213
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1238214
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238211
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->g:Ljava/lang/String;

    .line 1238212
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1238201
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238202
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1238203
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1238204
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1238205
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1238206
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1238207
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1238208
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238209
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1238216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1238217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1238218
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1238189
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->e:Ljava/lang/String;

    .line 1238190
    iget-object v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1238191
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1238192
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->f:Z

    .line 1238193
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1238198
    new-instance v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;-><init>()V

    .line 1238199
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1238200
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1238194
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1238195
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel$EventCalendarableItemsModel$ItemsModel$PageInfoModel;->f:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1238196
    const v0, 0x64005f0e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1238197
    const v0, 0x370fbffd

    return v0
.end method
