.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1244935
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;

    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1244936
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1244934
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1244915
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1244916
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1244917
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1244918
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1244919
    if-eqz v2, :cond_2

    .line 1244920
    const-string p0, "event_email_invitees"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1244921
    const/4 p0, 0x0

    .line 1244922
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1244923
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1244924
    if-eqz p0, :cond_0

    .line 1244925
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1244926
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1244927
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1244928
    if-eqz p0, :cond_1

    .line 1244929
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1244930
    invoke-static {v1, p0, p1, p2}, LX/7rC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1244931
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1244932
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1244933
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1244914
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel$Serializer;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
