.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7om;
.implements LX/7ol;
.implements LX/7oo;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3a5b02bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel$Serializer;
.end annotation


# instance fields
.field private A:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Z

.field private r:Z

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253923
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1253924
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1253925
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1253926
    return-void
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253927
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1253928
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1253929
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private p()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableTimeSlots"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253930
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0x1d39ca05

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->g:LX/3Sb;

    .line 1253931
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253932
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingDatePickerFormFieldFragmentModel$AvailableTimesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->h:Ljava/util/List;

    .line 1253933
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253934
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253935
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    return-object v0
.end method

.method private s()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1254019
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m:Ljava/util/List;

    .line 1254020
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private t()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrefillValues"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253936
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x12

    const v4, 0x5886ddaf

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w:LX/3Sb;

    .line 1253937
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253938
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 1253939
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method

.method private v()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSeparator"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253940
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253941
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeEnd"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253942
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253943
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->A:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeSelected"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253944
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253945
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->B:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeStart"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253946
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253947
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->C:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 25

    .prologue
    .line 1253874
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253875
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1253876
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p()LX/2uF;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v4

    .line 1253877
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1253878
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1253879
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x79daef33

    invoke-static {v8, v7, v9}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1253880
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1253881
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->s()LX/0Px;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/util/List;)I

    move-result v9

    .line 1253882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1253883
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1253884
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->a()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1253885
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 1253886
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->c()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    const v16, -0x6ccc86d3

    move/from16 v0, v16

    invoke-static {v15, v14, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1253887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m()LX/1vs;

    move-result-object v15

    iget-object v0, v15, LX/1vs;->a:LX/15i;

    move-object/from16 v16, v0

    iget v15, v15, LX/1vs;->b:I

    const v17, -0x7d3e8e01

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v15, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1253888
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t()LX/2uF;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v16

    .line 1253889
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->u()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1253890
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v()LX/1vs;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, LX/1vs;->b:I

    move/from16 v18, v0

    const v20, -0x2229e847

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1253891
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1253892
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w()LX/1vs;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget v0, v0, LX/1vs;->b:I

    move/from16 v20, v0

    const v22, 0x7094ead2

    move-object/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1253893
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x()LX/1vs;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    iget v0, v0, LX/1vs;->b:I

    move/from16 v21, v0

    const v23, 0x7550ba30

    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1253894
    invoke-direct/range {p0 .. p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y()LX/1vs;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget v0, v0, LX/1vs;->b:I

    move/from16 v22, v0

    const v24, -0x67018e1c

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1253895
    const/16 v23, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1253896
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1253897
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->f:Z

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1253898
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1253899
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1253900
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1253901
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1253902
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253903
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1253904
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1253905
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1253906
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1253907
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1253908
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253909
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1253910
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1253911
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1253912
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1253913
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1253914
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253915
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253916
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253917
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253918
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253919
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253920
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1253921
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1253922
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1253948
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1253949
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1253950
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1253951
    if-eqz v1, :cond_0

    .line 1253952
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253953
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->g:LX/3Sb;

    .line 1253954
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1253955
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1253956
    if-eqz v1, :cond_1

    .line 1253957
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253958
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->h:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 1253959
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1253960
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253961
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1253962
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253963
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingFormFieldFragmentModel$DefaultValueModel;

    .line 1253964
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1253965
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x79daef33

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1253966
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1253967
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253968
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j:I

    move-object v1, v0

    .line 1253969
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1253970
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1253971
    if-eqz v2, :cond_4

    .line 1253972
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253973
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->s:Ljava/util/List;

    move-object v1, v0

    .line 1253974
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1253975
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6ccc86d3

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1253976
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->c()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1253977
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253978
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->u:I

    move-object v1, v0

    .line 1253979
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1253980
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7d3e8e01

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1253981
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1253982
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253983
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v:I

    move-object v1, v0

    .line 1253984
    :cond_6
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1253985
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1253986
    if-eqz v2, :cond_7

    .line 1253987
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253988
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w:LX/3Sb;

    move-object v1, v0

    .line 1253989
    :cond_7
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1253990
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2229e847

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1253991
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1253992
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253993
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y:I

    move-object v1, v0

    .line 1253994
    :cond_8
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 1253995
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7094ead2

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1253996
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->w()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1253997
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1253998
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->A:I

    move-object v1, v0

    .line 1253999
    :cond_9
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_a

    .line 1254000
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7550ba30

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1254001
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1254002
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1254003
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->B:I

    move-object v1, v0

    .line 1254004
    :cond_a
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_b

    .line 1254005
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x67018e1c

    invoke-static {v2, v0, v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/graphql/EventsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1254006
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1254007
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1254008
    iput v3, v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->C:I

    move-object v1, v0

    .line 1254009
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1254010
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    .line 1254011
    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v0

    .line 1254012
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v0

    .line 1254013
    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    .line 1254014
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0

    .line 1254015
    :catchall_4
    move-exception v0

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v0

    .line 1254016
    :catchall_5
    move-exception v0

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v0

    .line 1254017
    :catchall_6
    move-exception v0

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v0

    :cond_c
    move-object p0, v1

    .line 1254018
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253831
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p:Ljava/lang/String;

    .line 1253832
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1253835
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1253836
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->f:Z

    .line 1253837
    const/4 v0, 0x5

    const v1, 0x79daef33

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j:I

    .line 1253838
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->k:Z

    .line 1253839
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q:Z

    .line 1253840
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->r:Z

    .line 1253841
    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t:I

    .line 1253842
    const/16 v0, 0x10

    const v1, -0x6ccc86d3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->u:I

    .line 1253843
    const/16 v0, 0x11

    const v1, -0x7d3e8e01

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v:I

    .line 1253844
    const/16 v0, 0x14

    const v1, -0x2229e847

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->y:I

    .line 1253845
    const/16 v0, 0x16

    const v1, 0x7094ead2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->A:I

    .line 1253846
    const/16 v0, 0x17

    const v1, 0x7550ba30

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->B:I

    .line 1253847
    const/16 v0, 0x18

    const v1, -0x67018e1c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->C:I

    .line 1253848
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1253849
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;-><init>()V

    .line 1253850
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1253851
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1253852
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253853
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->q:Z

    return v0
.end method

.method public final c()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPagesPlatformImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253854
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253855
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1253833
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253834
    iget-boolean v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->f:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1253856
    const v0, -0x37a4056d

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253857
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 1253858
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->l:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    return-object v0
.end method

.method public final eZ_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253859
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->n:Ljava/lang/String;

    .line 1253860
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1253861
    const v0, 0x1edc14d0

    return v0
.end method

.method public final fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253862
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->o:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->o:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 1253863
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->o:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1253864
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->s:Ljava/util/List;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingStringScalarFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->s:Ljava/util/List;

    .line 1253865
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->s:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253866
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->z:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->z:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    .line 1253867
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->z:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253868
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253869
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getParagraphContent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1253870
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253871
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->v:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 1253872
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1253873
    iget v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->t:I

    return v0
.end method
