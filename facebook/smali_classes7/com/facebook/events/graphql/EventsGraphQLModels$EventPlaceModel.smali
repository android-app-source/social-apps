.class public final Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2e36ec57
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249084
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1249090
    const-class v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1249088
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1249089
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1249085
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1249086
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1249087
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 2

    .prologue
    .line 1249022
    if-nez p0, :cond_0

    .line 1249023
    const/4 p0, 0x0

    .line 1249024
    :goto_0
    return-object p0

    .line 1249025
    :cond_0
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    if-eqz v0, :cond_1

    .line 1249026
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    goto :goto_0

    .line 1249027
    :cond_1
    new-instance v0, LX/7pj;

    invoke-direct {v0}, LX/7pj;-><init>()V

    .line 1249028
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1249029
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1249030
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    .line 1249031
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->d:Ljava/lang/String;

    .line 1249032
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eV_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->e:Ljava/lang/String;

    .line 1249033
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eW_()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    .line 1249034
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/7pj;->g:Ljava/lang/String;

    .line 1249035
    invoke-virtual {v0}, LX/7pj;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1249078
    iput-object p1, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k:Ljava/lang/String;

    .line 1249079
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1249080
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1249081
    if-eqz v0, :cond_0

    .line 1249082
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1249083
    :cond_0
    return-void
.end method

.method private l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249076
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    .line 1249077
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249074
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    .line 1249075
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1249056
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249057
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1249058
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1249059
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1249060
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1249061
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eV_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1249062
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1249063
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1249064
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1249065
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1249066
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1249067
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1249068
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1249069
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1249070
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1249071
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1249072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249073
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1249038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1249039
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1249040
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1249041
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1249042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1249043
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1249044
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1249045
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    .line 1249046
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1249047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1249048
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    .line 1249049
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1249050
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    .line 1249051
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1249052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1249053
    iput-object v0, v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    .line 1249054
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1249055
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1249037
    new-instance v0, LX/7pn;

    invoke-direct {v0, p1}, LX/7pn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249036
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->eV_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1249091
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249092
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    .line 1249093
    if-eqz v0, :cond_1

    .line 1249094
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1249095
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1249096
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1249097
    :goto_0
    return-void

    .line 1249098
    :cond_0
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1249099
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1249100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1249101
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1249102
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1248992
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1248993
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    .line 1248994
    if-eqz v0, :cond_0

    .line 1248995
    if-eqz p3, :cond_1

    .line 1248996
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1248997
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a(Ljava/lang/String;)V

    .line 1248998
    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1248999
    :cond_0
    :goto_0
    return-void

    .line 1249000
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1249001
    :cond_2
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249002
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249003
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1249004
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1249005
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1249006
    new-instance v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-direct {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;-><init>()V

    .line 1249007
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1249008
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249009
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249010
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$CityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1249011
    const v0, 0x43e61f7

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249012
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->h:Ljava/lang/String;

    .line 1249013
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final eV_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249014
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->i:Ljava/lang/String;

    .line 1249015
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic eW_()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249016
    invoke-direct {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1249017
    const v0, 0x499e8e7

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249018
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k:Ljava/lang/String;

    .line 1249019
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1249020
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    .line 1249021
    iget-object v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    return-object v0
.end method
