.class public final Lcom/facebook/pages/common/util/PagesScrollUtils$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:I

.field public final synthetic c:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;II)V
    .locals 0

    .prologue
    .line 1317949
    iput-object p1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    iput p2, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    iput p3, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1317950
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_3

    .line 1317951
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 1317952
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1317953
    instance-of v2, v0, Landroid/widget/ListView;

    if-eqz v2, :cond_5

    move-object v2, v0

    .line 1317954
    check-cast v2, Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-ne v2, v1, :cond_1

    move v4, v3

    .line 1317955
    :cond_1
    :goto_1
    move v0, v4

    .line 1317956
    if-nez v0, :cond_2

    .line 1317957
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    sget-object v2, LX/8FW;->DELAY:LX/8FW;

    iget v3, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->c:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/8FX;->b(Landroid/view/ViewGroup;ILX/8FW;I)V

    .line 1317958
    :cond_2
    return-void

    .line 1317959
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/widget/ScrollView;

    if-eqz v0, :cond_4

    .line 1317960
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/ScrollView;

    iget v1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    mul-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0

    .line 1317961
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 1317962
    iget-object v0, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->a:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 1317963
    instance-of v1, v0, LX/1P1;

    if-eqz v1, :cond_0

    .line 1317964
    check-cast v0, LX/1P1;

    iget v1, p0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;->b:I

    invoke-virtual {v0, v2, v1}, LX/1P1;->d(II)V

    goto :goto_0

    .line 1317965
    :cond_5
    instance-of v2, v0, Landroid/widget/ScrollView;

    if-eqz v2, :cond_6

    .line 1317966
    check-cast v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    neg-int v5, v1

    if-ne v2, v5, :cond_1

    move v4, v3

    goto :goto_1

    .line 1317967
    :cond_6
    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 1317968
    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    .line 1317969
    instance-of v5, v2, LX/1P1;

    if-eqz v5, :cond_1

    .line 1317970
    :try_start_0
    check-cast v2, LX/1P1;

    invoke-virtual {v2}, LX/1P1;->l()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_7

    move v2, v3

    .line 1317971
    :goto_2
    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-ne v2, v1, :cond_1

    move v4, v3

    goto :goto_1

    :cond_7
    move v2, v4

    .line 1317972
    goto :goto_2

    :catch_0
    move v2, v4

    goto :goto_2
.end method
