.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1314978
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;

    new-instance v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1314979
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1314980
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1314965
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1314966
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    .line 1314967
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1314968
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1314969
    if-eqz v2, :cond_0

    .line 1314970
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1314971
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1314972
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1314973
    if-eqz v2, :cond_1

    .line 1314974
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1314975
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1314976
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1314977
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1314964
    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel$Serializer;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageViewerProfilePermissionsModel;LX/0nX;LX/0my;)V

    return-void
.end method
