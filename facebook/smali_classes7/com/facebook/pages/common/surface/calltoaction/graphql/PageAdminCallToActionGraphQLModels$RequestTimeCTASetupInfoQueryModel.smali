.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64bb0376
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1315696
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1315695
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1315693
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1315694
    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315691
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->e:Ljava/lang/String;

    .line 1315692
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1315669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1315670
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1315671
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1315672
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1315673
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1315674
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1315675
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1315676
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1315677
    const/16 v7, 0xb

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1315678
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1315679
    const/4 v0, 0x1

    iget-boolean v7, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->f:Z

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1315680
    const/4 v0, 0x2

    iget-boolean v7, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->g:Z

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1315681
    const/4 v0, 0x3

    iget-boolean v7, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->h:Z

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1315682
    const/4 v0, 0x4

    iget-boolean v7, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->i:Z

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1315683
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1315684
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1315685
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1315686
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1315687
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1315688
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1315689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1315690
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1315656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1315657
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1315658
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    .line 1315659
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1315660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    .line 1315661
    iput-object v0, v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    .line 1315662
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1315663
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    .line 1315664
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1315665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    .line 1315666
    iput-object v0, v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    .line 1315667
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1315668
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1315650
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1315651
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->f:Z

    .line 1315652
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->g:Z

    .line 1315653
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->h:Z

    .line 1315654
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->i:Z

    .line 1315655
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1315648
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1315649
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->f:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1315627
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;-><init>()V

    .line 1315628
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1315629
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1315647
    const v0, -0x7eeb157e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1315646
    const v0, 0x1f52ec97

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1315644
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1315645
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->g:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1315642
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1315643
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->i:Z

    return v0
.end method

.method public final l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315640
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    .line 1315641
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315638
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k:Ljava/lang/String;

    .line 1315639
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315636
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l:Ljava/lang/String;

    .line 1315637
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315634
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    .line 1315635
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$ServiceSetupImageModel;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315632
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->n:Ljava/lang/String;

    .line 1315633
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1315630
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o:Ljava/lang/String;

    .line 1315631
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->o:Ljava/lang/String;

    return-object v0
.end method
