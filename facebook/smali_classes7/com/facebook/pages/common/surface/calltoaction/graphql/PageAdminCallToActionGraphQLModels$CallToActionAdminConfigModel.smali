.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x18454151
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1313964
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1313963
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1313961
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1313962
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1313958
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1313959
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1313960
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1313944
    if-nez p0, :cond_0

    .line 1313945
    const/4 p0, 0x0

    .line 1313946
    :goto_0
    return-object p0

    .line 1313947
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-eqz v0, :cond_1

    .line 1313948
    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    goto :goto_0

    .line 1313949
    :cond_1
    new-instance v0, LX/8EV;

    invoke-direct {v0}, LX/8EV;-><init>()V

    .line 1313950
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8EV;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    .line 1313951
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    iput-object v1, v0, LX/8EV;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 1313952
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8EV;->c:Ljava/lang/String;

    .line 1313953
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8EV;->d:Ljava/lang/String;

    .line 1313954
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/8EV;->e:LX/15i;

    iput v1, v0, LX/8EV;->f:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1313955
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8EV;->g:Ljava/lang/String;

    .line 1313956
    invoke-virtual {v0}, LX/8EV;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object p0

    goto :goto_0

    .line 1313957
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1313928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1313929
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1313930
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1313931
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1313932
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1313933
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x707d7c3d

    invoke-static {v5, v4, v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1313934
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1313935
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1313936
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1313937
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1313938
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1313939
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1313940
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1313941
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1313942
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1313943
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1313913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1313914
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1313915
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    .line 1313916
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1313917
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 1313918
    iput-object v0, v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    .line 1313919
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1313920
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x707d7c3d

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1313921
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1313922
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 1313923
    iput v3, v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->i:I

    move-object v1, v0

    .line 1313924
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1313925
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1313926
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1313927
    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313912
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1313965
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1313966
    const/4 v0, 0x4

    const v1, -0x707d7c3d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->i:I

    .line 1313967
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313910
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 1313911
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1313907
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;-><init>()V

    .line 1313908
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1313909
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313905
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->g:Ljava/lang/String;

    .line 1313906
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313903
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->h:Ljava/lang/String;

    .line 1313904
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final dQ_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313901
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1313902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1313900
    const v0, -0x5991127

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313898
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j:Ljava/lang/String;

    .line 1313899
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1313897
    const v0, 0x523d0f33    # 2.03000955E11f

    return v0
.end method

.method public final j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1313895
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    .line 1313896
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    return-object v0
.end method
