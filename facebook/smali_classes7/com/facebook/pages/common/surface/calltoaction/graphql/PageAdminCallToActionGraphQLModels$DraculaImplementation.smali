.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1314844
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1314845
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1314842
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1314843
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1314795
    if-nez p1, :cond_0

    .line 1314796
    :goto_0
    return v0

    .line 1314797
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1314798
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1314799
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1314800
    const v2, -0x33d4e228    # -4.4857184E7f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1314801
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v2

    .line 1314802
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1314803
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1314804
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1314805
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 1314806
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1314807
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1314808
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1314809
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1314810
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1314811
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1314812
    :sswitch_2
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 1314813
    const v1, 0x5d7b4121

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1314814
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1314815
    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1314816
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1314817
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1314818
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1314819
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1314820
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1314821
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1314822
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1314823
    const v2, 0x74836b19

    const/4 v5, 0x0

    .line 1314824
    if-nez v1, :cond_1

    move v4, v5

    .line 1314825
    :goto_1
    move v1, v4

    .line 1314826
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1314827
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1314828
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1314829
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v1

    .line 1314830
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1314831
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1314832
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1314833
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1314834
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1314835
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1314836
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1314837
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1314838
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1314839
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1314840
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1314841
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x707d7c3d -> :sswitch_0
        -0x44496c2c -> :sswitch_2
        -0x33d4e228 -> :sswitch_1
        0x31e50985 -> :sswitch_4
        0x5d7b4121 -> :sswitch_3
        0x74836b19 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1314794
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1314778
    sparse-switch p2, :sswitch_data_0

    .line 1314779
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1314780
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1314781
    const v1, -0x33d4e228    # -4.4857184E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1314782
    :goto_0
    :sswitch_1
    return-void

    .line 1314783
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1314784
    const v1, 0x5d7b4121

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1314785
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1314786
    const v1, 0x74836b19

    .line 1314787
    if-eqz v0, :cond_0

    .line 1314788
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1314789
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1314790
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1314791
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1314792
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1314793
    :cond_0
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x707d7c3d -> :sswitch_0
        -0x44496c2c -> :sswitch_2
        -0x33d4e228 -> :sswitch_1
        0x31e50985 -> :sswitch_3
        0x5d7b4121 -> :sswitch_1
        0x74836b19 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1314772
    if-eqz p1, :cond_0

    .line 1314773
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1314774
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    .line 1314775
    if-eq v0, v1, :cond_0

    .line 1314776
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1314777
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1314771
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1314769
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1314770
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1314846
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1314847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1314848
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1314849
    iput p2, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->b:I

    .line 1314850
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1314768
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1314767
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1314764
    iget v0, p0, LX/1vt;->c:I

    .line 1314765
    move v0, v0

    .line 1314766
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1314761
    iget v0, p0, LX/1vt;->c:I

    .line 1314762
    move v0, v0

    .line 1314763
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1314758
    iget v0, p0, LX/1vt;->b:I

    .line 1314759
    move v0, v0

    .line 1314760
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1314755
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1314756
    move-object v0, v0

    .line 1314757
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1314746
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1314747
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1314748
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1314749
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1314750
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1314751
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1314752
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1314753
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1314754
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1314743
    iget v0, p0, LX/1vt;->c:I

    .line 1314744
    move v0, v0

    .line 1314745
    return v0
.end method
