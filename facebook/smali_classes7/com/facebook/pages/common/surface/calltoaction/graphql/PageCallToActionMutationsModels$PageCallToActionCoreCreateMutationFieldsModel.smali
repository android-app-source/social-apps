.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x562bca02
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1317640
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1317643
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1317641
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1317642
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1317626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1317627
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1317628
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1317629
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1317630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1317631
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1317632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1317633
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1317634
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    .line 1317635
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1317636
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;

    .line 1317637
    iput-object v0, v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    .line 1317638
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1317639
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1317624
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    .line 1317625
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1317621
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;-><init>()V

    .line 1317622
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1317623
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1317620
    const v0, 0x18399e2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1317619
    const v0, 0x230312b3

    return v0
.end method
