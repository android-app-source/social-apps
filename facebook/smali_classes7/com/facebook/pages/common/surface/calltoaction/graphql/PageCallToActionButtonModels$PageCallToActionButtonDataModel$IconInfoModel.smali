.class public final Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xa2db182
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1316929
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1316971
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1316969
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1316970
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1316966
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1316967
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1316968
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;
    .locals 8

    .prologue
    .line 1316946
    if-nez p0, :cond_0

    .line 1316947
    const/4 p0, 0x0

    .line 1316948
    :goto_0
    return-object p0

    .line 1316949
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    if-eqz v0, :cond_1

    .line 1316950
    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    goto :goto_0

    .line 1316951
    :cond_1
    new-instance v0, LX/8F8;

    invoke-direct {v0}, LX/8F8;-><init>()V

    .line 1316952
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v1

    iput-object v1, v0, LX/8F8;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    .line 1316953
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1316954
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1316955
    iget-object v3, v0, LX/8F8;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1316956
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1316957
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1316958
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1316959
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1316960
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1316961
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1316962
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1316963
    new-instance v3, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;-><init>(LX/15i;)V

    .line 1316964
    move-object p0, v3

    .line 1316965
    goto :goto_0
.end method

.method private j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1316944
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    .line 1316945
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1316972
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1316973
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1316974
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1316975
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1316976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1316977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1316936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1316937
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1316938
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    .line 1316939
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1316940
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    .line 1316941
    iput-object v0, v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    .line 1316942
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1316943
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1316935
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1316932
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;-><init>()V

    .line 1316933
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1316934
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1316931
    const v0, -0x78e0460

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1316930
    const v0, 0x63601ac8    # 4.1340005E21f

    return v0
.end method
