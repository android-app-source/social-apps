.class public final Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/3iH;


# direct methods
.method public constructor <init>(LX/3iH;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1318153
    iput-object p1, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->c:LX/3iH;

    iput-object p2, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1318145
    :try_start_0
    iget-object v1, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->c:LX/3iH;

    iget-object v1, v1, LX/3iH;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->c:LX/3iH;

    iget-object v2, v2, LX/3iH;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    iget-object v3, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->a:Ljava/lang/String;

    const-class v4, LX/3iH;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/8Fb;

    move-object v8, v0

    .line 1318146
    if-nez v8, :cond_0

    .line 1318147
    iget-object v1, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1318148
    :goto_0
    return-void

    .line 1318149
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->c:LX/3iH;

    iget-object v1, v1, LX/3iH;->a:LX/2U1;

    iget-object v2, v8, LX/8Fb;->a:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, v8, LX/8Fb;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, LX/2U1;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/Boolean;LX/0am;)V

    .line 1318150
    iget-object v1, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, v8, LX/8Fb;->b:Ljava/lang/String;

    const v3, -0x5d915e14

    invoke-static {v1, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1318151
    :catch_0
    move-exception v1

    .line 1318152
    iget-object v2, p0, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v2, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
