.class public Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1313151
    new-instance v0, LX/8Dl;

    invoke-direct {v0}, LX/8Dl;-><init>()V

    sput-object v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;Ljava/util/ArrayList;Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1313152
    invoke-direct {p0, p1, p4, p5}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 1313153
    iput-object p2, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    .line 1313154
    iput-object p3, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->b:Ljava/lang/String;

    .line 1313155
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1313156
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1313157
    const-class v0, Lcom/facebook/ipc/pages/PageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    .line 1313158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->b:Ljava/lang/String;

    .line 1313159
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1313160
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1313161
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1313162
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1313163
    iget-object v0, p0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1313164
    return-void
.end method
