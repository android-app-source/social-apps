.class public final Lcom/facebook/friends/methods/BlockUserMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friends/methods/BlockUserMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Z

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1291030
    new-instance v0, LX/84e;

    invoke-direct {v0}, LX/84e;-><init>()V

    sput-object v0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 1291031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1291032
    iput-wide p1, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->b:J

    .line 1291033
    iput-wide p3, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->a:J

    .line 1291034
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->c:Z

    .line 1291035
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->d:Ljava/util/List;

    .line 1291036
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1291037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1291038
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->b:J

    .line 1291039
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 1291040
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 1291041
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->c:Z

    .line 1291042
    iget-boolean v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->c:Z

    if-eqz v0, :cond_0

    .line 1291043
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->a:J

    .line 1291044
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->d:Ljava/util/List;

    .line 1291045
    iget-object v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1291046
    :goto_0
    return-void

    .line 1291047
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->a:J

    .line 1291048
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->d:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1291049
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1291050
    iget-wide v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1291051
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->c:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 1291052
    iget-boolean v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->c:Z

    if-eqz v0, :cond_0

    .line 1291053
    iget-object v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1291054
    :goto_0
    return-void

    .line 1291055
    :cond_0
    iget-wide v0, p0, Lcom/facebook/friends/methods/BlockUserMethod$Params;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0
.end method
