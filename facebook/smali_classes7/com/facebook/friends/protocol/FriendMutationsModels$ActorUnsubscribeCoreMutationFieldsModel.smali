.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x431b1303
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293844
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293843
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1293841
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1293842
    return-void
.end method

.method private a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293839
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    .line 1293840
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1293845
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293846
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1293847
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1293848
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1293849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1293831
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293832
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1293833
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    .line 1293834
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1293835
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;

    .line 1293836
    iput-object v0, v1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    .line 1293837
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293838
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1293826
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;-><init>()V

    .line 1293827
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1293828
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1293830
    const v0, -0x86fb912

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1293829
    const v0, 0x4bdcc0b1    # 2.8934498E7f

    return v0
.end method
