.class public final Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1291540c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292376
    const-class v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292375
    const-class v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1292373
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1292374
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1292366
    iput-object p1, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1292367
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1292368
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1292369
    if-eqz v0, :cond_0

    .line 1292370
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1292371
    :cond_0
    return-void

    .line 1292372
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292364
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1292365
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1292334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292335
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1292336
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1292337
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1292338
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1292339
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1292340
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1292341
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1292342
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1292343
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1292344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1292356
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292357
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292358
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1292359
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1292360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    .line 1292361
    iput-object v0, v1, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1292362
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292363
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1292355
    new-instance v0, LX/85A;

    invoke-direct {v0, p1}, LX/85A;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292354
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1292377
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1292378
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1292379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1292380
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1292381
    :goto_0
    return-void

    .line 1292382
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1292351
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1292352
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1292353
    :cond_0
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292349
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1292350
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1292346
    new-instance v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;-><init>()V

    .line 1292347
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1292348
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292332
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1292333
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292330
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1292331
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292329
    const v0, 0x1c0303da

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292328
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292327
    const v0, 0x285feb

    return v0
.end method
