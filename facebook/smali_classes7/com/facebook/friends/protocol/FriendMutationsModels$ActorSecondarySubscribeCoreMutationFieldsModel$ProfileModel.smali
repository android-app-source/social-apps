.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55e6780e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293470
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293424
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1293468
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1293469
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 1293461
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1293462
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1293463
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1293464
    if-eqz v0, :cond_0

    .line 1293465
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1293466
    :cond_0
    return-void

    .line 1293467
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293458
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1293459
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1293460
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293456
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->f:Ljava/lang/String;

    .line 1293457
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293454
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1293455
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1293444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293445
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1293446
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1293447
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1293448
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1293449
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1293450
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1293451
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1293452
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293453
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1293441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293443
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1293440
    new-instance v0, LX/85i;

    invoke-direct {v0, p1}, LX/85i;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293439
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1293433
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293434
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1293435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1293436
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1293437
    :goto_0
    return-void

    .line 1293438
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1293430
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293431
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 1293432
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1293427
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;-><init>()V

    .line 1293428
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1293429
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1293426
    const v0, -0x2b63d846

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1293425
    const v0, 0x50c72189

    return v0
.end method
