.class public final Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2bc5ce14
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292004
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1291976
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1291977
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1291978
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1291979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1291980
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1291981
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1291982
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1291983
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1291984
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1291985
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1291986
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1291987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1291988
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1291989
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1291990
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1291991
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1291992
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1291993
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1291994
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1291995
    new-instance v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;-><init>()V

    .line 1291996
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1291997
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1291998
    const v0, -0x6100574a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1291999
    const v0, 0x55212970

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292000
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->f:Ljava/lang/String;

    .line 1292001
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292002
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->g:Ljava/lang/String;

    .line 1292003
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method
