.class public final Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1291243
    const-class v0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;

    new-instance v1, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1291244
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1291242
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1291165
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1291166
    const/4 v2, 0x0

    .line 1291167
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1291168
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291169
    :goto_0
    move v1, v2

    .line 1291170
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1291171
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1291172
    new-instance v1, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;-><init>()V

    .line 1291173
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1291174
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1291175
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1291176
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1291177
    :cond_0
    return-object v1

    .line 1291178
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291179
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1291180
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1291181
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291182
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1291183
    const-string v5, "actor"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1291184
    invoke-static {p1, v0}, LX/84m;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1291185
    :cond_3
    const-string v5, "daily_dialogue_contact_importer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1291186
    const/4 v4, 0x0

    .line 1291187
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 1291188
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291189
    :goto_2
    move v1, v4

    .line 1291190
    goto :goto_1

    .line 1291191
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1291192
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1291193
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1291194
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1291195
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291196
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1291197
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1291198
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291199
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_7

    if-eqz v8, :cond_7

    .line 1291200
    const-string v9, "favicon_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1291201
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_3

    .line 1291202
    :cond_8
    const-string v9, "main_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1291203
    invoke-static {p1, v0}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_3

    .line 1291204
    :cond_9
    const-string v9, "subtitle"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1291205
    const/4 v8, 0x0

    .line 1291206
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_10

    .line 1291207
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291208
    :goto_4
    move v5, v8

    .line 1291209
    goto :goto_3

    .line 1291210
    :cond_a
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1291211
    const/4 v8, 0x0

    .line 1291212
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v9, :cond_14

    .line 1291213
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291214
    :goto_5
    move v1, v8

    .line 1291215
    goto :goto_3

    .line 1291216
    :cond_b
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1291217
    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1291218
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1291219
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1291220
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1291221
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_c
    move v1, v4

    move v5, v4

    move v6, v4

    move v7, v4

    goto :goto_3

    .line 1291222
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291223
    :cond_e
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_f

    .line 1291224
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1291225
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291226
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v9, :cond_e

    .line 1291227
    const-string p0, "text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1291228
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 1291229
    :cond_f
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1291230
    invoke-virtual {v0, v8, v5}, LX/186;->b(II)V

    .line 1291231
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_4

    :cond_10
    move v5, v8

    goto :goto_6

    .line 1291232
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291233
    :cond_12
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_13

    .line 1291234
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1291235
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291236
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_12

    if-eqz v9, :cond_12

    .line 1291237
    const-string p0, "text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 1291238
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_7

    .line 1291239
    :cond_13
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1291240
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1291241
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_14
    move v1, v8

    goto :goto_7
.end method
