.class public final Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1292261
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1292262
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1292263
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1292264
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1292265
    if-nez p1, :cond_0

    .line 1292266
    :goto_0
    return v0

    .line 1292267
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1292268
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1292269
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1292270
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1292271
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1292272
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1292273
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3f0bbc2d
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1292278
    new-instance v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1292274
    packed-switch p0, :pswitch_data_0

    .line 1292275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1292276
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3f0bbc2d
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1292277
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1292258
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->b(I)V

    .line 1292259
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1292253
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1292254
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1292255
    :cond_0
    iput-object p1, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1292256
    iput p2, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->b:I

    .line 1292257
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1292260
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1292252
    new-instance v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292249
    iget v0, p0, LX/1vt;->c:I

    .line 1292250
    move v0, v0

    .line 1292251
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292246
    iget v0, p0, LX/1vt;->c:I

    .line 1292247
    move v0, v0

    .line 1292248
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1292243
    iget v0, p0, LX/1vt;->b:I

    .line 1292244
    move v0, v0

    .line 1292245
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292240
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1292241
    move-object v0, v0

    .line 1292242
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1292231
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1292232
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1292233
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1292234
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1292235
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1292236
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1292237
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1292238
    invoke-static {v3, v9, v2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1292239
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1292228
    iget v0, p0, LX/1vt;->c:I

    .line 1292229
    move v0, v0

    .line 1292230
    return v0
.end method
