.class public final Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1297124
    const-class v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;

    new-instance v1, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1297125
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1297126
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1297127
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1297128
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1297129
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1297130
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1297131
    if-eqz p0, :cond_0

    .line 1297132
    const-string p0, "friendship_status"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1297133
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1297134
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1297135
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1297136
    check-cast p1, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;->a(Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
