.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1294740
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;

    new-instance v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1294741
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1294763
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1294743
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1294744
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1294745
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1294746
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1294747
    if-eqz v2, :cond_2

    .line 1294748
    const-string p0, "viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1294749
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1294750
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1294751
    if-eqz p0, :cond_1

    .line 1294752
    const-string v0, "friending_possibilities"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1294753
    const/4 v0, 0x0

    .line 1294754
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1294755
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1294756
    if-eqz v0, :cond_0

    .line 1294757
    const-string v2, "total_possibility_count"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1294758
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1294759
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1294760
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1294761
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1294762
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1294742
    check-cast p1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Serializer;->a(Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
