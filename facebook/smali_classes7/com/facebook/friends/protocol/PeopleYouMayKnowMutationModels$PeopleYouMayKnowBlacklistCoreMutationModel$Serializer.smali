.class public final Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1297433
    const-class v0, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel;

    new-instance v1, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1297434
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1297432
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1297422
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1297423
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1297424
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1297425
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1297426
    if-eqz v2, :cond_0

    .line 1297427
    const-string p0, "blacklisted_user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1297428
    invoke-static {v1, v2, p1}, LX/86Y;->a(LX/15i;ILX/0nX;)V

    .line 1297429
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1297430
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1297431
    check-cast p1, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel$Serializer;->a(Lcom/facebook/friends/protocol/PeopleYouMayKnowMutationModels$PeopleYouMayKnowBlacklistCoreMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
