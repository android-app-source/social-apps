.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e157d63
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295290
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295374
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1295375
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1295376
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1295377
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1295378
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295379
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295380
    if-eqz v0, :cond_0

    .line 1295381
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295382
    :cond_0
    return-void

    .line 1295383
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 1295384
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1295385
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295386
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295387
    if-eqz v0, :cond_0

    .line 1295388
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295389
    :cond_0
    return-void

    .line 1295390
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1295391
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1295392
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295393
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295394
    if-eqz v0, :cond_0

    .line 1295395
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295396
    :cond_0
    return-void

    .line 1295397
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1295398
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->f:Z

    .line 1295399
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295400
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295401
    if-eqz v0, :cond_0

    .line 1295402
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295403
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1295404
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->g:Z

    .line 1295405
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295406
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295407
    if-eqz v0, :cond_0

    .line 1295408
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295409
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1295410
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->h:Z

    .line 1295411
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295412
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295413
    if-eqz v0, :cond_0

    .line 1295414
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295415
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295416
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1295417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1295418
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1295419
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295420
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->f:Z

    return v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1295421
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295422
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->g:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1295370
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295371
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->h:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295372
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->j:Ljava/lang/String;

    .line 1295373
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295368
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1295369
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295366
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1295367
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1295348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295349
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1295350
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1295351
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1295352
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1295353
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1295354
    const/16 v5, 0x9

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1295355
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1295356
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295357
    const/4 v0, 0x2

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->g:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295358
    const/4 v0, 0x3

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->h:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295359
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1295360
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1295361
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1295362
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1295363
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1295364
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295365
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1295345
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295347
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1295344
    new-instance v0, LX/85r;

    invoke-direct {v0, p1}, LX/85r;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295343
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1295337
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1295338
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->f:Z

    .line 1295339
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->g:Z

    .line 1295340
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->h:Z

    .line 1295341
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->k:Z

    .line 1295342
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1295311
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1295312
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295314
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1295315
    :goto_0
    return-void

    .line 1295316
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1295317
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295319
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295320
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295321
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295323
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295324
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1295325
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295327
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295328
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1295329
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295330
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295331
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295332
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1295333
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295335
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1295336
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1295298
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1295299
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->a(Z)V

    .line 1295300
    :cond_0
    :goto_0
    return-void

    .line 1295301
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295302
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->b(Z)V

    goto :goto_0

    .line 1295303
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1295304
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->c(Z)V

    goto :goto_0

    .line 1295305
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1295306
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 1295307
    :cond_4
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1295308
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 1295309
    :cond_5
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1295310
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1295295
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;-><init>()V

    .line 1295296
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1295297
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1295294
    const v0, 0x2494ea07

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1295293
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295291
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1295292
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionIgnoreCoreMutationFieldsModel$IgnoredSuggestedFriendModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method
