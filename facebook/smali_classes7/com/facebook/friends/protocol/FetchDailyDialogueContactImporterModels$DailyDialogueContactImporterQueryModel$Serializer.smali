.class public final Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1291245
    const-class v0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;

    new-instance v1, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1291246
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1291247
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1291248
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1291249
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1291250
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291251
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1291252
    if-eqz v2, :cond_0

    .line 1291253
    const-string p0, "actor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291254
    invoke-static {v1, v2, p1, p2}, LX/84m;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1291255
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1291256
    if-eqz v2, :cond_7

    .line 1291257
    const-string p0, "daily_dialogue_contact_importer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291258
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291259
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1291260
    if-eqz p0, :cond_1

    .line 1291261
    const-string v0, "favicon_image"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291262
    invoke-static {v1, p0, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1291263
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1291264
    if-eqz p0, :cond_2

    .line 1291265
    const-string v0, "main_image"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291266
    invoke-static {v1, p0, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1291267
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1291268
    if-eqz p0, :cond_4

    .line 1291269
    const-string v0, "subtitle"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291270
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291271
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1291272
    if-eqz v0, :cond_3

    .line 1291273
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291274
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1291275
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291276
    :cond_4
    const/4 p0, 0x3

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1291277
    if-eqz p0, :cond_6

    .line 1291278
    const-string v0, "title"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291279
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291280
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1291281
    if-eqz v0, :cond_5

    .line 1291282
    const-string v2, "text"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291283
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1291284
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291285
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291286
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291287
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1291288
    check-cast p1, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$Serializer;->a(Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
