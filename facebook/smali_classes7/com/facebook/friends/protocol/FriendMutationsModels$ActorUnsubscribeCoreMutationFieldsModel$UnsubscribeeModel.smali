.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1db2d7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293806
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293823
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1293821
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1293822
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 1293814
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->h:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1293815
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1293816
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1293817
    if-eqz v0, :cond_0

    .line 1293818
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1293819
    :cond_0
    return-void

    .line 1293820
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1293807
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1293808
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1293809
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1293810
    if-eqz v0, :cond_0

    .line 1293811
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1293812
    :cond_0
    return-void

    .line 1293813
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1293800
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->j:Z

    .line 1293801
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1293802
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1293803
    if-eqz v0, :cond_0

    .line 1293804
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1293805
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293797
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1293798
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1293799
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293795
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->f:Ljava/lang/String;

    .line 1293796
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293793
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->h:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->h:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1293794
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->h:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293791
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1293792
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->i:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1293824
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1293825
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->j:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1293777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293778
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1293779
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1293780
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1293781
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1293782
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1293783
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1293784
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1293785
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1293786
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1293787
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1293788
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1293789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293790
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1293774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293776
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1293773
    new-instance v0, LX/85k;

    invoke-direct {v0, p1}, LX/85k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293772
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1293768
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1293769
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->g:Z

    .line 1293770
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->j:Z

    .line 1293771
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1293742
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293743
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->l()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1293744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1293745
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    .line 1293746
    :goto_0
    return-void

    .line 1293747
    :cond_0
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1293748
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1293749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1293750
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1293751
    :cond_1
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1293752
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1293753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1293754
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1293755
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1293761
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1293762
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 1293763
    :cond_0
    :goto_0
    return-void

    .line 1293764
    :cond_1
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1293765
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0

    .line 1293766
    :cond_2
    const-string v0, "video_channel_is_viewer_following"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293767
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;->a(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1293758
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$UnsubscribeeModel;-><init>()V

    .line 1293759
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1293760
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1293757
    const v0, -0xbe1b7d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1293756
    const v0, 0x3c2b9d5

    return v0
.end method
