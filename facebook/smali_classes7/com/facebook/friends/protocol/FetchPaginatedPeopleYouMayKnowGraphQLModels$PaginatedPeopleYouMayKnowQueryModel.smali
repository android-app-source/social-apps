.class public final Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3d6171a0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292572
    const-class v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292571
    const-class v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1292569
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1292570
    return-void
.end method

.method private j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllUsers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292567
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->f:Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->f:Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    .line 1292568
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->f:Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1292558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292559
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1292560
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1292561
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1292562
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1292563
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1292564
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1292565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1292550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292551
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292552
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    .line 1292553
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1292554
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;

    .line 1292555
    iput-object v0, v1, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->f:Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    .line 1292556
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292557
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1292549
    new-instance v0, LX/85B;

    invoke-direct {v0, p1}, LX/85B;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292546
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1292547
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1292548
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1292543
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1292544
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->g:I

    .line 1292545
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1292541
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1292542
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1292532
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1292539
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1292540
    iget v0, p0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->g:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1292536
    new-instance v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;-><init>()V

    .line 1292537
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1292538
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllUsers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292535
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->j()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292534
    const v0, 0x540fd999

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292533
    const v0, 0x252222

    return v0
.end method
