.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e157d63
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295148
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295149
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1295150
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1295151
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1295152
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1295153
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295154
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295155
    if-eqz v0, :cond_0

    .line 1295156
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295157
    :cond_0
    return-void

    .line 1295158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 1295159
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1295160
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295161
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295162
    if-eqz v0, :cond_0

    .line 1295163
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295164
    :cond_0
    return-void

    .line 1295165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1295166
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1295167
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295168
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295169
    if-eqz v0, :cond_0

    .line 1295170
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1295171
    :cond_0
    return-void

    .line 1295172
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1295173
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->f:Z

    .line 1295174
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295175
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295176
    if-eqz v0, :cond_0

    .line 1295177
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295178
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1295179
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->g:Z

    .line 1295180
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295181
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295182
    if-eqz v0, :cond_0

    .line 1295183
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295184
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1295185
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->h:Z

    .line 1295186
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1295187
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1295188
    if-eqz v0, :cond_0

    .line 1295189
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1295190
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295191
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1295192
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1295193
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1295194
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295195
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->f:Z

    return v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1295196
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295197
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->g:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1295146
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1295147
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->h:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295198
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->j:Ljava/lang/String;

    .line 1295199
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295144
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1295145
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295142
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1295143
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1295124
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295125
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1295126
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1295127
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1295128
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1295129
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1295130
    const/16 v5, 0x9

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1295131
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1295132
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295133
    const/4 v0, 0x2

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->g:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295134
    const/4 v0, 0x3

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->h:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1295135
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1295136
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1295137
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1295138
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1295139
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1295140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295141
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1295067
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295068
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295069
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1295123
    new-instance v0, LX/85q;

    invoke-direct {v0, p1}, LX/85q;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295122
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1295116
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1295117
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->f:Z

    .line 1295118
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->g:Z

    .line 1295119
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->h:Z

    .line 1295120
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->k:Z

    .line 1295121
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1295090
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1295091
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295093
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1295094
    :goto_0
    return-void

    .line 1295095
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1295096
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295098
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295099
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295100
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295102
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295103
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1295104
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295106
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295107
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1295108
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295109
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295110
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1295111
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1295112
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1295113
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1295114
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1295115
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1295077
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1295078
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->a(Z)V

    .line 1295079
    :cond_0
    :goto_0
    return-void

    .line 1295080
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295081
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->b(Z)V

    goto :goto_0

    .line 1295082
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1295083
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->c(Z)V

    goto :goto_0

    .line 1295084
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1295085
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 1295086
    :cond_4
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1295087
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 1295088
    :cond_5
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1295089
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1295074
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;-><init>()V

    .line 1295075
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1295076
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1295073
    const v0, -0x721a2f62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1295072
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295070
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1295071
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestSendCoreMutationFieldsModel$FriendRequesteesModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method
