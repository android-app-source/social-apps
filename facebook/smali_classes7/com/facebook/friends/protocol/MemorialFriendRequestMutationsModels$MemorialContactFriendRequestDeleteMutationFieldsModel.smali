.class public final Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3642a959
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297078
    const-class v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297079
    const-class v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1297080
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1297081
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1297082
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1297083
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1297084
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1297085
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1297086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1297087
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1297088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1297089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1297090
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1297091
    iget-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1297092
    iget-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1297093
    new-instance v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$MemorialContactFriendRequestDeleteMutationFieldsModel;-><init>()V

    .line 1297094
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1297095
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1297096
    const v0, -0x516eeea3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1297097
    const v0, 0x3cdf61f5

    return v0
.end method
