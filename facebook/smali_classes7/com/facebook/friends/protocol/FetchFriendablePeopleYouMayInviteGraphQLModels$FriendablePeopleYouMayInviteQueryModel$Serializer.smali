.class public final Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1291882
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    new-instance v1, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1291883
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1291884
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1291885
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1291886
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1291887
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291888
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1291889
    if-eqz v2, :cond_7

    .line 1291890
    const-string v3, "people_you_may_invite"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291891
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291892
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1291893
    if-eqz v3, :cond_5

    .line 1291894
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291895
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1291896
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 1291897
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1291898
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1291899
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1291900
    if-eqz p0, :cond_0

    .line 1291901
    const-string v0, "name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291902
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1291903
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1291904
    if-eqz p0, :cond_1

    .line 1291905
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291906
    invoke-static {v1, p0, p1}, LX/84z;->a(LX/15i;ILX/0nX;)V

    .line 1291907
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1291908
    if-eqz p0, :cond_2

    .line 1291909
    const-string v0, "tracking_data_id"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291910
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1291911
    :cond_2
    const/4 p0, 0x3

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1291912
    if-eqz p0, :cond_3

    .line 1291913
    const-string v0, "tracking_data_key"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291914
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1291915
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291916
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1291917
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1291918
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1291919
    if-eqz v3, :cond_6

    .line 1291920
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1291921
    invoke-static {v1, v3, p1}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 1291922
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291923
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1291924
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1291925
    check-cast p1, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Serializer;->a(Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
