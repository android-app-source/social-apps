.class public final Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3642a959
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297153
    const-class v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297152
    const-class v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1297150
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1297151
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1297144
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1297145
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1297146
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1297147
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1297148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1297149
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1297154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1297155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1297156
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1297142
    iget-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1297143
    iget-object v0, p0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1297139
    new-instance v0, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/MemorialFriendRequestMutationsModels$UserAcceptMemorialFriendRequestMutationFieldsModel;-><init>()V

    .line 1297140
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1297141
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1297138
    const v0, 0x46b5048

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1297137
    const v0, -0x53142553

    return v0
.end method
