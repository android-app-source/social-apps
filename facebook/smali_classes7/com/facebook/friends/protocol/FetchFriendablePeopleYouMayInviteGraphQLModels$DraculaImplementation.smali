.class public final Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1291710
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1291711
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1291745
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1291746
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1291747
    if-nez p1, :cond_0

    move v0, v1

    .line 1291748
    :goto_0
    return v0

    .line 1291749
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1291750
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1291751
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1291752
    const v2, -0x3ac381e3

    const/4 v4, 0x0

    .line 1291753
    if-nez v0, :cond_1

    move v3, v4

    .line 1291754
    :goto_1
    move v2, v3

    .line 1291755
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1291756
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1291757
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1291758
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1291759
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1291760
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1291761
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1291762
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1291763
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    .line 1291764
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1291765
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1291766
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1291767
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1291768
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1291769
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1291770
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1291771
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1291772
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1291773
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1291774
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1291775
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 1291776
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1291777
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1291778
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v8

    .line 1291779
    invoke-static {p0, v8, v2, p3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v8

    aput v8, v3, v4

    .line 1291780
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1291781
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1291782
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ac381e3 -> :sswitch_1
        -0x1bac8835 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1291783
    new-instance v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1291806
    if-eqz p0, :cond_0

    .line 1291807
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1291808
    if-eq v0, p0, :cond_0

    .line 1291809
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1291810
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1291784
    sparse-switch p2, :sswitch_data_0

    .line 1291785
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1291786
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1291787
    const v1, -0x3ac381e3

    .line 1291788
    if-eqz v0, :cond_0

    .line 1291789
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v4

    .line 1291790
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    .line 1291791
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result p2

    .line 1291792
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1291793
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1291794
    :cond_0
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1291795
    invoke-static {v0, p3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1291796
    :goto_1
    return-void

    .line 1291797
    :sswitch_1
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$PersonYouMayInviteFieldsModel;

    .line 1291798
    invoke-static {v0, p3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ac381e3 -> :sswitch_1
        -0x1bac8835 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1291799
    if-eqz p1, :cond_0

    .line 1291800
    invoke-static {p0, p1, p2}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1291801
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;

    .line 1291802
    if-eq v0, v1, :cond_0

    .line 1291803
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1291804
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1291805
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1291738
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1291739
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1291740
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1291741
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1291742
    :cond_0
    iput-object p1, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1291743
    iput p2, p0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->b:I

    .line 1291744
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1291737
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1291736
    new-instance v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1291733
    iget v0, p0, LX/1vt;->c:I

    .line 1291734
    move v0, v0

    .line 1291735
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1291730
    iget v0, p0, LX/1vt;->c:I

    .line 1291731
    move v0, v0

    .line 1291732
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1291727
    iget v0, p0, LX/1vt;->b:I

    .line 1291728
    move v0, v0

    .line 1291729
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1291724
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1291725
    move-object v0, v0

    .line 1291726
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1291715
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1291716
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1291717
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1291718
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1291719
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1291720
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1291721
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1291722
    invoke-static {v3, v9, v2}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1291723
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1291712
    iget v0, p0, LX/1vt;->c:I

    .line 1291713
    move v0, v0

    .line 1291714
    return v0
.end method
