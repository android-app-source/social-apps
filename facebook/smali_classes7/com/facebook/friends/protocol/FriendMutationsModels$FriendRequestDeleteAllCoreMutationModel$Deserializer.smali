.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1294685
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;

    new-instance v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1294686
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1294687
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1294688
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1294689
    const/4 v2, 0x0

    .line 1294690
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1294691
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1294692
    :goto_0
    move v1, v2

    .line 1294693
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1294694
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1294695
    new-instance v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestDeleteAllCoreMutationModel;-><init>()V

    .line 1294696
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1294697
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1294698
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1294699
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1294700
    :cond_0
    return-object v1

    .line 1294701
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1294702
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1294703
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1294704
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1294705
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1294706
    const-string v4, "viewer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1294707
    const/4 v3, 0x0

    .line 1294708
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1294709
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1294710
    :goto_2
    move v1, v3

    .line 1294711
    goto :goto_1

    .line 1294712
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1294713
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1294714
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1294715
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1294716
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1294717
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1294718
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1294719
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1294720
    const-string v5, "friending_possibilities"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1294721
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1294722
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_d

    .line 1294723
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1294724
    :goto_4
    move v1, v4

    .line 1294725
    goto :goto_3

    .line 1294726
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1294727
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1294728
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 1294729
    :cond_9
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_b

    .line 1294730
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1294731
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1294732
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v7, :cond_9

    .line 1294733
    const-string p0, "total_possibility_count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1294734
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    goto :goto_5

    .line 1294735
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1294736
    :cond_b
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1294737
    if-eqz v1, :cond_c

    .line 1294738
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1294739
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v1, v4

    move v6, v4

    goto :goto_5
.end method
