.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a1a09d3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293502
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1293501
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1293497
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1293498
    return-void
.end method

.method private a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293499
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    .line 1293500
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1293503
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293504
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1293505
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1293506
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1293507
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1293487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1293488
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1293489
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    .line 1293490
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1293491
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;

    .line 1293492
    iput-object v0, v1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel$ProfileModel;

    .line 1293493
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1293494
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1293484
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSecondarySubscribeCoreMutationFieldsModel;-><init>()V

    .line 1293485
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1293486
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1293495
    const v0, -0x10d0b513

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1293496
    const v0, -0x3a6f78ed

    return v0
.end method
