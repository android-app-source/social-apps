.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d52f0bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295575
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1295574
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1295551
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1295552
    return-void
.end method

.method private a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295572
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    .line 1295573
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1295566
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295567
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1295568
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1295569
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1295570
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295571
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1295558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1295559
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1295560
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    .line 1295561
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1295562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;

    .line 1295563
    iput-object v0, v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel$SuggestedFriendModel;

    .line 1295564
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1295565
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1295555
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendSuggestionSendMutationModel;-><init>()V

    .line 1295556
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1295557
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1295554
    const v0, 0x5e7222d2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1295553
    const v0, 0x1198e2a3

    return v0
.end method
