.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1294021
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1294022
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1294019
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1294020
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1294006
    if-nez p1, :cond_0

    .line 1294007
    :goto_0
    return v0

    .line 1294008
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1294009
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1294010
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1294011
    const v2, -0x6f2ffee5

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1294012
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1294013
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1294014
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1294015
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1294016
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1294017
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1294018
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f2ffee5 -> :sswitch_1
        0x67208de1 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1293965
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1294001
    sparse-switch p2, :sswitch_data_0

    .line 1294002
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1294003
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1294004
    const v1, -0x6f2ffee5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1294005
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f2ffee5 -> :sswitch_1
        0x67208de1 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1293995
    if-eqz p1, :cond_0

    .line 1293996
    invoke-static {p0, p1, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;

    move-result-object v1

    .line 1293997
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;

    .line 1293998
    if-eq v0, v1, :cond_0

    .line 1293999
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1294000
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1293994
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1293992
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1293993
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1294023
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1294024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1294025
    :cond_0
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 1294026
    iput p2, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->b:I

    .line 1294027
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1293991
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1293990
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1293987
    iget v0, p0, LX/1vt;->c:I

    .line 1293988
    move v0, v0

    .line 1293989
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1293984
    iget v0, p0, LX/1vt;->c:I

    .line 1293985
    move v0, v0

    .line 1293986
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1293981
    iget v0, p0, LX/1vt;->b:I

    .line 1293982
    move v0, v0

    .line 1293983
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1293978
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1293979
    move-object v0, v0

    .line 1293980
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1293969
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1293970
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1293971
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1293972
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1293973
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1293974
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1293975
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1293976
    invoke-static {v3, v9, v2}, Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FriendMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1293977
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1293966
    iget v0, p0, LX/1vt;->c:I

    .line 1293967
    move v0, v0

    .line 1293968
    return v0
.end method
