.class public final Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3cbb1f2d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292178
    const-class v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292177
    const-class v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1292175
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1292176
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1292169
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292170
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1292171
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1292172
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1292173
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292174
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1292152
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292153
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292154
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    .line 1292155
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1292156
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;

    .line 1292157
    iput-object v0, v1, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->e:Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    .line 1292158
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292159
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1292168
    new-instance v0, LX/853;

    invoke-direct {v0, p1}, LX/853;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMemorialFriendingPossibilities"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292179
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->e:Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->e:Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    .line 1292180
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;->e:Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel$MemorialFriendingPossibilitiesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1292166
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1292167
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1292165
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1292162
    new-instance v0, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FetchMemorialFriendRequestsGraphQLModels$MemorialContactFriendingPossibilitiesModel;-><init>()V

    .line 1292163
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1292164
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292161
    const v0, 0x78f7fe3b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292160
    const v0, 0x285feb

    return v0
.end method
