.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4a92fe1b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1294675
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1294676
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1294673
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1294674
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1294667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1294668
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1294669
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1294670
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1294671
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1294672
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1294677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1294678
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1294679
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    .line 1294680
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1294681
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;

    .line 1294682
    iput-object v0, v1, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    .line 1294683
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1294684
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294665
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    .line 1294666
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;->e:Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1294662
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel;-><init>()V

    .line 1294663
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1294664
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1294660
    const v0, 0x32696343

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1294661
    const v0, 0x23bf4ec2

    return v0
.end method
