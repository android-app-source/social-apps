.class public final Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1292808
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1292809
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1292806
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1292807
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 1292783
    if-nez p1, :cond_0

    .line 1292784
    :goto_0
    return v0

    .line 1292785
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1292786
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1292787
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1292788
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1292789
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1292790
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1292791
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 1292792
    const v4, 0x69dac810

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1292793
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1292794
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1292795
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1292796
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1292797
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1292798
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1292799
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1292800
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1292801
    :sswitch_1
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1292802
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1292803
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1292804
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1292805
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x278752ac -> :sswitch_0
        0x69dac810 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1292774
    if-nez p0, :cond_0

    move v0, v1

    .line 1292775
    :goto_0
    return v0

    .line 1292776
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1292777
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1292778
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1292779
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1292780
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1292781
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1292782
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1292767
    const/4 v7, 0x0

    .line 1292768
    const/4 v1, 0x0

    .line 1292769
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1292770
    invoke-static {v2, v3, v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1292771
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1292772
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1292773
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1292766
    new-instance v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1292761
    sparse-switch p2, :sswitch_data_0

    .line 1292762
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1292763
    :sswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1292764
    const v1, 0x69dac810

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1292765
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x278752ac -> :sswitch_0
        0x69dac810 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1292755
    if-eqz p1, :cond_0

    .line 1292756
    invoke-static {p0, p1, p2}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1292757
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    .line 1292758
    if-eq v0, v1, :cond_0

    .line 1292759
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1292760
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1292754
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1292752
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1292753
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1292747
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1292748
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1292749
    :cond_0
    iput-object p1, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1292750
    iput p2, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->b:I

    .line 1292751
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1292721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1292746
    new-instance v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292743
    iget v0, p0, LX/1vt;->c:I

    .line 1292744
    move v0, v0

    .line 1292745
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292740
    iget v0, p0, LX/1vt;->c:I

    .line 1292741
    move v0, v0

    .line 1292742
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1292737
    iget v0, p0, LX/1vt;->b:I

    .line 1292738
    move v0, v0

    .line 1292739
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292734
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1292735
    move-object v0, v0

    .line 1292736
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1292725
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1292726
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1292727
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1292728
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1292729
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1292730
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1292731
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1292732
    invoke-static {v3, v9, v2}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1292733
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1292722
    iget v0, p0, LX/1vt;->c:I

    .line 1292723
    move v0, v0

    .line 1292724
    return v0
.end method
