.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e157d63
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1294578
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1294579
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1294580
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1294581
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1294582
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1294583
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294584
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294585
    if-eqz v0, :cond_0

    .line 1294586
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1294587
    :cond_0
    return-void

    .line 1294588
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 4

    .prologue
    .line 1294589
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1294590
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294591
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294592
    if-eqz v0, :cond_0

    .line 1294593
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1294594
    :cond_0
    return-void

    .line 1294595
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1294567
    iput-object p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1294568
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294569
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294570
    if-eqz v0, :cond_0

    .line 1294571
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1294572
    :cond_0
    return-void

    .line 1294573
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1294615
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->f:Z

    .line 1294616
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294617
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294618
    if-eqz v0, :cond_0

    .line 1294619
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1294620
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1294596
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->g:Z

    .line 1294597
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294598
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294599
    if-eqz v0, :cond_0

    .line 1294600
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1294601
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1294602
    iput-boolean p1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->h:Z

    .line 1294603
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1294604
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1294605
    if-eqz v0, :cond_0

    .line 1294606
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1294607
    :cond_0
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294608
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1294609
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1294610
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1294611
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1294612
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->f:Z

    return v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1294613
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1294614
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->g:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1294574
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1294575
    iget-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->h:Z

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294576
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->j:Ljava/lang/String;

    .line 1294577
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294513
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 1294514
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->l:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294488
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1294489
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1294490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1294491
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1294492
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1294493
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1294494
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1294495
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1294496
    const/16 v5, 0x9

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1294497
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1294498
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1294499
    const/4 v0, 0x2

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->g:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1294500
    const/4 v0, 0x3

    iget-boolean v5, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->h:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1294501
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1294502
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1294503
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1294504
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1294505
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1294506
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1294507
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1294508
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1294509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1294510
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1294511
    new-instance v0, LX/85o;

    invoke-direct {v0, p1}, LX/85o;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294512
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1294515
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1294516
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->f:Z

    .line 1294517
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->g:Z

    .line 1294518
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->h:Z

    .line 1294519
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->k:Z

    .line 1294520
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1294521
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1294522
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294524
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1294525
    :goto_0
    return-void

    .line 1294526
    :cond_0
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1294527
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294529
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1294530
    :cond_1
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1294531
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294533
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1294534
    :cond_2
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1294535
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294537
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1294538
    :cond_3
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1294539
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->p()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294540
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294541
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1294542
    :cond_4
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1294543
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1294544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1294545
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1294546
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1294547
    const-string v0, "can_viewer_message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1294548
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->a(Z)V

    .line 1294549
    :cond_0
    :goto_0
    return-void

    .line 1294550
    :cond_1
    const-string v0, "can_viewer_poke"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1294551
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->b(Z)V

    goto :goto_0

    .line 1294552
    :cond_2
    const-string v0, "can_viewer_post"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1294553
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->c(Z)V

    goto :goto_0

    .line 1294554
    :cond_3
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1294555
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0

    .line 1294556
    :cond_4
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1294557
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_0

    .line 1294558
    :cond_5
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1294559
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1294560
    new-instance v0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;-><init>()V

    .line 1294561
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1294562
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1294563
    const v0, 0x53c7d433

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1294564
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1294565
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1294566
    iget-object v0, p0, Lcom/facebook/friends/protocol/FriendMutationsModels$FriendRequestCancelCoreMutationFieldsModel$CancelledFriendRequesteeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method
