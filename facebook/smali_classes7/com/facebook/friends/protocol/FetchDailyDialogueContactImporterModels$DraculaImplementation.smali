.class public final Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1291383
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1291384
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1291413
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1291414
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1291385
    if-nez p1, :cond_0

    move v0, v1

    .line 1291386
    :goto_0
    return v0

    .line 1291387
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1291388
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1291389
    :sswitch_0
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1291390
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1291391
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1291392
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1291393
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 1291394
    const v4, -0x62a3b2b1

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1291395
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v4

    .line 1291396
    const v5, -0x3121e83

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 1291397
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1291398
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1291399
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1291400
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1291401
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1291402
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1291403
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1291404
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1291405
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1291406
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1291407
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1291408
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1291409
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1291410
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1291411
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1291412
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x62a3b2b1 -> :sswitch_1
        -0x3121e83 -> :sswitch_2
        0x680f5369 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1291382
    new-instance v0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1291377
    if-eqz p0, :cond_0

    .line 1291378
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1291379
    if-eq v0, p0, :cond_0

    .line 1291380
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1291381
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1291366
    sparse-switch p2, :sswitch_data_0

    .line 1291367
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1291368
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1291369
    invoke-static {v0, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1291370
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1291371
    invoke-static {v0, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1291372
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1291373
    const v1, -0x62a3b2b1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1291374
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1291375
    const v1, -0x3121e83

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1291376
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x62a3b2b1 -> :sswitch_1
        -0x3121e83 -> :sswitch_1
        0x680f5369 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1291360
    if-eqz p1, :cond_0

    .line 1291361
    invoke-static {p0, p1, p2}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;

    move-result-object v1

    .line 1291362
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;

    .line 1291363
    if-eq v0, v1, :cond_0

    .line 1291364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1291365
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1291359
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1291415
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1291416
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1291354
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1291355
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1291356
    :cond_0
    iput-object p1, p0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a:LX/15i;

    .line 1291357
    iput p2, p0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->b:I

    .line 1291358
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1291353
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1291352
    new-instance v0, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1291349
    iget v0, p0, LX/1vt;->c:I

    .line 1291350
    move v0, v0

    .line 1291351
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1291346
    iget v0, p0, LX/1vt;->c:I

    .line 1291347
    move v0, v0

    .line 1291348
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1291343
    iget v0, p0, LX/1vt;->b:I

    .line 1291344
    move v0, v0

    .line 1291345
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1291328
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1291329
    move-object v0, v0

    .line 1291330
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1291334
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1291335
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1291336
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1291337
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1291338
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1291339
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1291340
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1291341
    invoke-static {v3, v9, v2}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1291342
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1291331
    iget v0, p0, LX/1vt;->c:I

    .line 1291332
    move v0, v0

    .line 1291333
    return v0
.end method
