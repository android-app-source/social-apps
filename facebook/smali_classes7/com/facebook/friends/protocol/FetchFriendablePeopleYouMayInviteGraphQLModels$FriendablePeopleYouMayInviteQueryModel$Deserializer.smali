.class public final Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1291811
    const-class v0, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    new-instance v1, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1291812
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1291813
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1291814
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1291815
    const/4 v2, 0x0

    .line 1291816
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1291817
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291818
    :goto_0
    move v1, v2

    .line 1291819
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1291820
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1291821
    new-instance v1, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FetchFriendablePeopleYouMayInviteGraphQLModels$FriendablePeopleYouMayInviteQueryModel;-><init>()V

    .line 1291822
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1291823
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1291824
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1291825
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1291826
    :cond_0
    return-object v1

    .line 1291827
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291828
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1291829
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1291830
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291831
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1291832
    const-string v4, "people_you_may_invite"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1291833
    const/4 v3, 0x0

    .line 1291834
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1291835
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291836
    :goto_2
    move v1, v3

    .line 1291837
    goto :goto_1

    .line 1291838
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1291839
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1291840
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1291841
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291842
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1291843
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1291844
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291845
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 1291846
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1291847
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1291848
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_7

    .line 1291849
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1291850
    const/4 v6, 0x0

    .line 1291851
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_11

    .line 1291852
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291853
    :goto_5
    move v5, v6

    .line 1291854
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1291855
    :cond_7
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1291856
    goto :goto_3

    .line 1291857
    :cond_8
    const-string v6, "page_info"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1291858
    invoke-static {p1, v0}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_3

    .line 1291859
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1291860
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1291861
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1291862
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 1291863
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1291864
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_10

    .line 1291865
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1291866
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1291867
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v10, :cond_c

    .line 1291868
    const-string p0, "name"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 1291869
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_6

    .line 1291870
    :cond_d
    const-string p0, "node"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 1291871
    invoke-static {p1, v0}, LX/84z;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_6

    .line 1291872
    :cond_e
    const-string p0, "tracking_data_id"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 1291873
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 1291874
    :cond_f
    const-string p0, "tracking_data_key"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1291875
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 1291876
    :cond_10
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1291877
    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 1291878
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 1291879
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1291880
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1291881
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_11
    move v5, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto :goto_6
.end method
