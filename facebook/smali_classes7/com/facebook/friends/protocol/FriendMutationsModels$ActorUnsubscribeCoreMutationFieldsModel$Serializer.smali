.class public final Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1293718
    const-class v0, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;

    new-instance v1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1293719
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1293707
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1293708
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1293709
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1293710
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1293711
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1293712
    if-eqz v2, :cond_0

    .line 1293713
    const-string p0, "unsubscribee"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1293714
    invoke-static {v1, v2, p1}, LX/85x;->a(LX/15i;ILX/0nX;)V

    .line 1293715
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1293716
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1293717
    check-cast p1, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel$Serializer;->a(Lcom/facebook/friends/protocol/FriendMutationsModels$ActorUnsubscribeCoreMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
