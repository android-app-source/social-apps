.class public final Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x64e43c6f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292939
    const-class v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1292940
    const-class v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1292941
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1292942
    return-void
.end method

.method private j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292960
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->f:Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->f:Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    .line 1292961
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->f:Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1292943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292944
    invoke-virtual {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1292945
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1292946
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1292947
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1292948
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1292949
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1292950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292951
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1292952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1292953
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1292954
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    .line 1292955
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1292956
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;

    .line 1292957
    iput-object v0, v1, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->f:Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    .line 1292958
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1292959
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1292932
    new-instance v0, LX/85K;

    invoke-direct {v0, p1}, LX/85K;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292933
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1292934
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1292935
    :cond_0
    iget-object v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1292936
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1292937
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->g:I

    .line 1292938
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1292921
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1292922
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1292923
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1292924
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1292925
    iget v0, p0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->g:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1292926
    new-instance v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;

    invoke-direct {v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;-><init>()V

    .line 1292927
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1292928
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1292929
    invoke-direct {p0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteQueryModel;->j()Lcom/facebook/friends/protocol/FetchPeopleYouMayInviteGraphQLModels$PeopleYouMayInviteFeedUnitContactsConnectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1292930
    const v0, -0x43a3ecf6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1292931
    const v0, 0x252222

    return v0
.end method
