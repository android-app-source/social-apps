.class public Lcom/facebook/friends/ui/FriendingButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:LX/33M;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1297555
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1297556
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friends/ui/FriendingButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1297557
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1297558
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1297559
    invoke-direct {p0, p1, p2}, Lcom/facebook/friends/ui/FriendingButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1297560
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1297561
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1297562
    invoke-direct {p0, p1, p2}, Lcom/facebook/friends/ui/FriendingButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1297563
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1297564
    new-instance v0, LX/33M;

    invoke-direct {v0}, LX/33M;-><init>()V

    iput-object v0, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    .line 1297565
    iget-object v0, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    invoke-virtual {v0, p1, p2}, LX/33M;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1297566
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 1297567
    iget-object v0, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    invoke-virtual {v0, p1}, LX/33M;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1297568
    invoke-virtual {p0}, Lcom/facebook/friends/ui/FriendingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1297569
    iget-object v1, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    .line 1297570
    iget v2, v1, LX/33M;->a:I

    move v1, v2

    .line 1297571
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1297572
    iget-object v2, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    .line 1297573
    iget p1, v2, LX/33M;->b:I

    move v2, p1

    .line 1297574
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1297575
    invoke-virtual {p0, v1}, Lcom/facebook/friends/ui/FriendingButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1297576
    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/FriendingButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1297577
    return-void
.end method

.method public setActiveSource(I)V
    .locals 1

    .prologue
    .line 1297578
    iget-object v0, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    .line 1297579
    iput p1, v0, LX/33M;->d:I

    .line 1297580
    return-void
.end method

.method public setInactiveSource(I)V
    .locals 1

    .prologue
    .line 1297581
    iget-object v0, p0, Lcom/facebook/friends/ui/FriendingButton;->a:LX/33M;

    .line 1297582
    iput p1, v0, LX/33M;->e:I

    .line 1297583
    return-void
.end method
