.class public final Lcom/facebook/mqttlite/MqttPublishQueue$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Hh;

.field public final synthetic b:LX/6mp;

.field public final synthetic c:LX/1th;


# direct methods
.method public constructor <init>(LX/1th;LX/0Hh;LX/6mp;)V
    .locals 0

    .prologue
    .line 1146689
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->c:LX/1th;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->a:LX/0Hh;

    iput-object p3, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->b:LX/6mp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1146690
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->a:LX/0Hh;

    invoke-interface {v0}, LX/0Hh;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v5, ""

    .line 1146691
    :goto_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->c:LX/1th;

    iget-object v0, v0, LX/1th;->d:LX/05h;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->b:LX/6mp;

    iget-object v1, v1, LX/6mp;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->b:LX/6mp;

    iget v2, v2, LX/6mp;->g:I

    iget-object v3, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->b:LX/6mp;

    invoke-virtual {v3}, LX/6mp;->c()I

    move-result v3

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    iget-object v6, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->c:LX/1th;

    iget-object v6, v6, LX/1th;->e:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v6}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    iget-object v8, p0, Lcom/facebook/mqttlite/MqttPublishQueue$2;->b:LX/6mp;

    iget-wide v8, v8, LX/6mp;->f:J

    sub-long/2addr v6, v8

    .line 1146692
    const/16 v8, 0xc

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "operation"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    const-string v10, "msg_id"

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    const-string v10, "retry_count"

    aput-object v10, v8, v9

    const/4 v9, 0x5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x6

    const-string v10, "result"

    aput-object v10, v8, v9

    const/4 v9, 0x7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0x8

    const-string v10, "error_message"

    aput-object v10, v8, v9

    const/16 v9, 0x9

    aput-object v5, v8, v9

    const/16 v9, 0xa

    const-string v10, "timespan_ms"

    aput-object v10, v8, v9

    const/16 v9, 0xb

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v8}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v8

    .line 1146693
    const-string v9, "mqtt_queue_message"

    invoke-virtual {v0, v9, v8}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1146694
    return-void

    .line 1146695
    :cond_0
    const-string v5, "TimeoutException"

    goto :goto_0
.end method
