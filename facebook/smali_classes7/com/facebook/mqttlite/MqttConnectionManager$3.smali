.class public final Lcom/facebook/mqttlite/MqttConnectionManager$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6mq;

.field public final synthetic b:LX/6mp;

.field public final synthetic c:LX/1tA;


# direct methods
.method public constructor <init>(LX/1tA;LX/6mq;LX/6mp;)V
    .locals 0

    .prologue
    .line 1146636
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->c:LX/1tA;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->a:LX/6mq;

    iput-object p3, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->b:LX/6mp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1146637
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->a:LX/6mq;

    invoke-virtual {v0}, LX/0B1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146638
    const-string v0, "MqttConnectionManager"

    const-string v1, "MqttOperation is success, complete publishQueue operation, currentTime: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->c:LX/1tA;

    iget-object v4, v4, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146639
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->c:LX/1tA;

    iget-object v0, v0, LX/1tA;->w:LX/1th;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$3;->b:LX/6mp;

    iget v1, v1, LX/6mp;->g:I

    invoke-virtual {v0, v1}, LX/1th;->a(I)V

    .line 1146640
    :cond_0
    return-void
.end method
