.class public final Lcom/facebook/mqttlite/MqttConnectionManager$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6mp;

.field public final synthetic b:LX/6mq;

.field public final synthetic c:LX/1tA;


# direct methods
.method public constructor <init>(LX/1tA;LX/6mp;LX/6mq;)V
    .locals 0

    .prologue
    .line 1146641
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->c:LX/1tA;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->a:LX/6mp;

    iput-object p3, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->b:LX/6mq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1146642
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->c:LX/1tA;

    iget-object v0, v0, LX/056;->m:LX/072;

    if-nez v0, :cond_0

    .line 1146643
    const-string v0, "MqttConnectionManager"

    const-string v1, "Preemptive timer fired, starting new connection %d %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->a:LX/6mp;

    iget v4, v4, LX/6mp;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->b:LX/6mq;

    iget v4, v4, LX/0B1;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/05D;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146644
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->c:LX/1tA;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->c:LX/1tA;

    .line 1146645
    invoke-virtual {v1}, LX/056;->c()LX/072;

    move-result-object v2

    move-object v1, v2

    .line 1146646
    iput-object v1, v0, LX/1tA;->m:LX/072;

    .line 1146647
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->c:LX/1tA;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$4;->b:LX/6mq;

    iget v1, v1, LX/0B1;->c:I

    .line 1146648
    iput v1, v0, LX/1tA;->n:I

    .line 1146649
    :cond_0
    return-void
.end method
