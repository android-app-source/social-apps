.class public final Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:[B

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:LX/6mt;


# direct methods
.method public constructor <init>(LX/6mt;Ljava/lang/String;[BII)V
    .locals 0

    .prologue
    .line 1146755
    iput-object p1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->e:LX/6mt;

    iput-object p2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->b:[B

    iput p4, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->c:I

    iput p5, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1146756
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->e:LX/6mt;

    .line 1146757
    iget-boolean v1, v0, LX/6mt;->d:Z

    move v0, v1

    .line 1146758
    if-nez v0, :cond_0

    .line 1146759
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->e:LX/6mt;

    iget-object v0, v0, LX/6mt;->b:Lcom/facebook/proxygen/MQTTClient;

    iget-object v1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->b:[B

    iget v3, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->c:I

    iget v4, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$4;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/proxygen/MQTTClient;->publish(Ljava/lang/String;[BII)V

    .line 1146760
    :goto_0
    return-void

    .line 1146761
    :cond_0
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "publish ignored as client has been closed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
