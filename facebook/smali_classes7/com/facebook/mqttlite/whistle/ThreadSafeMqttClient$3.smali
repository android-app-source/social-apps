.class public final Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:[Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:LX/6mt;


# direct methods
.method public constructor <init>(LX/6mt;[Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1146748
    iput-object p1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->c:LX/6mt;

    iput-object p2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->a:[Ljava/lang/String;

    iput p3, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1146749
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->c:LX/6mt;

    .line 1146750
    iget-boolean v1, v0, LX/6mt;->d:Z

    move v0, v1

    .line 1146751
    if-nez v0, :cond_0

    .line 1146752
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->c:LX/6mt;

    iget-object v0, v0, LX/6mt;->b:Lcom/facebook/proxygen/MQTTClient;

    iget-object v1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->a:[Ljava/lang/String;

    iget v2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$3;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/proxygen/MQTTClient;->unSubscribe([Ljava/lang/String;I)V

    .line 1146753
    :goto_0
    return-void

    .line 1146754
    :cond_0
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "unsubscribe ignored as client has been closed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
