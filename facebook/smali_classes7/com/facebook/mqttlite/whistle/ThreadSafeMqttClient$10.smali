.class public final Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/6mt;


# direct methods
.method public constructor <init>(LX/6mt;)V
    .locals 0

    .prologue
    .line 1146723
    iput-object p1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;->a:LX/6mt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1146724
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;->a:LX/6mt;

    .line 1146725
    iget-boolean v1, v0, LX/6mt;->d:Z

    move v0, v1

    .line 1146726
    if-nez v0, :cond_0

    .line 1146727
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "closing client in thread"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146728
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;->a:LX/6mt;

    iget-object v0, v0, LX/6mt;->b:Lcom/facebook/proxygen/MQTTClient;

    invoke-virtual {v0}, Lcom/facebook/proxygen/MQTTClient;->close()V

    .line 1146729
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$10;->a:LX/6mt;

    const/4 v1, 0x1

    .line 1146730
    iput-boolean v1, v0, LX/6mt;->d:Z

    .line 1146731
    :goto_0
    return-void

    .line 1146732
    :cond_0
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "close ignored as client has been closed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
