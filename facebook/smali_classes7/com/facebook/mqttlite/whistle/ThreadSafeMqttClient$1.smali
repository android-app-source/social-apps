.class public final Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:[B

.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:Z

.field public final synthetic g:LX/6mt;


# direct methods
.method public constructor <init>(LX/6mt;Ljava/lang/String;I[BIIZ)V
    .locals 0

    .prologue
    .line 1146733
    iput-object p1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->g:LX/6mt;

    iput-object p2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->a:Ljava/lang/String;

    iput p3, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->b:I

    iput-object p4, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->c:[B

    iput p5, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->d:I

    iput p6, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->e:I

    iput-boolean p7, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1146734
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->g:LX/6mt;

    .line 1146735
    iget-boolean v1, v0, LX/6mt;->d:Z

    move v0, v1

    .line 1146736
    if-nez v0, :cond_0

    .line 1146737
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "connect to mqtt service in thread"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146738
    iget-object v0, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->g:LX/6mt;

    iget-object v0, v0, LX/6mt;->b:Lcom/facebook/proxygen/MQTTClient;

    iget-object v1, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->a:Ljava/lang/String;

    iget v2, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->b:I

    iget-object v3, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->c:[B

    iget v4, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->d:I

    iget v5, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->e:I

    iget-boolean v6, p0, Lcom/facebook/mqttlite/whistle/ThreadSafeMqttClient$1;->f:Z

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/proxygen/MQTTClient;->connect(Ljava/lang/String;I[BIIZ)V

    .line 1146739
    :goto_0
    return-void

    .line 1146740
    :cond_0
    sget-object v0, LX/6mt;->a:Ljava/lang/String;

    const-string v1, "connect ignored as client has been closed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
