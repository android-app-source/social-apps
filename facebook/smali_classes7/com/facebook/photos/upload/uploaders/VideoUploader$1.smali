.class public final Lcom/facebook/photos/upload/uploaders/VideoUploader$1;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Ob;

.field public final synthetic b:LX/8Om;


# direct methods
.method public constructor <init>(LX/8Om;LX/8Ob;)V
    .locals 0

    .prologue
    .line 1340299
    iput-object p1, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->b:LX/8Om;

    iput-object p2, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1340300
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v0, v0, LX/8Ob;->y:LX/73w;

    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v1, v1, LX/8Ob;->A:LX/74b;

    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v2, v2, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1340301
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1340302
    const-string v5, "upload_session_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340303
    sget-object v5, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_START:LX/74R;

    const/4 v6, 0x0

    invoke-static {v0, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340304
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->b:LX/8Om;

    iget-object v0, v0, LX/8Om;->h:LX/11H;

    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->b:LX/8Om;

    iget-object v1, v1, LX/8Om;->q:LX/8NN;

    new-instance v2, LX/8NO;

    iget-object v3, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v3, v3, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v4, v4, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340305
    iget-object v5, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v5

    .line 1340306
    iget-object v5, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v5, v5, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1340307
    iget-wide v8, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v6, v8

    .line 1340308
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LX/8NO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-class v3, LX/8Om;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1340309
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v0, v0, LX/8Ob;->y:LX/73w;

    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v1, v1, LX/8Ob;->A:LX/74b;

    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v2, v2, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1340310
    invoke-virtual {v1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v4

    .line 1340311
    const-string v5, "upload_session_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340312
    sget-object v5, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_SUCCESS:LX/74R;

    const/4 v6, 0x0

    invoke-static {v0, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1340313
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1340314
    :catch_0
    move-exception v0

    .line 1340315
    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v1, v1, LX/8Ob;->y:LX/73w;

    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v2, v2, LX/8Ob;->A:LX/74b;

    iget-object v3, p0, Lcom/facebook/photos/upload/uploaders/VideoUploader$1;->a:LX/8Ob;

    iget-object v3, v3, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1340316
    invoke-virtual {v2}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v3

    .line 1340317
    const-string v6, "upload_session_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340318
    new-instance v6, LX/73z;

    const/4 v7, 0x1

    invoke-direct {v6, v0, v7}, LX/73z;-><init>(Ljava/lang/Exception;Z)V

    .line 1340319
    invoke-static {v3, v6}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1340320
    sget-object v6, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_FAILURE:LX/74R;

    const/4 v7, 0x0

    invoke-static {v1, v6, v3, v7}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1340321
    goto :goto_0
.end method
