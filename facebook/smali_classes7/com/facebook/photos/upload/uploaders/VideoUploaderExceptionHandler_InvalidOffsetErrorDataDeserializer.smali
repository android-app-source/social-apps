.class public Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1341022
    const-class v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;

    new-instance v1, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;

    invoke-direct {v1}, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1341023
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1341002
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 1341003
    const-class v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 1341004
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 1341005
    const-class v1, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;

    monitor-enter v1

    .line 1341006
    :try_start_0
    sget-object v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 1341007
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1341008
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1341009
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 1341010
    :goto_1
    return-object v0

    .line 1341011
    :cond_2
    sget-object v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 1341012
    if-eqz v0, :cond_0

    .line 1341013
    monitor-exit v1

    goto :goto_1

    .line 1341014
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1341015
    :sswitch_0
    :try_start_3
    const-string v2, "start_offset"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "end_offset"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 1341016
    :pswitch_0
    const-class v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;

    const-string v2, "startOffset"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1341017
    :goto_2
    :try_start_4
    sget-object v2, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341018
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1341019
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;

    const-string v2, "endOffset"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 1341020
    :catch_0
    move-exception v0

    .line 1341021
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x694b4f0 -> :sswitch_0
        0xefe3c77 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
