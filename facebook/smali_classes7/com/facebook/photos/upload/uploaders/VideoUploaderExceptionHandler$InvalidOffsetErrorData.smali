.class public final Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# instance fields
.field public endOffset:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_offset"
    .end annotation
.end field

.field public startOffset:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_offset"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1340951
    const-class v0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler_InvalidOffsetErrorDataDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1340948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340949
    iput v0, p0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->startOffset:I

    .line 1340950
    iput v0, p0, Lcom/facebook/photos/upload/uploaders/VideoUploaderExceptionHandler$InvalidOffsetErrorData;->endOffset:I

    return-void
.end method
