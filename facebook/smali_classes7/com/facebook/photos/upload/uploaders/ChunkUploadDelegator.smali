.class public Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/11H;

.field private final d:LX/8NR;

.field private final e:LX/0So;

.field private final f:LX/0b3;

.field private final g:LX/7zS;

.field private final h:LX/74B;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;"
        }
    .end annotation
.end field

.field private volatile j:Z

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/fbuploader/FbUploader$FbUploadJobHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/lang/Object;

.field private m:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1338240
    const-class v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/11H;LX/8NR;LX/0So;LX/0b3;LX/7zS;LX/74B;LX/0Or;LX/0ad;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/8NR;",
            "LX/0So;",
            "LX/0b3;",
            "LX/7zS;",
            "LX/74B;",
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1338400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338401
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->l:Ljava/lang/Object;

    .line 1338402
    iput-object p1, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->b:Ljava/util/concurrent/ExecutorService;

    .line 1338403
    iput-object p2, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->c:LX/11H;

    .line 1338404
    iput-object p3, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->d:LX/8NR;

    .line 1338405
    iput-object p4, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->e:LX/0So;

    .line 1338406
    iput-object p5, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->f:LX/0b3;

    .line 1338407
    iput-object p6, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->g:LX/7zS;

    .line 1338408
    iput-object p7, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->h:LX/74B;

    .line 1338409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    .line 1338410
    iput-object p8, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->i:LX/0Or;

    .line 1338411
    iput-object p9, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->m:LX/0ad;

    .line 1338412
    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;LX/8Oj;)LX/8NT;
    .locals 40

    .prologue
    .line 1338360
    const/16 v35, 0x0

    .line 1338361
    const-wide/16 v6, 0x0

    .line 1338362
    const/4 v14, 0x0

    .line 1338363
    const/4 v4, 0x0

    .line 1338364
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->b:LX/8Ob;

    iget-object v5, v5, LX/8Ob;->c:LX/14U;

    invoke-static {v5}, LX/14U;->a(LX/14U;)LX/14U;

    move-result-object v37

    .line 1338365
    const-string v13, ""

    move-object/from16 v36, v4

    move-wide/from16 v38, v6

    .line 1338366
    :goto_0
    :try_start_0
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v5, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    invoke-virtual {v4, v6, v7}, LX/8Ob;->a(J)Ljava/lang/String;

    move-result-object v13

    .line 1338367
    new-instance v15, LX/8Nz;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v4, v4, LX/8Ob;->h:LX/8Oo;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v13, v4}, LX/8Nz;-><init>(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;Ljava/lang/String;LX/8Oo;)V

    .line 1338368
    move-object/from16 v0, v37

    invoke-virtual {v0, v15}, LX/14U;->a(LX/4ck;)V

    .line 1338369
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v4, v4, LX/8Ob;->h:LX/8Oo;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->b:LX/8Ob;

    iget-object v5, v5, LX/8Ob;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    move-object/from16 v0, p1

    iget-object v8, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v8, v8, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object/from16 v0, p1

    iget-object v10, v0, LX/8O1;->b:LX/8Ob;

    iget-wide v10, v10, LX/8Ob;->l:J

    move-object/from16 v0, p1

    iget-object v12, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v12}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v12

    invoke-virtual/range {v4 .. v13}, LX/8Oo;->a(Ljava/lang/String;JJJILjava/lang/String;)V

    .line 1338370
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->e:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v8

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object v6, v15

    move-object/from16 v7, v37

    .line 1338371
    invoke-static/range {v4 .. v9}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;LX/8Nz;LX/14U;J)Ljava/lang/String;

    move-result-object v34

    .line 1338372
    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, LX/14U;->a(LX/4ck;)V

    .line 1338373
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->c:LX/11H;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->d:LX/8NR;

    new-instance v15, LX/8NS;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->b:LX/8Ob;

    iget-object v6, v6, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v0, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v0, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v6}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v22

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v6}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->b:LX/8Ob;

    iget-object v0, v6, LX/8Ob;->j:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->b:LX/8Ob;

    iget-object v0, v6, LX/8Ob;->k:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p2

    iget v0, v0, LX/8Oj;->a:F

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->b:LX/8Ob;

    iget-object v6, v6, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_1

    const/16 v28, 0x1

    :goto_1
    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v0, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionStartOffset:J

    move-wide/from16 v29, v0

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v0, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    move-wide/from16 v31, v0

    const/16 v33, 0x0

    invoke-direct/range {v15 .. v34}, LX/8NS;-><init>(JJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;FZJJLX/8Oa;Ljava/lang/String;)V

    sget-object v6, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, v37

    invoke-virtual {v4, v5, v15, v0, v6}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8NT;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1338374
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->e:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v22

    .line 1338375
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v0, v5, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-wide/from16 v18, v0

    move-object/from16 v15, p2

    move-wide/from16 v16, v38

    move-wide/from16 v20, v8

    invoke-virtual/range {v15 .. v23}, LX/8Oj;->a(JJJJ)V

    .line 1338376
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->f:LX/0b3;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v8, v7, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    move-object/from16 v0, p1

    iget-object v7, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v10, v7, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionStartOffset:J

    sub-long/2addr v8, v10

    move-object/from16 v0, p2

    iget v7, v0, LX/8Oj;->a:F

    invoke-static {v5, v6, v8, v9, v7}, LX/8Op;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/0b3;JF)V

    .line 1338377
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->f:LX/8OL;

    const-string v6, "after chunk video"

    invoke-virtual {v5, v6}, LX/8OL;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1338378
    const/4 v6, 0x1

    .line 1338379
    :try_start_2
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->b:LX/8Ob;

    iget-object v5, v5, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v5, v13}, LX/8Oo;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move v14, v6

    move-object/from16 v35, v4

    move-object/from16 v4, v36

    .line 1338380
    :goto_2
    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->j:Z

    if-nez v5, :cond_0

    const-wide/16 v6, 0x1

    add-long v6, v6, v38

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->g:LX/8On;

    invoke-virtual {v5}, LX/8On;->c()I

    move-result v5

    int-to-long v8, v5

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 1338381
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->j:Z

    if-eqz v5, :cond_2

    .line 1338382
    new-instance v4, Ljava/lang/InterruptedException;

    const-string v5, "upload has failed in another partition"

    invoke-direct {v4, v5}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1338383
    :cond_1
    const/16 v28, 0x0

    goto/16 :goto_1

    .line 1338384
    :catch_0
    move-exception v4

    move-object v5, v4

    move v6, v14

    move-object/from16 v7, v35

    .line 1338385
    :goto_3
    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, LX/8Oj;->a(J)V

    .line 1338386
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v13}, LX/8Oj;->a(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;Ljava/lang/String;)LX/8O2;

    move-result-object v4

    .line 1338387
    move-object/from16 v0, p1

    move-wide/from16 v1, v38

    invoke-static {v0, v5, v1, v2, v4}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(LX/8O1;Ljava/lang/Exception;JLX/8O2;)Landroid/util/Pair;

    move-result-object v8

    .line 1338388
    if-eqz v8, :cond_6

    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v10, v4

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v14, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    cmp-long v4, v10, v14

    if-nez v4, :cond_6

    .line 1338389
    new-instance v7, LX/8NT;

    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v10, v4

    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v8, v4

    invoke-direct {v7, v10, v11, v8, v9}, LX/8NT;-><init>(JJ)V

    .line 1338390
    const/4 v6, 0x1

    .line 1338391
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v4, v4, LX/8Ob;->h:LX/8Oo;

    invoke-virtual {v4, v13}, LX/8Oo;->a(Ljava/lang/String;)V

    move-object v4, v5

    move v14, v6

    move-object/from16 v35, v7

    goto/16 :goto_2

    .line 1338392
    :cond_2
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->f:LX/8OL;

    const-string v6, "after chunk video retries"

    invoke-virtual {v5, v6}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1338393
    if-nez v14, :cond_3

    if-eqz v4, :cond_3

    .line 1338394
    throw v4

    .line 1338395
    :cond_3
    if-nez v35, :cond_4

    .line 1338396
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 1338397
    :cond_4
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->g:LX/8On;

    invoke-virtual {v4}, LX/8On;->b()V

    .line 1338398
    return-object v35

    .line 1338399
    :catch_1
    move-exception v5

    move v6, v14

    move-object v7, v4

    goto :goto_3

    :catch_2
    move-exception v5

    move-object v7, v4

    goto :goto_3

    :cond_5
    move-object/from16 v36, v4

    move-wide/from16 v38, v6

    goto/16 :goto_0

    :cond_6
    move-object v4, v5

    move v14, v6

    move-object/from16 v35, v7

    goto/16 :goto_2
.end method

.method private static a(LX/8O1;Ljava/lang/Exception;JLX/8O2;)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8O1;",
            "Ljava/lang/Exception;",
            "J",
            "LX/8O2;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1338355
    iget-object v1, p0, LX/8O1;->g:LX/8On;

    iget-object v0, p0, LX/8O1;->b:LX/8Ob;

    iget-object v3, v0, LX/8Ob;->x:LX/8Oi;

    move-object v2, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, LX/8On;->a(Ljava/lang/Exception;LX/8Oi;JLX/8O2;)Landroid/util/Pair;

    move-result-object v1

    .line 1338356
    if-eqz v1, :cond_0

    .line 1338357
    iget-object v2, p0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    .line 1338358
    iget-object v2, p0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    .line 1338359
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;
    .locals 11

    .prologue
    .line 1338352
    new-instance v1, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    invoke-static {p0}, LX/8NR;->a(LX/0QB;)LX/8NR;

    move-result-object v4

    check-cast v4, LX/8NR;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v6

    check-cast v6, LX/0b3;

    invoke-static {p0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v7

    check-cast v7, LX/7zS;

    invoke-static {p0}, LX/74B;->b(LX/0QB;)LX/74B;

    move-result-object v8

    check-cast v8, LX/74B;

    const/16 v9, 0x2f01

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v1 .. v10}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;-><init>(Ljava/util/concurrent/ExecutorService;LX/11H;LX/8NR;LX/0So;LX/0b3;LX/7zS;LX/74B;LX/0Or;LX/0ad;)V

    .line 1338353
    move-object v0, v1

    .line 1338354
    return-object v0
.end method

.method private static a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;LX/8Nz;LX/14U;J)Ljava/lang/String;
    .locals 18

    .prologue
    .line 1338321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->g:LX/7zS;

    invoke-virtual {v2}, LX/7zS;->a()LX/7z2;

    move-result-object v9

    .line 1338322
    new-instance v2, LX/7yy;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v4, v4, LX/8Ob;->j:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object/from16 v0, p1

    iget-object v8, v0, LX/8O1;->b:LX/8Ob;

    iget-object v8, v8, LX/8Ob;->k:Ljava/lang/String;

    invoke-direct/range {v2 .. v8}, LX/7yy;-><init>(Ljava/io/File;JJLjava/lang/String;)V

    .line 1338323
    new-instance v3, LX/7yu;

    const/4 v4, 0x2

    const/16 v5, 0x64

    const/16 v6, 0x7530

    invoke-direct {v3, v4, v5, v6}, LX/7yu;-><init>(III)V

    .line 1338324
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1338325
    const-string v5, "X_FB_VIDEO_WATERFALL_ID"

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v6}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338326
    new-instance v5, LX/7ys;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->m:LX/0ad;

    sget-short v7, LX/8Jz;->X:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->m:LX/0ad;

    sget v8, LX/8Jz;->V:I

    const/16 v10, 0x400

    invoke-interface {v7, v8, v10}, LX/0ad;->a(II)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->m:LX/0ad;

    sget-char v10, LX/8Jz;->W:C

    const-string v11, "SHA256"

    invoke-interface {v8, v10, v11}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, LX/7ys;-><init>(ZILjava/lang/String;)V

    .line 1338327
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->m:LX/0ad;

    sget v7, LX/8Jz;->U:I

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 1338328
    move-object/from16 v0, p1

    iget-object v7, v0, LX/8O1;->b:LX/8Ob;

    invoke-virtual {v7}, LX/8Ob;->f()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    if-lt v7, v6, :cond_0

    .line 1338329
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/7ys;->a(Z)V

    .line 1338330
    :cond_0
    new-instance v15, LX/7yw;

    sget-object v6, LX/7yt;->FACEBOOK:LX/7yt;

    invoke-direct {v15, v6, v4, v3, v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;)V

    .line 1338331
    new-instance v3, LX/8O0;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v4, v5}, LX/8O0;-><init>(LX/8Nz;J)V

    .line 1338332
    :try_start_0
    invoke-virtual {v9, v2, v15, v3}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;

    move-result-object v3

    .line 1338333
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_0
    .catch LX/7zB; {:try_start_0 .. :try_end_0} :catch_0

    .line 1338334
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1338335
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338336
    :try_start_2
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->f:LX/8OL;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, LX/8OL;->a(Ljava/util/ArrayList;)V

    .line 1338337
    invoke-virtual {v9, v3}, LX/7z2;->c(LX/7z0;)LX/7zL;

    move-result-object v4

    .line 1338338
    iget-object v5, v4, LX/7zL;->c:LX/7zK;

    sget-object v6, LX/7zK;->NOT_FOUND:LX/7zK;

    if-ne v5, v6, :cond_1

    .line 1338339
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->b:LX/8Ob;

    invoke-virtual {v5}, LX/8Ob;->e()V

    .line 1338340
    :cond_1
    iget-object v7, v4, LX/7zL;->a:Ljava/lang/String;

    .line 1338341
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_2
    .catch LX/7zB; {:try_start_2 .. :try_end_2} :catch_0

    .line 1338342
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1338343
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1338344
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->h:LX/74B;

    invoke-virtual {v15}, LX/7yw;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, LX/7yy;->e()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v8

    sub-long v8, v8, p4

    invoke-virtual {v2}, LX/7yy;->c()J

    move-result-wide v10

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8O1;->b:LX/8Ob;

    iget-wide v12, v3, LX/8Ob;->l:J

    move-object/from16 v0, p1

    iget-object v3, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v4 .. v14}, LX/74B;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 1338345
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LX/14U;->a(LX/4ck;)V
    :try_end_4
    .catch LX/7zB; {:try_start_4 .. :try_end_4} :catch_0

    .line 1338346
    return-object v7

    .line 1338347
    :catchall_0
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v3
    :try_end_6
    .catch LX/7zB; {:try_start_6 .. :try_end_6} :catch_0

    .line 1338348
    :catch_0
    move-exception v3

    move-object/from16 v17, v3

    .line 1338349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->h:LX/74B;

    invoke-virtual {v15}, LX/7yw;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/7yy;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, LX/7zB;->getMessage()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    iget-boolean v7, v0, LX/7zB;->mIsCancellation:Z

    move-object/from16 v0, v17

    iget-wide v8, v0, LX/7zB;->mBytesTransferred:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->e:LX/0So;

    invoke-interface {v10}, LX/0So;->now()J

    move-result-wide v10

    sub-long v10, v10, p4

    invoke-virtual {v2}, LX/7yy;->c()J

    move-result-wide v12

    move-object/from16 v0, p1

    iget-object v2, v0, LX/8O1;->b:LX/8Ob;

    iget-wide v14, v2, LX/8Ob;->l:J

    move-object/from16 v0, p1

    iget-object v2, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v3 .. v16}, LX/74B;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJJJJLjava/lang/String;)V

    .line 1338350
    throw v17

    .line 1338351
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v3
.end method

.method private static a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8O1;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1338320
    new-instance v0, LX/8Ny;

    invoke-direct {v0, p0, p1}, LX/8Ny;-><init>(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;)V

    return-object v0
.end method

.method private static a(LX/8Ob;)V
    .locals 14

    .prologue
    const/4 v10, 0x1

    .line 1338305
    iget-object v0, p0, LX/8Ob;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1338306
    :cond_0
    const/4 v0, 0x2

    iget-wide v2, p0, LX/8Ob;->l:J

    const-wide/32 v4, 0xa00000

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1338307
    iget-wide v2, p0, LX/8Ob;->l:J

    long-to-double v2, v2

    int-to-double v0, v0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-long v12, v0

    .line 1338308
    iget-wide v2, p0, LX/8Ob;->u:J

    .line 1338309
    :goto_0
    iget-wide v0, p0, LX/8Ob;->l:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 1338310
    add-long v0, v2, v12

    iget-wide v4, p0, LX/8Ob;->l:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 1338311
    new-instance v1, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v8, p0, LX/8Ob;->v:J

    move-wide v6, v2

    invoke-direct/range {v1 .. v9}, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;-><init>(JJJJ)V

    .line 1338312
    iget-object v0, p0, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v2, v4

    .line 1338313
    goto :goto_0

    .line 1338314
    :cond_1
    iget-object v0, p0, LX/8Ob;->g:Ljava/util/Map;

    iget-object v1, p0, LX/8Ob;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    .line 1338315
    if-eqz v0, :cond_3

    move v1, v10

    :goto_1
    const-string v2, "no upload record for this upload"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1338316
    if-eqz v0, :cond_2

    .line 1338317
    iget-object v1, p0, LX/8Ob;->B:Ljava/util/List;

    iput-object v1, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    .line 1338318
    :cond_2
    return-void

    .line 1338319
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(LX/8Ob;ILcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0cW;)V
    .locals 4

    .prologue
    .line 1338295
    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 1338296
    :try_start_0
    iget-object v0, p1, LX/8Ob;->g:Ljava/util/Map;

    iget-object v2, p1, LX/8Ob;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    .line 1338297
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p2, :cond_0

    .line 1338298
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1338299
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    invoke-interface {v2, p2, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1338300
    iget-object v2, p1, LX/8Ob;->g:Ljava/util/Map;

    iget-object v3, p1, LX/8Ob;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338301
    iget-object v2, p1, LX/8Ob;->d:Ljava/lang/String;

    invoke-virtual {p4, v2, v0}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v0

    .line 1338302
    if-nez v0, :cond_0

    .line 1338303
    iget-object v0, p1, LX/8Ob;->y:LX/73w;

    iget-object v2, p1, LX/8Ob;->A:LX/74b;

    invoke-virtual {v0, v2}, LX/73w;->d(LX/74b;)V

    .line 1338304
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;)V
    .locals 21

    .prologue
    .line 1338275
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    .line 1338276
    const-wide/16 v4, 0x0

    .line 1338277
    new-instance v20, LX/8Oj;

    invoke-direct/range {v20 .. v20}, LX/8Oj;-><init>()V

    move-wide/from16 v16, v4

    move-wide/from16 v18, v6

    .line 1338278
    :goto_0
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    move-object/from16 v0, p1

    iget-object v6, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v6, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 1338279
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;LX/8Oj;)LX/8NT;

    move-result-object v4

    .line 1338280
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    const-string v6, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, LX/8Oj;->a(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;Ljava/lang/String;)LX/8O2;

    move-result-object v15

    .line 1338281
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    invoke-virtual {v4}, LX/8NT;->a()J

    move-result-wide v6

    iput-wide v6, v5, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    .line 1338282
    move-object/from16 v0, p1

    iget-object v5, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    invoke-virtual {v4}, LX/8NT;->b()J

    move-result-wide v6

    invoke-virtual {v4}, LX/8NT;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v5, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    .line 1338283
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    iput-wide v4, v15, LX/8O2;->f:J

    .line 1338284
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    iput-wide v4, v15, LX/8O2;->g:J

    .line 1338285
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v5, v4, LX/8Ob;->y:LX/73w;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v6, v4, LX/8Ob;->A:LX/74b;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->b:LX/8Ob;

    iget-object v7, v4, LX/8Ob;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v8, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v10, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v12, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v14

    invoke-virtual/range {v5 .. v15}, LX/73w;->a(LX/74b;Ljava/lang/String;JJJILX/8O2;)V

    .line 1338286
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v4, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    cmp-long v4, v4, v18

    if-nez v4, :cond_0

    .line 1338287
    const-wide/16 v4, 0x1

    add-long v4, v4, v16

    .line 1338288
    const-wide/16 v6, 0x2

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    .line 1338289
    new-instance v4, LX/740;

    const-string v5, "Transfer chunk failure"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, LX/740;-><init>(Ljava/lang/String;Z)V

    throw v4

    .line 1338290
    :cond_0
    move-object/from16 v0, p1

    iget-object v4, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    iget-wide v6, v4, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    .line 1338291
    const-wide/16 v4, 0x0

    .line 1338292
    move-object/from16 v0, p1

    iget-object v8, v0, LX/8O1;->b:LX/8Ob;

    move-object/from16 v0, p1

    iget v9, v0, LX/8O1;->c:I

    move-object/from16 v0, p1

    iget-object v10, v0, LX/8O1;->d:Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/8O1;->e:LX/0cW;

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v10, v11}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(LX/8Ob;ILcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0cW;)V

    :goto_1
    move-wide/from16 v16, v4

    move-wide/from16 v18, v6

    .line 1338293
    goto/16 :goto_0

    .line 1338294
    :cond_1
    return-void

    :cond_2
    move-wide/from16 v6, v18

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;LX/0cW;LX/8OL;Ljava/util/concurrent/Semaphore;LX/8On;)V
    .locals 13

    .prologue
    .line 1338241
    invoke-static {p2}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(LX/8Ob;)V

    .line 1338242
    const/4 v2, 0x0

    iput-object v2, p2, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1338243
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->j:Z

    .line 1338244
    iget-object v2, p2, LX/8Ob;->U:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->k:Ljava/util/ArrayList;

    .line 1338245
    new-instance v11, Ljava/util/concurrent/ExecutorCompletionService;

    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->b:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v11, v2}, Ljava/util/concurrent/ExecutorCompletionService;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1338246
    const/4 v3, 0x0

    .line 1338247
    const/4 v2, 0x0

    .line 1338248
    invoke-virtual/range {p6 .. p6}, LX/8On;->a()LX/8Ne;

    move-result-object v4

    invoke-interface {v4}, LX/8Ne;->c()Z

    move-result v12

    .line 1338249
    const/4 v5, 0x0

    move v10, v3

    move v3, v2

    :goto_0
    iget-object v2, p2, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_1

    .line 1338250
    iget-object v2, p2, LX/8Ob;->B:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    .line 1338251
    iget-wide v6, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    iget-wide v8, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionStartOffset:J

    sub-long/2addr v6, v8

    .line 1338252
    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-ltz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    const-string v8, "partitionInfo improperly configured"

    invoke-static {v4, v8}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1338253
    int-to-long v8, v3

    add-long/2addr v6, v8

    long-to-int v9, v6

    .line 1338254
    iget-wide v6, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    iget-wide v2, v2, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    cmp-long v2, v6, v2

    if-gez v2, :cond_5

    .line 1338255
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Ne;

    .line 1338256
    move-object/from16 v0, p5

    invoke-interface {v2, v0}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1338257
    invoke-interface {v2, v12}, LX/8Ne;->a(Z)V

    .line 1338258
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, LX/8On;->a(LX/8Ne;)LX/8On;

    move-result-object v8

    .line 1338259
    new-instance v2, LX/8O1;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v8}, LX/8O1;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ob;ILX/0cW;LX/8OL;LX/8On;)V

    invoke-static {p0, v2}, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->a(Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;LX/8O1;)Ljava/util/concurrent/Callable;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/concurrent/CompletionService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 1338260
    add-int/lit8 v2, v10, 0x1

    .line 1338261
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move v3, v9

    move v10, v2

    goto :goto_0

    .line 1338262
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 1338263
    :cond_1
    iget-object v2, p2, LX/8Ob;->h:LX/8Oo;

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, LX/8Oo;->b(J)V

    .line 1338264
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v10, :cond_3

    .line 1338265
    invoke-interface {v11}, Ljava/util/concurrent/CompletionService;->take()Ljava/util/concurrent/Future;

    move-result-object v3

    .line 1338266
    const v4, -0x1950f5f

    :try_start_0
    invoke-static {v3, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1338267
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1338268
    :catch_0
    move-exception v3

    .line 1338269
    iget-object v4, p2, LX/8Ob;->w:Ljava/lang/Exception;

    if-nez v4, :cond_2

    .line 1338270
    iput-object v3, p2, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1338271
    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/facebook/photos/upload/uploaders/ChunkUploadDelegator;->j:Z

    goto :goto_4

    .line 1338272
    :cond_3
    iget-object v2, p2, LX/8Ob;->w:Ljava/lang/Exception;

    if-eqz v2, :cond_4

    .line 1338273
    iget-object v2, p2, LX/8Ob;->w:Ljava/lang/Exception;

    throw v2

    .line 1338274
    :cond_4
    return-void

    :cond_5
    move v2, v10

    goto :goto_2
.end method
