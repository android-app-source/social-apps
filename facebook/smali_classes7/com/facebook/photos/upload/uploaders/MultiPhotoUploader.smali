.class public Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8OM;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0b3;

.field private final c:LX/0Sh;

.field public final d:LX/03V;

.field private final e:LX/8LX;

.field public final f:LX/0SG;

.field private final g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

.field public final h:LX/0cW;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/8OL;

.field private final k:LX/8OJ;

.field public final l:LX/1EZ;

.field private final m:LX/0ad;

.field private final n:LX/0cX;

.field private o:Ljava/util/concurrent/Semaphore;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1339260
    const-class v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    sput-object v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/03V;LX/8LX;LX/0b3;LX/0SG;Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/0cW;LX/0Or;LX/8OL;LX/8OJ;LX/1EZ;LX/0ad;LX/0cX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8LX;",
            "LX/0b3;",
            "LX/0SG;",
            "Lcom/facebook/photos/upload/protocol/PhotoPublisher;",
            "LX/0cW;",
            "LX/0Or",
            "<",
            "LX/8Ne;",
            ">;",
            "LX/8OL;",
            "LX/8OJ;",
            "LX/1EZ;",
            "LX/0ad;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339073
    iput-object p1, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->c:LX/0Sh;

    .line 1339074
    iput-object p2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->d:LX/03V;

    .line 1339075
    iput-object p3, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->e:LX/8LX;

    .line 1339076
    iput-object p4, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b:LX/0b3;

    .line 1339077
    iput-object p5, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->f:LX/0SG;

    .line 1339078
    iput-object p6, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    .line 1339079
    iput-object p7, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    .line 1339080
    iput-object p8, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->i:LX/0Or;

    .line 1339081
    iput-object p9, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    .line 1339082
    iput-object p10, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->k:LX/8OJ;

    .line 1339083
    iput-object p11, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->l:LX/1EZ;

    .line 1339084
    iput-object p12, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->m:LX/0ad;

    .line 1339085
    iput-object p13, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->n:LX/0cX;

    .line 1339086
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1339256
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1339257
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1339258
    invoke-virtual {v0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1339259
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Exception;)LX/73z;
    .locals 4

    .prologue
    .line 1339253
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 1339254
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->d:LX/03V;

    sget-object v1, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrapping "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1339255
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->n:LX/0cX;

    invoke-static {v0, p1}, LX/8N0;->a(LX/0cX;Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/lang/String;I)LX/8Mw;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 1339241
    sget-object v0, LX/8OP;->a:[I

    .line 1339242
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v1, v1

    .line 1339243
    invoke-virtual {v1}, LX/8LR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1339244
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported reorder method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1339245
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    move-object v2, v2

    .line 1339246
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1339247
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v5, p4

    move-object v6, p3

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;

    move-result-object v0

    .line 1339248
    :goto_0
    return-object v0

    .line 1339249
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 1339250
    sget-object v1, LX/8LS;->ALBUM:LX/8LS;

    if-eq v0, v1, :cond_0

    move v4, v3

    .line 1339251
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object v1, p1

    move-object v2, p2

    move v3, v5

    move-object v5, p4

    move-object v6, p3

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v4, v5

    .line 1339252
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/0Px;Ljava/util/List;Ljava/util/List;I)LX/8Mw;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1339226
    sget-object v2, LX/8OP;->a:[I

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v3

    invoke-virtual {v3}, LX/8LR;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1339227
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported publish method "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1339228
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move/from16 v0, p7

    invoke-virtual {v2, p1, p2, p3, v0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;

    move-result-object v2

    .line 1339229
    :goto_0
    return-object v2

    .line 1339230
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v2 .. v8}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/util/List;Ljava/util/List;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339231
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->S()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v11

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v10, p7

    invoke-virtual/range {v2 .. v11}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILX/8LR;)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339232
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v2 .. v8}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339233
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move/from16 v0, p7

    invoke-virtual {v2, p1, p2, p3, v0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339234
    :pswitch_5
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move/from16 v0, p7

    invoke-virtual {v2, p1, p2, p3, v0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339235
    :pswitch_6
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v2 .. v7}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Ljava/util/List;Ljava/util/List;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339236
    :pswitch_7
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object/from16 v0, p4

    move/from16 v1, p7

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339237
    :pswitch_8
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object/from16 v0, p4

    move/from16 v1, p7

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;I)LX/8Mw;

    move-result-object v2

    goto :goto_0

    .line 1339238
    :pswitch_9
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object/from16 v0, p4

    invoke-virtual {v2, p1, v0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;)LX/8Mw;

    move-result-object v2

    goto/16 :goto_0

    .line 1339239
    :pswitch_a
    new-instance v2, LX/8Mw;

    invoke-virtual {p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    goto/16 :goto_0

    .line 1339240
    :pswitch_b
    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->g:Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-object/from16 v0, p4

    invoke-virtual {v2, p1, v0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;)LX/8Mw;

    move-result-object v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private static a(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ne;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/0Px;Ljava/util/List;Ljava/util/List;)LX/8Mw;
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/8Ne;",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1339201
    const/4 v7, 0x0

    .line 1339202
    const/4 v2, 0x0

    .line 1339203
    const/4 v1, 0x0

    .line 1339204
    const/4 v0, 0x0

    move-object v8, v0

    move v9, v1

    move-object v10, v2

    .line 1339205
    :goto_0
    const/4 v11, 0x0

    .line 1339206
    if-nez v9, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    .line 1339207
    :try_start_0
    invoke-direct/range {v0 .. v7}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/0Px;Ljava/util/List;Ljava/util/List;I)LX/8Mw;

    move-result-object v0

    .line 1339208
    :goto_1
    return-object v0

    :cond_0
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move-object v6, v8

    invoke-direct/range {v2 .. v7}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/lang/String;I)LX/8Mw;
    :try_end_0
    .catch LX/8N0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 1339209
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 1339210
    invoke-virtual {v3}, LX/8N0;->h()LX/73z;

    move-result-object v0

    invoke-virtual {v0}, LX/73z;->a()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, LX/4cm;

    if-eqz v0, :cond_7

    .line 1339211
    invoke-virtual {v3}, LX/8N0;->h()LX/73z;

    move-result-object v0

    invoke-virtual {v0}, LX/73z;->d()Ljava/lang/String;

    move-result-object v2

    .line 1339212
    invoke-static {v2, v10}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v4, v0

    .line 1339213
    :goto_3
    if-nez v4, :cond_1

    .line 1339214
    invoke-virtual {v3}, LX/8N0;->h()LX/73z;

    move-result-object v0

    invoke-interface {p3, v0}, LX/8Ne;->a(LX/73z;)V

    .line 1339215
    :cond_1
    invoke-virtual {v3}, LX/8N0;->h()LX/73z;

    move-result-object v0

    invoke-virtual {v0}, LX/73z;->a()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, LX/8N2;

    if-eqz v0, :cond_6

    .line 1339216
    invoke-virtual {v3}, LX/8N0;->h()LX/73z;

    move-result-object v0

    invoke-virtual {v0}, LX/73z;->a()Ljava/lang/Exception;

    move-result-object v0

    check-cast v0, LX/8N2;

    .line 1339217
    invoke-virtual {v0}, LX/8N2;->a()Ljava/lang/String;

    move-result-object v0

    .line 1339218
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1339219
    if-nez v9, :cond_2

    .line 1339220
    const/4 v7, 0x0

    .line 1339221
    :cond_2
    const/4 v1, 0x1

    .line 1339222
    :goto_4
    iget-object v5, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    invoke-virtual {v5}, LX/8OL;->e()Z

    move-result v5

    if-nez v5, :cond_3

    if-nez v4, :cond_5

    add-int/lit8 v7, v7, 0x1

    invoke-interface {p3}, LX/8Ne;->b()I

    move-result v4

    if-le v7, v4, :cond_5

    .line 1339223
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Publish cancelled at attempt #"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8OL;->a(Ljava/lang/String;)V

    .line 1339224
    throw v3

    .line 1339225
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move-object v8, v0

    move v9, v1

    move-object v10, v2

    goto/16 :goto_0

    :cond_6
    move-object v0, v8

    move v1, v9

    goto :goto_4

    :cond_7
    move v4, v11

    move-object v2, v10

    goto :goto_3

    :cond_8
    move v1, v9

    goto :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;
    .locals 1

    .prologue
    .line 1339200
    invoke-static {p0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b(LX/0QB;)Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Map;Lcom/facebook/photos/upload/operation/UploadRecords;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;",
            "Lcom/facebook/photos/upload/operation/UploadRecords;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1339194
    if-nez p2, :cond_1

    .line 1339195
    :cond_0
    return-void

    .line 1339196
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1339197
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1339198
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-static {p0, v2}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1339199
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadRecord;)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1339193
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/facebook/photos/upload/operation/UploadRecord;->uploadTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x44aa200

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;Ljava/util/Map;)Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 1339189
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1339190
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1339191
    invoke-virtual {v0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 1339192
    :cond_0
    return-object v1
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;
    .locals 14

    .prologue
    .line 1339187
    new-instance v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/8LX;->b(LX/0QB;)LX/8LX;

    move-result-object v3

    check-cast v3, LX/8LX;

    invoke-static {p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v4

    check-cast v4, LX/0b3;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b(LX/0QB;)Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    invoke-static {p0}, LX/0cW;->a(LX/0QB;)LX/0cW;

    move-result-object v7

    check-cast v7, LX/0cW;

    const/16 v8, 0x2f01

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/8OL;->b(LX/0QB;)LX/8OL;

    move-result-object v9

    check-cast v9, LX/8OL;

    invoke-static {p0}, LX/8OJ;->b(LX/0QB;)LX/8OJ;

    move-result-object v10

    check-cast v10, LX/8OJ;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v11

    check-cast v11, LX/1EZ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v13

    check-cast v13, LX/0cX;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;-><init>(LX/0Sh;LX/03V;LX/8LX;LX/0b3;LX/0SG;Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/0cW;LX/0Or;LX/8OL;LX/8OJ;LX/1EZ;LX/0ad;LX/0cX;)V

    .line 1339188
    return-object v0
.end method

.method private static b(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1339185
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v2, v2

    .line 1339186
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v0, :cond_0

    iget-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->m:LX/0ad;

    sget-short v3, LX/8Jz;->k:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private c(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 19

    .prologue
    .line 1339117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->c:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1339118
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339119
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339120
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->v()Lcom/facebook/photos/upload/operation/UploadRecords;

    move-result-object v13

    .line 1339121
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v18

    .line 1339122
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v17

    .line 1339123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->e:LX/8LX;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/8LX;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/util/List;

    move-result-object v16

    .line 1339124
    invoke-static/range {v16 .. v16}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->e:LX/8LX;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v4, v5}, LX/8LX;->a(Lcom/facebook/photos/upload/operation/UploadOperation;J)LX/73w;

    move-result-object v6

    .line 1339126
    const-string v2, "2.0"

    invoke-virtual {v6, v2}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v7

    .line 1339127
    new-instance v5, LX/8OR;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, LX/8OR;-><init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339128
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 1339129
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1339130
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1339131
    invoke-virtual {v2}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v10

    .line 1339132
    if-eqz v13, :cond_1

    invoke-virtual {v13, v10}, Lcom/facebook/photos/upload/operation/UploadRecords;->a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;

    move-result-object v4

    .line 1339133
    :goto_1
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1339134
    move-object/from16 v0, v18

    invoke-interface {v0, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339135
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339136
    iget-boolean v11, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->isRawUpload:Z

    if-eqz v11, :cond_0

    .line 1339137
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b(Z)V

    .line 1339138
    iget-wide v14, v4, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-virtual {v2, v14, v15}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(J)V

    .line 1339139
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339140
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    invoke-virtual {v2, v10, v4}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    goto :goto_0

    .line 1339141
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1339142
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1339143
    :cond_3
    new-instance v4, LX/8OO;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v5, v8, v1}, LX/8OO;-><init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8OR;Ljava/util/List;Ljava/util/Map;)V

    .line 1339144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/8Ne;

    .line 1339145
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v2

    invoke-interface {v9, v2}, LX/8Ne;->a(Z)V

    .line 1339146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    invoke-interface {v9, v2}, LX/8Ne;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1339147
    :try_start_0
    const-class v8, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->J()LX/8LR;

    move-result-object v2

    invoke-virtual {v2}, LX/8LR;->name()Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v2

    invoke-virtual {v2}, LX/8LS;->name()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aA()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v5, v2, v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    .line 1339148
    invoke-static/range {p0 .. p1}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->d(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339149
    invoke-static/range {p0 .. p1}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1339150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->k:LX/8OJ;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    move-object/from16 v8, p1

    invoke-virtual/range {v2 .. v11}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Semaphore;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1339151
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    const-string v3, "before publish"

    invoke-virtual {v2, v3}, LX/8OL;->b(Ljava/lang/String;)V

    .line 1339152
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-wide v2, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    .line 1339153
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1339154
    invoke-virtual {v14, v2, v3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a(J)V

    .line 1339155
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Ljava/util/List;Ljava/util/Map;)LX/0Px;

    move-result-object v15

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object v12, v6

    move-object v13, v9

    invoke-static/range {v10 .. v17}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/8Ne;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;LX/0Px;Ljava/util/List;Ljava/util/List;)LX/8Mw;
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    .line 1339156
    move-object/from16 v0, p1

    invoke-virtual {v6, v7, v0}, LX/73w;->b(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339157
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b(Ljava/util/List;Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v3

    .line 1339158
    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 1339159
    const-string v5, "graphql_story"

    iget-object v6, v2, LX/8Mw;->b:LX/0am;

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1339160
    new-instance v5, Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 1339161
    const-string v6, "shot_fbid"

    iget-object v7, v2, LX/8Mw;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339162
    iget-object v2, v2, LX/8Mw;->a:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Landroid/util/Pair;

    const/4 v7, 0x0

    const-string v8, "fbids"

    invoke-static {v8, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    const-string v7, "graphql_story"

    invoke-static {v7, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    aput-object v4, v6, v3

    const/4 v3, 0x2

    const-string v4, "shot_fbid"

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v2, v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;[Landroid/util/Pair;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    return-object v2

    .line 1339163
    :cond_4
    const/4 v2, 0x0

    move-object v5, v2

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1339164
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->k:LX/8OJ;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    move-object/from16 v8, p1

    invoke-virtual/range {v2 .. v10}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    .line 1339165
    :catch_0
    move-exception v2

    .line 1339166
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->t()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1339167
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->u()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-result-object v10

    const-string v11, "multiphotoupload exception"

    move-object/from16 v8, p1

    invoke-virtual/range {v6 .. v11}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;ILX/73y;Ljava/lang/String;)V

    .line 1339168
    :cond_7
    throw v2

    .line 1339169
    :catch_1
    move-exception v2

    .line 1339170
    instance-of v3, v2, LX/8N0;

    if-eqz v3, :cond_8

    .line 1339171
    check-cast v2, LX/8N0;

    move-object v12, v2

    .line 1339172
    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v9

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v3

    sub-int v10, v2, v3

    const/4 v11, 0x1

    move-object/from16 v8, p1

    invoke-virtual/range {v6 .. v12}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IIZLX/73y;)V

    .line 1339173
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v13}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Ljava/util/Map;Lcom/facebook/photos/upload/operation/UploadRecords;)V

    .line 1339174
    new-instance v2, LX/8OT;

    invoke-virtual {v12}, LX/8N0;->h()LX/73z;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v2, v3, v0}, LX/8OT;-><init>(LX/73z;Ljava/util/Map;)V

    throw v2

    .line 1339175
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v2

    .line 1339176
    new-instance v12, LX/8OQ;

    invoke-direct {v12, v2}, LX/8OQ;-><init>(LX/73z;)V

    goto :goto_5

    .line 1339177
    :catch_2
    move-exception v2

    .line 1339178
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->t()Z

    move-result v3

    if-nez v3, :cond_9

    .line 1339179
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->u()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-result-object v10

    const-string v11, "multi upload exception"

    move-object/from16 v8, p1

    invoke-virtual/range {v6 .. v11}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;ILX/73y;Ljava/lang/String;)V

    .line 1339180
    :cond_9
    throw v2

    .line 1339181
    :catch_3
    move-exception v2

    .line 1339182
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v12

    .line 1339183
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v8, p1

    invoke-virtual/range {v6 .. v12}, LX/73w;->a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IIZLX/73y;)V

    .line 1339184
    new-instance v2, LX/8OT;

    move-object/from16 v0, v18

    invoke-direct {v2, v12, v0}, LX/8OT;-><init>(LX/73z;Ljava/util/Map;)V

    throw v2
.end method

.method private static d(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 7

    .prologue
    .line 1339104
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v0

    .line 1339105
    invoke-static {p1}, LX/8LX;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/434;

    move-result-object v3

    .line 1339106
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v4, v0

    .line 1339107
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1339108
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->p()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1339109
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    .line 1339110
    if-eqz v0, :cond_0

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->k:LX/8OJ;

    .line 1339111
    iget-object p1, v6, LX/8OJ;->e:LX/8KY;

    invoke-virtual {p1, v2, v0}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    .line 1339112
    if-nez p1, :cond_3

    const/4 p1, 0x0

    :goto_1
    move v6, p1

    .line 1339113
    if-nez v6, :cond_1

    .line 1339114
    new-instance v1, LX/8ON;

    invoke-direct {v1, v0}, LX/8ON;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1339115
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1339116
    :cond_2
    return-void

    :cond_3
    invoke-static {p1, v3}, LX/8OJ;->a(Ljava/io/File;LX/434;)Z

    move-result p1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1339093
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1339094
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    .line 1339095
    const-string v1, "photo_upload_in_progress_waterfallid"

    invoke-static {v0, p1, v1}, LX/0cW;->b(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1339096
    invoke-static {v0, p1}, LX/0cW;->i(LX/0cW;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339097
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1339098
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1339099
    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    invoke-virtual {v1, p1}, LX/0cW;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339100
    :cond_1
    iput-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    return-object v0

    .line 1339101
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1339102
    iget-object v1, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    invoke-virtual {v1, p1}, LX/0cW;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1339103
    :cond_2
    iput-object v2, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1339090
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    .line 1339091
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    invoke-virtual {v0}, LX/8OL;->a()V

    .line 1339092
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1339087
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 1339088
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->o:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1339089
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->j:LX/8OL;

    invoke-virtual {v0}, LX/8OL;->c()Z

    move-result v0

    return v0
.end method
