.class public final Lcom/facebook/photos/upload/manager/UploadManager$4;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic e:LX/1EZ;


# direct methods
.method public constructor <init>(LX/1EZ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 0

    .prologue
    .line 1330981
    iput-object p1, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->e:LX/1EZ;

    iput-object p4, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->d:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-direct {p0, p2, p3}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1330982
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->e:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8KY;

    iget-object v1, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8KY;->a(Ljava/lang/String;)V

    .line 1330983
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->e:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadManager$4;->d:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330984
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330985
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    move-object v3, v1

    .line 1330986
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1330987
    :cond_0
    return-void

    .line 1330988
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 1330989
    const-class p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1330990
    const-string p0, "temp_file_to_clean_up"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1330991
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 1330992
    new-instance p0, Ljava/io/File;

    invoke-direct {p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 1330993
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method
