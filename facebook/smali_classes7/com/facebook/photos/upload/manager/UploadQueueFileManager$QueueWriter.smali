.class public abstract Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final b:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic c:LX/0cY;


# direct methods
.method public constructor <init>(LX/0cY;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 0

    .prologue
    .line 1331534
    iput-object p1, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331535
    iput-object p2, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1331536
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/io/DataOutputStream;)V
.end method

.method public final run()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1331537
    const/4 v5, 0x0

    .line 1331538
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget-object v0, v0, LX/0cY;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget-object v0, v0, LX/0cY;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 1331539
    :goto_0
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget-object v6, v6, LX/0cY;->d:Ljava/io/File;

    const/4 v7, 0x1

    invoke-direct {v4, v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1331540
    :try_start_1
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1331541
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1331542
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget v0, v0, LX/0cY;->e:I

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1331543
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget-object v0, v0, LX/0cY;->f:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1331544
    iget-object v0, p0, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->c:LX/0cY;

    iget-object v0, v0, LX/0cY;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1331545
    invoke-static {v0}, LX/0cY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 1331546
    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1331547
    :cond_0
    invoke-virtual {p0, v5}, Lcom/facebook/photos/upload/manager/UploadQueueFileManager$QueueWriter;->a(Ljava/io/DataOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1331548
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1331549
    :goto_1
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1331550
    :cond_1
    :goto_2
    return-void

    :cond_2
    move-wide v0, v2

    .line 1331551
    goto :goto_0

    .line 1331552
    :catch_0
    move-exception v0

    move-object v1, v5

    .line 1331553
    :goto_3
    :try_start_4
    sget-object v2, LX/0cY;->a:Ljava/lang/Class;

    const-string v3, "QueueWriter failed to append to queue: %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1331554
    if-eqz v1, :cond_1

    .line 1331555
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1331556
    :goto_4
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 1331557
    :catch_1
    goto :goto_2

    .line 1331558
    :catchall_0
    move-exception v0

    move-object v4, v5

    :goto_5
    if-eqz v4, :cond_3

    .line 1331559
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1331560
    :goto_6
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 1331561
    :cond_3
    :goto_7
    throw v0

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_6

    :catch_6
    goto :goto_7

    .line 1331562
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v4, v1

    goto :goto_5

    .line 1331563
    :catch_7
    move-exception v0

    move-object v1, v4

    goto :goto_3
.end method
