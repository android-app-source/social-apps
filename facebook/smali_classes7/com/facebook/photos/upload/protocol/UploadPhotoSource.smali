.class public Lcom/facebook/photos/upload/protocol/UploadPhotoSource;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1336993
    new-instance v0, LX/8NK;

    invoke-direct {v0}, LX/8NK;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1337003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337004
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a:Ljava/lang/String;

    .line 1337005
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->b:J

    .line 1337006
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1336999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1337000
    iput-object p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a:Ljava/lang/String;

    .line 1337001
    iput-wide p2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->b:J

    .line 1337002
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1336998
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1336997
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1336994
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336995
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336996
    return-void
.end method
