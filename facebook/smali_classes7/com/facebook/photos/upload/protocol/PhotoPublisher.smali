.class public Lcom/facebook/photos/upload/protocol/PhotoPublisher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/8Mt;

.field private final c:LX/0b3;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/MethodBatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/7mC;

.field private final f:LX/8NA;

.field private final g:LX/8NB;

.field private final h:LX/8N8;

.field private final i:LX/7lg;

.field private final j:LX/7lL;

.field private final k:LX/3HM;

.field private final l:LX/8NG;

.field private final m:LX/8ND;

.field private final n:LX/8NE;

.field private final o:LX/8Nb;

.field private final p:LX/3em;

.field private final q:LX/8Mv;

.field private final r:LX/0oz;

.field private final s:LX/0tX;

.field private final t:LX/7kY;

.field private final u:LX/7m9;

.field private final v:LX/8LX;

.field private final w:LX/7jB;

.field private final x:LX/8N9;

.field private final y:LX/0ad;

.field private final z:LX/0cX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1335764
    const-class v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/8Mt;LX/0b3;LX/0Or;LX/7mC;LX/8NA;LX/8NB;LX/8N8;LX/7lg;LX/7lL;LX/3HM;LX/8NG;LX/8ND;LX/8NE;LX/8Nb;LX/3em;LX/8Mv;LX/0oz;LX/0tX;LX/7kY;LX/7m9;LX/8LX;LX/7jB;LX/8N9;LX/0ad;LX/0cX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Mt;",
            "LX/0b3;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/MethodBatcher;",
            ">;",
            "LX/7mC;",
            "LX/8NA;",
            "LX/8NB;",
            "LX/8N8;",
            "LX/7lg;",
            "LX/7lL;",
            "LX/3HM;",
            "LX/8NG;",
            "LX/8ND;",
            "LX/8NE;",
            "LX/8Nb;",
            "LX/3em;",
            "LX/8Mv;",
            "LX/0oz;",
            "LX/0tX;",
            "LX/7kY;",
            "LX/7m9;",
            "LX/8LX;",
            "LX/7jB;",
            "LX/8N9;",
            "LX/0ad;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335871
    iput-object p1, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    .line 1335872
    iput-object p2, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    .line 1335873
    iput-object p3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    .line 1335874
    iput-object p4, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->e:LX/7mC;

    .line 1335875
    iput-object p5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->f:LX/8NA;

    .line 1335876
    iput-object p6, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->g:LX/8NB;

    .line 1335877
    iput-object p7, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->h:LX/8N8;

    .line 1335878
    iput-object p8, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->i:LX/7lg;

    .line 1335879
    iput-object p9, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->j:LX/7lL;

    .line 1335880
    iput-object p10, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->k:LX/3HM;

    .line 1335881
    iput-object p11, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->l:LX/8NG;

    .line 1335882
    iput-object p12, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->m:LX/8ND;

    .line 1335883
    iput-object p13, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->n:LX/8NE;

    .line 1335884
    iput-object p14, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->o:LX/8Nb;

    .line 1335885
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->p:LX/3em;

    .line 1335886
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->q:LX/8Mv;

    .line 1335887
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->r:LX/0oz;

    .line 1335888
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->s:LX/0tX;

    .line 1335889
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->t:LX/7kY;

    .line 1335890
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->u:LX/7m9;

    .line 1335891
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->v:LX/8LX;

    .line 1335892
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->w:LX/7jB;

    .line 1335893
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->x:LX/8N9;

    .line 1335894
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->y:LX/0ad;

    .line 1335895
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->z:LX/0cX;

    .line 1335896
    return-void
.end method

.method private static a(Ljava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1335860
    const/4 v0, 0x0

    .line 1335861
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1335862
    iget-wide v8, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v4, v8

    .line 1335863
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 1335864
    add-int/lit8 v1, v1, 0x1

    .line 1335865
    iget-object v3, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object v0, v3

    .line 1335866
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1335867
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1335868
    goto :goto_0

    .line 1335869
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILX/0e6;Z)LX/8Mw;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            "I",
            "LX/0e6",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            "Ljava/lang/Long;",
            ">;Z)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335815
    const-string v2, "2.0"

    invoke-virtual {p2, v2}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v9

    .line 1335816
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->U()Lcom/facebook/audience/model/UploadShot;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move v8, v2

    .line 1335817
    :goto_0
    if-eqz p6, :cond_6

    const/4 v2, 0x1

    :goto_1
    add-int/lit8 v3, v2, 0x1

    if-eqz v8, :cond_7

    const/4 v2, 0x1

    :goto_2
    add-int v4, v3, v2

    .line 1335818
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 1335819
    add-int/lit8 v4, v4, 0x1

    .line 1335820
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y()LX/0Px;

    move-result-object v2

    .line 1335821
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1335822
    add-int/lit8 v4, v4, 0x1

    .line 1335823
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    move-object v2, p2

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v7}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v4

    .line 1335824
    iget-object v2, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v3, LX/0bP;

    const/4 v5, 0x1

    const/4 v6, 0x1

    sget-object v7, LX/8Ki;->PUBLISHING:LX/8Ki;

    invoke-direct {v3, p1, v5, v6, v7}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1335825
    invoke-virtual {p2, v9, v4}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335826
    const/4 v3, 0x0

    .line 1335827
    iget-object v2, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/18W;

    invoke-virtual {v2}, LX/18W;->a()LX/2VK;

    move-result-object v5

    .line 1335828
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1335829
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-direct {p0, v5, v0, v2, v3}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(LX/2VK;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1335830
    :goto_3
    const-string v3, "publish"

    .line 1335831
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v6

    .line 1335832
    if-eqz v2, :cond_1

    .line 1335833
    invoke-virtual {v6, v2}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    .line 1335834
    :cond_1
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-static {v0, v1}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335835
    invoke-virtual {v6}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {v5, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 1335836
    if-eqz v8, :cond_2

    .line 1335837
    iget-object v2, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->x:LX/8N9;

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, p1, v6}, LX/8N9;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/Long;)LX/2Vj;

    move-result-object v2

    invoke-interface {v5, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 1335838
    :cond_2
    const-string v6, "fetchCreationStory"

    .line 1335839
    if-eqz p6, :cond_3

    .line 1335840
    new-instance v2, Lcom/facebook/api/story/FetchSingleStoryParams;

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    sget-object v10, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v11, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    const/4 v12, 0x0

    invoke-direct {v2, v7, v10, v11, v12}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1335841
    iget-object v7, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->k:LX/3HM;

    invoke-static {v7, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    invoke-interface {v5, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 1335842
    :cond_3
    :try_start_0
    invoke-virtual {p2, v9, v4}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335843
    const-string v2, "single_photo_publish"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v5, v2, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335844
    invoke-virtual {p2, v9, v4}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335845
    invoke-virtual {p2, v9, v4}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335846
    const/4 v2, 0x0

    .line 1335847
    if-eqz v8, :cond_8

    .line 1335848
    invoke-static {v5}, LX/8N9;->a(LX/2VK;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 1335849
    :goto_4
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 1335850
    if-eqz p6, :cond_4

    .line 1335851
    invoke-interface {v5, v6}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1335852
    iget-object v2, v2, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 1335853
    :cond_4
    new-instance v4, LX/8Mw;

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2, v3}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;Ljava/lang/String;)V

    return-object v4

    .line 1335854
    :cond_5
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_0

    .line 1335855
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1335856
    :catch_0
    move-exception v2

    .line 1335857
    invoke-static {v2}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v2

    .line 1335858
    invoke-virtual {p2, v9, v4, v2}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335859
    new-instance v3, LX/8N4;

    invoke-direct {v3, p0, v2}, LX/8N4;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/73z;)V

    throw v3

    :cond_8
    move-object v3, v2

    goto :goto_4

    :cond_9
    move-object v2, v3

    goto/16 :goto_3
.end method

.method private static a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;ILX/8Mx;)LX/8Mw;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "I",
            "LX/8Mx;",
            ")",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335797
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v1, v0

    .line 1335798
    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v3

    .line 1335799
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    move-object v0, v0

    .line 1335800
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move-object v0, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v1

    .line 1335801
    const-string v0, "2.0"

    invoke-virtual {p3, v0}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v2

    .line 1335802
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v3, LX/0bP;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    sget-object v6, LX/8Ki;->PUBLISHING:LX/8Ki;

    invoke-direct {v3, p1, v4, v5, v6}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1335803
    invoke-virtual {p3, v2, v1}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335804
    invoke-interface {p5}, LX/8Mx;->a()LX/399;

    move-result-object v0

    .line 1335805
    invoke-virtual {p3, v2, v1}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335806
    :try_start_0
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->s:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v0, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v3, 0x46052d9f

    invoke-static {v0, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    .line 1335807
    invoke-virtual {p3, v2, v1}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335808
    invoke-virtual {p3, v2, v1}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335809
    new-instance v0, LX/8Mw;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v0

    .line 1335810
    :catch_0
    move-exception v0

    .line 1335811
    instance-of v3, v0, Ljava/util/concurrent/ExecutionException;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Ljava/lang/Exception;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 1335812
    :cond_0
    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    .line 1335813
    invoke-virtual {p3, v2, v1, v0}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335814
    new-instance v1, LX/8N1;

    invoke-direct {v1, v0}, LX/8N1;-><init>(LX/73z;)V

    throw v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/upload/protocol/PhotoPublisher;
    .locals 1

    .prologue
    .line 1335796
    invoke-static {p0}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b(LX/0QB;)Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/2VK;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1335776
    iget-wide v4, p2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v4

    .line 1335777
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1335778
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1335779
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateInfoMethod"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1335780
    iget-object v1, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->m:LX/8ND;

    invoke-static {v1, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    .line 1335781
    iput-object v0, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 1335782
    move-object v1, v1

    .line 1335783
    if-eqz p4, :cond_0

    .line 1335784
    iput-object p4, v1, LX/2Vk;->d:Ljava/lang/String;

    .line 1335785
    :cond_0
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {p1, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1335786
    iget-object v1, p2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object v1, v1

    .line 1335787
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1335788
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateTagMethod"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 1335789
    iget-object v1, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->l:LX/8NG;

    invoke-static {v1, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    .line 1335790
    iput-object v0, v1, LX/2Vk;->d:Ljava/lang/String;

    .line 1335791
    move-object v0, v1

    .line 1335792
    iput-object p4, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1335793
    move-object v0, v0

    .line 1335794
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {p1, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1335795
    :cond_1
    :goto_0
    return-object p4

    :cond_2
    move-object p4, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1335773
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1335774
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1335775
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/2VK;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2VK;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1335765
    const/4 v0, 0x0

    .line 1335766
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1335767
    iget-boolean v3, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    move v3, v3

    .line 1335768
    if-eqz v3, :cond_1

    .line 1335769
    invoke-direct {p0, p1, v0, v1, p3}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(LX/2VK;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 1335770
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1335771
    goto :goto_0

    .line 1335772
    :cond_0
    return-object p3

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/upload/protocol/PhotoPublisher;
    .locals 27

    .prologue
    .line 1335574
    new-instance v1, Lcom/facebook/photos/upload/protocol/PhotoPublisher;

    invoke-static/range {p0 .. p0}, LX/8Mt;->a(LX/0QB;)LX/8Mt;

    move-result-object v2

    check-cast v2, LX/8Mt;

    invoke-static/range {p0 .. p0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v3

    check-cast v3, LX/0b3;

    const/16 v4, 0xb81

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/7mC;->a(LX/0QB;)LX/7mC;

    move-result-object v5

    check-cast v5, LX/7mC;

    invoke-static/range {p0 .. p0}, LX/8NA;->a(LX/0QB;)LX/8NA;

    move-result-object v6

    check-cast v6, LX/8NA;

    invoke-static/range {p0 .. p0}, LX/8NB;->a(LX/0QB;)LX/8NB;

    move-result-object v7

    check-cast v7, LX/8NB;

    invoke-static/range {p0 .. p0}, LX/8N8;->a(LX/0QB;)LX/8N8;

    move-result-object v8

    check-cast v8, LX/8N8;

    invoke-static/range {p0 .. p0}, LX/7lg;->a(LX/0QB;)LX/7lg;

    move-result-object v9

    check-cast v9, LX/7lg;

    invoke-static/range {p0 .. p0}, LX/7lL;->a(LX/0QB;)LX/7lL;

    move-result-object v10

    check-cast v10, LX/7lL;

    invoke-static/range {p0 .. p0}, LX/3HM;->a(LX/0QB;)LX/3HM;

    move-result-object v11

    check-cast v11, LX/3HM;

    invoke-static/range {p0 .. p0}, LX/8NG;->a(LX/0QB;)LX/8NG;

    move-result-object v12

    check-cast v12, LX/8NG;

    invoke-static/range {p0 .. p0}, LX/8ND;->a(LX/0QB;)LX/8ND;

    move-result-object v13

    check-cast v13, LX/8ND;

    invoke-static/range {p0 .. p0}, LX/8NE;->a(LX/0QB;)LX/8NE;

    move-result-object v14

    check-cast v14, LX/8NE;

    invoke-static/range {p0 .. p0}, LX/8Nb;->a(LX/0QB;)LX/8Nb;

    move-result-object v15

    check-cast v15, LX/8Nb;

    invoke-static/range {p0 .. p0}, LX/3em;->a(LX/0QB;)LX/3em;

    move-result-object v16

    check-cast v16, LX/3em;

    invoke-static/range {p0 .. p0}, LX/8Mv;->a(LX/0QB;)LX/8Mv;

    move-result-object v17

    check-cast v17, LX/8Mv;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v18

    check-cast v18, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v19

    check-cast v19, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/7kY;->a(LX/0QB;)LX/7kY;

    move-result-object v20

    check-cast v20, LX/7kY;

    invoke-static/range {p0 .. p0}, LX/7m9;->a(LX/0QB;)LX/7m9;

    move-result-object v21

    check-cast v21, LX/7m9;

    invoke-static/range {p0 .. p0}, LX/8LX;->a(LX/0QB;)LX/8LX;

    move-result-object v22

    check-cast v22, LX/8LX;

    invoke-static/range {p0 .. p0}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v23

    check-cast v23, LX/7jB;

    invoke-static/range {p0 .. p0}, LX/8N9;->a(LX/0QB;)LX/8N9;

    move-result-object v24

    check-cast v24, LX/8N9;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v26

    check-cast v26, LX/0cX;

    invoke-direct/range {v1 .. v26}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;-><init>(LX/8Mt;LX/0b3;LX/0Or;LX/7mC;LX/8NA;LX/8NB;LX/8N8;LX/7lg;LX/7lL;LX/3HM;LX/8NG;LX/8ND;LX/8NE;LX/8Nb;LX/3em;LX/8Mv;LX/0oz;LX/0tX;LX/7kY;LX/7m9;LX/8LX;LX/7jB;LX/8N9;LX/0ad;LX/0cX;)V

    .line 1335575
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/util/List;Ljava/util/List;I)LX/8Mw;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335710
    const-string v4, "2.0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v27

    .line 1335711
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    const/4 v4, 0x1

    move/from16 v22, v4

    .line 1335712
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v10

    .line 1335713
    if-eqz v22, :cond_1

    const/4 v4, 0x1

    :goto_1
    add-int/2addr v4, v10

    .line 1335714
    invoke-static/range {p5 .. p5}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Ljava/util/List;)I

    move-result v5

    add-int v6, v4, v5

    .line 1335715
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    move-object/from16 v4, p2

    move/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v28

    .line 1335716
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v5, LX/0bP;

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v6

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v7

    sget-object v8, LX/8Ki;->PUBLISHING:LX/8Ki;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6, v7, v8}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 1335717
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335718
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/18W;

    invoke-virtual {v4}, LX/18W;->a()LX/2VK;

    move-result-object v29

    .line 1335719
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2, v4}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/2VK;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1335720
    const-string v6, "status"

    .line 1335721
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->v:LX/8LX;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/8LX;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/5M9;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/5M9;->c(Z)LX/5M9;

    move-result-object v8

    .line 1335722
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->y:LX/0ad;

    sget-short v5, LX/8Jz;->i:S

    const/4 v9, 0x0

    invoke-interface {v4, v5, v9}, LX/0ad;->a(SZ)Z

    move-result v9

    .line 1335723
    if-eqz v9, :cond_3

    .line 1335724
    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    .line 1335725
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 1335726
    invoke-static {}, Lcom/facebook/composer/publish/common/MediaAttachment;->newBuilder()Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setMediaFbid(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setStickers(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setTextOverlay(Ljava/lang/String;)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setHasCrop(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setHasRotation(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v4

    invoke-virtual {v12, v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->setHasFilter(Z)Lcom/facebook/composer/publish/common/MediaAttachment$Builder;

    move-result-object v4

    .line 1335727
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/MediaAttachment$Builder;->a()Lcom/facebook/composer/publish/common/MediaAttachment;

    move-result-object v4

    invoke-virtual {v11, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335728
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 1335729
    :cond_0
    const/4 v4, 0x0

    move/from16 v22, v4

    goto/16 :goto_0

    .line 1335730
    :cond_1
    const/4 v4, 0x2

    goto/16 :goto_1

    .line 1335731
    :cond_2
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/5M9;->e(LX/0Px;)LX/5M9;

    .line 1335732
    :cond_3
    invoke-virtual {v8}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v4

    .line 1335733
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->e:LX/7mC;

    invoke-static {v5, v4}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335734
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->e:LX/7mC;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    .line 1335735
    if-eqz v7, :cond_4

    .line 1335736
    invoke-virtual {v4, v7}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    .line 1335737
    :cond_4
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335738
    if-nez v9, :cond_8

    .line 1335739
    const/4 v5, 0x0

    .line 1335740
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v30

    const/4 v4, 0x0

    move/from16 v24, v4

    move/from16 v25, v5

    move-object/from16 v23, v6

    :goto_3
    move/from16 v0, v24

    move/from16 v1, v30

    if-ge v0, v1, :cond_5

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1335741
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "photo_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1335742
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "{result=status:$.id}"

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v8

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v11}, LX/8Mv;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v12}, LX/8Mv;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v13}, LX/8Mv;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v13

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v14}, LX/8Mv;->d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v14

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v15}, LX/8Mv;->e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->an()LX/5Rn;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ao()J

    move-result-wide v17

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v20

    const/16 v21, 0x0

    invoke-static/range {v4 .. v21}, LX/8Mu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;ZZZLX/5Rn;JZZZ)LX/8Mu;

    move-result-object v4

    .line 1335743
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v5

    invoke-virtual {v5}, LX/2Vk;->a()LX/2Vj;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, LX/2VK;->a(LX/2Vj;)V

    .line 1335744
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v5, v4}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335745
    add-int/lit8 v5, v25, 0x1

    .line 1335746
    add-int/lit8 v4, v24, 0x1

    move/from16 v24, v4

    move/from16 v25, v5

    move-object/from16 v23, v26

    goto/16 :goto_3

    :cond_5
    move-object/from16 v4, v23

    .line 1335747
    :goto_4
    const-string v5, "fetch"

    .line 1335748
    if-nez v22, :cond_6

    .line 1335749
    new-instance v6, Lcom/facebook/api/story/FetchSingleStoryParams;

    const-string v7, "{result=status:$.id}"

    sget-object v8, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v9, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    const/4 v10, 0x0

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1335750
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->k:LX/3HM;

    invoke-static {v7, v6}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335751
    :cond_6
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335752
    const-string v4, "multi_photo_publish"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-interface {v0, v4, v6}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335753
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335754
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335755
    if-nez v22, :cond_7

    .line 1335756
    move-object/from16 v0, v29

    invoke-interface {v0, v5}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1335757
    new-instance v6, LX/8Mw;

    const-string v5, "status"

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v4, v4, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-direct {v6, v5, v4}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    move-object v4, v6

    .line 1335758
    :goto_5
    return-object v4

    .line 1335759
    :catch_0
    move-exception v4

    .line 1335760
    invoke-static {v4}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v4

    .line 1335761
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v4}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335762
    new-instance v5, LX/8N5;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, LX/8N5;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/73z;)V

    throw v5

    .line 1335763
    :cond_7
    const/4 v7, 0x1

    const/4 v8, 0x1

    const-string v4, "status"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v10, p3

    invoke-virtual/range {v4 .. v11}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;

    move-result-object v4

    goto :goto_5

    :cond_8
    move-object v4, v6

    goto/16 :goto_4
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;LX/0Px;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILX/8LR;)LX/8Mw;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/8LR;",
            ")",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335897
    const-string v4, "2.0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v36

    .line 1335898
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->O()LX/8LS;

    move-result-object v4

    sget-object v5, LX/8LS;->ALBUM:LX/8LS;

    if-eq v4, v5, :cond_2

    const/4 v4, 0x1

    move/from16 v29, v4

    .line 1335899
    :goto_0
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_3

    const/4 v4, 0x1

    move/from16 v30, v4

    .line 1335900
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v24

    .line 1335901
    if-eqz v29, :cond_4

    if-nez v30, :cond_4

    const/4 v4, 0x1

    :goto_2
    add-int v4, v4, v24

    .line 1335902
    invoke-static/range {p5 .. p5}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Ljava/util/List;)I

    move-result v5

    add-int v6, v4, v5

    .line 1335903
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    move-object/from16 v4, p2

    move/from16 v9, p8

    invoke-virtual/range {v4 .. v9}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v37

    .line 1335904
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v5, LX/0bP;

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v6

    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v7

    sget-object v8, LX/8Ki;->PUBLISHING:LX/8Ki;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6, v7, v8}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 1335905
    move-object/from16 v0, p2

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335906
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/18W;

    invoke-virtual {v4}, LX/18W;->a()LX/2VK;

    move-result-object v38

    .line 1335907
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2, v4}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/2VK;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1335908
    sget-object v4, LX/8LR;->EDIT_POST:LX/8LR;

    move-object/from16 v0, p9

    if-ne v0, v4, :cond_5

    const/4 v4, 0x1

    move/from16 v35, v4

    .line 1335909
    :goto_3
    if-nez v35, :cond_b

    .line 1335910
    const/4 v5, 0x0

    .line 1335911
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->z()LX/0Px;

    move-result-object v39

    .line 1335912
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v40

    const/4 v4, 0x0

    move/from16 v32, v4

    move/from16 v33, v5

    :goto_4
    move/from16 v0, v32

    move/from16 v1, v40

    if-ge v0, v1, :cond_9

    move-object/from16 v0, p3

    move/from16 v1, v32

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1335913
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "photo_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 1335914
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v12

    .line 1335915
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->E()J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-ltz v4, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->E()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    .line 1335916
    :goto_6
    const/4 v4, 0x0

    .line 1335917
    if-eqz v39, :cond_0

    .line 1335918
    move-object/from16 v0, v39

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    .line 1335919
    const-string v5, "caption"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1335920
    :cond_0
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->F()Ljava/lang/String;

    move-result-object v11

    if-nez v4, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->B()Ljava/lang/String;

    move-result-object v13

    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ah()Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ai()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->H()Z

    move-result v16

    move-object/from16 v0, p4

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p4

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p4

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v19

    move-object/from16 v0, p4

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v20

    move-object/from16 v0, p4

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v21

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v22

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->Z()Z

    move-result v25

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v27

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aS()Z

    move-result v28

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v5 .. v28}, LX/8Mu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJIZZZZ)LX/8Mu;

    move-result-object v4

    .line 1335921
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v5

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v5

    .line 1335922
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v6, v4}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335923
    if-eqz v31, :cond_1

    .line 1335924
    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    .line 1335925
    :cond_1
    invoke-virtual {v5}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335926
    add-int/lit8 v5, v33, 0x1

    .line 1335927
    add-int/lit8 v4, v32, 0x1

    move/from16 v32, v4

    move/from16 v33, v5

    move-object/from16 v31, v34

    goto/16 :goto_4

    .line 1335928
    :cond_2
    const/4 v4, 0x0

    move/from16 v29, v4

    goto/16 :goto_0

    .line 1335929
    :cond_3
    const/4 v4, 0x0

    move/from16 v30, v4

    goto/16 :goto_1

    .line 1335930
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1335931
    :cond_5
    const/4 v4, 0x0

    move/from16 v35, v4

    goto/16 :goto_3

    .line 1335932
    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_5

    .line 1335933
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_6

    :cond_8
    move-object v13, v4

    .line 1335934
    goto/16 :goto_7

    :cond_9
    move-object/from16 v6, v31

    .line 1335935
    :goto_8
    if-eqz v29, :cond_a

    .line 1335936
    new-instance v7, Lcom/facebook/api/story/FetchSingleStoryParams;

    if-eqz v35, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->T()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v4

    :goto_9
    sget-object v8, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eqz v35, :cond_f

    sget-object v5, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    :goto_a
    const/4 v9, 0x0

    invoke-direct {v7, v4, v8, v5, v9}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1335937
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->k:LX/3HM;

    invoke-static {v4, v7}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v5, "fetch"

    invoke-virtual {v4, v5}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335938
    :cond_a
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335939
    const-string v4, "multi_photo_publish_target"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-interface {v0, v4, v5}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335940
    move-object/from16 v0, p2

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335941
    move-object/from16 v0, p2

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335942
    if-nez v30, :cond_10

    .line 1335943
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    .line 1335944
    if-eqz v29, :cond_11

    .line 1335945
    const-string v4, "fetch"

    move-object/from16 v0, v38

    invoke-interface {v0, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1335946
    iget-object v4, v4, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    move-object v5, v4

    .line 1335947
    :goto_b
    new-instance v6, LX/8Mw;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4, v5}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    move-object v4, v6

    .line 1335948
    :goto_c
    return-object v4

    .line 1335949
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->T()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1335950
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1335951
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_c

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_c

    .line 1335952
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams;->getMediaFbIds()LX/0Px;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1335953
    :cond_c
    invoke-virtual/range {p3 .. p3}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_d
    if-ge v6, v8, :cond_d

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 1335954
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335955
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_d

    .line 1335956
    :cond_d
    invoke-static {v4}, Lcom/facebook/composer/publish/common/EditPostParams;->a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v4

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setMediaFbIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v4

    .line 1335957
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->u:LX/7m9;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v5, "edit_post"

    invoke-virtual {v4, v5}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335958
    const-string v4, "edit_post"

    move-object v6, v4

    goto/16 :goto_8

    .line 1335959
    :cond_e
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :cond_f
    sget-object v5, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    goto/16 :goto_a

    .line 1335960
    :catch_0
    move-exception v4

    .line 1335961
    invoke-static {v4}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v4

    .line 1335962
    move-object/from16 v0, p2

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2, v4}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335963
    new-instance v5, LX/8N6;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, LX/8N6;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/73z;)V

    throw v5

    .line 1335964
    :cond_10
    const/4 v7, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v8, v29

    move-object/from16 v10, p3

    invoke-virtual/range {v4 .. v11}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;

    move-result-object v4

    goto/16 :goto_c

    :cond_11
    move-object v5, v4

    goto/16 :goto_b
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;
    .locals 7

    .prologue
    .line 1335709
    iget-object v5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->f:LX/8NA;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILX/0e6;Z)LX/8Mw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Ljava/util/List;Ljava/util/List;I)LX/8Mw;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1335672
    const-string v0, "2.0"

    invoke-virtual {p2, v0}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v6

    .line 1335673
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v0

    .line 1335674
    add-int/lit8 v0, v0, 0x1

    .line 1335675
    invoke-static {p4}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Ljava/util/List;)I

    move-result v1

    add-int v2, v0, v1

    .line 1335676
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v1, v0

    .line 1335677
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v3

    .line 1335678
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    move-object v0, v0

    .line 1335679
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move-object v0, p2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v1

    .line 1335680
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v2, LX/0bP;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    sget-object v5, LX/8Ki;->PUBLISHING:LX/8Ki;

    invoke-direct {v2, p1, v3, v4, v5}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1335681
    invoke-virtual {p2, v6, v1}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335682
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v2

    .line 1335683
    invoke-static {p0, v2, p4, v9}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/2VK;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1335684
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1335685
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1335686
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1335687
    :cond_0
    const-string v5, "post_life_event_photo"

    .line 1335688
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-object v0, v0

    .line 1335689
    iget-object v7, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->j:LX/7lL;

    new-instance v8, LX/7l5;

    invoke-direct {v8, v0}, LX/7l5;-><init>(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;)V

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1335690
    iput-object v4, v8, LX/7l5;->l:LX/0Px;

    .line 1335691
    move-object v4, v8

    .line 1335692
    invoke-virtual {v4}, LX/7l5;->a()Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    move-result-object v4

    invoke-static {v7, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    .line 1335693
    iput-object v5, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 1335694
    move-object v4, v4

    .line 1335695
    if-eqz v3, :cond_1

    .line 1335696
    iput-object v3, v4, LX/2Vk;->d:Ljava/lang/String;

    .line 1335697
    :cond_1
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v3

    invoke-interface {v2, v3}, LX/2VK;->a(LX/2Vj;)V

    .line 1335698
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->j:LX/7lL;

    invoke-static {v3, v0}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335699
    :try_start_0
    invoke-virtual {p2, v6, v1}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335700
    const-string v0, "photo_upload_life_event"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v2, v0, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335701
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v4, LX/8Kd;

    sget-object v7, LX/8KZ;->UPLOAD_SUCCESS:LX/8KZ;

    invoke-interface {v2, v5}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, p1, v7, v0}, LX/8Kd;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 1335702
    invoke-virtual {p2, v6, v1}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335703
    invoke-virtual {p2, v6, v1}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335704
    :goto_1
    new-instance v1, LX/8Mw;

    invoke-interface {v2, v5}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v1

    .line 1335705
    :catch_0
    move-exception v0

    .line 1335706
    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    .line 1335707
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v4, LX/8Kd;

    sget-object v7, LX/8KZ;->UPLOAD_FAILED:LX/8KZ;

    invoke-direct {v4, p1, v7, v9}, LX/8Kd;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 1335708
    invoke-virtual {p2, v6, v1, v0}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)LX/8Mw;
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335637
    const-string v4, "2.0"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v33

    .line 1335638
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v24

    .line 1335639
    add-int/lit8 v4, v24, 0x2

    .line 1335640
    invoke-static/range {p5 .. p5}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Ljava/util/List;)I

    move-result v5

    add-int v6, v4, v5

    .line 1335641
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    move-object/from16 v4, p2

    move/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v34

    .line 1335642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v5, LX/0bP;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v7

    sget-object v8, LX/8Ki;->PUBLISHING:LX/8Ki;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6, v7, v8}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 1335643
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335644
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/18W;

    invoke-virtual {v4}, LX/18W;->a()LX/2VK;

    move-result-object v35

    .line 1335645
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2, v4}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/2VK;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1335646
    const-string v31, "post_review"

    .line 1335647
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->K()Lcom/facebook/composer/protocol/PostReviewParams;

    move-result-object v5

    invoke-static {v5}, LX/7li;->a(Lcom/facebook/composer/protocol/PostReviewParams;)LX/7li;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, LX/7li;->a(I)LX/7li;

    move-result-object v5

    invoke-virtual {v5}, LX/7li;->a()Lcom/facebook/composer/protocol/PostReviewParams;

    move-result-object v5

    .line 1335648
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->i:LX/7lg;

    invoke-static {v6, v5}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v6, v0}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v6

    .line 1335649
    if-eqz v4, :cond_0

    .line 1335650
    invoke-virtual {v6, v4}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    .line 1335651
    :cond_0
    invoke-virtual {v6}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335652
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->i:LX/7lg;

    invoke-static {v4, v5}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335653
    const/4 v4, 0x0

    .line 1335654
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    move/from16 v29, v4

    move-object/from16 v30, v31

    :goto_0
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1335655
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v29

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 1335656
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "{result="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":$.og_action_id}"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v19

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v20

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    invoke-static {v4}, LX/8Mv;->e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z

    move-result v21

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aj()J

    move-result-wide v22

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aM()Z

    move-result v27

    const/16 v28, 0x0

    invoke-static/range {v5 .. v28}, LX/8Mu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZJIZZZZ)LX/8Mu;

    move-result-object v4

    .line 1335657
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v5, v4}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335658
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->b:LX/8Mt;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v4

    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-interface {v0, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1335659
    add-int/lit8 v4, v29, 0x1

    move/from16 v29, v4

    move-object/from16 v30, v32

    .line 1335660
    goto/16 :goto_0

    .line 1335661
    :cond_1
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335662
    const-string v4, "photo_upload_review"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-interface {v0, v4, v5}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335663
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335664
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1335665
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v5, LX/8Kj;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, LX/8Kj;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 1335666
    new-instance v5, LX/8Mw;

    move-object/from16 v0, v35

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    invoke-direct {v5, v4, v6}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v5

    .line 1335667
    :catch_0
    move-exception v4

    .line 1335668
    :try_start_1
    invoke-static {v4}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v4

    .line 1335669
    move-object/from16 v0, p2

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2, v4}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335670
    new-instance v5, LX/8N3;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, LX/8N3;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/73z;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1335671
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->c:LX/0b3;

    new-instance v6, LX/8Kj;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, LX/8Kj;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    throw v4
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;ZZLjava/lang/String;LX/0Px;I)LX/8Mw;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "ZZ",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335610
    const-string v6, "fetch"

    .line 1335611
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v7

    .line 1335612
    const-string v0, "2.0"

    invoke-virtual {p2, v0}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v8

    .line 1335613
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz p4, :cond_2

    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move-object v0, p2

    move/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, LX/73w;->a(Ljava/lang/String;IIII)LX/74Z;

    move-result-object v1

    .line 1335614
    invoke-virtual {p2, v8, v1}, LX/73w;->a(LX/74b;LX/74Z;)V

    .line 1335615
    const-string v2, "updateOrderMethod"

    .line 1335616
    new-instance v3, LX/8NF;

    if-eqz p3, :cond_3

    const/16 v0, 0x5f

    invoke-static {p5, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-direct {v3, v0, p6, p3}, LX/8NF;-><init>(Ljava/lang/String;LX/0Px;Z)V

    .line 1335617
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->n:LX/8NE;

    invoke-static {v0, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v7, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1335618
    if-eqz p4, :cond_0

    .line 1335619
    new-instance v3, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eqz p3, :cond_4

    sget-object v0, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    :goto_2
    const/4 v5, 0x0

    invoke-direct {v3, p5, v4, v0, v5}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 1335620
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->k:LX/3HM;

    invoke-static {v0, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v7, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1335621
    :cond_0
    :try_start_0
    invoke-virtual {p2, v8, v1}, LX/73w;->b(LX/74b;LX/74Z;)V

    .line 1335622
    const-string v0, "multi_photo_reorder"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v7, v0, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1335623
    invoke-virtual {p2, v8, v1}, LX/73w;->c(LX/74b;LX/74Z;)V

    .line 1335624
    invoke-virtual {p2, v8, v1}, LX/73w;->d(LX/74b;LX/74Z;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335625
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1335626
    if-eqz p4, :cond_1

    .line 1335627
    invoke-interface {v7, v6}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1335628
    iget-object v0, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1335629
    :cond_1
    new-instance v1, LX/8Mw;

    invoke-direct {v1, p5, v0}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v1

    .line 1335630
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    move-object v0, p5

    .line 1335631
    goto :goto_1

    .line 1335632
    :cond_4
    sget-object v0, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    goto :goto_2

    .line 1335633
    :catch_0
    move-exception v0

    .line 1335634
    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    .line 1335635
    invoke-virtual {p2, v8, v1, v0}, LX/73w;->a(LX/74b;LX/74Z;LX/73y;)V

    .line 1335636
    new-instance v1, LX/8N2;

    invoke-direct {v1, v0, p5}, LX/8N2;-><init>(LX/73z;Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;)LX/8Mw;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335587
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v1

    .line 1335588
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1335589
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1335590
    new-instance v4, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;-><init>(Ljava/lang/String;I)V

    .line 1335591
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetch-image-info-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1335592
    iget-object v5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->p:LX/3em;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v5

    .line 1335593
    iput-object v0, v5, LX/2Vk;->c:Ljava/lang/String;

    .line 1335594
    move-object v5, v5

    .line 1335595
    invoke-virtual {v5}, LX/2Vk;->a()LX/2Vj;

    move-result-object v5

    invoke-interface {v1, v5}, LX/2VK;->a(LX/2Vj;)V

    .line 1335596
    iget-object v5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->p:LX/3em;

    invoke-static {v5, v4}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335597
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "{result="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":$..image.uri}"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1335598
    :cond_0
    const-string v0, "-1"

    const/16 v3, 0x7d0

    const/16 v4, 0xc8

    invoke-static {v0, p1, v2, v3, v4}, LX/8Nc;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;II)LX/8Nc;

    move-result-object v0

    .line 1335599
    const-string v2, "slideshow-video-post"

    .line 1335600
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->o:LX/8Nb;

    invoke-static {v3, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v3

    .line 1335601
    iput-object v2, v3, LX/2Vk;->c:Ljava/lang/String;

    .line 1335602
    move-object v3, v3

    .line 1335603
    invoke-virtual {v3}, LX/2Vk;->a()LX/2Vj;

    move-result-object v3

    invoke-interface {v1, v3}, LX/2VK;->a(LX/2Vj;)V

    .line 1335604
    iget-object v3, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->o:LX/8Nb;

    invoke-static {v3, v0}, LX/8NC;->a(LX/0e6;Ljava/lang/Object;)V

    .line 1335605
    :try_start_0
    const-string v0, "slideshow-video-batch"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v1, v0, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335606
    new-instance v3, LX/8Mw;

    invoke-interface {v1, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-direct {v3, v0, v1}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v3

    .line 1335607
    :catch_0
    move-exception v0

    .line 1335608
    invoke-static {v0}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    .line 1335609
    new-instance v1, LX/8N5;

    invoke-direct {v1, p0, v0}, LX/8N5;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;LX/73z;)V

    throw v1
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;I)LX/8Mw;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335586
    new-instance v5, LX/8My;

    invoke-direct {v5, p0, p2, p1}, LX/8My;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Ljava/util/List;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;ILX/8Mx;)LX/8Mw;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;
    .locals 7

    .prologue
    .line 1335585
    iget-object v5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->g:LX/8NB;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILX/0e6;Z)LX/8Mw;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;)LX/8Mw;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335578
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object v0, v0

    .line 1335579
    if-eqz v0, :cond_0

    .line 1335580
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-object v0, v0

    .line 1335581
    invoke-virtual {v0, p2}, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a(Ljava/util/List;)Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-result-object v0

    .line 1335582
    iget-object v1, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->t:LX/7kY;

    invoke-virtual {v1, v0}, LX/7kY;->a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)V

    .line 1335583
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->w:LX/7jB;

    new-instance v1, LX/7jG;

    invoke-direct {v1, p2}, LX/7jG;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1335584
    new-instance v0, LX/8Mw;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8Mw;-><init>(Ljava/lang/String;LX/0am;)V

    return-object v0
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;I)LX/8Mw;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "I)",
            "LX/8Mw;"
        }
    .end annotation

    .prologue
    .line 1335577
    new-instance v5, LX/8Mz;

    invoke-direct {v5, p0, p2, p1}, LX/8Mz;-><init>(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Ljava/util/List;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/List;LX/73w;ILX/8Mx;)LX/8Mw;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;I)LX/8Mw;
    .locals 7

    .prologue
    .line 1335576
    iget-object v5, p0, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->h:LX/8N8;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/upload/protocol/PhotoPublisher;->a(Lcom/facebook/photos/upload/protocol/PhotoPublisher;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73w;Lcom/facebook/photos/upload/protocol/UploadPhotoParams;ILX/0e6;Z)LX/8Mw;

    move-result-object v0

    return-object v0
.end method
