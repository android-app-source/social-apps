.class public final Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public static final b:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public static final c:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public static final d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;


# instance fields
.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1335968
    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1335969
    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    const-string v1, "{\"value\":\"SELF\"}"

    invoke-direct {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->b:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1335970
    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    const-string v1, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-direct {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->c:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1335971
    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    const-string v1, "{\"value\":\"EVERYONE\"}"

    invoke-direct {v0, v1}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1335972
    new-instance v0, LX/8N7;

    invoke-direct {v0}, LX/8N7;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1335973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335974
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    .line 1335975
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1335976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335977
    iput-object p1, p0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    .line 1335978
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1335979
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1335980
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1335981
    return-void
.end method
