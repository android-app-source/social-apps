.class public Lcom/facebook/photos/upload/protocol/UploadPhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/lang/String;

.field public B:Z

.field public C:J

.field public D:Z

.field public E:I

.field public F:J

.field public G:F

.field public H:F

.field public I:LX/5Rn;

.field public J:J

.field public K:Ljava/lang/String;

.field public final L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final N:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/PostChannelFeedbackState;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final O:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final P:Ljava/lang/String;

.field public final Q:Ljava/lang/String;

.field public final R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final S:J

.field public final T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final U:Z

.field public final V:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final W:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Y:Z

.field public final Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field private final a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

.field public final aa:Ljava/lang/String;

.field private final ab:Ljava/lang/String;

.field private final b:J

.field private final c:J

.field private final d:J

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Z

.field private final g:Z

.field public final h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:I

.field private final s:LX/434;

.field public final t:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final u:Z

.field public final v:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final w:Z

.field public final x:Z

.field public final y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1336858
    new-instance v0, LX/8NI;

    invoke-direct {v0}, LX/8NI;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1336859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336860
    const-class v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336861
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    .line 1336862
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    .line 1336863
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    .line 1336864
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    .line 1336865
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    .line 1336866
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g:Z

    .line 1336867
    const-class v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1336868
    const-class v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1336869
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    .line 1336870
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    .line 1336871
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    .line 1336872
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->B:Z

    .line 1336873
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    .line 1336874
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    .line 1336875
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->E:I

    .line 1336876
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1336877
    const-class v2, Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1336878
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    .line 1336879
    if-eqz v2, :cond_2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    .line 1336880
    const-class v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1336881
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    .line 1336882
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    .line 1336883
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    .line 1336884
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    .line 1336885
    new-instance v0, LX/434;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-direct {v0, v2, v3}, LX/434;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    .line 1336886
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1336887
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    .line 1336888
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    .line 1336889
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1336890
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    .line 1336891
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    .line 1336892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->z:Ljava/lang/String;

    .line 1336893
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->G:F

    .line 1336894
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H:F

    .line 1336895
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->I:LX/5Rn;

    .line 1336896
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->J:J

    .line 1336897
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    .line 1336898
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    .line 1336899
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    .line 1336900
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    .line 1336901
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    .line 1336902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    .line 1336903
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    .line 1336904
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->R:Ljava/lang/String;

    .line 1336905
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    .line 1336906
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    .line 1336907
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->U:Z

    .line 1336908
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->V:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1336909
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1336910
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->W:LX/0Px;

    .line 1336911
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    .line 1336912
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    .line 1336913
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1336914
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    .line 1336915
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ab:Ljava/lang/String;

    .line 1336916
    return-void

    :cond_1
    move-object v0, v1

    .line 1336917
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 1336918
    goto/16 :goto_1
.end method

.method private constructor <init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;JJJLjava/lang/String;ZZLcom/facebook/bitmaps/SphericalPhotoMetadata;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;Ljava/lang/String;ZLX/0Px;LX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/434;Lcom/facebook/auth/viewercontext/ViewerContext;ZJLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;FFLX/5Rn;JLcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/bitmaps/SphericalPhotoMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p18    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p19    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p23    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p27    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p30    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p36    # Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p37    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p38    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p39    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PostChannelFeedbackState;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p43    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p46    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p48    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p49    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p50    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/protocol/UploadPhotoSource;",
            "JJJ",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/bitmaps/SphericalPhotoMetadata;",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/434;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "ZJ",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "Ljava/lang/String;",
            "FF",
            "LX/5Rn;",
            "J",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1336919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336920
    iput-object p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336921
    iput-wide p2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    .line 1336922
    iput-wide p4, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    .line 1336923
    iput-wide p6, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    .line 1336924
    iput-object p8, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    .line 1336925
    iput-boolean p9, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    .line 1336926
    iput-boolean p10, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g:Z

    .line 1336927
    iput-object p11, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1336928
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    .line 1336929
    invoke-static/range {p12 .. p12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1336930
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    .line 1336931
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    .line 1336932
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    .line 1336933
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->B:Z

    .line 1336934
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    .line 1336935
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    .line 1336936
    const/4 v2, -0x1

    iput v2, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->E:I

    .line 1336937
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    .line 1336938
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    .line 1336939
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1336940
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    .line 1336941
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    .line 1336942
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    .line 1336943
    move/from16 v0, p21

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    .line 1336944
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1336945
    move/from16 v0, p24

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    .line 1336946
    move-wide/from16 v0, p25

    iput-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    .line 1336947
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1336948
    move/from16 v0, p28

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    .line 1336949
    move/from16 v0, p29

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    .line 1336950
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->z:Ljava/lang/String;

    .line 1336951
    move/from16 v0, p31

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->G:F

    .line 1336952
    move/from16 v0, p32

    iput v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H:F

    .line 1336953
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->I:LX/5Rn;

    .line 1336954
    move-wide/from16 v0, p34

    iput-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->J:J

    .line 1336955
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    .line 1336956
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    .line 1336957
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    .line 1336958
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    .line 1336959
    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    .line 1336960
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    .line 1336961
    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    .line 1336962
    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->R:Ljava/lang/String;

    .line 1336963
    move-wide/from16 v0, p44

    iput-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    .line 1336964
    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    .line 1336965
    move/from16 v0, p47

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->U:Z

    .line 1336966
    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->V:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1336967
    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->W:LX/0Px;

    .line 1336968
    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    .line 1336969
    move/from16 v0, p51

    iput-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    .line 1336970
    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1336971
    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    .line 1336972
    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ab:Ljava/lang/String;

    .line 1336973
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;JJJLjava/lang/String;ZZLcom/facebook/bitmaps/SphericalPhotoMetadata;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;Ljava/lang/String;ZLX/0Px;LX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/434;Lcom/facebook/auth/viewercontext/ViewerContext;ZJLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;FFLX/5Rn;JLcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1336974
    invoke-direct/range {p0 .. p54}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;JJJLjava/lang/String;ZZLcom/facebook/bitmaps/SphericalPhotoMetadata;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;Ljava/lang/String;ZLX/0Px;LX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/434;Lcom/facebook/auth/viewercontext/ViewerContext;ZJLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;FFLX/5Rn;JLcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final D()I
    .locals 1

    .prologue
    .line 1336975
    iget v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    return v0
.end method

.method public final E()LX/434;
    .locals 1

    .prologue
    .line 1336976
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    return-object v0
.end method

.method public final H()J
    .locals 2

    .prologue
    .line 1336977
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    return-wide v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1336989
    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "vault:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;
    .locals 57

    .prologue
    .line 1336978
    new-instance v2, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    new-instance v3, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v4}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    invoke-direct {v3, v4, v6, v7}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    move-wide/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->z:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->G:F

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H:F

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->I:LX/5Rn;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->J:J

    move-wide/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->R:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->U:Z

    move/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->V:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object/from16 v50, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->W:LX/0Px;

    move-object/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    move/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ab:Ljava/lang/String;

    move-object/from16 v56, v0

    invoke-direct/range {v2 .. v56}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;JJJLjava/lang/String;ZZLcom/facebook/bitmaps/SphericalPhotoMetadata;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;Ljava/lang/String;ZLX/0Px;LX/0Px;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/434;Lcom/facebook/auth/viewercontext/ViewerContext;ZJLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;FFLX/5Rn;JLcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1336979
    iput-wide p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    .line 1336980
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1336981
    iput-object p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    .line 1336982
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1336983
    iput-boolean p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->B:Z

    .line 1336984
    return-void
.end method

.method public final ac()Z
    .locals 1

    .prologue
    .line 1336985
    invoke-virtual {p0}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1336986
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1336855
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1336856
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    .line 1336857
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1336987
    iput-boolean p1, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    .line 1336988
    return-void
.end method

.method public final c()Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;
    .locals 1

    .prologue
    .line 1336766
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1336770
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336771
    iget-object p0, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1336772
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1336773
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1336774
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object v0, v0

    .line 1336775
    if-eqz v0, :cond_0

    .line 1336776
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1336777
    if-eqz v1, :cond_0

    .line 1336778
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1336779
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1336780
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1336781
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1336782
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1336767
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1336768
    const/4 v0, 0x0

    .line 1336769
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1336783
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1336784
    const/4 v0, 0x0

    .line 1336785
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1336786
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1336787
    const/4 v0, 0x0

    .line 1336788
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1336789
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1336790
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    return-object v0
.end method

.method public final p()Z
    .locals 5

    .prologue
    .line 1336791
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336792
    iget-wide v1, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->b:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1336793
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1336794
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1336795
    iget-wide v2, v0, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;->b:J

    move-wide v0, v2

    .line 1336796
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 1336797
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    return-wide v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 1336798
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1336799
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->a:Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336800
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336801
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336802
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336803
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336804
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336805
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336806
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->h:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336807
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336808
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336809
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336810
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336811
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->B:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336812
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336813
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->D:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336814
    iget v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->E:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1336815
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->l:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1336816
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1336817
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->n:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336818
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336819
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336820
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336821
    iget v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1336822
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    iget v0, v0, LX/434;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1336823
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->s:LX/434;

    iget v0, v0, LX/434;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1336824
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336825
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336826
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336827
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336828
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->x:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336829
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->w:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336830
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336831
    iget v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->G:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1336832
    iget v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->H:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1336833
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->I:LX/5Rn;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1336834
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->J:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336835
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336836
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336837
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336838
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336839
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1336840
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->P:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336841
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336842
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336843
    iget-wide v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1336844
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336845
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->U:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336846
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->V:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336847
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->W:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1336848
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->X:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336849
    iget-boolean v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Y:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1336850
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->Z:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1336851
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336852
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1336853
    return-void
.end method

.method public final y()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1336854
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->m:LX/0Px;

    return-object v0
.end method
