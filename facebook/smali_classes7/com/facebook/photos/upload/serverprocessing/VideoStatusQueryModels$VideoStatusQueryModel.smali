.class public final Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e61584c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1338015
    const-class v0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1338018
    const-class v0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1338016
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1338017
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1338013
    iget-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->e:Ljava/lang/String;

    .line 1338014
    iget-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1338004
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1338005
    invoke-direct {p0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1338006
    invoke-virtual {p0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1338007
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1338008
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1338009
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1338010
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1338011
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1338012
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1338019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1338020
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1338021
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1338003
    invoke-direct {p0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1338000
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1338001
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->f:I

    .line 1338002
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1337997
    new-instance v0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;-><init>()V

    .line 1337998
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1337999
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1337993
    const v0, -0x4e4f4bfe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1337996
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1337994
    iget-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 1337995
    iget-object v0, p0, Lcom/facebook/photos/upload/serverprocessing/VideoStatusQueryModels$VideoStatusQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    return-object v0
.end method
