.class public Lcom/facebook/photos/upload/operation/TranscodeInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/upload/operation/TranscodeInfoDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;
.end annotation


# static fields
.field public static a:I


# instance fields
.field public flowStartCount:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "flowStartCount"
    .end annotation
.end field

.field public isRequestedServerSettings:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isRequestedServerSettings"
    .end annotation
.end field

.field public isSegmentedTranscode:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isSegmentedTranscode"
    .end annotation
.end field

.field public isServerSettingsAvailable:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isServerSettingsAvailable"
    .end annotation
.end field

.field public isUsingContextualConfig:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isUsingContextualConfig"
    .end annotation
.end field

.field public serverSpecifiedTranscodeBitrate:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "serverSpecifiedTranscodeBitrate"
    .end annotation
.end field

.field public serverSpecifiedTranscodeDimension:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "serverSpecifiedTranscodeDimension"
    .end annotation
.end field

.field public skipBytesThreshold:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "skipBytesThreshold"
    .end annotation
.end field

.field public skipRatioThreshold:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "skipRatioThreshold"
    .end annotation
.end field

.field public transcodeFailCount:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "transcodeFailCount"
    .end annotation
.end field

.field public transcodeStartCount:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "transcodeStartCount"
    .end annotation
.end field

.field public transcodeSuccessCount:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "transcodeSuccessCount"
    .end annotation
.end field

.field public videoCodecResizeInitException:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "videoCodecResizeInitException"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1331671
    const-class v0, Lcom/facebook/photos/upload/operation/TranscodeInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1331672
    const-class v0, Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1331673
    const/4 v0, -0x1

    sput v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->a:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1331674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331675
    return-void
.end method

.method public constructor <init>(JJJJZZZJJZFIZ)V
    .locals 1

    .prologue
    .line 1331676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331677
    iput-wide p1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    .line 1331678
    iput-wide p3, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    .line 1331679
    iput-wide p5, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    .line 1331680
    iput-wide p7, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    .line 1331681
    iput-boolean p9, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    .line 1331682
    iput-boolean p10, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    .line 1331683
    iput-boolean p11, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    .line 1331684
    iput-wide p12, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    .line 1331685
    iput-wide p14, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    .line 1331686
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isUsingContextualConfig:Z

    .line 1331687
    move/from16 v0, p17

    iput v0, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipRatioThreshold:F

    .line 1331688
    move/from16 v0, p18

    iput v0, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipBytesThreshold:I

    .line 1331689
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    .line 1331690
    return-void
.end method
