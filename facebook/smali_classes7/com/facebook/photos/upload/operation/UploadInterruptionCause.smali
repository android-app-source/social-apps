.class public Lcom/facebook/photos/upload/operation/UploadInterruptionCause;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/73y;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadInterruptionCause;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:LX/73x;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1331796
    new-instance v0, LX/8LO;

    invoke-direct {v0}, LX/8LO;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/73z;)V
    .locals 1

    .prologue
    .line 1331781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331782
    iget-boolean v0, p1, LX/73z;->l:Z

    move v0, v0

    .line 1331783
    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    .line 1331784
    iget-boolean v0, p1, LX/73z;->m:Z

    move v0, v0

    .line 1331785
    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    .line 1331786
    iget-object v0, p1, LX/73z;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1331787
    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c:Ljava/lang/String;

    .line 1331788
    iget-object v0, p1, LX/73z;->o:LX/73x;

    move-object v0, v0

    .line 1331789
    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d:LX/73x;

    .line 1331790
    invoke-virtual {p1}, LX/73z;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->e:Ljava/lang/String;

    .line 1331791
    invoke-virtual {p1}, LX/73z;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->f:Ljava/lang/String;

    .line 1331792
    invoke-virtual {p1}, LX/73z;->e()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->g:I

    .line 1331793
    invoke-virtual {p1}, LX/73z;->f()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h:I

    .line 1331794
    invoke-virtual {p1}, LX/73z;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->i:Ljava/lang/String;

    .line 1331795
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1331769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1331770
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    .line 1331771
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    .line 1331772
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c:Ljava/lang/String;

    .line 1331773
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/73x;->valueOf(Ljava/lang/String;)LX/73x;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d:LX/73x;

    .line 1331774
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->e:Ljava/lang/String;

    .line 1331775
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->f:Ljava/lang/String;

    .line 1331776
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->g:I

    .line 1331777
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h:I

    .line 1331778
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->i:Ljava/lang/String;

    .line 1331779
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1331768
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1331767
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1331766
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1331780
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1331765
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1331764
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->g:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1331763
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1331762
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1331751
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1331752
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1331753
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1331754
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1331755
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d:LX/73x;

    invoke-virtual {v0}, LX/73x;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1331756
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1331757
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1331758
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1331759
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1331760
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1331761
    return-void
.end method
