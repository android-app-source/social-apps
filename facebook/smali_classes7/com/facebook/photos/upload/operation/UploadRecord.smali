.class public Lcom/facebook/photos/upload/operation/UploadRecord;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/upload/operation/UploadRecordDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/upload/operation/UploadRecordSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public fbid:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fbid"
    .end annotation
.end field

.field public isRawUpload:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isRawUpload"
    .end annotation
.end field

.field public multimediaInfo:Lcom/facebook/photos/upload/operation/MultimediaInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "multimediaInfo"
    .end annotation
.end field

.field public partitionInfo:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "partitionInfo"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadPartitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public sameHashExist:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sameHashExist"
    .end annotation
.end field

.field public transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "transcodeInfo"
    .end annotation
.end field

.field public uploadTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uploadTime"
    .end annotation
.end field

.field public videoId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "videoId"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1333382
    const-class v0, Lcom/facebook/photos/upload/operation/UploadRecordDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1333355
    const-class v0, Lcom/facebook/photos/upload/operation/UploadRecordSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1333380
    const-class v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    sput-object v0, Lcom/facebook/photos/upload/operation/UploadRecord;->a:Ljava/lang/Class;

    .line 1333381
    new-instance v0, LX/8LY;

    invoke-direct {v0}, LX/8LY;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/operation/UploadRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1333378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333379
    return-void
.end method

.method public constructor <init>(JJZ)V
    .locals 9

    .prologue
    .line 1333376
    const/4 v7, 0x0

    const-string v8, ""

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/facebook/photos/upload/operation/UploadRecord;-><init>(JJZZLjava/lang/String;)V

    .line 1333377
    return-void
.end method

.method public constructor <init>(JJZZLjava/lang/String;)V
    .locals 25

    .prologue
    .line 1333367
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 1333368
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    .line 1333369
    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/facebook/photos/upload/operation/UploadRecord;->uploadTime:J

    .line 1333370
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/photos/upload/operation/UploadRecord;->isRawUpload:Z

    .line 1333371
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/facebook/photos/upload/operation/UploadRecord;->sameHashExist:Z

    .line 1333372
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/facebook/photos/upload/operation/UploadRecord;->videoId:Ljava/lang/String;

    .line 1333373
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    .line 1333374
    new-instance v5, Lcom/facebook/photos/upload/operation/TranscodeInfo;

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, -0x2

    const-wide/16 v19, -0x2

    const/16 v21, 0x0

    const/high16 v22, -0x40000000    # -2.0f

    const/16 v23, -0x2

    const/16 v24, 0x0

    invoke-direct/range {v5 .. v24}, Lcom/facebook/photos/upload/operation/TranscodeInfo;-><init>(JJJJZZZJJZFIZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    .line 1333375
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/MultimediaInfo;)V
    .locals 2

    .prologue
    .line 1333363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333364
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    .line 1333365
    iput-object p1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->multimediaInfo:Lcom/facebook/photos/upload/operation/MultimediaInfo;

    .line 1333366
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;
    .locals 2

    .prologue
    .line 1333362
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-virtual {v0, p0, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1333361
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1333360
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1333356
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadRecord;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1333357
    :goto_0
    return-void

    .line 1333358
    :catch_0
    move-exception v0

    .line 1333359
    sget-object v1, Lcom/facebook/photos/upload/operation/UploadRecord;->a:Ljava/lang/Class;

    const-string v2, "Unable to serialize class to write to parcel"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
