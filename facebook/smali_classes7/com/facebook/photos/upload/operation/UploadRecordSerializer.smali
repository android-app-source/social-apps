.class public Lcom/facebook/photos/upload/operation/UploadRecordSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadRecord;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1333411
    const-class v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    new-instance v1, Lcom/facebook/photos/upload/operation/UploadRecordSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/upload/operation/UploadRecordSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1333412
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1333413
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/operation/UploadRecord;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1333414
    if-nez p0, :cond_0

    .line 1333415
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1333416
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1333417
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/upload/operation/UploadRecordSerializer;->b(Lcom/facebook/photos/upload/operation/UploadRecord;LX/0nX;LX/0my;)V

    .line 1333418
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1333419
    return-void
.end method

.method private static b(Lcom/facebook/photos/upload/operation/UploadRecord;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1333420
    const-string v0, "fbid"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333421
    const-string v0, "uploadTime"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->uploadTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333422
    const-string v0, "isRawUpload"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->isRawUpload:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1333423
    const-string v0, "sameHashExist"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->sameHashExist:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1333424
    const-string v0, "partitionInfo"

    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->partitionInfo:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1333425
    const-string v0, "videoId"

    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->videoId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333426
    const-string v0, "transcodeInfo"

    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->transcodeInfo:Lcom/facebook/photos/upload/operation/TranscodeInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1333427
    const-string v0, "multimediaInfo"

    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadRecord;->multimediaInfo:Lcom/facebook/photos/upload/operation/MultimediaInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1333428
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1333429
    check-cast p1, Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/upload/operation/UploadRecordSerializer;->a(Lcom/facebook/photos/upload/operation/UploadRecord;LX/0nX;LX/0my;)V

    return-void
.end method
