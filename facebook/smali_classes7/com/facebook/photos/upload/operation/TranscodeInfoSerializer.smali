.class public Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/upload/operation/TranscodeInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1331746
    const-class v0, Lcom/facebook/photos/upload/operation/TranscodeInfo;

    new-instance v1, Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1331747
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1331745
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/operation/TranscodeInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1331739
    if-nez p0, :cond_0

    .line 1331740
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1331741
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1331742
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;->b(Lcom/facebook/photos/upload/operation/TranscodeInfo;LX/0nX;LX/0my;)V

    .line 1331743
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1331744
    return-void
.end method

.method private static b(Lcom/facebook/photos/upload/operation/TranscodeInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1331725
    const-string v0, "flowStartCount"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->flowStartCount:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331726
    const-string v0, "transcodeStartCount"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeStartCount:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331727
    const-string v0, "transcodeSuccessCount"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeSuccessCount:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331728
    const-string v0, "transcodeFailCount"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->transcodeFailCount:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331729
    const-string v0, "isSegmentedTranscode"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isSegmentedTranscode:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331730
    const-string v0, "isRequestedServerSettings"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isRequestedServerSettings:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331731
    const-string v0, "isServerSettingsAvailable"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isServerSettingsAvailable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331732
    const-string v0, "serverSpecifiedTranscodeBitrate"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeBitrate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331733
    const-string v0, "serverSpecifiedTranscodeDimension"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->serverSpecifiedTranscodeDimension:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1331734
    const-string v0, "isUsingContextualConfig"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->isUsingContextualConfig:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331735
    const-string v0, "skipRatioThreshold"

    iget v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipRatioThreshold:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1331736
    const-string v0, "skipBytesThreshold"

    iget v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->skipBytesThreshold:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1331737
    const-string v0, "videoCodecResizeInitException"

    iget-boolean v1, p0, Lcom/facebook/photos/upload/operation/TranscodeInfo;->videoCodecResizeInitException:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1331738
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1331724
    check-cast p1, Lcom/facebook/photos/upload/operation/TranscodeInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/upload/operation/TranscodeInfoSerializer;->a(Lcom/facebook/photos/upload/operation/TranscodeInfo;LX/0nX;LX/0my;)V

    return-void
.end method
