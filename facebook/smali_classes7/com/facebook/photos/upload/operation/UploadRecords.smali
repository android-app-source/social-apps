.class public Lcom/facebook/photos/upload/operation/UploadRecords;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadRecords;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1333445
    new-instance v0, LX/8LZ;

    invoke-direct {v0}, LX/8LZ;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/operation/UploadRecords;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1333433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333434
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    .line 1333435
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1333436
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1333437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1333438
    iput-object p1, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    .line 1333439
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333440
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadRecord;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1333441
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecord;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1333442
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1333443
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadRecords;->a:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1333444
    return-void
.end method
