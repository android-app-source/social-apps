.class public Lcom/facebook/photos/upload/operation/UploadOperation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:J

.field public final B:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final C:Z

.field public final D:Z

.field public final E:LX/8LU;

.field public F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

.field public G:Lcom/facebook/photos/upload/operation/UploadRecords;

.field private final H:I

.field public I:Z

.field public final J:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

.field public final K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field public final L:J

.field public final M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final N:Ljava/lang/String;

.field public final O:I

.field public final P:I

.field private Q:Z

.field public R:Z

.field public S:I

.field public T:I

.field public U:I

.field public V:Z

.field public W:Z

.field public final X:I

.field public Y:F

.field public Z:F

.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Lcom/facebook/photos/tagging/store/TagStoreCopy;

.field private ab:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

.field public final ac:Ljava/lang/String;

.field public ad:Z

.field public final ae:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final af:J

.field public final ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final ai:Z

.field public final aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

.field public final ak:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final al:Lcom/facebook/composer/publish/common/EditPostParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final am:Lcom/facebook/audience/model/UploadShot;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final an:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ao:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ap:Z

.field public final aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public final ar:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Z

.field public final at:LX/5Ra;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Z

.field public final av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final aw:Z

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:J

.field public final h:Ljava/lang/String;

.field public final i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

.field public final j:J

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Z

.field public final n:Z

.field public final o:LX/8LR;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:LX/8LS;

.field public final s:Ljava/lang/String;

.field public final t:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final u:J

.field public final v:Z

.field public final w:Lcom/facebook/composer/protocol/PostReviewParams;

.field public final x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

.field public final y:J

.field public final z:LX/5Rn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1332299
    new-instance v0, LX/8LP;

    invoke-direct {v0}, LX/8LP;-><init>()V

    sput-object v0, Lcom/facebook/photos/upload/operation/UploadOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1332420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332421
    sget-object v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aa:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    .line 1332422
    sget-object v0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ab:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    .line 1332423
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    .line 1332424
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    .line 1332425
    if-eqz v0, :cond_1

    .line 1332426
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1332427
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1332428
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    .line 1332429
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    .line 1332430
    const-class v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1332431
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    .line 1332432
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    .line 1332433
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    .line 1332434
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    .line 1332435
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    .line 1332436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    .line 1332437
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    .line 1332438
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    .line 1332439
    const-class v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332440
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8LR;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    .line 1332441
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1332442
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    .line 1332443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    .line 1332444
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8LS;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    .line 1332445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    .line 1332446
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332447
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    .line 1332448
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->v:Z

    .line 1332449
    const-class v0, Lcom/facebook/composer/protocol/PostReviewParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/PostReviewParams;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1332450
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1332451
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    .line 1332452
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1332453
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    .line 1332454
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->C:Z

    .line 1332455
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    .line 1332456
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    .line 1332457
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->J:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1332458
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->X:I

    .line 1332459
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    .line 1332460
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    .line 1332461
    new-instance v0, LX/8LU;

    invoke-direct {v0, p1}, LX/8LU;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332462
    const-class v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332463
    const-class v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadRecords;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1332464
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->H:I

    .line 1332465
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->I:Z

    .line 1332466
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1332467
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->L:J

    .line 1332468
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Q:Z

    .line 1332469
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->V:Z

    .line 1332470
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->W:Z

    .line 1332471
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    .line 1332472
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    .line 1332473
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    .line 1332474
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    .line 1332475
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ae:Ljava/lang/String;

    .line 1332476
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->af:J

    .line 1332477
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ag:Ljava/lang/String;

    .line 1332478
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ah:Ljava/lang/String;

    .line 1332479
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ai:Z

    .line 1332480
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1332481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    .line 1332482
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->O:I

    .line 1332483
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->P:I

    .line 1332484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ak:Ljava/lang/String;

    .line 1332485
    const-class v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1332486
    const-class v0, Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/UploadShot;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    .line 1332487
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->S:I

    .line 1332488
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    .line 1332489
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->U:I

    .line 1332490
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    .line 1332491
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1332492
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1332493
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    .line 1332494
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    .line 1332495
    const-class v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1332496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ar:Ljava/lang/String;

    .line 1332497
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    .line 1332498
    const-class v0, LX/5Ra;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, LX/5Ra;

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->at:LX/5Ra;

    .line 1332499
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    .line 1332500
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->av:Ljava/lang/String;

    .line 1332501
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aw:Z

    .line 1332502
    return-void

    .line 1332503
    :cond_1
    iput-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 1332504
    goto/16 :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/8LR;LX/8LS;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;JZLcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/util/List;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZILcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;Lcom/facebook/audience/model/UploadShot;IIILjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;ZLX/5Ra;ZLjava/lang/String;Z)V
    .locals 7
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p25    # Lcom/facebook/composer/protocol/PostReviewParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p26    # Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p29    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p35    # Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p36    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p42    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p45    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p46    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p49    # Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p50    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p53    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p54    # Lcom/facebook/composer/publish/common/EditPostParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p55    # Lcom/facebook/audience/model/UploadShot;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p59    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p60    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p61    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p64    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p66    # LX/5Ra;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p68    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p69    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "LX/8LR;",
            "LX/8LS;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "JZ",
            "Lcom/facebook/composer/protocol/PostReviewParams;",
            "Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;",
            "J",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "LX/5Rn;",
            "J",
            "Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZI",
            "Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/EditPostParams;",
            "Lcom/facebook/audience/model/UploadShot;",
            "III",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;Z",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Ljava/lang/String;",
            "Z",
            "LX/5Ra;",
            "Z",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1332338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332339
    sget-object v2, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aa:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    .line 1332340
    sget-object v2, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ab:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    .line 1332341
    if-eqz p3, :cond_0

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1332342
    const-wide/16 v2, 0x0

    cmp-long v2, p27, v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1332343
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    .line 1332344
    iput-object p3, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    .line 1332345
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    .line 1332346
    iput-object p5, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    .line 1332347
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/composer/model/MinutiaeTag;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1332348
    iput-object p7, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    .line 1332349
    iput-wide p8, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    .line 1332350
    invoke-static/range {p10 .. p10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    .line 1332351
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    .line 1332352
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    .line 1332353
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    .line 1332354
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    .line 1332355
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    .line 1332356
    invoke-static/range {p17 .. p17}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332357
    invoke-static/range {p18 .. p18}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8LR;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    .line 1332358
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    .line 1332359
    invoke-static/range {p19 .. p19}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8LS;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    .line 1332360
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    .line 1332361
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332362
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    .line 1332363
    move/from16 v0, p24

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->v:Z

    .line 1332364
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1332365
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1332366
    move-wide/from16 v0, p27

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    .line 1332367
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1332368
    move/from16 v0, p30

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->C:Z

    .line 1332369
    move/from16 v0, p31

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    .line 1332370
    invoke-static/range {p32 .. p32}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Rn;

    iput-object v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    .line 1332371
    move-wide/from16 v0, p33

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    .line 1332372
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->J:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    .line 1332373
    move/from16 v0, p48

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->X:I

    .line 1332374
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v2

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    .line 1332375
    new-instance v4, LX/8LU;

    invoke-direct {v4, v2, v3}, LX/8LU;-><init>(J)V

    iput-object v4, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332376
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332377
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1332378
    const v4, -0x543210ee

    iput v4, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->H:I

    .line 1332379
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->I:Z

    .line 1332380
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1332381
    move-wide/from16 v0, p37

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->L:J

    .line 1332382
    if-eqz p39, :cond_1

    invoke-interface/range {p39 .. p39}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p39

    :cond_2
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    .line 1332383
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Q:Z

    .line 1332384
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    .line 1332385
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->V:Z

    .line 1332386
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->W:Z

    .line 1332387
    move/from16 v0, p40

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    .line 1332388
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    .line 1332389
    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ae:Ljava/lang/String;

    .line 1332390
    move-wide/from16 v0, p43

    iput-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->af:J

    .line 1332391
    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ag:Ljava/lang/String;

    .line 1332392
    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ah:Ljava/lang/String;

    .line 1332393
    move/from16 v0, p47

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ai:Z

    .line 1332394
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    .line 1332395
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    .line 1332396
    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1332397
    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    .line 1332398
    move/from16 v0, p51

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->O:I

    .line 1332399
    move/from16 v0, p52

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->P:I

    .line 1332400
    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ak:Ljava/lang/String;

    .line 1332401
    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1332402
    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    .line 1332403
    move/from16 v0, p56

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->S:I

    .line 1332404
    move/from16 v0, p57

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    .line 1332405
    move/from16 v0, p58

    iput v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->U:I

    .line 1332406
    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    .line 1332407
    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1332408
    move-object/from16 v0, p61

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    .line 1332409
    move/from16 v0, p62

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    .line 1332410
    move-object/from16 v0, p63

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1332411
    move-object/from16 v0, p64

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ar:Ljava/lang/String;

    .line 1332412
    move/from16 v0, p65

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    .line 1332413
    move-object/from16 v0, p66

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->at:LX/5Ra;

    .line 1332414
    move/from16 v0, p67

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    .line 1332415
    move-object/from16 v0, p68

    iput-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->av:Ljava/lang/String;

    .line 1332416
    move/from16 v0, p69

    iput-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aw:Z

    .line 1332417
    return-void

    .line 1332418
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1332419
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public synthetic constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/8LR;LX/8LS;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;JZLcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/util/List;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZILcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;Lcom/facebook/audience/model/UploadShot;IIILjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;ZLX/5Ra;ZLjava/lang/String;ZLX/8LP;)V
    .locals 1

    .prologue
    .line 1332337
    invoke-direct/range {p0 .. p69}, Lcom/facebook/photos/upload/operation/UploadOperation;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/8LR;LX/8LS;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;JZLcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;JLcom/facebook/share/model/ComposerAppAttribution;ZZLX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/util/List;ZLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZILcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;Lcom/facebook/audience/model/UploadShot;IIILjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;ZLX/5Ra;ZLjava/lang/String;Z)V

    return-void
.end method

.method public static a(Ljava/io/DataInput;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 6

    .prologue
    .line 1332322
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1332323
    :try_start_0
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 1332324
    if-lez v0, :cond_0

    const/high16 v2, 0x100000

    if-le v0, v2, :cond_1

    .line 1332325
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v2, "invalid operation size"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1332326
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 1332327
    :cond_1
    :try_start_1
    new-array v2, v0, [B

    .line 1332328
    invoke-interface {p0, v2}, Ljava/io/DataInput;->readFully([B)V

    .line 1332329
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 1332330
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 1332331
    sget-object v0, Lcom/facebook/photos/upload/operation/UploadOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1332332
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1332333
    iget v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->H:I

    const v3, -0x543210ee

    if-eq v2, v3, :cond_2

    .line 1332334
    new-instance v0, Ljava/io/IOException;

    const-string v2, "invalid operation"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1332335
    :cond_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1332336
    return-object v0
.end method

.method public static b(Ljava/io/File;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1332300
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 1332301
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-lez v0, :cond_4

    const-wide/32 v2, 0x100000

    cmp-long v0, v4, v2

    if-gez v0, :cond_4

    .line 1332302
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 1332303
    long-to-int v0, v4

    new-array v0, v0, [B

    .line 1332304
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1332305
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    .line 1332306
    int-to-long v8, v6

    cmp-long v4, v8, v4

    if-nez v4, :cond_3

    .line 1332307
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v6}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 1332308
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 1332309
    sget-object v0, Lcom/facebook/photos/upload/operation/UploadOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1332310
    :try_start_2
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1332311
    iget v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->H:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const v5, -0x543210ee

    if-eq v4, v5, :cond_0

    move-object v0, v1

    .line 1332312
    :cond_0
    :goto_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1332313
    :goto_1
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 1332314
    :goto_2
    return-object v0

    .line 1332315
    :catch_0
    move-object v0, v1

    :goto_3
    if-eqz v1, :cond_1

    .line 1332316
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1332317
    :cond_1
    :goto_4
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    .line 1332318
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_2

    .line 1332319
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1332320
    :cond_2
    :goto_6
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0

    :catch_1
    goto :goto_1

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_6

    .line 1332321
    :catchall_1
    move-exception v0

    goto :goto_5

    :catch_4
    move-object v0, v1

    move-object v1, v2

    goto :goto_3

    :catch_5
    move-object v1, v2

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final A()Landroid/os/Bundle;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332126
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1332127
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1332128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332298
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final C()J
    .locals 2

    .prologue
    .line 1332297
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    return-wide v0
.end method

.method public final D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;
    .locals 1

    .prologue
    .line 1332296
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    return-object v0
.end method

.method public final E()J
    .locals 2

    .prologue
    .line 1332295
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    return-wide v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332273
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332293
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 1332292
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    return v0
.end method

.method public final J()LX/8LR;
    .locals 1

    .prologue
    .line 1332291
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    return-object v0
.end method

.method public final K()Lcom/facebook/composer/protocol/PostReviewParams;
    .locals 1

    .prologue
    .line 1332290
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    return-object v0
.end method

.method public final M()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1332289
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332288
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final O()LX/8LS;
    .locals 1

    .prologue
    .line 1332287
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    return-object v0
.end method

.method public final P()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332286
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1332276
    const-string v0, "STORYLINE"

    .line 1332277
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    move-object v1, v1

    .line 1332278
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332279
    const-string v0, "STORYLINE"

    .line 1332280
    :goto_0
    return-object v0

    .line 1332281
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-eq v0, v1, :cond_2

    .line 1332282
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1332283
    :cond_2
    sget-object v1, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1332284
    const-string v0, "ANIMATED_GIFS"

    goto :goto_0

    .line 1332285
    :cond_3
    const-string v0, "CORE_VIDEOS"

    goto :goto_0
.end method

.method public final R()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 1332275
    sget-object v1, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332274
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ak:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Lcom/facebook/composer/publish/common/EditPostParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332507
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    return-object v0
.end method

.method public final U()Lcom/facebook/audience/model/UploadShot;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332546
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    return-object v0
.end method

.method public final V()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332547
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final X()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1

    .prologue
    .line 1332545
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    return-object v0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 1332556
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->C:Z

    return v0
.end method

.method public final Z()Z
    .locals 1

    .prologue
    .line 1332557
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    return v0
.end method

.method public final a()LX/0ck;
    .locals 1

    .prologue
    .line 1332558
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0ck;->VIDEO:LX/0ck;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0ck;->PHOTO:LX/0ck;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 2

    .prologue
    .line 1332559
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332560
    iput-wide p1, v0, LX/8LU;->a:J

    .line 1332561
    iput-wide p1, v0, LX/8LU;->b:J

    .line 1332562
    iput-wide p1, v0, LX/8LU;->c:J

    .line 1332563
    iget v1, v0, LX/8LU;->d:I

    if-nez v1, :cond_0

    iget v1, v0, LX/8LU;->e:I

    if-nez v1, :cond_0

    iget v1, v0, LX/8LU;->f:I

    if-nez v1, :cond_0

    iget v1, v0, LX/8LU;->g:I

    if-nez v1, :cond_0

    iget v1, v0, LX/8LU;->h:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1332564
    return-void

    .line 1332565
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/tagging/store/TagStoreCopy;Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;)V
    .locals 0

    .prologue
    .line 1332566
    iput-object p1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aa:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    .line 1332567
    iput-object p2, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ab:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    .line 1332568
    return-void
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/upload/operation/UploadInterruptionCause;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332569
    iput-object p1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332570
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332571
    iget-boolean p0, v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    move v0, p0

    .line 1332572
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1332573
    :goto_0
    iput-boolean v0, v1, LX/8LU;->j:Z

    .line 1332574
    return-void

    .line 1332575
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadRecords;)V
    .locals 0

    .prologue
    .line 1332576
    iput-object p1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1332577
    return-void
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 3

    .prologue
    .line 1332548
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1332549
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1332550
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 1332551
    array-length v2, v0

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1332552
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1332553
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1332554
    return-void

    .line 1332555
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/io/File;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1332517
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 1332518
    invoke-virtual {p0, v4, v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1332519
    const/4 v3, 0x0

    .line 1332520
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1332521
    :try_start_1
    invoke-virtual {v4}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 1332522
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1332523
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move v2, v1

    .line 1332524
    :goto_0
    if-eqz v2, :cond_0

    .line 1332525
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 1332526
    :cond_0
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 1332527
    :goto_1
    if-nez v2, :cond_4

    :goto_2
    return v0

    .line 1332528
    :catch_0
    move v2, v0

    goto :goto_0

    .line 1332529
    :catch_1
    move-object v2, v3

    .line 1332530
    :goto_3
    if-eqz v2, :cond_1

    .line 1332531
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1332532
    :cond_1
    :goto_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 1332533
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    move v2, v0

    .line 1332534
    goto :goto_1

    .line 1332535
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v3, :cond_2

    .line 1332536
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1332537
    :cond_2
    :goto_6
    if-eqz v1, :cond_3

    .line 1332538
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 1332539
    :cond_3
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    throw v2

    .line 1332540
    :catch_2
    move v1, v0

    goto :goto_6

    :cond_4
    move v0, v1

    .line 1332541
    goto :goto_2

    .line 1332542
    :catch_3
    goto :goto_4

    .line 1332543
    :catchall_1
    move-exception v3

    move-object v5, v3

    move-object v3, v2

    move-object v2, v5

    goto :goto_5

    .line 1332544
    :catch_4
    goto :goto_3
.end method

.method public final aA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332516
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final aB()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332515
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public final aC()J
    .locals 2

    .prologue
    .line 1332506
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->af:J

    return-wide v0
.end method

.method public final aD()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332514
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ag:Ljava/lang/String;

    return-object v0
.end method

.method public final aE()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332513
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public final aF()Z
    .locals 1

    .prologue
    .line 1332512
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ai:Z

    return v0
.end method

.method public final aJ()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332511
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    return-object v0
.end method

.method public final aK()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332510
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    return-object v0
.end method

.method public final aL()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332509
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    return-object v0
.end method

.method public final aM()Z
    .locals 1

    .prologue
    .line 1332508
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    return v0
.end method

.method public final aN()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;
    .locals 1

    .prologue
    .line 1332294
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    return-object v0
.end method

.method public final aO()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332505
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ar:Ljava/lang/String;

    return-object v0
.end method

.method public final aP()LX/5Ra;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332122
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->at:LX/5Ra;

    return-object v0
.end method

.method public final aR()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332121
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->av:Ljava/lang/String;

    return-object v0
.end method

.method public final aS()Z
    .locals 1

    .prologue
    .line 1332120
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aw:Z

    return v0
.end method

.method public final aa()Z
    .locals 2

    .prologue
    .line 1332119
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->MOMENTS_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->COMMENT_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->LIVE_VIDEO:LX/8LS;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->GIF:LX/8LS;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ab()Z
    .locals 2

    .prologue
    .line 1332118
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    sget-object v1, LX/8LR;->MULTIMEDIA:LX/8LR;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    sget-object v1, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ac()Z
    .locals 2

    .prologue
    .line 1332117
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iget-object v1, v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ad()Z
    .locals 2

    .prologue
    .line 1332116
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    sget-object v1, LX/8LR;->SLIDESHOW:LX/8LR;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ae()Z
    .locals 2

    .prologue
    .line 1332115
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    sget-object v1, LX/8LS;->GIF:LX/8LS;

    if-ne v0, v1, :cond_0

    const-string v0, "ANIMATED_GIFS"

    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final af()Z
    .locals 1

    .prologue
    .line 1332114
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ah()Lcom/facebook/ipc/composer/model/MinutiaeTag;
    .locals 1

    .prologue
    .line 1332113
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    return-object v0
.end method

.method public final ai()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332112
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final aj()J
    .locals 2

    .prologue
    .line 1332111
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    return-wide v0
.end method

.method public final ak()J
    .locals 4

    .prologue
    .line 1332091
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332092
    iget-wide v2, v0, LX/8LU;->a:J

    move-wide v0, v2

    .line 1332093
    return-wide v0
.end method

.method public final al()V
    .locals 1

    .prologue
    .line 1332095
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    invoke-virtual {v0}, LX/8LU;->a()V

    .line 1332096
    return-void
.end method

.method public final am()V
    .locals 1

    .prologue
    .line 1332097
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332098
    iget p0, v0, LX/8LU;->d:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, LX/8LU;->d:I

    .line 1332099
    return-void
.end method

.method public final an()LX/5Rn;
    .locals 1

    .prologue
    .line 1332100
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    return-object v0
.end method

.method public final ao()J
    .locals 2

    .prologue
    .line 1332101
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    return-wide v0
.end method

.method public final av()Z
    .locals 1

    .prologue
    .line 1332102
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->W:Z

    return v0
.end method

.method public final ax()Lcom/facebook/photos/tagging/store/TagStoreCopy;
    .locals 1

    .prologue
    .line 1332103
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aa:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    return-object v0
.end method

.method public final ay()Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;
    .locals 1

    .prologue
    .line 1332104
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ab:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    return-object v0
.end method

.method public final az()Z
    .locals 1

    .prologue
    .line 1332105
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1332106
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)V
    .locals 2

    .prologue
    .line 1332107
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332108
    iput-wide p1, v0, LX/8LU;->c:J

    .line 1332109
    iget v1, v0, LX/8LU;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/8LU;->g:I

    .line 1332110
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1332094
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final c(J)V
    .locals 2

    .prologue
    .line 1332230
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332231
    iput-wide p1, v0, LX/8LU;->c:J

    .line 1332232
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 1332271
    iput-boolean p1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Q:Z

    .line 1332272
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1332266
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332267
    iget v1, v0, LX/8LU;->h:I

    move v0, v1

    .line 1332268
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332269
    iget p0, v1, LX/8LU;->g:I

    move v1, p0

    .line 1332270
    add-int/2addr v0, v1

    return v0
.end method

.method public final d(J)V
    .locals 4

    .prologue
    .line 1332256
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    const/4 v3, 0x0

    .line 1332257
    iget v1, v0, LX/8LU;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/8LU;->f:I

    .line 1332258
    iget v1, v0, LX/8LU;->h:I

    iget v2, v0, LX/8LU;->g:I

    add-int/2addr v1, v2

    iput v1, v0, LX/8LU;->h:I

    .line 1332259
    iput v3, v0, LX/8LU;->g:I

    .line 1332260
    invoke-virtual {v0}, LX/8LU;->a()V

    .line 1332261
    iput-wide p1, v0, LX/8LU;->b:J

    .line 1332262
    iput-wide p1, v0, LX/8LU;->c:J

    .line 1332263
    iput v3, v0, LX/8LU;->i:I

    .line 1332264
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8LU;->j:Z

    .line 1332265
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 1332254
    iput-boolean p1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    .line 1332255
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1332253
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1332250
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332251
    iget p0, v0, LX/8LU;->f:I

    move v0, p0

    .line 1332252
    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 1332245
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332246
    iget v1, v0, LX/8LU;->e:I

    move v0, v1

    .line 1332247
    iget-object v1, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332248
    iget p0, v1, LX/8LU;->d:I

    move v1, p0

    .line 1332249
    add-int/2addr v0, v1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1332242
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332243
    iget p0, v0, LX/8LU;->g:I

    move v0, p0

    .line 1332244
    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1332239
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332240
    iget-boolean p0, v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->b:Z

    move v0, p0

    .line 1332241
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1332236
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332237
    iget-boolean p0, v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    move v0, p0

    .line 1332238
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1332233
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    .line 1332234
    iget-boolean p0, v0, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a:Z

    move v0, p0

    .line 1332235
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1332123
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332124
    iget-boolean p0, v0, LX/8LU;->j:Z

    move v0, p0

    .line 1332125
    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1332227
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332228
    iget p0, v0, LX/8LU;->i:I

    move v0, p0

    .line 1332229
    return v0
.end method

.method public final n()J
    .locals 4

    .prologue
    .line 1332224
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332225
    iget-wide v2, v0, LX/8LU;->c:J

    move-wide v0, v2

    .line 1332226
    return-wide v0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 1332221
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332222
    iget p0, v0, LX/8LU;->i:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, LX/8LU;->i:I

    .line 1332223
    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 1332220
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->I:Z

    return v0
.end method

.method public final u()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332219
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    return-object v0
.end method

.method public final v()Lcom/facebook/photos/upload/operation/UploadRecords;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332218
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    return-object v0
.end method

.method public final w()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332217
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1332131
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1332132
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332133
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    if-eqz v0, :cond_0

    .line 1332134
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1332135
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332136
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->e:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332137
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332138
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332139
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332140
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332141
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332142
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332143
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332144
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332145
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332146
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->o:LX/8LR;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1332147
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1332148
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332149
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1332150
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332151
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332152
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->u:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332153
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->v:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332154
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->w:Lcom/facebook/composer/protocol/PostReviewParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332155
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->x:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332156
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->y:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332157
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->B:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332158
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->D:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332159
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->C:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332160
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->z:LX/5Rn;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1332161
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->A:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332162
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->J:Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1332163
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->X:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332164
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1332165
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Z:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1332166
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1332167
    iget-wide v2, v0, LX/8LU;->a:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332168
    iget-wide v2, v0, LX/8LU;->b:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332169
    iget-wide v2, v0, LX/8LU;->c:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332170
    iget v2, v0, LX/8LU;->d:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332171
    iget v2, v0, LX/8LU;->e:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332172
    iget v2, v0, LX/8LU;->f:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332173
    iget v2, v0, LX/8LU;->g:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332174
    iget v2, v0, LX/8LU;->h:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332175
    iget v2, v0, LX/8LU;->i:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332176
    iget-boolean v2, v0, LX/8LU;->j:Z

    invoke-static {p1, v2}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332177
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332178
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332179
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->H:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332180
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332181
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->L:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332182
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->Q:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332183
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->V:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332184
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->W:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332185
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->M:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1332186
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->R:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332187
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332188
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332189
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332190
    iget-wide v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->af:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1332191
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332192
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ah:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332193
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ai:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332194
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aj:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332195
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332196
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->O:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332197
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->P:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332198
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332199
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->al:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332200
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332201
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->S:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332202
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332203
    iget v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->U:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1332204
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332205
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->an:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332206
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ao:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1332207
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ap:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332208
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aq:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332209
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->ar:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332210
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->as:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332211
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->at:LX/5Ra;

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1332212
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332213
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->av:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1332214
    iget-boolean v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->aw:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1332215
    return-void

    .line 1332216
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final y()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1332130
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    return-object v0
.end method

.method public final z()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1332129
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->b:LX/0Px;

    return-object v0
.end method
