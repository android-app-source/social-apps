.class public Lcom/facebook/photos/upload/operation/UploadPartitionInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadPartitionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1333346
    const-class v0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    new-instance v1, Lcom/facebook/photos/upload/operation/UploadPartitionInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/upload/operation/UploadPartitionInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1333347
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1333345
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1333333
    if-nez p0, :cond_0

    .line 1333334
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1333335
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1333336
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/upload/operation/UploadPartitionInfoSerializer;->b(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0nX;LX/0my;)V

    .line 1333337
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1333338
    return-void
.end method

.method private static b(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1333340
    const-string v0, "partitionStartOffset"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionStartOffset:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333341
    const-string v0, "partitionEndOffset"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->partitionEndOffset:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333342
    const-string v0, "chunkedUploadOffset"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333343
    const-string v0, "chunkedUploadChunkLength"

    iget-wide v2, p0, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333344
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1333339
    check-cast p1, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/upload/operation/UploadPartitionInfoSerializer;->a(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;LX/0nX;LX/0my;)V

    return-void
.end method
