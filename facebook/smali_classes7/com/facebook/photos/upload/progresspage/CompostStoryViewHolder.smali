.class public Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final A:LX/0ad;

.field public final B:Landroid/os/Handler;

.field public final C:Z

.field public final D:Ljava/lang/String;

.field public E:Landroid/view/View;

.field public F:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public G:Lcom/facebook/resources/ui/FbTextView;

.field public H:Lcom/facebook/resources/ui/FbTextView;

.field private I:Lcom/facebook/resources/ui/FbTextView;

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8MA;",
            ">;"
        }
    .end annotation
.end field

.field public K:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder$DraftViewHolderListener;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/8K6;

.field public M:Z

.field public N:LX/8MZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Landroid/content/Context;

.field public final m:Landroid/widget/LinearLayout;

.field public final n:LX/8Mb;

.field private final o:LX/0kb;

.field public final p:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final q:LX/11S;

.field public final r:LX/0hy;

.field public final s:Lcom/facebook/content/SecureContextHelper;

.field public final t:LX/1RW;

.field public final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0SG;

.field private final x:LX/0b3;

.field public final y:LX/1EZ;

.field public final z:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1334745
    const-class v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;LX/8Mb;LX/0kb;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/11S;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/1RW;LX/0Ot;LX/0Ot;LX/0SG;LX/0b3;LX/1EZ;LX/03V;LX/0ad;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0Ot;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "LX/8Mb;",
            "LX/0kb;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/11S;",
            "LX/0hy;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1RW;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0SG;",
            "LX/0b3;",
            "LX/1EZ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334746
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1334747
    iput-object p1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    move-object v1, p2

    .line 1334748
    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    .line 1334749
    iput-object p3, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->n:LX/8Mb;

    .line 1334750
    iput-object p4, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->o:LX/0kb;

    .line 1334751
    iput-object p5, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->p:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1334752
    iput-object p6, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->q:LX/11S;

    .line 1334753
    iput-object p7, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->r:LX/0hy;

    .line 1334754
    iput-object p8, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->s:Lcom/facebook/content/SecureContextHelper;

    .line 1334755
    const v1, 0x7f0d2fee

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    .line 1334756
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    const v2, 0x7f0d2fea

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->F:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1334757
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    const v2, 0x7f0d2feb

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    .line 1334758
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    const v2, 0x7f0d2fec

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->H:Lcom/facebook/resources/ui/FbTextView;

    .line 1334759
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    const v2, 0x7f0d2fed    # 1.8767E38f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    .line 1334760
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->J:Ljava/util/List;

    .line 1334761
    iput-object p9, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    .line 1334762
    iput-object p10, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->u:LX/0Ot;

    .line 1334763
    iput-object p11, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->v:LX/0Ot;

    .line 1334764
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->w:LX/0SG;

    .line 1334765
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    .line 1334766
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->K:LX/0am;

    .line 1334767
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B:Landroid/os/Handler;

    .line 1334768
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->F()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->C:Z

    .line 1334769
    iget-boolean v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->C:Z

    if-eqz v1, :cond_0

    .line 1334770
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->D:Ljava/lang/String;

    .line 1334771
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->O:Ljava/lang/Runnable;

    .line 1334772
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->x:LX/0b3;

    .line 1334773
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334774
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->z:LX/03V;

    .line 1334775
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v2, LX/1aO;->aJ:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->M:Z

    .line 1334776
    return-void

    .line 1334777
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->D:Ljava/lang/String;

    goto :goto_0
.end method

.method public static B(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)Z
    .locals 3

    .prologue
    .line 1334778
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v1, LX/1aO;->au:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private F()Z
    .locals 3

    .prologue
    .line 1334779
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v1, LX/1aO;->ax:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private a(LX/8MA;)V
    .locals 1

    .prologue
    .line 1334780
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1334781
    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V
    .locals 3

    .prologue
    .line 1334782
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->q:LX/11S;

    sget-object v2, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {v1, v2, p1, p2}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334783
    return-void
.end method

.method public static a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/2EJ;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 2

    .prologue
    .line 1334784
    new-instance v0, LX/8MQ;

    invoke-direct {v0, p0, p1}, LX/8MQ;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/2EJ;)V

    .line 1334785
    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(LX/8MA;)V

    .line 1334786
    new-instance v1, LX/8MR;

    invoke-direct {v1, p0, v0, p2}, LX/8MR;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8MA;Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {p1, v1}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1334787
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;)V
    .locals 2

    .prologue
    .line 1334788
    new-instance v0, LX/8MB;

    invoke-direct {v0, p0, p1}, LX/8MB;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;)V

    .line 1334789
    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(LX/8MA;)V

    .line 1334790
    new-instance v1, LX/8MM;

    invoke-direct {v1, p0, v0}, LX/8MM;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8MA;)V

    .line 1334791
    iput-object v1, p1, LX/0ht;->H:LX/2dD;

    .line 1334792
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;Z)V
    .locals 2

    .prologue
    .line 1334793
    const v0, 0x7f082061

    invoke-virtual {p2, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1334794
    new-instance v1, LX/8MV;

    invoke-direct {v1, p0, p3, p1}, LX/8MV;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;ZLX/7ml;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334795
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8Ka;I)V
    .locals 5

    .prologue
    .line 1334796
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v1, LX/1aO;->aI:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1334797
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334798
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08206f

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 1334799
    iget v4, p1, LX/8Ka;->a:I

    move v4, v4

    .line 1334800
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1334801
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334802
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    const-string v1, "multi_photos_uploading"

    invoke-virtual {v0, v1}, LX/1RW;->l(Ljava/lang/String;)V

    .line 1334803
    return-void

    .line 1334804
    :cond_0
    const-string v1, "{current}"

    .line 1334805
    iget v2, p1, LX/8Ka;->a:I

    move v2, v2

    .line 1334806
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1334807
    const-string v1, "{total}"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8Kb;)V
    .locals 10

    .prologue
    const-wide/16 v6, 0x400

    .line 1334714
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v1, LX/1aO;->aL:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1334715
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334716
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08206e

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 1334717
    iget-wide v8, p1, LX/8Kb;->a:J

    move-wide v4, v8

    .line 1334718
    div-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1334719
    iget-wide v8, p1, LX/8Kb;->b:J

    move-wide v4, v8

    .line 1334720
    div-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1334721
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334722
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    const-string v1, "single_photo_uploading"

    invoke-virtual {v0, v1}, LX/1RW;->l(Ljava/lang/String;)V

    .line 1334723
    return-void

    .line 1334724
    :cond_0
    const-string v1, "{current}"

    .line 1334725
    iget-wide v8, p1, LX/8Kb;->a:J

    move-wide v2, v8

    .line 1334726
    div-long/2addr v2, v6

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1334727
    const-string v1, "{total}"

    .line 1334728
    iget-wide v8, p1, LX/8Kb;->b:J

    move-wide v2, v8

    .line 1334729
    div-long/2addr v2, v6

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;Ljava/lang/String;LX/7ml;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1334730
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    invoke-virtual {p2}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, LX/7mi;->k()I

    move-result v3

    invoke-virtual {p2}, LX/7mi;->g()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, LX/7mi;->g()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p2}, LX/7mi;->j()I

    move-result v5

    invoke-virtual {p2}, LX/7mi;->f()I

    move-result v6

    .line 1334731
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334732
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    if-nez v1, :cond_2

    move v7, v10

    .line 1334733
    :goto_1
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334734
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    move v8, v11

    .line 1334735
    :goto_2
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334736
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    if-eqz v1, :cond_4

    move v9, v11

    .line 1334737
    :goto_3
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334738
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v1

    if-eqz v1, :cond_0

    move v10, v11

    .line 1334739
    :cond_0
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334740
    invoke-static {v1}, LX/17E;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v11

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;IIIIIZZZZ)V

    .line 1334741
    return-void

    .line 1334742
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 1334743
    :cond_2
    iget-object v1, p2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334744
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    goto :goto_1

    :cond_3
    move v8, v10

    goto :goto_2

    :cond_4
    move v9, v10

    goto :goto_3
.end method

.method private static b(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V
    .locals 11

    .prologue
    .line 1334697
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 1334698
    const-wide/32 v2, 0x5265c00

    div-long v2, v0, v2

    long-to-int v2, v2

    .line 1334699
    long-to-double v4, v0

    const-wide v6, 0x4194997000000000L    # 8.64E7

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    .line 1334700
    const-wide/32 v4, 0x5265c00

    rem-long/2addr v0, v4

    .line 1334701
    const-wide/32 v4, 0x36ee80

    div-long v4, v0, v4

    long-to-int v4, v4

    .line 1334702
    long-to-double v6, v0

    const-wide v8, 0x414b774000000000L    # 3600000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v5, v6

    .line 1334703
    const-wide/32 v6, 0x36ee80

    rem-long/2addr v0, v6

    .line 1334704
    const-wide/32 v6, 0xea60

    div-long v6, v0, v6

    long-to-int v6, v6

    .line 1334705
    long-to-double v0, v0

    const-wide v8, 0x40ed4c0000000000L    # 60000.0

    div-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1334706
    if-lez v2, :cond_0

    .line 1334707
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00f8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334708
    :goto_0
    return-void

    .line 1334709
    :cond_0
    if-lez v4, :cond_1

    .line 1334710
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00f9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1334711
    :cond_1
    if-lez v6, :cond_2

    .line 1334712
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00fa

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1334713
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->I:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    const v2, 0x7f082000

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8MA;)V
    .locals 1

    .prologue
    .line 1334695
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1334696
    return-void
.end method

.method private static c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V
    .locals 1

    .prologue
    .line 1334693
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1334694
    return-void
.end method

.method private static c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V
    .locals 2

    .prologue
    .line 1334691
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    new-instance v1, LX/8M9;

    invoke-direct {v1, p0, p1}, LX/8M9;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1334692
    return-void
.end method

.method public static c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1334684
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v2, LX/1aO;->ar:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334685
    iget-object v1, p1, LX/7ml;->b:LX/7mm;

    move-object v1, v1

    .line 1334686
    sget-object v2, LX/7mm;->POST:LX/7mm;

    if-ne v1, v2, :cond_0

    .line 1334687
    iget-object v1, p1, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v1, v1

    .line 1334688
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1334689
    iget-object v1, p1, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v1, v1

    .line 1334690
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/PublishPostParams;->isCompostDraftable:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static c$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;)V
    .locals 4

    .prologue
    .line 1334680
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v1, LX/1aO;->ao:C

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1334681
    invoke-virtual {p2, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1334682
    new-instance v1, LX/8M1;

    invoke-direct {v1, p0, p1}, LX/8M1;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334683
    return-void
.end method

.method public static d(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)LX/7mj;
    .locals 4

    .prologue
    .line 1334670
    invoke-virtual {p1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v0

    .line 1334671
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334672
    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1334673
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->n:LX/8Mb;

    .line 1334674
    iget-object v3, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1334675
    invoke-virtual {v2, v3}, LX/8Mb;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1334676
    invoke-static {v0, v1}, LX/7mj;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/7mj;

    move-result-object v0

    .line 1334677
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->n:LX/8Mb;

    .line 1334678
    iget-object v2, v1, LX/8Mb;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7mW;

    invoke-virtual {v2, v0}, LX/7mW;->a(LX/7mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1334679
    return-object v0
.end method

.method public static d(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1334660
    if-le p1, v3, :cond_1

    .line 1334661
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v1, LX/1aO;->aH:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1334662
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334663
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082070

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1334664
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334665
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    const-string v1, "multi_photos_waiting "

    invoke-virtual {v0, v1}, LX/1RW;->l(Ljava/lang/String;)V

    .line 1334666
    :goto_1
    return-void

    .line 1334667
    :cond_0
    const-string v1, "{total}"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1334668
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v2, LX/1aO;->aK:C

    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f082071

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334669
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    const-string v1, "single_photo_waiting"

    invoke-virtual {v0, v1}, LX/1RW;->l(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static i(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 1334587
    invoke-virtual {p1}, LX/7mi;->j()I

    move-result v0

    .line 1334588
    invoke-virtual {p1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v1

    .line 1334589
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->p:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1334590
    iget-object v3, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 1334591
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 1334592
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v3

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v3

    iget v3, v3, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v3

    iget-boolean v3, v3, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-nez v3, :cond_e

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 1334593
    move v2, v2

    .line 1334594
    if-eqz v2, :cond_4

    .line 1334595
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334596
    sget-object v2, LX/7mk;->VIDEO:LX/7mk;

    if-ne v1, v2, :cond_0

    .line 1334597
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffd    # 1.809411E38f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1334598
    :goto_1
    return-void

    .line 1334599
    :cond_0
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334600
    sget-object v2, LX/7mk;->TEXT:LX/7mk;

    if-eq v1, v2, :cond_1

    .line 1334601
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334602
    sget-object v2, LX/7mk;->OTHER:LX/7mk;

    if-ne v1, v2, :cond_2

    .line 1334603
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffe

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1

    .line 1334604
    :cond_2
    if-ne v0, v4, :cond_3

    .line 1334605
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00f7

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1334606
    :cond_3
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00f7

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1334607
    :cond_4
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->o:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1334608
    iget-boolean v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->M:Z

    if-eqz v2, :cond_5

    .line 1334609
    const/4 v2, 0x1

    if-le v0, v2, :cond_10

    .line 1334610
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334611
    iget-object v3, v2, LX/1EZ;->C:LX/8Ka;

    move-object v2, v3

    .line 1334612
    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334613
    iget-object v3, v2, LX/1EZ;->C:LX/8Ka;

    move-object v2, v3

    .line 1334614
    iget-object v3, v2, LX/8Ka;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1334615
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1334616
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334617
    iget-object v3, v2, LX/1EZ;->C:LX/8Ka;

    move-object v2, v3

    .line 1334618
    invoke-static {p0, v2, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8Ka;I)V

    .line 1334619
    :goto_2
    new-instance v2, LX/8MZ;

    invoke-direct {v2, p0}, LX/8MZ;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)V

    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->N:LX/8MZ;

    .line 1334620
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->N:LX/8MZ;

    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->x:LX/0b3;

    .line 1334621
    iput-object v3, v2, LX/8MZ;->d:LX/0b3;

    .line 1334622
    const/4 v4, 0x1

    if-le v0, v4, :cond_12

    .line 1334623
    new-instance v4, LX/8MX;

    iget-object v5, v2, LX/8MZ;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-direct {v4, v5, v1, v0}, LX/8MX;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;Ljava/lang/String;I)V

    iput-object v4, v2, LX/8MZ;->c:LX/8KR;

    .line 1334624
    :goto_3
    iget-object v4, v2, LX/8MZ;->d:LX/0b3;

    iget-object v5, v2, LX/8MZ;->c:LX/8KR;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    move-result v4

    iput-boolean v4, v2, LX/8MZ;->b:Z

    .line 1334625
    goto/16 :goto_1

    .line 1334626
    :cond_5
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334627
    sget-object v2, LX/7mk;->VIDEO:LX/7mk;

    if-ne v1, v2, :cond_7

    .line 1334628
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->n:LX/8Mb;

    .line 1334629
    iget-object v1, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334630
    invoke-virtual {v0, v1}, LX/8Mb;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1334631
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081fff

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 1334632
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffa

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 1334633
    :cond_7
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334634
    sget-object v2, LX/7mk;->TEXT:LX/7mk;

    if-eq v1, v2, :cond_8

    .line 1334635
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334636
    sget-object v2, LX/7mk;->OTHER:LX/7mk;

    if-ne v1, v2, :cond_9

    .line 1334637
    :cond_8
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffb

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 1334638
    :cond_9
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00f5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1334639
    :cond_a
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334640
    sget-object v2, LX/7mk;->VIDEO:LX/7mk;

    if-ne v1, v2, :cond_b

    .line 1334641
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffc

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 1334642
    :cond_b
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334643
    sget-object v2, LX/7mk;->TEXT:LX/7mk;

    if-eq v1, v2, :cond_c

    .line 1334644
    iget-object v1, p1, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334645
    sget-object v2, LX/7mk;->OTHER:LX/7mk;

    if-ne v1, v2, :cond_d

    .line 1334646
    :cond_c
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081ffb

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_1

    .line 1334647
    :cond_d
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00f6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1334648
    :cond_f
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->d(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V

    goto/16 :goto_2

    .line 1334649
    :cond_10
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334650
    iget-object v3, v2, LX/1EZ;->B:LX/8Kb;

    move-object v2, v3

    .line 1334651
    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334652
    iget-object v3, v2, LX/1EZ;->B:LX/8Kb;

    move-object v2, v3

    .line 1334653
    iget-object v3, v2, LX/8Kb;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1334654
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1334655
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->y:LX/1EZ;

    .line 1334656
    iget-object v3, v2, LX/1EZ;->B:LX/8Kb;

    move-object v2, v3

    .line 1334657
    invoke-static {p0, v2}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/8Kb;)V

    goto/16 :goto_2

    .line 1334658
    :cond_11
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->d(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V

    goto/16 :goto_2

    .line 1334659
    :cond_12
    new-instance v4, LX/8MY;

    iget-object v5, v2, LX/8MZ;->a:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-direct {v4, v5, v1}, LX/8MY;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;Ljava/lang/String;)V

    iput-object v4, v2, LX/8MZ;->c:LX/8KR;

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(LX/7mi;LX/8K6;)V
    .locals 12

    .prologue
    .line 1334513
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1334514
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1334515
    iput-object p2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->L:LX/8K6;

    .line 1334516
    sget-object v0, LX/8MP;->a:[I

    invoke-virtual {p2}, LX/8K6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1334517
    :goto_0
    iget-object v0, p1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->x(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1334518
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 1334519
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1334520
    :goto_1
    move-object v0, v0

    .line 1334521
    invoke-virtual {p1}, LX/7mi;->f()I

    move-result v1

    if-lez v1, :cond_3

    .line 1334522
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->F:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f020401

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1334523
    :goto_2
    invoke-virtual {p1}, LX/7mi;->g()LX/0am;

    move-result-object v0

    .line 1334524
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1334525
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->H:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1334526
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->H:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334527
    :goto_3
    return-void

    :pswitch_0
    move-object v0, p1

    .line 1334528
    check-cast v0, LX/7ml;

    .line 1334529
    const/4 v3, 0x0

    .line 1334530
    iget-object v1, v0, LX/7ml;->b:LX/7mm;

    move-object v1, v1

    .line 1334531
    sget-object v2, LX/7mm;->POST:LX/7mm;

    if-ne v1, v2, :cond_1

    .line 1334532
    iget-object v1, v0, LX/7ml;->c:LX/7mk;

    move-object v1, v1

    .line 1334533
    sget-object v2, LX/7mk;->TEXT:LX/7mk;

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-short v2, LX/1aO;->V:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1334534
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334535
    :goto_4
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->i(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    .line 1334536
    invoke-virtual {p1}, LX/7mi;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 1334537
    check-cast v0, LX/7mo;

    .line 1334538
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    new-instance v2, LX/8M8;

    invoke-direct {v2, p0, v0}, LX/8M8;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mo;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1334539
    iget-object v1, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334540
    invoke-static {v1}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1334541
    iget-object v1, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1334542
    invoke-static {v1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1334543
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082002

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1334544
    :goto_5
    invoke-virtual {p1}, LX/7mi;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V

    goto/16 :goto_0

    :pswitch_2
    move-object v0, p1

    .line 1334545
    check-cast v0, LX/7mj;

    .line 1334546
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334547
    new-instance v1, LX/5OM;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1334548
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1334549
    const v3, 0x7f082064

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 1334550
    new-instance v4, LX/8M2;

    invoke-direct {v4, p0, v0}, LX/8M2;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334551
    const v3, 0x7f082065

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 1334552
    new-instance v3, LX/8M3;

    invoke-direct {v3, p0, v0}, LX/8M3;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334553
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    new-instance v3, LX/8M4;

    invoke-direct {v3, p0, v1, v0}, LX/8M4;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;LX/7mj;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1334554
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    .line 1334555
    const v0, 0x7f082004

    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V

    .line 1334556
    invoke-virtual {p1}, LX/7mi;->a()J

    move-result-wide v0

    .line 1334557
    iget-object v6, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-wide v8, LX/1aO;->m:J

    const-wide/16 v10, 0x3

    invoke-interface {v6, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    mul-long/2addr v6, v8

    move-wide v2, v6

    .line 1334558
    add-long/2addr v0, v2

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->b(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V

    goto/16 :goto_0

    :pswitch_3
    move-object v0, p1

    .line 1334559
    check-cast v0, LX/7mj;

    .line 1334560
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334561
    new-instance v1, LX/5OM;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1334562
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1334563
    const v3, 0x7f082063

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 1334564
    new-instance v4, LX/8M5;

    invoke-direct {v4, p0, v0}, LX/8M5;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334565
    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v4, LX/1aO;->aE:C

    iget-object v5, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f082066

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1334566
    new-instance v3, LX/8M6;

    invoke-direct {v3, p0, v0}, LX/8M6;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1334567
    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    new-instance v3, LX/8M7;

    invoke-direct {v3, p0, v1, v0}, LX/8M7;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;LX/7mj;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1334568
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7mj;)V

    .line 1334569
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->A:LX/0ad;

    sget-char v3, LX/1aO;->aD:C

    iget-object v4, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f082005

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334570
    iget-wide v6, v0, LX/7mj;->c:J

    move-wide v0, v6

    .line 1334571
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->b(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V

    goto/16 :goto_0

    :pswitch_4
    move-object v0, p1

    .line 1334572
    check-cast v0, LX/7ml;

    .line 1334573
    iget-object v1, v0, LX/7ml;->b:LX/7mm;

    move-object v1, v1

    .line 1334574
    sget-object v2, LX/7mm;->POST:LX/7mm;

    if-eq v1, v2, :cond_9

    .line 1334575
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334576
    :goto_6
    const v0, 0x7f082006

    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;I)V

    .line 1334577
    invoke-virtual {p1}, LX/7mi;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;J)V

    goto/16 :goto_0

    .line 1334578
    :cond_3
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1334579
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->F:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2

    .line 1334580
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->F:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f02191b    # 1.7293E38f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto/16 :goto_1

    .line 1334581
    :cond_6
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->H:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 1334582
    :cond_7
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1334583
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    new-instance v2, LX/8MS;

    invoke-direct {v2, p0, v0}, LX/8MS;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 1334584
    :cond_8
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->G:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082003

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_5

    .line 1334585
    :cond_9
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1334586
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->E:Landroid/view/View;

    new-instance v2, LX/8MT;

    invoke-direct {v2, p0, v0}, LX/8MT;-><init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/8Lo;)V
    .locals 1
    .param p1    # LX/8Lo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1334511
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->K:LX/0am;

    .line 1334512
    return-void
.end method
