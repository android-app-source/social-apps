.class public final Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostNotificationService;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1333760
    iput-object p1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iput-object p2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->a:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, -0x2

    .line 1333761
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    const/16 v3, 0x7d3

    const/16 v4, 0x8

    move v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 1333762
    const/16 v1, 0x50

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1333763
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iget-object v1, v1, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->c:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->a:Landroid/view/ViewGroup;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1333764
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-wide v2, LX/1aO;->m:J

    const-wide/16 v4, 0x3

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1333765
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->a:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iget-object v3, v3, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00fc

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v7}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    const v2, 0x7f08206c

    new-instance v3, LX/8Lm;

    invoke-direct {v3, p0}, LX/8Lm;-><init>(Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;)V

    invoke-virtual {v0, v2, v3}, LX/3oW;->a(ILandroid/view/View$OnClickListener;)LX/3oW;

    move-result-object v0

    new-instance v2, LX/8Ll;

    invoke-direct {v2, p0}, LX/8Ll;-><init>(Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;)V

    .line 1333766
    iput-object v2, v0, LX/3oW;->f:LX/3oV;

    .line 1333767
    move-object v0, v0

    .line 1333768
    iput-object v0, v1, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->n:LX/3oW;

    .line 1333769
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;->b:Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->n:LX/3oW;

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 1333770
    return-void
.end method
