.class public Lcom/facebook/photos/upload/progresspage/CompostNotificationService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/app/NotificationManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1RX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1RW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/12x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/Long;

.field public n:LX/3oW;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1333833
    const-string v0, "CompostNotificationService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1333834
    const-string v0, "NULL_INTENT"

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->l:Ljava/lang/String;

    .line 1333835
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->m:Ljava/lang/Long;

    .line 1333836
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1333771
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1333772
    const-string v1, "notif_operation"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "draft_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "draft_save_time"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "push_notification_title"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "push_notification_text"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1333773
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1333829
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->n:LX/3oW;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->k:LX/1Ml;

    const-string v1, "android.permission.SYSTEM_ALERT_WINDOW"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1333830
    new-instance v0, Landroid/support/design/widget/CoordinatorLayout;

    invoke-direct {v0, p0}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    .line 1333831
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->j:LX/0Sh;

    new-instance v2, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService$1;-><init>(Lcom/facebook/photos/upload/progresspage/CompostNotificationService;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1333832
    :cond_0
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1333827
    iget-object v6, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->i:LX/12x;

    const/4 v7, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long v8, v0, v2

    iget-object v10, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {v10, v11, v0, v1}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v6, v7, v8, v9, v0}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    .line 1333828
    return-void
.end method

.method private static a(Lcom/facebook/photos/upload/progresspage/CompostNotificationService;LX/0ad;Lcom/facebook/content/SecureContextHelper;Landroid/view/WindowManager;Landroid/content/Context;Landroid/app/NotificationManager;LX/1RX;LX/0SG;LX/1RW;LX/12x;LX/0Sh;LX/1Ml;)V
    .locals 0

    .prologue
    .line 1333826
    iput-object p1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    iput-object p2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->c:Landroid/view/WindowManager;

    iput-object p4, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    iput-object p5, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->e:Landroid/app/NotificationManager;

    iput-object p6, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->f:LX/1RX;

    iput-object p7, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->g:LX/0SG;

    iput-object p8, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->h:LX/1RW;

    iput-object p9, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->i:LX/12x;

    iput-object p10, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->j:LX/0Sh;

    iput-object p11, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->k:LX/1Ml;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;

    invoke-static {v11}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v11}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v11}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    const-class v4, Landroid/content/Context;

    invoke-interface {v11, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v11}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    invoke-static {v11}, LX/1RX;->a(LX/0QB;)LX/1RX;

    move-result-object v6

    check-cast v6, LX/1RX;

    invoke-static {v11}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v11}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v8

    check-cast v8, LX/1RW;

    invoke-static {v11}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v9

    check-cast v9, LX/12x;

    invoke-static {v11}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static {v11}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v11

    check-cast v11, LX/1Ml;

    invoke-static/range {v0 .. v11}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Lcom/facebook/photos/upload/progresspage/CompostNotificationService;LX/0ad;Lcom/facebook/content/SecureContextHelper;Landroid/view/WindowManager;Landroid/content/Context;Landroid/app/NotificationManager;LX/1RX;LX/0SG;LX/1RW;LX/12x;LX/0Sh;LX/1Ml;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1333816
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->f:LX/1RX;

    invoke-virtual {v0}, LX/1RX;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1333817
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    const/16 v1, 0x24d6

    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->b()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1333818
    new-instance v1, LX/2HB;

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    if-nez p4, :cond_0

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    const v3, 0x7f08206a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p4

    :cond_0
    invoke-virtual {v1, p4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    const v2, 0x7f0218e4

    invoke-virtual {v1, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    if-nez p5, :cond_1

    iget-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    const v3, 0x7f08206b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p5

    :cond_1
    invoke-virtual {v1, p5}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    .line 1333819
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1333820
    move-object v0, v1

    .line 1333821
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    .line 1333822
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->e:Landroid/app/NotificationManager;

    const-string v2, "CompostNotificationService"

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v4, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1333823
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->h:LX/1RW;

    const-string v3, "push"

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    .line 1333824
    iget-object p0, v0, LX/1RW;->a:LX/0Zb;

    const-string p1, "log_user_notified"

    invoke-static {v0, p1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "notification_operation"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "story_id"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "notification_type"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "client_time"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "draft_save_time"

    invoke-virtual {p1, p2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333825
    :cond_2
    return-void
.end method

.method private b()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1333815
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->d:Landroid/content/Context;

    sget-object v1, LX/8Lr;->DRAFT_PUSH_NOTIFICATION:LX/8Lr;

    invoke-static {v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1333814
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->f:LX/1RX;

    invoke-virtual {v1}, LX/1RX;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-short v2, LX/1aO;->q:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x5d3a7f98

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 1333778
    const-string v0, "push_notification"

    .line 1333779
    const-string v2, "NULL_INTENT"

    .line 1333780
    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->m:Ljava/lang/Long;

    .line 1333781
    if-eqz p1, :cond_b

    .line 1333782
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 1333783
    const-string v1, "notif_operation"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1333784
    const-string v0, "notif_operation"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1333785
    :goto_0
    const-string v0, "draft_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1333786
    const-string v0, "draft_id"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1333787
    :goto_1
    const-string v2, "draft_save_time"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1333788
    const-string v2, "draft_save_time"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "draft_save_time"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1333789
    :goto_3
    const-string v3, "push_notification_title"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1333790
    const-string v3, "push_notification_title"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1333791
    :goto_4
    const-string v5, "push_notification_text"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1333792
    const-string v5, "push_notification_text"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    .line 1333793
    :goto_5
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget v6, LX/1aO;->o:I

    invoke-interface {v0, v6, v10}, LX/0ad;->a(II)I

    move-result v8

    .line 1333794
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget v6, LX/1aO;->t:I

    invoke-interface {v0, v6, v10}, LX/0ad;->a(II)I

    move-result v6

    .line 1333795
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget v9, LX/1aO;->y:I

    invoke-interface {v0, v9, v10}, LX/0ad;->a(II)I

    move-result v9

    .line 1333796
    if-eqz v1, :cond_0

    const-string v0, "push_notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1333797
    :cond_0
    const-string v1, "push_notification"

    .line 1333798
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a()V

    move-object v0, p0

    .line 1333799
    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333800
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v8, :cond_1

    .line 1333801
    const-string v6, "push_notification_reminder_1"

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->p:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->n:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v4, p0

    move v5, v8

    move-object v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v10}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333802
    :cond_1
    :goto_6
    const v0, -0x2de5917e

    invoke-static {v0, v11}, LX/02F;->d(II)V

    return-void

    .line 1333803
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_2

    .line 1333804
    :cond_3
    const-string v0, "push_notification_reminder_1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, p0

    .line 1333805
    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333806
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    .line 1333807
    sub-int v5, v6, v8

    const-string v6, "push_notification_reminder_2"

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->u:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->s:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v4, p0

    move-object v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v10}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1333808
    :cond_4
    const-string v0, "push_notification_reminder_2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, p0

    .line 1333809
    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333810
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v9, :cond_1

    .line 1333811
    add-int v0, v8, v6

    sub-int v5, v9, v0

    const-string v6, "push_notification_reminder_3"

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->z:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a:LX/0ad;

    sget-char v1, LX/1aO;->x:C

    invoke-interface {v0, v1, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v4, p0

    move-object v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v10}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1333812
    :cond_5
    const-string v0, "push_notification_reminder_3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 1333813
    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_6
    move-object v5, v7

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_5

    :cond_7
    move-object v3, v7

    goto/16 :goto_4

    :cond_8
    move-object v2, v3

    goto/16 :goto_3

    :cond_9
    move-object v0, v2

    goto/16 :goto_1

    :cond_a
    move-object v1, v0

    goto/16 :goto_0

    :cond_b
    move-object v5, v7

    move-object v4, v7

    move-object v1, v0

    goto/16 :goto_5
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x6d59fd0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1333774
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1333775
    invoke-static {p0, p0}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1333776
    const v1, 0x7f0e0242

    invoke-virtual {p0, v1}, Lcom/facebook/photos/upload/progresspage/CompostNotificationService;->setTheme(I)V

    .line 1333777
    const/16 v1, 0x25

    const v2, 0x312af83b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
