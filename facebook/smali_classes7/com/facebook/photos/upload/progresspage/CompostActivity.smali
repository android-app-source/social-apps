.class public Lcom/facebook/photos/upload/progresspage/CompostActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/1RW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1333471
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/8Lr;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1333470
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/8Lr;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1333446
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/upload/progresspage/CompostActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "draft_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/8Lr;Ljava/lang/String;)Lcom/facebook/photos/upload/progresspage/CompostFragment;
    .locals 3

    .prologue
    .line 1333460
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1333461
    const-string v0, "source"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1333462
    const-string v0, "draft_id"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1333463
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;

    .line 1333464
    if-eqz v0, :cond_0

    .line 1333465
    :goto_0
    return-object v0

    .line 1333466
    :cond_0
    new-instance v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-direct {v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;-><init>()V

    move-object v0, v0

    .line 1333467
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1333468
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1333469
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->b()Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/upload/progresspage/CompostActivity;

    invoke-static {v0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v0

    check-cast v0, LX/1RW;

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostActivity;->p:LX/1RW;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1333450
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1333451
    const v0, 0x7f030351

    invoke-virtual {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->setContentView(I)V

    .line 1333452
    invoke-virtual {p0}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8Lr;

    .line 1333453
    if-nez v0, :cond_0

    .line 1333454
    sget-object v0, LX/8Lr;->UNKNOWN:LX/8Lr;

    .line 1333455
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "draft_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(LX/8Lr;Ljava/lang/String;)Lcom/facebook/photos/upload/progresspage/CompostFragment;

    .line 1333456
    invoke-static {p0, p0}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1333457
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostActivity;->p:LX/1RW;

    iget-object v0, v0, LX/8Lr;->analyticsName:Ljava/lang/String;

    .line 1333458
    iget-object v2, v1, LX/1RW;->a:LX/0Zb;

    const-string p0, "opening_page"

    invoke-static {v1, p0}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333459
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1333447
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1333448
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostActivity;->p:LX/1RW;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1RW;->a(Z)V

    .line 1333449
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/activity/FbFragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
