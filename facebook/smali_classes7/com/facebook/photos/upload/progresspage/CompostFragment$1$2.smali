.class public final Lcom/facebook/photos/upload/progresspage/CompostFragment$1$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7mi;

.field public final synthetic b:LX/8Lc;


# direct methods
.method public constructor <init>(LX/8Lc;LX/7mi;)V
    .locals 0

    .prologue
    .line 1333519
    iput-object p1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment$1$2;->b:LX/8Lc;

    iput-object p2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment$1$2;->a:LX/7mi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1333492
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment$1$2;->b:LX/8Lc;

    iget-object v0, v0, LX/8Lc;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    iget-object v0, v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment$1$2;->a:LX/7mi;

    .line 1333493
    const/4 v4, 0x0

    .line 1333494
    move v3, v4

    :goto_0
    iget-object v2, v0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    move v5, v4

    .line 1333495
    :goto_1
    iget-object v2, v0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_1

    .line 1333496
    iget-object v2, v0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7mi;

    .line 1333497
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v6, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1333498
    new-instance v2, LX/8Lp;

    add-int/lit8 v4, v5, 0x1

    invoke-direct {v2, v3, v4}, LX/8Lp;-><init>(II)V

    .line 1333499
    :goto_2
    move-object v2, v2

    .line 1333500
    const/4 v6, 0x2

    .line 1333501
    invoke-static {v0, v2}, LX/8Lq;->a(LX/8Lq;LX/8Lp;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1333502
    :goto_3
    return-void

    .line 1333503
    :cond_0
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 1333504
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1333505
    :cond_2
    new-instance v2, LX/8Lp;

    iget-object v3, v0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, LX/8Lp;-><init>(II)V

    goto :goto_2

    .line 1333506
    :cond_3
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    .line 1333507
    iget-object v3, v0, LX/8Lq;->b:Ljava/util/List;

    iget v5, v2, LX/8Lp;->a:I

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget v5, v2, LX/8Lp;->b:I

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1333508
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v3

    .line 1333509
    invoke-static {v0, v2}, LX/8Lq;->b(LX/8Lq;LX/8Lp;)I

    move-result v5

    .line 1333510
    sub-int/2addr v4, v3

    if-ne v4, v6, :cond_5

    .line 1333511
    add-int/lit8 v4, v5, -0x1

    invoke-virtual {v0, v4, v6}, LX/1OM;->d(II)V

    .line 1333512
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/1OM;->i_(I)V

    .line 1333513
    :goto_4
    invoke-virtual {v0, v5, v3}, LX/1OM;->a(II)V

    .line 1333514
    invoke-static {v0}, LX/8Lq;->h(LX/8Lq;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, LX/8Lq;->h:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1333515
    iget-object v3, v0, LX/8Lq;->h:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Ld;

    .line 1333516
    iget-object v0, v3, LX/8Ld;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-static {v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->p(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    .line 1333517
    :cond_4
    goto :goto_3

    .line 1333518
    :cond_5
    invoke-virtual {v0, v5}, LX/1OM;->d(I)V

    goto :goto_4
.end method
