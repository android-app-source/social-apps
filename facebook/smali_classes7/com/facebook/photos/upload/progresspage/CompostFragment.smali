.class public Lcom/facebook/photos/upload/progresspage/CompostFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/1RW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8Mb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8Mg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8M0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/8K3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/8Lq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public o:LX/107;

.field public p:Z

.field public q:Landroid/support/v7/widget/RecyclerView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/8Lr;

.field public t:Ljava/lang/String;

.field private final u:LX/8Lc;

.field private final v:LX/8Ld;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1333677
    const-class v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1333678
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1333679
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1333680
    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->i:LX/0Ot;

    .line 1333681
    new-instance v0, LX/8Lc;

    invoke-direct {v0, p0}, LX/8Lc;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->u:LX/8Lc;

    .line 1333682
    new-instance v0, LX/8Ld;

    invoke-direct {v0, p0}, LX/8Ld;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->v:LX/8Ld;

    return-void
.end method

.method public static a(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V
    .locals 2

    .prologue
    .line 1333683
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 1333684
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1333685
    :cond_0
    :goto_0
    return-void

    .line 1333686
    :cond_1
    if-nez p1, :cond_0

    .line 1333687
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1333688
    const v1, 0x7f0d0b02

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-static {p0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v2

    check-cast v2, LX/1RW;

    invoke-static {p0}, LX/8Mb;->b(LX/0QB;)LX/8Mb;

    move-result-object v3

    check-cast v3, LX/8Mb;

    new-instance v6, LX/8Mg;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v4

    check-cast v4, LX/1Kf;

    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v5

    check-cast v5, LX/1nC;

    invoke-direct {v6, v4, v5}, LX/8Mg;-><init>(LX/1Kf;LX/1nC;)V

    move-object v4, v6

    check-cast v4, LX/8Mg;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {p0}, LX/8M0;->b(LX/0QB;)LX/8M0;

    move-result-object v6

    check-cast v6, LX/8M0;

    invoke-static {p0}, LX/8K3;->b(LX/0QB;)LX/8K3;

    move-result-object v7

    check-cast v7, LX/8K3;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0x19c6

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    new-instance v0, LX/8Lq;

    const-class v10, LX/8Ma;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/8Ma;

    const-class v11, LX/8Lk;

    invoke-interface {p0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/8Lk;

    const-class p1, LX/8Lb;

    invoke-interface {p0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/8Lb;

    invoke-direct {v0, v10, v11, p1}, LX/8Lq;-><init>(LX/8Ma;LX/8Lk;LX/8Lb;)V

    move-object v10, v0

    check-cast v10, LX/8Lq;

    const/16 v11, 0x122d

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v2, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    iput-object v3, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->c:LX/8Mb;

    iput-object v4, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d:LX/8Mg;

    iput-object v5, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->e:LX/1Kf;

    iput-object v6, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    iput-object v7, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->g:LX/8K3;

    iput-object v8, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->i:LX/0Ot;

    iput-object v10, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    iput-object v11, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->k:LX/0Or;

    iput-object p0, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->l:LX/0ad;

    return-void
.end method

.method private static a(LX/8Lr;)Z
    .locals 1

    .prologue
    .line 1333696
    sget-object v0, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION:LX/8Lr;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8Lr;->DRAFT_JEWEL_NOTIFICATION_WITH_DRAFTID:LX/8Lr;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8Lr;->DRAFT_PUSH_NOTIFICATION:LX/8Lr;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8Lr;->SNACKBAR:LX/8Lr;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8Lr;->DRAFT_FEED_ENTRY_POINT:LX/8Lr;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/progresspage/CompostFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1333689
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->g:LX/8K3;

    .line 1333690
    iget-object v1, v0, LX/8K3;->d:LX/8K2;

    move-object v0, v1

    .line 1333691
    sget-object v1, LX/8K2;->CONNECTED:LX/8K2;

    if-ne v0, v1, :cond_0

    const-string v0, "connected"

    .line 1333692
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    .line 1333693
    iget-object v2, v1, LX/1RW;->a:LX/0Zb;

    const-string v3, "internet_status"

    invoke-static {v1, v3}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "status"

    invoke-virtual {v3, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "trigger"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333694
    return-void

    .line 1333695
    :cond_0
    const-string v0, "no_internet"

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1333697
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->v:LX/8Ld;

    invoke-virtual {v0, v1}, LX/8Lq;->a(LX/8Ld;)V

    .line 1333698
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->s:LX/8Lr;

    invoke-static {v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(LX/8Lr;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1333699
    sget-object v0, LX/8K6;->DRAFT_SECTION:LX/8K6;

    sget-object v1, LX/8K6;->SCHEDULED_SECTION:LX/8K6;

    sget-object v2, LX/8K6;->FATAL_SECTION:LX/8K6;

    sget-object v3, LX/8K6;->PENDING_SECTION:LX/8K6;

    sget-object v4, LX/8K6;->UPLOADED_SECTION:LX/8K6;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1333700
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    .line 1333701
    iget-object v2, v1, LX/8Lq;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1333702
    const/4 v2, 0x0

    :goto_1
    iget-object v3, v1, LX/8Lq;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1333703
    iget-object v3, v1, LX/8Lq;->b:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1333704
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1333705
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->l:LX/0ad;

    sget-short v2, LX/1aO;->K:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    const/4 v3, 0x0

    .line 1333706
    move v2, v3

    :goto_2
    invoke-static {}, LX/8K6;->values()[LX/8K6;

    move-result-object v4

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 1333707
    iget-object p0, v0, LX/8Lq;->a:Ljava/util/List;

    sget-object v4, LX/8K6;->PENDING_SECTION:LX/8K6;

    invoke-static {v0, v4}, LX/8Lq;->a(LX/8Lq;LX/8K6;)I

    move-result v4

    if-ne v2, v4, :cond_1

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    :goto_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1333708
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v4, v3

    .line 1333709
    goto :goto_3

    .line 1333710
    :cond_2
    return-void

    .line 1333711
    :cond_3
    sget-object v0, LX/8K6;->FATAL_SECTION:LX/8K6;

    sget-object v1, LX/8K6;->PENDING_SECTION:LX/8K6;

    sget-object v2, LX/8K6;->SCHEDULED_SECTION:LX/8K6;

    sget-object v3, LX/8K6;->UPLOADED_SECTION:LX/8K6;

    sget-object v4, LX/8K6;->DRAFT_SECTION:LX/8K6;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V
    .locals 2

    .prologue
    .line 1333553
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1333554
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->r:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1333555
    :cond_0
    :goto_0
    return-void

    .line 1333556
    :cond_1
    if-nez p1, :cond_0

    .line 1333557
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1333558
    const v1, 0x7f0d0b03

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->r:Landroid/view/View;

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V
    .locals 3

    .prologue
    const/16 v0, 0x8

    .line 1333661
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1333662
    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1333663
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1333664
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1333665
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    .line 1333666
    iget-object v1, v0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1333667
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 1333668
    :cond_0
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1333669
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    const/4 v1, 0x0

    .line 1333670
    iput v1, v0, LX/8M0;->t:I

    .line 1333671
    iput-boolean v1, v0, LX/8M0;->u:Z

    .line 1333672
    sget-object v1, LX/8Ly;->FETCH_PENDING:LX/8Ly;

    iget-object v2, v0, LX/8M0;->d:LX/7ma;

    invoke-virtual {v2}, LX/7ma;->c()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/8M0;->a(LX/8M0;LX/8Ly;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1333673
    sget-object v1, LX/8Ly;->FETCH_UPLOADED:LX/8Ly;

    iget-object v2, v0, LX/8M0;->e:LX/7mf;

    invoke-virtual {v2}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/8M0;->a(LX/8M0;LX/8Ly;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1333674
    sget-object v1, LX/8Ly;->FETCH_DRAFTS:LX/8Ly;

    iget-object v2, v0, LX/8M0;->f:LX/7mW;

    invoke-virtual {v2}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/8M0;->a(LX/8M0;LX/8Ly;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1333675
    sget-object v1, LX/8Ly;->FETCH_FATAL:LX/8Ly;

    iget-object v2, v0, LX/8M0;->g:LX/7mY;

    invoke-virtual {v2}, LX/7mY;->c()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/8M0;->a(LX/8M0;LX/8Ly;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1333676
    return-void
.end method

.method public static m(Lcom/facebook/photos/upload/progresspage/CompostFragment;)Z
    .locals 1

    .prologue
    .line 1333658
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v0}, LX/8M0;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    invoke-virtual {v0}, LX/8M0;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    .line 1333659
    iget-object p0, v0, LX/8M0;->q:Ljava/util/List;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    move-object v0, p0

    .line 1333660
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1333651
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1333652
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1333653
    invoke-static {p0, v1}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1333654
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1333655
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    .line 1333656
    iget-object v1, v0, LX/1RW;->a:LX/0Zb;

    const-string p0, "show_null_state"

    invoke-static {v0, p0}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333657
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1333642
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1333643
    const-class v0, Lcom/facebook/photos/upload/progresspage/CompostFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1333644
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1333645
    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8Lr;

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->s:LX/8Lr;

    .line 1333646
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1333647
    const-string v2, "draft_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->t:Ljava/lang/String;

    .line 1333648
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->l:LX/0ad;

    sget-short v2, LX/1aO;->F:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->s:LX/8Lr;

    invoke-static {v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(LX/8Lr;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->p:Z

    .line 1333649
    return-void

    .line 1333650
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1333621
    if-nez p3, :cond_1

    .line 1333622
    :cond_0
    :goto_0
    return-void

    .line 1333623
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1333624
    :cond_2
    :goto_1
    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1333625
    const-string v0, "try_show_survey_on_result_integration_point_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1333626
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 1333627
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1333628
    move-object v0, v0

    .line 1333629
    sget-object v1, LX/1x2;->LCAU:LX/1x2;

    invoke-virtual {v0, v1}, LX/0gt;->a(LX/1x2;)LX/0gt;

    move-result-object v0

    .line 1333630
    const-string v1, "try_show_survey_on_result_extra_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1333631
    const-string v1, "try_show_survey_on_result_extra_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1333632
    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/os/Bundle;)LX/0gt;

    .line 1333633
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 1333634
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 1333635
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1333636
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1333637
    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 1333638
    :goto_2
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->b:LX/1RW;

    .line 1333639
    iget-object v2, v1, LX/1RW;->a:LX/0Zb;

    const-string v3, "publish_draft"

    invoke-static {v3, v0}, LX/1RW;->f(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "story_id"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "network_connectivity"

    iget-object p2, v1, LX/1RW;->b:LX/0kb;

    invoke-virtual {p2}, LX/0kb;->d()Z

    move-result p2

    invoke-virtual {v3, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1333640
    goto :goto_1

    .line 1333641
    :cond_4
    const-string v0, ""

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a193f97    # 2510821.8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1333592
    const v0, 0x7f030352

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1333593
    const v0, 0x7f0d00bc

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1333594
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v3, 0x7f082007

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1333595
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v3, LX/8Lf;

    invoke-direct {v3, p0}, LX/8Lf;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1333596
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f021839

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1333597
    iput-object v3, v0, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 1333598
    move-object v0, v0

    .line 1333599
    const-string v3, "Entry point for Simplepicker"

    .line 1333600
    iput-object v3, v0, LX/108;->j:Ljava/lang/String;

    .line 1333601
    move-object v0, v0

    .line 1333602
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1333603
    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v3, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1333604
    new-instance v0, LX/8Lg;

    invoke-direct {v0, p0}, LX/8Lg;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->o:LX/107;

    .line 1333605
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->o:LX/107;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1333606
    invoke-direct {p0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->c()V

    .line 1333607
    const v0, 0x7f0d0b04

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1333608
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    iget-object v3, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->u:LX/8Lc;

    .line 1333609
    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, LX/8M0;->s:LX/0am;

    .line 1333610
    const-string v0, "init"

    invoke-static {p0, v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostFragment;Ljava/lang/String;)V

    .line 1333611
    iget-object v0, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->c:LX/8Mb;

    .line 1333612
    iget-object v3, v0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object p0

    .line 1333613
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1333614
    :cond_0
    const/16 v0, 0x2b

    const v3, -0x6caa5cf0

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2

    .line 1333615
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, p1, :cond_0

    invoke-virtual {p0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/common/PendingStory;

    .line 1333616
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    if-eqz p2, :cond_2

    iget-object p2, v0, LX/8Mb;->o:LX/0ad;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p3

    invoke-static {p2, p3}, LX/8Nv;->a(LX/0ad;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 1333617
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object p2

    .line 1333618
    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->d()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v3

    iget-boolean v3, v3, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-eqz v3, :cond_3

    iget-object v3, v0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1EZ;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v3, p3}, LX/1EZ;->f(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1333619
    iget-object v3, v0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    .line 1333620
    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x1baa4f5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1333584
    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1333585
    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->q:Landroid/support/v7/widget/RecyclerView;

    .line 1333586
    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1333587
    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->r:Landroid/view/View;

    .line 1333588
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    invoke-virtual {v1, v2}, LX/8Lq;->a(LX/8Ld;)V

    .line 1333589
    iput-object v2, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    .line 1333590
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1333591
    const/16 v1, 0x2b

    const v2, -0x63bf9732

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x61c3b370

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1333573
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1333574
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    .line 1333575
    iget-object v2, v1, LX/8M0;->f:LX/7mW;

    invoke-virtual {v2}, LX/7mV;->d()V

    .line 1333576
    iget-object v2, v1, LX/8M0;->e:LX/7mf;

    invoke-virtual {v2}, LX/7mV;->d()V

    .line 1333577
    iget-object v2, v1, LX/8M0;->d:LX/7ma;

    invoke-virtual {v2}, LX/7mV;->d()V

    .line 1333578
    iget-object v2, v1, LX/8M0;->g:LX/7mY;

    invoke-virtual {v2}, LX/7mV;->d()V

    .line 1333579
    iget-object v2, v1, LX/8M0;->c:LX/0b3;

    iget-object v4, v1, LX/8M0;->n:LX/8KR;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 1333580
    iget-object v2, v1, LX/8M0;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1333581
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->g:LX/8K3;

    invoke-virtual {v1}, LX/8K3;->c()V

    .line 1333582
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->j:LX/8Lq;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1333583
    const/16 v1, 0x2b

    const v2, -0x1fc2407f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x44b88140

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1333559
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1333560
    invoke-static {p0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->e$redex0(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    .line 1333561
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->f:LX/8M0;

    .line 1333562
    iget-object v2, v1, LX/8M0;->c:LX/0b3;

    iget-object v4, v1, LX/8M0;->n:LX/8KR;

    invoke-virtual {v2, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 1333563
    iget-object v2, v1, LX/8M0;->d:LX/7ma;

    iget-object v4, v1, LX/8M0;->l:LX/7mc;

    .line 1333564
    iput-object v4, v2, LX/7mV;->d:LX/7mc;

    .line 1333565
    iget-object v2, v1, LX/8M0;->e:LX/7mf;

    iget-object v4, v1, LX/8M0;->j:LX/7mc;

    .line 1333566
    iput-object v4, v2, LX/7mV;->d:LX/7mc;

    .line 1333567
    iget-object v2, v1, LX/8M0;->f:LX/7mW;

    iget-object v4, v1, LX/8M0;->k:LX/7mc;

    .line 1333568
    iput-object v4, v2, LX/7mV;->d:LX/7mc;

    .line 1333569
    iget-object v2, v1, LX/8M0;->g:LX/7mY;

    iget-object v4, v1, LX/8M0;->m:LX/7mc;

    .line 1333570
    iput-object v4, v2, LX/7mV;->d:LX/7mc;

    .line 1333571
    iget-object v1, p0, Lcom/facebook/photos/upload/progresspage/CompostFragment;->g:LX/8K3;

    new-instance v2, LX/8Le;

    invoke-direct {v2, p0}, LX/8Le;-><init>(Lcom/facebook/photos/upload/progresspage/CompostFragment;)V

    invoke-virtual {v1, v2}, LX/8K3;->a(LX/8K1;)V

    .line 1333572
    const/16 v1, 0x2b

    const v2, -0x50553f48

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
