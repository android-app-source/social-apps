.class public Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:LX/0Tn;

.field private static final z:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/8KT;

.field private B:LX/8KS;

.field private C:Ljava/lang/String;

.field public D:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public E:Landroid/content/Intent;

.field public F:LX/2EJ;

.field private G:Ljava/lang/Long;

.field public q:LX/1EZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/8KB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1330250
    const-class v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    sput-object v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    .line 1330251
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "upload/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->p:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1330056
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1330057
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1330240
    const-string v0, "upload_options"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330241
    invoke-static {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->b(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    .line 1330242
    :goto_0
    return-void

    .line 1330243
    :cond_0
    const-string v0, "cancel_request"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1330244
    const-string v0, "Upload Dialog Cancel Request"

    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1330245
    :cond_1
    const-string v0, "upload_success"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330246
    invoke-static {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->l(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    goto :goto_0

    .line 1330247
    :cond_2
    const-string v0, "too_slow_request"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1330248
    invoke-direct {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->m()V

    goto :goto_0

    .line 1330249
    :cond_3
    const-string v0, "Upload Dialog Default"

    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(LX/0ju;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1330146
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v4

    .line 1330147
    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    .line 1330148
    iget-object v5, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330149
    iget-object v6, v5, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v5, v6

    .line 1330150
    if-eqz v5, :cond_5

    .line 1330151
    iget-object v0, v5, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->d:LX/73x;

    move-object v0, v0

    .line 1330152
    if-nez v0, :cond_0

    .line 1330153
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    sget-object v6, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "null diagnostic "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330154
    if-eqz v4, :cond_4

    sget-object v0, LX/73x;->TRANSIENT_ERROR:LX/73x;

    .line 1330155
    :cond_0
    :goto_0
    sget-object v6, LX/8KH;->a:[I

    invoke-virtual {v0}, LX/73x;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    .line 1330156
    if-eqz v4, :cond_6

    .line 1330157
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v0, p0}, LX/8KB;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1330158
    :cond_1
    :goto_1
    if-eqz v1, :cond_7

    .line 1330159
    invoke-virtual {p1, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1330160
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f4

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1330161
    if-eqz v2, :cond_2

    .line 1330162
    iget-object v3, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330163
    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1330164
    const v4, 0x7f082048

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1330165
    :goto_3
    move-object v2, v4

    .line 1330166
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1330167
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1330168
    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1330169
    :cond_2
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1330170
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1330171
    :cond_3
    invoke-virtual {p1, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1330172
    return-void

    .line 1330173
    :cond_4
    sget-object v0, LX/73x;->PERMANENT_ERROR:LX/73x;

    goto :goto_0

    .line 1330174
    :cond_5
    iget-object v6, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    sget-object v7, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "null record"

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1330175
    :pswitch_0
    const v3, 0x7f08204a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1330176
    goto :goto_1

    .line 1330177
    :pswitch_1
    const v3, 0x7f08204b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1330178
    goto :goto_1

    .line 1330179
    :pswitch_2
    if-eqz v5, :cond_9

    .line 1330180
    iget-object v0, v5, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1330181
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1330182
    const v5, 0x7f08204c

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-virtual {p0, v5, v6}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1330183
    :goto_4
    if-nez v0, :cond_1

    .line 1330184
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v0, p0}, LX/8KB;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1330185
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v0, p0}, LX/8KB;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1330186
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v0, p0}, LX/8KB;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1330187
    :pswitch_5
    iget-object v3, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v3

    iget-object v5, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v5

    .line 1330188
    if-eqz v5, :cond_d

    .line 1330189
    const v6, 0x7f082052

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1330190
    :goto_5
    move-object v0, v6

    .line 1330191
    goto/16 :goto_1

    .line 1330192
    :pswitch_6
    const v2, 0x7f08204f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 1330193
    move v2, v3

    .line 1330194
    goto/16 :goto_1

    .line 1330195
    :pswitch_7
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330196
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1330197
    const v2, 0x7f08205c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1330198
    :goto_6
    move-object v1, v2

    .line 1330199
    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 1330200
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1330201
    const v5, 0x7f08205f

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "\n\n"

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1330202
    :goto_7
    move-object v0, v5

    .line 1330203
    move v2, v3

    .line 1330204
    goto/16 :goto_1

    .line 1330205
    :pswitch_8
    const v3, 0x7f082053

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1330206
    goto/16 :goto_1

    .line 1330207
    :pswitch_9
    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330208
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1330209
    const v5, 0x7f082057

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1330210
    :goto_8
    move-object v0, v5

    .line 1330211
    move v2, v3

    .line 1330212
    goto/16 :goto_1

    .line 1330213
    :pswitch_a
    const v2, 0x7f082054

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "\n\n"

    aput-object v7, v5, v6

    invoke-virtual {p0, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 1330214
    move v2, v3

    .line 1330215
    goto/16 :goto_1

    .line 1330216
    :cond_7
    if-eqz v4, :cond_8

    .line 1330217
    const v3, 0x7f082041

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0}, LX/8KB;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1330218
    invoke-virtual {p1, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    goto/16 :goto_2

    .line 1330219
    :cond_8
    const v3, 0x7f082042

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0}, LX/8KB;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1330220
    invoke-virtual {p1, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    goto/16 :goto_2

    :cond_9
    move-object v0, v1

    goto/16 :goto_4

    .line 1330221
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1330222
    const v4, 0x7f082047

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 1330223
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_c

    .line 1330224
    const v4, 0x7f082046

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 1330225
    :cond_c
    const v4, 0x7f082049

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 1330226
    :cond_d
    if-eqz v3, :cond_e

    .line 1330227
    const v6, 0x7f082051

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 1330228
    :cond_e
    const v6, 0x7f082050

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 1330229
    :cond_f
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1330230
    const v2, 0x7f08205b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 1330231
    :cond_10
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_11

    .line 1330232
    const v2, 0x7f082059

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 1330233
    :cond_11
    const v2, 0x7f08205a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 1330234
    :cond_12
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v5

    if-ne v5, v6, :cond_13

    .line 1330235
    const v5, 0x7f08205d

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "\n\n"

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_7

    .line 1330236
    :cond_13
    const v5, 0x7f08205e

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "\n\n"

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_7

    .line 1330237
    :cond_14
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1330238
    const v5, 0x7f082056

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_8

    .line 1330239
    :cond_15
    const v5, 0x7f082055

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private static a(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;LX/1EZ;LX/0b3;LX/8KB;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/0SG;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;",
            "LX/1EZ;",
            "LX/0b3;",
            "Lcom/facebook/photos/upload/dialog/UploadDialogConfiguration;",
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1330145
    iput-object p1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->q:LX/1EZ;

    iput-object p2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    iput-object p3, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    iput-object p4, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->t:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->u:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->v:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    iput-object p8, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->x:LX/0SG;

    iput-object p9, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->y:LX/0ad;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-static {v9}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-static {v9}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v2

    check-cast v2, LX/0b3;

    new-instance v4, LX/8KB;

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v3}, LX/8KB;-><init>(LX/0ad;)V

    move-object v3, v4

    check-cast v3, LX/8KB;

    const/16 v4, 0x2ee5

    invoke-static {v9, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf9a

    invoke-static {v9, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e3

    invoke-static {v9, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v9}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static/range {v0 .. v9}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;LX/1EZ;LX/0b3;LX/8KB;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/0SG;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 1330140
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1330141
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v1, 0x1

    .line 1330142
    iput-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->W:Z

    .line 1330143
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->q:LX/1EZ;

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const-string v2, "StopSlowUpload"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1330144
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, -0x1

    .line 1330137
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, p1}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    .line 1330138
    const-string v1, "2.0"

    sget-object v2, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    move v5, v4

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/73w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 1330139
    return-void
.end method

.method public static b(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 8

    .prologue
    const v2, 0x108009b

    .line 1330252
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 1330253
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    sget-object v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "null retry intent"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330254
    invoke-virtual {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->finish()V

    .line 1330255
    :goto_0
    return-void

    .line 1330256
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330257
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v0, v1

    .line 1330258
    if-nez v0, :cond_1

    .line 1330259
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    sget-object v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "no partial record"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330260
    invoke-virtual {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->finish()V

    goto :goto_0

    .line 1330261
    :cond_1
    const-string v0, "upload_options"

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330262
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330263
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    .line 1330264
    iget-object v2, v1, LX/8KB;->a:LX/0ad;

    sget-char v3, LX/8Jz;->R:C

    const v4, 0x7f08203f

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "\n\n"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "\n"

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330265
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1330266
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    .line 1330267
    iget-object v2, v1, LX/8KB;->a:LX/0ad;

    sget-char v3, LX/8Jz;->Q:C

    const v4, 0x7f082044

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330268
    new-instance v2, LX/8KI;

    invoke-direct {v2, p0}, LX/8KI;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330269
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KJ;

    invoke-direct {v2, p0}, LX/8KJ;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330270
    :goto_1
    new-instance v1, LX/8KN;

    invoke-direct {v1, p0}, LX/8KN;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 1330271
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    .line 1330272
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto/16 :goto_0

    .line 1330273
    :cond_2
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    .line 1330274
    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a(LX/0ju;)V

    .line 1330275
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1330276
    const v2, 0x7f082043

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330277
    new-instance v2, LX/8KK;

    invoke-direct {v2, p0}, LX/8KK;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330278
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KL;

    invoke-direct {v2, p0}, LX/8KL;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_1

    .line 1330279
    :cond_3
    const v2, 0x7f082045

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330280
    new-instance v2, LX/8KM;

    invoke-direct {v2, p0}, LX/8KM;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1330121
    const-string v0, "cancel_request"

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330122
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x108009b

    invoke-virtual {v0, v1}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330123
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ae()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1330124
    const v3, 0x7f08203a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1330125
    :goto_0
    move-object v1, v3

    .line 1330126
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KO;

    invoke-direct {v2, p0, p1}, LX/8KO;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 1330127
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KP;

    invoke-direct {v2, p0}, LX/8KP;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330128
    new-instance v1, LX/8KQ;

    invoke-direct {v1, p0}, LX/8KQ;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 1330129
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    .line 1330130
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1330131
    return-void

    .line 1330132
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1330133
    const v3, 0x7f082039

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1330134
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1330135
    iget-object v3, v1, LX/8KB;->a:LX/0ad;

    sget-char v4, LX/8Jz;->N:C

    const v5, 0x7f082038

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1330136
    :cond_2
    iget-object v3, v1, LX/8KB;->a:LX/0ad;

    sget-char v4, LX/8Jz;->S:C

    const v5, 0x7f08203c

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1330110
    if-eqz p1, :cond_0

    .line 1330111
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330112
    const-string v0, "upload_op"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330113
    const-string v0, "retry_intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    .line 1330114
    const-string v0, "eta"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330115
    const-string v0, "eta"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->G:Ljava/lang/Long;

    .line 1330116
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_1

    .line 1330117
    invoke-direct {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a()V

    .line 1330118
    :goto_0
    return-void

    .line 1330119
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->w:LX/03V;

    sget-object v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->z:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "invalid intent"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330120
    invoke-virtual {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->finish()V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 6

    .prologue
    .line 1330100
    const-string v0, "upload_success"

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330101
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1330102
    const v2, 0x7f08203d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, LX/8KB;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330103
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x108009b

    invoke-virtual {v0, v1}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    .line 1330104
    const v2, 0x7f08203e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330105
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1330106
    const v2, 0x7f082040

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1330107
    new-instance v2, LX/8KD;

    invoke-direct {v2, p0}, LX/8KD;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/8KC;

    invoke-direct {v1, p0}, LX/8KC;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    .line 1330108
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1330109
    return-void
.end method

.method private m()V
    .locals 7

    .prologue
    .line 1330090
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->G:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1330091
    const v1, 0x7f08203b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1330092
    const-string v1, "cancel_request"

    iput-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330093
    new-instance v1, LX/0ju;

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v2, p0}, LX/8KB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x108009b

    invoke-virtual {v1, v2}, LX/0ju;->c(I)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1330094
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KE;

    invoke-direct {v2, p0}, LX/8KE;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330095
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->s:LX/8KB;

    invoke-virtual {v1, p0}, LX/8KB;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/8KF;

    invoke-direct {v2, p0}, LX/8KF;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1330096
    new-instance v1, LX/8KG;

    invoke-direct {v1, p0}, LX/8KG;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 1330097
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    .line 1330098
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1330099
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1330084
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 1330085
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    if-eqz v0, :cond_0

    .line 1330086
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1330087
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->F:LX/2EJ;

    .line 1330088
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->c(Landroid/content/Intent;)V

    .line 1330089
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1330067
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1330068
    invoke-static {p0, p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1330069
    new-instance v0, LX/8KT;

    invoke-direct {v0, p0}, LX/8KT;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->A:LX/8KT;

    .line 1330070
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->A:LX/8KT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1330071
    new-instance v0, LX/8KS;

    invoke-direct {v0, p0}, LX/8KS;-><init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->B:LX/8KS;

    .line 1330072
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->B:LX/8KS;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1330073
    const v0, 0x7f03154a

    invoke-virtual {p0, v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->setContentView(I)V

    .line 1330074
    if-eqz p1, :cond_0

    .line 1330075
    const-string v0, "upload_op"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330076
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-nez v0, :cond_1

    .line 1330077
    invoke-virtual {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->c(Landroid/content/Intent;)V

    .line 1330078
    :goto_0
    return-void

    .line 1330079
    :cond_1
    const-string v0, "action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    .line 1330080
    const-string v0, "retry_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    .line 1330081
    const-string v0, "eta"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330082
    const-string v0, "eta"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->G:Ljava/lang/Long;

    .line 1330083
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x21b74dc8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1330062
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1330063
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    if-eqz v1, :cond_0

    .line 1330064
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->A:LX/8KT;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1330065
    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->r:LX/0b3;

    iget-object v2, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->B:LX/8KS;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1330066
    :cond_0
    const/16 v1, 0x23

    const v2, 0x5a142a46

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1330058
    const-string v0, "action"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330059
    const-string v0, "upload_op"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1330060
    const-string v0, "retry_intent"

    iget-object v1, p0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->E:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1330061
    return-void
.end method
