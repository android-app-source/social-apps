.class public final Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8Nw;


# direct methods
.method public constructor <init>(LX/8Nw;)V
    .locals 0

    .prologue
    .line 1338073
    iput-object p1, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1338074
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v1, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v1, v1, LX/8Nw;->k:LX/8OM;

    invoke-interface {v1}, LX/8OM;->b()Z

    move-result v1

    .line 1338075
    iput-boolean v1, v0, LX/8Nw;->l:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338076
    iget-object v0, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v0, v0, LX/8Nw;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1338077
    :goto_0
    return-void

    .line 1338078
    :catch_0
    move-exception v0

    .line 1338079
    :try_start_1
    iget-object v1, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v1, v1, LX/8Nw;->g:LX/03V;

    const-string v2, "MediaUploader.cancel"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338080
    iget-object v0, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v0, v0, LX/8Nw;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;->a:LX/8Nw;

    iget-object v1, v1, LX/8Nw;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
