.class public Lcom/facebook/photos/upload/service/PhotosUploadHelperService;
.super LX/0te;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1338072
    const-class v0, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;

    sput-object v0, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1338071
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1338070
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x24

    const v1, -0x6eebcc73

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1338049
    if-eqz p1, :cond_0

    .line 1338050
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    .line 1338051
    const/4 v2, 0x0

    .line 1338052
    :try_start_0
    const-string v0, "uploadOp"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 1338053
    :goto_0
    if-eqz v1, :cond_0

    .line 1338054
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 1338055
    if-eqz v2, :cond_1

    const-string v0, "com.facebook.photos.upload.service.retry"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1338056
    invoke-static {v4}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v0

    check-cast v0, LX/1EZ;

    sget-object v2, LX/8Kx;->UserRetry:LX/8Kx;

    const-string v4, "User retry"

    invoke-virtual {v0, v1, v2, v4}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 1338057
    const v0, 0x7f082033

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 1338058
    :cond_0
    :goto_1
    invoke-virtual {p0, p3}, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;->stopSelf(I)V

    .line 1338059
    const v0, -0x30964775

    invoke-static {v0, v3}, LX/02F;->d(II)V

    return v8

    .line 1338060
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1338061
    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v5, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "can\'t read operation: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0

    .line 1338062
    :cond_1
    if-eqz v2, :cond_2

    const-string v0, "com.facebook.photos.upload.service.success"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1338063
    const-string v0, "success_result"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1338064
    if-eqz v2, :cond_0

    .line 1338065
    invoke-static {v4}, LX/8L4;->a(LX/0QB;)LX/8L4;

    move-result-object v0

    check-cast v0, LX/8L4;

    .line 1338066
    invoke-virtual {v0, v1, v2}, LX/8L4;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    goto :goto_1

    .line 1338067
    :cond_2
    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v4, Lcom/facebook/photos/upload/service/PhotosUploadHelperService;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid action for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1338068
    iget-object v6, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v6

    .line 1338069
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
