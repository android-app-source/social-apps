.class public Lcom/facebook/photos/imageprocessing/FiltersEngine;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Landroid/graphics/RectF;

.field public static d:Z

.field private static e:Ljava/lang/Throwable;

.field private static g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:Lcom/facebook/photos/imageprocessing/FiltersEngine;


# instance fields
.field private final f:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1321725
    const-class v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a:Ljava/lang/String;

    .line 1321726
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-ApplyToFile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b:Ljava/lang/String;

    .line 1321727
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->c:Landroid/graphics/RectF;

    .line 1321728
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->d:Z

    .line 1321729
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->e:Ljava/lang/Throwable;

    .line 1321730
    :try_start_0
    const-string v0, "fb_creativeediting"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1321731
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->d:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1321732
    :goto_0
    return-void

    .line 1321733
    :catch_0
    move-exception v0

    .line 1321734
    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->e:Ljava/lang/Throwable;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321661
    iput-object p1, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1321662
    sput-object p2, Lcom/facebook/photos/imageprocessing/FiltersEngine;->g:LX/0Ot;

    .line 1321663
    invoke-static {}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b()V

    .line 1321664
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;
    .locals 3

    .prologue
    .line 1321715
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    if-nez v0, :cond_1

    .line 1321716
    const-class v1, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    monitor-enter v1

    .line 1321717
    :try_start_0
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1321718
    if-eqz v2, :cond_0

    .line 1321719
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1321720
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1321721
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1321722
    :cond_1
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    return-object v0

    .line 1321723
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1321724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(F)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1321710
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1321711
    const-string v1, "slider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1321712
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1321713
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1321714
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1321703
    const-string v0, ""

    .line 1321704
    sget-object v1, LX/5iL;->AE08bit:LX/5iL;

    invoke-virtual {v1}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1321705
    const v0, 0x3e4ccccd    # 0.2f

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(F)Ljava/lang/String;

    move-result-object v0

    .line 1321706
    :cond_0
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321707
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321708
    invoke-static {p0, p1, p3, p2, v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->applyAutoEnhanceFilter(JLandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    .line 1321709
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;ILX/5iL;[Landroid/graphics/RectF;II)Z
    .locals 8

    .prologue
    .line 1321697
    const-string v6, ""

    .line 1321698
    sget-object v0, LX/5iL;->AE08bit:LX/5iL;

    if-ne p3, v0, :cond_0

    .line 1321699
    const v0, 0x3e4ccccd    # 0.2f

    invoke-static {v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(F)Ljava/lang/String;

    move-result-object v6

    .line 1321700
    :cond_0
    invoke-virtual {p3}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321701
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321702
    invoke-virtual {p3}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p4

    move v4, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->applyAutoEnhanceFilterToJpegFile(Ljava/lang/String;Ljava/lang/String;I[Landroid/graphics/RectF;ILjava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;)[Landroid/graphics/RectF;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1321677
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1321678
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1321679
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->b:LX/0Px;

    move-object v0, v0

    .line 1321680
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1321681
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->b:LX/0Px;

    move-object v0, v0

    .line 1321682
    new-array v1, v2, [Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    .line 1321683
    :goto_0
    return-object v0

    .line 1321684
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1321685
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1321686
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v4

    .line 1321687
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1321688
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->c:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v4, v0, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1321689
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->b:LX/0Px;

    move-object v6, v0

    .line 1321690
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_3

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1321691
    invoke-virtual {v4, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1321692
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1321693
    invoke-virtual {v5, v8}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1321694
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321695
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1321696
    :cond_3
    new-array v0, v2, [Landroid/graphics/RectF;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    goto :goto_0
.end method

.method private static native applyAutoEnhanceFilter(JLandroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private static native applyAutoEnhanceFilterToJpegFile(Ljava/lang/String;Ljava/lang/String;I[Landroid/graphics/RectF;ILjava/lang/String;Ljava/lang/String;I)Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public static synthetic b(Landroid/graphics/Bitmap;)J
    .locals 2

    .prologue
    .line 1321676
    invoke-static {p0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->init(Landroid/graphics/Bitmap;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;
    .locals 3

    .prologue
    .line 1321674
    new-instance v1, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v2, 0x259

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/facebook/photos/imageprocessing/FiltersEngine;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;)V

    .line 1321675
    return-object v1
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 1321666
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->e:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    .line 1321667
    :goto_0
    return-void

    .line 1321668
    :cond_0
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a:Ljava/lang/String;

    const-string v1, "Failed to load the creative editing library."

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/imageprocessing/FiltersEngine;->e:Ljava/lang/Throwable;

    .line 1321669
    iput-object v1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1321670
    move-object v0, v0

    .line 1321671
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 1321672
    sget-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1321673
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->e:Ljava/lang/Throwable;

    goto :goto_0
.end method

.method private static native init(Landroid/graphics/Bitmap;)J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public static native preprocess([Landroid/graphics/RectF;J)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public static native releaseSession(J)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)LX/8Ht;
    .locals 1

    .prologue
    .line 1321665
    new-instance v0, LX/8Ht;

    invoke-direct {v0, p0, p1}, LX/8Ht;-><init>(Lcom/facebook/photos/imageprocessing/FiltersEngine;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;III)Z
    .locals 7

    .prologue
    .line 1321638
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321639
    iget-object v0, p3, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1321640
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321641
    iget-object v0, p3, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1321642
    invoke-static {v0}, LX/5iB;->d(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1321643
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1321644
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1321645
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321646
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x140011

    sget-object v2, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 1321647
    :cond_0
    invoke-static {p3}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;)[Landroid/graphics/RectF;

    move-result-object v4

    .line 1321648
    iget-object v0, p3, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1321649
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5iL;->getValue(Ljava/lang/String;)LX/5iL;

    move-result-object v3

    move-object v0, p1

    move-object v1, p2

    move v2, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Ljava/lang/String;Ljava/lang/String;ILX/5iL;[Landroid/graphics/RectF;II)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1321650
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1321651
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x140011

    sget-object v2, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 1321652
    :cond_1
    const/4 v0, 0x0

    .line 1321653
    :goto_2
    return v0

    .line 1321654
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1321655
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1321656
    :cond_4
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, LX/2Qx;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1321657
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1321658
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersEngine;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x140011

    sget-object v2, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1321659
    :cond_5
    const/4 v0, 0x1

    goto :goto_2
.end method
