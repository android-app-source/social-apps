.class public Lcom/facebook/photos/imageprocessing/ImageDupeDetector;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;

.field private static final c:LX/1o9;


# instance fields
.field public final d:LX/1HI;

.field private final e:LX/0TD;

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final g:Ljava/lang/Object;

.field public volatile h:F


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x100

    .line 1321959
    const-class v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    const-string v1, "creative_editing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1321960
    const-class v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->b:Ljava/lang/String;

    .line 1321961
    new-instance v0, LX/1o9;

    invoke-direct {v0, v2, v2}, LX/1o9;-><init>(II)V

    sput-object v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->c:LX/1o9;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321953
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    .line 1321954
    const v0, -0x3af52000    # -2222.0f

    iput v0, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->h:F

    .line 1321955
    iput-object p1, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    .line 1321956
    iput-object p2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->e:LX/0TD;

    .line 1321957
    iput-object p3, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1321958
    return-void
.end method

.method public static b(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    .locals 3

    .prologue
    .line 1321948
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x220004

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1321949
    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 1321950
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    const v2, -0xda98d9e

    invoke-static {v0, v2}, LX/02L;->b(Ljava/lang/Object;I)V

    .line 1321951
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized similarityScore(Ljava/lang/String;DIZLjava/lang/String;DIZ)F
    .locals 14
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1321930
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x220004

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1321931
    const v2, -0x3af52000    # -2222.0f

    iput v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->h:F

    .line 1321932
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1321933
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    invoke-static {v5}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->c:LX/1o9;

    invoke-virtual {v3, v4}, LX/1bX;->a(LX/1o9;)LX/1bX;

    move-result-object v3

    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v2

    .line 1321934
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p6

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    .line 1321935
    iget-object v3, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->d:LX/1HI;

    invoke-static {v9}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    sget-object v6, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->c:LX/1o9;

    invoke-virtual {v4, v6}, LX/1bX;->a(LX/1o9;)LX/1bX;

    move-result-object v4

    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    sget-object v6, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v6}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v13

    .line 1321936
    new-instance v3, LX/8Hy;

    if-eqz p5, :cond_0

    const/4 v5, 0x0

    :cond_0
    if-eqz p10, :cond_1

    const/4 v9, 0x0

    :cond_1
    move-object v4, p0

    move-wide/from16 v6, p2

    move/from16 v8, p4

    move-wide/from16 v10, p7

    move/from16 v12, p9

    invoke-direct/range {v3 .. v12}, LX/8Hy;-><init>(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;Landroid/net/Uri;DILandroid/net/Uri;DI)V

    .line 1321937
    const/4 v4, 0x2

    new-array v4, v4, [LX/1ca;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v13, v4, v2

    invoke-static {v4}, LX/4e2;->a([LX/1ca;)LX/4e2;

    move-result-object v4

    .line 1321938
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->e:LX/0TD;

    invoke-virtual {v4, v3, v2}, LX/1cZ;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1321939
    :try_start_1
    iget-object v3, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1321940
    :try_start_2
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->g:Ljava/lang/Object;

    const-wide/16 v6, 0x7d0

    const v5, -0x4e454910

    invoke-static {v2, v6, v7, v5}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 1321941
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1321942
    :try_start_3
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x220004

    const/4 v5, 0x2

    invoke-interface {v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1321943
    :goto_0
    :try_start_4
    invoke-virtual {v4}, LX/1cZ;->g()Z

    .line 1321944
    iget v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->h:F
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return v2

    .line 1321945
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1321946
    :catch_0
    :try_start_7
    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x220004

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 1321947
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method
