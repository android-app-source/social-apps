.class public final Lcom/facebook/photos/imageprocessing/ImageDupeDetector$SimilarityScore;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final SIMILARITY_ERROR:F = -2222.0f
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321926
    const-string v0, "fb_creativeediting"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1321927
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1321928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Landroid/graphics/Bitmap;DILandroid/graphics/Bitmap;DI)F
    .locals 1

    .prologue
    .line 1321929
    invoke-static/range {p0 .. p7}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector$SimilarityScore;->similarityScore(Landroid/graphics/Bitmap;DILandroid/graphics/Bitmap;DI)F

    move-result v0

    return v0
.end method

.method private static native similarityScore(Landroid/graphics/Bitmap;DILandroid/graphics/Bitmap;DI)F
.end method
