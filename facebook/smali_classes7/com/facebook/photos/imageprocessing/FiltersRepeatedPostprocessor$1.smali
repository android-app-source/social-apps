.class public final Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic b:Landroid/graphics/Bitmap;

.field public final synthetic c:[Landroid/graphics/RectF;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/8Hx;


# direct methods
.method public constructor <init>(LX/8Hx;Ljava/util/concurrent/CountDownLatch;Landroid/graphics/Bitmap;[Landroid/graphics/RectF;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1321756
    iput-object p1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    iput-object p2, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->a:Ljava/util/concurrent/CountDownLatch;

    iput-object p3, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->b:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->c:[Landroid/graphics/RectF;

    iput-object p5, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1321757
    monitor-enter p0

    .line 1321758
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    iget-object v0, v0, LX/8Hx;->l:LX/8Hw;

    sget-object v1, LX/8Hw;->PREPROCESSING:LX/8Hw;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    iget-object v0, v0, LX/8Hx;->l:LX/8Hw;

    sget-object v1, LX/8Hw;->APPLYING:LX/8Hw;

    if-eq v0, v1, :cond_0

    .line 1321759
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1321760
    monitor-exit p0

    .line 1321761
    :goto_0
    return-void

    .line 1321762
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1321763
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    iget-object v0, v0, LX/8Hx;->k:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Landroid/graphics/Bitmap;)LX/8Ht;

    move-result-object v0

    .line 1321764
    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->c:[Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, LX/8Ht;->a([Landroid/graphics/RectF;)V

    .line 1321765
    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->b:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/8Ht;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 1321766
    monitor-enter p0

    .line 1321767
    :try_start_1
    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    const/4 v2, 0x1

    .line 1321768
    iput-boolean v2, v1, LX/8Hx;->h:Z

    .line 1321769
    iget-object v1, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v0

    .line 1321770
    iput-object v0, v1, LX/8Hx;->f:LX/1FJ;

    .line 1321771
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    iget-boolean v0, v0, LX/8Hx;->i:Z

    if-nez v0, :cond_1

    .line 1321772
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->e:LX/8Hx;

    invoke-virtual {v0}, LX/8Hx;->d()V

    .line 1321773
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/imageprocessing/FiltersRepeatedPostprocessor$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1321774
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1321775
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
