.class public Lcom/facebook/photos/data/model/FacebookPhoto;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/data/model/FacebookPhotoDeserializer;
.end annotation


# instance fields
.field private mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aid"
    .end annotation
.end field

.field private final mCanTag:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_tag"
    .end annotation
.end field

.field private mCaption:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation
.end field

.field private final mCreated:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "created"
    .end annotation
.end field

.field private final mObjectId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "object_id"
    .end annotation
.end field

.field private final mOwner:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "owner"
    .end annotation
.end field

.field private final mPhotoId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pid"
    .end annotation
.end field

.field private final mPosition:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "position"
    .end annotation
.end field

.field private final mSrcUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src"
    .end annotation
.end field

.field private final mSrcUrlBig:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src_big"
    .end annotation
.end field

.field private final mSrcUrlBigWebp:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src_big_webp"
    .end annotation
.end field

.field private final mSrcUrlSmall:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src_small"
    .end annotation
.end field

.field private final mSrcUrlSmallWebp:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src_small_webp"
    .end annotation
.end field

.field private final mSrcUrlWebp:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src_webp"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1321084
    const-class v0, Lcom/facebook/photos/data/model/FacebookPhotoDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 1321068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321069
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mPhotoId:Ljava/lang/String;

    .line 1321070
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mAlbumId:Ljava/lang/String;

    .line 1321071
    iput-wide v2, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mOwner:J

    .line 1321072
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrl:Ljava/lang/String;

    .line 1321073
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrlBig:Ljava/lang/String;

    .line 1321074
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrlSmall:Ljava/lang/String;

    .line 1321075
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrlWebp:Ljava/lang/String;

    .line 1321076
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrlBigWebp:Ljava/lang/String;

    .line 1321077
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mSrcUrlSmallWebp:Ljava/lang/String;

    .line 1321078
    iput-object v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mCaption:Ljava/lang/String;

    .line 1321079
    iput-wide v2, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mCreated:J

    .line 1321080
    iput-wide v2, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mObjectId:J

    .line 1321081
    iput-wide v2, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mPosition:J

    .line 1321082
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/data/model/FacebookPhoto;->mCanTag:Z

    .line 1321083
    return-void
.end method
