.class public Lcom/facebook/photos/data/model/PhotoPlaceholder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/model/PhotoPlaceholder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321156
    new-instance v0, LX/8HX;

    invoke-direct {v0}, LX/8HX;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1321157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321158
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->a:J

    .line 1321159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->b:I

    .line 1321160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->c:I

    .line 1321161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->d:Ljava/lang/String;

    .line 1321162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->e:Ljava/lang/String;

    .line 1321163
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1321164
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1321165
    iget-wide v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1321166
    iget v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1321167
    iget v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1321168
    iget-object v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321169
    iget-object v0, p0, Lcom/facebook/photos/data/model/PhotoPlaceholder;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321170
    return-void
.end method
