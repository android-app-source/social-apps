.class public final Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final canViewerComment:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_viewer_comment"
    .end annotation
.end field

.field public final canViewerLike:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_viewer_like"
    .end annotation
.end field

.field public final commentCount:LX/8HW;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "comments"
    .end annotation
.end field

.field public final doesViewerLike:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "does_viewer_like"
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final likeCount:LX/8HW;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "likers"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321128
    new-instance v0, LX/8HV;

    invoke-direct {v0}, LX/8HV;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1321129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321130
    iput-object v1, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->id:Ljava/lang/String;

    .line 1321131
    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->doesViewerLike:Z

    .line 1321132
    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerLike:Z

    .line 1321133
    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerComment:Z

    .line 1321134
    iput-object v1, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->likeCount:LX/8HW;

    .line 1321135
    iput-object v1, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->commentCount:LX/8HW;

    .line 1321136
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1321137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->id:Ljava/lang/String;

    .line 1321139
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->doesViewerLike:Z

    .line 1321140
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerLike:Z

    .line 1321141
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerComment:Z

    .line 1321142
    new-instance v0, LX/8HW;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LX/8HW;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->likeCount:LX/8HW;

    .line 1321143
    new-instance v0, LX/8HW;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LX/8HW;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->commentCount:LX/8HW;

    .line 1321144
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1321145
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1321146
    iget-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321147
    iget-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->doesViewerLike:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1321148
    iget-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerLike:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1321149
    iget-boolean v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->canViewerComment:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1321150
    iget-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->likeCount:LX/8HW;

    iget-wide v0, v0, LX/8HW;->count:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1321151
    iget-object v0, p0, Lcom/facebook/photos/data/model/LimitedPhotoFeedbackData;->commentCount:LX/8HW;

    iget-wide v0, v0, LX/8HW;->count:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1321152
    return-void
.end method
