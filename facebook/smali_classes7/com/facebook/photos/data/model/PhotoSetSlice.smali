.class public Lcom/facebook/photos/data/model/PhotoSetSlice;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/model/PhotoSetSlice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Z

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/data/model/PhotoPlaceholder;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321200
    new-instance v0, LX/8HZ;

    invoke-direct {v0}, LX/8HZ;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/model/PhotoSetSlice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1321201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->a:Ljava/lang/String;

    .line 1321203
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->b:J

    .line 1321204
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->c:Z

    .line 1321205
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1321206
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1321207
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->d:LX/0Px;

    .line 1321208
    iget-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->d:LX/0Px;

    invoke-static {v0}, Lcom/facebook/photos/data/model/PhotoSet;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->e:LX/0Px;

    .line 1321209
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1321210
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1321211
    iget-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321212
    iget-wide v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1321213
    iget-boolean v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1321214
    iget-object v0, p0, Lcom/facebook/photos/data/model/PhotoSetSlice;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1321215
    return-void
.end method
