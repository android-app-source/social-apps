.class public Lcom/facebook/photos/data/service/PhotosServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile u:Lcom/facebook/photos/data/service/PhotosServiceHandler;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/8Gt;

.field public final c:LX/8Gr;

.field private final d:LX/8HS;

.field public final e:LX/8H2;

.field public final f:LX/8H7;

.field public final g:LX/8Gz;

.field public final h:LX/8Gp;

.field public final i:LX/8H5;

.field public final j:LX/8HA;

.field public final k:LX/8HJ;

.field public final l:LX/8HI;

.field public final m:LX/8Gx;

.field public final n:LX/5gy;

.field private final o:LX/8Gv;

.field private final p:LX/8HM;

.field public final q:LX/8HC;

.field public final r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/MethodBatcher;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/8Go;

.field public final t:LX/8HR;


# direct methods
.method public constructor <init>(LX/0Or;LX/8Gt;LX/8Gv;LX/8Gr;LX/8HS;LX/8H2;LX/8H7;LX/8Gz;LX/8Gp;LX/8H5;LX/8HA;LX/8HJ;LX/8HI;LX/8Gx;LX/5gy;LX/8HM;LX/8HC;LX/0Or;LX/8Go;LX/8HR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/8Gt;",
            "LX/8Gv;",
            "LX/8Gr;",
            "LX/8HS;",
            "LX/8H2;",
            "LX/8H7;",
            "LX/8Gz;",
            "LX/8Gp;",
            "LX/8H5;",
            "LX/8HA;",
            "LX/8HJ;",
            "LX/8HI;",
            "LX/8Gx;",
            "LX/5gy;",
            "LX/8HM;",
            "LX/8HC;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/MethodBatcher;",
            ">;",
            "LX/8Go;",
            "LX/8HR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321404
    iput-object p1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    .line 1321405
    iput-object p2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->b:LX/8Gt;

    .line 1321406
    iput-object p4, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->c:LX/8Gr;

    .line 1321407
    iput-object p5, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->d:LX/8HS;

    .line 1321408
    iput-object p6, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->e:LX/8H2;

    .line 1321409
    iput-object p7, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->f:LX/8H7;

    .line 1321410
    iput-object p8, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->g:LX/8Gz;

    .line 1321411
    iput-object p9, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->h:LX/8Gp;

    .line 1321412
    iput-object p10, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->i:LX/8H5;

    .line 1321413
    iput-object p11, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->j:LX/8HA;

    .line 1321414
    iput-object p12, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->k:LX/8HJ;

    .line 1321415
    iput-object p13, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->l:LX/8HI;

    .line 1321416
    iput-object p14, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->m:LX/8Gx;

    .line 1321417
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->n:LX/5gy;

    .line 1321418
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->r:LX/0Or;

    .line 1321419
    iput-object p3, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->o:LX/8Gv;

    .line 1321420
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->p:LX/8HM;

    .line 1321421
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->q:LX/8HC;

    .line 1321422
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->s:LX/8Go;

    .line 1321423
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->t:LX/8HR;

    .line 1321424
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/data/service/PhotosServiceHandler;
    .locals 3

    .prologue
    .line 1321393
    sget-object v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->u:Lcom/facebook/photos/data/service/PhotosServiceHandler;

    if-nez v0, :cond_1

    .line 1321394
    const-class v1, Lcom/facebook/photos/data/service/PhotosServiceHandler;

    monitor-enter v1

    .line 1321395
    :try_start_0
    sget-object v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->u:Lcom/facebook/photos/data/service/PhotosServiceHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1321396
    if-eqz v2, :cond_0

    .line 1321397
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/photos/data/service/PhotosServiceHandler;->b(LX/0QB;)Lcom/facebook/photos/data/service/PhotosServiceHandler;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->u:Lcom/facebook/photos/data/service/PhotosServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1321398
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1321399
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1321400
    :cond_1
    sget-object v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->u:Lcom/facebook/photos/data/service/PhotosServiceHandler;

    return-object v0

    .line 1321401
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1321402
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/photos/data/service/PhotosServiceHandler;
    .locals 23

    .prologue
    .line 1321425
    new-instance v2, Lcom/facebook/photos/data/service/PhotosServiceHandler;

    const/16 v3, 0xb83

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/8Gt;->a(LX/0QB;)LX/8Gt;

    move-result-object v4

    check-cast v4, LX/8Gt;

    invoke-static/range {p0 .. p0}, LX/8Gv;->a(LX/0QB;)LX/8Gv;

    move-result-object v5

    check-cast v5, LX/8Gv;

    invoke-static/range {p0 .. p0}, LX/8Gr;->a(LX/0QB;)LX/8Gr;

    move-result-object v6

    check-cast v6, LX/8Gr;

    invoke-static/range {p0 .. p0}, LX/8HS;->a(LX/0QB;)LX/8HS;

    move-result-object v7

    check-cast v7, LX/8HS;

    invoke-static/range {p0 .. p0}, LX/8H2;->a(LX/0QB;)LX/8H2;

    move-result-object v8

    check-cast v8, LX/8H2;

    invoke-static/range {p0 .. p0}, LX/8H7;->a(LX/0QB;)LX/8H7;

    move-result-object v9

    check-cast v9, LX/8H7;

    invoke-static/range {p0 .. p0}, LX/8Gz;->a(LX/0QB;)LX/8Gz;

    move-result-object v10

    check-cast v10, LX/8Gz;

    invoke-static/range {p0 .. p0}, LX/8Gp;->a(LX/0QB;)LX/8Gp;

    move-result-object v11

    check-cast v11, LX/8Gp;

    invoke-static/range {p0 .. p0}, LX/8H5;->a(LX/0QB;)LX/8H5;

    move-result-object v12

    check-cast v12, LX/8H5;

    invoke-static/range {p0 .. p0}, LX/8HA;->a(LX/0QB;)LX/8HA;

    move-result-object v13

    check-cast v13, LX/8HA;

    invoke-static/range {p0 .. p0}, LX/8HJ;->a(LX/0QB;)LX/8HJ;

    move-result-object v14

    check-cast v14, LX/8HJ;

    invoke-static/range {p0 .. p0}, LX/8HI;->a(LX/0QB;)LX/8HI;

    move-result-object v15

    check-cast v15, LX/8HI;

    invoke-static/range {p0 .. p0}, LX/8Gx;->a(LX/0QB;)LX/8Gx;

    move-result-object v16

    check-cast v16, LX/8Gx;

    invoke-static/range {p0 .. p0}, LX/5gy;->a(LX/0QB;)LX/5gy;

    move-result-object v17

    check-cast v17, LX/5gy;

    invoke-static/range {p0 .. p0}, LX/8HM;->a(LX/0QB;)LX/8HM;

    move-result-object v18

    check-cast v18, LX/8HM;

    invoke-static/range {p0 .. p0}, LX/8HC;->a(LX/0QB;)LX/8HC;

    move-result-object v19

    check-cast v19, LX/8HC;

    const/16 v20, 0xb81

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/8Go;->a(LX/0QB;)LX/8Go;

    move-result-object v21

    check-cast v21, LX/8Go;

    invoke-static/range {p0 .. p0}, LX/8HR;->a(LX/0QB;)LX/8HR;

    move-result-object v22

    check-cast v22, LX/8HR;

    invoke-direct/range {v2 .. v22}, Lcom/facebook/photos/data/service/PhotosServiceHandler;-><init>(LX/0Or;LX/8Gt;LX/8Gv;LX/8Gr;LX/8HS;LX/8H2;LX/8H7;LX/8Gz;LX/8Gp;LX/8H5;LX/8HA;LX/8HJ;LX/8HI;LX/8Gx;LX/5gy;LX/8HM;LX/8HC;LX/0Or;LX/8Go;LX/8HR;)V

    .line 1321426
    return-object v2
.end method

.method private d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 17

    .prologue
    .line 1321365
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getBundle()Landroid/os/Bundle;

    move-result-object v11

    .line 1321366
    const-string v1, "updateAlbumParams"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;

    .line 1321367
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/18W;

    invoke-virtual {v1}, LX/18W;->a()LX/2VK;

    move-result-object v12

    .line 1321368
    const-string v13, "change_information"

    .line 1321369
    const-string v14, "make_shared"

    .line 1321370
    const-string v15, "add_contributors"

    .line 1321371
    const-string v16, "delete_contributors"

    .line 1321372
    const-string v1, "IsAlbumTypeShared"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1321373
    iget-object v2, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    sget-object v3, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    if-ne v2, v3, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move v10, v1

    .line 1321374
    :goto_0
    new-instance v1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;

    iget-object v2, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    iget-object v3, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    iget-object v4, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    iget-object v5, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    iget-object v6, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    iget-object v7, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    iget-object v8, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    invoke-direct/range {v1 .. v8}, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8HU;Ljava/lang/Boolean;)V

    .line 1321375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->d:LX/8HS;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v13}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v12, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1321376
    if-eqz v10, :cond_0

    .line 1321377
    new-instance v1, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;

    iget-object v2, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    iget-object v3, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->o:LX/8Gv;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v14}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v13}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v12, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1321379
    :cond_0
    const-string v1, "addContributors"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;

    .line 1321380
    if-eqz v1, :cond_1

    .line 1321381
    if-eqz v10, :cond_4

    .line 1321382
    invoke-static {v1}, LX/8HO;->a(Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;)LX/8HO;

    move-result-object v1

    const-string v2, "{result=%s:id}"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v14, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8HO;->a(Ljava/lang/String;)LX/8HO;

    move-result-object v1

    invoke-virtual {v1}, LX/8HO;->a()Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;

    move-result-object v1

    .line 1321383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->p:LX/8HM;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v15}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v14}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v12, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1321384
    :cond_1
    :goto_1
    const-string v1, "deleteContributors"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;

    .line 1321385
    if-eqz v1, :cond_2

    .line 1321386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->p:LX/8HM;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v13}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v12, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1321387
    :cond_2
    const-string v1, "create_shared_album"

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v12, v1, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1321388
    if-eqz v10, :cond_5

    invoke-interface {v12, v14}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1321389
    :goto_2
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    return-object v1

    .line 1321390
    :cond_3
    const/4 v1, 0x0

    move v10, v1

    goto/16 :goto_0

    .line 1321391
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->p:LX/8HM;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v15}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1, v13}, LX/2Vk;->b(Ljava/lang/String;)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v12, v1}, LX/2VK;->a(LX/2Vj;)V

    goto :goto_1

    .line 1321392
    :cond_5
    iget-object v1, v9, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 1321216
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1321217
    const-string v1, "photos_create_album"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1321218
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321219
    const-string v1, "createAlbumParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/CreatePhotoAlbumParams;

    .line 1321220
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->b:LX/8Gt;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321221
    :goto_0
    return-object v0

    .line 1321222
    :cond_0
    const-string v1, "create_page_album"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1321223
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 1321224
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v5, v4

    .line 1321225
    iget-object v4, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->r:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/18W;

    invoke-virtual {v4}, LX/18W;->a()LX/2VK;

    move-result-object v13

    .line 1321226
    const-string v4, "createPageAlbumParams"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;

    .line 1321227
    iget-object v5, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->c:LX/8Gr;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v5, "create-page-album"

    .line 1321228
    iput-object v5, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 1321229
    move-object v4, v4

    .line 1321230
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v13, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1321231
    new-instance v4, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;

    const-string v5, "{result=create-page-album:$.id}"

    move-object v7, v6

    move v9, v8

    move v10, v8

    move v11, v8

    move v12, v8

    invoke-direct/range {v4 .. v12}, Lcom/facebook/photos/albums/protocols/FetchSingleAlbumParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIII)V

    .line 1321232
    iget-object v5, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->n:LX/5gy;

    invoke-static {v5, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v5, "fetch-album"

    .line 1321233
    iput-object v5, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 1321234
    move-object v4, v4

    .line 1321235
    const-string v5, "create-page-album"

    .line 1321236
    iput-object v5, v4, LX/2Vk;->d:Ljava/lang/String;

    .line 1321237
    move-object v4, v4

    .line 1321238
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v13, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 1321239
    const-string v4, "create-page-album"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-interface {v13, v4, v5}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1321240
    const-string v4, "fetch-album"

    invoke-interface {v13, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1321241
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    move-object v0, v4

    .line 1321242
    goto :goto_0

    .line 1321243
    :cond_1
    const-string v1, "create_page_album_for_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1321244
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321245
    const-string v1, "createPageAlbumParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;

    .line 1321246
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->c:LX/8Gr;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1321247
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321248
    goto/16 :goto_0

    .line 1321249
    :cond_2
    const-string v1, "photos_update_album"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1321250
    invoke-direct {p0, p1}, Lcom/facebook/photos/data/service/PhotosServiceHandler;->d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 1321251
    :cond_3
    const-string v1, "delete_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1321252
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321253
    const-string v1, "deletePhotoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoParams;

    .line 1321254
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->e:LX/8H2;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoResponse;

    .line 1321255
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321256
    goto/16 :goto_0

    .line 1321257
    :cond_4
    const-string v1, "delete_video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1321258
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321259
    const-string v1, "deleteVideoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeleteVideoParams;

    .line 1321260
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->f:LX/8H7;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeleteVideoResponse;

    .line 1321261
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321262
    goto/16 :goto_0

    .line 1321263
    :cond_5
    const-string v1, "delete_photo_album"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1321264
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321265
    const-string v1, "deletePhotoAlbumParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;

    .line 1321266
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->g:LX/8Gz;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;

    .line 1321267
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321268
    goto/16 :goto_0

    .line 1321269
    :cond_6
    const-string v1, "add_photo_tag"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1321270
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321271
    const-string v1, "addPhotoTagParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/AddPhotoTagParams;

    .line 1321272
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->h:LX/8Gp;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321273
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321274
    move-object v0, v0

    .line 1321275
    goto/16 :goto_0

    .line 1321276
    :cond_7
    const-string v1, "delete_photo_tag"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1321277
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321278
    const-string v1, "deletePhotoTagParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;

    .line 1321279
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->i:LX/8H5;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321280
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321281
    move-object v0, v0

    .line 1321282
    goto/16 :goto_0

    .line 1321283
    :cond_8
    const-string v1, "edit_photo_caption"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1321284
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321285
    const-string v1, "editPhotoCaptionParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;

    .line 1321286
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->j:LX/8HA;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321287
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321288
    move-object v0, v0

    .line 1321289
    goto/16 :goto_0

    .line 1321290
    :cond_9
    const-string v1, "fetch_photos_metadata"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1321291
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321292
    const-string v1, "fetchPhotosMetadataParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1321293
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->k:LX/8HJ;

    .line 1321294
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1321295
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321296
    goto/16 :goto_0

    .line 1321297
    :cond_a
    const-string v1, "fetch_photos_extra_logging_metadata"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1321298
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321299
    const-string v1, "fetchPhotosMetadataParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    .line 1321300
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->l:LX/8HI;

    .line 1321301
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1321302
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1321303
    goto/16 :goto_0

    .line 1321304
    :cond_b
    const-string v1, "crop_profile_picture"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1321305
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321306
    const-string v1, "cropProfilePictureParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/CropProfilePictureParams;

    .line 1321307
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->m:LX/8Gx;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321308
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321309
    move-object v0, v0

    .line 1321310
    goto/16 :goto_0

    .line 1321311
    :cond_c
    const-string v1, "edit_photo_location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1321312
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321313
    const-string v1, "editPhotoLocationParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;

    .line 1321314
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->q:LX/8HC;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321315
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321316
    move-object v0, v0

    .line 1321317
    goto/16 :goto_0

    .line 1321318
    :cond_d
    const-string v1, "accept_place_suggestion"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1321319
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321320
    const-string v1, "placeSuggestionMutationParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;

    .line 1321321
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->s:LX/8Go;

    .line 1321322
    new-instance v2, LX/4K2;

    invoke-direct {v2}, LX/4K2;-><init>()V

    .line 1321323
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1321324
    const-string p1, "photo_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321325
    move-object v2, v2

    .line 1321326
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1321327
    const-string p1, "place_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321328
    move-object v2, v2

    .line 1321329
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->c:Ljava/lang/String;

    move-object p0, p0

    .line 1321330
    const-string p1, "session_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321331
    move-object v2, v2

    .line 1321332
    iget-object p0, v1, LX/8Go;->a:Ljava/lang/String;

    .line 1321333
    const-string p1, "actor_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321334
    move-object v2, v2

    .line 1321335
    new-instance p0, LX/5k1;

    invoke-direct {p0}, LX/5k1;-><init>()V

    move-object p0, p0

    .line 1321336
    const-string p1, "input"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1321337
    iget-object v2, v1, LX/8Go;->b:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1321338
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321339
    move-object v0, v0

    .line 1321340
    goto/16 :goto_0

    .line 1321341
    :cond_e
    const-string v1, "reject_place_suggestion"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1321342
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1321343
    const-string v1, "placeSuggestionMutationParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;

    .line 1321344
    iget-object v1, p0, Lcom/facebook/photos/data/service/PhotosServiceHandler;->t:LX/8HR;

    .line 1321345
    new-instance v2, LX/4KA;

    invoke-direct {v2}, LX/4KA;-><init>()V

    .line 1321346
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1321347
    const-string p1, "photo_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321348
    move-object v2, v2

    .line 1321349
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1321350
    const-string p1, "place_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321351
    move-object v2, v2

    .line 1321352
    iget-object p0, v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->c:Ljava/lang/String;

    move-object p0, p0

    .line 1321353
    const-string p1, "session_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321354
    move-object v2, v2

    .line 1321355
    iget-object p0, v1, LX/8HR;->a:Ljava/lang/String;

    .line 1321356
    const-string p1, "actor_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321357
    move-object v2, v2

    .line 1321358
    new-instance p0, LX/5k2;

    invoke-direct {p0}, LX/5k2;-><init>()V

    move-object p0, p0

    .line 1321359
    const-string p1, "input"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1321360
    iget-object v2, v1, LX/8HR;->b:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1321361
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1321362
    move-object v0, v0

    .line 1321363
    goto/16 :goto_0

    .line 1321364
    :cond_f
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported operation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
