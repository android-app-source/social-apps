.class public Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/8HU;

.field public final g:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1321058
    new-instance v0, LX/8HT;

    invoke-direct {v0}, LX/8HT;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1321048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321049
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    .line 1321050
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    .line 1321051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    .line 1321052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    .line 1321053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    .line 1321054
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8HU;

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    .line 1321055
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    .line 1321056
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8HU;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/8HU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1321059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321060
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    .line 1321061
    iput-object p2, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    .line 1321062
    iput-object p3, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    .line 1321063
    iput-object p4, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    .line 1321064
    iput-object p5, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    .line 1321065
    iput-object p6, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    .line 1321066
    iput-object p7, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    .line 1321067
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1321057
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1321040
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321041
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321042
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321043
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321044
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1321045
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1321046
    iget-object v0, p0, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1321047
    return-void
.end method
