.class public Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:LX/8HP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320962
    new-instance v0, LX/8HN;

    invoke-direct {v0}, LX/8HN;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Ljava/lang/String;LX/8HP;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/8HP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1320957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320958
    iput-object p2, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->b:Ljava/lang/String;

    .line 1320959
    iput-object p3, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    .line 1320960
    iput-object p1, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    .line 1320961
    return-void
.end method

.method public constructor <init>(LX/8HO;)V
    .locals 1

    .prologue
    .line 1320963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320964
    iget-object v0, p1, LX/8HO;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->b:Ljava/lang/String;

    .line 1320965
    iget-object v0, p1, LX/8HO;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    .line 1320966
    iget-object v0, p1, LX/8HO;->c:LX/8HP;

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    .line 1320967
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320949
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1320950
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1320951
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    .line 1320952
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->b:Ljava/lang/String;

    .line 1320953
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8HP;->valueOf(Ljava/lang/String;)LX/8HP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    .line 1320954
    iget-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    sget-object v1, LX/8HP;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/8HP;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1320955
    return-void

    .line 1320956
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320947
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320943
    iget-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1320944
    iget-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320945
    iget-object v0, p0, Lcom/facebook/photos/data/method/ModifyAlbumContributorParams;->c:LX/8HP;

    invoke-virtual {v0}, LX/8HP;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320946
    return-void
.end method
