.class public Lcom/facebook/photos/data/method/EditPhotoLocationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/EditPhotoLocationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320651
    new-instance v0, LX/8HD;

    invoke-direct {v0}, LX/8HD;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320653
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->a:Ljava/lang/String;

    .line 1320654
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->b:Ljava/lang/String;

    .line 1320655
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->c:Ljava/lang/String;

    .line 1320656
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1320657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320658
    iput-object p1, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->a:Ljava/lang/String;

    .line 1320659
    iput-object p2, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->b:Ljava/lang/String;

    .line 1320660
    iput-object p3, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->c:Ljava/lang/String;

    .line 1320661
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320662
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320663
    iget-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320664
    iget-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320665
    iget-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoLocationParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320666
    return-void
.end method
