.class public Lcom/facebook/photos/data/method/CropProfilePictureParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/CropProfilePictureParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/graphics/RectF;

.field public final c:J

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320313
    new-instance v0, LX/8Gy;

    invoke-direct {v0}, LX/8Gy;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320315
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->a:Ljava/lang/String;

    .line 1320316
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->b:Landroid/graphics/RectF;

    .line 1320317
    iget-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->b:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->readFromParcel(Landroid/os/Parcel;)V

    .line 1320318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->e:Ljava/lang/String;

    .line 1320319
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->c:J

    .line 1320320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->d:Ljava/lang/String;

    .line 1320321
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1320322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320323
    iput-object p1, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->a:Ljava/lang/String;

    .line 1320324
    iput-object p2, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->b:Landroid/graphics/RectF;

    .line 1320325
    iput-object p3, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->e:Ljava/lang/String;

    .line 1320326
    iput-wide p4, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->c:J

    .line 1320327
    iput-object p6, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->d:Ljava/lang/String;

    .line 1320328
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320329
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1320330
    iget-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320331
    iget-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->b:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1320332
    iget-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320333
    iget-wide v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1320334
    iget-object v0, p0, Lcom/facebook/photos/data/method/CropProfilePictureParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320335
    return-void
.end method
