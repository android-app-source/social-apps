.class public Lcom/facebook/photos/data/method/DeletePhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeletePhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320426
    new-instance v0, LX/8H3;

    invoke-direct {v0}, LX/8H3;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeletePhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320428
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoParams;->a:Ljava/lang/String;

    .line 1320429
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1320423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320424
    iput-object p1, p0, Lcom/facebook/photos/data/method/DeletePhotoParams;->a:Ljava/lang/String;

    .line 1320425
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320420
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320421
    iget-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320422
    return-void
.end method
