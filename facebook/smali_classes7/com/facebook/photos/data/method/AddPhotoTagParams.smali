.class public Lcom/facebook/photos/data/method/AddPhotoTagParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/AddPhotoTagParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:D

.field public final d:D

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320041
    new-instance v0, LX/8Gq;

    invoke-direct {v0}, LX/8Gq;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320043
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->a:Ljava/lang/String;

    .line 1320044
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->b:Ljava/lang/String;

    .line 1320045
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->c:D

    .line 1320046
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d:D

    .line 1320047
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->e:Ljava/lang/String;

    .line 1320048
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V
    .locals 4

    .prologue
    .line 1320049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320050
    iput-object p1, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->a:Ljava/lang/String;

    .line 1320051
    iget-wide v2, p2, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v2

    .line 1320052
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->b:Ljava/lang/String;

    .line 1320053
    iget-object v0, p2, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1320054
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->e:Ljava/lang/String;

    .line 1320055
    iget-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320056
    iget v0, p3, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->c:D

    .line 1320057
    iget v0, p3, Landroid/graphics/PointF;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d:D

    .line 1320058
    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 1320059
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1320060
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "x"

    .line 1320061
    iget-wide v8, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->c:D

    move-wide v4, v8

    .line 1320062
    mul-double/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "y"

    .line 1320063
    iget-wide v8, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d:D

    move-wide v4, v8

    .line 1320064
    mul-double/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v1

    .line 1320065
    iget-object v2, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1320066
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1320067
    const-string v2, "tag_uid"

    .line 1320068
    iget-object v3, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1320069
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1320070
    :goto_0
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1320071
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1320072
    :cond_0
    :try_start_1
    const-string v2, "tag_text"

    .line 1320073
    iget-object v3, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1320074
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1320075
    :catch_0
    move-exception v0

    .line 1320076
    const-string v1, "AddPhotoTagParams"

    const-string v2, "JSON exception encoding params"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1320077
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320078
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1320079
    iget-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320080
    iget-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320081
    iget-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1320082
    iget-wide v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->d:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1320083
    iget-object v0, p0, Lcom/facebook/photos/data/method/AddPhotoTagParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320084
    return-void
.end method
