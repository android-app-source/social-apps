.class public Lcom/facebook/photos/data/method/EditPhotoCaptionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/EditPhotoCaptionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/175;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320606
    new-instance v0, LX/8HB;

    invoke-direct {v0}, LX/8HB;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320608
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->a:Ljava/lang/String;

    .line 1320609
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/175;

    iput-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->b:LX/175;

    .line 1320610
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/175;)V
    .locals 0
    .param p2    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1320611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320612
    iput-object p1, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->a:Ljava/lang/String;

    .line 1320613
    iput-object p2, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->b:LX/175;

    .line 1320614
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320602
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320603
    iget-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320604
    iget-object v0, p0, Lcom/facebook/photos/data/method/EditPhotoCaptionParams;->b:LX/175;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1320605
    return-void
.end method
