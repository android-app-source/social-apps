.class public Lcom/facebook/photos/data/method/DeletePhotoResponse;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeletePhotoResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field private b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320433
    new-instance v0, LX/8H4;

    invoke-direct {v0}, LX/8H4;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320435
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->a:Z

    .line 1320436
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->b:Z

    .line 1320437
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 1320438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320439
    iput-boolean p1, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->a:Z

    .line 1320440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->b:Z

    .line 1320441
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320442
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320443
    iget-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1320444
    iget-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1320445
    return-void
.end method
