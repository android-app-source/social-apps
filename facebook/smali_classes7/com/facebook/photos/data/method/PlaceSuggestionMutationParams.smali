.class public Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320971
    new-instance v0, LX/8HQ;

    invoke-direct {v0}, LX/8HQ;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320973
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->a:Ljava/lang/String;

    .line 1320974
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->b:Ljava/lang/String;

    .line 1320975
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->c:Ljava/lang/String;

    .line 1320976
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1320977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320978
    iput-object p1, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->a:Ljava/lang/String;

    .line 1320979
    iput-object p2, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->b:Ljava/lang/String;

    .line 1320980
    iput-object p3, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->c:Ljava/lang/String;

    .line 1320981
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320982
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320983
    iget-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320984
    iget-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320985
    iget-object v0, p0, Lcom/facebook/photos/data/method/PlaceSuggestionMutationParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320986
    return-void
.end method
