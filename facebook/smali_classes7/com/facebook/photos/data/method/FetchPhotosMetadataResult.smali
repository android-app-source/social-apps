.class public Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320880
    new-instance v0, LX/8HL;

    invoke-direct {v0}, LX/8HL;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320882
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    .line 1320883
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    const-class v1, Lcom/facebook/photos/data/model/FacebookPhoto;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1320884
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1320885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320886
    iput-object p1, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    .line 1320887
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320888
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320889
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1320890
    return-void
.end method
