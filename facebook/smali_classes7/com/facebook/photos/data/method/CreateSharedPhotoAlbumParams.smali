.class public Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320249
    new-instance v0, LX/8Gw;

    invoke-direct {v0}, LX/8Gw;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320251
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->a:Ljava/lang/String;

    .line 1320252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->b:Ljava/lang/String;

    .line 1320253
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1320254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320255
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1320256
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1320257
    iput-object p1, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->a:Ljava/lang/String;

    .line 1320258
    iput-object p2, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->b:Ljava/lang/String;

    .line 1320259
    return-void

    :cond_0
    move v0, v2

    .line 1320260
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1320261
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320262
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320263
    iget-object v0, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320264
    iget-object v0, p0, Lcom/facebook/photos/data/method/CreateSharedPhotoAlbumParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320265
    return-void
.end method
