.class public Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320103
    new-instance v0, LX/8Gs;

    invoke-direct {v0}, LX/8Gs;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1320104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320105
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320106
    iput-wide p1, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->a:J

    .line 1320107
    iput-object p3, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->b:Ljava/lang/String;

    .line 1320108
    iput-object p4, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->c:Ljava/lang/String;

    .line 1320109
    iput-object p5, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->d:Ljava/lang/String;

    .line 1320110
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->a:J

    .line 1320113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->b:Ljava/lang/String;

    .line 1320114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->c:Ljava/lang/String;

    .line 1320115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->d:Ljava/lang/String;

    .line 1320116
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320117
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1320118
    iget-wide v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1320119
    iget-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320120
    iget-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320121
    iget-object v0, p0, Lcom/facebook/photos/data/method/CreatePagePhotoAlbumParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320122
    return-void
.end method
