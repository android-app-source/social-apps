.class public Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320857
    new-instance v0, LX/8HK;

    invoke-direct {v0}, LX/8HK;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1320858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320859
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    .line 1320860
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    const-class v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1320861
    const-class v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    iput-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    .line 1320862
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/photos/base/photos/PhotoFetchInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1320863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320864
    iput-object p1, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    .line 1320865
    iput-object p2, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    .line 1320866
    return-void
.end method


# virtual methods
.method public final b()LX/3WA;
    .locals 1

    .prologue
    .line 1320867
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    if-nez v0, :cond_0

    .line 1320868
    const/4 v0, 0x0

    .line 1320869
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    iget-object v0, v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->a:LX/3WA;

    goto :goto_0
.end method

.method public final c()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1320870
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    if-nez v0, :cond_0

    .line 1320871
    const/4 v0, 0x0

    .line 1320872
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    iget-object v0, v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->b:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320873
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320874
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1320875
    iget-object v0, p0, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->b:Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1320876
    return-void
.end method
