.class public Lcom/facebook/photos/data/method/DeleteVideoResponse;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeleteVideoResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320559
    new-instance v0, LX/8H9;

    invoke-direct {v0}, LX/8H9;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeleteVideoResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320567
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/method/DeleteVideoResponse;->a:Z

    .line 1320568
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1320563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320564
    iput-boolean p1, p0, Lcom/facebook/photos/data/method/DeleteVideoResponse;->a:Z

    .line 1320565
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320562
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320560
    iget-boolean v0, p0, Lcom/facebook/photos/data/method/DeleteVideoResponse;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1320561
    return-void
.end method
