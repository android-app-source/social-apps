.class public Lcom/facebook/photos/data/method/DeleteVideoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeleteVideoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320555
    new-instance v0, LX/8H8;

    invoke-direct {v0}, LX/8H8;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeleteVideoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320553
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/DeleteVideoParams;->a:Ljava/lang/String;

    .line 1320554
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1320546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320547
    iput-object p1, p0, Lcom/facebook/photos/data/method/DeleteVideoParams;->a:Ljava/lang/String;

    .line 1320548
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320551
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320549
    iget-object v0, p0, Lcom/facebook/photos/data/method/DeleteVideoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320550
    return-void
.end method
