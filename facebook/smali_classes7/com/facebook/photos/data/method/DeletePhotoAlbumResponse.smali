.class public Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320380
    new-instance v0, LX/8H1;

    invoke-direct {v0}, LX/8H1;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320388
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;->a:Z

    .line 1320389
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1320384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320385
    iput-boolean p1, p0, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;->a:Z

    .line 1320386
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320383
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320381
    iget-boolean v0, p0, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1320382
    return-void
.end method
