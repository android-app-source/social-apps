.class public Lcom/facebook/photos/data/method/DeletePhotoTagParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/data/method/DeletePhotoTagParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320480
    new-instance v0, LX/8H6;

    invoke-direct {v0}, LX/8H6;-><init>()V

    sput-object v0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1320481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320482
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->a:Ljava/lang/String;

    .line 1320483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    .line 1320484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->c:Ljava/lang/String;

    .line 1320485
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1320486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320487
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320488
    iput-object p1, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->a:Ljava/lang/String;

    .line 1320489
    iput-object p2, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    .line 1320490
    iput-object p3, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->c:Ljava/lang/String;

    .line 1320491
    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1320492
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1320493
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1320494
    iget-object v2, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1320495
    if-eqz v2, :cond_0

    .line 1320496
    iget-object v2, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1320497
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1320498
    const-string v2, "tag_uid"

    .line 1320499
    iget-object v3, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1320500
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1320501
    :goto_0
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1320502
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1320503
    :cond_0
    :try_start_1
    const-string v2, "tag_text"

    .line 1320504
    iget-object v3, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1320505
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1320506
    :catch_0
    move-exception v0

    .line 1320507
    const-string v1, "DeletePhotoTagParams"

    const-string v2, "JSON exception encoding params"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1320508
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1320509
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1320510
    iget-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320511
    iget-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320512
    iget-object v0, p0, Lcom/facebook/photos/data/method/DeletePhotoTagParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1320513
    return-void
.end method
