.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1326245
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1326246
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1326247
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1326248
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1326249
    const/4 v2, 0x0

    .line 1326250
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1326251
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1326252
    :goto_0
    move v1, v2

    .line 1326253
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1326254
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1326255
    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;-><init>()V

    .line 1326256
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1326257
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1326258
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1326259
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1326260
    :cond_0
    return-object v1

    .line 1326261
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1326262
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1326263
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1326264
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1326265
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1326266
    const-string v4, "uploaded_mediaset"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1326267
    const/4 v3, 0x0

    .line 1326268
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1326269
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1326270
    :goto_2
    move v1, v3

    .line 1326271
    goto :goto_1

    .line 1326272
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1326273
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1326274
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1326275
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1326276
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1326277
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1326278
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1326279
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1326280
    const-string v5, "media"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1326281
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1326282
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_d

    .line 1326283
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1326284
    :goto_4
    move v1, v4

    .line 1326285
    goto :goto_3

    .line 1326286
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1326287
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1326288
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    .line 1326289
    :cond_9
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_b

    .line 1326290
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1326291
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1326292
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v7, :cond_9

    .line 1326293
    const-string p0, "count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1326294
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    goto :goto_5

    .line 1326295
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1326296
    :cond_b
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1326297
    if-eqz v1, :cond_c

    .line 1326298
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 1326299
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v1, v4

    move v6, v4

    goto :goto_5
.end method
