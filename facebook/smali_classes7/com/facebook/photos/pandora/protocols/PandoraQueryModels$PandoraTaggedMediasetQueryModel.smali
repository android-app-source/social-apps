.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3e3cf76
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1326243
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1326242
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1326240
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1326241
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1326237
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1326238
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1326239
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1326229
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1326230
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1326231
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1326232
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1326233
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1326234
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1326235
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1326236
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1326221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1326222
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1326223
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    .line 1326224
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1326225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    .line 1326226
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    .line 1326227
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1326228
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1326244
    new-instance v0, LX/8Ih;

    invoke-direct {v0, p1}, LX/8Ih;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1326219
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    .line 1326220
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1326217
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1326218
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1326216
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1326213
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;-><init>()V

    .line 1326214
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1326215
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1326212
    const v0, 0x45e964a7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1326211
    const v0, 0x252222

    return v0
.end method
