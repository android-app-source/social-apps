.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x579cfe29
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323640
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323641
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1323642
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1323643
    return-void
.end method

.method private a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323644
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    .line 1323645
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1323646
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323647
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1323648
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1323649
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1323650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323651
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1323652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323653
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1323654
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    .line 1323655
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1323656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;

    .line 1323657
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    .line 1323658
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323659
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1323660
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel;-><init>()V

    .line 1323661
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1323662
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1323663
    const v0, 0x166fb804

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1323664
    const v0, -0x6747e1ce

    return v0
.end method
