.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1325993
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1325994
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1325992
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1325937
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1325938
    const/4 v2, 0x0

    .line 1325939
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1325940
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1325941
    :goto_0
    move v1, v2

    .line 1325942
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1325943
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1325944
    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;-><init>()V

    .line 1325945
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1325946
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1325947
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1325948
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1325949
    :cond_0
    return-object v1

    .line 1325950
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1325951
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1325952
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1325953
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1325954
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1325955
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1325956
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1325957
    :cond_4
    const-string v5, "tagged_mediaset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1325958
    const/4 v4, 0x0

    .line 1325959
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_a

    .line 1325960
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1325961
    :goto_2
    move v1, v4

    .line 1325962
    goto :goto_1

    .line 1325963
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1325964
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1325965
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1325966
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1325967
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1325968
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1325969
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1325970
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1325971
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_8

    if-eqz v5, :cond_8

    .line 1325972
    const-string v6, "media"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1325973
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1325974
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_f

    .line 1325975
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1325976
    :goto_4
    move v1, v5

    .line 1325977
    goto :goto_3

    .line 1325978
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1325979
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1325980
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v1, v4

    goto :goto_3

    .line 1325981
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_d

    .line 1325982
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1325983
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1325984
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v8, :cond_b

    .line 1325985
    const-string p0, "count"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1325986
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v6

    goto :goto_5

    .line 1325987
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1325988
    :cond_d
    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1325989
    if-eqz v1, :cond_e

    .line 1325990
    invoke-virtual {v0, v5, v7, v5}, LX/186;->a(III)V

    .line 1325991
    :cond_e
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_4

    :cond_f
    move v1, v5

    move v7, v5

    goto :goto_5
.end method
