.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1325997
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1325998
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1325999
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1326000
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1326001
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1326002
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326003
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1326004
    if-eqz v2, :cond_0

    .line 1326005
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326006
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1326007
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1326008
    if-eqz v2, :cond_3

    .line 1326009
    const-string p0, "tagged_mediaset"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326010
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326011
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1326012
    if-eqz p0, :cond_2

    .line 1326013
    const-string v0, "media"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326014
    const/4 v0, 0x0

    .line 1326015
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326016
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1326017
    if-eqz v0, :cond_1

    .line 1326018
    const-string v2, "count"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326019
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1326020
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326021
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326022
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326023
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1326024
    check-cast p1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel$Serializer;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
