.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d9daa47
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323443
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323442
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1323440
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1323441
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323437
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1323438
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1323439
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1323429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323430
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1323431
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1323432
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1323433
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1323434
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1323435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323436
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1323421
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323422
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1323423
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    .line 1323424
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1323425
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;

    .line 1323426
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    .line 1323427
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323428
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323414
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    .line 1323415
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel$MediaModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1323418
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel$MediaSetModel;-><init>()V

    .line 1323419
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1323420
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1323417
    const v0, 0xad5c570

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1323416
    const v0, -0x31d68202

    return v0
.end method
