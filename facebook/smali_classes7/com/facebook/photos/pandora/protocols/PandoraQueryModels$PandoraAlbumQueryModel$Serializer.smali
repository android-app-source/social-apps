.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1323236
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1323237
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1323238
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1323239
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1323240
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1323241
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1323242
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1323243
    if-eqz v2, :cond_0

    .line 1323244
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1323245
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1323246
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1323247
    if-eqz v2, :cond_1

    .line 1323248
    const-string p0, "media"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1323249
    invoke-static {v1, v2, p1, p2}, LX/8Il;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1323250
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1323251
    if-eqz v2, :cond_3

    .line 1323252
    const-string p0, "photo_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1323253
    const/4 p0, 0x0

    .line 1323254
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1323255
    invoke-virtual {v1, v2, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1323256
    if-eqz p0, :cond_2

    .line 1323257
    const-string v0, "count"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1323258
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1323259
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1323260
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1323261
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1323262
    check-cast p1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Serializer;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
