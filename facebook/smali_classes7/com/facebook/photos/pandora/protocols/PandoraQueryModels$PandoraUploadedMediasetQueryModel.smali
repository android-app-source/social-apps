.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7dfc7047
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1326526
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1326525
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1326523
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1326524
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1326517
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1326518
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1326519
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1326520
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1326521
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1326522
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1326509
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1326510
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1326511
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    .line 1326512
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1326513
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;

    .line 1326514
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    .line 1326515
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1326516
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1326508
    new-instance v0, LX/8Ij;

    invoke-direct {v0, p1}, LX/8Ij;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1326527
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    .line 1326528
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;->e:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel$UploadedMediasetModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1326506
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1326507
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1326505
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1326502
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediasetQueryModel;-><init>()V

    .line 1326503
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1326504
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1326501
    const v0, 0x345388be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1326500
    const v0, 0x285feb

    return v0
.end method
