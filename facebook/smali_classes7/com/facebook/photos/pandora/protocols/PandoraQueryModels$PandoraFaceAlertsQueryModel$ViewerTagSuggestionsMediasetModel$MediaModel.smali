.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2c61d028
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323600
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323599
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1323597
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1323598
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1323595
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->e:Ljava/util/List;

    .line 1323596
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323601
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1323602
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1323587
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323588
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1323589
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1323590
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1323591
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1323592
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1323593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323594
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1323574
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323575
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1323576
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1323577
    if-eqz v1, :cond_2

    .line 1323578
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    .line 1323579
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1323580
    :goto_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1323581
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1323582
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1323583
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    .line 1323584
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1323585
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323586
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1323571
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;-><init>()V

    .line 1323572
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1323573
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1323570
    const v0, -0x66a26ed2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1323569
    const v0, 0x2c8ffc04

    return v0
.end method
