.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1323133
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1323134
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1323135
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1323136
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1323137
    const/4 v2, 0x0

    .line 1323138
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 1323139
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1323140
    :goto_0
    move v1, v2

    .line 1323141
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1323142
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1323143
    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;-><init>()V

    .line 1323144
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1323145
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1323146
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1323147
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1323148
    :cond_0
    return-object v1

    .line 1323149
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1323150
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1323151
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1323152
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1323153
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 1323154
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1323155
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1323156
    :cond_4
    const-string v6, "media"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1323157
    invoke-static {p1, v0}, LX/8Il;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1323158
    :cond_5
    const-string v6, "photo_items"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1323159
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1323160
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_c

    .line 1323161
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1323162
    :goto_2
    move v1, v5

    .line 1323163
    goto :goto_1

    .line 1323164
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1323165
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1323166
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1323167
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1323168
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 1323169
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_a

    .line 1323170
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1323171
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1323172
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_8

    if-eqz v8, :cond_8

    .line 1323173
    const-string p0, "count"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1323174
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v6

    goto :goto_3

    .line 1323175
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1323176
    :cond_a
    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1323177
    if-eqz v1, :cond_b

    .line 1323178
    invoke-virtual {v0, v5, v7, v5}, LX/186;->a(III)V

    .line 1323179
    :cond_b
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_c
    move v1, v5

    move v7, v5

    goto :goto_3
.end method
