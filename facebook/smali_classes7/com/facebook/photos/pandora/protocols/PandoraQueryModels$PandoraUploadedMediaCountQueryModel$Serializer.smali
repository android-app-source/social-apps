.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1326302
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;

    new-instance v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1326303
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1326325
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1326305
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1326306
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1326307
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326308
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1326309
    if-eqz v2, :cond_2

    .line 1326310
    const-string p0, "uploaded_mediaset"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326311
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326312
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1326313
    if-eqz p0, :cond_1

    .line 1326314
    const-string v0, "media"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326315
    const/4 v0, 0x0

    .line 1326316
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1326317
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1326318
    if-eqz v0, :cond_0

    .line 1326319
    const-string v2, "count"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1326320
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1326321
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326322
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326323
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1326324
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1326304
    check-cast p1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel$Serializer;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraUploadedMediaCountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
