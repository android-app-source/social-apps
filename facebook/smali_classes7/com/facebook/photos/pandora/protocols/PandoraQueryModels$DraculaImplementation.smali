.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1323058
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1323059
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1323131
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1323132
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1323105
    if-nez p1, :cond_0

    .line 1323106
    :goto_0
    return v0

    .line 1323107
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1323108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1323109
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1323110
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1323111
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1323112
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1323113
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1323114
    const v2, -0x3f1b2bd6

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1323115
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1323116
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1323117
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1323118
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1323119
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1323120
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1323121
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1323122
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1323123
    const v2, 0x63f4893f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1323124
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1323125
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1323126
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1323127
    :sswitch_4
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1323128
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1323129
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1323130
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f1b2bd6 -> :sswitch_2
        -0xb7f15ea -> :sswitch_1
        0x18de0297 -> :sswitch_3
        0x61c64982 -> :sswitch_0
        0x63f4893f -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1323104
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1323097
    sparse-switch p2, :sswitch_data_0

    .line 1323098
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1323099
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1323100
    const v1, -0x3f1b2bd6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1323101
    :goto_0
    :sswitch_1
    return-void

    .line 1323102
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1323103
    const v1, 0x63f4893f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3f1b2bd6 -> :sswitch_1
        -0xb7f15ea -> :sswitch_0
        0x18de0297 -> :sswitch_2
        0x61c64982 -> :sswitch_1
        0x63f4893f -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1323091
    if-eqz p1, :cond_0

    .line 1323092
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1323093
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;

    .line 1323094
    if-eq v0, v1, :cond_0

    .line 1323095
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1323096
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1323090
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1323088
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1323089
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1323083
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1323084
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1323085
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1323086
    iput p2, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->b:I

    .line 1323087
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1323082
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1323081
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1323078
    iget v0, p0, LX/1vt;->c:I

    .line 1323079
    move v0, v0

    .line 1323080
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1323055
    iget v0, p0, LX/1vt;->c:I

    .line 1323056
    move v0, v0

    .line 1323057
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1323075
    iget v0, p0, LX/1vt;->b:I

    .line 1323076
    move v0, v0

    .line 1323077
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323072
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1323073
    move-object v0, v0

    .line 1323074
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1323063
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1323064
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1323065
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1323066
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1323067
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1323068
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1323069
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1323070
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1323071
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1323060
    iget v0, p0, LX/1vt;->c:I

    .line 1323061
    move v0, v0

    .line 1323062
    return v0
.end method
