.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5AI;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2bfe009e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:I

.field private H:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1325816
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1325815
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1325813
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1325814
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1325810
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1325811
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1325812
    return-void
.end method

.method private H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325808
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1325809
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325806
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325807
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    return-object v0
.end method

.method private J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325804
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325805
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325692
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 1325693
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    return-object v0
.end method

.method private L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325751
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 1325752
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325749
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325750
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    return-object v0
.end method

.method private N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325747
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1325748
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325745
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1325746
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325743
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1325744
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325741
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    .line 1325742
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    return-object v0
.end method

.method private R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325666
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    .line 1325667
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;
    .locals 4

    .prologue
    .line 1325700
    if-nez p0, :cond_0

    .line 1325701
    const/4 p0, 0x0

    .line 1325702
    :goto_0
    return-object p0

    .line 1325703
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    if-eqz v0, :cond_1

    .line 1325704
    check-cast p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    goto :goto_0

    .line 1325705
    :cond_1
    new-instance v2, LX/8Ic;

    invoke-direct {v2}, LX/8Ic;-><init>()V

    .line 1325706
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->b()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->a:Z

    .line 1325707
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->c()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->b:Z

    .line 1325708
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->d()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->c:Z

    .line 1325709
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->e()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->d:Z

    .line 1325710
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->ac_()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->e:Z

    .line 1325711
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->ad_()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->f:Z

    .line 1325712
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->g:Z

    .line 1325713
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->h:Z

    .line 1325714
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->i:Z

    .line 1325715
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->j:Ljava/lang/String;

    .line 1325716
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->k:Ljava/lang/String;

    .line 1325717
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->l:Z

    .line 1325718
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->m:Ljava/lang/String;

    .line 1325719
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->n:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1325720
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->o:Z

    .line 1325721
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->p:Ljava/lang/String;

    .line 1325722
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->q:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325723
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325724
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->x()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->s:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 1325725
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->t:Ljava/lang/String;

    .line 1325726
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 1325727
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->z()Z

    move-result v0

    iput-boolean v0, v2, LX/8Ic;->v:Z

    .line 1325728
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1325729
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1325730
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1325731
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1325732
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->w:LX/0Px;

    .line 1325733
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->x:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325734
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1325735
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r()LX/59N;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;->a(LX/59N;)Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->z:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1325736
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->A:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1325737
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->B:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    .line 1325738
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F()I

    move-result v0

    iput v0, v2, LX/8Ic;->C:I

    .line 1325739
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v0

    iput-object v0, v2, LX/8Ic;->D:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    .line 1325740
    invoke-virtual {v2}, LX/8Ic;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1325694
    iput p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G:I

    .line 1325695
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325696
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325697
    if-eqz v0, :cond_0

    .line 1325698
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1325699
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1325931
    iput-object p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325932
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325933
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325934
    if-eqz v0, :cond_0

    .line 1325935
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1325936
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1325817
    iput-object p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325818
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325819
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325820
    if-eqz v0, :cond_0

    .line 1325821
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1325822
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1325925
    iput-object p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1325926
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325927
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325928
    if-eqz v0, :cond_0

    .line 1325929
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1325930
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1325919
    iput-object p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325920
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325921
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325922
    if-eqz v0, :cond_0

    .line 1325923
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1325924
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1325913
    iput-boolean p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->g:Z

    .line 1325914
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325915
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325916
    if-eqz v0, :cond_0

    .line 1325917
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1325918
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1325907
    iput-boolean p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k:Z

    .line 1325908
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325909
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325910
    if-eqz v0, :cond_0

    .line 1325911
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1325912
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1325901
    iput-boolean p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p:Z

    .line 1325902
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325903
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325904
    if-eqz v0, :cond_0

    .line 1325905
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1325906
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1325895
    iput-boolean p1, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s:Z

    .line 1325896
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1325897
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1325898
    if-eqz v0, :cond_0

    .line 1325899
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1325900
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1325893
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A:Ljava/util/List;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A:Ljava/util/List;

    .line 1325894
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325892
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325891
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325890
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325889
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final F()I
    .locals 2

    .prologue
    .line 1325887
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325888
    iget v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G:I

    return v0
.end method

.method public final synthetic G()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325886
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 1325753
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1325754
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1325755
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1325756
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1325757
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1325758
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1325759
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1325760
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1325761
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1325762
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1325763
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1325764
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 1325765
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1325766
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1325767
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1325768
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1325769
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1325770
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1325771
    const/16 v20, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1325772
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->e:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325773
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->f:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325774
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->g:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325775
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->h:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325776
    const/16 v20, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->i:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325777
    const/16 v20, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325778
    const/16 v20, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325779
    const/16 v20, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325780
    const/16 v20, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1325781
    const/16 v20, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1325782
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1325783
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1325784
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1325785
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1325786
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1325787
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1325788
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1325789
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1325790
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1325791
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1325792
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1325793
    const/16 v3, 0x15

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->z:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1325794
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1325795
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1325796
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1325797
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1325798
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1325799
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1325800
    const/16 v3, 0x1c

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1325801
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1325802
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1325803
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1325823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1325824
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1325825
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1325826
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1325827
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325828
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 1325829
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1325830
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325831
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1325832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325833
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325834
    :cond_1
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1325835
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325836
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1325837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325838
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325839
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1325840
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 1325841
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1325842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325843
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    .line 1325844
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1325845
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 1325846
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1325847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325848
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 1325849
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1325850
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1325851
    if-eqz v2, :cond_5

    .line 1325852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325853
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A:Ljava/util/List;

    move-object v1, v0

    .line 1325854
    :cond_5
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1325855
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325856
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1325857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325858
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325859
    :cond_6
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1325860
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1325861
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1325862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325863
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 1325864
    :cond_7
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1325865
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1325866
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1325867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325868
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    .line 1325869
    :cond_8
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1325870
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1325871
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1325872
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325873
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 1325874
    :cond_9
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1325875
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    .line 1325876
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->Q()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1325877
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325878
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    .line 1325879
    :cond_a
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1325880
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    .line 1325881
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->R()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1325882
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    .line 1325883
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    .line 1325884
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1325885
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1325518
    new-instance v0, LX/8Id;

    invoke-direct {v0, p1}, LX/8Id;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325663
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1325648
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1325649
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->e:Z

    .line 1325650
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->f:Z

    .line 1325651
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->g:Z

    .line 1325652
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->h:Z

    .line 1325653
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->i:Z

    .line 1325654
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j:Z

    .line 1325655
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k:Z

    .line 1325656
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l:Z

    .line 1325657
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m:Z

    .line 1325658
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p:Z

    .line 1325659
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s:Z

    .line 1325660
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->z:Z

    .line 1325661
    const/16 v0, 0x1c

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G:I

    .line 1325662
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1325596
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325597
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325599
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1325600
    :goto_0
    return-void

    .line 1325601
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1325602
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325604
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1325605
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1325606
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325607
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325608
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1325609
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1325610
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325612
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1325613
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1325614
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 1325615
    if-eqz v0, :cond_9

    .line 1325616
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325617
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325618
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1325619
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1325620
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1325621
    if-eqz v0, :cond_9

    .line 1325622
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325623
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325624
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1325625
    :cond_5
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1325626
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1325627
    if-eqz v0, :cond_9

    .line 1325628
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325629
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325630
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1325631
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1325632
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1325633
    if-eqz v0, :cond_9

    .line 1325634
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325635
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325636
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1325637
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1325638
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1325639
    if-eqz v0, :cond_9

    .line 1325640
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325641
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325642
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1325643
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1325644
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1325645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1325646
    const/16 v0, 0x1c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1325647
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1325587
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1325588
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;)V

    .line 1325589
    :cond_0
    :goto_0
    return-void

    .line 1325590
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1325591
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 1325592
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1325593
    check-cast p2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;)V

    goto :goto_0

    .line 1325594
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325595
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1325536
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1325537
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(Z)V

    .line 1325538
    :cond_0
    :goto_0
    return-void

    .line 1325539
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1325540
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->b(Z)V

    goto :goto_0

    .line 1325541
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1325542
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->c(Z)V

    goto :goto_0

    .line 1325543
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1325544
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->d(Z)V

    goto :goto_0

    .line 1325545
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1325546
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    .line 1325547
    if-eqz v0, :cond_0

    .line 1325548
    if-eqz p3, :cond_5

    .line 1325549
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    .line 1325550
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    .line 1325551
    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    goto :goto_0

    .line 1325552
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 1325553
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1325554
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1325555
    if-eqz v0, :cond_0

    .line 1325556
    if-eqz p3, :cond_7

    .line 1325557
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 1325558
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 1325559
    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 1325560
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 1325561
    :cond_8
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1325562
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1325563
    if-eqz v0, :cond_0

    .line 1325564
    if-eqz p3, :cond_9

    .line 1325565
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    .line 1325566
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    .line 1325567
    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 1325568
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 1325569
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1325570
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1325571
    if-eqz v0, :cond_0

    .line 1325572
    if-eqz p3, :cond_b

    .line 1325573
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325574
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    .line 1325575
    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 1325576
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a(I)V

    goto/16 :goto_0

    .line 1325577
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1325578
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->M()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1325579
    if-eqz v0, :cond_0

    .line 1325580
    if-eqz p3, :cond_d

    .line 1325581
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    .line 1325582
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    .line 1325583
    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    goto/16 :goto_0

    .line 1325584
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b(I)V

    goto/16 :goto_0

    .line 1325585
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325586
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 1325534
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325535
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 1325532
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325533
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1325529
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;-><init>()V

    .line 1325530
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1325531
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1325527
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325528
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1325525
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325526
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1325523
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325524
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1325522
    const v0, 0x5792332

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1325520
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325521
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1325519
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1325516
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325517
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1325677
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325678
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325690
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n:Ljava/lang/String;

    .line 1325691
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1325688
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325689
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325686
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q:Ljava/lang/String;

    .line 1325687
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1325684
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325685
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325682
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t:Ljava/lang/String;

    .line 1325683
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325680
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->x:Ljava/lang/String;

    .line 1325681
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325679
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->O()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 1325664
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325665
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325675
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o:Ljava/lang/String;

    .line 1325676
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325674
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325673
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->I()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325672
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325671
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->K()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1325670
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->L()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 1325668
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1325669
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->z:Z

    return v0
.end method
