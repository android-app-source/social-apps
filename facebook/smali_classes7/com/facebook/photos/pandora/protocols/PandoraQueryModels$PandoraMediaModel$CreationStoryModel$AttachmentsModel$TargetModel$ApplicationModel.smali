.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1324197
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1324244
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1324242
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1324243
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1324239
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1324240
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1324241
    return-void
.end method

.method public static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;
    .locals 9

    .prologue
    .line 1324216
    if-nez p0, :cond_0

    .line 1324217
    const/4 p0, 0x0

    .line 1324218
    :goto_0
    return-object p0

    .line 1324219
    :cond_0
    instance-of v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    if-eqz v0, :cond_1

    .line 1324220
    check-cast p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    goto :goto_0

    .line 1324221
    :cond_1
    new-instance v0, LX/8IT;

    invoke-direct {v0}, LX/8IT;-><init>()V

    .line 1324222
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8IT;->a:Ljava/lang/String;

    .line 1324223
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8IT;->b:Ljava/lang/String;

    .line 1324224
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1324225
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1324226
    iget-object v3, v0, LX/8IT;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1324227
    iget-object v5, v0, LX/8IT;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1324228
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1324229
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1324230
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1324231
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1324232
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1324233
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1324234
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1324235
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1324236
    new-instance v3, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    invoke-direct {v3, v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;-><init>(LX/15i;)V

    .line 1324237
    move-object p0, v3

    .line 1324238
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1324208
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1324209
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1324210
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1324211
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1324212
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1324213
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1324214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1324215
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1324245
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1324246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1324247
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1324207
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1324204
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;-><init>()V

    .line 1324205
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1324206
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1324202
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->e:Ljava/lang/String;

    .line 1324203
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1324200
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->f:Ljava/lang/String;

    .line 1324201
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1324199
    const v0, -0x2b3b2ef6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1324198
    const v0, -0x3ff252d0

    return v0
.end method
