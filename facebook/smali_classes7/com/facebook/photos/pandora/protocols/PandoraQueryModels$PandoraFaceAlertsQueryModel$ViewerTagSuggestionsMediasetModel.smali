.class public final Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3911cdf9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323639
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1323638
    const-class v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1323636
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1323637
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323633
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1323634
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1323635
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1323631
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    .line 1323632
    iget-object v0, p0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1323623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323624
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1323625
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->j()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1323626
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1323627
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1323628
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1323629
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323630
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1323610
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1323611
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->j()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1323612
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->j()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    .line 1323613
    invoke-direct {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->j()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1323614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    .line 1323615
    iput-object v0, v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;->f:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel$MediaModel;

    .line 1323616
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1323617
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1323620
    new-instance v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraFaceAlertsQueryModel$ViewerTagSuggestionsMediasetModel;-><init>()V

    .line 1323621
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1323622
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1323619
    const v0, 0x71c28cd7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1323618
    const v0, -0x31d68202

    return v0
.end method
