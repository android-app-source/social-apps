.class public final Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328506
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;B)V
    .locals 0

    .prologue
    .line 1328505
    invoke-direct {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1328499
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-boolean v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-nez v0, :cond_0

    .line 1328500
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1328501
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, v1, Lcom/facebook/photos/tagging/shared/TagTypeahead;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setTagSuggestions(Ljava/util/List;)V

    .line 1328502
    :cond_0
    :goto_0
    return-void

    .line 1328503
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v0, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->A:LX/8JS;

    if-eqz v0, :cond_0

    .line 1328504
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, v1, Lcom/facebook/photos/tagging/shared/TagTypeahead;->A:LX/8JS;

    invoke-interface {v1}, LX/8JS;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setTagSuggestions(Ljava/util/List;)V

    goto :goto_0
.end method
