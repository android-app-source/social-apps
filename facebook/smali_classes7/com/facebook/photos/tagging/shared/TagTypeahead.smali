.class public Lcom/facebook/photos/tagging/shared/TagTypeahead;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/7Gn;


# instance fields
.field public A:LX/8JS;

.field private B:Landroid/os/Handler;

.field private C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

.field private final D:LX/0Yb;

.field public E:Z

.field private F:LX/8JV;

.field private G:LX/8nB;

.field private final H:LX/4mR;

.field private final I:LX/4mR;

.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/73w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/8JL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/4mV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

.field private i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/EditText;

.field private k:Landroid/widget/ProgressBar;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ListView;

.field public n:LX/4mU;

.field public o:LX/8Hs;

.field private p:LX/8Hs;

.field public q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

.field public r:LX/8JT;

.field private s:Landroid/view/GestureDetector;

.field public t:Z

.field public u:Z

.field public v:Ljava/lang/String;

.field public w:Landroid/text/Editable;

.field public x:Z

.field private y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1328672
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328673
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x96

    const/4 v4, 0x0

    .line 1328674
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328675
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->w:Landroid/text/Editable;

    .line 1328676
    new-instance v0, LX/8JP;

    invoke-direct {v0, p0}, LX/8JP;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->H:LX/4mR;

    .line 1328677
    new-instance v0, LX/8JQ;

    invoke-direct {v0, p0}, LX/8JQ;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->I:LX/4mR;

    .line 1328678
    const-class v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-static {v0, p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1328679
    const v0, 0x7f03146b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1328680
    const v0, 0x7f0d2e77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1328681
    const v0, 0x7f0d2e78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->i:Landroid/widget/LinearLayout;

    .line 1328682
    const v0, 0x7f0d2e79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    .line 1328683
    const v0, 0x7f0d2e7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->k:Landroid/widget/ProgressBar;

    .line 1328684
    const v0, 0x7f0d2e7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    .line 1328685
    const v0, 0x7f0d2e7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->m:Landroid/widget/ListView;

    .line 1328686
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    new-instance v2, LX/8JM;

    invoke-direct {v2, p0}, LX/8JM;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->D:LX/0Yb;

    .line 1328687
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->B:Landroid/os/Handler;

    .line 1328688
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f:LX/4mV;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0, v1}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    .line 1328689
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 1328690
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 1328691
    iget-object v2, v0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1328692
    if-nez v2, :cond_0

    .line 1328693
    :goto_0
    new-instance v0, LX/8Hs;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->i:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f:LX/4mV;

    invoke-direct {v0, v1, v6, v7, v2}, LX/8Hs;-><init>(Landroid/view/View;JLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->o:LX/8Hs;

    .line 1328694
    new-instance v0, LX/8Hs;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->k:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f:LX/4mV;

    invoke-direct {v0, v1, v6, v7, v2}, LX/8Hs;-><init>(Landroid/view/View;JLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->p:LX/8Hs;

    .line 1328695
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setVisibility(I)V

    .line 1328696
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    const v1, 0x7f0819ee

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 1328697
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1328698
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/8JR;

    invoke-direct {v2, p0}, LX/8JR;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->s:Landroid/view/GestureDetector;

    .line 1328699
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    .line 1328700
    iput-boolean v4, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->z:Z

    .line 1328701
    iput-boolean v4, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->u:Z

    .line 1328702
    new-instance v0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;

    invoke-direct {v0, p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    invoke-static {p0, v0}, LX/8He;->b(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1328703
    return-void

    .line 1328704
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1328705
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1328706
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1328707
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1328708
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->d:LX/8JL;

    invoke-virtual {v0}, LX/8JL;->a()Ljava/util/List;

    move-result-object v3

    .line 1328709
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1328710
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 1328711
    :goto_0
    return-object p1

    .line 1328712
    :cond_0
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1328713
    iget-wide v8, v0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v8

    .line 1328714
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1328715
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1328716
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1328717
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v7, :cond_1

    .line 1328718
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v7, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    :goto_1
    move-object p1, v1

    .line 1328719
    goto :goto_0

    .line 1328720
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1328721
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1328722
    :cond_3
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private a(LX/8nB;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 3

    .prologue
    .line 1328657
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->G:LX/8nB;

    .line 1328658
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->G:LX/8nB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->G:LX/8nB;

    invoke-virtual {v0}, LX/8nB;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1328659
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->G:LX/8nB;

    invoke-virtual {v1}, LX/8nB;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1328660
    :cond_0
    new-instance v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->c:LX/0Sh;

    invoke-direct {v0, v1, p1, v2}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;-><init>(Landroid/content/Context;LX/8nB;LX/0Sh;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    .line 1328661
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    new-instance v1, LX/8JN;

    invoke-direct {v1, p0}, LX/8JN;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328662
    iput-object v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->h:Landroid/widget/Filter$FilterListener;

    .line 1328663
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1328664
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->m:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1328665
    return-void
.end method

.method private static a(Lcom/facebook/photos/tagging/shared/TagTypeahead;LX/0Or;LX/73w;LX/0Sh;LX/8JL;LX/0Xl;LX/4mV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/tagging/shared/TagTypeahead;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/8JL;",
            "LX/0Xl;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1328723
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b:LX/73w;

    iput-object p3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->c:LX/0Sh;

    iput-object p4, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->d:LX/8JL;

    iput-object p5, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->e:LX/0Xl;

    iput-object p6, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f:LX/4mV;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/16 v1, 0x12cb

    invoke-static {v6, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v6}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v2

    check-cast v2, LX/73w;

    invoke-static {v6}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v6}, LX/8JL;->a(LX/0QB;)LX/8JL;

    move-result-object v4

    check-cast v4, LX/8JL;

    invoke-static {v6}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v6}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v6

    check-cast v6, LX/4mV;

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Lcom/facebook/photos/tagging/shared/TagTypeahead;LX/0Or;LX/73w;LX/0Sh;LX/8JL;LX/0Xl;LX/4mV;)V

    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1328741
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    if-eqz v0, :cond_0

    .line 1328742
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h()V

    .line 1328743
    new-instance v0, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    .line 1328744
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->B:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    const-wide/16 v2, 0x1388

    const v4, -0x6435ec72

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1328745
    :cond_0
    return-void
.end method

.method public static f(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 1

    .prologue
    .line 1328724
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setVisibility(I)V

    .line 1328725
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1328726
    return-void
.end method

.method public static g(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 1

    .prologue
    .line 1328727
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setVisibility(I)V

    .line 1328728
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j()V

    .line 1328729
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1328730
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    if-eqz v0, :cond_0

    .line 1328731
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->B:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1328732
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->C:Lcom/facebook/photos/tagging/shared/TagTypeahead$DelayedLocalSuggestionsRunnable;

    .line 1328733
    :cond_0
    return-void
.end method

.method public static i(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 1

    .prologue
    .line 1328734
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a()V

    .line 1328735
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328736
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1328737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->y:Z

    .line 1328738
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1328739
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->y:Z

    .line 1328740
    return-void
.end method

.method public static k$redex0(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 2

    .prologue
    .line 1328666
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->p:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->c()V

    .line 1328667
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1328668
    return-void
.end method

.method public static l(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 2

    .prologue
    .line 1328669
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->p:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->d()V

    .line 1328670
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1328671
    return-void
.end method


# virtual methods
.method public final a(LX/8JT;)V
    .locals 0

    .prologue
    .line 1328545
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->r:LX/8JT;

    .line 1328546
    return-void
.end method

.method public final a(LX/8JX;)V
    .locals 1

    .prologue
    .line 1328535
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->G:LX/8nB;

    invoke-virtual {v0, p1}, LX/8nB;->a(LX/8JX;)V

    .line 1328536
    return-void
.end method

.method public final a(LX/8nB;)V
    .locals 2

    .prologue
    .line 1328537
    new-instance v0, LX/8JU;

    invoke-direct {v0, p0}, LX/8JU;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8nB;Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1328538
    return-void
.end method

.method public final a(Landroid/graphics/PointF;F)V
    .locals 3

    .prologue
    .line 1328539
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->E:Z

    if-eqz v0, :cond_0

    .line 1328540
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1328541
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    new-instance v2, Lcom/facebook/photos/tagging/shared/TagTypeahead$5;

    invoke-direct {v2, p0, v0, p2}, Lcom/facebook/photos/tagging/shared/TagTypeahead$5;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;Landroid/graphics/PointF;F)V

    invoke-static {v1, v2}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1328542
    :goto_0
    return-void

    .line 1328543
    :cond_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v2}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1328544
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v1, v0, p2}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(FF)V

    goto :goto_0
.end method

.method public final a(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1328547
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->y:Z

    if-nez v0, :cond_1

    .line 1328548
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1328549
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1328550
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b()V

    .line 1328551
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1328552
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->w:Landroid/text/Editable;

    .line 1328553
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    .line 1328554
    return-void

    .line 1328555
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-eqz v0, :cond_3

    .line 1328556
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a()V

    .line 1328557
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    goto :goto_0

    .line 1328558
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b()V

    goto :goto_0
.end method

.method public final a(ZLX/8JW;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/8JW;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1328559
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->D:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1328560
    iput-boolean v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->u:Z

    .line 1328561
    iget v0, p4, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v0, v3

    iput v0, p4, Landroid/graphics/PointF;->x:F

    .line 1328562
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p5, Landroid/graphics/PointF;->x:F

    .line 1328563
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    .line 1328564
    iput-object p2, v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->c:LX/8JW;

    .line 1328565
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b()V

    .line 1328566
    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    .line 1328567
    invoke-direct {p0, p3}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1328568
    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    .line 1328569
    iput-object v0, v3, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d:Ljava/util/List;

    .line 1328570
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a()V

    .line 1328571
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-nez v0, :cond_2

    .line 1328572
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->p:LX/8Hs;

    invoke-virtual {v0, v2}, LX/8Hs;->a(Z)V

    .line 1328573
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1328574
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 1328575
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    move v0, v0

    .line 1328576
    if-nez v0, :cond_4

    .line 1328577
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v3

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    .line 1328578
    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v3, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setMaxHeight(I)V

    .line 1328579
    if-eqz p1, :cond_3

    .line 1328580
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v5, v5}, LX/4mU;->a(FF)V

    .line 1328581
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v5}, LX/4mU;->a(F)V

    .line 1328582
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v5}, LX/4mU;->c(F)V

    .line 1328583
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v5}, LX/4mU;->e(F)V

    .line 1328584
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, LX/4mU;->g(F)V

    .line 1328585
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1}, LX/4mU;->i(F)V

    .line 1328586
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->o:LX/8Hs;

    invoke-virtual {v0, v2}, LX/8Hs;->b(Z)V

    .line 1328587
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->H:LX/4mR;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1328588
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v6}, LX/4mU;->b(F)V

    .line 1328589
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v6}, LX/4mU;->d(F)V

    .line 1328590
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v6}, LX/4mU;->f(F)V

    .line 1328591
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p5, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, LX/4mU;->h(F)V

    .line 1328592
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1328593
    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->e()V

    .line 1328594
    return-void

    :cond_1
    move v0, v2

    .line 1328595
    goto/16 :goto_0

    .line 1328596
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->p:LX/8Hs;

    invoke-virtual {v0, v2}, LX/8Hs;->b(Z)V

    .line 1328597
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1328598
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v2, p5, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v2}, LX/4mU;->g(F)V

    .line 1328599
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v2, p5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2}, LX/4mU;->i(F)V

    .line 1328600
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->f(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328601
    iput-boolean v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    goto :goto_2

    .line 1328602
    :cond_4
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j()V

    .line 1328603
    if-eqz p6, :cond_0

    .line 1328604
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    invoke-static {v0, v1}, LX/8Hi;->a(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(ZLandroid/graphics/PointF;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1328605
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->D:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1328606
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h()V

    .line 1328607
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    invoke-static {v0, v1}, LX/8Hi;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1328608
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->c()V

    .line 1328609
    if-eqz p1, :cond_0

    .line 1328610
    iget v0, p2, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 1328611
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->I:LX/4mR;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1328612
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v2}, LX/4mU;->b(F)V

    .line 1328613
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v2}, LX/4mU;->d(F)V

    .line 1328614
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    invoke-virtual {v0, v2}, LX/4mU;->f(F)V

    .line 1328615
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p2, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, LX/4mU;->h(F)V

    .line 1328616
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->n:LX/4mU;

    iget v1, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1328617
    :goto_0
    iput-boolean v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->u:Z

    .line 1328618
    iput-boolean v3, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    .line 1328619
    return-void

    .line 1328620
    :cond_0
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->g(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1328621
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->clear()V

    .line 1328622
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->k$redex0(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    .line 1328623
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2174b3b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1328624
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1328625
    new-instance v1, LX/8JV;

    invoke-direct {v1, p0}, LX/8JV;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    iput-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->F:LX/8JV;

    .line 1328626
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->F:LX/8JV;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1328627
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    new-instance v2, LX/8JO;

    invoke-direct {v2, p0}, LX/8JO;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1328628
    const/16 v1, 0x2d

    const v2, -0x11456491

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x717441a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1328629
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->F:LX/8JV;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1328630
    iput-object v4, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->F:LX/8JV;

    .line 1328631
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1328632
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1328633
    const/16 v1, 0x2d

    const v2, 0x5ff410d2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x764bfadc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1328634
    iget-boolean v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    move v2, v2

    .line 1328635
    if-eqz v2, :cond_0

    .line 1328636
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->s:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1328637
    const v2, 0x2e49f408

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1328638
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const v2, 0x1631b875

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setDefaultTagSuggestions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1328639
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->g:Ljava/util/List;

    .line 1328640
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->e()V

    .line 1328641
    return-void
.end method

.method public setTagSuggestions(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1328642
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    if-eqz v0, :cond_1

    .line 1328643
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    move v0, v0

    .line 1328644
    if-eqz v0, :cond_1

    .line 1328645
    :cond_0
    :goto_0
    return-void

    .line 1328646
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1328647
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->q:Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    .line 1328648
    iput-object v0, v1, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d:Ljava/util/List;

    .line 1328649
    iput-boolean v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->x:Z

    .line 1328650
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->t:Z

    move v0, v0

    .line 1328651
    if-eqz v0, :cond_2

    .line 1328652
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1328653
    invoke-static {p0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->i(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V

    goto :goto_0

    .line 1328654
    :cond_2
    iput-boolean v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->z:Z

    goto :goto_0
.end method

.method public setTagSuggestionsAdapter(LX/8JS;)V
    .locals 0

    .prologue
    .line 1328655
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->A:LX/8JS;

    .line 1328656
    return-void
.end method
