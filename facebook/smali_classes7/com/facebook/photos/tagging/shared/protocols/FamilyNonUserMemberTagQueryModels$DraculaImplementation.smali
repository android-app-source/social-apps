.class public final Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1329391
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1329392
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1329389
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1329390
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1329360
    if-nez p1, :cond_0

    move v0, v1

    .line 1329361
    :goto_0
    return v0

    .line 1329362
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1329363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1329364
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1329365
    const v2, 0x5f9b6617

    const/4 v5, 0x0

    .line 1329366
    if-nez v0, :cond_1

    move v4, v5

    .line 1329367
    :goto_1
    move v0, v4

    .line 1329368
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1329369
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1329370
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1329371
    :sswitch_1
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    .line 1329372
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1329373
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1329374
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1329375
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1329376
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1329377
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1329378
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1329379
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1329380
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1329381
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 1329382
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1329383
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1329384
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1329385
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1329386
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1329387
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1329388
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x45d848db -> :sswitch_0
        0x1e57dc8a -> :sswitch_2
        0x5f9b6617 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1329359
    new-instance v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1329354
    if-eqz p0, :cond_0

    .line 1329355
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1329356
    if-eq v0, p0, :cond_0

    .line 1329357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1329358
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1329341
    sparse-switch p2, :sswitch_data_0

    .line 1329342
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1329343
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1329344
    const v1, 0x5f9b6617

    .line 1329345
    if-eqz v0, :cond_0

    .line 1329346
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1329347
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1329348
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1329349
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1329350
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1329351
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1329352
    :sswitch_2
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    .line 1329353
    invoke-static {v0, p3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x45d848db -> :sswitch_0
        0x1e57dc8a -> :sswitch_1
        0x5f9b6617 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1329335
    if-eqz p1, :cond_0

    .line 1329336
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1329337
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    .line 1329338
    if-eq v0, v1, :cond_0

    .line 1329339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1329340
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1329334
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1329332
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1329333
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1329327
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1329328
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1329329
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1329330
    iput p2, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->b:I

    .line 1329331
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1329301
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1329326
    new-instance v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1329323
    iget v0, p0, LX/1vt;->c:I

    .line 1329324
    move v0, v0

    .line 1329325
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1329320
    iget v0, p0, LX/1vt;->c:I

    .line 1329321
    move v0, v0

    .line 1329322
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1329317
    iget v0, p0, LX/1vt;->b:I

    .line 1329318
    move v0, v0

    .line 1329319
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1329314
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1329315
    move-object v0, v0

    .line 1329316
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1329305
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1329306
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1329307
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1329308
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1329309
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1329310
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1329311
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1329312
    invoke-static {v3, v9, v2}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1329313
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1329302
    iget v0, p0, LX/1vt;->c:I

    .line 1329303
    move v0, v0

    .line 1329304
    return v0
.end method
