.class public final Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x170a92fe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1329566
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1329565
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1329548
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1329549
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1329585
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->f:Ljava/lang/String;

    .line 1329586
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1329567
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1329568
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x45d848db

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1329569
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1329570
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1329571
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1329572
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1329573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1329574
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1329575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1329576
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1329577
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x45d848db

    invoke-static {v2, v0, v3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1329578
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1329579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    .line 1329580
    iput v3, v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->e:I

    .line 1329581
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1329582
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1329583
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1329584
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1329563
    new-instance v0, LX/8Jr;

    invoke-direct {v0, p1}, LX/8Jr;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1329564
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1329560
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1329561
    const/4 v0, 0x0

    const v1, -0x45d848db

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->e:I

    .line 1329562
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1329558
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1329559
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1329557
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1329554
    new-instance v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-direct {v0}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;-><init>()V

    .line 1329555
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1329556
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1329553
    const v0, 0xe9c77cd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1329552
    const v0, 0x285feb

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFamilyNonUserMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1329550
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1329551
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
