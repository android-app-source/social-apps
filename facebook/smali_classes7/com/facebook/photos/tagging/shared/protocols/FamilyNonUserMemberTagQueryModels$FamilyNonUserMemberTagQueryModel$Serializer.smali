.class public final Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1329546
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    new-instance v1, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1329547
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1329545
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1329516
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1329517
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1329518
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1329519
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1329520
    if-eqz v2, :cond_3

    .line 1329521
    const-string v3, "family_non_user_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1329522
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1329523
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1329524
    if-eqz v3, :cond_2

    .line 1329525
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1329526
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1329527
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 1329528
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1329529
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1329530
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1329531
    if-eqz p0, :cond_0

    .line 1329532
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1329533
    invoke-static {v1, p0, p1, p2}, LX/8Jt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1329534
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1329535
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1329536
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1329537
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1329538
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1329539
    if-eqz v2, :cond_4

    .line 1329540
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1329541
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1329542
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1329543
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1329544
    check-cast p1, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Serializer;->a(Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
