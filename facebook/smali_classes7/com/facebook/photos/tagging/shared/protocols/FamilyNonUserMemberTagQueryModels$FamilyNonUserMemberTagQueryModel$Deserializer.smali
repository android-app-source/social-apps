.class public final Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1329393
    const-class v0, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    new-instance v1, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1329394
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1329395
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1329396
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1329397
    const/4 v2, 0x0

    .line 1329398
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1329399
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329400
    :goto_0
    move v1, v2

    .line 1329401
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1329402
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1329403
    new-instance v1, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;

    invoke-direct {v1}, Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;-><init>()V

    .line 1329404
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1329405
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1329406
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1329407
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1329408
    :cond_0
    return-object v1

    .line 1329409
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329410
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1329411
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1329412
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1329413
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1329414
    const-string v5, "family_non_user_members"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1329415
    const/4 v4, 0x0

    .line 1329416
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 1329417
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329418
    :goto_2
    move v3, v4

    .line 1329419
    goto :goto_1

    .line 1329420
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1329421
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1329422
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1329423
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1329424
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1329425
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1329426
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329427
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1329428
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1329429
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1329430
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 1329431
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1329432
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1329433
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_8

    .line 1329434
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1329435
    const/4 v6, 0x0

    .line 1329436
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_e

    .line 1329437
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329438
    :goto_5
    move v5, v6

    .line 1329439
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1329440
    :cond_8
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1329441
    goto :goto_3

    .line 1329442
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1329443
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1329444
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    move v3, v4

    goto :goto_3

    .line 1329445
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1329446
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_d

    .line 1329447
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1329448
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1329449
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v7, :cond_c

    .line 1329450
    const-string p0, "node"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1329451
    invoke-static {p1, v0}, LX/8Jt;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_6

    .line 1329452
    :cond_d
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1329453
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1329454
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_e
    move v5, v6

    goto :goto_6
.end method
