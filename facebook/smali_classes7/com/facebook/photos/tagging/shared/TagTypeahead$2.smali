.class public final Lcom/facebook/photos/tagging/shared/TagTypeahead$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328462
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1328463
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v2}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    .line 1328464
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v2, 0x1

    .line 1328465
    iput-boolean v2, v1, Lcom/facebook/photos/tagging/shared/TagTypeahead;->E:Z

    .line 1328466
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeahead$2;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, v1, Lcom/facebook/photos/tagging/shared/TagTypeahead;->h:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setMaxWidth(I)V

    .line 1328467
    return-void
.end method
