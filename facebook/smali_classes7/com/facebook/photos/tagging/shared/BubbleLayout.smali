.class public Lcom/facebook/photos/tagging/shared/BubbleLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:F

.field public final b:F

.field private final c:F

.field private final d:F

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Z

.field private h:Landroid/graphics/Path;

.field private i:F

.field private j:I

.field private k:I

.field public l:LX/8JG;

.field private m:LX/8Jh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1328066
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328067
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/16 v7, 0xff

    const/16 v6, 0xcc

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1327957
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1327958
    new-instance v0, LX/8Jh;

    invoke-direct {v0}, LX/8Jh;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    .line 1327959
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a:F

    .line 1327960
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    .line 1327961
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    .line 1327962
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->d:F

    .line 1327963
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->BubbleLayout:[I

    invoke-virtual {v0, p2, v1, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1327964
    :try_start_0
    const/16 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1327965
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1327966
    invoke-virtual {p0, v4}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setWillNotDraw(Z)V

    .line 1327967
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    float-to-int v0, v0

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    float-to-int v3, v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setPadding(IIII)V

    .line 1327968
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    .line 1327969
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->g:Z

    if-eqz v0, :cond_0

    .line 1327970
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v7, v7, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1327971
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1327972
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1327973
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1327974
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    .line 1327975
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1327976
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1327977
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1327978
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1327979
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    .line 1327980
    sget-object v0, LX/8JG;->UP:LX/8JG;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    .line 1327981
    return-void

    .line 1327982
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 1327983
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328068
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328069
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328070
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328071
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v2, v3

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v6, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328072
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328073
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328074
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v2, v3

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328075
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328076
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 1328077
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    sget-object v1, LX/8JG;->DOWN:LX/8JG;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    sget-object v1, LX/8JG;->UP:LX/8JG;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/tagging/shared/BubbleLayout;F)V
    .locals 0

    .prologue
    .line 1328078
    iput p1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    .line 1328079
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b()V

    .line 1328080
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->invalidate()V

    .line 1328081
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1328082
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    div-float/2addr v0, v4

    .line 1328083
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1328084
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a:F

    mul-float/2addr v0, v4

    .line 1328085
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    .line 1328086
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328087
    sget-object v2, LX/8JF;->a:[I

    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    invoke-virtual {v3}, LX/8JG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1328088
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    .line 1328089
    :goto_0
    return-void

    .line 1328090
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328091
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328092
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->d(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328093
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->g(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328094
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328095
    :pswitch_5
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    .line 1328096
    :pswitch_6
    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f(Landroid/graphics/Path;Landroid/graphics/RectF;F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private b(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328144
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328145
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328146
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328147
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328148
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v4, v5

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328149
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328150
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v6, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328151
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v4, v5

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328152
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328153
    return-void
.end method

.method private c(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v5, 0x42b40000    # 90.0f

    .line 1328097
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328098
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328099
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328100
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328101
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, -0x3ccc0000    # -180.0f

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328102
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328103
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328104
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v5, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328105
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328106
    return-void
.end method

.method private d(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v5, 0x42b40000    # 90.0f

    .line 1328107
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328108
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328109
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328110
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328111
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328112
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v5, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328113
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328114
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328115
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328116
    return-void
.end method

.method private e(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328117
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328118
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328119
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328120
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328121
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, -0x3ccc0000    # -180.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328122
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328123
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v2, v3

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328124
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328125
    return-void
.end method

.method private f(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328126
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328127
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328128
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328129
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v1, v3

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328130
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v2, v3

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v6, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328131
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328132
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328133
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328134
    return-void
.end method

.method private g(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328135
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328136
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328137
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328138
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328139
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v4, v5

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328140
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328141
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v3, v4

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v6, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328142
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328143
    return-void
.end method

.method private h(Landroid/graphics/Path;Landroid/graphics/RectF;F)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, 0x42b40000    # 90.0f

    .line 1328044
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 1328045
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v1, v3

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1328046
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328047
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1328048
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    sub-float/2addr v1, p3

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328049
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, p3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v0, v6, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328050
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, p3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v4, v5

    add-float/2addr v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v0, v1, v6}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 1328051
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 1328052
    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 6

    .prologue
    .line 1328053
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1328054
    :goto_0
    return-void

    .line 1328055
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getArrowOffset()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1328056
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    .line 1328057
    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1328058
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_1

    .line 1328059
    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    .line 1328060
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 1328061
    float-to-long v4, p2

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1328062
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1328063
    new-instance v3, LX/8JE;

    invoke-direct {v3, p0, v1, v0}, LX/8JE;-><init>(Lcom/facebook/photos/tagging/shared/BubbleLayout;FF)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1328064
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 1328065
    :cond_1
    invoke-static {p0, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a$redex0(Lcom/facebook/photos/tagging/shared/BubbleLayout;F)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(LX/8JG;LX/8Jh;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1328018
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1328019
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1328020
    iget-object v2, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    neg-int v3, v0

    div-int/lit8 v3, v3, 0x2

    neg-int v4, v1

    div-int/lit8 v4, v4, 0x2

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1328021
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget-object v1, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1328022
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    neg-float v1, v1

    float-to-int v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->c:F

    neg-float v2, v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 1328023
    sget-object v0, LX/8JG;->UP:LX/8JG;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/8JG;->UPRIGHT:LX/8JG;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/8JG;->UPLEFT:LX/8JG;

    if-ne p1, v0, :cond_1

    .line 1328024
    :cond_0
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    float-to-int v0, v0

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1328025
    iget-object v1, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1328026
    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1328027
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1328028
    :cond_1
    sget-object v0, LX/8JG;->DOWN:LX/8JG;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/8JG;->DOWNLEFT:LX/8JG;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/8JG;->DOWNRIGHT:LX/8JG;

    if-ne p1, v0, :cond_3

    .line 1328029
    :cond_2
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    float-to-int v0, v0

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1328030
    iget-object v1, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    neg-int v2, v0

    invoke-virtual {v1, v5, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1328031
    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    neg-int v0, v0

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 1328032
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1328033
    :cond_3
    sget-object v0, LX/8JG;->LEFT:LX/8JG;

    if-eq p1, v0, :cond_4

    sget-object v0, LX/8JG;->DOWNLEFT:LX/8JG;

    if-eq p1, v0, :cond_4

    sget-object v0, LX/8JG;->UPLEFT:LX/8JG;

    if-ne p1, v0, :cond_5

    .line 1328034
    :cond_4
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    float-to-int v0, v0

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1328035
    iget-object v1, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1328036
    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1328037
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1328038
    :cond_5
    sget-object v0, LX/8JG;->RIGHT:LX/8JG;

    if-eq p1, v0, :cond_6

    sget-object v0, LX/8JG;->DOWNRIGHT:LX/8JG;

    if-eq p1, v0, :cond_6

    sget-object v0, LX/8JG;->UPRIGHT:LX/8JG;

    if-ne p1, v0, :cond_7

    .line 1328039
    :cond_6
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    float-to-int v0, v0

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1328040
    iget-object v1, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    neg-int v2, v0

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1328041
    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    neg-int v0, v0

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1328042
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1328043
    :cond_7
    return-void
.end method

.method public getArrowDirection()LX/8JG;
    .locals 1

    .prologue
    .line 1328017
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    return-object v0
.end method

.method public getArrowLength()F
    .locals 1

    .prologue
    .line 1328016
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    return v0
.end method

.method public getArrowOffset()F
    .locals 2

    .prologue
    .line 1328013
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1328014
    const/4 v0, 0x0

    .line 1328015
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b:F

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->d:F

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public getArrowPosition()F
    .locals 1

    .prologue
    .line 1328012
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->i:F

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1328008
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1328009
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1328010
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->h:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1328011
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1328005
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1328006
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b()V

    .line 1328007
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1327999
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->j:I

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->j:I

    if-le v0, v1, :cond_0

    .line 1328000
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->j:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 1328001
    :cond_0
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->k:I

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->k:I

    if-le v0, v1, :cond_1

    .line 1328002
    iget v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->k:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 1328003
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1328004
    return-void
.end method

.method public setArrowDirection(LX/8JG;)V
    .locals 4

    .prologue
    .line 1327993
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->l:LX/8JG;

    .line 1327994
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(LX/8JG;LX/8Jh;)V

    .line 1327995
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    invoke-virtual {v0}, LX/8Jh;->a()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    invoke-virtual {v1}, LX/8Jh;->b()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    invoke-virtual {v2}, LX/8Jh;->c()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->m:LX/8Jh;

    invoke-virtual {v3}, LX/8Jh;->d()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setPadding(IIII)V

    .line 1327996
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->b()V

    .line 1327997
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->invalidate()V

    .line 1327998
    return-void
.end method

.method public setArrowPosition(F)V
    .locals 1

    .prologue
    .line 1327990
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1327991
    :goto_0
    return-void

    .line 1327992
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(FF)V

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 1327987
    iput p1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->k:I

    .line 1327988
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->requestLayout()V

    .line 1327989
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0

    .prologue
    .line 1327984
    iput p1, p0, Lcom/facebook/photos/tagging/shared/BubbleLayout;->j:I

    .line 1327985
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->requestLayout()V

    .line 1327986
    return-void
.end method
