.class public Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;
.super Landroid/widget/ArrayAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/8nB;

.field public c:LX/8JW;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/widget/Filter;

.field public f:Z

.field public g:LX/0Sh;

.field public h:Landroid/widget/Filter$FilterListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1328876
    const-class v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;

    const-string v1, "photo_tag_friends"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8nB;LX/0Sh;)V
    .locals 2

    .prologue
    .line 1328871
    const v0, 0x7f03146d

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1328872
    iput-object p2, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    .line 1328873
    new-instance v0, LX/8JZ;

    invoke-direct {v0, p0}, LX/8JZ;-><init>(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->e:Landroid/widget/Filter;

    .line 1328874
    iput-object p3, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->g:LX/0Sh;

    .line 1328875
    return-void
.end method

.method private a(I)LX/8Ja;
    .locals 1

    .prologue
    .line 1328870
    invoke-virtual {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    if-eqz v0, :cond_0

    sget-object v0, LX/8Ja;->SECTION_HEADER_VIEW:LX/8Ja;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/8Ja;->ITEM_VIEW:LX/8Ja;

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1328863
    if-nez p2, :cond_0

    .line 1328864
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1328865
    const v1, 0x7f03146c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1328866
    :cond_0
    const v0, 0x7f0d2e7d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1328867
    invoke-virtual {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    iget-object v1, v1, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1328868
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1328869
    return-object p2
.end method

.method private a(Ljava/util/List;LX/8nE;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "LX/8nE;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1328851
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1328852
    if-eqz v0, :cond_0

    .line 1328853
    invoke-direct {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1328854
    iget-object v6, v0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v6, v6

    .line 1328855
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v3

    .line 1328856
    :goto_1
    if-eqz v4, :cond_1

    if-nez v1, :cond_1

    if-eqz p3, :cond_1

    .line 1328857
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2}, LX/8nE;->getCustomizedNameResourceId()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1328858
    new-instance v6, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    invoke-virtual {p2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v1, v7}, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->add(Ljava/lang/Object;)V

    move v1, v3

    .line 1328859
    :cond_1
    if-eqz v4, :cond_0

    .line 1328860
    invoke-virtual {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v4, v2

    .line 1328861
    goto :goto_1

    .line 1328862
    :cond_3
    return-void
.end method

.method private a(Lcom/facebook/tagging/model/TaggingProfile;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1328840
    if-nez p1, :cond_1

    .line 1328841
    :cond_0
    :goto_0
    return v0

    .line 1328842
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->c:LX/8JW;

    if-eqz v1, :cond_3

    .line 1328843
    sget-object v1, LX/7Gr;->TEXT:LX/7Gr;

    .line 1328844
    iget-object v2, p1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v2, v2

    .line 1328845
    invoke-virtual {v1, v2}, LX/7Gr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1328846
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->c:LX/8JW;

    invoke-virtual {p1}, Lcom/facebook/tagging/model/TaggingProfile;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/8JW;->b(Ljava/lang/String;)Z

    move-result v1

    .line 1328847
    :goto_1
    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1328848
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->c:LX/8JW;

    .line 1328849
    iget-wide v4, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v2, v4

    .line 1328850
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/8JW;->a(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1328818
    if-nez p2, :cond_0

    .line 1328819
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1328820
    const v1, 0x7f03146d

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1328821
    :cond_0
    const v0, 0x7f0d2e7e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1328822
    const v1, 0x7f0d2e7f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1328823
    invoke-virtual {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1328824
    iget-object v3, v2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1328825
    if-eqz v3, :cond_3

    .line 1328826
    iget-object v3, v2, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v3, v3

    .line 1328827
    sget-object v4, LX/7Gr;->TEXT:LX/7Gr;

    if-eq v3, v4, :cond_3

    .line 1328828
    iget-object v3, v2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1328829
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1328830
    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1328831
    :cond_1
    :goto_0
    iget-object v3, v2, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1328832
    if-nez v3, :cond_2

    .line 1328833
    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1328834
    :cond_2
    iget-object v0, v2, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1328835
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1328836
    return-object p2

    .line 1328837
    :cond_3
    iget-object v3, v2, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v3, v3

    .line 1328838
    sget-object v4, LX/7Gr;->TEXT:LX/7Gr;

    if-ne v3, v4, :cond_1

    .line 1328839
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1328812
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    invoke-virtual {v1}, LX/8nB;->d()LX/0Px;

    move-result-object v2

    .line 1328813
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    move v1, v0

    .line 1328814
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1328815
    invoke-static {v0}, LX/8nE;->valueOf(Ljava/lang/String;)LX/8nE;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a(Ljava/util/List;LX/8nE;Z)V

    goto :goto_1

    .line 1328816
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1328817
    :cond_1
    return-void
.end method

.method public static e$redex0(Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;)V
    .locals 3

    .prologue
    .line 1328807
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328808
    :cond_0
    return-void

    .line 1328809
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1328810
    invoke-direct {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1328811
    invoke-virtual {p0, v0}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1328793
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->e:Landroid/widget/Filter;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1328794
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1328805
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->f:Z

    .line 1328806
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1328803
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->f:Z

    .line 1328804
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1328802
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b:LX/8nB;

    invoke-virtual {v0}, LX/8nB;->a()Z

    move-result v0

    return v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1328801
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->e:Landroid/widget/Filter;

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1328800
    invoke-direct {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a(I)LX/8Ja;

    move-result-object v0

    invoke-virtual {v0}, LX/8Ja;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1328796
    invoke-static {}, LX/8Ja;->values()[LX/8Ja;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->getItemViewType(I)I

    move-result v1

    aget-object v0, v0, v1

    .line 1328797
    sget-object v1, LX/8Ja;->ITEM_VIEW:LX/8Ja;

    invoke-virtual {v0, v1}, LX/8Ja;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328798
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1328799
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/photos/tagging/shared/TagTypeaheadAdapter;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1328795
    invoke-static {}, LX/8Ja;->values()[LX/8Ja;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
