.class public Lcom/facebook/photos/tagging/shared/FaceBoxesView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final b:Landroid/graphics/Matrix;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/LinearGradient;

.field private f:Landroid/animation/ValueAnimator;

.field private g:Landroid/animation/Animator$AnimatorListener;

.field private h:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field public i:LX/8JH;

.field private j:Z

.field private final k:Landroid/graphics/Matrix;

.field private final l:Landroid/graphics/Matrix;

.field private final m:Landroid/graphics/Matrix;

.field private final n:Landroid/graphics/Matrix;

.field private final o:Landroid/graphics/Matrix;

.field private final p:Landroid/graphics/RectF;

.field private final q:Landroid/graphics/RectF;

.field private final r:[F

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/graphics/RectF;

.field private final u:Landroid/graphics/RectF;

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1328345
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->b:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1328329
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1328330
    new-instance v0, LX/8JI;

    invoke-direct {v0, p0}, LX/8JI;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->g:Landroid/animation/Animator$AnimatorListener;

    .line 1328331
    new-instance v0, LX/8JJ;

    invoke-direct {v0, p0}, LX/8JJ;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->h:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1328332
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    .line 1328333
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    .line 1328334
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    .line 1328335
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    .line 1328336
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    .line 1328337
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    .line 1328338
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->q:Landroid/graphics/RectF;

    .line 1328339
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->r:[F

    .line 1328340
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    .line 1328341
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->u:Landroid/graphics/RectF;

    .line 1328342
    iput-boolean v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    .line 1328343
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c()V

    .line 1328344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1328313
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1328314
    new-instance v0, LX/8JI;

    invoke-direct {v0, p0}, LX/8JI;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->g:Landroid/animation/Animator$AnimatorListener;

    .line 1328315
    new-instance v0, LX/8JJ;

    invoke-direct {v0, p0}, LX/8JJ;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->h:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1328316
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    .line 1328317
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    .line 1328318
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    .line 1328319
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    .line 1328320
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    .line 1328321
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    .line 1328322
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->q:Landroid/graphics/RectF;

    .line 1328323
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->r:[F

    .line 1328324
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    .line 1328325
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->u:Landroid/graphics/RectF;

    .line 1328326
    iput-boolean v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    .line 1328327
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c()V

    .line 1328328
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1328196
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1328197
    new-instance v0, LX/8JI;

    invoke-direct {v0, p0}, LX/8JI;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->g:Landroid/animation/Animator$AnimatorListener;

    .line 1328198
    new-instance v0, LX/8JJ;

    invoke-direct {v0, p0}, LX/8JJ;-><init>(Lcom/facebook/photos/tagging/shared/FaceBoxesView;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->h:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1328199
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    .line 1328200
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    .line 1328201
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    .line 1328202
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    .line 1328203
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    .line 1328204
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    .line 1328205
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->q:Landroid/graphics/RectF;

    .line 1328206
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->r:[F

    .line 1328207
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    .line 1328208
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->u:Landroid/graphics/RectF;

    .line 1328209
    iput-boolean v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    .line 1328210
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c()V

    .line 1328211
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 1328302
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1328303
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328304
    :cond_0
    :goto_0
    return-void

    .line 1328305
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    .line 1328306
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1328307
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x514

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1328308
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 1328309
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->g:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1328310
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->h:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1328311
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 1328312
    :array_0
    .array-data 4
        -0x41666666    # -0.3f
        0x3fa66666    # 1.3f
    .end array-data
.end method

.method private a(Landroid/graphics/Matrix;FF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1328292
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    if-nez v0, :cond_0

    .line 1328293
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1328294
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2, p3, v1, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 1328295
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1328296
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1328297
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1328298
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1328299
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1328300
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->invalidate()V

    .line 1328301
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a:LX/0Sh;

    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1328282
    const-class v0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-static {v0, p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1328283
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    .line 1328284
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    const v1, -0x5a000001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1328285
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1328286
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1328287
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d:Landroid/graphics/Paint;

    .line 1328288
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1328289
    new-instance v0, Landroid/graphics/LinearGradient;

    const/4 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    new-array v5, v4, [I

    fill-array-data v5, :array_0

    new-array v6, v4, [F

    fill-array-data v6, :array_1

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v4, v2

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->e:Landroid/graphics/LinearGradient;

    .line 1328290
    return-void

    .line 1328291
    :array_0
    .array-data 4
        -0x5a000001
        -0x1
        -0x1
        -0x5a000001
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1328278
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1328279
    sget-object v0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->b:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Landroid/graphics/Matrix;FF)V

    .line 1328280
    return-void

    .line 1328281
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCompensation()F
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1328273
    iget-boolean v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    if-eqz v1, :cond_0

    .line 1328274
    :goto_0
    return v0

    .line 1328275
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->r:[F

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1328276
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->r:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 1328277
    div-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1328271
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(I)V

    .line 1328272
    return-void
.end method

.method public final a(IILandroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1328267
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328268
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-direct {p0, p3, v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Landroid/graphics/Matrix;FF)V

    .line 1328269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->j:Z

    .line 1328270
    return-void
.end method

.method public final a(Landroid/graphics/PointF;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1328258
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1328259
    :goto_0
    return v0

    .line 1328260
    :cond_0
    const/4 v0, 0x2

    new-array v3, v0, [F

    iget v0, p1, Landroid/graphics/PointF;->x:F

    aput v0, v3, v1

    iget v0, p1, Landroid/graphics/PointF;->y:F

    aput v0, v3, v2

    .line 1328261
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1328262
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1328263
    aget v5, v3, v1

    aget v6, v3, v2

    invoke-virtual {v0, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1328264
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->i:LX/8JH;

    invoke-interface {v1, v0}, LX/8JH;->a(Landroid/graphics/RectF;)V

    move v0, v2

    .line 1328265
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1328266
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1328252
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1328253
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 1328254
    :goto_0
    return-void

    .line 1328255
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1328256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    .line 1328257
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->invalidate()V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x16807c2d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1328249
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->b()V

    .line 1328250
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1328251
    const/16 v1, 0x2d

    const v2, 0x587c6732

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1328223
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1328224
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1328225
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1328226
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1328227
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1328228
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 1328229
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    const v2, 0x3e99999a    # 0.3f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1328230
    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1328231
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1328232
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->e:Landroid/graphics/LinearGradient;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/LinearGradient;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 1328233
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->e:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1328234
    :goto_0
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getCompensation()F

    move-result v0

    .line 1328235
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 1328236
    mul-float v2, v0, v1

    mul-float/2addr v2, v4

    .line 1328237
    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, v0

    .line 1328238
    div-float v3, v1, v4

    .line 1328239
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1328240
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1328241
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 1328242
    iget-object v4, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1328243
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->q:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v3

    iget-object v5, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v3

    iget-object v6, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    add-float/2addr v6, v3

    iget-object v7, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v7, v3

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1328244
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->q:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1328245
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->p:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1328246
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0

    .line 1328247
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1328248
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2c008f41

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1328219
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1328220
    iget-boolean v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->j:Z

    if-nez v1, :cond_0

    .line 1328221
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d()V

    .line 1328222
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x52d3bc82

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setDraweeMatrix(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1328212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    .line 1328213
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1328214
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1328215
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1328216
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1328217
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->invalidate()V

    .line 1328218
    return-void
.end method

.method public setFaceBoxes(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1328187
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1328188
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->b()V

    .line 1328189
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1328190
    if-eqz p1, :cond_0

    .line 1328191
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1328192
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->j:Z

    if-nez v0, :cond_1

    .line 1328193
    invoke-direct {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->d()V

    .line 1328194
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->invalidate()V

    .line 1328195
    return-void
.end method

.method public setFaceboxClickedListener(LX/8JH;)V
    .locals 0

    .prologue
    .line 1328185
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->i:LX/8JH;

    .line 1328186
    return-void
.end method

.method public setImageBounds(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 1328175
    iput-object p1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->t:Landroid/graphics/RectF;

    .line 1328176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->v:Z

    .line 1328177
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1328178
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->t:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 1328179
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->u:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->t:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1328180
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1328181
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1328182
    iget-object v0, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->m:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->n:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1328183
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->invalidate()V

    .line 1328184
    return-void
.end method
