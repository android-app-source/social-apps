.class public Lcom/facebook/photos/tagging/store/TagStoreCopy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/tagging/store/TagStoreCopy;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/photos/tagging/store/TagStoreCopy;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1169495
    new-instance v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/facebook/photos/tagging/store/TagStoreCopy;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a:Lcom/facebook/photos/tagging/store/TagStoreCopy;

    .line 1169496
    new-instance v0, LX/75R;

    invoke-direct {v0}, LX/75R;-><init>()V

    sput-object v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1169497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169498
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    .line 1169499
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    const-class v1, Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1169500
    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1169501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169502
    iput-object p1, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    .line 1169503
    return-void
.end method

.method public static a(LX/75Q;LX/0Px;)Lcom/facebook/photos/tagging/store/TagStoreCopy;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/75Q;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Lcom/facebook/photos/tagging/store/TagStoreCopy;"
        }
    .end annotation

    .prologue
    .line 1169504
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1169505
    if-eqz p1, :cond_1

    .line 1169506
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1169507
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->PHOTO:LX/4gF;

    if-ne v4, v5, :cond_0

    .line 1169508
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    .line 1169509
    invoke-virtual {p0, v0}, LX/75Q;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169510
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1169511
    :cond_1
    new-instance v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;

    invoke-direct {v0, v2}, Lcom/facebook/photos/tagging/store/TagStoreCopy;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static a(LX/75Q;Ljava/util/List;)Lcom/facebook/photos/tagging/store/TagStoreCopy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/75Q;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ">;)",
            "Lcom/facebook/photos/tagging/store/TagStoreCopy;"
        }
    .end annotation

    .prologue
    .line 1169512
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1169513
    if-eqz p1, :cond_0

    .line 1169514
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaIdKey;

    .line 1169515
    invoke-virtual {p0, v0}, LX/75Q;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1169516
    :cond_0
    new-instance v0, Lcom/facebook/photos/tagging/store/TagStoreCopy;

    invoke-direct {v0, v1}, Lcom/facebook/photos/tagging/store/TagStoreCopy;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)Z
    .locals 1

    .prologue
    .line 1169517
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1169518
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1169519
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169520
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1169521
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1169522
    return-void
.end method
