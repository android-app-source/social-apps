.class public Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1169158
    new-instance v0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->a:Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    .line 1169159
    new-instance v0, LX/75G;

    invoke-direct {v0}, LX/75G;-><init>()V

    sput-object v0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1169154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    .line 1169156
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    const-class v1, Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1169157
    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1169151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169152
    iput-object p1, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    .line 1169153
    return-void
.end method

.method public static a(LX/75F;Ljava/util/List;)Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/75F;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ">;)",
            "Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;"
        }
    .end annotation

    .prologue
    .line 1169146
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1169147
    if-eqz p1, :cond_0

    .line 1169148
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaIdKey;

    .line 1169149
    invoke-virtual {p0, v0}, LX/75F;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1169150
    :cond_0
    new-instance v0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;

    invoke-direct {v0, v1}, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)Z
    .locals 1

    .prologue
    .line 1169140
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1169141
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169145
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169144
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1169142
    iget-object v0, p0, Lcom/facebook/photos/tagging/store/FaceBoxStoreCopy;->b:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1169143
    return-void
.end method
