.class public Lcom/facebook/photos/base/tagging/Tag;
.super LX/759;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/362;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/photos/base/tagging/TagTarget;

.field public b:Lcom/facebook/user/model/Name;

.field public c:J

.field public d:Z

.field public e:Z

.field public f:LX/7Gr;

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/364;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168907
    new-instance v0, LX/758;

    invoke-direct {v0}, LX/758;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/tagging/Tag;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    .line 1168973
    invoke-direct {p0}, LX/759;-><init>()V

    .line 1168974
    const-class v0, Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/TagTarget;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1168975
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    .line 1168976
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    .line 1168977
    invoke-static {}, LX/7Gr;->values()[LX/7Gr;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    .line 1168978
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    .line 1168979
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->d:Z

    .line 1168980
    const-class v0, LX/364;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    .line 1168981
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1168982
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1168983
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/364;

    .line 1168984
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 1168985
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 1168986
    iget-object v5, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1168987
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1168988
    :cond_0
    return-void
.end method

.method private constructor <init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JLX/7Gr;)V
    .locals 9

    .prologue
    .line 1168971
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;)V

    .line 1168972
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JLX/7Gr;Z)V
    .locals 9

    .prologue
    .line 1168969
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v7, p5

    move v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;Z)V

    .line 1168970
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;)V
    .locals 9

    .prologue
    .line 1168967
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;Z)V

    .line 1168968
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;Z)V
    .locals 1

    .prologue
    .line 1168958
    invoke-direct {p0}, LX/759;-><init>()V

    .line 1168959
    iput-object p1, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1168960
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    .line 1168961
    iput-wide p3, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    .line 1168962
    iput-boolean p5, p0, Lcom/facebook/photos/base/tagging/Tag;->d:Z

    .line 1168963
    iput-boolean p7, p0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    .line 1168964
    iput-object p6, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    .line 1168965
    const-class v0, LX/364;

    invoke-static {v0}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    .line 1168966
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;
    .locals 7

    .prologue
    .line 1168949
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v0, v0

    .line 1168950
    instance-of v0, v0, Lcom/facebook/photos/base/tagging/TagPoint;

    if-eqz v0, :cond_0

    .line 1168951
    new-instance v2, Lcom/facebook/photos/base/tagging/TagPoint;

    new-instance v0, Landroid/graphics/PointF;

    iget v1, p2, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v1, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v1}, Lcom/facebook/photos/base/tagging/TagTarget;->n()Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/base/tagging/TagPoint;-><init>(Landroid/graphics/PointF;Ljava/util/List;)V

    .line 1168952
    :goto_0
    new-instance v1, Lcom/facebook/photos/base/tagging/Tag;

    iget-object v3, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    iget-wide v4, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    iget-object v6, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JLX/7Gr;)V

    return-object v1

    .line 1168953
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    instance-of v0, v0, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_1

    .line 1168954
    new-instance v2, Lcom/facebook/photos/base/tagging/FaceBox;

    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->n()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1168955
    iget-boolean v3, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v0, v3

    .line 1168956
    invoke-direct {v2, p1, v1, v0}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;Z)V

    goto :goto_0

    .line 1168957
    :cond_1
    new-instance v2, Lcom/facebook/photos/base/tagging/FaceBoxStub;

    invoke-direct {v2, p1}, Lcom/facebook/photos/base/tagging/FaceBoxStub;-><init>(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public final a()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1168948
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1168946
    iput-wide p1, p0, Lcom/facebook/photos/base/tagging/Tag;->h:J

    .line 1168947
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/364;",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1168944
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1168945
    return-void
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1168943
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1168942
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/facebook/photos/base/tagging/TagTarget;
    .locals 1

    .prologue
    .line 1168941
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1168940
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1168939
    iget-wide v0, p0, Lcom/facebook/photos/base/tagging/Tag;->h:J

    return-wide v0
.end method

.method public final f()Lcom/facebook/user/model/Name;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168938
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1168937
    iget-wide v0, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    return-wide v0
.end method

.method public final i()LX/7Gr;
    .locals 1

    .prologue
    .line 1168936
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1168935
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    sget-object v1, LX/7Gr;->TEXT:LX/7Gr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1168934
    iget-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1168933
    iget-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    return v0
.end method

.method public final m()Lorg/json/JSONObject;
    .locals 8

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1168920
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-double v0, v0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1168921
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "x"

    iget-object v2, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v4

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "y"

    iget-object v2, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->f()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v4

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    .line 1168922
    iget-wide v6, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v2, v6

    .line 1168923
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 1168924
    const-string v1, "tag_uid"

    .line 1168925
    iget-wide v6, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v2, v6

    .line 1168926
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1168927
    :goto_1
    return-object v0

    .line 1168928
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1168929
    :cond_1
    const-string v1, "tag_text"

    iget-object v2, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1168930
    :catch_0
    move-exception v0

    .line 1168931
    const-string v1, ""

    const-string v2, "inconceivable JSON exception"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1168932
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1168908
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168909
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168910
    iget-wide v0, p0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1168911
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->f:LX/7Gr;

    invoke-virtual {v0}, LX/7Gr;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168912
    iget-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168913
    iget-boolean v0, p0, Lcom/facebook/photos/base/tagging/Tag;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168914
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168915
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/364;

    .line 1168916
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1168917
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1168918
    iget-object v1, p0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    goto :goto_0

    .line 1168919
    :cond_0
    return-void
.end method
