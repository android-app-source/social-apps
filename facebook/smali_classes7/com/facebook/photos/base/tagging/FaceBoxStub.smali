.class public Lcom/facebook/photos/base/tagging/FaceBoxStub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/photos/base/tagging/TagTarget;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBoxStub;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/graphics/RectF;

.field public b:Landroid/graphics/PointF;

.field public c:Landroid/graphics/PointF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168895
    new-instance v0, LX/756;

    invoke-direct {v0}, LX/756;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 1168890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168891
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->a:Landroid/graphics/RectF;

    .line 1168892
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    .line 1168893
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    .line 1168894
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1168870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168871
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->a:Landroid/graphics/RectF;

    .line 1168872
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    .line 1168873
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1168874
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 1168875
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    .line 1168876
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1168877
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 1168878
    return-void
.end method


# virtual methods
.method public final d()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1168889
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->a:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1168888
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1168887
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final f()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1168886
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1168885
    const/4 v0, 0x0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1168879
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->a:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168880
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1168881
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1168882
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1168883
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/FaceBoxStub;->c:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1168884
    return-void
.end method
