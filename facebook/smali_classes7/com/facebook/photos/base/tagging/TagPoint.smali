.class public Lcom/facebook/photos/base/tagging/TagPoint;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/photos/base/tagging/TagTarget;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/tagging/TagPoint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/graphics/PointF;

.field public b:Landroid/graphics/RectF;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169031
    new-instance v0, LX/75A;

    invoke-direct {v0}, LX/75A;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/tagging/TagPoint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/PointF;Ljava/util/List;)V
    .locals 5
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1168992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168993
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    .line 1168994
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->b:Landroid/graphics/RectF;

    .line 1168995
    iput-object p2, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    .line 1168996
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1168997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168998
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->b:Landroid/graphics/RectF;

    .line 1168999
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->b:Landroid/graphics/RectF;

    .line 1169000
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1169001
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1169002
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1169003
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1169004
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    .line 1169005
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    .line 1169006
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1169007
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 1169008
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    .line 1169009
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1169010
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    .line 1169011
    :goto_0
    return-void

    .line 1169012
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    const-class v1, Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    goto :goto_0
.end method


# virtual methods
.method public final d()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1169013
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->b:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169014
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1169015
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final f()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1169016
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1169017
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1169018
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->b:Landroid/graphics/RectF;

    .line 1169019
    iget p2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169020
    iget p2, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169021
    iget p2, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169022
    iget p2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169023
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->a:Landroid/graphics/PointF;

    .line 1169024
    iget p2, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169025
    iget p2, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1169026
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1169027
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1169028
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/TagPoint;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1169029
    :goto_0
    return-void

    .line 1169030
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
