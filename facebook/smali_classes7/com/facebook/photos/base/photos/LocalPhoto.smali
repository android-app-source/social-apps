.class public Lcom/facebook/photos/base/photos/LocalPhoto;
.super LX/74x;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/photos/LocalPhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Ljava/lang/String;

.field public e:I

.field public f:Z

.field private g:Lcom/facebook/ipc/media/MediaIdKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168745
    new-instance v0, LX/74u;

    invoke-direct {v0}, LX/74u;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/photos/LocalPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/util/List;Ljava/util/List;Ljava/lang/String;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1168688
    invoke-direct {p0, p1, p2, p3, p4}, LX/74x;-><init>(JLjava/util/List;Ljava/util/List;)V

    .line 1168689
    iput-object p5, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    .line 1168690
    iput p6, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    .line 1168691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    .line 1168692
    new-instance v0, Lcom/facebook/ipc/media/MediaIdKey;

    iget-object v1, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->g:Lcom/facebook/ipc/media/MediaIdKey;

    .line 1168693
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;JLjava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1168739
    invoke-direct {p0, p2, p3, p4, p5}, LX/74x;-><init>(JLjava/util/List;Ljava/util/List;)V

    .line 1168740
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    .line 1168741
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    .line 1168742
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    .line 1168743
    new-instance v0, Lcom/facebook/ipc/media/MediaIdKey;

    iget-object v1, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    invoke-direct {v0, v1, p2, p3}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->g:Lcom/facebook/ipc/media/MediaIdKey;

    .line 1168744
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;JLjava/util/List;Ljava/util/List;B)V
    .locals 0

    .prologue
    .line 1168738
    invoke-direct/range {p0 .. p5}, Lcom/facebook/photos/base/photos/LocalPhoto;-><init>(Landroid/os/Parcel;JLjava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static a(Ljava/lang/String;ILX/74z;)LX/4n9;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v4, 0xf0

    const/4 v1, 0x1

    .line 1168716
    if-nez p0, :cond_0

    .line 1168717
    const/4 v0, 0x0

    .line 1168718
    :goto_0
    return-object v0

    .line 1168719
    :cond_0
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v2

    .line 1168720
    sget-object v0, LX/74v;->a:[I

    invoke-virtual {p2}, LX/74z;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1168721
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1168722
    :pswitch_0
    invoke-virtual {v2, v1}, LX/4n6;->a(Z)LX/4n6;

    .line 1168723
    new-instance v0, LX/74y;

    sget-object v3, LX/74z;->SCREENNAIL:LX/74z;

    invoke-direct {v0, p1, v3}, LX/74y;-><init>(ILX/74z;)V

    .line 1168724
    :goto_1
    sget-object v3, LX/74z;->THUMBNAIL:LX/74z;

    if-ne p2, v3, :cond_1

    .line 1168725
    :goto_2
    iput-boolean v1, v2, LX/4n6;->e:Z

    .line 1168726
    move-object v1, v2

    .line 1168727
    invoke-virtual {v1}, LX/4n6;->f()LX/4n5;

    move-result-object v1

    .line 1168728
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1168729
    invoke-static {v2}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v2

    .line 1168730
    iput-object v0, v2, LX/4n8;->d:LX/4n2;

    .line 1168731
    move-object v0, v2

    .line 1168732
    iput-object v1, v0, LX/4n8;->e:LX/4n5;

    .line 1168733
    move-object v0, v0

    .line 1168734
    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object v0

    goto :goto_0

    .line 1168735
    :pswitch_1
    invoke-virtual {v2, v4, v4}, LX/4n6;->a(II)LX/4n6;

    .line 1168736
    new-instance v0, LX/74y;

    sget-object v3, LX/74z;->THUMBNAIL:LX/74z;

    invoke-direct {v0, p1, v3}, LX/74y;-><init>(ILX/74z;)V

    goto :goto_1

    .line 1168737
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/74z;)LX/4n9;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168715
    iget-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    iget v1, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    invoke-static {v0, v1, p1}, Lcom/facebook/photos/base/photos/LocalPhoto;->a(Ljava/lang/String;ILX/74z;)LX/4n9;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/ipc/media/MediaIdKey;
    .locals 1

    .prologue
    .line 1168714
    iget-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->g:Lcom/facebook/ipc/media/MediaIdKey;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1168713
    iget v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1168712
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1168694
    iget-object v0, p0, LX/74x;->d:Ljava/util/List;

    move-object v0, v0

    .line 1168695
    if-eqz v0, :cond_0

    .line 1168696
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168697
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1168698
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 1168699
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168700
    :cond_1
    iget-object v0, p0, LX/74x;->c:Ljava/util/List;

    if-nez v0, :cond_3

    .line 1168701
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168702
    :cond_2
    iget-wide v2, p0, LX/74w;->a:J

    move-wide v0, v2

    .line 1168703
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1168704
    iget v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168705
    iget-object v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168706
    iget-boolean v0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168707
    return-void

    .line 1168708
    :cond_3
    iget-object v0, p0, LX/74x;->c:Ljava/util/List;

    .line 1168709
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1168710
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1168711
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1
.end method
