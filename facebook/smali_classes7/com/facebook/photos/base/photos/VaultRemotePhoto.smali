.class public Lcom/facebook/photos/base/photos/VaultRemotePhoto;
.super Lcom/facebook/photos/base/photos/VaultPhoto;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/photos/VaultRemotePhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Ljava/lang/String;

.field private d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168825
    new-instance v0, LX/753;

    invoke-direct {v0}, LX/753;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;J)V
    .locals 1

    .prologue
    .line 1168861
    invoke-direct {p0}, Lcom/facebook/photos/base/photos/VaultPhoto;-><init>()V

    .line 1168862
    iput-wide p1, p0, LX/74w;->a:J

    .line 1168863
    iput-object p3, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    .line 1168864
    iput-wide p4, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    .line 1168865
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1168855
    invoke-direct {p0}, Lcom/facebook/photos/base/photos/VaultPhoto;-><init>()V

    .line 1168856
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->a:J

    .line 1168857
    const-class v0, Landroid/graphics/PointF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->b:Landroid/graphics/PointF;

    .line 1168858
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    .line 1168859
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    .line 1168860
    return-void
.end method


# virtual methods
.method public final a(LX/74z;)LX/4n9;
    .locals 4

    .prologue
    const/16 v3, 0xf0

    .line 1168837
    iget-object v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1168838
    const/4 v0, 0x0

    .line 1168839
    :goto_0
    return-object v0

    .line 1168840
    :cond_0
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v1

    .line 1168841
    sget-object v0, LX/754;->a:[I

    invoke-virtual {p1}, LX/74z;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1168842
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1168843
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/4n6;->a(Z)LX/4n6;

    .line 1168844
    new-instance v0, LX/755;

    sget-object v2, LX/74z;->SCREENNAIL:LX/74z;

    invoke-direct {v0, v2}, LX/755;-><init>(LX/74z;)V

    .line 1168845
    :goto_1
    invoke-virtual {v1}, LX/4n6;->f()LX/4n5;

    move-result-object v1

    .line 1168846
    iget-object v2, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1168847
    invoke-static {v2}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v2

    .line 1168848
    iput-object v0, v2, LX/4n8;->d:LX/4n2;

    .line 1168849
    move-object v0, v2

    .line 1168850
    iput-object v1, v0, LX/4n8;->e:LX/4n5;

    .line 1168851
    move-object v0, v0

    .line 1168852
    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object v0

    goto :goto_0

    .line 1168853
    :pswitch_1
    invoke-virtual {v1, v3, v3}, LX/4n6;->a(II)LX/4n6;

    .line 1168854
    new-instance v0, LX/755;

    sget-object v2, LX/74z;->THUMBNAIL:LX/74z;

    invoke-direct {v0, v2}, LX/755;-><init>(LX/74z;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1168866
    iget-wide v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1168836
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1168832
    instance-of v1, p1, Lcom/facebook/photos/base/photos/VaultRemotePhoto;

    if-nez v1, :cond_1

    .line 1168833
    :cond_0
    :goto_0
    return v0

    .line 1168834
    :cond_1
    check-cast p1, Lcom/facebook/photos/base/photos/VaultRemotePhoto;

    .line 1168835
    iget-wide v2, p0, LX/74w;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, LX/74w;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/74w;->b:Landroid/graphics/PointF;

    iget-object v2, p1, LX/74w;->b:Landroid/graphics/PointF;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1168831
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LX/74w;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/74w;->b:Landroid/graphics/PointF;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1168826
    iget-wide v0, p0, LX/74w;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1168827
    iget-object v0, p0, LX/74w;->b:Landroid/graphics/PointF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168828
    iget-object v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168829
    iget-wide v0, p0, Lcom/facebook/photos/base/photos/VaultRemotePhoto;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1168830
    return-void
.end method
