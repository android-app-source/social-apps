.class public Lcom/facebook/photos/base/media/PhotoItem;
.super Lcom/facebook/ipc/media/MediaItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;"
        }
    .end annotation
.end field

.field private static c:I


# instance fields
.field public d:Z

.field public e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

.field public f:Z

.field public g:Z

.field private h:Ljava/lang/String;

.field public i:Lcom/facebook/photos/base/photos/LocalPhoto;

.field public j:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168613
    const/4 v0, 0x0

    sput v0, Lcom/facebook/photos/base/media/PhotoItem;->c:I

    .line 1168614
    new-instance v0, LX/74r;

    invoke-direct {v0}, LX/74r;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/media/PhotoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/74k;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1168605
    iget-object v1, p1, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    iget-wide v2, p1, LX/74k;->b:J

    iget-wide v4, p1, LX/74k;->a:J

    iget-object v6, p1, LX/74k;->g:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/ipc/media/MediaItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;JJLjava/lang/String;)V

    .line 1168606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    .line 1168607
    iput-object v8, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    .line 1168608
    new-instance v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v7

    move-object v5, v8

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/photos/LocalPhoto;-><init>(JLjava/util/List;Ljava/util/List;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1168609
    iget-boolean v0, p1, LX/74k;->c:Z

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    .line 1168610
    iget-object v0, p1, LX/74k;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1168611
    iget-boolean v0, p1, LX/74k;->d:Z

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->f:Z

    .line 1168612
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1168592
    invoke-direct {p0, p1}, Lcom/facebook/ipc/media/MediaItem;-><init>(Landroid/os/Parcel;)V

    .line 1168593
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    .line 1168594
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    .line 1168595
    const-class v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1168596
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    .line 1168597
    const-class v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1168598
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->f:Z

    .line 1168599
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    .line 1168600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    .line 1168601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1168602
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1168603
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    .line 1168604
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 1168571
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    .line 1168572
    return-void
.end method

.method public final r()LX/74w;
    .locals 1

    .prologue
    .line 1168591
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    return-object v0
.end method

.method public final t()Lcom/facebook/bitmaps/SphericalPhotoMetadata;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168590
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 1168589
    iget-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1168588
    invoke-static {p0}, LX/75E;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1168577
    invoke-super {p0, p1, p2}, Lcom/facebook/ipc/media/MediaItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1168578
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168579
    iget-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168580
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1168581
    iget-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168582
    iget-boolean v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1168583
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168584
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1168585
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168586
    return-void

    .line 1168587
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1168573
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1168574
    sget v0, Lcom/facebook/photos/base/media/PhotoItem;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    .line 1168575
    sget v0, Lcom/facebook/photos/base/media/PhotoItem;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/photos/base/media/PhotoItem;->c:I

    .line 1168576
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/media/PhotoItem;->h:Ljava/lang/String;

    return-object v0
.end method
