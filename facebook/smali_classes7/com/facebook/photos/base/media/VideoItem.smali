.class public Lcom/facebook/photos/base/media/VideoItem;
.super Lcom/facebook/ipc/media/MediaItem;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/media/VideoItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/String;

.field public final d:J

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1168630
    new-instance v0, LX/74t;

    invoke-direct {v0}, LX/74t;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/media/VideoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/74m;)V
    .locals 7

    .prologue
    .line 1168639
    iget-object v1, p1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    iget-wide v2, p1, LX/74m;->a:J

    iget-wide v4, p1, LX/74m;->b:J

    iget-object v6, p1, LX/74m;->f:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/ipc/media/MediaItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;JJLjava/lang/String;)V

    .line 1168640
    iget-object v0, p1, LX/74m;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->c:Ljava/lang/String;

    .line 1168641
    iget-wide v0, p1, LX/74m;->d:J

    iput-wide v0, p0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    .line 1168642
    iget-object v0, p1, LX/74m;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->e:Ljava/lang/String;

    .line 1168643
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1168631
    invoke-direct {p0, p1}, Lcom/facebook/ipc/media/MediaItem;-><init>(Landroid/os/Parcel;)V

    .line 1168632
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->c:Ljava/lang/String;

    .line 1168633
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    .line 1168634
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->e:Ljava/lang/String;

    .line 1168635
    return-void
.end method


# virtual methods
.method public final s()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168636
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1168637
    const/4 v0, 0x0

    .line 1168638
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final t()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1168622
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1168623
    const/4 v0, 0x0

    .line 1168624
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1168625
    invoke-super {p0, p1, p2}, Lcom/facebook/ipc/media/MediaItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1168626
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168627
    iget-wide v0, p0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1168628
    iget-object v0, p0, Lcom/facebook/photos/base/media/VideoItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1168629
    return-void
.end method
