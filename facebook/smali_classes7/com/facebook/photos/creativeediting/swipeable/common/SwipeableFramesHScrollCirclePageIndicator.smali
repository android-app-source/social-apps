.class public Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;
.super Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;
.source ""


# instance fields
.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Paint;

.field private h:F

.field private i:Z

.field private j:Z

.field public k:Z

.field private l:F

.field private m:I

.field private n:Landroid/graphics/Bitmap;

.field private o:Landroid/graphics/Bitmap;

.field private p:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1319076
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;-><init>(Landroid/content/Context;)V

    .line 1319077
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    .line 1319078
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    .line 1319079
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    .line 1319080
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    .line 1319081
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1319082
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1319069
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1319070
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    .line 1319071
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    .line 1319072
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    .line 1319073
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    .line 1319074
    invoke-direct {p0, p1, p2}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1319075
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1319062
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1319063
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    .line 1319064
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    .line 1319065
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    .line 1319066
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    .line 1319067
    invoke-direct {p0, p1, p2}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1319068
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1318962
    const/16 v0, 0xa

    iput v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->c:I

    .line 1318963
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->a:I

    .line 1318964
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->SwipeableFramesHScrollCirclePageIndicator:[I

    invoke-virtual {v0, p2, v1, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1318965
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->h:F

    .line 1318966
    const/16 v1, 0x0

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->m:I

    .line 1318967
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->l:F

    .line 1318968
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPageColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318969
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1318970
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getFillColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318971
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1318972
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getStrokeColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318973
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getStrokeWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1318974
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1318975
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    const/16 v2, 0x2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318976
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    iget v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->l:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1318977
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1318978
    iget v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->h:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    .line 1318979
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    move v1, v1

    .line 1318980
    iput v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->h:F

    .line 1318981
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1318982
    return-void
.end method

.method private getCircleCount()I
    .locals 2

    .prologue
    .line 1319060
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    move v0, v0

    .line 1319061
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    if-ge v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    goto :goto_0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const v12, 0x800005

    const v9, 0x800003

    const/4 v11, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 1319004
    invoke-direct {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getCircleCount()I

    move-result v7

    .line 1319005
    if-nez v7, :cond_0

    .line 1319006
    :goto_0
    return-void

    .line 1319007
    :cond_0
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v0, :cond_3

    .line 1319008
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getWidth()I

    move-result v3

    .line 1319009
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingLeft()I

    move-result v2

    .line 1319010
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingRight()I

    move-result v1

    .line 1319011
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingTop()I

    move-result v0

    .line 1319012
    :goto_1
    iget v4, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    move v4, v4

    .line 1319013
    mul-float v5, v4, v10

    iget v6, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->h:F

    add-float v8, v5, v6

    .line 1319014
    int-to-float v0, v0

    add-float v5, v0, v4

    .line 1319015
    iget v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->m:I

    and-int/2addr v0, v9

    if-eq v0, v9, :cond_4

    .line 1319016
    iget v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->m:I

    and-int/2addr v0, v12

    if-ne v0, v12, :cond_4

    .line 1319017
    sub-int v0, v3, v1

    int-to-float v0, v0

    int-to-float v6, v7

    mul-float/2addr v6, v8

    sub-float/2addr v0, v6

    .line 1319018
    :goto_2
    iget v6, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->m:I

    and-int/lit8 v6, v6, 0x11

    const/16 v9, 0x11

    if-ne v6, v9, :cond_1

    .line 1319019
    sub-int v2, v3, v2

    sub-int v1, v2, v1

    int-to-float v1, v1

    int-to-float v2, v7

    mul-float/2addr v2, v8

    sub-float/2addr v1, v2

    div-float/2addr v1, v10

    add-float/2addr v0, v1

    .line 1319020
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    cmpl-float v1, v1, v11

    if-lez v1, :cond_12

    .line 1319021
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v10

    sub-float v1, v4, v1

    .line 1319022
    :goto_3
    const/4 v2, 0x0

    move v6, v2

    :goto_4
    if-ge v6, v7, :cond_c

    .line 1319023
    int-to-float v2, v6

    mul-float/2addr v2, v8

    add-float/2addr v2, v0

    .line 1319024
    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v3, :cond_5

    move v3, v2

    move v2, v5

    .line 1319025
    :goto_5
    if-nez v6, :cond_7

    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->i:Z

    if-nez v9, :cond_7

    .line 1319026
    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v9, :cond_6

    .line 1319027
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->o:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v3, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1319028
    :cond_2
    :goto_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_4

    .line 1319029
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getHeight()I

    move-result v3

    .line 1319030
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingTop()I

    move-result v2

    .line 1319031
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingBottom()I

    move-result v1

    .line 1319032
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getPaddingLeft()I

    move-result v0

    goto :goto_1

    .line 1319033
    :cond_4
    int-to-float v0, v2

    add-float/2addr v0, v4

    goto :goto_2

    :cond_5
    move v3, v5

    .line 1319034
    goto :goto_5

    .line 1319035
    :cond_6
    iget v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->l:F

    cmpl-float v9, v9, v11

    if-eqz v9, :cond_2

    .line 1319036
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v4, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 1319037
    :cond_7
    add-int/lit8 v9, v7, -0x1

    if-ne v6, v9, :cond_9

    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v9, :cond_9

    .line 1319038
    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->k:Z

    if-eqz v9, :cond_8

    .line 1319039
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->p:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v3, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 1319040
    :cond_8
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->n:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v3, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 1319041
    :cond_9
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {v9}, Landroid/graphics/Paint;->getAlpha()I

    move-result v9

    if-lez v9, :cond_a

    .line 1319042
    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v9, :cond_b

    .line 1319043
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->n:Landroid/graphics/Bitmap;

    iget-object v10, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v3, v2, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1319044
    :cond_a
    :goto_7
    cmpl-float v9, v1, v4

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-nez v9, :cond_2

    .line 1319045
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v4, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 1319046
    :cond_b
    iget-object v9, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_7

    .line 1319047
    :cond_c
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    .line 1319048
    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v2, :cond_d

    .line 1319049
    add-float/2addr v0, v1

    move v13, v5

    move v5, v0

    move v0, v13

    .line 1319050
    :goto_8
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    add-int/lit8 v2, v7, -0x1

    if-ne v1, v2, :cond_f

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v1, :cond_f

    .line 1319051
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->k:Z

    if-eqz v1, :cond_e

    .line 1319052
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->p:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1319053
    :cond_d
    add-float/2addr v0, v1

    goto :goto_8

    .line 1319054
    :cond_e
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->n:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1319055
    :cond_f
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->i:Z

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v1, :cond_10

    .line 1319056
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->o:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1319057
    :cond_10
    iget-boolean v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    if-eqz v1, :cond_11

    .line 1319058
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->n:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1319059
    :cond_11
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0, v4, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_12
    move v1, v4

    goto/16 :goto_3
.end method

.method public setFillFirstCircle(Z)V
    .locals 0

    .prologue
    .line 1319001
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->i:Z

    .line 1319002
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->invalidate()V

    .line 1319003
    return-void
.end method

.method public setIsPurpleRainStyle(Z)V
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1318985
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->j:Z

    .line 1318986
    if-eqz p1, :cond_0

    .line 1318987
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1318988
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 1318989
    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v1, v2

    div-float/2addr v1, v3

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v1, v2

    .line 1318990
    mul-float v2, v1, v3

    float-to-int v2, v2

    .line 1318991
    invoke-virtual {p0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setRadius(F)V

    .line 1318992
    int-to-float v1, v2

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v1, v3

    iput v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->h:F

    .line 1318993
    const v1, 0x7f021559

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1, v2, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->n:Landroid/graphics/Bitmap;

    .line 1318994
    const v1, 0x7f02155b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1, v2, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->o:Landroid/graphics/Bitmap;

    .line 1318995
    const v1, 0x7f02155a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1, v2, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->p:Landroid/graphics/Bitmap;

    .line 1318996
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    const v2, 0x7f0a0100

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318997
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318998
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    const v2, 0x7f0a0100

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1318999
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->invalidate()V

    .line 1319000
    return-void
.end method

.method public setShowLastDotAsStar(Z)V
    .locals 0

    .prologue
    .line 1318983
    iput-boolean p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->k:Z

    .line 1318984
    return-void
.end method
