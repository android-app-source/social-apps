.class public Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile o:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;


# instance fields
.field public e:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/1FZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0hB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/8Fz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/8Yg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1318622
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a:Ljava/lang/String;

    .line 1318623
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    const-string v1, "creative_editing_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1318624
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->c:Ljava/util/List;

    .line 1318625
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1318626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318627
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;FLcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1318647
    sget-object v1, LX/8G5;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1318648
    :goto_0
    :pswitch_0
    return v0

    .line 1318649
    :pswitch_1
    invoke-virtual {p0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 1318650
    :pswitch_2
    invoke-virtual {p0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p1

    float-to-int v0, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;
    .locals 13

    .prologue
    .line 1318628
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->o:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    if-nez v0, :cond_1

    .line 1318629
    const-class v1, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    monitor-enter v1

    .line 1318630
    :try_start_0
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->o:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1318631
    if-eqz v2, :cond_0

    .line 1318632
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1318633
    new-instance v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-direct {v3}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;-><init>()V

    .line 1318634
    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v9

    check-cast v9, LX/1FZ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    invoke-static {v0}, LX/8Fz;->a(LX/0QB;)LX/8Fz;

    move-result-object v12

    check-cast v12, LX/8Fz;

    invoke-static {v0}, LX/8Yg;->a(LX/0QB;)LX/8Yg;

    move-result-object p0

    check-cast p0, LX/8Yg;

    .line 1318635
    iput-object v4, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->e:LX/1HI;

    iput-object v5, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->f:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->g:LX/0TD;

    iput-object v7, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->h:LX/0Sh;

    iput-object v8, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->i:Landroid/content/Context;

    iput-object v9, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->j:LX/1FZ;

    iput-object v10, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->k:LX/03V;

    iput-object v11, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->l:LX/0hB;

    iput-object v12, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->m:LX/8Fz;

    iput-object p0, v3, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->n:LX/8Yg;

    .line 1318636
    move-object v0, v3

    .line 1318637
    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->o:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1318638
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1318639
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1318640
    :cond_1
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->o:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    return-object v0

    .line 1318641
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1318642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;)Ljava/io/File;
    .locals 4

    .prologue
    .line 1318643
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->i:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Frames_Text"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1318644
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1318645
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 1318646
    :cond_0
    return-object v0
.end method

.method private a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;)Ljava/lang/String;
    .locals 13

    .prologue
    .line 1318605
    invoke-static {p1}, LX/8Fz;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318606
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->m:LX/8Fz;

    const/4 v1, 0x0

    .line 1318607
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 1318608
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1318609
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v7, :cond_0

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$ClientGeneratedTextInfoModel;

    .line 1318610
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$ClientGeneratedTextInfoModel;->a()I

    move-result v8

    invoke-virtual {v4, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1318611
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$ClientGeneratedTextInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    move-result-object v3

    .line 1318612
    sget-object v9, LX/8Fy;->a:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1318613
    const-string v9, ""

    :goto_1
    move-object v8, v9

    .line 1318614
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$ClientGeneratedTextInfoModel;->a()I

    move-result v3

    .line 1318615
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1318616
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1318617
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1318618
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1318619
    :goto_2
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1318620
    :pswitch_0
    iget-object v9, v0, LX/8Fz;->a:LX/11R;

    sget-object v10, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    iget-object v11, v0, LX/8Fz;->b:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    invoke-virtual {v9, v10, v11, v12}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1318621
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\."

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/0Px;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1318582
    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;)Ljava/io/File;

    move-result-object v7

    .line 1318583
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_1

    .line 1318584
    :cond_0
    return-void

    .line 1318585
    :cond_1
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1318586
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1318587
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v10

    const/4 v2, 0x0

    move v6, v2

    :goto_0
    if-ge v6, v10, :cond_8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318588
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 1318589
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v12, :cond_4

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;

    .line 1318590
    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1318591
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_2

    .line 1318592
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v14, v4}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1318593
    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 1318594
    :cond_4
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_7

    aget-object v11, v4, v3

    .line 1318595
    invoke-virtual {v8, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_6

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1318596
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1318597
    :cond_5
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1318598
    :cond_6
    invoke-virtual {v8, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v14

    invoke-virtual {v14}, LX/0SF;->a()J

    move-result-wide v14

    const-wide/32 v16, 0x48190800

    sub-long v14, v14, v16

    cmp-long v12, v12, v14

    if-gez v12, :cond_5

    .line 1318599
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1318600
    :cond_7
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_0

    .line 1318601
    :cond_8
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_0

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 1318602
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1318603
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1318604
    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5
.end method

.method private a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Typeface;Ljava/lang/String;DDLcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;Z)V
    .locals 10
    .param p4    # Landroid/graphics/Typeface;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1318544
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->h:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1318545
    const/4 v4, 0x0

    .line 1318546
    const/4 v3, 0x0

    .line 1318547
    :try_start_0
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1318548
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1318549
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->k:LX/03V;

    sget-object v5, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected failure: Frame "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " has an empty text asset!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/03V;->a(LX/0VG;)V

    .line 1318550
    :cond_0
    :goto_0
    return-void

    .line 1318551
    :cond_1
    if-eqz p11, :cond_4

    const/16 v2, 0x3e8

    .line 1318552
    :goto_1
    move-wide/from16 v0, p6

    double-to-int v6, v0

    mul-int/2addr v2, v6

    div-int/lit8 v2, v2, 0x64

    int-to-float v6, v2

    .line 1318553
    const-string v2, "#"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1318554
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "#"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p3

    .line 1318555
    :cond_2
    :try_start_1
    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 1318556
    :goto_2
    :try_start_2
    new-instance v7, Landroid/text/TextPaint;

    const/4 v8, 0x3

    invoke-direct {v7, v8}, Landroid/text/TextPaint;-><init>(I)V

    .line 1318557
    invoke-virtual {v7, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 1318558
    invoke-virtual {v7, v6}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1318559
    if-nez p4, :cond_3

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->i:Landroid/content/Context;

    sget-object v6, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v8, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v9, 0x0

    invoke-static {v2, v6, v8, v9}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p4

    :cond_3
    invoke-virtual {v7, p4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1318560
    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v6

    float-to-int v6, v6

    add-int/2addr v6, v2

    .line 1318561
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->l:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    move-wide/from16 v0, p8

    double-to-int v8, v0

    mul-int/2addr v2, v8

    div-int/lit8 v8, v2, 0x64

    .line 1318562
    if-lez v8, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1318563
    if-lez v6, :cond_6

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1318564
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->j:LX/1FZ;

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v8, v6, v9}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v3

    .line 1318565
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1318566
    new-instance v6, Landroid/graphics/Canvas;

    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v6, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1318567
    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    move-object/from16 v0, p10

    invoke-static {v6, v2, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Landroid/graphics/Canvas;FLcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v6}, Landroid/graphics/Canvas;->getHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {v6, v5, v2, v8, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1318568
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1318569
    :try_start_3
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {v2, v4, v6, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1318570
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 1318571
    if-eqz v3, :cond_0

    .line 1318572
    invoke-virtual {v3}, LX/1FJ;->close()V

    goto/16 :goto_0

    .line 1318573
    :cond_4
    const/16 v2, 0x2ee

    goto/16 :goto_1

    .line 1318574
    :catch_0
    const/4 v2, -0x1

    goto/16 :goto_2

    .line 1318575
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 1318576
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 1318577
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v4, :cond_7

    .line 1318578
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1318579
    :cond_7
    if-eqz v3, :cond_8

    .line 1318580
    invoke-virtual {v3}, LX/1FJ;->close()V

    :cond_8
    throw v2

    .line 1318581
    :catchall_1
    move-exception v2

    move-object v4, v5

    goto :goto_5
.end method

.method public static d(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ")",
            "LX/0Px",
            "<",
            "LX/8GF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1318533
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1318534
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1318535
    :goto_0
    return-object v0

    .line 1318536
    :cond_1
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1318537
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_3

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;

    move v2, v3

    .line 1318538
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1318539
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v1

    .line 1318540
    new-instance v8, LX/8GF;

    invoke-direct {v8, v1}, LX/8GF;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1318541
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1318542
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1318543
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized e(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 20

    .prologue
    .line 1318503
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_1

    .line 1318504
    :cond_0
    monitor-exit p0

    return-void

    .line 1318505
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->n:LX/8Yg;

    invoke-static/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v16

    .line 1318506
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, LX/0Px;->size()I

    move-result v18

    const/4 v2, 0x0

    move v15, v2

    :goto_0
    move/from16 v0, v18

    if-ge v15, v0, :cond_0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;

    move-object v14, v0

    .line 1318507
    sget-object v2, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1318508
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3, v2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 1318509
    invoke-static {v14}, LX/8Fz;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1318510
    :cond_3
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v2

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/File;->setLastModified(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1318511
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1318512
    :cond_4
    const/4 v4, 0x1

    if-ne v2, v4, :cond_6

    const/4 v13, 0x1

    .line 1318513
    :goto_2
    if-eqz v13, :cond_7

    .line 1318514
    :try_start_2
    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->l()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;

    move-result-object v2

    .line 1318515
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;->b()D

    move-result-wide v8

    .line 1318516
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;->c()D

    move-result-wide v10

    .line 1318517
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v12

    .line 1318518
    :goto_3
    const/4 v6, 0x0

    .line 1318519
    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 1318520
    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Typeface;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v6, v2

    .line 1318521
    :cond_5
    :try_start_3
    sget-object v2, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1318522
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->bc_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v13}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Typeface;Ljava/lang/String;DDLcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1318523
    :try_start_4
    sget-object v2, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1318524
    :cond_6
    const/4 v13, 0x0

    goto :goto_2

    .line 1318525
    :cond_7
    invoke-virtual {v14}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;

    move-result-object v2

    .line 1318526
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;->b()D

    move-result-wide v8

    .line 1318527
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;->c()D

    move-result-wide v10

    .line 1318528
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v12

    goto :goto_3

    .line 1318529
    :catch_0
    move-exception v2

    .line 1318530
    :try_start_5
    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a:Ljava/lang/String;

    const-string v5, "i/o exception while generating frame asset"

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1318531
    :try_start_6
    sget-object v2, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :catchall_1
    move-exception v2

    sget-object v4, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1318532
    :cond_8
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;Ljava/lang/String;I)Ljava/io/File;
    .locals 6

    .prologue
    .line 1318496
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1318497
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$CustomFontModel$EdgesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$MediaEffectCustomFontResourceFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1318498
    :goto_0
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1318499
    invoke-static {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;)Ljava/io/File;

    move-result-object v2

    .line 1318500
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->bc_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v3

    .line 1318501
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 1318502
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/0Px;LX/8G6;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;",
            "LX/8G6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1318494
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->g:LX/0TD;

    new-instance v1, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/0Px;LX/8G6;)V

    const v2, -0x377a81aa

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1318495
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 6

    .prologue
    .line 1318488
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aX_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 1318489
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;

    .line 1318490
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1318491
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->e:LX/1HI;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    sget-object v5, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v0, v5}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 1318492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1318493
    :cond_0
    return-void
.end method
