.class public Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:LX/5iG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1318651
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1318652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1318653
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1318654
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1318655
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1318656
    return-void
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 3
    .param p0    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 1318657
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1318658
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    iget v1, p0, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, p0, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, p0, Landroid/graphics/RectF;->right:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1318659
    invoke-super {p0, p1}, Landroid/widget/ImageView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1318660
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    if-nez v0, :cond_1

    .line 1318661
    :cond_0
    return-void

    .line 1318662
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->b:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    .line 1318663
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->b:Landroid/graphics/RectF;

    .line 1318664
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    invoke-virtual {v0}, LX/5iG;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1318665
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    invoke-virtual {v4, v0}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1318666
    iget-object v4, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    invoke-virtual {v4, v0}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;

    move-result-object v4

    invoke-virtual {v4}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1318667
    iget-object v5, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->b:Landroid/graphics/RectF;

    invoke-static {p1, v4, v0, v5}, LX/8GJ;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Lcom/facebook/photos/creativeediting/model/StickerParams;Landroid/graphics/RectF;)V

    .line 1318668
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public setActualImageBounds(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1318669
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->b:Landroid/graphics/RectF;

    invoke-static {v0, p1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318670
    :goto_0
    return-void

    .line 1318671
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->b:Landroid/graphics/RectF;

    .line 1318672
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->invalidate()V

    goto :goto_0
.end method

.method public setSwipeableItem(LX/5iG;)V
    .locals 0

    .prologue
    .line 1318673
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    .line 1318674
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->invalidate()V

    .line 1318675
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1318676
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->a:LX/5iG;

    invoke-virtual {v0, p1}, LX/5iG;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318677
    const/4 v0, 0x1

    .line 1318678
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
