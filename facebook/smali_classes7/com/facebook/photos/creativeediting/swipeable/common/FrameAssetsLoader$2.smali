.class public final Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/8G6;

.field public final synthetic c:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/0Px;LX/8G6;)V
    .locals 0

    .prologue
    .line 1318452
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->c:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    iput-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->a:LX/0Px;

    iput-object p3, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->b:LX/8G6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1318453
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->c:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->a:LX/0Px;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader$2;->b:LX/8G6;

    const/4 v5, 0x0

    .line 1318454
    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->h:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 1318455
    invoke-static {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/0Px;)V

    .line 1318456
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_3

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318457
    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v9, 0x0

    .line 1318458
    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    .line 1318459
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aX_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;->a()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    move v10, v9

    .line 1318460
    :goto_1
    if-ge v10, v13, :cond_2

    invoke-virtual {v12, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;

    .line 1318461
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 1318462
    iget-object v8, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->e:LX/1HI;

    invoke-virtual {v8, v14}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1318463
    const/4 v8, 0x1

    .line 1318464
    :goto_2
    if-nez v8, :cond_0

    .line 1318465
    iget-object v8, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->e:LX/1HI;

    invoke-static {v14}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v14

    sget-object p0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v14, p0}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v8

    .line 1318466
    invoke-static {v8}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v8

    .line 1318467
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1318468
    :cond_0
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_1

    .line 1318469
    :cond_1
    :try_start_0
    iget-object v8, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->e:LX/1HI;

    invoke-virtual {v8, v14}, LX/1HI;->d(Landroid/net/Uri;)LX/1ca;

    move-result-object v8

    invoke-static {v8}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v8

    const p0, -0x4aa0f2b4

    invoke-static {v8, p0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    goto :goto_2

    .line 1318470
    :catch_0
    move v8, v9

    goto :goto_2

    :catch_1
    move v8, v9

    goto :goto_2

    .line 1318471
    :cond_2
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    invoke-static {v8}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v8, v8

    .line 1318472
    aput-object v8, v7, v5

    const/4 v8, 0x1

    .line 1318473
    iget-object v9, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->n:LX/8Yg;

    invoke-static {v3}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->d(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)LX/0Px;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/8Yg;->a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 1318474
    new-instance v10, LX/8G4;

    invoke-direct {v10, v0, v3}, LX/8G4;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    iget-object v11, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->g:LX/0TD;

    invoke-static {v9, v10, v11}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object v9, v9

    .line 1318475
    aput-object v9, v7, v8

    invoke-static {v7}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v8, LX/8G3;

    invoke-direct {v8, v0, v2, v3}, LX/8G3;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/8G6;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    iget-object v3, v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, v8, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1318476
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_0

    .line 1318477
    :cond_3
    return-void
.end method
