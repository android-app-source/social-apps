.class public final Lcom/facebook/photos/creativeediting/utilities/CreativeEditingFileManager$DeleteOldDirectoryScheduler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8GS;


# direct methods
.method public constructor <init>(LX/8GS;)V
    .locals 0

    .prologue
    .line 1319493
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingFileManager$DeleteOldDirectoryScheduler$1;->a:LX/8GS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1319494
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingFileManager$DeleteOldDirectoryScheduler$1;->a:LX/8GS;

    const/4 v1, 0x0

    .line 1319495
    iget-object v2, v0, LX/8GS;->a:LX/8GT;

    iget-object v2, v2, LX/8GT;->a:Landroid/content/Context;

    const-string v3, "ce"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    .line 1319496
    if-nez v2, :cond_1

    .line 1319497
    :cond_0
    return-void

    .line 1319498
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1319499
    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1319500
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319501
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1319502
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    sub-long/2addr v5, v7

    .line 1319503
    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-ltz v5, :cond_2

    .line 1319504
    invoke-static {v4}, LX/2W9;->a(Ljava/io/File;)Z

    .line 1319505
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 1319506
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
