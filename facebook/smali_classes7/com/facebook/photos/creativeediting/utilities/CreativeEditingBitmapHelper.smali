.class public Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:I

.field private final c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final d:LX/8GZ;

.field private final e:Landroid/content/Context;

.field private final f:LX/1HI;

.field private final g:LX/1FZ;

.field private final h:Lcom/facebook/photos/imageprocessing/FiltersEngine;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1319482
    const-class v0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    const-string v1, "creative_editing_in_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;Ljava/util/concurrent/ExecutorService;LX/8GZ;Landroid/content/Context;LX/1FZ;Lcom/facebook/photos/imageprocessing/FiltersEngine;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319484
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->b:I

    .line 1319485
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->f:LX/1HI;

    .line 1319486
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->c:Ljava/util/concurrent/ExecutorService;

    .line 1319487
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->d:LX/8GZ;

    .line 1319488
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->e:Landroid/content/Context;

    .line 1319489
    iput-object p5, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->g:LX/1FZ;

    .line 1319490
    iput-object p6, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1319491
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;
    .locals 1

    .prologue
    .line 1319492
    invoke-static {p0}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->b(LX/0QB;)Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;II)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "II)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/16 v0, 0x3e8

    .line 1319472
    if-lez p2, :cond_0

    .line 1319473
    :goto_0
    if-lez p3, :cond_1

    .line 1319474
    :goto_1
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    invoke-direct {v1, p2, p3}, LX/1o9;-><init>(II)V

    .line 1319475
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1319476
    move-object v0, v0

    .line 1319477
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1319478
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->f:LX/1HI;

    sget-object v2, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 1319479
    return-object v0

    :cond_0
    move p2, v0

    .line 1319480
    goto :goto_0

    :cond_1
    move p3, v0

    .line 1319481
    goto :goto_1
.end method

.method private a(Ljava/util/List;LX/362;III)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;",
            "LX/362;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 1319449
    instance-of v0, p2, LX/5i8;

    if-nez v0, :cond_0

    .line 1319450
    :goto_0
    return-void

    .line 1319451
    :cond_0
    check-cast p2, LX/5i8;

    .line 1319452
    int-to-float v0, p3

    invoke-interface {p2}, LX/5i8;->e()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 1319453
    int-to-float v0, p4

    invoke-interface {p2}, LX/5i8;->f()F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1319454
    const/16 v2, 0x5a

    if-eq p5, v2, :cond_1

    const/16 v2, 0x10e

    if-ne p5, v2, :cond_2

    :cond_1
    move v3, v1

    move v1, v0

    move v0, v3

    .line 1319455
    :cond_2
    invoke-interface {p2}, LX/5i8;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v2, v1, v0}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Landroid/net/Uri;II)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;
    .locals 7

    .prologue
    .line 1319470
    new-instance v0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object v3

    check-cast v3, LX/8GZ;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v5

    check-cast v5, LX/1FZ;

    invoke-static {p0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(LX/0QB;)Lcom/facebook/photos/imageprocessing/FiltersEngine;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;-><init>(LX/1HI;Ljava/util/concurrent/ExecutorService;LX/8GZ;Landroid/content/Context;LX/1FZ;Lcom/facebook/photos/imageprocessing/FiltersEngine;)V

    .line 1319471
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;IIILjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Landroid/graphics/RectF;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "III",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "LX/0Px",
            "<",
            "LX/362;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;",
            "Landroid/graphics/RectF;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1319456
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1319457
    invoke-direct/range {p0 .. p3}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Landroid/net/Uri;II)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1319458
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->d:LX/8GZ;

    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v1, v0, v3}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1319459
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->d:LX/8GZ;

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 1319460
    if-eqz v7, :cond_0

    .line 1319461
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/362;

    move-object v1, p0

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    .line 1319462
    invoke-direct/range {v1 .. v6}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Ljava/util/List;LX/362;III)V

    goto :goto_0

    .line 1319463
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->d:LX/8GZ;

    new-instance v3, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5, v6, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    move/from16 v0, p4

    invoke-virtual {v1, v3, v0}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1319464
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->d:LX/8GZ;

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/8GZ;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 1319465
    if-eqz v8, :cond_1

    .line 1319466
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/362;

    move-object v1, p0

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    .line 1319467
    invoke-direct/range {v1 .. v6}, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a(Ljava/util/List;LX/362;III)V

    goto :goto_1

    .line 1319468
    :cond_1
    invoke-static {v2}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v12

    .line 1319469
    new-instance v1, LX/8GR;

    if-nez v7, :cond_2

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    :goto_2
    if-nez v8, :cond_3

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v3

    :goto_3
    iget-object v5, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->h:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    iget-object v10, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->f:LX/1HI;

    iget-object v11, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->g:LX/1FZ;

    move/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object v8, p1

    move/from16 v9, p10

    invoke-direct/range {v1 .. v11}, LX/8GR;-><init>(LX/0Px;LX/0Px;ILcom/facebook/photos/imageprocessing/FiltersEngine;Ljava/lang/String;LX/0Px;Landroid/net/Uri;ZLX/1HI;LX/1FZ;)V

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v12, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    return-object v1

    :cond_2
    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_2

    :cond_3
    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto :goto_3
.end method
