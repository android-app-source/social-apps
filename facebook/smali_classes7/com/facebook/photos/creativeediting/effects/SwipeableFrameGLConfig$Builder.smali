.class public final Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:Ljava/lang/String;

.field public final mFrame:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1318268
    const-class v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1318262
    new-instance v0, LX/8Ft;

    invoke-direct {v0}, LX/8Ft;-><init>()V

    .line 1318263
    const/4 v0, 0x0

    move-object v0, v0

    .line 1318264
    sput-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318265
    new-instance v0, LX/8Fu;

    invoke-direct {v0}, LX/8Fu;-><init>()V

    .line 1318266
    const-string v0, "SwipeableFrame"

    move-object v0, v0

    .line 1318267
    sput-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1318258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318259
    sget-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->mFrame:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318260
    sget-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->c:Ljava/lang/String;

    .line 1318261
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 1

    .prologue
    .line 1318254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318255
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->mFrame:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318256
    sget-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->c:Ljava/lang/String;

    .line 1318257
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
    .locals 2

    .prologue
    .line 1318251
    new-instance v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;-><init>(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;)V

    return-object v0
.end method

.method public setRenderKey(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1318252
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->c:Ljava/lang/String;

    .line 1318253
    return-object p0
.end method
