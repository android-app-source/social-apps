.class public Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/videocodec/effects/common/GLRendererConfig;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

.field private final b:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1318296
    const-class v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1318297
    const-class v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1318302
    new-instance v0, LX/8Fs;

    invoke-direct {v0}, LX/8Fs;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1318280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318281
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    .line 1318283
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;)V
    .locals 1

    .prologue
    .line 1318298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318299
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->mFrame:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318300
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    .line 1318301
    return-void
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;
    .locals 2

    .prologue
    .line 1318294
    new-instance v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig$Builder;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1318295
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1318285
    if-ne p0, p1, :cond_1

    .line 1318286
    :cond_0
    :goto_0
    return v0

    .line 1318287
    :cond_1
    instance-of v2, p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1318288
    goto :goto_0

    .line 1318289
    :cond_2
    check-cast p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    .line 1318290
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1318291
    goto :goto_0

    .line 1318292
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1318293
    goto :goto_0
.end method

.method public getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame"
    .end annotation

    .prologue
    .line 1318284
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1318279
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "render_key"
    .end annotation

    .prologue
    .line 1318278
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1318275
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1318276
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1318277
    return-void
.end method
