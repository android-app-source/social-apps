.class public Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1318303
    const-class v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    new-instance v1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1318304
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1318305
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1318306
    if-nez p0, :cond_0

    .line 1318307
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1318308
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1318309
    invoke-static {p0, p1, p2}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;->b(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;LX/0nX;LX/0my;)V

    .line 1318310
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1318311
    return-void
.end method

.method private static b(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1318312
    const-string v0, "frame"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1318313
    const-string v0, "render_key"

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->renderKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1318314
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1318315
    check-cast p1, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfigSerializer;->a(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;LX/0nX;LX/0my;)V

    return-void
.end method
