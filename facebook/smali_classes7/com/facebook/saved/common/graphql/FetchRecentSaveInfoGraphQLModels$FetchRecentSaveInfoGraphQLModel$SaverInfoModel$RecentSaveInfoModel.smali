.class public final Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d00a578
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1174237
    const-class v0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1174238
    const-class v0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1174246
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1174247
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1174239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1174240
    invoke-virtual {p0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1174241
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1174242
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1174243
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1174244
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1174245
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1174232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1174233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1174234
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174235
    iget-object v0, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    iput-object v0, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    .line 1174236
    iget-object v0, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1174229
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1174230
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->f:I

    .line 1174231
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1174226
    new-instance v0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;

    invoke-direct {v0}, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;-><init>()V

    .line 1174227
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1174228
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1174225
    const v0, -0x31750708

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1174224
    const v0, -0x2679809a

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1174222
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174223
    iget v0, p0, Lcom/facebook/saved/common/graphql/FetchRecentSaveInfoGraphQLModels$FetchRecentSaveInfoGraphQLModel$SaverInfoModel$RecentSaveInfoModel;->f:I

    return v0
.end method
