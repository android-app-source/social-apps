.class public final Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x11ae32bc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1175624
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1175623
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1175621
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1175622
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1175619
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1175620
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventViewerCapability"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175616
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1175617
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175614
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1175615
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1175603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1175604
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1175605
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x4ead19c2

    invoke-static {v2, v1, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1175606
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1175607
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1175608
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1175609
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1175610
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1175611
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1175612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1175613
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1175588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1175589
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1175590
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4ead19c2

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1175591
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1175592
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;

    .line 1175593
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->g:I

    move-object v1, v0

    .line 1175594
    :cond_0
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1175595
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1175596
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1175597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;

    .line 1175598
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1175599
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1175600
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1175601
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1175602
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1175575
    new-instance v0, LX/7A5;

    invoke-direct {v0, p1}, LX/7A5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1175584
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1175585
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->f:Z

    .line 1175586
    const/4 v0, 0x2

    const v1, -0x4ead19c2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;->g:I

    .line 1175587
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1175582
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1175583
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1175581
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1175578
    new-instance v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;-><init>()V

    .line 1175579
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1175580
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1175577
    const v0, 0x2fa856de

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1175576
    const v0, 0x252222

    return v0
.end method
