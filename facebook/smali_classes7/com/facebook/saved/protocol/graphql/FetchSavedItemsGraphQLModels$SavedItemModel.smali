.class public final Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ac94232
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1175406
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1175405
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1175403
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1175404
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1175381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1175382
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x3fb50ca8

    invoke-static {v1, v0, v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1175383
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1175384
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1175385
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1175386
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1175387
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1175388
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x4cb5d5f3    # 9.5334296E7f

    invoke-static {v7, v6, v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1175389
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x6d151bc4

    invoke-static {v8, v7, v9}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1175390
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1175391
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1175392
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1175393
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1175394
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1175395
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1175396
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1175397
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1175398
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1175399
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1175400
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1175401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1175402
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1175334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1175335
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1175336
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3fb50ca8

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1175337
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1175338
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175339
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->e:I

    move-object v1, v0

    .line 1175340
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1175341
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 1175342
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1175343
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175344
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->f:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 1175345
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1175346
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1175347
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1175348
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175349
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1175350
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1175351
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1175352
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1175353
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175354
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1175355
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1175356
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 1175357
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1175358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175359
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->i:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 1175360
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1175361
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 1175362
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1175363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175364
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 1175365
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1175366
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4cb5d5f3    # 9.5334296E7f

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1175367
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1175368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175369
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k:I

    move-object v1, v0

    .line 1175370
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 1175371
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6d151bc4

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1175372
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1175373
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    .line 1175374
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l:I

    move-object v1, v0

    .line 1175375
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1175376
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 1175377
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1175378
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1175379
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_8
    move-object p0, v1

    .line 1175380
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttributionText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1175332
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1175333
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1175307
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1175308
    const/4 v0, 0x0

    const v1, 0x3fb50ca8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->e:I

    .line 1175309
    const/4 v0, 0x6

    const v1, 0x4cb5d5f3    # 9.5334296E7f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k:I

    .line 1175310
    const/4 v0, 0x7

    const v1, -0x6d151bc4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l:I

    .line 1175311
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1175329
    new-instance v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;-><init>()V

    .line 1175330
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1175331
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1175328
    const v0, -0x75ad6573    # -1.01402E-32f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1175407
    const v0, -0x939b30f

    return v0
.end method

.method public final j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGlobalShare"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175326
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->f:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->f:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 1175327
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->f:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175324
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1175325
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175322
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1175323
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    return-object v0
.end method

.method public final m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175320
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->i:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->i:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 1175321
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->i:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175318
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 1175319
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    return-object v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitleText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175316
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1175317
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175314
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1175315
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175312
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m:Ljava/lang/String;

    .line 1175313
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m:Ljava/lang/String;

    return-object v0
.end method
