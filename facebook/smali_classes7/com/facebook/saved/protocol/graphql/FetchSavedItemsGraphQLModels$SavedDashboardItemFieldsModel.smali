.class public final Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/59b;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd76b80f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:I

.field private C:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:D

.field private k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:D

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Z

.field private x:D

.field private y:D

.field private z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1175011
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1174935
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1174936
    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1174937
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1174938
    const/16 v0, 0x1b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1174939
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1174940
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 1174941
    iput-object p1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1174942
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1174943
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1174944
    if-eqz v0, :cond_0

    .line 1174945
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x1a

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1174946
    :cond_0
    return-void

    .line 1174947
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174948
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->v:Ljava/lang/String;

    .line 1174949
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 1174950
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174951
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->w:Z

    return v0
.end method

.method public final C()D
    .locals 2

    .prologue
    .line 1174952
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174953
    iget-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x:D

    return-wide v0
.end method

.method public final D()D
    .locals 2

    .prologue
    .line 1174954
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174955
    iget-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->y:D

    return-wide v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174956
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z:Ljava/lang/String;

    .line 1174957
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175018
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->A:Ljava/lang/String;

    .line 1175019
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final G()I
    .locals 2

    .prologue
    .line 1174958
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174959
    iget v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->B:I

    return v0
.end method

.method public final H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175016
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 1175017
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    return-object v0
.end method

.method public final I()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerRecommendation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175014
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1175015
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->D:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1175012
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1175013
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 1174882
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1174883
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1174884
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x4ead19c2

    invoke-static {v4, v3, v5}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1174885
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1174886
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1174887
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1174888
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1174889
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1174890
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->A()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1174891
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1174892
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->F()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1174893
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1174894
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x52c4008e

    invoke-static {v5, v4, v6}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1174895
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->J()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 1174896
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1174897
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 1174898
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 1174899
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 1174900
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 1174901
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1174902
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174903
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1174904
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1174905
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1174906
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1174907
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->o:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1174908
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1174909
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1174910
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1174911
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1174912
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->t:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174913
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1174914
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1174915
    const/16 v2, 0x12

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->w:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1174916
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174917
    const/16 v3, 0x14

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->y:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1174918
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1174919
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1174920
    const/16 v2, 0x17

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->B:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1174921
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1174922
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1174923
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1174924
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1174925
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1174980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1174981
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1174982
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4ead19c2

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1174983
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1174984
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1174985
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->i:I

    move-object v1, v0

    .line 1174986
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1174987
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1174988
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1174989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1174990
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1174991
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1174992
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1174993
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1174994
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1174995
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->l:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1174996
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1174997
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 1174998
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->H()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1174999
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1175000
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$VideoChannelIdForVideoFragmentModel$VideoChannelModel;

    .line 1175001
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 1175002
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x52c4008e

    invoke-static {v2, v0, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1175003
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1175004
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 1175005
    iput v3, v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->D:I

    move-object v1, v0

    .line 1175006
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1175007
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    .line 1175008
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1175009
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_5
    move-object p0, v1

    .line 1175010
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1174979
    new-instance v0, LX/7A2;

    invoke-direct {v0, p1}, LX/7A2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174978
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1174960
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1174961
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->f:Z

    .line 1174962
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->g:Z

    .line 1174963
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->h:Z

    .line 1174964
    const/4 v0, 0x4

    const v1, -0x4ead19c2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->i:I

    .line 1174965
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j:D

    .line 1174966
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n:I

    .line 1174967
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->o:I

    .line 1174968
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p:I

    .line 1174969
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q:Z

    .line 1174970
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r:Z

    .line 1174971
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->t:D

    .line 1174972
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->w:Z

    .line 1174973
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x:D

    .line 1174974
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->y:D

    .line 1174975
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->B:I

    .line 1174976
    const/16 v0, 0x19

    const v1, 0x52c4008e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->D:I

    .line 1174977
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1174926
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1174927
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->J()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1174928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1174929
    const/16 v0, 0x1a

    iput v0, p2, LX/18L;->c:I

    .line 1174930
    :goto_0
    return-void

    .line 1174931
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1174932
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1174933
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 1174934
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1174842
    new-instance v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;-><init>()V

    .line 1174843
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1174844
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1174845
    const v0, -0x4ac718a9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1174846
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174847
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1174848
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1174849
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1174850
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174851
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->f:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1174852
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174853
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->g:Z

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1174854
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174855
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->h:Z

    return v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventViewerCapability"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174856
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174857
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()D
    .locals 2

    .prologue
    .line 1174858
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174859
    iget-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j:D

    return-wide v0
.end method

.method public final p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174860
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    .line 1174861
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174862
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->l:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->l:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1174863
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->l:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174864
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->m:Ljava/lang/String;

    .line 1174865
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1174866
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174867
    iget v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n:I

    return v0
.end method

.method public final t()I
    .locals 2

    .prologue
    .line 1174868
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174869
    iget v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->o:I

    return v0
.end method

.method public final u()I
    .locals 2

    .prologue
    .line 1174870
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174871
    iget v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p:I

    return v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 1174872
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174873
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->q:Z

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1174874
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174875
    iget-boolean v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r:Z

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174876
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->s:Ljava/lang/String;

    .line 1174877
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 1174878
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1174879
    iget-wide v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->t:D

    return-wide v0
.end method

.method public final z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174880
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->u:Ljava/lang/String;

    .line 1174881
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->u:Ljava/lang/String;

    return-object v0
.end method
