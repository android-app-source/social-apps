.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1073651a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176850
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176849
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1176847
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176848
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1176844
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176845
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176846
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176800
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    .line 1176801
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176842
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176843
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1176834
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176835
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1176836
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1176837
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1176838
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1176839
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1176840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176841
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1176826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176827
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1176828
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176829
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1176830
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    .line 1176831
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176832
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176833
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1176825
    new-instance v0, LX/7AW;

    invoke-direct {v0, p1}, LX/7AW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176824
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1176816
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176817
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    .line 1176818
    if-eqz v0, :cond_0

    .line 1176819
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1176820
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1176821
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1176822
    :goto_0
    return-void

    .line 1176823
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1176807
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176808
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    .line 1176809
    if-eqz v0, :cond_0

    .line 1176810
    if-eqz p3, :cond_1

    .line 1176811
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176812
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 1176813
    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176814
    :cond_0
    :goto_0
    return-void

    .line 1176815
    :cond_1
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1176804
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;-><init>()V

    .line 1176805
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176806
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1176803
    const v0, -0x6550fd41

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1176802
    const v0, 0x4c808d5

    return v0
.end method
