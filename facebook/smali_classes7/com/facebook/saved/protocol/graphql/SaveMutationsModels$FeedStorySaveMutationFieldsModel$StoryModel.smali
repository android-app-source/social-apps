.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x516c9259
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176657
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176656
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1176654
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176655
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1176651
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176652
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176653
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176649
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    .line 1176650
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176607
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176608
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1176641
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176642
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1176643
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1176644
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1176645
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1176646
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1176647
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176648
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1176633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176634
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1176635
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176636
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1176637
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;

    .line 1176638
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176639
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176640
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1176632
    new-instance v0, LX/7AS;

    invoke-direct {v0, p1}, LX/7AS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176631
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1176623
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176624
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    .line 1176625
    if-eqz v0, :cond_0

    .line 1176626
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1176627
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1176628
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1176629
    :goto_0
    return-void

    .line 1176630
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1176614
    const-string v0, "save_info.viewer_save_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176615
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->k()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    move-result-object v0

    .line 1176616
    if-eqz v0, :cond_0

    .line 1176617
    if-eqz p3, :cond_1

    .line 1176618
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176619
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 1176620
    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;->f:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 1176621
    :cond_0
    :goto_0
    return-void

    .line 1176622
    :cond_1
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1176611
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;-><init>()V

    .line 1176612
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176613
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1176610
    const v0, -0x5d1c094c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1176609
    const v0, 0x4c808d5

    return v0
.end method
