.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1613da4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176976
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176996
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1176994
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176995
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1176991
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176992
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176993
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 1176984
    iput-object p1, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1176985
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1176986
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1176987
    if-eqz v0, :cond_0

    .line 1176988
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1176989
    :cond_0
    return-void

    .line 1176990
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176981
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1176982
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1176983
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176979
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    .line 1176980
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176977
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1176978
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1176997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176998
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1176999
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1177000
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->l()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1177001
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1177002
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1177003
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1177004
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1177005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1177006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1176957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176959
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1176960
    new-instance v0, LX/7Ac;

    invoke-direct {v0, p1}, LX/7Ac;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176961
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1176962
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176963
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->l()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1176964
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1176965
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1176966
    :goto_0
    return-void

    .line 1176967
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1176968
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1176969
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 1176970
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1176971
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;-><init>()V

    .line 1176972
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176973
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1176974
    const v0, -0x360d59ef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1176975
    const v0, -0x2d284ade

    return v0
.end method
