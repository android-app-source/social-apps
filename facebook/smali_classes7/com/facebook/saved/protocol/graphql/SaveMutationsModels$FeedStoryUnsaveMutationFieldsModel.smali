.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7cb6666b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176864
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176873
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1176874
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176875
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1176876
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176877
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176878
    return-void
.end method

.method private a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176865
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    .line 1176866
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1176867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176868
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1176869
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1176870
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1176871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176872
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1176851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176852
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1176853
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    .line 1176854
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1176855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;

    .line 1176856
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    .line 1176857
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176858
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1176859
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;-><init>()V

    .line 1176860
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176861
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1176862
    const v0, 0x62755a94

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1176863
    const v0, -0x21b3aa3e

    return v0
.end method
