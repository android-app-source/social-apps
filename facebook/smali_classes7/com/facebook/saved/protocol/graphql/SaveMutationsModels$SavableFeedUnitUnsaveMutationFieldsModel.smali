.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3d9582bf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1177340
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1177339
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1177359
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1177360
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1177356
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1177357
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1177358
    return-void
.end method

.method private a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1177354
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    .line 1177355
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1177361
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1177362
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1177363
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1177364
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1177365
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1177366
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1177346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1177347
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1177348
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    .line 1177349
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->a()Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1177350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;

    .line 1177351
    iput-object v0, v1, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;->e:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    .line 1177352
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1177353
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1177343
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;-><init>()V

    .line 1177344
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1177345
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1177342
    const v0, -0x5efda223

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1177341
    const v0, -0x621248f2

    return v0
.end method
