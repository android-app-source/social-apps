.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b52884d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176792
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1176791
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1176789
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176790
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1176786
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1176787
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176788
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1176777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176778
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1176779
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1176780
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1176781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1176783
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1176784
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1176785
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1176763
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1176764
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 1176765
    iput-object p1, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1176766
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1176767
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1176768
    if-eqz v0, :cond_0

    .line 1176769
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1176770
    :cond_0
    return-void

    .line 1176771
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1176772
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;-><init>()V

    .line 1176773
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1176774
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1176775
    const v0, 0x5b6149f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1176776
    const v0, -0x3a83a440

    return v0
.end method
