.class public final Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1174527
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1174528
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1174607
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1174608
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1174566
    if-nez p1, :cond_0

    .line 1174567
    :goto_0
    return v0

    .line 1174568
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1174569
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1174570
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1174571
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174572
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1174573
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174574
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1174575
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1174576
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174577
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1174578
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174579
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1174580
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1174581
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174582
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1174583
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174584
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1174585
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1174586
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174587
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1174588
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174589
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1174590
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1174591
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174592
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1174593
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174594
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1174595
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1174596
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174597
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1174598
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1174599
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1174600
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174601
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1174602
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1174603
    :sswitch_7
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1174604
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1174605
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1174606
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6d151bc4 -> :sswitch_4
        -0x4ead19c2 -> :sswitch_6
        -0x4b1bb36c -> :sswitch_0
        -0x2d411a22 -> :sswitch_2
        -0xc169f1 -> :sswitch_5
        0x3fb50ca8 -> :sswitch_1
        0x4cb5d5f3 -> :sswitch_3
        0x52c4008e -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1174565
    new-instance v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1174562
    sparse-switch p0, :sswitch_data_0

    .line 1174563
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1174564
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6d151bc4 -> :sswitch_0
        -0x4ead19c2 -> :sswitch_0
        -0x4b1bb36c -> :sswitch_0
        -0x2d411a22 -> :sswitch_0
        -0xc169f1 -> :sswitch_0
        0x3fb50ca8 -> :sswitch_0
        0x4cb5d5f3 -> :sswitch_0
        0x52c4008e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1174561
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1174559
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->b(I)V

    .line 1174560
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1174554
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1174555
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1174556
    :cond_0
    iput-object p1, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1174557
    iput p2, p0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->b:I

    .line 1174558
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1174609
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1174553
    new-instance v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1174550
    iget v0, p0, LX/1vt;->c:I

    .line 1174551
    move v0, v0

    .line 1174552
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1174547
    iget v0, p0, LX/1vt;->c:I

    .line 1174548
    move v0, v0

    .line 1174549
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1174544
    iget v0, p0, LX/1vt;->b:I

    .line 1174545
    move v0, v0

    .line 1174546
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1174541
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1174542
    move-object v0, v0

    .line 1174543
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1174532
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1174533
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1174534
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1174535
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1174536
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1174537
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1174538
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1174539
    invoke-static {v3, v9, v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1174540
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1174529
    iget v0, p0, LX/1vt;->c:I

    .line 1174530
    move v0, v0

    .line 1174531
    return v0
.end method
