.class public final Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1613da4c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1177227
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1177240
    const-class v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1177238
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1177239
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1177235
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1177236
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1177237
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 1177228
    iput-object p1, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1177229
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1177230
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1177231
    if-eqz v0, :cond_0

    .line 1177232
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1177233
    :cond_0
    return-void

    .line 1177234
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1177224
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1177225
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1177226
    :cond_0
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1177222
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    .line 1177223
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1177220
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1177221
    iget-object v0, p0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1177241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1177242
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1177243
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1177244
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->l()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1177245
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1177246
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1177247
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1177248
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1177249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1177250
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1177217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1177218
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1177219
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1177216
    new-instance v0, LX/7Ah;

    invoke-direct {v0, p1}, LX/7Ah;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1177215
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1177201
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177202
    invoke-direct {p0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->l()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1177203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1177204
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1177205
    :goto_0
    return-void

    .line 1177206
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1177212
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177213
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 1177214
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1177209
    new-instance v0, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-direct {v0}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;-><init>()V

    .line 1177210
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1177211
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1177208
    const v0, -0x43bda282

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1177207
    const v0, -0x2d284ade

    return v0
.end method
