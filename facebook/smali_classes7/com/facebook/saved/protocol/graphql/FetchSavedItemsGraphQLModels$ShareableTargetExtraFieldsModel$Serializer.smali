.class public final Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1175573
    const-class v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;

    new-instance v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1175574
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1175550
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1175551
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1175552
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1175553
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1175554
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1175555
    if-eqz v2, :cond_0

    .line 1175556
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175557
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1175558
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1175559
    if-eqz v2, :cond_1

    .line 1175560
    const-string p0, "can_viewer_share"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175561
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1175562
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1175563
    if-eqz v2, :cond_2

    .line 1175564
    const-string p0, "event_viewer_capability"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175565
    invoke-static {v1, v2, p1}, LX/7AH;->a(LX/15i;ILX/0nX;)V

    .line 1175566
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1175567
    if-eqz v2, :cond_3

    .line 1175568
    const-string p0, "global_share"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1175569
    invoke-static {v1, v2, p1}, LX/7AI;->a(LX/15i;ILX/0nX;)V

    .line 1175570
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1175571
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1175572
    check-cast p1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$Serializer;->a(Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
