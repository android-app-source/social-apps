.class public final Lcom/facebook/common/netchecker/NetChecker$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2EL;


# direct methods
.method public constructor <init>(LX/2EL;J)V
    .locals 0

    .prologue
    .line 1232348
    iput-object p1, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    iput-wide p2, p0, Lcom/facebook/common/netchecker/NetChecker$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 1232349
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    iget-object v0, v0, LX/2EL;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->t()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/common/netchecker/NetChecker$1;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1232350
    invoke-direct {p0}, Lcom/facebook/common/netchecker/NetChecker$1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232351
    :goto_0
    return-void

    .line 1232352
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    iget-object v0, v0, LX/2EL;->d:LX/2EN;

    .line 1232353
    :try_start_0
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    sget-object v2, LX/2EN;->c:Lorg/apache/http/client/RedirectHandler;

    .line 1232354
    iput-object v2, v1, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 1232355
    move-object v1, v1

    .line 1232356
    sget-object v2, LX/2EN;->d:Lorg/apache/http/client/ResponseHandler;

    .line 1232357
    iput-object v2, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1232358
    move-object v1, v1

    .line 1232359
    iget-object v2, v0, LX/2EN;->b:LX/0Uh;

    const/16 v3, 0x113

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1232360
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "http://portal.fb.com/mobile/status.php"

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1232361
    iput-object v2, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1232362
    move-object v2, v1

    .line 1232363
    const-string v3, "CaptivePortalDetector"

    .line 1232364
    iput-object v3, v2, LX/15E;->c:Ljava/lang/String;

    .line 1232365
    :goto_1
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v1

    .line 1232366
    iget-object v2, v0, LX/2EN;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2EQ;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1232367
    :goto_2
    move-object v0, v1

    .line 1232368
    iget-object v1, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    monitor-enter v1

    .line 1232369
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/common/netchecker/NetChecker$1;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1232370
    monitor-exit v1

    goto :goto_0

    .line 1232371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1232372
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    sget-object v3, LX/2EL;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1232373
    iput-object v3, v2, LX/2EL;->l:Ljava/util/concurrent/Future;

    .line 1232374
    iget-object v2, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    iget-object v3, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    iget-object v3, v3, LX/2EL;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    .line 1232375
    iput-wide v4, v2, LX/2EL;->j:J

    .line 1232376
    iget-object v2, p0, Lcom/facebook/common/netchecker/NetChecker$1;->b:LX/2EL;

    invoke-static {v2, v0}, LX/2EL;->a$redex0(LX/2EL;LX/2EQ;)V

    .line 1232377
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1232378
    :cond_2
    :try_start_3
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "http://b-www.facebook.com/mobile/status.php"

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1232379
    iput-object v2, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1232380
    goto :goto_1

    .line 1232381
    :catch_0
    sget-object v1, LX/2EQ;->NOT_CAPTIVE_PORTAL:LX/2EQ;

    goto :goto_2
.end method
