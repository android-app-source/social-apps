.class public Lcom/facebook/api/ufiservices/UFIServicesHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/26t;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6BR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6BP;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6BH;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6BI;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8OJ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Om;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8LV;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/1BA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1223665
    const-class v0, Lcom/facebook/api/ufiservices/UFIServicesHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1BA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6BR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6BP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6BH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6BI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8OJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Om;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8LV;",
            ">;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1223666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1223667
    iput-object p1, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->b:LX/0Or;

    .line 1223668
    iput-object p2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->c:LX/0Ot;

    .line 1223669
    iput-object p3, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->d:LX/0Ot;

    .line 1223670
    iput-object p4, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->e:LX/0Ot;

    .line 1223671
    iput-object p5, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->f:LX/0Ot;

    .line 1223672
    iput-object p6, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->g:LX/0Ot;

    .line 1223673
    iput-object p7, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->h:LX/0Ot;

    .line 1223674
    iput-object p8, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->i:LX/0Ot;

    .line 1223675
    iput-object p9, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->j:LX/1BA;

    .line 1223676
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 1223677
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1223678
    const-string v1, "feed_delete_comment"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1223679
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1223680
    const-string v1, "deleteCommentParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;

    .line 1223681
    iget-object v1, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object p2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->e:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0e6;

    invoke-virtual {v1, p2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1223682
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1223683
    move-object v0, v0

    .line 1223684
    :goto_0
    return-object v0

    .line 1223685
    :cond_0
    const-string v1, "feed_toggle_like"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "feed_toggle_page_like"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1223686
    :cond_1
    iget-object v0, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->j:LX/1BA;

    const v1, 0x710003

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 1223687
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1223688
    const-string v1, "toggleLikeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    .line 1223689
    iget-object v1, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object p2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0e6;

    invoke-virtual {v1, p2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1223690
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1223691
    goto :goto_0

    .line 1223692
    :cond_2
    const-string v1, "feed_set_notify_me"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1223693
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1223694
    const-string v1, "setNotifyMeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    .line 1223695
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1223696
    new-instance v1, LX/5H5;

    invoke-direct {v1, v0}, LX/5H5;-><init>(Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)V

    move-object v1, v1

    .line 1223697
    iget-object p2, v0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->c:Ljava/lang/String;

    move-object v0, p2

    .line 1223698
    iput-object v0, v1, LX/5H5;->e:Ljava/lang/String;

    .line 1223699
    move-object v0, v1

    .line 1223700
    invoke-virtual {v0}, LX/5H5;->g()Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    move-result-object p2

    .line 1223701
    iget-object v0, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v0, v1, p2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1223702
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1223703
    goto/16 :goto_0

    .line 1223704
    :cond_3
    const-string v1, "feed_edit_comment"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1223705
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1223706
    const-string v1, "editCommentParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;

    .line 1223707
    iget-object v1, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1223708
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1223709
    move-object v0, v0

    .line 1223710
    goto/16 :goto_0

    .line 1223711
    :cond_4
    const-string v1, "feed_add_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1223712
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 1223713
    const-string v3, "addPhotoAttachmentParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    .line 1223714
    iget-object v2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8OJ;

    iget-object v3, v6, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v4, v6, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->b:Ljava/lang/String;

    const-string v5, "photo_comment_batch"

    iget-object v6, v6, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    sget-object v7, Lcom/facebook/api/ufiservices/UFIServicesHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v2 .. v7}, LX/8OJ;->a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v2

    .line 1223715
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 1223716
    goto/16 :goto_0

    .line 1223717
    :cond_5
    const-string v1, "feed_add_video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1223718
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 1223719
    const-string v3, "addVideoAttachmentParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;

    .line 1223720
    iget-object v3, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8LV;

    iget-object v4, v2, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v5, v2, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/facebook/api/ufiservices/AddPhotoAttachmentParams;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1223721
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1223722
    new-instance v7, LX/8LQ;

    invoke-direct {v7}, LX/8LQ;-><init>()V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    .line 1223723
    iput-object v8, v7, LX/8LQ;->b:LX/0Px;

    .line 1223724
    move-object v7, v7

    .line 1223725
    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1223726
    iput-object v6, v7, LX/8LQ;->c:LX/0Px;

    .line 1223727
    move-object v6, v7

    .line 1223728
    iput-object v5, v6, LX/8LQ;->a:Ljava/lang/String;

    .line 1223729
    move-object v6, v6

    .line 1223730
    iput-object v2, v6, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1223731
    move-object v6, v6

    .line 1223732
    sget-object v7, LX/8LS;->COMMENT_VIDEO:LX/8LS;

    .line 1223733
    iput-object v7, v6, LX/8LQ;->q:LX/8LS;

    .line 1223734
    move-object v6, v6

    .line 1223735
    const-string v7, "comment_video"

    .line 1223736
    iput-object v7, v6, LX/8LQ;->i:Ljava/lang/String;

    .line 1223737
    move-object v7, v6

    .line 1223738
    iget-object v6, v3, LX/8LV;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1223739
    iput-wide v8, v7, LX/8LQ;->h:J

    .line 1223740
    move-object v6, v7

    .line 1223741
    sget-object v7, LX/5Rn;->NORMAL:LX/5Rn;

    .line 1223742
    iput-object v7, v6, LX/8LQ;->B:LX/5Rn;

    .line 1223743
    move-object v6, v6

    .line 1223744
    sget-object v7, LX/8LR;->VIDEO_TARGET:LX/8LR;

    .line 1223745
    iput-object v7, v6, LX/8LQ;->p:LX/8LR;

    .line 1223746
    move-object v6, v6

    .line 1223747
    sget-object v7, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->b:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1223748
    iput-object v7, v6, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1223749
    move-object v6, v6

    .line 1223750
    iget-object v7, v3, LX/8LV;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 1223751
    iput-wide v8, v6, LX/8LQ;->x:J

    .line 1223752
    move-object v6, v6

    .line 1223753
    invoke-virtual {v6}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v6

    move-object v3, v6

    .line 1223754
    iget-object v2, p0, Lcom/facebook/api/ufiservices/UFIServicesHandler;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Om;

    invoke-virtual {v2, v3}, LX/8Om;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 1223755
    goto/16 :goto_0

    .line 1223756
    :cond_6
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0
.end method
