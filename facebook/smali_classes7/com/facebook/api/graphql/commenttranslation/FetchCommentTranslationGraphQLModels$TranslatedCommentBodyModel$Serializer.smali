.class public final Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1222145
    const-class v0, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;

    new-instance v1, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1222146
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1222147
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1222148
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1222149
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1222150
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1222151
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1222152
    if-eqz v2, :cond_0

    .line 1222153
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222154
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1222155
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1222156
    if-eqz v2, :cond_1

    .line 1222157
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222158
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1222159
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1222160
    if-eqz v2, :cond_2

    .line 1222161
    const-string p0, "translated_body_for_viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222162
    invoke-static {v1, v2, p1, p2}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1222163
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1222164
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1222165
    check-cast p1, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel$Serializer;->a(Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;LX/0nX;LX/0my;)V

    return-void
.end method
