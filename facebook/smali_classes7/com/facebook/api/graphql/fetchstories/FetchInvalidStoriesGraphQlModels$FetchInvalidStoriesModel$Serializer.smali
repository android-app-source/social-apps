.class public final Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1222760
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1222761
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1222762
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1222747
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1222748
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1222749
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1222750
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1222751
    if-eqz p0, :cond_0

    .line 1222752
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222753
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1222754
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1222755
    if-eqz p0, :cond_1

    .line 1222756
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222757
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1222758
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1222759
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1222746
    check-cast p1, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel$Serializer;->a(Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;LX/0nX;LX/0my;)V

    return-void
.end method
