.class public final Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ba80ee0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222510
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222523
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1222521
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1222522
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommenters"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1222519
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1222520
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1222513
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222514
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1222515
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1222516
    if-eqz v0, :cond_0

    .line 1222517
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1222518
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222511
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222512
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1222459
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1222460
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x2055724b

    invoke-static {v1, v0, v2}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1222461
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1222462
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1222463
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1222464
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1222465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1222466
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1222495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1222496
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1222497
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2055724b

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1222498
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1222499
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    .line 1222500
    iput v3, v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->e:I

    move-object v1, v0

    .line 1222501
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1222502
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222503
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1222504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    .line 1222505
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222506
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1222507
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1222508
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1222509
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1222524
    new-instance v0, LX/7fR;

    invoke-direct {v0, p1}, LX/7fR;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1222492
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1222493
    const/4 v0, 0x0

    const v1, 0x2055724b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->e:I

    .line 1222494
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1222484
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1222485
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 1222486
    if-eqz v0, :cond_0

    .line 1222487
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1222488
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1222489
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1222490
    :goto_0
    return-void

    .line 1222491
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1222481
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1222482
    check-cast p2, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->a(Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;)V

    .line 1222483
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1222472
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1222473
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->j()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 1222474
    if-eqz v0, :cond_0

    .line 1222475
    if-eqz p3, :cond_1

    .line 1222476
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222477
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;->a(I)V

    .line 1222478
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;

    .line 1222479
    :cond_0
    :goto_0
    return-void

    .line 1222480
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel$LikersModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1222469
    new-instance v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;-><init>()V

    .line 1222470
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1222471
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1222468
    const v0, 0x51d76d57

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1222467
    const v0, -0x78fb05b

    return v0
.end method
