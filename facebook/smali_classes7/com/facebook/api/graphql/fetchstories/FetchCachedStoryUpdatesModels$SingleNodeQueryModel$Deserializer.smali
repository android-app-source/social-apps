.class public final Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1222387
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1222388
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1222386
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1222316
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1222317
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1222318
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1222319
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1222320
    :goto_0
    move v1, v2

    .line 1222321
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1222322
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1222323
    new-instance v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;-><init>()V

    .line 1222324
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1222325
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1222326
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1222327
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1222328
    :cond_0
    return-object v1

    .line 1222329
    :cond_1
    const-string v12, "viewer_readstate"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1222330
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    .line 1222331
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1222332
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1222333
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1222334
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, p0, :cond_2

    if-eqz v11, :cond_2

    .line 1222335
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_3

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1222336
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1222337
    :cond_4
    const-string v12, "attached_story"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1222338
    invoke-static {p1, v0}, LX/7fU;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1222339
    :cond_5
    const-string v12, "feedback"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1222340
    invoke-static {p1, v0}, LX/7fW;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1222341
    :cond_6
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1222342
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1222343
    :cond_7
    const-string v12, "message"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1222344
    const/4 v11, 0x0

    .line 1222345
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v12, :cond_10

    .line 1222346
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1222347
    :goto_2
    move v6, v11

    .line 1222348
    goto :goto_1

    .line 1222349
    :cond_8
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1222350
    const/4 v11, 0x0

    .line 1222351
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v12, :cond_14

    .line 1222352
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1222353
    :goto_3
    move v5, v11

    .line 1222354
    goto/16 :goto_1

    .line 1222355
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1222356
    :cond_a
    const/4 v11, 0x7

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1222357
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1222358
    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1222359
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1222360
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1222361
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1222362
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1222363
    if-eqz v1, :cond_b

    .line 1222364
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1222365
    :cond_b
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1

    .line 1222366
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1222367
    :cond_e
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_f

    .line 1222368
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1222369
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1222370
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v12, :cond_e

    .line 1222371
    const-string p0, "text"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1222372
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 1222373
    :cond_f
    const/4 v12, 0x1

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1222374
    invoke-virtual {v0, v11, v6}, LX/186;->b(II)V

    .line 1222375
    invoke-virtual {v0}, LX/186;->d()I

    move-result v11

    goto/16 :goto_2

    :cond_10
    move v6, v11

    goto :goto_4

    .line 1222376
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1222377
    :cond_12
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_13

    .line 1222378
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1222379
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1222380
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_12

    if-eqz v12, :cond_12

    .line 1222381
    const-string p0, "text"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1222382
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_5

    .line 1222383
    :cond_13
    const/4 v12, 0x1

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1222384
    invoke-virtual {v0, v11, v5}, LX/186;->b(II)V

    .line 1222385
    invoke-virtual {v0}, LX/186;->d()I

    move-result v11

    goto/16 :goto_3

    :cond_14
    move v5, v11

    goto :goto_5
.end method
