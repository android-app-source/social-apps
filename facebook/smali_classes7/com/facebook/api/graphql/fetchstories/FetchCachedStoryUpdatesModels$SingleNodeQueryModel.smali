.class public final Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x103ed2f4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222631
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222630
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1222628
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1222629
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222625
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1222626
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1222627
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222623
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    .line 1222624
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    return-object v0
.end method

.method private l()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedback"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222621
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->g:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->g:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    .line 1222622
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->g:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222619
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->h:Ljava/lang/String;

    .line 1222620
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222617
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1222618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222615
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1222616
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1222632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1222633
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1222634
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1222635
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->l()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1222636
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1222637
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x7b49069

    invoke-static {v5, v4, v6}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1222638
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->o()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x2002b5c8

    invoke-static {v6, v5, v7}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1222639
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1222640
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1222641
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1222642
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1222643
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1222644
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1222645
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1222646
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1222647
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1222648
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1222589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1222590
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1222591
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    .line 1222592
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1222593
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    .line 1222594
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->f:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$AttachedStoryModel;

    .line 1222595
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->l()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1222596
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->l()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    .line 1222597
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->l()Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1222598
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    .line 1222599
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->g:Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$FeedbackModel;

    .line 1222600
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1222601
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7b49069

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1222602
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1222603
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    .line 1222604
    iput v3, v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->i:I

    move-object v1, v0

    .line 1222605
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1222606
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2002b5c8

    invoke-static {v2, v0, v3}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1222607
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1222608
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    .line 1222609
    iput v3, v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->j:I

    move-object v1, v0

    .line 1222610
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1222611
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1222612
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1222613
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 1222614
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1222588
    new-instance v0, LX/7fS;

    invoke-direct {v0, p1}, LX/7fS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1222587
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1222582
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1222583
    const/4 v0, 0x4

    const v1, 0x7b49069

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->i:I

    .line 1222584
    const/4 v0, 0x5

    const v1, -0x2002b5c8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->j:I

    .line 1222585
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;->k:Z

    .line 1222586
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1222580
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1222581
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1222579
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1222576
    new-instance v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;-><init>()V

    .line 1222577
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1222578
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1222575
    const v0, -0x1cad6b2f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1222574
    const v0, 0x252222

    return v0
.end method
