.class public final Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1222572
    const-class v0, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    new-instance v1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1222573
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1222571
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1222527
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1222528
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1222529
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1222530
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1222531
    if-eqz v2, :cond_0

    .line 1222532
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222533
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1222534
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1222535
    if-eqz v2, :cond_1

    .line 1222536
    const-string p0, "attached_story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222537
    invoke-static {v1, v2, p1}, LX/8vK;->a(LX/15i;ILX/0nX;)V

    .line 1222538
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1222539
    if-eqz v2, :cond_2

    .line 1222540
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222541
    invoke-static {v1, v2, p1, p2}, LX/8vK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1222542
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1222543
    if-eqz v2, :cond_3

    .line 1222544
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222545
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1222546
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1222547
    if-eqz v2, :cond_5

    .line 1222548
    const-string p0, "message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222549
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1222550
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1222551
    if-eqz p0, :cond_4

    .line 1222552
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222553
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1222554
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1222555
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1222556
    if-eqz v2, :cond_7

    .line 1222557
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222558
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1222559
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1222560
    if-eqz p0, :cond_6

    .line 1222561
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222562
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1222563
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1222564
    :cond_7
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1222565
    if-eqz v2, :cond_8

    .line 1222566
    const-string p0, "viewer_readstate"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1222567
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1222568
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1222569
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1222570
    check-cast p1, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel$Serializer;->a(Lcom/facebook/api/graphql/fetchstories/FetchCachedStoryUpdatesModels$SingleNodeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
