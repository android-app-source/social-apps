.class public final Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x54a5691b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1223585
    const-class v0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1223588
    const-class v0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1223586
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1223587
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1223577
    iget-object v0, p0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->e:Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->e:Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    .line 1223578
    iget-object v0, p0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->e:Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1223579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1223580
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->a()Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1223581
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1223582
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1223583
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1223584
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1223564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1223565
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->a()Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1223566
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->a()Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    .line 1223567
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->a()Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1223568
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;

    .line 1223569
    iput-object v0, v1, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;->e:Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel$ActorModel;

    .line 1223570
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1223571
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1223574
    new-instance v0, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/translations/TranslationRatingGraphQLModels$FBTranslationFeedbackRatingMutationFragmentModel$ViewerModel;-><init>()V

    .line 1223575
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1223576
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1223572
    const v0, 0x570324e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1223573
    const v0, -0x6747e1ce

    return v0
.end method
