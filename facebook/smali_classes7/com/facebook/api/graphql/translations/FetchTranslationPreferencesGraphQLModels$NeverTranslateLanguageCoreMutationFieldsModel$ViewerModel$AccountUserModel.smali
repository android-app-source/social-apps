.class public final Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1223312
    const-class v0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1223323
    const-class v0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1223321
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1223322
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1223319
    iget-object v0, p0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;->e:Ljava/lang/String;

    .line 1223320
    iget-object v0, p0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1223313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1223314
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1223315
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1223316
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1223317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1223318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1223309
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1223310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1223311
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1223324
    new-instance v0, LX/7fl;

    invoke-direct {v0, p1}, LX/7fl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1223308
    invoke-direct {p0}, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1223300
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1223301
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1223307
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1223304
    new-instance v0, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/translations/FetchTranslationPreferencesGraphQLModels$NeverTranslateLanguageCoreMutationFieldsModel$ViewerModel$AccountUserModel;-><init>()V

    .line 1223305
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1223306
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1223303
    const v0, -0x70f509d2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1223302
    const v0, 0x285feb

    return v0
.end method
