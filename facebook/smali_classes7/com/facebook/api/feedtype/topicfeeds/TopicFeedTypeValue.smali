.class public Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/graphql/model/GraphQLExploreFeed;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1222116
    new-instance v0, LX/7fK;

    invoke-direct {v0}, LX/7fK;-><init>()V

    sput-object v0, Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1222113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1222114
    const-class v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    iput-object v0, p0, Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;->a:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 1222115
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1222112
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1222109
    iget-object v0, p0, Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;->a:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1222110
    iget-object v0, p0, Lcom/facebook/api/feedtype/topicfeeds/TopicFeedTypeValue;->a:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1222111
    return-void
.end method
