.class public final Lcom/facebook/audience/util/MediaProcessor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7gj;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/1En;


# direct methods
.method public constructor <init>(LX/1En;LX/7gj;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1225936
    iput-object p1, p0, Lcom/facebook/audience/util/MediaProcessor$1;->c:LX/1En;

    iput-object p2, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    iput-object p3, p0, Lcom/facebook/audience/util/MediaProcessor$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1225937
    :try_start_0
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    invoke-virtual {v0}, LX/7gj;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1225938
    sget-object v3, LX/7hM;->b:LX/7hH;

    .line 1225939
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 1225940
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1225941
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1225942
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1225943
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 1225944
    :cond_0
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1225945
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 1225946
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v2, v2

    .line 1225947
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, v4, :cond_4

    :cond_1
    :goto_0
    move v0, v1

    .line 1225948
    if-eqz v0, :cond_2

    .line 1225949
    iget-object v6, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225950
    iget-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1225951
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225952
    iget-boolean v4, v2, LX/7gj;->h:Z

    move v2, v4

    .line 1225953
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225954
    iget v7, v5, LX/7gj;->k:F

    move v5, v7

    .line 1225955
    invoke-static/range {v0 .. v5}, LX/7hI;->a(Landroid/graphics/Bitmap;IZLX/7hH;IF)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1225956
    iput-object v0, v6, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1225957
    :cond_2
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->c:LX/1En;

    iget-object v0, v0, LX/1En;->c:LX/1Er;

    const-string v1, "BackstageTemp"

    const-string v2, ".jpg"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1225958
    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225959
    iget-object v2, v1, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1225960
    const/16 v2, 0x64

    .line 1225961
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1225962
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v1, v4, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1225963
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    move-object v1, v3

    .line 1225964
    invoke-static {v1, v0}, LX/7hJ;->a([BLjava/io/File;)V

    .line 1225965
    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1225966
    iput-object v0, v1, LX/7gj;->e:Ljava/lang/String;

    .line 1225967
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    const/4 v1, 0x0

    .line 1225968
    iput-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1225969
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x66e9945d

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1225970
    :goto_1
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->c:LX/1En;

    iget-object v0, v0, LX/1En;->d:LX/1Eo;

    sget-object v1, LX/7gQ;->SAVE_SELF_STACK:LX/7gQ;

    invoke-virtual {v0, v1}, LX/1Eo;->a(LX/7gQ;)V

    .line 1225971
    :goto_2
    return-void

    .line 1225972
    :cond_3
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225973
    iget-object v1, v0, LX/7gj;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1225974
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225975
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$1;->a:LX/7gj;

    .line 1225976
    iget-object v2, v1, LX/7gj;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1225977
    iput-object v1, v0, LX/7gj;->e:Ljava/lang/String;

    .line 1225978
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x30a444d5

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1225979
    :catch_0
    move-exception v0

    .line 1225980
    sget-object v1, LX/1En;->a:Ljava/lang/String;

    const-string v2, "Exception trying to process the media"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1225981
    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$1;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
