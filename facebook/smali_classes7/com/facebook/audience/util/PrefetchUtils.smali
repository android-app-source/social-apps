.class public Lcom/facebook/audience/util/PrefetchUtils;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:Lcom/facebook/audience/util/PrefetchUtils;


# instance fields
.field private a:LX/1M7;

.field private final b:LX/1HI;

.field private final c:LX/1Lg;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/36s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1HI;LX/1Lg;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226065
    iput-object p1, p0, Lcom/facebook/audience/util/PrefetchUtils;->b:LX/1HI;

    .line 1226066
    iput-object p2, p0, Lcom/facebook/audience/util/PrefetchUtils;->c:LX/1Lg;

    .line 1226067
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/audience/util/PrefetchUtils;->d:Ljava/util/Map;

    .line 1226068
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/audience/util/PrefetchUtils;
    .locals 5

    .prologue
    .line 1226069
    sget-object v0, Lcom/facebook/audience/util/PrefetchUtils;->e:Lcom/facebook/audience/util/PrefetchUtils;

    if-nez v0, :cond_1

    .line 1226070
    const-class v1, Lcom/facebook/audience/util/PrefetchUtils;

    monitor-enter v1

    .line 1226071
    :try_start_0
    sget-object v0, Lcom/facebook/audience/util/PrefetchUtils;->e:Lcom/facebook/audience/util/PrefetchUtils;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1226072
    if-eqz v2, :cond_0

    .line 1226073
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1226074
    new-instance p0, Lcom/facebook/audience/util/PrefetchUtils;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object v4

    check-cast v4, LX/1Lg;

    invoke-direct {p0, v3, v4}, Lcom/facebook/audience/util/PrefetchUtils;-><init>(LX/1HI;LX/1Lg;)V

    .line 1226075
    move-object v0, p0

    .line 1226076
    sput-object v0, Lcom/facebook/audience/util/PrefetchUtils;->e:Lcom/facebook/audience/util/PrefetchUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1226077
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1226078
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1226079
    :cond_1
    sget-object v0, Lcom/facebook/audience/util/PrefetchUtils;->e:Lcom/facebook/audience/util/PrefetchUtils;

    return-object v0

    .line 1226080
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1226081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226082
    if-nez p1, :cond_0

    .line 1226083
    :goto_0
    return-void

    .line 1226084
    :cond_0
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bc;->MEDIUM:LX/1bc;

    .line 1226085
    iput-object v1, v0, LX/1bX;->i:LX/1bc;

    .line 1226086
    move-object v0, v0

    .line 1226087
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1226088
    iget-object v1, p0, Lcom/facebook/audience/util/PrefetchUtils;->b:LX/1HI;

    const-class v2, Lcom/facebook/audience/util/PrefetchUtils;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 5
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 1226089
    iget-object v0, p0, Lcom/facebook/audience/util/PrefetchUtils;->c:LX/1Lg;

    sget-object v1, LX/1Li;->DIRECT_INBOX:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/util/PrefetchUtils;->a:LX/1M7;

    .line 1226090
    if-eqz p1, :cond_0

    .line 1226091
    new-instance v0, LX/36s;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1226092
    sget-object v1, LX/36t;->PROGRESSIVE:LX/36t;

    .line 1226093
    iput-object v1, v0, LX/36s;->h:LX/36t;

    .line 1226094
    iget-object v1, p0, Lcom/facebook/audience/util/PrefetchUtils;->d:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1226095
    iget-object v1, p0, Lcom/facebook/audience/util/PrefetchUtils;->a:LX/1M7;

    new-array v2, v4, [LX/36s;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1M7;->a([LX/36s;)V

    .line 1226096
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/util/PrefetchUtils;->a:LX/1M7;

    invoke-interface {v0, v4}, LX/1M7;->a(Z)V

    .line 1226097
    return-void
.end method
