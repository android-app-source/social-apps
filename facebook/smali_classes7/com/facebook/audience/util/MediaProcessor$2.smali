.class public final Lcom/facebook/audience/util/MediaProcessor$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7gj;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/1En;


# direct methods
.method public constructor <init>(LX/1En;LX/7gj;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1225982
    iput-object p1, p0, Lcom/facebook/audience/util/MediaProcessor$2;->c:LX/1En;

    iput-object p2, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    iput-object p3, p0, Lcom/facebook/audience/util/MediaProcessor$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1225983
    :try_start_0
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    invoke-virtual {v0}, LX/7gj;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1225984
    sget-object v3, LX/7hM;->c:LX/7hH;

    .line 1225985
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225986
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v2

    .line 1225987
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225988
    iget-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v2

    .line 1225989
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 1225990
    :cond_0
    iget-object v7, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225991
    iget-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1225992
    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225993
    iget v2, v1, LX/7gj;->g:I

    move v1, v2

    .line 1225994
    iget-object v2, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225995
    iget-boolean v4, v2, LX/7gj;->h:Z

    move v2, v4

    .line 1225996
    iget-object v4, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225997
    iget v5, v4, LX/7gj;->j:I

    move v4, v5

    .line 1225998
    iget-object v5, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1225999
    iget v8, v5, LX/7gj;->k:F

    move v5, v8

    .line 1226000
    invoke-static/range {v0 .. v5}, LX/7hI;->a(Landroid/graphics/Bitmap;IZLX/7hH;IF)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1226001
    iput-object v0, v7, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1226002
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1226003
    iget-object v1, v0, LX/7gj;->b:LX/7gi;

    move-object v0, v1

    .line 1226004
    invoke-static {v0}, LX/7hJ;->a(LX/7gi;)Ljava/io/File;

    move-result-object v0

    .line 1226005
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1226006
    :try_start_1
    iget-object v2, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1226007
    iget-object v3, v2, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v2, v3

    .line 1226008
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x5a

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1226009
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 1226010
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1226011
    iget-object v2, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1226012
    iput-object v0, v2, LX/7gj;->f:Ljava/lang/String;

    .line 1226013
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    const/4 v2, 0x0

    .line 1226014
    iput-object v2, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1226015
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x0

    const v3, 0x7a618ed8

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1226016
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1226017
    :goto_1
    if-eqz v6, :cond_2

    .line 1226018
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1226019
    :cond_2
    :goto_2
    return-void

    .line 1226020
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1226021
    iget-object v1, v0, LX/7gj;->b:LX/7gi;

    move-object v0, v1

    .line 1226022
    invoke-static {v0}, LX/7hJ;->a(LX/7gi;)Ljava/io/File;

    move-result-object v0

    .line 1226023
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1226024
    :try_start_5
    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    .line 1226025
    iget-object v4, v1, LX/7gj;->d:Ljava/lang/String;

    move-object v1, v4

    .line 1226026
    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1226027
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1226028
    const/16 v3, 0x400

    :try_start_6
    new-array v3, v3, [B

    .line 1226029
    :goto_3
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_5

    .line 1226030
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_3

    .line 1226031
    :catch_0
    move-exception v0

    move-object v6, v2

    .line 1226032
    :goto_4
    :try_start_7
    sget-object v2, LX/1En;->a:Ljava/lang/String;

    const-string v3, "Exception trying to save the media into disk"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1226033
    iget-object v2, p0, Lcom/facebook/audience/util/MediaProcessor$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v2, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 1226034
    if-eqz v6, :cond_4

    .line 1226035
    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 1226036
    :cond_4
    :goto_5
    if-eqz v1, :cond_2

    .line 1226037
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_2

    .line 1226038
    :catch_1
    move-exception v0

    .line 1226039
    sget-object v1, LX/1En;->a:Ljava/lang/String;

    const-string v2, "Exception trying to close input stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1226040
    :cond_5
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1226041
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 1226042
    iget-object v3, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1226043
    iput-object v0, v3, LX/7gj;->f:Ljava/lang/String;

    .line 1226044
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->a:LX/7gj;

    const/4 v3, 0x0

    .line 1226045
    iput-object v3, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    .line 1226046
    iget-object v0, p0, Lcom/facebook/audience/util/MediaProcessor$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v3, 0x0

    const v4, 0x6f4bd072

    invoke-static {v0, v3, v4}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-object v6, v1

    move-object v1, v2

    goto :goto_0

    .line 1226047
    :catch_2
    move-exception v0

    .line 1226048
    sget-object v1, LX/1En;->a:Ljava/lang/String;

    const-string v2, "Exception trying to close output stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1226049
    :catch_3
    move-exception v0

    .line 1226050
    sget-object v1, LX/1En;->a:Ljava/lang/String;

    const-string v2, "Exception trying to close input stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1226051
    :catch_4
    move-exception v0

    .line 1226052
    sget-object v2, LX/1En;->a:Ljava/lang/String;

    const-string v3, "Exception trying to close output stream"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1226053
    :catchall_0
    move-exception v0

    move-object v2, v6

    :goto_6
    if-eqz v2, :cond_6

    .line 1226054
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 1226055
    :cond_6
    :goto_7
    if-eqz v6, :cond_7

    .line 1226056
    :try_start_c
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 1226057
    :cond_7
    :goto_8
    throw v0

    .line 1226058
    :catch_5
    move-exception v1

    .line 1226059
    sget-object v2, LX/1En;->a:Ljava/lang/String;

    const-string v3, "Exception trying to close output stream"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 1226060
    :catch_6
    move-exception v1

    .line 1226061
    sget-object v2, LX/1En;->a:Ljava/lang/String;

    const-string v3, "Exception trying to close input stream"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 1226062
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v6, v1

    goto :goto_6

    :catchall_4
    move-exception v0

    move-object v2, v6

    move-object v6, v1

    goto :goto_6

    .line 1226063
    :catch_7
    move-exception v0

    move-object v1, v6

    goto/16 :goto_4

    :catch_8
    move-exception v0

    move-object v8, v6

    move-object v6, v1

    move-object v1, v8

    goto/16 :goto_4

    :catch_9
    move-exception v0

    move-object v1, v6

    move-object v6, v2

    goto/16 :goto_4
.end method
