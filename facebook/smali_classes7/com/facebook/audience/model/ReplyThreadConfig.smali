.class public Lcom/facebook/audience/model/ReplyThreadConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/ReplyThreadConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:LX/7gw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225138
    new-instance v0, LX/7gt;

    invoke-direct {v0}, LX/7gt;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/ReplyThreadConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7gu;)V
    .locals 1

    .prologue
    .line 1225132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225133
    iget-boolean v0, p1, LX/7gu;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    .line 1225134
    iget-boolean v0, p1, LX/7gu;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    .line 1225135
    iget-boolean v0, p1, LX/7gu;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    .line 1225136
    iget-object v0, p1, LX/7gu;->d:LX/7gw;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    .line 1225137
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1225121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    .line 1225123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    .line 1225124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    .line 1225125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1225126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    .line 1225127
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1225128
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1225129
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1225130
    goto :goto_2

    .line 1225131
    :cond_3
    invoke-static {}, LX/7gw;->values()[LX/7gw;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    goto :goto_3
.end method

.method public static newBuilder()LX/7gu;
    .locals 2

    .prologue
    .line 1225094
    new-instance v0, LX/7gu;

    invoke-direct {v0}, LX/7gu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1225120
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1225107
    if-ne p0, p1, :cond_1

    .line 1225108
    :cond_0
    :goto_0
    return v0

    .line 1225109
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/ReplyThreadConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1225110
    goto :goto_0

    .line 1225111
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 1225112
    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1225113
    goto :goto_0

    .line 1225114
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1225115
    goto :goto_0

    .line 1225116
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1225117
    goto :goto_0

    .line 1225118
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1225119
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1225106
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1225095
    iget-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225096
    iget-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225097
    iget-boolean v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225098
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    if-nez v0, :cond_3

    .line 1225099
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225100
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1225101
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1225102
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1225103
    goto :goto_2

    .line 1225104
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225105
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    invoke-virtual {v0}, LX/7gw;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3
.end method
