.class public Lcom/facebook/audience/model/MinimalReplyThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/MinimalReplyThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224857
    new-instance v0, LX/7gl;

    invoke-direct {v0}, LX/7gl;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/MinimalReplyThread;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7gm;)V
    .locals 1

    .prologue
    .line 1224850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224851
    iget-object v0, p1, LX/7gm;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    .line 1224852
    iget-boolean v0, p1, LX/7gm;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    .line 1224853
    iget-object v0, p1, LX/7gm;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    .line 1224854
    iget-object v0, p1, LX/7gm;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    .line 1224855
    iget-object v0, p1, LX/7gm;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    .line 1224856
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1224842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224843
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    .line 1224844
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    .line 1224845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    .line 1224846
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    .line 1224847
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    .line 1224848
    return-void

    .line 1224849
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/7gm;
    .locals 2

    .prologue
    .line 1224858
    new-instance v0, LX/7gm;

    invoke-direct {v0}, LX/7gm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1224841
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1224826
    if-ne p0, p1, :cond_1

    .line 1224827
    :cond_0
    :goto_0
    return v0

    .line 1224828
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/MinimalReplyThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1224829
    goto :goto_0

    .line 1224830
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 1224831
    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1224832
    goto :goto_0

    .line 1224833
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1224834
    goto :goto_0

    .line 1224835
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1224836
    goto :goto_0

    .line 1224837
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1224838
    goto :goto_0

    .line 1224839
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1224840
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1224818
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1224819
    iget-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224820
    iget-boolean v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224821
    iget-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224822
    iget-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224823
    iget-object v0, p0, Lcom/facebook/audience/model/MinimalReplyThread;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224824
    return-void

    .line 1224825
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
