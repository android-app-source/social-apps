.class public final Lcom/facebook/audience/model/AudienceControlData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/7gf;


# instance fields
.field public b:LX/7gf;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1224627
    const-class v0, Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224628
    new-instance v0, LX/7gg;

    invoke-direct {v0}, LX/7gg;-><init>()V

    .line 1224629
    sget-object v0, LX/7gf;->NEWS_FEED:LX/7gf;

    move-object v0, v0

    .line 1224630
    sput-object v0, Lcom/facebook/audience/model/AudienceControlData$Builder;->a:LX/7gf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1224631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224632
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData$Builder;->a:LX/7gf;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->b:LX/7gf;

    .line 1224633
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->c:Ljava/lang/String;

    .line 1224634
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->d:Ljava/lang/String;

    .line 1224635
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->e:Ljava/lang/String;

    .line 1224636
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->f:Ljava/lang/String;

    .line 1224637
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/model/AudienceControlData;
    .locals 2

    .prologue
    .line 1224638
    new-instance v0, Lcom/facebook/audience/model/AudienceControlData;

    invoke-direct {v0, p0}, Lcom/facebook/audience/model/AudienceControlData;-><init>(Lcom/facebook/audience/model/AudienceControlData$Builder;)V

    return-object v0
.end method

.method public setAudienceType(LX/7gf;)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audience_type"
    .end annotation

    .prologue
    .line 1224639
    iput-object p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->b:LX/7gf;

    .line 1224640
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1224641
    iput-object p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->c:Ljava/lang/String;

    .line 1224642
    return-object p0
.end method

.method public setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "low_res_profile_uri"
    .end annotation

    .prologue
    .line 1224643
    iput-object p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->d:Ljava/lang/String;

    .line 1224644
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 1224645
    iput-object p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->e:Ljava/lang/String;

    .line 1224646
    return-object p0
.end method

.method public setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profile_uri"
    .end annotation

    .prologue
    .line 1224647
    iput-object p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->f:Ljava/lang/String;

    .line 1224648
    return-object p0
.end method

.method public setRanking(I)Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ranking"
    .end annotation

    .prologue
    .line 1224649
    iput p1, p0, Lcom/facebook/audience/model/AudienceControlData$Builder;->g:I

    .line 1224650
    return-object p0
.end method
