.class public Lcom/facebook/audience/model/SharesheetSelectedAudience;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/SharesheetSelectedAudience;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225243
    new-instance v0, LX/7h3;

    invoke-direct {v0}, LX/7h3;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7h4;)V
    .locals 1

    .prologue
    .line 1225244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225245
    iget-object v0, p1, LX/7h4;->a:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    .line 1225246
    iget-object v0, p1, LX/7h4;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1225247
    iget-boolean v0, p1, LX/7h4;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    .line 1225248
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1225249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move v1, v2

    .line 1225251
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 1225252
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1225253
    aput-object v0, v4, v1

    .line 1225254
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1225255
    :cond_0
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    .line 1225256
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1225257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1225258
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_1

    move v2, v3

    :cond_1
    iput-boolean v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    .line 1225259
    return-void

    .line 1225260
    :cond_2
    sget-object v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_1
.end method

.method public static newBuilder()LX/7h4;
    .locals 2

    .prologue
    .line 1225261
    new-instance v0, LX/7h4;

    invoke-direct {v0}, LX/7h4;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1225262
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1225263
    if-ne p0, p1, :cond_1

    .line 1225264
    :cond_0
    :goto_0
    return v0

    .line 1225265
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/SharesheetSelectedAudience;

    if-nez v2, :cond_2

    move v0, v1

    .line 1225266
    goto :goto_0

    .line 1225267
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/SharesheetSelectedAudience;

    .line 1225268
    iget-object v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1225269
    goto :goto_0

    .line 1225270
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, p1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1225271
    goto :goto_0

    .line 1225272
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1225273
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1225274
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1225275
    iget-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225276
    iget-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1225277
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1225278
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1225279
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1225280
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225281
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225282
    return-void

    .line 1225283
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225284
    iget-object v0, p0, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/privacy/model/SelectablePrivacyData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1225285
    goto :goto_2
.end method
