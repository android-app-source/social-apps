.class public Lcom/facebook/audience/model/UploadShot;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/model/UploadShot$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/model/UploadShotSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/UploadShot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/7h9;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Z

.field public final h:LX/7gi;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:J

.field public final n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1225421
    const-class v0, Lcom/facebook/audience/model/UploadShot$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1225422
    const-class v0, Lcom/facebook/audience/model/UploadShotSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225423
    new-instance v0, LX/7h6;

    invoke-direct {v0}, LX/7h6;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/UploadShot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1225424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225425
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/audience/model/AudienceControlData;

    move v1, v2

    .line 1225426
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 1225427
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1225428
    aput-object v0, v4, v1

    .line 1225429
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1225430
    :cond_0
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    .line 1225431
    invoke-static {}, LX/7h9;->values()[LX/7h9;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    .line 1225432
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    .line 1225433
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    .line 1225434
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1225435
    iput-object v5, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1225436
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move v1, v2

    .line 1225437
    :goto_2
    array-length v0, v4

    if-ge v1, v0, :cond_2

    .line 1225438
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    .line 1225439
    aput-object v0, v4, v1

    .line 1225440
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1225441
    :cond_1
    sget-object v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    goto :goto_1

    .line 1225442
    :cond_2
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    .line 1225443
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_3

    move v2, v3

    :cond_3
    iput-boolean v2, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    .line 1225444
    invoke-static {}, LX/7gi;->values()[LX/7gi;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    .line 1225445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    .line 1225446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    .line 1225447
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    .line 1225448
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    .line 1225449
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    .line 1225450
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1225451
    iput-object v5, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1225452
    :goto_3
    return-void

    .line 1225453
    :cond_4
    sget-object v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    goto :goto_3
.end method

.method public constructor <init>(Lcom/facebook/audience/model/UploadShot$Builder;)V
    .locals 2

    .prologue
    .line 1225454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225455
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    .line 1225456
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->d:LX/7h9;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7h9;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    .line 1225457
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    .line 1225458
    iget-wide v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    .line 1225459
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->g:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1225460
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->h:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    .line 1225461
    iget-boolean v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    .line 1225462
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->j:LX/7gi;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7gi;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    .line 1225463
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    .line 1225464
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->l:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    .line 1225465
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    .line 1225466
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    .line 1225467
    iget-wide v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->o:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    .line 1225468
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot$Builder;->p:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1225469
    return-void
.end method

.method public static a(Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 2

    .prologue
    .line 1225507
    new-instance v0, Lcom/facebook/audience/model/UploadShot$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/audience/model/UploadShot$Builder;-><init>(Lcom/facebook/audience/model/UploadShot;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 2

    .prologue
    .line 1225470
    new-instance v0, Lcom/facebook/audience/model/UploadShot$Builder;

    invoke-direct {v0}, Lcom/facebook/audience/model/UploadShot$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1225471
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1225472
    if-ne p0, p1, :cond_1

    .line 1225473
    :cond_0
    :goto_0
    return v0

    .line 1225474
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/UploadShot;

    if-nez v2, :cond_2

    move v0, v1

    .line 1225475
    goto :goto_0

    .line 1225476
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/UploadShot;

    .line 1225477
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1225478
    goto :goto_0

    .line 1225479
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1225480
    goto :goto_0

    .line 1225481
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1225482
    goto :goto_0

    .line 1225483
    :cond_5
    iget-wide v2, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    iget-wide v4, p1, Lcom/facebook/audience/model/UploadShot;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 1225484
    goto :goto_0

    .line 1225485
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1225486
    goto :goto_0

    .line 1225487
    :cond_7
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1225488
    goto :goto_0

    .line 1225489
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/UploadShot;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1225490
    goto :goto_0

    .line 1225491
    :cond_9
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1225492
    goto :goto_0

    .line 1225493
    :cond_a
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1225494
    goto :goto_0

    .line 1225495
    :cond_b
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1225496
    goto :goto_0

    .line 1225497
    :cond_c
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1225498
    goto/16 :goto_0

    .line 1225499
    :cond_d
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1225500
    goto/16 :goto_0

    .line 1225501
    :cond_e
    iget-wide v2, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    iget-wide v4, p1, Lcom/facebook/audience/model/UploadShot;->m:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 1225502
    goto/16 :goto_0

    .line 1225503
    :cond_f
    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iget-object v3, p1, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1225504
    goto/16 :goto_0
.end method

.method public getAudience()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audience"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225505
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    return-object v0
.end method

.method public getBackstagePostType()LX/7h9;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "backstage_post_type"
    .end annotation

    .prologue
    .line 1225506
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    return-object v0
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation

    .prologue
    .line 1225419
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedAtTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "created_at_time"
    .end annotation

    .prologue
    .line 1225420
    iget-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    return-wide v0
.end method

.method public getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creative_editing_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1225418
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    return-object v0
.end method

.method public getInspirationPromptAnalytics()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_prompt_analytics"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225417
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    return-object v0
.end method

.method public getIsPrivate()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_private"
    .end annotation

    .prologue
    .line 1225416
    iget-boolean v0, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    return v0
.end method

.method public getMediaType()LX/7gi;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_type"
    .end annotation

    .prologue
    .line 1225415
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .prologue
    .line 1225414
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "path"
    .end annotation

    .prologue
    .line 1225413
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getReactionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_id"
    .end annotation

    .prologue
    .line 1225412
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getReplyThreadId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reply_thread_id"
    .end annotation

    .prologue
    .line 1225411
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getTimezoneOffset()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timezone_offset"
    .end annotation

    .prologue
    .line 1225410
    iget-wide v0, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    return-wide v0
.end method

.method public getVideoCreativeEditingData()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_creative_editing_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1225409
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1225408
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1225380
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225381
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1225382
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1225383
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1225384
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    invoke-virtual {v0}, LX/7h9;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225385
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225386
    iget-wide v4, p0, Lcom/facebook/audience/model/UploadShot;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1225387
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-nez v0, :cond_1

    .line 1225388
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225389
    :goto_1
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225390
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    .line 1225391
    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1225392
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1225393
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225394
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 1225395
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/audience/model/UploadShot;->g:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225396
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    invoke-virtual {v0}, LX/7gi;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225397
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225398
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225399
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225400
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225401
    iget-wide v4, p0, Lcom/facebook/audience/model/UploadShot;->m:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1225402
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-nez v0, :cond_4

    .line 1225403
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225404
    :goto_4
    return-void

    :cond_3
    move v0, v2

    .line 1225405
    goto :goto_3

    .line 1225406
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225407
    iget-object v0, p0, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_4
.end method
