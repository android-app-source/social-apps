.class public final Lcom/facebook/audience/model/AudienceControlData$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/audience/model/AudienceControlData;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224651
    new-instance v0, Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/AudienceControlData$Deserializer;->a:Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1224655
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1224656
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/audience/model/AudienceControlData;
    .locals 1

    .prologue
    .line 1224653
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData$Deserializer;->a:Lcom/facebook/audience/model/AudienceControlData_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData$Builder;

    .line 1224654
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1224652
    invoke-static {p1, p2}, Lcom/facebook/audience/model/AudienceControlData$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    return-object v0
.end method
