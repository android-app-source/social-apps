.class public final Lcom/facebook/audience/model/UploadShot$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/audience/model/UploadShot;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225379
    new-instance v0, Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/UploadShot$Deserializer;->a:Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1225377
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1225378
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/audience/model/UploadShot;
    .locals 1

    .prologue
    .line 1225375
    sget-object v0, Lcom/facebook/audience/model/UploadShot$Deserializer;->a:Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/UploadShot$Builder;

    .line 1225376
    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot$Builder;->a()Lcom/facebook/audience/model/UploadShot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1225374
    invoke-static {p1, p2}, Lcom/facebook/audience/model/UploadShot$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/audience/model/UploadShot;

    move-result-object v0

    return-object v0
.end method
