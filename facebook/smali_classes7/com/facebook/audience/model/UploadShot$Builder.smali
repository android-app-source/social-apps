.class public final Lcom/facebook/audience/model/UploadShot$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/7h9;

.field private static final b:LX/7gi;


# instance fields
.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/7h9;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:LX/7gi;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:J

.field public p:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1225371
    const-class v0, Lcom/facebook/audience/model/UploadShot_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225365
    new-instance v0, LX/7hA;

    invoke-direct {v0}, LX/7hA;-><init>()V

    .line 1225366
    sget-object v0, LX/7h9;->REGULAR:LX/7h9;

    move-object v0, v0

    .line 1225367
    sput-object v0, Lcom/facebook/audience/model/UploadShot$Builder;->a:LX/7h9;

    .line 1225368
    new-instance v0, LX/7hB;

    invoke-direct {v0}, LX/7hB;-><init>()V

    .line 1225369
    sget-object v0, LX/7gi;->PHOTO:LX/7gi;

    move-object v0, v0

    .line 1225370
    sput-object v0, Lcom/facebook/audience/model/UploadShot$Builder;->b:LX/7gi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1225352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225353
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1225354
    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->c:LX/0Px;

    .line 1225355
    sget-object v0, Lcom/facebook/audience/model/UploadShot$Builder;->a:LX/7h9;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->d:LX/7h9;

    .line 1225356
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->e:Ljava/lang/String;

    .line 1225357
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1225358
    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->h:LX/0Px;

    .line 1225359
    sget-object v0, Lcom/facebook/audience/model/UploadShot$Builder;->b:LX/7gi;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->j:LX/7gi;

    .line 1225360
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->k:Ljava/lang/String;

    .line 1225361
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->l:Ljava/lang/String;

    .line 1225362
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->m:Ljava/lang/String;

    .line 1225363
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->n:Ljava/lang/String;

    .line 1225364
    return-void
.end method

.method public constructor <init>(Lcom/facebook/audience/model/UploadShot;)V
    .locals 2

    .prologue
    .line 1225319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225320
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1225321
    instance-of v0, p1, Lcom/facebook/audience/model/UploadShot;

    if-eqz v0, :cond_0

    .line 1225322
    check-cast p1, Lcom/facebook/audience/model/UploadShot;

    .line 1225323
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->c:LX/0Px;

    .line 1225324
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->b:LX/7h9;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->d:LX/7h9;

    .line 1225325
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->e:Ljava/lang/String;

    .line 1225326
    iget-wide v0, p1, Lcom/facebook/audience/model/UploadShot;->d:J

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->f:J

    .line 1225327
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->g:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1225328
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->f:LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->h:LX/0Px;

    .line 1225329
    iget-boolean v0, p1, Lcom/facebook/audience/model/UploadShot;->g:Z

    iput-boolean v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->i:Z

    .line 1225330
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->h:LX/7gi;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->j:LX/7gi;

    .line 1225331
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->k:Ljava/lang/String;

    .line 1225332
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->l:Ljava/lang/String;

    .line 1225333
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->m:Ljava/lang/String;

    .line 1225334
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->n:Ljava/lang/String;

    .line 1225335
    iget-wide v0, p1, Lcom/facebook/audience/model/UploadShot;->m:J

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->o:J

    .line 1225336
    iget-object v0, p1, Lcom/facebook/audience/model/UploadShot;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->p:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1225337
    :goto_0
    return-void

    .line 1225338
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->c:LX/0Px;

    .line 1225339
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getBackstagePostType()LX/7h9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->d:LX/7h9;

    .line 1225340
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->e:Ljava/lang/String;

    .line 1225341
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->f:J

    .line 1225342
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->g:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1225343
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getInspirationPromptAnalytics()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->h:LX/0Px;

    .line 1225344
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->i:Z

    .line 1225345
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->j:LX/7gi;

    .line 1225346
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->k:Ljava/lang/String;

    .line 1225347
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->l:Ljava/lang/String;

    .line 1225348
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getReactionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->m:Ljava/lang/String;

    .line 1225349
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->n:Ljava/lang/String;

    .line 1225350
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getTimezoneOffset()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->o:J

    .line 1225351
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getVideoCreativeEditingData()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/UploadShot$Builder;->p:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/model/UploadShot;
    .locals 2

    .prologue
    .line 1225318
    new-instance v0, Lcom/facebook/audience/model/UploadShot;

    invoke-direct {v0, p0}, Lcom/facebook/audience/model/UploadShot;-><init>(Lcom/facebook/audience/model/UploadShot$Builder;)V

    return-object v0
.end method

.method public setAudience(LX/0Px;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audience"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;)",
            "Lcom/facebook/audience/model/UploadShot$Builder;"
        }
    .end annotation

    .prologue
    .line 1225316
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->c:LX/0Px;

    .line 1225317
    return-object p0
.end method

.method public setBackstagePostType(LX/7h9;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "backstage_post_type"
    .end annotation

    .prologue
    .line 1225314
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->d:LX/7h9;

    .line 1225315
    return-object p0
.end method

.method public setCaption(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation

    .prologue
    .line 1225312
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->e:Ljava/lang/String;

    .line 1225313
    return-object p0
.end method

.method public setCreatedAtTime(J)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "created_at_time"
    .end annotation

    .prologue
    .line 1225310
    iput-wide p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->f:J

    .line 1225311
    return-object p0
.end method

.method public setCreativeEditingData(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creative_editing_data"
    .end annotation

    .prologue
    .line 1225372
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->g:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1225373
    return-object p0
.end method

.method public setInspirationPromptAnalytics(LX/0Px;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_prompt_analytics"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;)",
            "Lcom/facebook/audience/model/UploadShot$Builder;"
        }
    .end annotation

    .prologue
    .line 1225308
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->h:LX/0Px;

    .line 1225309
    return-object p0
.end method

.method public setIsPrivate(Z)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_private"
    .end annotation

    .prologue
    .line 1225292
    iput-boolean p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->i:Z

    .line 1225293
    return-object p0
.end method

.method public setMediaType(LX/7gi;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_type"
    .end annotation

    .prologue
    .line 1225306
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->j:LX/7gi;

    .line 1225307
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "message"
    .end annotation

    .prologue
    .line 1225304
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->k:Ljava/lang/String;

    .line 1225305
    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "path"
    .end annotation

    .prologue
    .line 1225302
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->l:Ljava/lang/String;

    .line 1225303
    return-object p0
.end method

.method public setReactionId(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_id"
    .end annotation

    .prologue
    .line 1225300
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->m:Ljava/lang/String;

    .line 1225301
    return-object p0
.end method

.method public setReplyThreadId(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reply_thread_id"
    .end annotation

    .prologue
    .line 1225298
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->n:Ljava/lang/String;

    .line 1225299
    return-object p0
.end method

.method public setTimezoneOffset(J)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timezone_offset"
    .end annotation

    .prologue
    .line 1225296
    iput-wide p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->o:J

    .line 1225297
    return-object p0
.end method

.method public setVideoCreativeEditingData(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/audience/model/UploadShot$Builder;
    .locals 0
    .param p1    # Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_creative_editing_data"
    .end annotation

    .prologue
    .line 1225294
    iput-object p1, p0, Lcom/facebook/audience/model/UploadShot$Builder;->p:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1225295
    return-object p0
.end method
