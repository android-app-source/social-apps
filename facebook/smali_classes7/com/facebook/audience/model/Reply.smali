.class public Lcom/facebook/audience/model/Reply;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:F

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:LX/7gq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:J

.field private final j:J

.field public final k:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224962
    new-instance v0, LX/7gn;

    invoke-direct {v0}, LX/7gn;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/Reply;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7go;)V
    .locals 2

    .prologue
    .line 1224949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224950
    iget v0, p1, LX/7go;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/audience/model/Reply;->a:F

    .line 1224951
    iget-object v0, p1, LX/7go;->b:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    .line 1224952
    iget-boolean v0, p1, LX/7go;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/Reply;->c:Z

    .line 1224953
    iget-object v0, p1, LX/7go;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    .line 1224954
    iget-object v0, p1, LX/7go;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    .line 1224955
    iget-object v0, p1, LX/7go;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    .line 1224956
    iget-object v0, p1, LX/7go;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    .line 1224957
    iget-object v0, p1, LX/7go;->h:LX/7gq;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    .line 1224958
    iget-wide v0, p1, LX/7go;->i:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/Reply;->i:J

    .line 1224959
    iget-wide v0, p1, LX/7go;->j:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/Reply;->j:J

    .line 1224960
    iget-object v0, p1, LX/7go;->k:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    .line 1224961
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1224925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224926
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/audience/model/Reply;->a:F

    .line 1224927
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1224928
    iput-object v2, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    .line 1224929
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/audience/model/Reply;->c:Z

    .line 1224930
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1224931
    iput-object v2, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    .line 1224932
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    .line 1224933
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1224934
    iput-object v2, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    .line 1224935
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    .line 1224936
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1224937
    iput-object v2, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    .line 1224938
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/Reply;->i:J

    .line 1224939
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/model/Reply;->j:J

    .line 1224940
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1224941
    iput-object v2, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    .line 1224942
    :goto_5
    return-void

    .line 1224943
    :cond_0
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    goto :goto_0

    .line 1224944
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1224945
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    goto :goto_2

    .line 1224946
    :cond_3
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    goto :goto_3

    .line 1224947
    :cond_4
    invoke-static {}, LX/7gq;->values()[LX/7gq;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    goto :goto_4

    .line 1224948
    :cond_5
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    goto :goto_5
.end method

.method public static newBuilder()LX/7go;
    .locals 2

    .prologue
    .line 1224924
    new-instance v0, LX/7go;

    invoke-direct {v0}, LX/7go;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1224867
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1224897
    if-ne p0, p1, :cond_1

    .line 1224898
    :cond_0
    :goto_0
    return v0

    .line 1224899
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/Reply;

    if-nez v2, :cond_2

    move v0, v1

    .line 1224900
    goto :goto_0

    .line 1224901
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/Reply;

    .line 1224902
    iget v2, p0, Lcom/facebook/audience/model/Reply;->a:F

    iget v3, p1, Lcom/facebook/audience/model/Reply;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    .line 1224903
    goto :goto_0

    .line 1224904
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1224905
    goto :goto_0

    .line 1224906
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/model/Reply;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/Reply;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1224907
    goto :goto_0

    .line 1224908
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1224909
    goto :goto_0

    .line 1224910
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1224911
    goto :goto_0

    .line 1224912
    :cond_7
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1224913
    goto :goto_0

    .line 1224914
    :cond_8
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1224915
    goto :goto_0

    .line 1224916
    :cond_9
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1224917
    goto :goto_0

    .line 1224918
    :cond_a
    iget-wide v2, p0, Lcom/facebook/audience/model/Reply;->i:J

    iget-wide v4, p1, Lcom/facebook/audience/model/Reply;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    move v0, v1

    .line 1224919
    goto :goto_0

    .line 1224920
    :cond_b
    iget-wide v2, p0, Lcom/facebook/audience/model/Reply;->j:J

    iget-wide v4, p1, Lcom/facebook/audience/model/Reply;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 1224921
    goto :goto_0

    .line 1224922
    :cond_c
    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1224923
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1224896
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/audience/model/Reply;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/model/Reply;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/facebook/audience/model/Reply;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/audience/model/Reply;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1224868
    iget v0, p0, Lcom/facebook/audience/model/Reply;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1224869
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1224870
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224871
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/audience/model/Reply;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224872
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1224873
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224874
    :goto_2
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224875
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    if-nez v0, :cond_3

    .line 1224876
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224877
    :goto_3
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224878
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    if-nez v0, :cond_4

    .line 1224879
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224880
    :goto_4
    iget-wide v4, p0, Lcom/facebook/audience/model/Reply;->i:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1224881
    iget-wide v4, p0, Lcom/facebook/audience/model/Reply;->j:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1224882
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    if-nez v0, :cond_5

    .line 1224883
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224884
    :goto_5
    return-void

    .line 1224885
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224886
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1224887
    goto :goto_1

    .line 1224888
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224889
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 1224890
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224891
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_3

    .line 1224892
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224893
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    invoke-virtual {v0}, LX/7gq;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 1224894
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224895
    iget-object v0, p0, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_5
.end method
