.class public Lcom/facebook/audience/model/AudienceControlDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/model/AudienceControlData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1224724
    const-class v0, Lcom/facebook/audience/model/AudienceControlData;

    new-instance v1, Lcom/facebook/audience/model/AudienceControlDataSerializer;

    invoke-direct {v1}, Lcom/facebook/audience/model/AudienceControlDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1224725
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1224723
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/model/AudienceControlData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1224717
    if-nez p0, :cond_0

    .line 1224718
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1224719
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1224720
    invoke-static {p0, p1, p2}, Lcom/facebook/audience/model/AudienceControlDataSerializer;->b(Lcom/facebook/audience/model/AudienceControlData;LX/0nX;LX/0my;)V

    .line 1224721
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1224722
    return-void
.end method

.method private static b(Lcom/facebook/audience/model/AudienceControlData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1224710
    const-string v0, "audience_type"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getAudienceType()LX/7gf;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1224711
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224712
    const-string v0, "low_res_profile_uri"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getLowResProfileUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224713
    const-string v0, "name"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224714
    const-string v0, "profile_uri"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224715
    const-string v0, "ranking"

    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getRanking()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1224716
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1224709
    check-cast p1, Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/model/AudienceControlDataSerializer;->a(Lcom/facebook/audience/model/AudienceControlData;LX/0nX;LX/0my;)V

    return-void
.end method
