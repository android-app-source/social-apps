.class public Lcom/facebook/audience/model/UploadShotSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/model/UploadShot;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1225565
    const-class v0, Lcom/facebook/audience/model/UploadShot;

    new-instance v1, Lcom/facebook/audience/model/UploadShotSerializer;

    invoke-direct {v1}, Lcom/facebook/audience/model/UploadShotSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1225566
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1225548
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/model/UploadShot;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1225567
    if-nez p0, :cond_0

    .line 1225568
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1225569
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1225570
    invoke-static {p0, p1, p2}, Lcom/facebook/audience/model/UploadShotSerializer;->b(Lcom/facebook/audience/model/UploadShot;LX/0nX;LX/0my;)V

    .line 1225571
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1225572
    return-void
.end method

.method private static b(Lcom/facebook/audience/model/UploadShot;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1225550
    const-string v0, "audience"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1225551
    const-string v0, "backstage_post_type"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getBackstagePostType()LX/7h9;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1225552
    const-string v0, "caption"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225553
    const-string v0, "created_at_time"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1225554
    const-string v0, "creative_editing_data"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1225555
    const-string v0, "inspiration_prompt_analytics"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getInspirationPromptAnalytics()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1225556
    const-string v0, "is_private"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1225557
    const-string v0, "media_type"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1225558
    const-string v0, "message"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225559
    const-string v0, "path"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225560
    const-string v0, "reaction_id"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getReactionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225561
    const-string v0, "reply_thread_id"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225562
    const-string v0, "timezone_offset"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getTimezoneOffset()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1225563
    const-string v0, "video_creative_editing_data"

    invoke-virtual {p0}, Lcom/facebook/audience/model/UploadShot;->getVideoCreativeEditingData()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1225564
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1225549
    check-cast p1, Lcom/facebook/audience/model/UploadShot;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/model/UploadShotSerializer;->a(Lcom/facebook/audience/model/UploadShot;LX/0nX;LX/0my;)V

    return-void
.end method
