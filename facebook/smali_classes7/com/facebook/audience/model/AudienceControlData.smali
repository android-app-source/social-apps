.class public Lcom/facebook/audience/model/AudienceControlData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/model/AudienceControlData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/model/AudienceControlDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/7gf;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1224707
    const-class v0, Lcom/facebook/audience/model/AudienceControlData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1224706
    const-class v0, Lcom/facebook/audience/model/AudienceControlDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1224705
    new-instance v0, LX/7ge;

    invoke-direct {v0}, LX/7ge;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1224697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224698
    invoke-static {}, LX/7gf;->values()[LX/7gf;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    .line 1224699
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    .line 1224700
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    .line 1224701
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    .line 1224702
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    .line 1224703
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    .line 1224704
    return-void
.end method

.method public constructor <init>(Lcom/facebook/audience/model/AudienceControlData$Builder;)V
    .locals 1

    .prologue
    .line 1224689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224690
    iget-object v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->b:LX/7gf;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7gf;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    .line 1224691
    iget-object v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    .line 1224692
    iget-object v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    .line 1224693
    iget-object v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    .line 1224694
    iget-object v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    .line 1224695
    iget v0, p1, Lcom/facebook/audience/model/AudienceControlData$Builder;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    .line 1224696
    return-void
.end method

.method public static newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;
    .locals 2

    .prologue
    .line 1224688
    new-instance v0, Lcom/facebook/audience/model/AudienceControlData$Builder;

    invoke-direct {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1224687
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1224670
    if-ne p0, p1, :cond_1

    .line 1224671
    :cond_0
    :goto_0
    return v0

    .line 1224672
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/AudienceControlData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1224673
    goto :goto_0

    .line 1224674
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/AudienceControlData;

    .line 1224675
    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    iget-object v3, p1, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1224676
    goto :goto_0

    .line 1224677
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1224678
    goto :goto_0

    .line 1224679
    :cond_4
    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1224680
    goto :goto_0

    .line 1224681
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1224682
    goto :goto_0

    .line 1224683
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1224684
    goto :goto_0

    .line 1224685
    :cond_7
    iget v2, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    iget v3, p1, Lcom/facebook/audience/model/AudienceControlData;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1224686
    goto :goto_0
.end method

.method public getAudienceType()LX/7gf;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "audience_type"
    .end annotation

    .prologue
    .line 1224708
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1224669
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getLowResProfileUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "low_res_profile_uri"
    .end annotation

    .prologue
    .line 1224668
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 1224667
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profile_uri"
    .end annotation

    .prologue
    .line 1224666
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getRanking()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ranking"
    .end annotation

    .prologue
    .line 1224665
    iget v0, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1224664
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1224657
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->a:LX/7gf;

    invoke-virtual {v0}, LX/7gf;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224658
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224659
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224660
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224661
    iget-object v0, p0, Lcom/facebook/audience/model/AudienceControlData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1224662
    iget v0, p0, Lcom/facebook/audience/model/AudienceControlData;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224663
    return-void
.end method
