.class public Lcom/facebook/audience/model/ReplyThread;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/model/ReplyThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Z

.field public final d:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/audience/model/Reply;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225013
    new-instance v0, LX/7gr;

    invoke-direct {v0}, LX/7gr;-><init>()V

    sput-object v0, Lcom/facebook/audience/model/ReplyThread;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7gs;)V
    .locals 1

    .prologue
    .line 1225005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225006
    iget-object v0, p1, LX/7gs;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    .line 1225007
    iget-boolean v0, p1, LX/7gs;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThread;->b:Z

    .line 1225008
    iget-boolean v0, p1, LX/7gs;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThread;->c:Z

    .line 1225009
    iget-object v0, p1, LX/7gs;->d:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1225010
    iget-object v0, p1, LX/7gs;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    .line 1225011
    iget-object v0, p1, LX/7gs;->f:Lcom/facebook/audience/model/Reply;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    .line 1225012
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1225051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    .line 1225053
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/audience/model/ReplyThread;->b:Z

    .line 1225054
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/audience/model/ReplyThread;->c:Z

    .line 1225055
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1225056
    iput-object v3, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1225057
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/facebook/audience/model/Reply;

    .line 1225058
    :goto_3
    array-length v0, v1

    if-ge v2, v0, :cond_3

    .line 1225059
    sget-object v0, Lcom/facebook/audience/model/Reply;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1225060
    aput-object v0, v1, v2

    .line 1225061
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_0
    move v0, v2

    .line 1225062
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1225063
    goto :goto_1

    .line 1225064
    :cond_2
    sget-object v0, Lcom/facebook/audience/model/AudienceControlData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_2

    .line 1225065
    :cond_3
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    .line 1225066
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1225067
    iput-object v3, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    .line 1225068
    :goto_4
    return-void

    .line 1225069
    :cond_4
    sget-object v0, Lcom/facebook/audience/model/Reply;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    iput-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    goto :goto_4
.end method

.method public static a(Lcom/facebook/audience/model/ReplyThread;)LX/7gs;
    .locals 2

    .prologue
    .line 1225050
    new-instance v0, LX/7gs;

    invoke-direct {v0, p0}, LX/7gs;-><init>(Lcom/facebook/audience/model/ReplyThread;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1225070
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1225033
    if-ne p0, p1, :cond_1

    .line 1225034
    :cond_0
    :goto_0
    return v0

    .line 1225035
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/model/ReplyThread;

    if-nez v2, :cond_2

    move v0, v1

    .line 1225036
    goto :goto_0

    .line 1225037
    :cond_2
    check-cast p1, Lcom/facebook/audience/model/ReplyThread;

    .line 1225038
    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1225039
    goto :goto_0

    .line 1225040
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThread;->b:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/ReplyThread;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1225041
    goto :goto_0

    .line 1225042
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThread;->c:Z

    iget-boolean v3, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1225043
    goto :goto_0

    .line 1225044
    :cond_5
    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1225045
    goto :goto_0

    .line 1225046
    :cond_6
    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1225047
    goto :goto_0

    .line 1225048
    :cond_7
    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1225049
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1225032
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThread;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/model/ReplyThread;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1225014
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1225015
    iget-boolean v0, p0, Lcom/facebook/audience/model/ReplyThread;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225016
    iget-boolean v0, p0, Lcom/facebook/audience/model/ReplyThread;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225017
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    if-nez v0, :cond_2

    .line 1225018
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225019
    :goto_2
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225020
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_3

    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1225021
    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/Reply;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1225022
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_0
    move v0, v2

    .line 1225023
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1225024
    goto :goto_1

    .line 1225025
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225026
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/AudienceControlData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2

    .line 1225027
    :cond_3
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    if-nez v0, :cond_4

    .line 1225028
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225029
    :goto_4
    return-void

    .line 1225030
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225031
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/audience/model/Reply;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_4
.end method
