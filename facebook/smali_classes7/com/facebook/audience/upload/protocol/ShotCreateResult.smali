.class public Lcom/facebook/audience/upload/protocol/ShotCreateResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/upload/protocol/ShotCreateResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1225841
    new-instance v0, LX/7hF;

    invoke-direct {v0}, LX/7hF;-><init>()V

    sput-object v0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1225824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225825
    iput-object p1, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    .line 1225826
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1225827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225828
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1225829
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1225830
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1225831
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1225832
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    .line 1225833
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225834
    iget-object v0, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1225835
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1225836
    iget-object v0, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 1225837
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225838
    :goto_0
    return-void

    .line 1225839
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1225840
    iget-object v0, p0, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto :goto_0
.end method
