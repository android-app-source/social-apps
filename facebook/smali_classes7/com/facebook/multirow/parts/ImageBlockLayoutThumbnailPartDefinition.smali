.class public Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Cr;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Pn;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311875
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311876
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;
    .locals 3

    .prologue
    .line 1311877
    const-class v1, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    monitor-enter v1

    .line 1311878
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311879
    sput-object v2, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311880
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311881
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311882
    new-instance v0, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;-><init>()V

    .line 1311883
    move-object v0, v0

    .line 1311884
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1311888
    check-cast p2, LX/8Cr;

    check-cast p3, LX/1Pn;

    .line 1311889
    iget v0, p2, LX/8Cr;->c:I

    if-lez v0, :cond_0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p2, LX/8Cr;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xf30e1e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311890
    check-cast p1, LX/8Cr;

    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1311891
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311892
    iget-object v1, p1, LX/8Cr;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 1311893
    iget-object v1, p1, LX/8Cr;->b:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1311894
    :cond_0
    iget-object v1, p1, LX/8Cr;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1311895
    iget-object v1, p1, LX/8Cr;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1311896
    :cond_1
    iget v1, p1, LX/8Cr;->d:I

    if-lez v1, :cond_2

    .line 1311897
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, LX/8Cr;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1311898
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1311899
    :cond_2
    iget v1, p1, LX/8Cr;->e:I

    if-lez v1, :cond_3

    .line 1311900
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, LX/8Cr;->e:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1311901
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1311902
    :cond_3
    const/16 v1, 0x1f

    const v2, 0x6ad8ee7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
