.class public Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Cn;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311774
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311775
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;
    .locals 3

    .prologue
    .line 1311776
    const-class v1, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    monitor-enter v1

    .line 1311777
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311778
    sput-object v2, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311779
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311780
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311781
    new-instance v0, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;-><init>()V

    .line 1311782
    move-object v0, v0

    .line 1311783
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311784
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311785
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311786
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4b3e5fa5    # 1.2476325E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311787
    check-cast p1, LX/8Cn;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 1311788
    iget-object v1, p1, LX/8Cn;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1311789
    iget-object v1, p1, LX/8Cn;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 1311790
    :cond_0
    iget-object v1, p1, LX/8Cn;->a:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1311791
    const/16 v1, 0x1f

    const v2, -0x51d09185

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
