.class public Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "LX/8Cp;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311839
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311840
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;
    .locals 3

    .prologue
    .line 1311828
    const-class v1, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    monitor-enter v1

    .line 1311829
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311830
    sput-object v2, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311831
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311832
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311833
    new-instance v0, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;-><init>()V

    .line 1311834
    move-object v0, v0

    .line 1311835
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311836
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311837
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1311822
    new-instance v0, LX/8Cp;

    new-instance v1, LX/8sz;

    invoke-direct {v1}, LX/8sz;-><init>()V

    invoke-direct {v0, v1}, LX/8Cp;-><init>(LX/8sz;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7c97452e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311825
    check-cast p2, LX/8Cp;

    .line 1311826
    iget-object v1, p2, LX/8Cp;->a:LX/8sz;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1311827
    const/16 v1, 0x1f

    const v2, 0x5c232d7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1311823
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1311824
    return-void
.end method
