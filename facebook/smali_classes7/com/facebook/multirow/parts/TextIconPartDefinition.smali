.class public Lcom/facebook/multirow/parts/TextIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Ct;",
        "LX/8Cu;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0wM;

.field public final c:LX/0hL;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;LX/0hL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311940
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311941
    iput-object p1, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a:Landroid/content/res/Resources;

    .line 1311942
    iput-object p2, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->b:LX/0wM;

    .line 1311943
    iput-object p3, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->c:LX/0hL;

    .line 1311944
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/TextIconPartDefinition;
    .locals 6

    .prologue
    .line 1311967
    const-class v1, Lcom/facebook/multirow/parts/TextIconPartDefinition;

    monitor-enter v1

    .line 1311968
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311969
    sput-object v2, Lcom/facebook/multirow/parts/TextIconPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311970
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311971
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1311972
    new-instance p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v5

    check-cast v5, LX/0hL;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/multirow/parts/TextIconPartDefinition;-><init>(Landroid/content/res/Resources;LX/0wM;LX/0hL;)V

    .line 1311973
    move-object v0, p0

    .line 1311974
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311975
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/TextIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311976
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1311978
    check-cast p2, LX/8Ct;

    const/4 v1, 0x0

    .line 1311979
    iget v0, p2, LX/8Ct;->a:I

    if-nez v0, :cond_0

    .line 1311980
    new-instance v0, LX/8Cu;

    invoke-direct {v0, v1, v1}, LX/8Cu;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;)V

    .line 1311981
    :goto_0
    return-object v0

    .line 1311982
    :cond_0
    iget-object v0, p2, LX/8Ct;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->b:LX/0wM;

    iget v2, p2, LX/8Ct;->a:I

    iget-object v3, p2, LX/8Ct;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1311983
    :goto_1
    iget-object v2, p2, LX/8Ct;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a:Landroid/content/res/Resources;

    iget-object v2, p2, LX/8Ct;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1311984
    :cond_1
    new-instance v2, LX/8Cu;

    invoke-direct {v2, v0, v1}, LX/8Cu;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;)V

    move-object v0, v2

    goto :goto_0

    .line 1311985
    :cond_2
    iget-object v0, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a:Landroid/content/res/Resources;

    iget v2, p2, LX/8Ct;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6627aab7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311954
    check-cast p1, LX/8Ct;

    check-cast p2, LX/8Cu;

    check-cast p4, Landroid/widget/TextView;

    const/4 p3, 0x2

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x3

    .line 1311955
    iget-object v1, p2, LX/8Cu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1311956
    iget-object v1, p2, LX/8Cu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1311957
    :cond_0
    iget-object v1, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->c:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1311958
    invoke-virtual {p4}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1311959
    iget-object v2, p1, LX/8Ct;->d:LX/8Cs;

    invoke-virtual {v2}, LX/8Cs;->ordinal()I

    move-result v2

    iget-object v4, p2, LX/8Cu;->a:Landroid/graphics/drawable/Drawable;

    aput-object v4, v1, v2

    .line 1311960
    aget-object v2, v1, v5

    aget-object v4, v1, v7

    aget-object v5, v1, p3

    aget-object v1, v1, v6

    invoke-virtual {p4, v2, v4, v5, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1311961
    const/4 v1, 0x5

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1311962
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x2d33843f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1311963
    :cond_1
    invoke-virtual {p4}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1311964
    iget-object v2, p1, LX/8Ct;->d:LX/8Cs;

    invoke-virtual {v2}, LX/8Cs;->ordinal()I

    move-result v2

    iget-object v4, p2, LX/8Cu;->a:Landroid/graphics/drawable/Drawable;

    aput-object v4, v1, v2

    .line 1311965
    aget-object v2, v1, v5

    aget-object v4, v1, v7

    aget-object v5, v1, p3

    aget-object v1, v1, v6

    invoke-virtual {p4, v2, v4, v5, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1311966
    invoke-virtual {p4, v6}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 1311945
    check-cast p1, LX/8Ct;

    check-cast p4, Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1311946
    iget-object v0, p0, Lcom/facebook/multirow/parts/TextIconPartDefinition;->c:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311947
    invoke-virtual {p4}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1311948
    iget-object v1, p1, LX/8Ct;->d:LX/8Cs;

    invoke-virtual {v1}, LX/8Cs;->ordinal()I

    move-result v1

    aput-object v6, v0, v1

    .line 1311949
    aget-object v1, v0, v2

    aget-object v2, v0, v3

    aget-object v3, v0, v4

    aget-object v0, v0, v5

    invoke-virtual {p4, v1, v2, v3, v0}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1311950
    :goto_0
    return-void

    .line 1311951
    :cond_0
    invoke-virtual {p4}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1311952
    iget-object v1, p1, LX/8Ct;->d:LX/8Cs;

    invoke-virtual {v1}, LX/8Cs;->ordinal()I

    move-result v1

    aput-object v6, v0, v1

    .line 1311953
    aget-object v1, v0, v2

    aget-object v2, v0, v3

    aget-object v3, v0, v4

    aget-object v0, v0, v5

    invoke-virtual {p4, v1, v2, v3, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
