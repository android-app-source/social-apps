.class public Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Co;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311797
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311798
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;
    .locals 3

    .prologue
    .line 1311799
    const-class v1, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    monitor-enter v1

    .line 1311800
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311801
    sput-object v2, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311802
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311803
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311804
    new-instance v0, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;-><init>()V

    .line 1311805
    move-object v0, v0

    .line 1311806
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311807
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311808
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2ed01624

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311810
    check-cast p1, LX/8Co;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 1311811
    iget-object v1, p1, LX/8Co;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311812
    iget-boolean v1, p1, LX/8Co;->b:Z

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 1311813
    iget-object v1, p1, LX/8Co;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311814
    const/16 v1, 0x1f

    const v2, 0x7ff46c5a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1311815
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v0, 0x0

    .line 1311816
    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311817
    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311818
    return-void
.end method
