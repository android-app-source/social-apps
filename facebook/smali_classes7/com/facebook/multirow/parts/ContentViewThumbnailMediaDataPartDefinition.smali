.class public Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Ch;",
        "LX/1aZ;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1311574
    const-class v0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311575
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311576
    iput-object p1, p0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->b:LX/0Or;

    .line 1311577
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;
    .locals 4

    .prologue
    .line 1311578
    const-class v1, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    monitor-enter v1

    .line 1311579
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311580
    sput-object v2, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311581
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311582
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1311583
    new-instance v3, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;-><init>(LX/0Or;)V

    .line 1311584
    move-object v0, v3

    .line 1311585
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311586
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311587
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1311589
    check-cast p2, LX/8Ch;

    const/4 v0, 0x0

    .line 1311590
    if-eqz p2, :cond_0

    iget-object v1, p2, LX/8Ch;->a:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1311591
    :cond_0
    :goto_0
    return-object v0

    .line 1311592
    :cond_1
    iget-object v1, p2, LX/8Ch;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    iget-object v2, p2, LX/8Ch;->b:Ljava/lang/String;

    invoke-static {v2}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v2

    .line 1311593
    iput-object v2, v1, LX/1bX;->m:LX/1ny;

    .line 1311594
    move-object v1, v1

    .line 1311595
    iget v2, p2, LX/8Ch;->c:I

    if-lez v2, :cond_2

    iget v2, p2, LX/8Ch;->d:I

    if-lez v2, :cond_2

    new-instance v0, LX/1o9;

    iget v2, p2, LX/8Ch;->c:I

    iget v3, p2, LX/8Ch;->d:I

    invoke-direct {v0, v2, v3}, LX/1o9;-><init>(II)V

    .line 1311596
    :cond_2
    iput-object v0, v1, LX/1bX;->c:LX/1o9;

    .line 1311597
    move-object v0, v1

    .line 1311598
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1311599
    iget-object v0, p0, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3bef7d1d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311600
    check-cast p2, LX/1aZ;

    check-cast p3, LX/1Pp;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1311601
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailController(LX/1aZ;)V

    .line 1311602
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getController()LX/1aZ;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getImageRequest()LX/1bf;

    move-result-object p0

    const-class p1, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    invoke-interface {p3, v1, v2, p0, p1}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1311603
    const/16 v1, 0x1f

    const v2, -0x297220d3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
