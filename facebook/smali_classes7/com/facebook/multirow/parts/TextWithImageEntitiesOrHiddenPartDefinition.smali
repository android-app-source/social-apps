.class public Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/8Cv;",
        "Ljava/lang/Float;",
        "TE;",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311993
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311994
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;
    .locals 3

    .prologue
    .line 1311995
    const-class v1, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    monitor-enter v1

    .line 1311996
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311997
    sput-object v2, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1312000
    new-instance v0, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;-><init>()V

    .line 1312001
    move-object v0, v0

    .line 1312002
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1312003
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1312004
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1312005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1312006
    check-cast p2, LX/8Cv;

    check-cast p3, LX/1Pn;

    .line 1312007
    if-eqz p2, :cond_0

    iget v0, p2, LX/8Cv;->b:I

    if-eqz v0, :cond_0

    .line 1312008
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p2, LX/8Cv;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 1312009
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x233f762a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1312010
    check-cast p1, LX/8Cv;

    check-cast p2, Ljava/lang/Float;

    check-cast p4, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1312011
    if-nez p1, :cond_1

    const/4 v1, 0x0

    move-object v2, v1

    .line 1312012
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1312013
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1312014
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x628a740c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1312015
    :cond_1
    iget-object v1, p1, LX/8Cv;->a:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v2, v1

    goto :goto_0

    .line 1312016
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1312017
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 p0, 0x0

    cmpl-float v1, v1, p0

    if-lez v1, :cond_3

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    :goto_2
    iget p0, p1, LX/8Cv;->c:I

    invoke-virtual {p4, v2, v1, p0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;FI)V

    goto :goto_1

    :cond_3
    invoke-virtual {p4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getTextSize()F

    move-result v1

    goto :goto_2
.end method
