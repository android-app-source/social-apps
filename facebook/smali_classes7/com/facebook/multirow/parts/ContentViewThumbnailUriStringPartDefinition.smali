.class public Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311621
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311622
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;
    .locals 3

    .prologue
    .line 1311623
    const-class v1, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    monitor-enter v1

    .line 1311624
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311625
    sput-object v2, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311626
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311627
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311628
    new-instance v0, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;-><init>()V

    .line 1311629
    move-object v0, v0

    .line 1311630
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311631
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311632
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5ef7ea5b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311634
    check-cast p1, Ljava/lang/String;

    check-cast p3, LX/1Pp;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1311635
    invoke-virtual {p4, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1311636
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getController()LX/1aZ;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getImageRequest()LX/1bf;

    move-result-object p0

    const-class p2, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p2

    invoke-interface {p3, v1, v2, p0, p2}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1311637
    const/16 v1, 0x1f

    const v2, -0x7f3caba

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
