.class public Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311520
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311521
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;
    .locals 3

    .prologue
    .line 1311522
    const-class v1, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    monitor-enter v1

    .line 1311523
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311524
    sput-object v2, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311525
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311526
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311527
    new-instance v0, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;-><init>()V

    .line 1311528
    move-object v0, v0

    .line 1311529
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311530
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311531
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x465d8932

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311533
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1311534
    invoke-virtual {p4, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1311535
    const/16 v1, 0x1f

    const v2, -0x1b4a4a57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
