.class public Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Double;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/4nz;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311654
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1311655
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;
    .locals 3

    .prologue
    .line 1311656
    const-class v1, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    monitor-enter v1

    .line 1311657
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311658
    sput-object v2, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311659
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311660
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311661
    new-instance v0, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;-><init>()V

    .line 1311662
    move-object v0, v0

    .line 1311663
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311664
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311665
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311666
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3589b3b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311667
    check-cast p1, Ljava/lang/Double;

    check-cast p4, LX/4nz;

    .line 1311668
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_0

    .line 1311669
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 1311670
    iput-wide v4, p4, LX/4nz;->a:D

    .line 1311671
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x6002105f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
