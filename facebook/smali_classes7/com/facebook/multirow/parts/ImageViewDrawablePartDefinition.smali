.class public Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311903
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;
    .locals 3

    .prologue
    .line 1311910
    const-class v1, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    monitor-enter v1

    .line 1311911
    :try_start_0
    sget-object v0, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1311912
    sput-object v2, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1311913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1311915
    new-instance v0, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-direct {v0}, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;-><init>()V

    .line 1311916
    move-object v0, v0

    .line 1311917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1311918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1311919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1311920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1cb6c0d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1311907
    check-cast p1, Landroid/graphics/drawable/Drawable;

    check-cast p4, Landroid/widget/ImageView;

    .line 1311908
    invoke-virtual {p4, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311909
    const/16 v1, 0x1f

    const v2, 0x4fa824b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1311904
    check-cast p4, Landroid/widget/ImageView;

    .line 1311905
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311906
    return-void
.end method
