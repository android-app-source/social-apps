.class public final Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63392d13
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1229360
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1229361
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1229362
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1229363
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1229346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1229347
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1229348
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1229349
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1229350
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1229351
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1229352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1229353
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1229354
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1229355
    invoke-virtual {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1229356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;

    .line 1229357
    iput-object v0, v1, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->e:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1229358
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1229359
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229339
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->e:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->e:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1229340
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;->e:Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1229341
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/commerce/publishing/graphql/CommerceProductCreateMutationModels$CommerceProductCreateMutationFieldsModel;-><init>()V

    .line 1229342
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1229343
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1229344
    const v0, -0x19708ffa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1229345
    const v0, -0x27fb52f6

    return v0
.end method
