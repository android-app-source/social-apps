.class public final Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1231867
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1231868
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1231899
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1231900
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1231870
    if-nez p1, :cond_0

    move v0, v1

    .line 1231871
    :goto_0
    return v0

    .line 1231872
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1231873
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1231874
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1231875
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1231876
    const v3, -0x40c13bea

    const/4 v7, 0x0

    .line 1231877
    if-nez v2, :cond_1

    move v6, v7

    .line 1231878
    :goto_1
    move v2, v6

    .line 1231879
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1231880
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1231881
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1231882
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1231883
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1231884
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1231885
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1231886
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1231887
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1231888
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1231889
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1231890
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1231891
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    .line 1231892
    if-nez p1, :cond_2

    const/4 v6, 0x0

    .line 1231893
    :goto_2
    if-ge v7, p1, :cond_3

    .line 1231894
    invoke-virtual {p0, v2, v7}, LX/15i;->q(II)I

    move-result p2

    .line 1231895
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v6, v7

    .line 1231896
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1231897
    :cond_2
    new-array v6, p1, [I

    goto :goto_2

    .line 1231898
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p3, v6, v7}, LX/186;->a([IZ)I

    move-result v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x40c13bea -> :sswitch_1
        -0x346db6ef -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1231869
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1231814
    if-eqz p0, :cond_0

    .line 1231815
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1231816
    if-eq v0, p0, :cond_0

    .line 1231817
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1231818
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1231854
    sparse-switch p2, :sswitch_data_0

    .line 1231855
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1231856
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1231857
    const v1, -0x40c13bea

    .line 1231858
    if-eqz v0, :cond_0

    .line 1231859
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1231860
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1231861
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1231862
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1231863
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1231864
    :cond_0
    :goto_1
    return-void

    .line 1231865
    :sswitch_1
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    .line 1231866
    invoke-static {v0, p3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x40c13bea -> :sswitch_1
        -0x346db6ef -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1231848
    if-eqz p1, :cond_0

    .line 1231849
    invoke-static {p0, p1, p2}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1231850
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;

    .line 1231851
    if-eq v0, v1, :cond_0

    .line 1231852
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1231853
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1231847
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1231845
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1231846
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1231901
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1231902
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1231903
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1231904
    iput p2, p0, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->b:I

    .line 1231905
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1231844
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1231843
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1231840
    iget v0, p0, LX/1vt;->c:I

    .line 1231841
    move v0, v0

    .line 1231842
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1231837
    iget v0, p0, LX/1vt;->c:I

    .line 1231838
    move v0, v0

    .line 1231839
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1231834
    iget v0, p0, LX/1vt;->b:I

    .line 1231835
    move v0, v0

    .line 1231836
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1231831
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1231832
    move-object v0, v0

    .line 1231833
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1231822
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1231823
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1231824
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1231825
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1231826
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1231827
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1231828
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1231829
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/FetchProductCatalogQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1231830
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1231819
    iget v0, p0, LX/1vt;->c:I

    .line 1231820
    move v0, v0

    .line 1231821
    return v0
.end method
