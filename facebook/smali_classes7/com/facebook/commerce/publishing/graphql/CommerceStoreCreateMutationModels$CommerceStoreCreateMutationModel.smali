.class public final Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e797928
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1230539
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1230540
    const-class v0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1230541
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1230542
    return-void
.end method

.method private a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommerceStore"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1230543
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->e:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->e:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    .line 1230544
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->e:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1230545
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1230546
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1230547
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1230548
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1230549
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1230550
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1230551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1230552
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1230553
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    .line 1230554
    invoke-direct {p0}, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->a()Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1230555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;

    .line 1230556
    iput-object v0, v1, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;->e:Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel;

    .line 1230557
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1230558
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1230559
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;

    invoke-direct {v0}, Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;-><init>()V

    .line 1230560
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1230561
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1230562
    const v0, 0x670e058

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1230563
    const v0, -0x4926dd83

    return v0
.end method
