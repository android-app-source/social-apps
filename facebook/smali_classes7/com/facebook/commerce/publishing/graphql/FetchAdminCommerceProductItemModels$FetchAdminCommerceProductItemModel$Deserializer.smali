.class public final Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1231228
    const-class v0, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;

    new-instance v1, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1231229
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1231230
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1231231
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1231232
    const/4 v11, 0x0

    .line 1231233
    const/4 v10, 0x0

    .line 1231234
    const/4 v9, 0x0

    .line 1231235
    const/4 v8, 0x0

    .line 1231236
    const/4 v7, 0x0

    .line 1231237
    const/4 v6, 0x0

    .line 1231238
    const/4 v5, 0x0

    .line 1231239
    const/4 v4, 0x0

    .line 1231240
    const/4 v3, 0x0

    .line 1231241
    const/4 v2, 0x0

    .line 1231242
    const/4 v1, 0x0

    .line 1231243
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1231244
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1231245
    const/4 v1, 0x0

    .line 1231246
    :goto_0
    move v1, v1

    .line 1231247
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1231248
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1231249
    new-instance v1, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;

    invoke-direct {v1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;-><init>()V

    .line 1231250
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1231251
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1231252
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1231253
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1231254
    :cond_0
    return-object v1

    .line 1231255
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1231256
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_d

    .line 1231257
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1231258
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1231259
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1231260
    const-string p0, "__type__"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1231261
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    goto :goto_1

    .line 1231262
    :cond_4
    const-string p0, "commerce_featured_item"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1231263
    const/4 v1, 0x1

    .line 1231264
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1231265
    :cond_5
    const-string p0, "commerce_product_visibility"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1231266
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1231267
    :cond_6
    const-string p0, "description"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1231268
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1231269
    :cond_7
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1231270
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1231271
    :cond_8
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1231272
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1231273
    :cond_9
    const-string p0, "ordered_images"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1231274
    invoke-static {p1, v0}, LX/7jj;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1231275
    :cond_a
    const-string p0, "page"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 1231276
    invoke-static {p1, v0}, LX/7kC;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1231277
    :cond_b
    const-string p0, "productImagesLarge"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1231278
    invoke-static {p1, v0}, LX/7jl;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1231279
    :cond_c
    const-string p0, "product_item_price"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1231280
    invoke-static {p1, v0}, LX/7ir;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1231281
    :cond_d
    const/16 v12, 0xa

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1231282
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1231283
    if-eqz v1, :cond_e

    .line 1231284
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v10}, LX/186;->a(IZ)V

    .line 1231285
    :cond_e
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1231286
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1231287
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1231288
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1231289
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1231290
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1231291
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1231292
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1231293
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
