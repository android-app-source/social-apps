.class public final Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1230150
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1230151
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1230148
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1230149
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1230120
    if-nez p1, :cond_0

    .line 1230121
    :goto_0
    return v0

    .line 1230122
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1230123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1230124
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1230125
    const v2, 0x51fd3f55

    const/4 v5, 0x0

    .line 1230126
    if-nez v1, :cond_1

    move v4, v5

    .line 1230127
    :goto_1
    move v1, v4

    .line 1230128
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1230129
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1230130
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1230131
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1230132
    const v2, -0x45823ad3

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1230133
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1230134
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1230135
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1230136
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1230137
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1230138
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1230139
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1230140
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1230141
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1230142
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1230143
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1230144
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 1230145
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1230146
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1230147
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x45823ad3 -> :sswitch_2
        -0x2ee95c47 -> :sswitch_0
        0x51fd3f55 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1230119
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1230106
    sparse-switch p2, :sswitch_data_0

    .line 1230107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1230108
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1230109
    const v1, 0x51fd3f55

    .line 1230110
    if-eqz v0, :cond_0

    .line 1230111
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1230112
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1230113
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1230114
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1230115
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1230116
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1230117
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1230118
    const v1, -0x45823ad3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x45823ad3 -> :sswitch_1
        -0x2ee95c47 -> :sswitch_0
        0x51fd3f55 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1230100
    if-eqz p1, :cond_0

    .line 1230101
    invoke-static {p0, p1, p2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 1230102
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;

    .line 1230103
    if-eq v0, v1, :cond_0

    .line 1230104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1230105
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1230099
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1230152
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1230153
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1230068
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1230069
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1230070
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 1230071
    iput p2, p0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->b:I

    .line 1230072
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1230098
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1230097
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1230094
    iget v0, p0, LX/1vt;->c:I

    .line 1230095
    move v0, v0

    .line 1230096
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1230091
    iget v0, p0, LX/1vt;->c:I

    .line 1230092
    move v0, v0

    .line 1230093
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1230088
    iget v0, p0, LX/1vt;->b:I

    .line 1230089
    move v0, v0

    .line 1230090
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1230085
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1230086
    move-object v0, v0

    .line 1230087
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1230076
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1230077
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1230078
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1230079
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1230080
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1230081
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1230082
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1230083
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1230084
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1230073
    iget v0, p0, LX/1vt;->c:I

    .line 1230074
    move v0, v0

    .line 1230075
    return v0
.end method
