.class public final Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1231003
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1231004
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1231005
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1231006
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1231007
    if-nez p1, :cond_0

    .line 1231008
    :goto_0
    return v0

    .line 1231009
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1231010
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1231011
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1231012
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1231013
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1231014
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1231015
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x48b11fb2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1231020
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1231016
    packed-switch p0, :pswitch_data_0

    .line 1231017
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1231018
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x48b11fb2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1231019
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1230996
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->b(I)V

    .line 1230997
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1230998
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1230999
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1231000
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->a:LX/15i;

    .line 1231001
    iput p2, p0, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->b:I

    .line 1231002
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1230995
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1230994
    new-instance v0, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1230991
    iget v0, p0, LX/1vt;->c:I

    .line 1230992
    move v0, v0

    .line 1230993
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1230988
    iget v0, p0, LX/1vt;->c:I

    .line 1230989
    move v0, v0

    .line 1230990
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1230985
    iget v0, p0, LX/1vt;->b:I

    .line 1230986
    move v0, v0

    .line 1230987
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1230982
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1230983
    move-object v0, v0

    .line 1230984
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1230973
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1230974
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1230975
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1230976
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1230977
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1230978
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1230979
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1230980
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1230981
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1230970
    iget v0, p0, LX/1vt;->c:I

    .line 1230971
    move v0, v0

    .line 1230972
    return v0
.end method
