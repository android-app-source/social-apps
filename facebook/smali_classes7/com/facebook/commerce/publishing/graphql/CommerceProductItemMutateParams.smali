.class public Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/Integer;

.field public final h:Ljava/lang/String;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/03R;

.field public final k:LX/03R;

.field public final l:LX/7ja;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1229592
    new-instance v0, LX/7jX;

    invoke-direct {v0}, LX/7jX;-><init>()V

    sput-object v0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1229574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229575
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    .line 1229576
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    .line 1229577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    .line 1229578
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    .line 1229579
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    .line 1229580
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    .line 1229581
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    .line 1229582
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    .line 1229583
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1229584
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    .line 1229585
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    .line 1229586
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    .line 1229587
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/7ja;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->l:LX/7ja;

    .line 1229588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    .line 1229589
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1229590
    return-void

    .line 1229591
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;LX/0Px;LX/03R;LX/03R;LX/7ja;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/03R;",
            "LX/03R;",
            "LX/7ja;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1229610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229611
    iput-object p1, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    .line 1229612
    iput-object p2, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    .line 1229613
    iput-object p3, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    .line 1229614
    iput-object p4, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    .line 1229615
    iput-object p5, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    .line 1229616
    iput-object p6, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    .line 1229617
    iput-object p7, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    .line 1229618
    iput-object p8, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    .line 1229619
    iput-object p9, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    .line 1229620
    iput-object p10, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    .line 1229621
    iput-object p11, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    .line 1229622
    iput-object p12, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->l:LX/7ja;

    .line 1229623
    iput-object p13, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    .line 1229624
    iput-object p14, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1229625
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;LX/0Px;LX/03R;LX/03R;LX/7ja;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;B)V
    .locals 0

    .prologue
    .line 1229609
    invoke-direct/range {p0 .. p14}, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;LX/0Px;LX/03R;LX/03R;LX/7ja;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1229626
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1229627
    :cond_0
    :goto_0
    return-object p0

    .line 1229628
    :cond_1
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1229629
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add photoIds with no corresponding PENDING_MEDIA_ITEM_UPLOAD placeholders."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1229630
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 1229631
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1229632
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v6, :cond_5

    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1229633
    const-string v7, "pending_media_item_upload"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1229634
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1229635
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1229636
    :cond_4
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 1229637
    :cond_5
    if-ne v4, v1, :cond_6

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1229638
    invoke-static {p0}, LX/7jY;->a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;)LX/7jY;

    move-result-object v0

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1229639
    iput-object v1, v0, LX/7jY;->i:LX/0Px;

    .line 1229640
    move-object v0, v0

    .line 1229641
    invoke-virtual {v0}, LX/7jY;->a()Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    move-result-object p0

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1229642
    goto :goto_3
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1229608
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1229593
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229594
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229595
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229596
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229597
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229598
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229599
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->g:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1229600
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229601
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1229602
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->j:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1229603
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->k:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1229604
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->l:LX/7ja;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1229605
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229606
    iget-object v0, p0, Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;->n:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1229607
    return-void
.end method
