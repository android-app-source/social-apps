.class public Lcom/facebook/commerce/core/ui/NoticeView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Landroid/graphics/drawable/GradientDrawable;

.field private g:Landroid/graphics/drawable/GradientDrawable;

.field private h:Landroid/content/res/Resources;

.field private i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1228845
    sget-object v0, LX/7iu;->NOTIFY:LX/7iu;

    iget v0, v0, LX/7iu;->iconResId:I

    sput v0, Lcom/facebook/commerce/core/ui/NoticeView;->a:I

    .line 1228846
    sget-object v0, LX/7iu;->NOTIFY:LX/7iu;

    iget v0, v0, LX/7iu;->colorResId:I

    sput v0, Lcom/facebook/commerce/core/ui/NoticeView;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1228884
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1228885
    invoke-direct {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->a()V

    .line 1228886
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1228881
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1228882
    invoke-direct {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->a()V

    .line 1228883
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1228878
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1228879
    invoke-direct {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->a()V

    .line 1228880
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1228863
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/commerce/core/ui/NoticeView;->setOrientation(I)V

    .line 1228864
    const v0, 0x7f030c1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1228865
    invoke-virtual {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    .line 1228866
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    const v1, 0x7f0b0cea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->i:I

    .line 1228867
    const v0, 0x7f0d1dd2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1228868
    const v0, 0x7f0d1dd3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1228869
    const v0, 0x7f0d1dd4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1228870
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    const v1, 0x7f02112a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->f:Landroid/graphics/drawable/GradientDrawable;

    .line 1228871
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->f:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    sget v2, Lcom/facebook/commerce/core/ui/NoticeView;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1228872
    invoke-virtual {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021129

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->g:Landroid/graphics/drawable/GradientDrawable;

    .line 1228873
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->g:Landroid/graphics/drawable/GradientDrawable;

    iget v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->i:I

    iget-object v2, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    sget v3, Lcom/facebook/commerce/core/ui/NoticeView;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 1228874
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->f:Landroid/graphics/drawable/GradientDrawable;

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1228875
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    sget v1, Lcom/facebook/commerce/core/ui/NoticeView;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1228876
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->g:Landroid/graphics/drawable/GradientDrawable;

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1228877
    return-void
.end method


# virtual methods
.method public setLevel(LX/7iu;)V
    .locals 4

    .prologue
    .line 1228855
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1228856
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->f:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1228857
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->f:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    iget v2, p1, LX/7iu;->colorResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1228858
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->g:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1228859
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->g:Landroid/graphics/drawable/GradientDrawable;

    iget v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->i:I

    iget-object v2, p0, Lcom/facebook/commerce/core/ui/NoticeView;->h:Landroid/content/res/Resources;

    iget v3, p1, LX/7iu;->colorResId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 1228860
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p1, LX/7iu;->iconResId:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1228861
    invoke-virtual {p0}, Lcom/facebook/commerce/core/ui/NoticeView;->invalidate()V

    .line 1228862
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1228851
    iget-object v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1228852
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228853
    return-void

    .line 1228854
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1228847
    iget-object v1, p0, Lcom/facebook/commerce/core/ui/NoticeView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1228848
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/NoticeView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228849
    return-void

    .line 1228850
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
