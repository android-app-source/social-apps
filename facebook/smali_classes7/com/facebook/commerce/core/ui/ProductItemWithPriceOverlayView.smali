.class public Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1229033
    const-class v0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1229038
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1229039
    const p1, 0x7f03103f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1229040
    const p1, 0x7f0d2700

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1229041
    const p1, 0x7f0d2704

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1229042
    const p1, 0x7f020a3d

    invoke-virtual {p0, p1}, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->setBackgroundResource(I)V

    .line 1229043
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)V
    .locals 2
    .param p2    # Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229034
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1229035
    if-eqz p2, :cond_0

    .line 1229036
    iget-object v0, p0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p2}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1229037
    :cond_0
    return-void
.end method
