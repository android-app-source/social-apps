.class public Lcom/facebook/commerce/core/ui/ProductItemViewBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1228936
    const-class v0, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1228937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1228938
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/commerce/core/ui/ProductItemViewBinder;
    .locals 1

    .prologue
    .line 1228939
    new-instance v0, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;

    invoke-direct {v0}, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;-><init>()V

    .line 1228940
    move-object v0, v0

    .line 1228941
    return-object v0
.end method

.method public static a(LX/7iz;LX/7j1;)V
    .locals 4

    .prologue
    .line 1228942
    iget-object v0, p1, LX/7j1;->g:LX/0am;

    move-object v0, v0

    .line 1228943
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1228944
    iget-object v0, p1, LX/7j1;->g:LX/0am;

    move-object v0, v0

    .line 1228945
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v1, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, v1}, LX/7iz;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1228946
    :goto_0
    iget-object v0, p1, LX/7j1;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1228947
    iget-object v1, p0, LX/7iz;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228948
    iget-boolean v0, p1, LX/7j1;->b:Z

    move v0, v0

    .line 1228949
    if-eqz v0, :cond_1

    .line 1228950
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1228951
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020c0b

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1228952
    iget-object v0, p0, LX/7iz;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020639

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundResource(I)V

    .line 1228953
    iget-object v0, p0, LX/7iz;->r:Landroid/content/Context;

    const v1, 0x7f0814bc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/7iz;->r:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {p0, v0, v1}, LX/7iz;->a(LX/7iz;Ljava/lang/String;I)V

    .line 1228954
    iget-object v0, p0, LX/7iz;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021502

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/7iz;->a(LX/7iz;Landroid/graphics/drawable/Drawable;)V

    .line 1228955
    :goto_1
    iget-boolean v0, p1, LX/7j1;->a:Z

    move v0, v0

    .line 1228956
    if-eqz v0, :cond_3

    const/16 v1, 0x80

    .line 1228957
    :goto_2
    iget-object v2, p0, LX/7iz;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object p1, p0, LX/7iz;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1228958
    iget-object v2, p0, LX/7iz;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object p1, p0, LX/7iz;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1228959
    if-eqz v0, :cond_4

    .line 1228960
    iget-object v1, p0, LX/7iz;->q:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1228961
    :goto_3
    return-void

    .line 1228962
    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lcom/facebook/commerce/core/ui/ProductItemViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, v1}, LX/7iz;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 1228963
    :cond_1
    iget-boolean v0, p1, LX/7j1;->c:Z

    move v0, v0

    .line 1228964
    if-eqz v0, :cond_2

    .line 1228965
    iget-boolean v0, p1, LX/7j1;->d:Z

    move v0, v0

    .line 1228966
    invoke-static {p0, v0}, LX/7iz;->d(LX/7iz;Z)V

    .line 1228967
    iget-object v1, p0, LX/7iz;->r:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0814bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/7iz;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {p0, v1, v2}, LX/7iz;->a(LX/7iz;Ljava/lang/String;I)V

    .line 1228968
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/7iz;->a(LX/7iz;Landroid/graphics/drawable/Drawable;)V

    .line 1228969
    goto :goto_1

    .line 1228970
    :cond_2
    iget-boolean v0, p1, LX/7j1;->d:Z

    move v0, v0

    .line 1228971
    iget-object v1, p1, LX/7j1;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1228972
    invoke-static {p0, v0}, LX/7iz;->d(LX/7iz;Z)V

    .line 1228973
    iget-object v2, p0, LX/7iz;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {p0, v1, v2}, LX/7iz;->a(LX/7iz;Ljava/lang/String;I)V

    .line 1228974
    const/4 v2, 0x0

    invoke-static {p0, v2}, LX/7iz;->a(LX/7iz;Landroid/graphics/drawable/Drawable;)V

    .line 1228975
    goto/16 :goto_1

    .line 1228976
    :cond_3
    const/16 v1, 0xff

    goto/16 :goto_2

    .line 1228977
    :cond_4
    iget-object v1, p0, LX/7iz;->q:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->c()V

    goto :goto_3
.end method
