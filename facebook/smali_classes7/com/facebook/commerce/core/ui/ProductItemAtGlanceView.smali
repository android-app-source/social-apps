.class public Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field public d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1228911
    const-class v0, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1228912
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1228913
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->setOrientation(I)V

    .line 1228914
    const p1, 0x7f03103c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1228915
    const p1, 0x7f0d2700

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1228916
    const p1, 0x7f0d2703

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1228917
    const p1, 0x7f0d2704

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/commerce/core/ui/ProductItemAtGlanceView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1228918
    return-void
.end method
