.class public final Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1228314
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1228315
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1228370
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1228371
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1228321
    if-nez p1, :cond_0

    move v0, v1

    .line 1228322
    :goto_0
    return v0

    .line 1228323
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1228324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1228325
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1228326
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1228327
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1228328
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1228329
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1228330
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1228331
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1228332
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1228333
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1228334
    :sswitch_2
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1228335
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1228336
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    .line 1228337
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1228338
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1228339
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1228340
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1228341
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1228342
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1228343
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1228344
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1228345
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1228346
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1228347
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1228348
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1228349
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 1228350
    const v3, 0x6a396f09

    const/4 v5, 0x0

    .line 1228351
    if-nez v2, :cond_1

    move v4, v5

    .line 1228352
    :goto_1
    move v2, v4

    .line 1228353
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1228354
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1228355
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1228356
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1228357
    :sswitch_4
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    .line 1228358
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1228359
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1228360
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1228361
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1228362
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v8

    .line 1228363
    if-nez v8, :cond_2

    const/4 v4, 0x0

    .line 1228364
    :goto_2
    if-ge v5, v8, :cond_3

    .line 1228365
    invoke-virtual {p0, v2, v5}, LX/15i;->q(II)I

    move-result p1

    .line 1228366
    invoke-static {p0, p1, v3, p3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p1

    aput p1, v4, v5

    .line 1228367
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1228368
    :cond_2
    new-array v4, v8, [I

    goto :goto_2

    .line 1228369
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x729b7dec -> :sswitch_2
        -0xb5e5073 -> :sswitch_1
        0x38a550e5 -> :sswitch_3
        0x64e68067 -> :sswitch_0
        0x6a396f09 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1228320
    new-instance v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1228316
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1228317
    if-eqz v0, :cond_0

    .line 1228318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1228319
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1228258
    if-eqz p0, :cond_0

    .line 1228259
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1228260
    if-eq v0, p0, :cond_0

    .line 1228261
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1228262
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1228296
    sparse-switch p2, :sswitch_data_0

    .line 1228297
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1228298
    :sswitch_0
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1228299
    invoke-static {v0, p3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1228300
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceMerchantSettingsModel;

    .line 1228301
    invoke-static {v0, p3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1228302
    :goto_0
    :sswitch_1
    return-void

    .line 1228303
    :sswitch_2
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1228304
    const v1, 0x6a396f09

    .line 1228305
    if-eqz v0, :cond_0

    .line 1228306
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1228307
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1228308
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1228309
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1228310
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1228311
    :cond_0
    goto :goto_0

    .line 1228312
    :sswitch_3
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    .line 1228313
    invoke-static {v0, p3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x729b7dec -> :sswitch_0
        -0xb5e5073 -> :sswitch_1
        0x38a550e5 -> :sswitch_2
        0x64e68067 -> :sswitch_1
        0x6a396f09 -> :sswitch_3
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1228290
    if-eqz p1, :cond_0

    .line 1228291
    invoke-static {p0, p1, p2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 1228292
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    .line 1228293
    if-eq v0, v1, :cond_0

    .line 1228294
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1228295
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1228289
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1228372
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1228373
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1228253
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1228254
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1228255
    :cond_0
    iput-object p1, p0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 1228256
    iput p2, p0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->b:I

    .line 1228257
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1228263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1228264
    new-instance v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1228265
    iget v0, p0, LX/1vt;->c:I

    .line 1228266
    move v0, v0

    .line 1228267
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1228268
    iget v0, p0, LX/1vt;->c:I

    .line 1228269
    move v0, v0

    .line 1228270
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1228271
    iget v0, p0, LX/1vt;->b:I

    .line 1228272
    move v0, v0

    .line 1228273
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1228274
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1228275
    move-object v0, v0

    .line 1228276
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1228277
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1228278
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1228279
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1228280
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1228281
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1228282
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1228283
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1228284
    invoke-static {v3, v9, v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1228285
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1228286
    iget v0, p0, LX/1vt;->c:I

    .line 1228287
    move v0, v0

    .line 1228288
    return v0
.end method
