.class public final Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1227715
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;

    new-instance v1, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1227716
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1227717
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1227718
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1227719
    const/4 v11, 0x0

    .line 1227720
    const/4 v10, 0x0

    .line 1227721
    const/4 v9, 0x0

    .line 1227722
    const/4 v8, 0x0

    .line 1227723
    const/4 v7, 0x0

    .line 1227724
    const/4 v6, 0x0

    .line 1227725
    const/4 v5, 0x0

    .line 1227726
    const/4 v4, 0x0

    .line 1227727
    const/4 v3, 0x0

    .line 1227728
    const/4 v2, 0x0

    .line 1227729
    const/4 v1, 0x0

    .line 1227730
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, p0, :cond_2

    .line 1227731
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1227732
    const/4 v1, 0x0

    .line 1227733
    :goto_0
    move v1, v1

    .line 1227734
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1227735
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1227736
    new-instance v1, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;

    invoke-direct {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;-><init>()V

    .line 1227737
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1227738
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1227739
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1227740
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1227741
    :cond_0
    return-object v1

    .line 1227742
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1227743
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_b

    .line 1227744
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1227745
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1227746
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1227747
    const-string p0, "can_viewer_like"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1227748
    const/4 v2, 0x1

    .line 1227749
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1227750
    :cond_3
    const-string p0, "category_names"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1227751
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1227752
    :cond_4
    const-string p0, "category_type"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1227753
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1227754
    :cond_5
    const-string p0, "does_viewer_like"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1227755
    const/4 v1, 0x1

    .line 1227756
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1227757
    :cond_6
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1227758
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1227759
    :cond_7
    const-string p0, "name"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1227760
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1227761
    :cond_8
    const-string p0, "page_likers"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1227762
    invoke-static {p1, v0}, LX/7ii;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1227763
    :cond_9
    const-string p0, "profile_picture"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1227764
    invoke-static {p1, v0}, LX/7ij;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1227765
    :cond_a
    const-string p0, "viewer_profile_permissions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1227766
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1227767
    :cond_b
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1227768
    if-eqz v2, :cond_c

    .line 1227769
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 1227770
    :cond_c
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1227771
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1227772
    if-eqz v1, :cond_d

    .line 1227773
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 1227774
    :cond_d
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1227775
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1227776
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1227777
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1227778
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1227779
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
