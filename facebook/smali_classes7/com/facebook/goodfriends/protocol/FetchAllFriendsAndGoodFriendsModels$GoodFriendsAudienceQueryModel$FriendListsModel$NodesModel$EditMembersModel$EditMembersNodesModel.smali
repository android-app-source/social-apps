.class public final Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16323fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1301024
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1301023
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1301021
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1301022
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1301013
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1301014
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1301015
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1301016
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1301017
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1301018
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1301019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1301020
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1300996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1300997
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1300998
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    .line 1300999
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1301000
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;

    .line 1301001
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->f:Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    .line 1301002
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1301003
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1301011
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 1301012
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1301008
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;-><init>()V

    .line 1301009
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1301010
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1301007
    const v0, 0x3b40aa80

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1301006
    const v0, 0x423ebf6c

    return v0
.end method

.method public final j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1301004
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->f:Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->f:Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    .line 1301005
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->f:Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    return-object v0
.end method
