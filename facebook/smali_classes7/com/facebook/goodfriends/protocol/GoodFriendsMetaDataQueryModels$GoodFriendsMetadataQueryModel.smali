.class public final Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5899d367
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302456
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302432
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1302433
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1302434
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1302435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302436
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1302437
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1302438
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1302439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1302441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302442
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1302443
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    .line 1302444
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1302445
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;

    .line 1302446
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    .line 1302447
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302448
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302449
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    .line 1302450
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel$FriendListsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1302451
    new-instance v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMetaDataQueryModels$GoodFriendsMetadataQueryModel;-><init>()V

    .line 1302452
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1302453
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1302454
    const v0, -0x7541f50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1302455
    const v0, -0x6747e1ce

    return v0
.end method
