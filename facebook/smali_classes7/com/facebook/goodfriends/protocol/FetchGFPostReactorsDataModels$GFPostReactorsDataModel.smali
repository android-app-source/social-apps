.class public final Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x564f66f4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302043
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302040
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1302041
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1302042
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302044
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1302045
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1302046
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302047
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->f:Ljava/lang/String;

    .line 1302048
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1302049
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302050
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1302051
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1302052
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1302053
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1302054
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1302055
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1302056
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1302057
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302058
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1302031
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302032
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1302033
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    .line 1302034
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1302035
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;

    .line 1302036
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->g:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    .line 1302037
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302038
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1302039
    new-instance v0, LX/88I;

    invoke-direct {v0, p1}, LX/88I;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302030
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1302028
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1302029
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1302027
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1302024
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;-><init>()V

    .line 1302025
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1302026
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1302020
    const v0, -0x51ca2542

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1302023
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getReactors"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302021
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->g:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->g:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    .line 1302022
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel;->g:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel;

    return-object v0
.end method
