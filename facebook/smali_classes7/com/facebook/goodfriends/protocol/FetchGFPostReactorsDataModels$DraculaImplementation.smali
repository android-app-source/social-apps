.class public final Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1301793
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1301794
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1301791
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1301792
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1301778
    if-nez p1, :cond_0

    .line 1301779
    :goto_0
    return v0

    .line 1301780
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1301781
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1301782
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1301783
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1301784
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1301785
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1301786
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1301787
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1301788
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1301789
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1301790
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5f9a6c9f -> :sswitch_0
        -0x5e7db208 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1301777
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1301774
    sparse-switch p0, :sswitch_data_0

    .line 1301775
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1301776
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5f9a6c9f -> :sswitch_0
        -0x5e7db208 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1301773
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1301771
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->b(I)V

    .line 1301772
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1301795
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1301796
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1301797
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a:LX/15i;

    .line 1301798
    iput p2, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->b:I

    .line 1301799
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1301770
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1301769
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1301766
    iget v0, p0, LX/1vt;->c:I

    .line 1301767
    move v0, v0

    .line 1301768
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1301763
    iget v0, p0, LX/1vt;->c:I

    .line 1301764
    move v0, v0

    .line 1301765
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1301760
    iget v0, p0, LX/1vt;->b:I

    .line 1301761
    move v0, v0

    .line 1301762
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1301757
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1301758
    move-object v0, v0

    .line 1301759
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1301748
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1301749
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1301750
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1301751
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1301752
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1301753
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1301754
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1301755
    invoke-static {v3, v9, v2}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1301756
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1301745
    iget v0, p0, LX/1vt;->c:I

    .line 1301746
    move v0, v0

    .line 1301747
    return v0
.end method
