.class public final Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1300840
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1300841
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1300838
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1300839
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1300829
    if-nez p1, :cond_0

    .line 1300830
    :goto_0
    return v0

    .line 1300831
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1300832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1300833
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1300834
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1300835
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1300836
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1300837
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1bd42d20
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1300828
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1300825
    packed-switch p0, :pswitch_data_0

    .line 1300826
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1300827
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1bd42d20
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1300824
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1300822
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->b(I)V

    .line 1300823
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1300817
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1300818
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1300819
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->a:LX/15i;

    .line 1300820
    iput p2, p0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->b:I

    .line 1300821
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1300816
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1300815
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1300791
    iget v0, p0, LX/1vt;->c:I

    .line 1300792
    move v0, v0

    .line 1300793
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1300812
    iget v0, p0, LX/1vt;->c:I

    .line 1300813
    move v0, v0

    .line 1300814
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1300809
    iget v0, p0, LX/1vt;->b:I

    .line 1300810
    move v0, v0

    .line 1300811
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1300806
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1300807
    move-object v0, v0

    .line 1300808
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1300797
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1300798
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1300799
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1300800
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1300801
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1300802
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1300803
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1300804
    invoke-static {v3, v9, v2}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1300805
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1300794
    iget v0, p0, LX/1vt;->c:I

    .line 1300795
    move v0, v0

    .line 1300796
    return v0
.end method
