.class public final Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x41d7677d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302772
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302748
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1302770
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1302771
    return-void
.end method

.method private a()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302768
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    .line 1302769
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1302762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302763
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1302764
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1302765
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1302766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302767
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1302754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302755
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1302756
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    .line 1302757
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->a()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1302758
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;

    .line 1302759
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;->e:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    .line 1302760
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302761
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1302751
    new-instance v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel;-><init>()V

    .line 1302752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1302753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1302750
    const v0, -0x10b98c62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1302749
    const v0, -0x4efbb587

    return v0
.end method
