.class public final Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12806213
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302703
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302716
    const-class v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1302717
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1302718
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1302719
    iput-object p1, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->e:Ljava/lang/String;

    .line 1302720
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1302721
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1302722
    if-eqz v0, :cond_0

    .line 1302723
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1302724
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1302729
    iput-object p1, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->g:Ljava/lang/String;

    .line 1302730
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1302731
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1302732
    if-eqz v0, :cond_0

    .line 1302733
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1302734
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302725
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->e:Ljava/lang/String;

    .line 1302726
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302727
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    .line 1302728
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302704
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->g:Ljava/lang/String;

    .line 1302705
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1302706
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302707
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1302708
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1302709
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1302710
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1302711
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1302712
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1302713
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1302714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302715
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1302659
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1302660
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1302661
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    .line 1302662
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1302663
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    .line 1302664
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    .line 1302665
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1302666
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1302667
    new-instance v0, LX/88Z;

    invoke-direct {v0, p1}, LX/88Z;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1302668
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1302669
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1302670
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1302671
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1302672
    iput v2, p2, LX/18L;->c:I

    .line 1302673
    :goto_0
    return-void

    .line 1302674
    :cond_0
    const-string v0, "members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1302675
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v0

    .line 1302676
    if-eqz v0, :cond_2

    .line 1302677
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1302678
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1302679
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1302680
    :cond_1
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1302681
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1302682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1302683
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1302684
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1302685
    const-string v0, "id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1302686
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->a(Ljava/lang/String;)V

    .line 1302687
    :cond_0
    :goto_0
    return-void

    .line 1302688
    :cond_1
    const-string v0, "members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1302689
    invoke-direct {p0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->k()Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    move-result-object v0

    .line 1302690
    if-eqz v0, :cond_0

    .line 1302691
    if-eqz p3, :cond_2

    .line 1302692
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    .line 1302693
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;->a(I)V

    .line 1302694
    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->f:Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;

    goto :goto_0

    .line 1302695
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel$MembersModel;->a(I)V

    goto :goto_0

    .line 1302696
    :cond_3
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1302697
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1302698
    new-instance v0, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/GoodFriendsMutatorsModels$FriendListUpdateMembersFieldsModel$FriendListModel;-><init>()V

    .line 1302699
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1302700
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1302701
    const v0, -0x1bac2a11

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1302702
    const v0, 0xe198c7c

    return v0
.end method
