.class public final Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ec03137
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1301966
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1301928
    const-class v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1301964
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1301965
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1301956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1301957
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x5f9a6c9f

    invoke-static {v1, v0, v2}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1301958
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1301959
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1301960
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1301961
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1301962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1301963
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1301941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1301942
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1301943
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5f9a6c9f

    invoke-static {v2, v0, v3}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1301944
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1301945
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;

    .line 1301946
    iput v3, v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->e:I

    move-object v1, v0

    .line 1301947
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1301948
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    .line 1301949
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1301950
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;

    .line 1301951
    iput-object v0, v1, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->f:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    .line 1301952
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1301953
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1301954
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1301955
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedbackReactionInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1301939
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1301940
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1301936
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1301937
    const/4 v0, 0x0

    const v1, -0x5f9a6c9f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->e:I

    .line 1301938
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1301933
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;-><init>()V

    .line 1301934
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1301935
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1301932
    const v0, -0x12227a5b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1301931
    const v0, 0x2ac821ce

    return v0
.end method

.method public final j()Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1301929
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->f:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->f:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    .line 1301930
    iget-object v0, p0, Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel;->f:Lcom/facebook/goodfriends/protocol/FetchGFPostReactorsDataModels$GFPostReactorsDataModel$ReactorsModel$EdgesModel$NodeModel;

    return-object v0
.end method
