.class public final Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1301550
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1301551
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1301548
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1301549
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1301534
    if-nez p1, :cond_0

    .line 1301535
    :goto_0
    return v0

    .line 1301536
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1301537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1301538
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1301539
    const v2, -0x69aeee12

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1301540
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1301541
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1301542
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1301543
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1301544
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1301545
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1301546
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1301547
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69aeee12 -> :sswitch_1
        0x28afd67d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1301533
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1301528
    sparse-switch p2, :sswitch_data_0

    .line 1301529
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1301530
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1301531
    const v1, -0x69aeee12

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1301532
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69aeee12 -> :sswitch_1
        0x28afd67d -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1301489
    if-eqz p1, :cond_0

    .line 1301490
    invoke-static {p0, p1, p2}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1301491
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;

    .line 1301492
    if-eq v0, v1, :cond_0

    .line 1301493
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1301494
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1301527
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1301525
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1301526
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1301520
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1301521
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1301522
    :cond_0
    iput-object p1, p0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1301523
    iput p2, p0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->b:I

    .line 1301524
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1301488
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1301495
    new-instance v0, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1301496
    iget v0, p0, LX/1vt;->c:I

    .line 1301497
    move v0, v0

    .line 1301498
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1301499
    iget v0, p0, LX/1vt;->c:I

    .line 1301500
    move v0, v0

    .line 1301501
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1301502
    iget v0, p0, LX/1vt;->b:I

    .line 1301503
    move v0, v0

    .line 1301504
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1301505
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1301506
    move-object v0, v0

    .line 1301507
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1301508
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1301509
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1301510
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1301511
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1301512
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1301513
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1301514
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1301515
    invoke-static {v3, v9, v2}, Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/goodfriends/protocol/FetchCoverPicPayloadQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1301516
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1301517
    iget v0, p0, LX/1vt;->c:I

    .line 1301518
    move v0, v0

    .line 1301519
    return v0
.end method
