.class public Lcom/facebook/goodfriends/data/FriendData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public d:LX/87j;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1300542
    new-instance v0, LX/87i;

    invoke-direct {v0}, LX/87i;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/data/FriendData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1300543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300544
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    .line 1300545
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    .line 1300546
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    .line 1300547
    invoke-static {}, LX/87j;->values()[LX/87j;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->d:LX/87j;

    .line 1300548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->e:Ljava/lang/String;

    .line 1300549
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 1300550
    sget-object v4, LX/87j;->NOT_SELECTED:LX/87j;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/goodfriends/data/FriendData;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/87j;Ljava/lang/String;)V

    .line 1300551
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/87j;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1300552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300553
    iput-object p1, p0, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    .line 1300554
    iput-object p2, p0, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    .line 1300555
    iput-object p3, p0, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    .line 1300556
    iput-object p4, p0, Lcom/facebook/goodfriends/data/FriendData;->d:LX/87j;

    .line 1300557
    iput-object p5, p0, Lcom/facebook/goodfriends/data/FriendData;->e:Ljava/lang/String;

    .line 1300558
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1300559
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->d:LX/87j;

    sget-object v1, LX/87j;->SELECTED:LX/87j;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1300560
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1300561
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1300562
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1300563
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1300564
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->d:LX/87j;

    invoke-virtual {v0}, LX/87j;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1300565
    iget-object v0, p0, Lcom/facebook/goodfriends/data/FriendData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1300566
    return-void
.end method
