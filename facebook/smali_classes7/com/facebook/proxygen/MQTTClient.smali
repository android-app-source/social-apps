.class public Lcom/facebook/proxygen/MQTTClient;
.super LX/160;
.source ""


# instance fields
.field public mByteEventLogger:Lcom/facebook/proxygen/ByteEventLogger;

.field private final mCallback:Lcom/facebook/proxygen/MQTTClientCallback;

.field private final mFactory:Lcom/facebook/proxygen/MQTTClientFactory;

.field public mLogger:Lcom/facebook/proxygen/AnalyticsLogger;

.field private final mParams:LX/75w;

.field public mRadioStatusMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;


# direct methods
.method public constructor <init>(Lcom/facebook/proxygen/MQTTClientFactory;Lcom/facebook/proxygen/MQTTClientCallback;LX/75w;)V
    .locals 0

    .prologue
    .line 1170358
    invoke-direct {p0}, LX/160;-><init>()V

    .line 1170359
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClient;->mFactory:Lcom/facebook/proxygen/MQTTClientFactory;

    .line 1170360
    iput-object p2, p0, Lcom/facebook/proxygen/MQTTClient;->mCallback:Lcom/facebook/proxygen/MQTTClientCallback;

    .line 1170361
    iput-object p3, p0, Lcom/facebook/proxygen/MQTTClient;->mParams:LX/75w;

    .line 1170362
    return-void
.end method

.method private native init(Lcom/facebook/proxygen/MQTTClientFactory;Lcom/facebook/proxygen/MQTTClientCallback;Ljava/lang/String;IIZLcom/facebook/proxygen/AnalyticsLogger;Lcom/facebook/proxygen/ByteEventLogger;Lcom/facebook/proxygen/RadioStatusMonitor;)V
.end method


# virtual methods
.method public native close()V
.end method

.method public connect(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1170363
    invoke-virtual/range {p0 .. p5}, Lcom/facebook/proxygen/MQTTClient;->connectWithPassword(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1170364
    return-void
.end method

.method public native connect(Ljava/lang/String;I[BIIZ)V
.end method

.method public native connectWithPassword(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
.end method

.method public native disconnect()V
.end method

.method public init()V
    .locals 10

    .prologue
    .line 1170354
    iget-object v1, p0, Lcom/facebook/proxygen/MQTTClient;->mFactory:Lcom/facebook/proxygen/MQTTClientFactory;

    iget-object v2, p0, Lcom/facebook/proxygen/MQTTClient;->mCallback:Lcom/facebook/proxygen/MQTTClientCallback;

    iget-object v0, p0, Lcom/facebook/proxygen/MQTTClient;->mParams:LX/75w;

    iget-object v3, v0, LX/75w;->clientId:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/proxygen/MQTTClient;->mParams:LX/75w;

    iget-object v0, v0, LX/75w;->publishFormat:LX/75v;

    invoke-virtual {v0}, LX/75v;->getValue()I

    move-result v4

    iget-object v0, p0, Lcom/facebook/proxygen/MQTTClient;->mParams:LX/75w;

    iget v5, v0, LX/75w;->keepaliveSecs:I

    iget-object v0, p0, Lcom/facebook/proxygen/MQTTClient;->mParams:LX/75w;

    iget-boolean v6, v0, LX/75w;->enableTopicEncoding:Z

    iget-object v7, p0, Lcom/facebook/proxygen/MQTTClient;->mLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    iget-object v8, p0, Lcom/facebook/proxygen/MQTTClient;->mByteEventLogger:Lcom/facebook/proxygen/ByteEventLogger;

    iget-object v9, p0, Lcom/facebook/proxygen/MQTTClient;->mRadioStatusMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/proxygen/MQTTClient;->init(Lcom/facebook/proxygen/MQTTClientFactory;Lcom/facebook/proxygen/MQTTClientCallback;Ljava/lang/String;IIZLcom/facebook/proxygen/AnalyticsLogger;Lcom/facebook/proxygen/ByteEventLogger;Lcom/facebook/proxygen/RadioStatusMonitor;)V

    .line 1170355
    return-void
.end method

.method public publish(Ljava/lang/String;[BII)V
    .locals 7

    .prologue
    .line 1170356
    const/4 v3, 0x0

    array-length v4, p2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/proxygen/MQTTClient;->publish(Ljava/lang/String;[BIIII)V

    .line 1170357
    return-void
.end method

.method public native publish(Ljava/lang/String;[BIIII)V
.end method

.method public native sendKeepAliveOnce()V
.end method

.method public native sendPingResponse()V
.end method

.method public setAnalyticsLogger(Lcom/facebook/proxygen/AnalyticsLogger;)Lcom/facebook/proxygen/MQTTClient;
    .locals 0

    .prologue
    .line 1170348
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClient;->mLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    .line 1170349
    return-object p0
.end method

.method public setByteEventLogger(Lcom/facebook/proxygen/ByteEventLogger;)Lcom/facebook/proxygen/MQTTClient;
    .locals 0

    .prologue
    .line 1170350
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClient;->mByteEventLogger:Lcom/facebook/proxygen/ByteEventLogger;

    .line 1170351
    return-object p0
.end method

.method public native setForeground(Z)V
.end method

.method public setRadioStatusMonitor(Lcom/facebook/proxygen/RadioStatusMonitor;)Lcom/facebook/proxygen/MQTTClient;
    .locals 0

    .prologue
    .line 1170352
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClient;->mRadioStatusMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    .line 1170353
    return-object p0
.end method

.method public native stopConnectingIfConnectNotSent()V
.end method

.method public native subscribe([Ljava/lang/String;[I)V
.end method

.method public native unSubscribe([Ljava/lang/String;I)V
.end method
