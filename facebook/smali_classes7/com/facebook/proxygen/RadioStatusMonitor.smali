.class public Lcom/facebook/proxygen/RadioStatusMonitor;
.super LX/160;
.source ""


# static fields
.field private static mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;


# instance fields
.field private final mEventBase:Lcom/facebook/proxygen/EventBase;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1170462
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/proxygen/RadioStatusMonitor;->mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    return-void
.end method

.method private constructor <init>(Lcom/facebook/proxygen/EventBase;)V
    .locals 1

    .prologue
    .line 1170449
    invoke-direct {p0}, LX/160;-><init>()V

    .line 1170450
    iput-object p1, p0, Lcom/facebook/proxygen/RadioStatusMonitor;->mEventBase:Lcom/facebook/proxygen/EventBase;

    .line 1170451
    iget-object v0, p0, Lcom/facebook/proxygen/RadioStatusMonitor;->mEventBase:Lcom/facebook/proxygen/EventBase;

    invoke-virtual {p0, v0}, Lcom/facebook/proxygen/RadioStatusMonitor;->init(Lcom/facebook/proxygen/EventBase;)V

    .line 1170452
    return-void
.end method

.method public static declared-synchronized createOrGetMonitorInstance(Lcom/facebook/proxygen/EventBase;)Lcom/facebook/proxygen/RadioStatusMonitor;
    .locals 2

    .prologue
    .line 1170458
    const-class v1, Lcom/facebook/proxygen/RadioStatusMonitor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/proxygen/RadioStatusMonitor;->mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    if-nez v0, :cond_0

    .line 1170459
    new-instance v0, Lcom/facebook/proxygen/RadioStatusMonitor;

    invoke-direct {v0, p0}, Lcom/facebook/proxygen/RadioStatusMonitor;-><init>(Lcom/facebook/proxygen/EventBase;)V

    sput-object v0, Lcom/facebook/proxygen/RadioStatusMonitor;->mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;

    .line 1170460
    :cond_0
    sget-object v0, Lcom/facebook/proxygen/RadioStatusMonitor;->mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1170461
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getMonitorInstance()Lcom/facebook/proxygen/RadioStatusMonitor;
    .locals 2

    .prologue
    .line 1170457
    const-class v0, Lcom/facebook/proxygen/RadioStatusMonitor;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/facebook/proxygen/RadioStatusMonitor;->mMonitor:Lcom/facebook/proxygen/RadioStatusMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public native close()V
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 1170453
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/proxygen/RadioStatusMonitor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170454
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1170455
    return-void

    .line 1170456
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public native getRadioData()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public native init(Lcom/facebook/proxygen/EventBase;)V
.end method
