.class public Lcom/facebook/proxygen/MQTTClientFactory;
.super LX/160;
.source ""


# instance fields
.field private final mEventbase:Lcom/facebook/proxygen/EventBase;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field public mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field public mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

.field private final mSettings:LX/761;

.field public mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;


# direct methods
.method public constructor <init>(Lcom/facebook/proxygen/EventBase;Ljava/util/concurrent/Executor;LX/761;)V
    .locals 0

    .prologue
    .line 1170406
    invoke-direct {p0}, LX/160;-><init>()V

    .line 1170407
    iput-object p3, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    .line 1170408
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mEventbase:Lcom/facebook/proxygen/EventBase;

    .line 1170409
    iput-object p2, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mExecutor:Ljava/util/concurrent/Executor;

    .line 1170410
    return-void
.end method

.method private native init(Lcom/facebook/proxygen/EventBase;Ljava/util/concurrent/Executor;IIZZLcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/ZeroProtocolSettings;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
.end method


# virtual methods
.method public native close()V
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 1170402
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/proxygen/MQTTClientFactory;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170403
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1170404
    return-void

    .line 1170405
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public init()V
    .locals 18

    .prologue
    .line 1170411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mEventbase:Lcom/facebook/proxygen/EventBase;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget v4, v1, LX/761;->connectTimeout:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget v5, v1, LX/761;->pingRespTimeout:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-boolean v6, v1, LX/761;->verifyCertificates:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-boolean v7, v1, LX/761;->zlibCompression:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-object v11, v1, LX/761;->proxyAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget v12, v1, LX/761;->proxyPort:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-object v13, v1, LX/761;->secureProxyAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget v14, v1, LX/761;->secureProxyPort:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-object v15, v1, LX/761;->bypassProxyDomains:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-object v0, v1, LX/761;->proxyUserAgent:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mSettings:LX/761;

    iget-boolean v0, v1, LX/761;->proxyFallbackEnabled:Z

    move/from16 v17, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v17}, Lcom/facebook/proxygen/MQTTClientFactory;->init(Lcom/facebook/proxygen/EventBase;Ljava/util/concurrent/Executor;IIZZLcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/PersistentSSLCacheSettings;Lcom/facebook/proxygen/ZeroProtocolSettings;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1170412
    return-void
.end method

.method public native networkReset()V
.end method

.method public setPersistentDNSCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/MQTTClientFactory;
    .locals 0

    .prologue
    .line 1170400
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 1170401
    return-object p0
.end method

.method public setPersistentSSLCacheSettings(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)Lcom/facebook/proxygen/MQTTClientFactory;
    .locals 0

    .prologue
    .line 1170398
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 1170399
    return-object p0
.end method

.method public setZeroProtocolSettings(Lcom/facebook/proxygen/ZeroProtocolSettings;)Lcom/facebook/proxygen/MQTTClientFactory;
    .locals 0

    .prologue
    .line 1170396
    iput-object p1, p0, Lcom/facebook/proxygen/MQTTClientFactory;->mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;

    .line 1170397
    return-object p0
.end method
