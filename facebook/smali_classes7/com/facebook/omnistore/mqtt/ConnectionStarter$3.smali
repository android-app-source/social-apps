.class public final Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/2zl;


# direct methods
.method public constructor <init>(LX/2zl;)V
    .locals 0

    .prologue
    .line 1147192
    iput-object p1, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1147193
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    const/4 v1, 0x1

    .line 1147194
    iput-boolean v1, v0, LX/2zl;->mIsAppActive:Z

    .line 1147195
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    iget-boolean v0, v0, LX/2zl;->mPendingConnect:Z

    if-eqz v0, :cond_0

    .line 1147196
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    const/4 v1, 0x0

    .line 1147197
    iput-boolean v1, v0, LX/2zl;->mPendingConnect:Z

    .line 1147198
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    iget-object v0, v0, LX/2zl;->mCallback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    iget-object v0, v0, LX/2zl;->mChannelConnectivityTracker:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147199
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$3;->this$0:LX/2zl;

    iget-object v0, v0, LX/2zl;->mCallback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-virtual {v0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->connectionEstablished()V

    .line 1147200
    :cond_0
    return-void
.end method
