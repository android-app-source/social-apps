.class public final Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/2zl;

.field public final synthetic val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;


# direct methods
.method public constructor <init>(LX/2zl;Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V
    .locals 0

    .prologue
    .line 1147177
    iput-object p1, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    iput-object p2, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1147178
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    iget-object v1, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    .line 1147179
    iput-object v1, v0, LX/2zl;->mCallback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    .line 1147180
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    iget-object v0, v0, LX/2zl;->mChannelConnectivityTracker:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1147181
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    iget-boolean v0, v0, LX/2zl;->mIsAppActive:Z

    if-eqz v0, :cond_0

    .line 1147182
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    .line 1147183
    iput-boolean v2, v0, LX/2zl;->mPendingConnect:Z

    .line 1147184
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    iget-object v0, v0, LX/2zl;->mCallback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-virtual {v0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->connectionEstablished()V

    .line 1147185
    :goto_0
    return-void

    .line 1147186
    :cond_0
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    const/4 v1, 0x1

    .line 1147187
    iput-boolean v1, v0, LX/2zl;->mPendingConnect:Z

    .line 1147188
    goto :goto_0

    .line 1147189
    :cond_1
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;->this$0:LX/2zl;

    .line 1147190
    iput-boolean v2, v0, LX/2zl;->mPendingConnect:Z

    .line 1147191
    goto :goto_0
.end method
