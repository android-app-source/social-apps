.class public final Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final boxBottom:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "box_bottom"
    .end annotation
.end field

.field public final boxLeft:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "box_left"
    .end annotation
.end field

.field public final boxRight:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "box_right"
    .end annotation
.end field

.field public final boxTop:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "box_top"
    .end annotation
.end field

.field public final created:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "created"
    .end annotation
.end field

.field public final firstName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_name"
    .end annotation
.end field

.field public final isAutoTag:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_auto_tag"
    .end annotation
.end field

.field public final taggedId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_id"
    .end annotation
.end field

.field public final taggingProfileType:LX/7Gr;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagging_profile_type"
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232942
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232941
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1232940
    new-instance v0, LX/7kx;

    invoke-direct {v0}, LX/7kx;-><init>()V

    sput-object v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 1232938
    sget-object v8, LX/7Gr;->UNKNOWN:LX/7Gr;

    const/4 v9, 0x0

    const-string v12, ""

    const-string v13, ""

    move-object v1, p0

    move v5, v4

    move v6, v4

    move v7, v4

    move-wide v10, v2

    invoke-direct/range {v1 .. v13}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;-><init>(JFFFFLX/7Gr;ZJLjava/lang/String;Ljava/lang/String;)V

    .line 1232939
    return-void
.end method

.method public constructor <init>(JFFFFLX/7Gr;ZJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1232926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232927
    iput-wide p1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggedId:J

    .line 1232928
    iput p3, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxLeft:F

    .line 1232929
    iput p4, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxTop:F

    .line 1232930
    iput p5, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxRight:F

    .line 1232931
    iput p6, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxBottom:F

    .line 1232932
    iput-object p7, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggingProfileType:LX/7Gr;

    .line 1232933
    iput-boolean p8, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->isAutoTag:Z

    .line 1232934
    iput-wide p9, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->created:J

    .line 1232935
    iput-object p11, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->text:Ljava/lang/String;

    .line 1232936
    iput-object p12, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->firstName:Ljava/lang/String;

    .line 1232937
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232915
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggedId:J

    .line 1232916
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxLeft:F

    .line 1232917
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxTop:F

    .line 1232918
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxRight:F

    .line 1232919
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxBottom:F

    .line 1232920
    const-class v0, LX/7Gr;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Gr;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggingProfileType:LX/7Gr;

    .line 1232921
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->isAutoTag:Z

    .line 1232922
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->created:J

    .line 1232923
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->text:Ljava/lang/String;

    .line 1232924
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->firstName:Ljava/lang/String;

    .line 1232925
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232913
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232902
    iget-wide v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggedId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1232903
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxLeft:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1232904
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxTop:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1232905
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxRight:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1232906
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxBottom:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1232907
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggingProfileType:LX/7Gr;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1232908
    iget-boolean v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->isAutoTag:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1232909
    iget-wide v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->created:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1232910
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1232911
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->firstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1232912
    return-void
.end method
