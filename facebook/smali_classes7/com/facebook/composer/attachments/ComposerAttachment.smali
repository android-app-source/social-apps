.class public Lcom/facebook/composer/attachments/ComposerAttachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/attachments/ComposerAttachmentDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/ipc/media/MediaItem;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption"
    .end annotation
.end field

.field public final mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creative_editing_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mId:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "serialized_media_item"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation
.end field

.field public final mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_creative_editing_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_tagging_info"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mVideoUploadQuality:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_upload_quality"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232755
    const-class v0, Lcom/facebook/composer/attachments/ComposerAttachmentDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232756
    const-class v0, Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1232757
    new-instance v0, LX/7ku;

    invoke-direct {v0}, LX/7ku;-><init>()V

    sput-object v0, Lcom/facebook/composer/attachments/ComposerAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1232758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232759
    iput-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1232760
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    .line 1232761
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232762
    iput-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232763
    iput-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232764
    iput-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232765
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    .line 1232766
    const-string v0, "standard"

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    .line 1232767
    return-void
.end method

.method public constructor <init>(LX/7kv;)V
    .locals 1

    .prologue
    .line 1232768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232769
    iget-object v0, p1, LX/7kv;->b:Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1232770
    iget-object v0, p1, LX/7kv;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    .line 1232771
    iget-object v0, p1, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232772
    iget-object v0, p1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232773
    iget-object v0, p1, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232774
    iget-object v0, p1, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232775
    iget v0, p1, LX/7kv;->h:I

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    .line 1232776
    iget-object v0, p1, LX/7kv;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    .line 1232777
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1232778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232779
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1232780
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    .line 1232781
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    .line 1232782
    const-class v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232783
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1232784
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1232785
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1232786
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    .line 1232787
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    .line 1232788
    return-void
.end method

.method public static a(Ljava/util/Collection;)LX/0Px;
    .locals 3
    .param p0    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232789
    if-nez p0, :cond_0

    .line 1232790
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1232791
    :goto_0
    return-object v0

    .line 1232792
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1232793
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1232794
    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v0

    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232795
    if-eqz v0, :cond_1

    .line 1232796
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1232797
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;LX/0Px;)LX/0Px;
    .locals 6
    .param p0    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1232798
    if-nez p0, :cond_0

    .line 1232799
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1232800
    :goto_0
    return-object v0

    .line 1232801
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1232802
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1232803
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1232804
    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v4

    .line 1232805
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232806
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1232807
    iput-object v0, v4, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1232808
    :cond_1
    invoke-virtual {v4}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232809
    if-eqz v0, :cond_2

    .line 1232810
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232811
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 1232812
    goto :goto_2

    :cond_3
    move v0, v1

    .line 1232813
    goto :goto_1

    .line 1232814
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;LX/74n;)LX/0Px;
    .locals 3
    .param p0    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/74n;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/74n;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232815
    if-nez p0, :cond_0

    .line 1232816
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1232817
    :goto_0
    return-object v0

    .line 1232818
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1232819
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1232820
    invoke-static {v0, p1}, LX/7kv;->a(Landroid/net/Uri;LX/74n;)LX/7kv;

    move-result-object v0

    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232821
    if-eqz v0, :cond_1

    .line 1232822
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1232823
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0Px;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232824
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1232825
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_0

    invoke-virtual {p0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;

    .line 1232826
    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxLeft:F

    iget v3, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxTop:F

    iget v4, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxRight:F

    iget v5, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxBottom:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1232827
    new-instance v2, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-direct {v2, v1}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;)V

    .line 1232828
    new-instance v1, Lcom/facebook/photos/base/tagging/Tag;

    new-instance v3, Lcom/facebook/user/model/Name;

    iget-object v4, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->firstName:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->text:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v4, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggedId:J

    iget-object v6, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggingProfileType:LX/7Gr;

    iget-boolean v7, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->isAutoTag:Z

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JLX/7Gr;Z)V

    .line 1232829
    iget-wide v2, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->created:J

    .line 1232830
    iput-wide v2, v1, Lcom/facebook/photos/base/tagging/Tag;->h:J

    .line 1232831
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1232832
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1232833
    :cond_0
    return-object v9
.end method

.method private b(Lcom/facebook/photos/tagging/store/TagStoreCopy;)LX/0Px;
    .locals 20
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/tagging/store/TagStoreCopy;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232834
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    instance-of v2, v2, Lcom/facebook/photos/base/media/PhotoItem;

    if-nez v2, :cond_1

    .line 1232835
    :cond_0
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    .line 1232836
    :goto_0
    return-object v2

    .line 1232837
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v2

    .line 1232838
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->a(Lcom/facebook/ipc/media/MediaIdKey;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1232839
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    goto :goto_0

    .line 1232840
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v17

    .line 1232841
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/facebook/photos/tagging/store/TagStoreCopy;->b(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/0Px;->size()I

    move-result v19

    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/Tag;

    .line 1232842
    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->d()Lcom/facebook/photos/base/tagging/TagTarget;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v9

    .line 1232843
    new-instance v3, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->h()J

    move-result-wide v4

    iget v6, v9, Landroid/graphics/RectF;->left:F

    iget v7, v9, Landroid/graphics/RectF;->top:F

    iget v8, v9, Landroid/graphics/RectF;->right:F

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->i()LX/7Gr;

    move-result-object v10

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->l()Z

    move-result v11

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->e()J

    move-result-wide v12

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->f()Lcom/facebook/user/model/Name;

    move-result-object v14

    invoke-virtual {v14}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->f()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {v3 .. v15}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;-><init>(JFFFFLX/7Gr;ZJLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232844
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_1

    .line 1232845
    :cond_3
    invoke-virtual/range {v17 .. v17}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/74n;LX/75Q;)V
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232744
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    if-nez v0, :cond_1

    .line 1232745
    :cond_0
    :goto_0
    return-void

    .line 1232746
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    sget-object v1, LX/74j;->REMOTE_MEDIA:LX/74j;

    invoke-virtual {p1, v0, v1}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 1232747
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    iget-object v0, v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1232748
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1232749
    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    iget-object v1, v1, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(LX/0Px;)Ljava/util/List;

    move-result-object v1

    .line 1232750
    iget-object v2, p2, LX/75Q;->h:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1232751
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1232752
    if-eqz v2, :cond_0

    .line 1232753
    iget-object v2, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v2

    .line 1232754
    check-cast v0, LX/74x;

    invoke-virtual {p2, v0, v1}, LX/75Q;->a(LX/74x;Ljava/util/List;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/photos/tagging/store/TagStoreCopy;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232846
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b(Lcom/facebook/photos/tagging/store/TagStoreCopy;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->a(LX/0Px;)Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    .line 1232847
    return-void

    .line 1232848
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232743
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1232738
    if-ne p0, p1, :cond_1

    .line 1232739
    :cond_0
    :goto_0
    return v0

    .line 1232740
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 1232741
    goto :goto_0

    .line 1232742
    :cond_2
    iget-object v2, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v3, p1, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v3, p1, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iget-object v3, p1, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b()Lcom/facebook/ipc/media/MediaItem;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232737
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232736
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232735
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232734
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232733
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    return-object v0
.end method

.method public final f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232732
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    return-object v0
.end method

.method public final g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232731
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232730
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    return-object v0
.end method

.method public final i()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1232729
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1232719
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1232720
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1232721
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1232722
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1232723
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1232724
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1232725
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1232726
    iget v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1232727
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1232728
    return-void
.end method
