.class public Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1232982
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    new-instance v1, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1232983
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1232984
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1232985
    if-nez p0, :cond_0

    .line 1232986
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1232987
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1232988
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;->b(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;LX/0nX;LX/0my;)V

    .line 1232989
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1232990
    return-void
.end method

.method private static b(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1232991
    const-string v0, "photo_tags"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1232992
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1232993
    check-cast p1, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;->a(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;LX/0nX;LX/0my;)V

    return-void
.end method
