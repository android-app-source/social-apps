.class public Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/attachments/ComposerAttachment;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1232877
    const-class v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    new-instance v1, Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1232878
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1232889
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerAttachment;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1232890
    if-nez p0, :cond_0

    .line 1232891
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1232892
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1232893
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;->b(Lcom/facebook/composer/attachments/ComposerAttachment;LX/0nX;LX/0my;)V

    .line 1232894
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1232895
    return-void
.end method

.method private static b(Lcom/facebook/composer/attachments/ComposerAttachment;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1232880
    const-string v0, "serialized_media_item"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mSerializedMediaItemInternal:Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1232881
    const-string v0, "uri"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mUri:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232882
    const-string v0, "caption"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCaption:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1232883
    const-string v0, "creative_editing_data"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mCreativeEditingData:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1232884
    const-string v0, "video_creative_editing_data"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoCreativeEditingData:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1232885
    const-string v0, "video_tagging_info"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoTaggingInfo:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1232886
    const-string v0, "id"

    iget v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1232887
    const-string v0, "video_upload_quality"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerAttachment;->mVideoUploadQuality:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232888
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1232879
    check-cast p1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/attachments/ComposerAttachmentSerializer;->a(Lcom/facebook/composer/attachments/ComposerAttachment;LX/0nX;LX/0my;)V

    return-void
.end method
