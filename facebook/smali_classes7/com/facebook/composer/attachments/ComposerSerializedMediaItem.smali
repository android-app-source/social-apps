.class public Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/attachments/ComposerSerializedMediaItemDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mPhotoTags:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo_tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232943
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1232944
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItemSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1232945
    new-instance v0, LX/7kw;

    invoke-direct {v0}, LX/7kw;-><init>()V

    sput-object v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1232946
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1232947
    invoke-direct {p0, v0}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;-><init>(LX/0Px;)V

    .line 1232948
    return-void
.end method

.method private constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1232949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232950
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    .line 1232951
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1232952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232953
    sget-object v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    .line 1232954
    return-void
.end method

.method public static a(LX/0Px;)Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;
    .locals 1
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
            ">;)",
            "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;"
        }
    .end annotation

    .prologue
    .line 1232955
    new-instance v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;

    if-eqz p0, :cond_0

    :goto_0
    invoke-direct {v0, p0}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;-><init>(LX/0Px;)V

    return-object v0

    .line 1232956
    :cond_0
    sget-object p0, LX/0Q7;->a:LX/0Px;

    move-object p0, p0

    .line 1232957
    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1232958
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1232959
    iget-object v0, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem;->mPhotoTags:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1232960
    return-void
.end method
