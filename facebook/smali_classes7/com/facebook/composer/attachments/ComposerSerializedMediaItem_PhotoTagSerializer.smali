.class public Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1233043
    const-class v0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;

    new-instance v1, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1233044
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233042
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233036
    if-nez p0, :cond_0

    .line 1233037
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1233038
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233039
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;->b(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;LX/0nX;LX/0my;)V

    .line 1233040
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233041
    return-void
.end method

.method private static b(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1233024
    const-string v0, "tagged_id"

    iget-wide v2, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggedId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1233025
    const-string v0, "box_left"

    iget v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxLeft:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1233026
    const-string v0, "box_top"

    iget v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxTop:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1233027
    const-string v0, "box_right"

    iget v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxRight:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1233028
    const-string v0, "box_bottom"

    iget v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->boxBottom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1233029
    const-string v0, "tagging_profile_type"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->taggingProfileType:LX/7Gr;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1233030
    const-string v0, "is_auto_tag"

    iget-boolean v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->isAutoTag:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1233031
    const-string v0, "created"

    iget-wide v2, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->created:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1233032
    const-string v0, "text"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->text:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233033
    const-string v0, "first_name"

    iget-object v1, p0, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;->firstName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233034
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233035
    check-cast p1, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/attachments/ComposerSerializedMediaItem_PhotoTagSerializer;->a(Lcom/facebook/composer/attachments/ComposerSerializedMediaItem$PhotoTag;LX/0nX;LX/0my;)V

    return-void
.end method
