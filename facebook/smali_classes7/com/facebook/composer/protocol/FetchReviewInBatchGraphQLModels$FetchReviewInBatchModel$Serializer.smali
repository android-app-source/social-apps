.class public final Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1235133
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    new-instance v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1235134
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1235135
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1235136
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1235137
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 1235138
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1235139
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1235140
    if-eqz v2, :cond_0

    .line 1235141
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235142
    invoke-static {v1, v0, v5, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1235143
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1235144
    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 1235145
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235146
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1235147
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235148
    if-eqz v2, :cond_2

    .line 1235149
    const-string v3, "creator"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235150
    invoke-static {v1, v2, p1, p2}, LX/5u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1235151
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235152
    if-eqz v2, :cond_3

    .line 1235153
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235154
    invoke-static {v1, v2, p1, p2}, LX/4aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1235155
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1235156
    if-eqz v2, :cond_4

    .line 1235157
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235158
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1235159
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1235160
    if-eqz v2, :cond_5

    .line 1235161
    const-string v3, "page_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235162
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1235163
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235164
    if-eqz v2, :cond_7

    .line 1235165
    const-string v3, "photos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235166
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1235167
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_6

    .line 1235168
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1, p2}, LX/7lc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1235169
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1235170
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1235171
    :cond_7
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235172
    if-eqz v2, :cond_8

    .line 1235173
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235174
    invoke-static {v1, v2, p1, p2}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1235175
    :cond_8
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235176
    if-eqz v2, :cond_9

    .line 1235177
    const-string v3, "reviewer_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235178
    invoke-static {v1, v2, p1, p2}, LX/5uA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1235179
    :cond_9
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235180
    if-eqz v2, :cond_a

    .line 1235181
    const-string v3, "story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235182
    invoke-static {v1, v2, p1}, LX/5u7;->a(LX/15i;ILX/0nX;)V

    .line 1235183
    :cond_a
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1235184
    if-eqz v2, :cond_b

    .line 1235185
    const-string v3, "value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235186
    invoke-static {v1, v2, p1}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 1235187
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1235188
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1235189
    check-cast p1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;->a(Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;LX/0nX;LX/0my;)V

    return-void
.end method
