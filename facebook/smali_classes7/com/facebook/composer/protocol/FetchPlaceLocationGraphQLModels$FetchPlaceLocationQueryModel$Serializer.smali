.class public final Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1234983
    const-class v0, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    new-instance v1, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1234984
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1234985
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1234986
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1234987
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1234988
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234989
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1234990
    if-eqz v2, :cond_0

    .line 1234991
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234992
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1234993
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1234994
    if-eqz v2, :cond_1

    .line 1234995
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234996
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1234997
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1234998
    if-eqz v2, :cond_2

    .line 1234999
    const-string p0, "location"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235000
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1235001
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1235002
    if-eqz v2, :cond_3

    .line 1235003
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1235004
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1235005
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1235006
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1235007
    check-cast p1, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel$Serializer;->a(Lcom/facebook/composer/protocol/FetchPlaceLocationGraphQLModels$FetchPlaceLocationQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
