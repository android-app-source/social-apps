.class public Lcom/facebook/composer/protocol/PostReviewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/protocol/PostReviewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final e:I

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public final h:Lcom/facebook/ipc/media/MediaItem;

.field public final i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1235492
    new-instance v0, LX/7lh;

    invoke-direct {v0}, LX/7lh;-><init>()V

    sput-object v0, Lcom/facebook/composer/protocol/PostReviewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7li;)V
    .locals 2

    .prologue
    .line 1235459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235460
    iget-object v0, p1, LX/7li;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->a:Ljava/lang/String;

    .line 1235461
    iget-wide v0, p1, LX/7li;->b:J

    iput-wide v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    .line 1235462
    iget-object v0, p1, LX/7li;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    .line 1235463
    iget-object v0, p1, LX/7li;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1235464
    iget v0, p1, LX/7li;->e:I

    iput v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    .line 1235465
    iget-object v0, p1, LX/7li;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    .line 1235466
    iget-object v0, p1, LX/7li;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    .line 1235467
    iget-object v0, p1, LX/7li;->h:Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->h:Lcom/facebook/ipc/media/MediaItem;

    .line 1235468
    iget v0, p1, LX/7li;->i:I

    iput v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->i:I

    .line 1235469
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1235481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235482
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->a:Ljava/lang/String;

    .line 1235483
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    .line 1235484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    .line 1235485
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1235486
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    .line 1235487
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    .line 1235488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    .line 1235489
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->h:Lcom/facebook/ipc/media/MediaItem;

    .line 1235490
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->i:I

    .line 1235491
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1235480
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1235470
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1235471
    iget-wide v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1235472
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1235473
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1235474
    iget v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1235475
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1235476
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1235477
    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->h:Lcom/facebook/ipc/media/MediaItem;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1235478
    iget v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1235479
    return-void
.end method
