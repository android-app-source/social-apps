.class public final Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6bed54b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1235124
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1235123
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1235121
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1235122
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235103
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->e:Ljava/lang/String;

    .line 1235104
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235119
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1235120
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1235125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1235126
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1235127
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1235128
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1235129
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1235130
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1235131
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1235132
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1235111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1235112
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1235113
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1235114
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1235115
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;

    .line 1235116
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1235117
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1235118
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235110
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1235107
    new-instance v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;

    invoke-direct {v0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;-><init>()V

    .line 1235108
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1235109
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1235106
    const v0, 0x7033117a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1235105
    const v0, 0x4984e12

    return v0
.end method
