.class public final Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5tj;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf002b08
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1235225
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1235217
    const-class v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1235218
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1235219
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235220
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1235221
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1235222
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235223
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1235224
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235210
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1235211
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1235226
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel$PhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->k:Ljava/util/List;

    .line 1235227
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235297
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1235298
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235228
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1235229
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    return-object v0
.end method

.method private s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235230
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1235231
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    return-object v0
.end method

.method private t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235232
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1235233
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1235234
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1235235
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->m()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1235236
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1235237
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1235238
    invoke-virtual {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1235239
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->p()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 1235240
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1235241
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1235242
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1235243
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1235244
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1235245
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1235246
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1235247
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1235248
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1235249
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1235250
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->j:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1235251
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1235252
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1235253
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1235254
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1235255
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1235256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1235257
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1235258
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1235259
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1235260
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1235261
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1235262
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235263
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1235264
    :cond_0
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1235265
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1235266
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1235267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235268
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1235269
    :cond_1
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1235270
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1235271
    if-eqz v2, :cond_2

    .line 1235272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235273
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->k:Ljava/util/List;

    move-object v1, v0

    .line 1235274
    :cond_2
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1235275
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1235276
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1235277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235278
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->l:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1235279
    :cond_3
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1235280
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1235281
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1235282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235283
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1235284
    :cond_4
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1235285
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1235286
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1235287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235288
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1235289
    :cond_5
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1235290
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1235291
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1235292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    .line 1235293
    iput-object v0, v1, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1235294
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1235295
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1235296
    new-instance v0, LX/7la;

    invoke-direct {v0, p1}, LX/7la;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235212
    invoke-virtual {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1235213
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1235214
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->f:J

    .line 1235215
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->j:I

    .line 1235216
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1235190
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1235191
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1235192
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1235193
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1235194
    iget v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->j:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1235195
    new-instance v0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;

    invoke-direct {v0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;-><init>()V

    .line 1235196
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1235197
    return-object v0
.end method

.method public final bg_()J
    .locals 2

    .prologue
    .line 1235198
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1235199
    iget-wide v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->f:J

    return-wide v0
.end method

.method public final synthetic bh_()LX/1VU;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235200
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235201
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235202
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1235203
    const v0, 0x401757a8

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235204
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1235205
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235206
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->i:Ljava/lang/String;

    .line 1235207
    iget-object v0, p0, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235208
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->r()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1235209
    invoke-direct {p0}, Lcom/facebook/composer/protocol/FetchReviewInBatchGraphQLModels$FetchReviewInBatchModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    return-object v0
.end method
