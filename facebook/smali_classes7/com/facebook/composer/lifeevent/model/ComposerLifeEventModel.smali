.class public Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public final c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1233216
    new-instance v0, LX/7l2;

    invoke-direct {v0}, LX/7l2;-><init>()V

    sput-object v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7l3;)V
    .locals 1

    .prologue
    .line 1233217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233218
    iget-object v0, p1, LX/7l3;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    .line 1233219
    iget-object v0, p1, LX/7l3;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233220
    iget-object v0, p1, LX/7l3;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1233221
    iget-object v0, p1, LX/7l3;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->d:Ljava/lang/String;

    .line 1233222
    iget-boolean v0, p1, LX/7l3;->e:Z

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->e:Z

    .line 1233223
    iget-object v0, p1, LX/7l3;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    .line 1233224
    iget-object v0, p1, LX/7l3;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->g:Ljava/lang/String;

    .line 1233225
    iget-boolean v0, p1, LX/7l3;->h:Z

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    .line 1233226
    iget-object v0, p1, LX/7l3;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    .line 1233227
    iget-object v0, p1, LX/7l3;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->j:Ljava/lang/String;

    .line 1233228
    iget-object v0, p1, LX/7l3;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->k:Ljava/lang/String;

    .line 1233229
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1233230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    .line 1233232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233233
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1233234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->d:Ljava/lang/String;

    .line 1233235
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->e:Z

    .line 1233236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    .line 1233237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->g:Ljava/lang/String;

    .line 1233238
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    .line 1233239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    .line 1233240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->j:Ljava/lang/String;

    .line 1233241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->k:Ljava/lang/String;

    .line 1233242
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1233243
    const/4 v0, 0x0

    return v0
.end method

.method public final l()LX/7l3;
    .locals 1

    .prologue
    .line 1233244
    new-instance v0, LX/7l3;

    invoke-direct {v0, p0}, LX/7l3;-><init>(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1233245
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233246
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233247
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1233248
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233249
    iget-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1233250
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233251
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233252
    iget-boolean v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1233253
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233254
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233255
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233256
    return-void
.end method
