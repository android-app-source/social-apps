.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1233557
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1233558
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233559
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1233560
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1233561
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1233562
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233563
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1233564
    if-eqz v2, :cond_0

    .line 1233565
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233566
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1233567
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1233568
    if-eqz v2, :cond_4

    .line 1233569
    const-string p0, "birthdate"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233570
    const/4 p2, 0x0

    .line 1233571
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233572
    invoke-virtual {v1, v2, p2, p2}, LX/15i;->a(III)I

    move-result p0

    .line 1233573
    if-eqz p0, :cond_1

    .line 1233574
    const-string v0, "day"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233575
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1233576
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 1233577
    if-eqz p0, :cond_2

    .line 1233578
    const-string v0, "month"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233579
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1233580
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v2, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 1233581
    if-eqz p0, :cond_3

    .line 1233582
    const-string v0, "year"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233583
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1233584
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233585
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233586
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233587
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Serializer;->a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
