.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1233667
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1233668
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233669
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1233670
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1233671
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1233672
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233673
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1233674
    if-eqz v2, :cond_1

    .line 1233675
    const-string p0, "search_results"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233676
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233677
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1233678
    if-eqz p0, :cond_0

    .line 1233679
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1233680
    invoke-static {v1, p0, p1, p2}, LX/7lJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1233681
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233682
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233683
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233684
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel$Serializer;->a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
