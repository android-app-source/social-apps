.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1234050
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1234051
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1234052
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1234053
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1234054
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1234055
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234056
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1234057
    if-eqz v2, :cond_7

    .line 1234058
    const-string v3, "life_event_categories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234059
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234060
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1234061
    if-eqz v3, :cond_0

    .line 1234062
    const-string v4, "custom_life_event"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234063
    invoke-static {v1, v3, p1, p2}, LX/7lH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1234064
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1234065
    if-eqz v3, :cond_1

    .line 1234066
    const-string v4, "life_event_for_empty_state"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234067
    invoke-static {v1, v3, p1, p2}, LX/7lH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1234068
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1234069
    if-eqz v3, :cond_6

    .line 1234070
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234071
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1234072
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 1234073
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1234074
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234075
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1234076
    if-eqz p0, :cond_3

    .line 1234077
    const-string v0, "life_event_types"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234078
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234079
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1234080
    if-eqz v0, :cond_2

    .line 1234081
    const-string v2, "nodes"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234082
    invoke-static {v1, v0, p1, p2}, LX/7lH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1234083
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234084
    :cond_3
    const/4 p0, 0x1

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1234085
    if-eqz p0, :cond_4

    .line 1234086
    const-string v0, "name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234087
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1234088
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234089
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1234090
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1234091
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234092
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234093
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1234094
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Serializer;->a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
