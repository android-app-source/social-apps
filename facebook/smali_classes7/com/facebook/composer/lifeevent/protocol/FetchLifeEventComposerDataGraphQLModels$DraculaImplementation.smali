.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1233868
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1233869
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1233866
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1233867
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1233795
    if-nez p1, :cond_0

    move v0, v1

    .line 1233796
    :goto_0
    return v0

    .line 1233797
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1233798
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1233799
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1233800
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1233801
    invoke-virtual {p0, p1, v6, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1233802
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1233803
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1233804
    invoke-virtual {p3, v5, v2, v1}, LX/186;->a(III)V

    .line 1233805
    invoke-virtual {p3, v6, v3, v1}, LX/186;->a(III)V

    .line 1233806
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1233807
    :sswitch_1
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233808
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233809
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233810
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233811
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1233812
    :sswitch_2
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233813
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233814
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233815
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233816
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1233817
    :sswitch_3
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1233818
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1233819
    const-class v2, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v5, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1233820
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1233821
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1233822
    const v4, -0x3ec3e34a

    const/4 v9, 0x0

    .line 1233823
    if-nez v3, :cond_1

    move v8, v9

    .line 1233824
    :goto_1
    move v3, v8

    .line 1233825
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1233826
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233827
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1233828
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1233829
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233830
    :sswitch_4
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233831
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233832
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233833
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233834
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233835
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1233836
    const v2, 0x5b468419

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1233837
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1233838
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1233839
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1233840
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233841
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1233842
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233843
    :sswitch_6
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233844
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233845
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233846
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233847
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233848
    :sswitch_7
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233849
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233850
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233851
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233852
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233853
    :sswitch_8
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233854
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1233855
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1233856
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1233857
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1233858
    :cond_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result p1

    .line 1233859
    if-nez p1, :cond_2

    const/4 v8, 0x0

    .line 1233860
    :goto_2
    if-ge v9, p1, :cond_3

    .line 1233861
    invoke-virtual {p0, v3, v9}, LX/15i;->q(II)I

    move-result p2

    .line 1233862
    invoke-static {p0, p2, v4, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v8, v9

    .line 1233863
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 1233864
    :cond_2
    new-array v8, p1, [I

    goto :goto_2

    .line 1233865
    :cond_3
    const/4 v9, 0x1

    invoke-virtual {p3, v8, v9}, LX/186;->a([IZ)I

    move-result v8

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x747094e5 -> :sswitch_0
        -0x491dec14 -> :sswitch_8
        -0x40d21504 -> :sswitch_2
        -0x3ec3e34a -> :sswitch_5
        -0x3c67c8be -> :sswitch_3
        -0x2645fc4b -> :sswitch_7
        0x2418470f -> :sswitch_4
        0x4b5b28ef -> :sswitch_1
        0x5b468419 -> :sswitch_6
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1233794
    new-instance v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1233790
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1233791
    if-eqz v0, :cond_0

    .line 1233792
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1233793
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1233785
    if-eqz p0, :cond_0

    .line 1233786
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1233787
    if-eq v0, p0, :cond_0

    .line 1233788
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1233789
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1233755
    sparse-switch p2, :sswitch_data_0

    .line 1233756
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1233757
    :sswitch_0
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233758
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1233759
    :goto_0
    :sswitch_1
    return-void

    .line 1233760
    :sswitch_2
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233761
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1233762
    :sswitch_3
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1233763
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1233764
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233765
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1233766
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1233767
    const v1, -0x3ec3e34a

    .line 1233768
    if-eqz v0, :cond_0

    .line 1233769
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1233770
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1233771
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1233772
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1233773
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1233774
    :cond_0
    goto :goto_0

    .line 1233775
    :sswitch_4
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233776
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1233777
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1233778
    const v1, 0x5b468419

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1233779
    :sswitch_6
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233780
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1233781
    :sswitch_7
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233782
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1233783
    :sswitch_8
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1233784
    invoke-static {v0, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x747094e5 -> :sswitch_1
        -0x491dec14 -> :sswitch_8
        -0x40d21504 -> :sswitch_2
        -0x3ec3e34a -> :sswitch_5
        -0x3c67c8be -> :sswitch_3
        -0x2645fc4b -> :sswitch_7
        0x2418470f -> :sswitch_4
        0x4b5b28ef -> :sswitch_0
        0x5b468419 -> :sswitch_6
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1233749
    if-eqz p1, :cond_0

    .line 1233750
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1233751
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;

    .line 1233752
    if-eq v0, v1, :cond_0

    .line 1233753
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1233754
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1233748
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1233715
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1233716
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1233743
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1233744
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1233745
    :cond_0
    iput-object p1, p0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1233746
    iput p2, p0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->b:I

    .line 1233747
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1233742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1233741
    new-instance v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1233738
    iget v0, p0, LX/1vt;->c:I

    .line 1233739
    move v0, v0

    .line 1233740
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1233735
    iget v0, p0, LX/1vt;->c:I

    .line 1233736
    move v0, v0

    .line 1233737
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1233732
    iget v0, p0, LX/1vt;->b:I

    .line 1233733
    move v0, v0

    .line 1233734
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1233729
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1233730
    move-object v0, v0

    .line 1233731
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1233720
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1233721
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1233722
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1233723
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1233724
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1233725
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1233726
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1233727
    invoke-static {v3, v9, v2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1233728
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1233717
    iget v0, p0, LX/1vt;->c:I

    .line 1233718
    move v0, v0

    .line 1233719
    return v0
.end method
