.class public Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final composerSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public final employerHubId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "employer_hub_id"
    .end annotation
.end field

.field public final endDate:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_date"
    .end annotation
.end field

.field public final iconId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "icon_id"
    .end annotation
.end field

.field public final isCurrent:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_current"
    .end annotation
.end field

.field public final isGraduated:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_graduate"
    .end annotation
.end field

.field public final lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "life_event_type"
    .end annotation
.end field

.field public final photoFbids:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo_fbids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final place:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place"
    .end annotation
.end field

.field public final privacy:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy"
    .end annotation
.end field

.field public final schoolHubId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "school_hub_id"
    .end annotation
.end field

.field public final schoolType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "school_type"
    .end annotation
.end field

.field public final shouldUpdateRelationshipStatus:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_update_relationship_status"
    .end annotation
.end field

.field public final startDate:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_date"
    .end annotation
.end field

.field public final story:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story"
    .end annotation
.end field

.field public final surface:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "surface"
    .end annotation
.end field

.field public final tags:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1233304
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1233305
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1233306
    new-instance v0, LX/7l4;

    invoke-direct {v0}, LX/7l4;-><init>()V

    sput-object v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1233307
    new-instance v0, LX/7l5;

    invoke-direct {v0}, LX/7l5;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;-><init>(LX/7l5;)V

    .line 1233308
    return-void
.end method

.method public constructor <init>(LX/7l5;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1233309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233310
    iget-object v0, p1, LX/7l5;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    .line 1233311
    iget-object v0, p1, LX/7l5;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    .line 1233312
    iget-object v0, p1, LX/7l5;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    .line 1233313
    iget-object v0, p1, LX/7l5;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    .line 1233314
    iget-object v0, p1, LX/7l5;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    .line 1233315
    iget-object v0, p1, LX/7l5;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    .line 1233316
    iget-object v0, p1, LX/7l5;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    .line 1233317
    iget-object v0, p1, LX/7l5;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    .line 1233318
    iget-object v0, p1, LX/7l5;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    .line 1233319
    iget-object v0, p1, LX/7l5;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    .line 1233320
    iget-object v0, p1, LX/7l5;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    .line 1233321
    iget-object v0, p1, LX/7l5;->l:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    .line 1233322
    iget-object v0, p1, LX/7l5;->m:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233323
    iget-boolean v0, p1, LX/7l5;->n:Z

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    .line 1233324
    iget-boolean v0, p1, LX/7l5;->o:Z

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    .line 1233325
    iget-object v0, p1, LX/7l5;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    .line 1233326
    iget-object v0, p1, LX/7l5;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    .line 1233327
    iget-boolean v0, p1, LX/7l5;->r:Z

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    .line 1233328
    iget-object v0, p1, LX/7l5;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    .line 1233329
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1233330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    .line 1233332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    .line 1233333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    .line 1233334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    .line 1233335
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    .line 1233336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    .line 1233337
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    .line 1233338
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    .line 1233339
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    .line 1233340
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    .line 1233341
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    .line 1233342
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    .line 1233343
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233344
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    .line 1233345
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    .line 1233346
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    .line 1233347
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    .line 1233348
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    .line 1233349
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    .line 1233350
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1233351
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1233352
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233353
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233354
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233355
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233356
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233357
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233358
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233359
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233360
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233361
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233362
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1233363
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1233364
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233365
    iget-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1233366
    iget-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1233367
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233368
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233369
    iget-boolean v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1233370
    iget-object v0, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1233371
    return-void
.end method
