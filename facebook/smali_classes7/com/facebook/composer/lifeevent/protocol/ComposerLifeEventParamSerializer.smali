.class public Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1233411
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1233412
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233413
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233414
    if-nez p0, :cond_0

    .line 1233415
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1233416
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1233417
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;->b(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;LX/0nX;LX/0my;)V

    .line 1233418
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1233419
    return-void
.end method

.method private static b(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1233420
    const-string v0, "composer_session_id"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233421
    const-string v0, "user_id"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233422
    const-string v0, "icon_id"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233423
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233424
    const-string v0, "story"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233425
    const-string v0, "start_date"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233426
    const-string v0, "end_date"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233427
    const-string v0, "surface"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233428
    const-string v0, "place"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233429
    const-string v0, "privacy"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233430
    const-string v0, "tags"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1233431
    const-string v0, "photo_fbids"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1233432
    const-string v0, "life_event_type"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1233433
    const-string v0, "should_update_relationship_status"

    iget-boolean v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1233434
    const-string v0, "is_graduate"

    iget-boolean v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1233435
    const-string v0, "school_type"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233436
    const-string v0, "school_hub_id"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233437
    const-string v0, "is_current"

    iget-boolean v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1233438
    const-string v0, "employer_hub_id"

    iget-object v1, p0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233439
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1233440
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParamSerializer;->a(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;LX/0nX;LX/0my;)V

    return-void
.end method
