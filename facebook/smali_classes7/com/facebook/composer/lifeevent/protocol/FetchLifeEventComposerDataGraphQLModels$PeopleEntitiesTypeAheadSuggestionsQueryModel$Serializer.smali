.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1234405
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1234406
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1234407
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1234391
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1234392
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1234393
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234394
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1234395
    if-eqz v2, :cond_1

    .line 1234396
    const-string p0, "search_results"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234397
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234398
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1234399
    if-eqz p0, :cond_0

    .line 1234400
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234401
    invoke-static {v1, p0, p1, p2}, LX/7lJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1234402
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234403
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234404
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1234390
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel$Serializer;->a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
