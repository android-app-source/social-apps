.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1234165
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1234166
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1234182
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1234168
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1234169
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1234170
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234171
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1234172
    if-eqz v2, :cond_1

    .line 1234173
    const-string p0, "search_results"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234174
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1234175
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1234176
    if-eqz p0, :cond_0

    .line 1234177
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1234178
    invoke-static {v1, p0, p1, p2}, LX/7lJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1234179
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234180
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1234181
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1234167
    check-cast p1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel$Serializer;->a(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$HighSchoolEntitiesTypeAheadSuggestionsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
