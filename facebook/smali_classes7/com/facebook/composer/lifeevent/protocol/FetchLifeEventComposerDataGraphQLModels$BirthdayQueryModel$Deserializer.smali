.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1233503
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1233504
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233505
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1233506
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1233507
    const/4 v2, 0x0

    .line 1233508
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1233509
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233510
    :goto_0
    move v1, v2

    .line 1233511
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1233512
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1233513
    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$BirthdayQueryModel;-><init>()V

    .line 1233514
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1233515
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1233516
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1233517
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1233518
    :cond_0
    return-object v1

    .line 1233519
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233520
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1233521
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1233522
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1233523
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1233524
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1233525
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1233526
    :cond_4
    const-string v5, "birthdate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1233527
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1233528
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_f

    .line 1233529
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233530
    :goto_2
    move v1, v4

    .line 1233531
    goto :goto_1

    .line 1233532
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1233533
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1233534
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1233535
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1233536
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 1233537
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1233538
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1233539
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v11, :cond_7

    .line 1233540
    const-string p0, "day"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1233541
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v5

    goto :goto_3

    .line 1233542
    :cond_8
    const-string p0, "month"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1233543
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_3

    .line 1233544
    :cond_9
    const-string p0, "year"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1233545
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v8, v1

    move v1, v5

    goto :goto_3

    .line 1233546
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1233547
    :cond_b
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1233548
    if-eqz v7, :cond_c

    .line 1233549
    invoke-virtual {v0, v4, v10, v4}, LX/186;->a(III)V

    .line 1233550
    :cond_c
    if-eqz v6, :cond_d

    .line 1233551
    invoke-virtual {v0, v5, v9, v4}, LX/186;->a(III)V

    .line 1233552
    :cond_d
    if-eqz v1, :cond_e

    .line 1233553
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8, v4}, LX/186;->a(III)V

    .line 1233554
    :cond_e
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_f
    move v1, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto :goto_3
.end method
