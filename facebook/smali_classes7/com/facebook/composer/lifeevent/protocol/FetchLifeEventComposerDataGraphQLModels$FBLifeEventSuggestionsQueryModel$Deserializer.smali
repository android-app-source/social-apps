.class public final Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1234048
    const-class v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1234049
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1233968
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1233969
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1233970
    const/4 v2, 0x0

    .line 1233971
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1233972
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233973
    :goto_0
    move v1, v2

    .line 1233974
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1233975
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1233976
    new-instance v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    invoke-direct {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;-><init>()V

    .line 1233977
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1233978
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1233979
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1233980
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1233981
    :cond_0
    return-object v1

    .line 1233982
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233983
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1233984
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1233985
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1233986
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1233987
    const-string v4, "life_event_categories"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1233988
    const/4 v3, 0x0

    .line 1233989
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_b

    .line 1233990
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233991
    :goto_2
    move v1, v3

    .line 1233992
    goto :goto_1

    .line 1233993
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1233994
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1233995
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1233996
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1233997
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_a

    .line 1233998
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1233999
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1234000
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 1234001
    const-string v7, "custom_life_event"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1234002
    invoke-static {p1, v0}, LX/7lH;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 1234003
    :cond_7
    const-string v7, "life_event_for_empty_state"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1234004
    invoke-static {p1, v0}, LX/7lH;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_3

    .line 1234005
    :cond_8
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1234006
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1234007
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_9

    .line 1234008
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1234009
    const/4 v7, 0x0

    .line 1234010
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_10

    .line 1234011
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1234012
    :goto_5
    move v6, v7

    .line 1234013
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1234014
    :cond_9
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1234015
    goto :goto_3

    .line 1234016
    :cond_a
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1234017
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1234018
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1234019
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1234020
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_b
    move v1, v3

    move v4, v3

    move v5, v3

    goto/16 :goto_3

    .line 1234021
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1234022
    :cond_d
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 1234023
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1234024
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1234025
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, p0, :cond_d

    if-eqz v9, :cond_d

    .line 1234026
    const-string v10, "life_event_types"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 1234027
    const/4 v9, 0x0

    .line 1234028
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_14

    .line 1234029
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1234030
    :goto_7
    move v8, v9

    .line 1234031
    goto :goto_6

    .line 1234032
    :cond_e
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1234033
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 1234034
    :cond_f
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1234035
    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 1234036
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1234037
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_10
    move v6, v7

    move v8, v7

    goto :goto_6

    .line 1234038
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1234039
    :cond_12
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_13

    .line 1234040
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1234041
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1234042
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_12

    if-eqz v10, :cond_12

    .line 1234043
    const-string p0, "nodes"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 1234044
    invoke-static {p1, v0}, LX/7lH;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_8

    .line 1234045
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1234046
    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1234047
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_7

    :cond_14
    move v8, v9

    goto :goto_8
.end method
