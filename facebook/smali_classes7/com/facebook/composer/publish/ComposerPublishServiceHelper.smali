.class public Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# instance fields
.field public a:LX/7ll;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/7m8;

.field private final d:LX/0lC;

.field private final e:LX/0aG;

.field public final f:LX/1CW;

.field public final g:LX/0gd;

.field public final h:LX/Gvn;

.field private final i:LX/03V;

.field public final j:LX/4nT;

.field private final k:LX/0SI;

.field public final l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/7ly;

.field public final p:LX/0ad;

.field public final q:LX/7m2;

.field public final r:LX/0ie;

.field private final s:LX/3iH;

.field private final t:LX/8iM;

.field public final u:Landroid/content/Context;

.field public v:Landroid/content/Context;

.field public final w:Landroid/app/Application$ActivityLifecycleCallbacks;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7m8;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/1CW;LX/0gd;LX/0lC;LX/Gvn;LX/03V;LX/4nT;LX/0SI;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/0Ot;LX/7ly;LX/0ad;LX/7m2;LX/0ie;LX/3iH;LX/8iM;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/7m8;",
            "LX/0aG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/1CW;",
            "LX/0gd;",
            "LX/0lC;",
            "Lcom/facebook/intent/notifications/INotificationRenderer;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/4nT;",
            "LX/0SI;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;",
            "LX/7ly;",
            "LX/0ad;",
            "LX/7m2;",
            "LX/0ie;",
            "LX/3iH;",
            "LX/8iM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1235943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235944
    new-instance v1, LX/7lk;

    invoke-direct {v1, p0}, LX/7lk;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;)V

    iput-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->w:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 1235945
    new-instance v1, LX/7lm;

    invoke-direct {v1, p0}, LX/7lm;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;)V

    iput-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a:LX/7ll;

    .line 1235946
    iput-object p1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->u:Landroid/content/Context;

    .line 1235947
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->u:Landroid/content/Context;

    iput-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    .line 1235948
    iput-object p2, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    .line 1235949
    iput-object p3, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e:LX/0aG;

    .line 1235950
    iput-object p4, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    .line 1235951
    iput-object p5, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    .line 1235952
    iput-object p6, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    .line 1235953
    iput-object p7, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->d:LX/0lC;

    .line 1235954
    iput-object p8, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->h:LX/Gvn;

    .line 1235955
    iput-object p9, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->i:LX/03V;

    .line 1235956
    iput-object p10, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->j:LX/4nT;

    .line 1235957
    iput-object p11, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->k:LX/0SI;

    .line 1235958
    iput-object p12, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1235959
    iput-object p13, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->m:LX/0Ot;

    .line 1235960
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->n:LX/0Ot;

    .line 1235961
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->o:LX/7ly;

    .line 1235962
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->p:LX/0ad;

    .line 1235963
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->q:LX/7m2;

    .line 1235964
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->r:LX/0ie;

    .line 1235965
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->s:LX/3iH;

    .line 1235966
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->t:LX/8iM;

    .line 1235967
    return-void
.end method

.method private a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)LX/7ln;
    .locals 1

    .prologue
    .line 1235894
    new-instance v0, LX/7ln;

    invoke-direct {v0, p0, p1, p2, p3}, LX/7ln;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .locals 3

    .prologue
    .line 1235968
    sget-object v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    if-nez v0, :cond_1

    .line 1235969
    const-class v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    monitor-enter v1

    .line 1235970
    :try_start_0
    sget-object v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1235971
    if-eqz v2, :cond_0

    .line 1235972
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1235973
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1235974
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1235975
    :cond_1
    sget-object v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    return-object v0

    .line 1235976
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1235977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 1

    .prologue
    .line 1235978
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1235979
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/2Oo;

    if-eqz v0, :cond_2

    .line 1235980
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1235981
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 1235982
    iget-object v3, v1, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    move-object v1, v3

    .line 1235983
    if-eqz v1, :cond_2

    .line 1235984
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1235985
    iget-object v3, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    move-object v0, v3

    .line 1235986
    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1235987
    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1235988
    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1235989
    const-string v1, "allow_user_retry"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "allow_user_retry"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1235990
    const-string v1, "error_user_title"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1235991
    const-string v3, "error_user_msg"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1235992
    new-instance v3, LX/4mr;

    invoke-direct {v3, v1, v0}, LX/4mr;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1235993
    :goto_0
    move-object v1, v0

    .line 1235994
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1235995
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    .line 1235996
    iput-boolean v2, v0, LX/2rc;->f:Z

    .line 1235997
    move-object v2, v0

    .line 1235998
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mr;

    iget-object v0, v0, LX/4mr;->a:Ljava/lang/String;

    .line 1235999
    iput-object v0, v2, LX/2rc;->g:Ljava/lang/String;

    .line 1236000
    move-object v2, v2

    .line 1236001
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mr;

    iget-object v0, v0, LX/4mr;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/2rc;->a(Ljava/lang/String;)LX/2rc;

    move-result-object v0

    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    .line 1236002
    :goto_1
    return-object v0

    .line 1236003
    :cond_0
    const/4 v1, 0x0

    .line 1236004
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f:LX/1CW;

    invoke-virtual {v0, p1, v2, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1236005
    invoke-static {p1}, LX/1CW;->c(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    .line 1236006
    invoke-direct {p0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/http/protocol/ApiErrorResult;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1236007
    iget-object v0, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    move-object v1, v0

    .line 1236008
    iget-object v0, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    move-object v0, v0

    .line 1236009
    :cond_1
    new-instance v3, LX/2rc;

    invoke-direct {v3}, LX/2rc;-><init>()V

    const/4 v4, 0x0

    .line 1236010
    iput-boolean v4, v3, LX/2rc;->a:Z

    .line 1236011
    move-object v3, v3

    .line 1236012
    iput-object v1, v3, LX/2rc;->g:Ljava/lang/String;

    .line 1236013
    move-object v1, v3

    .line 1236014
    invoke-virtual {v1, v0}, LX/2rc;->a(Ljava/lang/String;)LX/2rc;

    move-result-object v0

    const-string v1, "SentryBlock"

    .line 1236015
    iput-object v1, v0, LX/2rc;->c:Ljava/lang/String;

    .line 1236016
    move-object v0, v0

    .line 1236017
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    .line 1236018
    iput v1, v0, LX/2rc;->d:I

    .line 1236019
    move-object v0, v0

    .line 1236020
    iget v1, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v1, v1

    .line 1236021
    iput v1, v0, LX/2rc;->e:I

    .line 1236022
    move-object v0, v0

    .line 1236023
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 2

    .prologue
    .line 1236024
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b(Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1236025
    :goto_0
    return-object v0

    :catch_0
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v1, 0x1

    .line 1236026
    iput-boolean v1, v0, LX/2rc;->a:Z

    .line 1236027
    move-object v0, v0

    .line 1236028
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/7ll;",
            "Lcom/facebook/composer/publish/SentryWarningDialogController$Delegate;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236029
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1236030
    const-string v0, "publishPostParams"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1236031
    const-string v0, "extra_actor_viewer_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1236032
    const-string v0, "overridden_viewer_context"

    const-string v2, "extra_actor_viewer_context"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1236033
    :cond_0
    const-string v0, "extra_optimistic_feed_story"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1236034
    if-nez v6, :cond_1

    iget-boolean v0, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    if-eqz v0, :cond_2

    .line 1236035
    :cond_1
    const-string v0, "suppress_failure_notification"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1236036
    :goto_0
    const-string v2, "publish_post"

    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1236037
    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-direct {v1, p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    invoke-direct {p0, v6, v1, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1236038
    :cond_2
    const-string v0, "suppress_failure_notification"

    const-string v2, "suppress_failure_notification"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;LX/7ln;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/7ll;",
            "Lcom/facebook/composer/publish/SentryWarningDialogController$Delegate;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235930
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1235931
    const-string v0, "publishPostParams"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235932
    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->k:LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p5

    .line 1235933
    :cond_0
    const-string v0, "overridden_viewer_context"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235934
    const-string v0, "extra_optimistic_feed_story"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1235935
    if-eqz v6, :cond_1

    .line 1235936
    const-string v0, "suppress_failure_notification"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1235937
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    .line 1235938
    const-string v2, "publish_share"

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1235939
    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-direct {v1, p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    invoke-direct {p0, v6, v1, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1235940
    :cond_1
    const-string v0, "suppress_failure_notification"

    const-string v2, "suppress_failure_notification"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "LX/7ll;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236039
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p5    # LX/7ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "LX/7ll;",
            "Lcom/facebook/composer/publish/SentryWarningDialogController$Delegate;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236040
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e:LX/0aG;

    const v1, 0x15178e0b

    invoke-static {v0, p2, p1, v1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v10

    .line 1236041
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1236042
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1236043
    if-nez v0, :cond_0

    .line 1236044
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v0, v10

    .line 1236045
    :goto_0
    return-object v0

    .line 1236046
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1236047
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v1

    .line 1236048
    iget-object v3, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getAttemptCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setAttemptCount(I)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v1

    invoke-virtual {v3, v4, v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1236049
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v9

    .line 1236050
    iget-object v0, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    if-ne v0, v1, :cond_2

    const/4 v5, 0x1

    .line 1236051
    :goto_1
    new-instance v0, LX/7lq;

    move-object v1, p0

    move-object/from16 v3, p5

    move-object v4, p1

    move-object v6, p3

    move-object v7, p4

    move-object v8, p2

    invoke-direct/range {v0 .. v9}, LX/7lq;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ln;Landroid/os/Bundle;ZLandroid/content/Intent;LX/7ll;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v10, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move-object v0, v10

    .line 1236052
    goto :goto_0

    .line 1236053
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/composer/publish/common/PostParamsWrapper;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1236054
    if-eqz p1, :cond_0

    .line 1236055
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mW;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1236056
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    invoke-virtual {v0, p2, p1}, LX/7m8;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1236057
    :cond_0
    invoke-direct {p0, p3, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    .line 1236058
    return-object p3
.end method

.method public static synthetic a(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1236059
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1236060
    :try_start_0
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->d:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1236061
    :goto_0
    return-object v0

    .line 1236062
    :catch_0
    move-exception v0

    .line 1236063
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->i:LX/03V;

    const-string v2, "composer_publish_params_json_failed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1236064
    const-string v0, ""

    goto :goto_0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "Lcom/facebook/composer/publish/common/PostParamsWrapper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1236065
    new-instance v0, LX/7lp;

    invoke-direct {v0, p0, p2}, LX/7lp;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1236066
    return-void
.end method

.method private a(Lcom/facebook/http/protocol/ApiErrorResult;)Z
    .locals 2

    .prologue
    .line 1236067
    iget-object v0, p1, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    move-object v0, v0

    .line 1236068
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1236069
    iget-object v0, p1, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    move-object v0, v0

    .line 1236070
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1236071
    iget v0, p1, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v0, v0

    .line 1236072
    const v1, 0x156cd0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->t:LX/8iM;

    invoke-virtual {v0}, LX/8iM;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Ljava/lang/String;Lcom/facebook/composer/publish/common/ErrorDetails;)V
    .locals 4

    .prologue
    .line 1236073
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1236074
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1236075
    :cond_0
    :goto_0
    return-void

    .line 1236076
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setErrorDetails(Lcom/facebook/composer/publish/common/ErrorDetails;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    .line 1236077
    invoke-static {v1, v2, v3, v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1236078
    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .locals 23

    .prologue
    .line 1235941
    new-instance v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/7m8;->a(LX/0QB;)LX/7m8;

    move-result-object v4

    check-cast v4, LX/7m8;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/1CW;->a(LX/0QB;)LX/1CW;

    move-result-object v7

    check-cast v7, LX/1CW;

    invoke-static/range {p0 .. p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v8

    check-cast v8, LX/0gd;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/Gvn;->a(LX/0QB;)LX/Gvn;

    move-result-object v10

    check-cast v10, LX/Gvn;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/4nT;->a(LX/0QB;)LX/4nT;

    move-result-object v12

    check-cast v12, LX/4nT;

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v13

    check-cast v13, LX/0SI;

    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v14

    check-cast v14, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    const/16 v15, 0x19ec

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x19ef

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const-class v17, LX/7ly;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/7ly;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    const-class v19, LX/7m2;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/7m2;

    invoke-static/range {p0 .. p0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v20

    check-cast v20, LX/0ie;

    invoke-static/range {p0 .. p0}, LX/3iH;->a(LX/0QB;)LX/3iH;

    move-result-object v21

    check-cast v21, LX/3iH;

    invoke-static/range {p0 .. p0}, LX/8iM;->a(LX/0QB;)LX/8iM;

    move-result-object v22

    check-cast v22, LX/8iM;

    invoke-direct/range {v2 .. v22}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;-><init>(Landroid/content/Context;LX/7m8;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/1CW;LX/0gd;LX/0lC;LX/Gvn;LX/03V;LX/4nT;LX/0SI;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/0Ot;LX/7ly;LX/0ad;LX/7m2;LX/0ie;LX/3iH;LX/8iM;)V

    .line 1235942
    return-object v2
.end method

.method private b(Lcom/facebook/fbservice/service/ServiceException;Ljava/lang/String;)Lcom/facebook/composer/publish/common/ErrorDetails;
    .locals 10

    .prologue
    .line 1235818
    invoke-static {p1}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1235819
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    .line 1235820
    :goto_0
    return-object v0

    .line 1235821
    :cond_0
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->b(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1235822
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 1235823
    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v0

    .line 1235824
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v2

    .line 1235825
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    .line 1235826
    iget v4, v1, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v4, v4

    .line 1235827
    iget-boolean v5, v1, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    move v1, v5

    .line 1235828
    if-nez v1, :cond_1

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 1235829
    iget-object v5, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v5, v5

    .line 1235830
    iget-object v6, v5, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v5, v6

    .line 1235831
    const-string v6, "originalExceptionMessage"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1235832
    iget-object v6, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->i:LX/03V;

    const-string v7, "composer_publish_partial_error_result"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "session_id: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", exception_msg: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235833
    :cond_1
    new-instance v5, LX/2rc;

    invoke-direct {v5}, LX/2rc;-><init>()V

    .line 1235834
    iput-boolean v1, v5, LX/2rc;->a:Z

    .line 1235835
    move-object v1, v5

    .line 1235836
    invoke-virtual {v1, v0}, LX/2rc;->a(Ljava/lang/String;)LX/2rc;

    move-result-object v0

    .line 1235837
    iput-object v2, v0, LX/2rc;->c:Ljava/lang/String;

    .line 1235838
    move-object v0, v0

    .line 1235839
    iput v3, v0, LX/2rc;->d:I

    .line 1235840
    move-object v0, v0

    .line 1235841
    iput v4, v0, LX/2rc;->e:I

    .line 1235842
    move-object v0, v0

    .line 1235843
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/7ll;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235844
    const-string v0, "simplePhotoUploadParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;

    .line 1235845
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235846
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1235847
    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235848
    const-string v2, "simplePhotoUploadParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235849
    const-string v0, "publish_photo"

    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->f(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1235850
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 1235851
    if-nez v1, :cond_0

    .line 1235852
    :goto_0
    return-void

    .line 1235853
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mW;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1235854
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/7m8;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235855
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1235856
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235857
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->h()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v1

    .line 1235858
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235859
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1235860
    const-string v3, "publishEditPostParamsKey"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235861
    iget-object v3, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e:LX/0aG;

    const-string v4, "publish_edit_post"

    const v5, -0x777f041b

    invoke-static {v3, v4, v2, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 1235862
    new-instance v3, LX/7lr;

    invoke-direct {v3, p0, v1}, LX/7lr;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/publish/common/EditPostParams;)V

    iget-object v4, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1235863
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    new-instance v3, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-direct {v3, v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/EditPostParams;)V

    invoke-direct {p0, v0, v3, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private e(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1235864
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1235865
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "publish func must contains publish post/edit post params key."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1235866
    :cond_2
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1235867
    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {v1, v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1235868
    :goto_0
    const-string v0, "extra_optimistic_feed_story"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1235869
    iget-object v2, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 1235870
    if-nez v0, :cond_3

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1235871
    :cond_3
    invoke-static {}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->newBuilder()Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v2

    .line 1235872
    iget-object v3, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1235873
    invoke-static {v3, v1, v0, v2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1235874
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->c(Ljava/lang/String;)V

    .line 1235875
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1235876
    :cond_4
    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {v1, v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/EditPostParams;)V

    goto :goto_0

    .line 1235877
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    goto :goto_1
.end method

.method private f(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1235878
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235879
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    const-class v3, Lcom/facebook/composer/publish/ComposerPublishService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1235880
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1235881
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->i()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1235882
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235883
    const-string v1, "publishEditPostParamsKey"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1235884
    if-eqz v0, :cond_1

    .line 1235885
    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1235886
    new-instance v1, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-direct {v1, v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    move-object v0, v1

    .line 1235887
    :goto_1
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1235888
    const-string v1, "suppress_failure_notification"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1235889
    invoke-virtual {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1235890
    invoke-direct {p0, v1, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V

    .line 1235891
    return-object v1

    .line 1235892
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1235893
    :cond_1
    new-instance v3, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {v3, v0}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/EditPostParams;)V

    move-object v0, v3

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "LX/7ll;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1235792
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1235793
    const-string v1, "is_uploading_media"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1235794
    invoke-direct {p0, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b(Ljava/lang/String;)V

    .line 1235795
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1235796
    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1235797
    :goto_0
    return-object v0

    .line 1235798
    :cond_0
    const-string v1, "publishEditPostParamsKey"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1235799
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1235800
    invoke-direct {p0, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1235801
    :cond_1
    const-string v1, "publishPostParams"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235802
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235803
    invoke-interface {p2, v2}, LX/7ll;->a(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1235804
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 1235805
    if-eqz v1, :cond_2

    .line 1235806
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v1, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v3, v3, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->publishAttemptInfo:Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    invoke-virtual {v3}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->getAttemptCount()I

    move-result v3

    :goto_1
    move v3, v3

    .line 1235807
    if-lez v3, :cond_2

    .line 1235808
    invoke-interface {p2, v1}, LX/7ll;->a(Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 1235809
    :cond_2
    invoke-direct {p0, p1, v2, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)LX/7ln;

    move-result-object v4

    .line 1235810
    iget-object v1, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v1, :cond_3

    .line 1235811
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    .line 1235812
    invoke-direct/range {v0 .. v5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;LX/7ln;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1235813
    :cond_3
    const-string v1, "simplePhotoUploadParams"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1235814
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1235815
    invoke-direct {p0, p1, v2, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1235816
    :cond_4
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1235817
    invoke-direct {p0, p1, v2, p2, v4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;Lcom/facebook/composer/publish/common/PublishPostParams;LX/7ll;LX/7ln;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235895
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a:LX/7ll;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/composer/protocol/PostReviewParams;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/protocol/PostReviewParams;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235896
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235897
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1235898
    const-string v1, "publishReviewParams"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235899
    if-eqz p2, :cond_0

    .line 1235900
    const-string v1, "overridden_viewer_context"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235901
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e:LX/0aG;

    const-string v2, "publish_review"

    const v3, 0x284897ec

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1235902
    iget-object v1, p1, Lcom/facebook/composer/protocol/PostReviewParams;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1235903
    new-instance v1, LX/7ls;

    invoke-direct {v1, p0, p1}, LX/7ls;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/protocol/PostReviewParams;)V

    iget-object v2, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1235904
    :cond_1
    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1235905
    iput-object p1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    .line 1235906
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->v:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->w:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1235907
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5Ro;)V
    .locals 4

    .prologue
    .line 1235908
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1235909
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1235910
    :cond_0
    :goto_0
    return-void

    .line 1235911
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setRetrySource(LX/5Ro;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v0

    .line 1235912
    invoke-static {v1, v2, v3, v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1235913
    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1235914
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1235915
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->l:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 1235916
    if-nez v1, :cond_0

    .line 1235917
    :goto_0
    return-void

    .line 1235918
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mW;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1235919
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c:LX/7m8;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/7m8;->a(Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method public final c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235920
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a:LX/7ll;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;LX/7ll;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1235921
    const-string v0, "publishLifeEventParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1235922
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235923
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->g:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_PUBLISH_START:LX/0ge;

    iget-object v2, v6, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1235924
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1235925
    const-string v0, "publishLifeEventParams"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1235926
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->e:LX/0aG;

    const-string v1, "publish_life_event"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x784312e2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 1235927
    invoke-direct {p0, v6}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1235928
    new-instance v2, LX/7lo;

    invoke-direct {v2, p0, v6, v1}, LX/7lo;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1235929
    return-object v0
.end method
