.class public Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236778
    new-instance v0, LX/7mG;

    invoke-direct {v0}, LX/7mG;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1236766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236767
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->a:Ljava/lang/String;

    .line 1236768
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->b:Ljava/lang/String;

    .line 1236769
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1236774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236775
    iput-object p1, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->a:Ljava/lang/String;

    .line 1236776
    iput-object p2, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->b:Ljava/lang/String;

    .line 1236777
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1236773
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1236770
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1236771
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1236772
    return-void
.end method
