.class public Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:LX/5Rn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1236746
    new-instance v0, LX/7mF;

    invoke-direct {v0}, LX/7mF;-><init>()V

    sput-object v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1236747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236748
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->a:Ljava/lang/String;

    .line 1236749
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->b:J

    .line 1236750
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->c:Ljava/lang/String;

    .line 1236751
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->d:Ljava/lang/String;

    .line 1236752
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->e:J

    .line 1236753
    const-class v0, LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->f:LX/5Rn;

    .line 1236754
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1236755
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1236756
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1236757
    iget-wide v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1236758
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1236759
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1236760
    iget-wide v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1236761
    iget-object v0, p0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;->f:LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1236762
    return-void
.end method
