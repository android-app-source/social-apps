.class public Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1236164
    const-class v0, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    new-instance v1, Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1236165
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1236166
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1236167
    if-nez p0, :cond_0

    .line 1236168
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1236169
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1236170
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;->b(Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;LX/0nX;LX/0my;)V

    .line 1236171
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1236172
    return-void
.end method

.method private static b(Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1236173
    const-string v0, "error_details"

    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;->mErrorDetails:Lcom/facebook/composer/publish/common/ErrorDetails;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1236174
    const-string v0, "post_params"

    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;->mPostParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1236175
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1236176
    check-cast p1, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;->a(Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;LX/0nX;LX/0my;)V

    return-void
.end method
