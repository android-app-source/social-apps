.class public Lcom/facebook/composer/publish/ComposerPublishServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/26t;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mC;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mD;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mE;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7m9;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7lg;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7lL;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7lf;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mA;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7m9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7lg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7lf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7lL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1235646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235647
    iput-object p1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    .line 1235648
    iput-object p2, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->b:LX/0Ot;

    .line 1235649
    iput-object p3, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->c:LX/0Ot;

    .line 1235650
    iput-object p4, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->d:LX/0Ot;

    .line 1235651
    iput-object p5, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->e:LX/0Ot;

    .line 1235652
    iput-object p6, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->f:LX/0Ot;

    .line 1235653
    iput-object p7, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->h:LX/0Ot;

    .line 1235654
    iput-object p8, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->i:LX/0Ot;

    .line 1235655
    iput-object p9, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->g:LX/0Ot;

    .line 1235656
    iput-object p10, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->j:LX/0Ot;

    .line 1235657
    iput-object p11, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->k:LX/0Ot;

    .line 1235658
    return-void
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1235635
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 1235636
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 1235637
    const-string v3, "graphObjectPosts"

    .line 1235638
    const-string v0, "publishLifeEventParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1235639
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235640
    iput-object v3, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235641
    move-object v0, v0

    .line 1235642
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235643
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-interface {v2, v3, v0}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235644
    invoke-interface {v2, v3}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1235645
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHandler;
    .locals 12

    .prologue
    .line 1235633
    new-instance v0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;

    const/16 v1, 0xb79

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x19cc

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x19cd

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x19ce

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x19ca

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x19c4

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x19c3

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x6b4

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x198b

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x19cb

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xa9c

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1235634
    return-object v0
.end method

.method private b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 1235615
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 1235616
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v6

    .line 1235617
    const-string v2, "graphObjectShares"

    .line 1235618
    const-string v0, "publishPostParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235619
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235620
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235621
    move-object v0, v0

    .line 1235622
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235623
    const-string v7, "fetchShare"

    .line 1235624
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryParams;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "{result="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":$.id}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    sget-object v3, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    const/16 v4, 0x19

    iget-object v5, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->k:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tO;

    invoke-virtual {v5}, LX/0tO;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;IZ)V

    .line 1235625
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235626
    iput-object v7, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235627
    move-object v0, v0

    .line 1235628
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235629
    const-string v0, "publishShare"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v6, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235630
    invoke-interface {v6, v7}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1235631
    iget-object v1, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1235632
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 1235528
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 1235529
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v6

    .line 1235530
    const-string v0, "publishPostParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235531
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    if-eqz v1, :cond_0

    .line 1235532
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->d:Lcom/facebook/composer/publish/common/PollUploadParams;

    invoke-static {v1, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "uploadPoll"

    .line 1235533
    iput-object v2, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 1235534
    move-object v1, v1

    .line 1235535
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v6, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1235536
    new-instance v1, LX/5M9;

    invoke-direct {v1, v0}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1235537
    const-string v0, "{result=uploadPoll:$.id}"

    .line 1235538
    iput-object v0, v1, LX/5M9;->af:Ljava/lang/String;

    .line 1235539
    invoke-virtual {v1}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    move-object v1, v0

    .line 1235540
    :goto_0
    const-string v2, "graphObjectPosts"

    .line 1235541
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235542
    iput-object v2, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235543
    move-object v0, v0

    .line 1235544
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235545
    const-string v7, "fetchPost"

    .line 1235546
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryParams;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "{result="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":$.id}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    sget-object v3, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    const/16 v4, 0x19

    iget-object v5, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->k:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tO;

    invoke-virtual {v5}, LX/0tO;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;IZ)V

    .line 1235547
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235548
    iput-object v7, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235549
    move-object v0, v0

    .line 1235550
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235551
    const-string v0, "publishPost"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v6, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235552
    invoke-interface {v6, v7}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1235553
    iget-object v1, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1235554
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method private d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1235605
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 1235606
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 1235607
    const-string v0, "simplePhotoUploadParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadParams;

    .line 1235608
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "graphObjectPhoto"

    .line 1235609
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235610
    move-object v0, v0

    .line 1235611
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235612
    const-string v0, "publishPhoto"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235613
    const-string v0, "graphObjectPhoto"

    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/protocol/SimplePhotoUploadResult;

    .line 1235614
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 1235585
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 1235586
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v6

    .line 1235587
    const-string v7, "editPost"

    .line 1235588
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1235589
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235590
    iput-object v7, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235591
    move-object v0, v0

    .line 1235592
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235593
    const-string v8, "fetchPost"

    .line 1235594
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryParams;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v3, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->k:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tO;

    invoke-virtual {v5}, LX/0tO;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;IZ)V

    .line 1235595
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235596
    iput-object v8, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235597
    move-object v0, v0

    .line 1235598
    iput-object v7, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 1235599
    move-object v0, v0

    .line 1235600
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v6, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235601
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-interface {v6, v7, v0}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235602
    invoke-interface {v6, v8}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/story/FetchSingleStoryResult;

    .line 1235603
    iget-object v1, v0, Lcom/facebook/api/story/FetchSingleStoryResult;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1235604
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method private f(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1235570
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 1235571
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1235572
    const-string v1, "publishReviewParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1235573
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "post_review"

    .line 1235574
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235575
    move-object v0, v0

    .line 1235576
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235577
    const-string v1, "fetchReview"

    .line 1235578
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 1235579
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 1235580
    move-object v0, v0

    .line 1235581
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 1235582
    const-string v0, "postReviewAndFetchReview"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v2, v0, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1235583
    invoke-interface {v2, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    .line 1235584
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 1235555
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1235556
    const-string v1, "publish_post"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1235557
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1235558
    :goto_0
    return-object v0

    .line 1235559
    :cond_0
    const-string v1, "publish_share"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1235560
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1235561
    :cond_1
    const-string v1, "publish_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1235562
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1235563
    :cond_2
    const-string v1, "publish_edit_post"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1235564
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1235565
    :cond_3
    const-string v1, "publish_review"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1235566
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->f(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1235567
    :cond_4
    const-string v1, "publish_life_event"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1235568
    invoke-direct {p0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHandler;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1235569
    :cond_5
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
