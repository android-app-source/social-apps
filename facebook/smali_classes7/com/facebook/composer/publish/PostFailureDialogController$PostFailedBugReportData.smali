.class public final Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;
.end annotation


# instance fields
.field public final mErrorDetails:Lcom/facebook/composer/publish/common/ErrorDetails;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "error_details"
    .end annotation
.end field

.field public final mPostParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_params"
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1236102
    const-class v0, Lcom/facebook/composer/publish/PostFailureDialogController_PostFailedBugReportDataSerializer;

    return-object v0
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/common/ErrorDetails;Lcom/facebook/composer/publish/common/PostParamsWrapper;)V
    .locals 0

    .prologue
    .line 1236103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236104
    iput-object p1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;->mErrorDetails:Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 1236105
    iput-object p2, p0, Lcom/facebook/composer/publish/PostFailureDialogController$PostFailedBugReportData;->mPostParamsWrapper:Lcom/facebook/composer/publish/common/PostParamsWrapper;

    .line 1236106
    return-void
.end method
