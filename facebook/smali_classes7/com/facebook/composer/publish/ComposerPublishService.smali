.class public Lcom/facebook/composer/publish/ComposerPublishService;
.super LX/1ZN;
.source ""


# instance fields
.field private a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private b:Ljava/util/concurrent/ExecutorService;

.field public c:LX/4nT;

.field private d:LX/0Vt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1235521
    const-string v0, "ComposerPublishService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1235522
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1235519
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/publish/ComposerPublishService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 1235520
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/composer/publish/ComposerPublishService;

    invoke-static {v3}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v3}, LX/4nT;->b(LX/0QB;)LX/4nT;

    move-result-object v2

    check-cast v2, LX/4nT;

    invoke-static {v3}, LX/0Vs;->a(LX/0QB;)LX/0Vs;

    move-result-object v3

    check-cast v3, LX/0Vt;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/composer/publish/ComposerPublishService;->a(Ljava/util/concurrent/ExecutorService;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/4nT;LX/0Vt;)V

    return-void
.end method

.method private a(Ljava/util/concurrent/ExecutorService;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/4nT;LX/0Vt;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1235523
    iput-object p1, p0, Lcom/facebook/composer/publish/ComposerPublishService;->b:Ljava/util/concurrent/ExecutorService;

    .line 1235524
    iput-object p2, p0, Lcom/facebook/composer/publish/ComposerPublishService;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1235525
    iput-object p3, p0, Lcom/facebook/composer/publish/ComposerPublishService;->c:LX/4nT;

    .line 1235526
    iput-object p4, p0, Lcom/facebook/composer/publish/ComposerPublishService;->d:LX/0Vt;

    .line 1235527
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x5232c296

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1235503
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1235504
    const-string v1, "publishPostParams"

    new-instance v3, LX/5M9;

    invoke-direct {v3, v0}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    const/4 v4, 0x0

    .line 1235505
    iput-boolean v4, v3, LX/5M9;->ad:Z

    .line 1235506
    move-object v3, v3

    .line 1235507
    invoke-virtual {v3}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1235508
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishService;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v1, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1235509
    iget-boolean v1, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isBackoutDraft:Z

    if-eqz v1, :cond_0

    .line 1235510
    const v1, 0x7f08149f

    .line 1235511
    :goto_0
    iget-object v4, p0, Lcom/facebook/composer/publish/ComposerPublishService;->c:LX/4nT;

    invoke-virtual {p0, v1}, Lcom/facebook/composer/publish/ComposerPublishService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1235512
    new-instance v1, LX/7lj;

    invoke-direct {v1, p0, v0}, LX/7lj;-><init>(Lcom/facebook/composer/publish/ComposerPublishService;Lcom/facebook/composer/publish/common/PublishPostParams;)V

    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishService;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1235513
    invoke-static {v3}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 1235514
    const v0, -0x4f331c31

    invoke-static {v0, v2}, LX/02F;->d(II)V

    return-void

    .line 1235515
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/publish/ComposerPublishService;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v4, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    .line 1235516
    const v1, 0x7f08146f

    .line 1235517
    iget-object v4, p0, Lcom/facebook/composer/publish/ComposerPublishService;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iget-object v5, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    sget-object v6, LX/5Ro;->NOTIFICATION:LX/5Ro;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Ljava/lang/String;LX/5Ro;)V

    goto :goto_0

    .line 1235518
    :cond_1
    const v1, 0x7f081412

    goto :goto_0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 1235502
    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishService;->d:LX/0Vt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/publish/ComposerPublishService;->d:LX/0Vt;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/1ZN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x3b28f850

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1235499
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1235500
    invoke-static {p0, p0}, Lcom/facebook/composer/publish/ComposerPublishService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1235501
    const/16 v1, 0x25

    const v2, -0x2554932d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
