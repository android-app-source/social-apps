.class public final Lcom/facebook/composer/publish/PostFailureDialogController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/composer/publish/common/PendingStory;

.field public final synthetic d:LX/7lx;


# direct methods
.method public constructor <init>(LX/7lx;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 1236079
    iput-object p1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iput-object p2, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->c:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1236080
    new-instance v0, LX/31Y;

    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v1, v1, LX/7lx;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    .line 1236081
    iget-object v1, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v1, v1, LX/7lx;->j:LX/C9S;

    if-eqz v1, :cond_0

    .line 1236082
    const v1, 0x7f0810ab

    iget-object v2, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v3, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v3, v3, LX/7lx;->g:Ljava/lang/String;

    .line 1236083
    new-instance v4, LX/7lu;

    invoke-direct {v4, v2, v3}, LX/7lu;-><init>(LX/7lx;Ljava/lang/String;)V

    move-object v2, v4

    .line 1236084
    invoke-virtual {v0, v1, v2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0810aa

    iget-object v3, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v4, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->c:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-static {v3, v4}, LX/7lx;->a$redex0(LX/7lx;Lcom/facebook/composer/publish/common/PendingStory;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1236085
    :goto_0
    const v1, 0x7f0810a9

    .line 1236086
    new-instance v2, LX/7lv;

    invoke-direct {v2}, LX/7lv;-><init>()V

    move-object v2, v2

    .line 1236087
    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1236088
    return-void

    .line 1236089
    :cond_0
    const v1, 0x7f0810aa

    iget-object v2, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->d:LX/7lx;

    iget-object v3, p0, Lcom/facebook/composer/publish/PostFailureDialogController$1;->c:Lcom/facebook/composer/publish/common/PendingStory;

    invoke-static {v2, v3}, LX/7lx;->a$redex0(LX/7lx;Lcom/facebook/composer/publish/common/PendingStory;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_0
.end method
