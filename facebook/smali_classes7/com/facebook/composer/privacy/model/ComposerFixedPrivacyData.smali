.class public Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1234808
    new-instance v0, LX/7lM;

    invoke-direct {v0}, LX/7lM;-><init>()V

    sput-object v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7lN;)V
    .locals 1

    .prologue
    .line 1234809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234810
    iget-object v0, p1, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1234811
    iget-object v0, p1, LX/7lN;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    .line 1234812
    iget-object v0, p1, LX/7lN;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    .line 1234813
    iget-object v0, p1, LX/7lN;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    .line 1234814
    return-void

    .line 1234815
    :cond_0
    iget-object v0, p1, LX/7lN;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1234816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234817
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1234818
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    .line 1234819
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    .line 1234820
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    .line 1234821
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1234822
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1234823
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1234824
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1234825
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1234826
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1234827
    return-void

    .line 1234828
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
