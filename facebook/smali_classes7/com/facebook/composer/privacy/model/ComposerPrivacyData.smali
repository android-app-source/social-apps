.class public final Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

.field public final b:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1234884
    new-instance v0, LX/7lO;

    invoke-direct {v0}, LX/7lO;-><init>()V

    sput-object v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7lP;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1234885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234886
    iget-boolean v0, p1, LX/7lP;->a:Z

    if-nez v0, :cond_0

    .line 1234887
    iget-object v0, p1, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    const-string v3, "If the privacy is disabled, we should not have fixed privacy data."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234888
    iget-object v0, p1, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "If the privacy is disabled, we should not have selectable privacy data."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234889
    :cond_0
    iget-object v0, p1, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    .line 1234890
    iget-object v0, p1, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    const-string v3, "If we have selectable privacy data, we should not have fixed privacy data."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234891
    :cond_1
    iget-object v0, p1, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_2

    .line 1234892
    iget-object v0, p1, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_6

    :goto_3
    const-string v0, "If we have fixed privacy data, we should not have selectable privacy data."

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1234893
    :cond_2
    iget-object v0, p1, LX/7lP;->g:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1234894
    iget-boolean v0, p1, LX/7lP;->a:Z

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    .line 1234895
    iget-boolean v0, p1, LX/7lP;->b:Z

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    .line 1234896
    iget-boolean v0, p1, LX/7lP;->c:Z

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    .line 1234897
    iget-object v0, p1, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->f:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1234898
    iget-object v0, p1, LX/7lP;->f:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234899
    iget-object v0, p1, LX/7lP;->e:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234900
    iget-object v0, p1, LX/7lP;->g:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    .line 1234901
    iget-boolean v0, p1, LX/7lP;->h:Z

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    .line 1234902
    return-void

    :cond_3
    move v0, v2

    .line 1234903
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1234904
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1234905
    goto :goto_2

    :cond_6
    move v1, v2

    .line 1234906
    goto :goto_3
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1234907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234908
    const-class v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1234909
    const-class v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1234910
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    .line 1234911
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    .line 1234912
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    .line 1234913
    const-class v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->f:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1234914
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    .line 1234915
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    .line 1234916
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1234917
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_0

    .line 1234918
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    .line 1234919
    :goto_0
    return-object v0

    .line 1234920
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    .line 1234921
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1234922
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1234923
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1234924
    const-class v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "isEnabled"

    iget-boolean v2, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isLoading"

    iget-boolean v2, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v1

    const-string v2, "isSelectable"

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1234925
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1234926
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1234927
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1234928
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1234929
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1234930
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->f:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1234931
    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1234932
    iget-boolean v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1234933
    return-void
.end method
