.class public Lcom/facebook/platform/params/PlatformComposerParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/params/PlatformComposerParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341757
    new-instance v0, LX/8PQ;

    invoke-direct {v0}, LX/8PQ;-><init>()V

    sput-object v0, Lcom/facebook/platform/params/PlatformComposerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1341727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341728
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->a:Ljava/lang/String;

    .line 1341729
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->b:Z

    .line 1341730
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/platform/params/PlatformComposerParams;->c:Z

    .line 1341731
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->d:Ljava/lang/String;

    .line 1341732
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->e:Ljava/lang/String;

    .line 1341733
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->f:Ljava/lang/String;

    .line 1341734
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->g:Ljava/lang/String;

    .line 1341735
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->h:Ljava/lang/String;

    .line 1341736
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1341737
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1341738
    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->i:Ljava/util/Set;

    .line 1341739
    return-void

    :cond_0
    move v0, v2

    .line 1341740
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1341741
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341759
    iput-object p1, p0, Lcom/facebook/platform/params/PlatformComposerParams;->a:Ljava/lang/String;

    .line 1341760
    iput-boolean p2, p0, Lcom/facebook/platform/params/PlatformComposerParams;->b:Z

    .line 1341761
    iput-boolean p3, p0, Lcom/facebook/platform/params/PlatformComposerParams;->c:Z

    .line 1341762
    iput-object p4, p0, Lcom/facebook/platform/params/PlatformComposerParams;->d:Ljava/lang/String;

    .line 1341763
    iput-object p5, p0, Lcom/facebook/platform/params/PlatformComposerParams;->e:Ljava/lang/String;

    .line 1341764
    iput-object p6, p0, Lcom/facebook/platform/params/PlatformComposerParams;->f:Ljava/lang/String;

    .line 1341765
    iput-object p7, p0, Lcom/facebook/platform/params/PlatformComposerParams;->g:Ljava/lang/String;

    .line 1341766
    iput-object p8, p0, Lcom/facebook/platform/params/PlatformComposerParams;->h:Ljava/lang/String;

    .line 1341767
    iput-object p9, p0, Lcom/facebook/platform/params/PlatformComposerParams;->i:Ljava/util/Set;

    .line 1341768
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341756
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1341742
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341743
    iget-boolean v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1341744
    iget-boolean v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->c:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1341745
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341746
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341747
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341748
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341749
    iget-object v0, p0, Lcom/facebook/platform/params/PlatformComposerParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1341751
    iget-object v1, p0, Lcom/facebook/platform/params/PlatformComposerParams;->i:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1341752
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1341753
    return-void

    :cond_0
    move v0, v2

    .line 1341754
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1341755
    goto :goto_1
.end method
