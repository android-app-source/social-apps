.class public final Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341792
    new-instance v0, LX/8PS;

    invoke-direct {v0}, LX/8PS;-><init>()V

    sput-object v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341794
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;->a:Ljava/lang/String;

    .line 1341795
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1341796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341797
    iput-object p1, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;->a:Ljava/lang/String;

    .line 1341798
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341799
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341800
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341801
    return-void
.end method
