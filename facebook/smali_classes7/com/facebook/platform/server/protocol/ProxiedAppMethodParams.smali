.class public Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341604
    new-instance v0, LX/8PV;

    invoke-direct {v0}, LX/8PV;-><init>()V

    sput-object v0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341621
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a:Ljava/lang/String;

    .line 1341622
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->b:Ljava/lang/String;

    .line 1341623
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->c:Ljava/lang/String;

    .line 1341624
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1341610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341611
    iput-object p1, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a:Ljava/lang/String;

    .line 1341612
    iput-object p2, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->b:Ljava/lang/String;

    .line 1341613
    iput-object p3, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->c:Ljava/lang/String;

    .line 1341614
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341615
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_id"

    iget-object v2, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1341616
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1341617
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_name"

    iget-object v2, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1341618
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "android_key_hash"

    iget-object v2, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1341619
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1341609
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341605
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341606
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341607
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/ProxiedAppMethodParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341608
    return-void
.end method
