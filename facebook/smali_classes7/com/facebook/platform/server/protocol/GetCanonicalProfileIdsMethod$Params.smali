.class public final Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341825
    new-instance v0, LX/8PU;

    invoke-direct {v0}, LX/8PU;-><init>()V

    sput-object v0, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341827
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->a:Ljava/util/ArrayList;

    .line 1341828
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341830
    iput-object p1, p0, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->a:Ljava/util/ArrayList;

    .line 1341831
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341832
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341833
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1341834
    return-void
.end method
