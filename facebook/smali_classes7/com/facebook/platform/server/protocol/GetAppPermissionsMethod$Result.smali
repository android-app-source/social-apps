.class public final Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341820
    new-instance v0, LX/8PT;

    invoke-direct {v0}, LX/8PT;-><init>()V

    sput-object v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1341805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341806
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->a:Z

    .line 1341807
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->b:Ljava/util/List;

    .line 1341808
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1341809
    return-void

    .line 1341810
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341812
    iput-boolean p1, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->a:Z

    .line 1341813
    iput-object p2, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->b:Ljava/util/List;

    .line 1341814
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341815
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341816
    iget-boolean v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1341817
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1341818
    return-void

    .line 1341819
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
