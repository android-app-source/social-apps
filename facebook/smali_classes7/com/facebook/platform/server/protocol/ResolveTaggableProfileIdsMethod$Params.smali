.class public final Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341842
    new-instance v0, LX/8PW;

    invoke-direct {v0}, LX/8PW;-><init>()V

    sput-object v0, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341844
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->a:Ljava/util/List;

    .line 1341845
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341847
    iput-object p1, p0, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->a:Ljava/util/List;

    .line 1341848
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341849
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341850
    iget-object v0, p0, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1341851
    return-void
.end method
