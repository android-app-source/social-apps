.class public final Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x17771df5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1169711
    const-class v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1169710
    const-class v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1169708
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1169709
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1169701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1169702
    invoke-virtual {p0}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1169703
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1169704
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1169705
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1169706
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1169707
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1169699
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1169700
    iget-wide v0, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->e:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1169712
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1169713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1169714
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1169696
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1169697
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->e:J

    .line 1169698
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1169693
    new-instance v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;

    invoke-direct {v0}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;-><init>()V

    .line 1169694
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1169695
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1169692
    const v0, -0x514b72ea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1169691
    const v0, 0x39baf9ad

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169689
    iget-object v0, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->f:Ljava/lang/String;

    .line 1169690
    iget-object v0, p0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method
