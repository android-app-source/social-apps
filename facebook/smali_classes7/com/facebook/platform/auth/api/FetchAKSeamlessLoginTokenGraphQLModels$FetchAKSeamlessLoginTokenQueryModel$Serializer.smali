.class public final Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1169672
    const-class v0, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;

    new-instance v1, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1169673
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1169674
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1169675
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1169676
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1169677
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1169678
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1169679
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1169680
    const-string v4, "expiration_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1169681
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1169682
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1169683
    if-eqz v2, :cond_1

    .line 1169684
    const-string v3, "token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1169685
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1169686
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1169687
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1169688
    check-cast p1, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel$Serializer;->a(Lcom/facebook/platform/auth/api/FetchAKSeamlessLoginTokenGraphQLModels$FetchAKSeamlessLoginTokenQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
