.class public Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;
.super LX/4hr;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hr",
        "<",
        "LX/75k;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/0aG;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169885
    const-class v0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0aG;LX/0Or;LX/0TD;)V
    .locals 1
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/75k;",
            ">;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169886
    const v0, 0x10008

    invoke-direct {p0, p1, v0}, LX/4hr;-><init>(LX/0Or;I)V

    .line 1169887
    iput-object p2, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->c:LX/0aG;

    .line 1169888
    iput-object p3, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->d:LX/0Or;

    .line 1169889
    iput-object p4, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->e:LX/0TD;

    .line 1169890
    return-void
.end method

.method public static b(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 1169891
    :try_start_0
    iget-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v0, p0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1169892
    :goto_0
    return-void

    .line 1169893
    :catch_0
    move-exception v0

    .line 1169894
    sget-object v1, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->b:Ljava/lang/String;

    const-string v2, "Unable to respond to seamless login token request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Message;LX/4ht;)V
    .locals 7

    .prologue
    .line 1169895
    check-cast p2, LX/75k;

    .line 1169896
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-nez v0, :cond_0

    .line 1169897
    :goto_0
    return-void

    .line 1169898
    :cond_0
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 1169899
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1169900
    iget v1, p1, Landroid/os/Message;->arg2:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 1169901
    const v1, 0x10009

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1169902
    move-object v6, v0

    .line 1169903
    iget-object v0, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1169904
    if-eqz v0, :cond_1

    .line 1169905
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1169906
    if-nez v0, :cond_2

    .line 1169907
    :cond_1
    const/4 v0, 0x0

    new-instance v1, Lcom/facebook/fbservice/service/ServiceException;

    sget-object v2, LX/1nY;->HTTP_400_AUTHENTICATION:LX/1nY;

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-static {v0, v1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1169908
    invoke-static {v6}, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->b(Landroid/os/Message;)V

    goto :goto_0

    .line 1169909
    :cond_2
    iget-object v0, p2, LX/4ht;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1169910
    iget-object v1, p2, LX/4ht;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1169911
    new-instance v3, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;

    invoke-direct {v3, v0, v1}, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169912
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1169913
    const-string v0, "ak_seamless_login_param"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1169914
    iget-object v0, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->c:LX/0aG;

    const-string v1, "ak_seamless_login"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x3e0c9049

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1169915
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1169916
    new-instance v1, LX/75j;

    invoke-direct {v1, p0, v6}, LX/75j;-><init>(Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;Landroid/os/Message;)V

    iget-object v2, p0, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;->e:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
