.class public final Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169751
    new-instance v0, LX/75c;

    invoke-direct {v0}, LX/75c;-><init>()V

    sput-object v0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1169747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169748
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->a:Ljava/lang/String;

    .line 1169749
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->b:Ljava/lang/String;

    .line 1169750
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1169752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169753
    iput-object p1, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->a:Ljava/lang/String;

    .line 1169754
    iput-object p2, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->b:Ljava/lang/String;

    .line 1169755
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169746
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1169743
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169744
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AKSeamlessLoginMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169745
    return-void
.end method
