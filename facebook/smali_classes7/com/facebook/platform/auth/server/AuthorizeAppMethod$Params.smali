.class public final Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169804
    new-instance v0, LX/75e;

    invoke-direct {v0}, LX/75e;-><init>()V

    sput-object v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1169797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169798
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->a:Ljava/lang/String;

    .line 1169799
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->b:Ljava/lang/String;

    .line 1169800
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->c:LX/0am;

    .line 1169801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->d:LX/0am;

    .line 1169802
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->e:LX/0am;

    .line 1169803
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0am;LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1169790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169791
    iput-object p1, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->a:Ljava/lang/String;

    .line 1169792
    iput-object p2, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->b:Ljava/lang/String;

    .line 1169793
    iput-object p3, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->c:LX/0am;

    .line 1169794
    iput-object p4, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->d:LX/0am;

    .line 1169795
    iput-object p5, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->e:LX/0am;

    .line 1169796
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0am;LX/0am;LX/0am;B)V
    .locals 0

    .prologue
    .line 1169782
    invoke-direct/range {p0 .. p5}, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0am;LX/0am;LX/0am;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169789
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1169783
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169784
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169785
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169786
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169787
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1169788
    return-void
.end method
