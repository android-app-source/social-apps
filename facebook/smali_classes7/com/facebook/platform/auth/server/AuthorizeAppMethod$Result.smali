.class public final Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169824
    new-instance v0, LX/75g;

    invoke-direct {v0}, LX/75g;-><init>()V

    sput-object v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1169818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169819
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->a:Ljava/lang/String;

    .line 1169820
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->b:J

    .line 1169821
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->c:Ljava/util/List;

    .line 1169822
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1169823
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1169808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169809
    iput-object p1, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->a:Ljava/lang/String;

    .line 1169810
    iput-wide p2, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->b:J

    .line 1169811
    iput-object p4, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->c:Ljava/util/List;

    .line 1169812
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169817
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1169813
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169814
    iget-wide v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1169815
    iget-object v0, p0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1169816
    return-void
.end method
