.class public final Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1169844
    new-instance v0, LX/75i;

    invoke-direct {v0}, LX/75i;-><init>()V

    sput-object v0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1169845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169846
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->a:Ljava/lang/String;

    .line 1169847
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->b:J

    .line 1169848
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1169849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169850
    iput-object p1, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->a:Ljava/lang/String;

    .line 1169851
    iput-wide p2, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->b:J

    .line 1169852
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1169853
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1169854
    iget-object v0, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1169855
    iget-wide v0, p0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1169856
    return-void
.end method
