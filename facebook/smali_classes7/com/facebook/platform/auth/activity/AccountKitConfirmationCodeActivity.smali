.class public Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1169641
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1169642
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1169643
    const v0, 0x7f0300c6

    invoke-virtual {p0, v0}, Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;->setContentView(I)V

    .line 1169644
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1169645
    invoke-virtual {p0}, Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "confirmation_code"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1169646
    const v0, 0x7f0d0509

    invoke-virtual {p0, v0}, Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1169647
    const v0, 0x7f0d050a

    invoke-virtual {p0, v0}, Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/75W;

    invoke-direct {v2, p0, v1}, LX/75W;-><init>(Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1169648
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1169649
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1169650
    const v0, 0x7f0400b7

    const v1, 0x7f0400ed

    invoke-virtual {p0, v0, v1}, Lcom/facebook/platform/auth/activity/AccountKitConfirmationCodeActivity;->overridePendingTransition(II)V

    .line 1169651
    return-void
.end method
