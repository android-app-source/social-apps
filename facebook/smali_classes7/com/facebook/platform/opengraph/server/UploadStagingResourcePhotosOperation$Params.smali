.class public final Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341714
    new-instance v0, LX/8PP;

    invoke-direct {v0}, LX/8PP;-><init>()V

    sput-object v0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1341715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341716
    iput-object p1, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;->a:LX/0P1;

    .line 1341717
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341719
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;->a:LX/0P1;

    .line 1341720
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341721
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341722
    iget-object v0, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;->a:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1341723
    return-void
.end method
