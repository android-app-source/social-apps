.class public final Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0m9;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341692
    new-instance v0, LX/8PO;

    invoke-direct {v0}, LX/8PO;-><init>()V

    sput-object v0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1341693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->a:Ljava/lang/String;

    .line 1341695
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->b:LX/0m9;

    .line 1341696
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->c:Ljava/lang/String;

    .line 1341697
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0m9;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-<init>"
        }
    .end annotation

    .prologue
    .line 1341698
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;-><init>(Ljava/lang/String;LX/0m9;Ljava/lang/String;)V

    .line 1341699
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/0m9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1341700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341701
    iput-object p1, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->a:Ljava/lang/String;

    .line 1341702
    iput-object p2, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->b:LX/0m9;

    .line 1341703
    iput-object p3, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->c:Ljava/lang/String;

    .line 1341704
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341705
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341706
    iget-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341707
    iget-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->b:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341708
    iget-object v0, p0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341709
    return-void
.end method
