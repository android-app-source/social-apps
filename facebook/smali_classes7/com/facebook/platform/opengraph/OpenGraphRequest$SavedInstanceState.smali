.class public final Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1341343
    new-instance v0, LX/8PE;

    invoke-direct {v0}, LX/8PE;-><init>()V

    sput-object v0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8PF;)V
    .locals 1

    .prologue
    .line 1341349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341350
    iget-object v0, p1, LX/8PF;->c:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->a:Ljava/lang/String;

    .line 1341351
    iget-object v0, p1, LX/8PF;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->b:Ljava/lang/String;

    .line 1341352
    iget-object v0, p1, LX/8PF;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->c:Ljava/lang/String;

    .line 1341353
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1341354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->a:Ljava/lang/String;

    .line 1341356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->b:Ljava/lang/String;

    .line 1341357
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->c:Ljava/lang/String;

    .line 1341358
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1341348
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1341344
    iget-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341345
    iget-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341346
    iget-object v0, p0, Lcom/facebook/platform/opengraph/OpenGraphRequest$SavedInstanceState;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1341347
    return-void
.end method
