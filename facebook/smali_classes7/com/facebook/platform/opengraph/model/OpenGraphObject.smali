.class public Lcom/facebook/platform/opengraph/model/OpenGraphObject;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/opengraph/model/OpenGraphObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1170182
    new-instance v0, LX/75t;

    invoke-direct {v0}, LX/75t;-><init>()V

    sput-object v0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1170183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->b:Ljava/lang/String;

    .line 1170185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->a:Ljava/lang/String;

    .line 1170186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->c:Ljava/lang/String;

    .line 1170187
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1170188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170189
    iput-object p1, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->b:Ljava/lang/String;

    .line 1170190
    iput-object p2, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->a:Ljava/lang/String;

    .line 1170191
    iput-object p3, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->c:Ljava/lang/String;

    .line 1170192
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1170193
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1170194
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1170195
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1170196
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphObject;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1170197
    return-void
.end method
