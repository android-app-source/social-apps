.class public Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1170081
    new-instance v0, LX/75r;

    invoke-direct {v0}, LX/75r;-><init>()V

    sput-object v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1170082
    invoke-direct {p0, v0, v0}, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 1170083
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1170084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170085
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->a:Ljava/lang/String;

    .line 1170086
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    .line 1170087
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    const-class v1, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1170088
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1170089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170090
    iput-object p1, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->a:Ljava/lang/String;

    .line 1170091
    iput-object p2, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    .line 1170092
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1170093
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1170094
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1170095
    iget-object v0, p0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1170096
    return-void
.end method
