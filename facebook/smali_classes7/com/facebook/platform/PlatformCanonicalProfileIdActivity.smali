.class public Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/17Y;

.field private q:LX/0aG;

.field public r:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1341201
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1341202
    invoke-static {p1}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/17Y;LX/0aG;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1341203
    iput-object p1, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->p:LX/17Y;

    .line 1341204
    iput-object p2, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->q:LX/0aG;

    .line 1341205
    iput-object p3, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->r:Lcom/facebook/content/SecureContextHelper;

    .line 1341206
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;

    invoke-static {v2}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {v2}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->a(LX/17Y;LX/0aG;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1341207
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1341208
    new-instance v0, LX/8Os;

    invoke-direct {v0, p0, p1}, LX/8Os;-><init>(Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;Ljava/lang/String;)V

    .line 1341209
    new-instance v1, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;

    invoke-direct {v1, v0}, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;-><init>(Ljava/util/ArrayList;)V

    .line 1341210
    const-string v0, "app_scoped_ids"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1341211
    iget-object v0, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->q:LX/0aG;

    const-string v1, "platform_get_canonical_profile_ids"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const/4 v4, 0x0

    const v5, 0x5c7606c9

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1341212
    new-instance v1, LX/8Ot;

    invoke-direct {v1, p0}, LX/8Ot;-><init>(Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1341213
    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1341214
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1341215
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1341216
    invoke-static {p0, p0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1341217
    invoke-virtual {p0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1341218
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1341219
    const-string v2, "com.facebook.katana.profile.type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1341220
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "app_scoped_user"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341221
    invoke-direct {p0, v1}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->b(Ljava/lang/String;)V

    .line 1341222
    :goto_0
    return-void

    .line 1341223
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->p:LX/17Y;

    invoke-static {v1}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1341224
    iget-object v1, p0, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1341225
    invoke-virtual {p0}, Lcom/facebook/platform/PlatformCanonicalProfileIdActivity;->finish()V

    goto :goto_0
.end method
