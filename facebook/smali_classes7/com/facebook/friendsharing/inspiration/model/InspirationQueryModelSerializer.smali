.class public Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299145
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1299146
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299147
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299148
    if-nez p0, :cond_0

    .line 1299149
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1299150
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1299151
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0nX;LX/0my;)V

    .line 1299152
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1299153
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1299154
    const-string v0, "default_inspiration_landing_index"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getDefaultInspirationLandingIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299155
    const-string v0, "inspiration_models"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1299156
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299157
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
