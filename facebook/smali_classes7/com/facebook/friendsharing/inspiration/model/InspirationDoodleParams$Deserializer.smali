.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298158
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1298153
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1298154
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
    .locals 1

    .prologue
    .line 1298156
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    .line 1298157
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1298155
    invoke-static {p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    return-object v0
.end method
