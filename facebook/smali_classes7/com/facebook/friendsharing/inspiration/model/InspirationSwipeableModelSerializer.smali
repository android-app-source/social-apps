.class public Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299605
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1299606
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299607
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299608
    if-nez p0, :cond_0

    .line 1299609
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1299610
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1299611
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;LX/0nX;LX/0my;)V

    .line 1299612
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1299613
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1299614
    const-string v0, "are_effects_loading"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->areEffectsLoading()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299615
    const-string v0, "inspiration_models"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1299616
    const-string v0, "is_from_tray"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->isFromTray()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299617
    const-string v0, "last_selected_pre_capture_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299618
    const-string v0, "selected_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299619
    const-string v0, "star_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299620
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299621
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;LX/0nX;LX/0my;)V

    return-void
.end method
