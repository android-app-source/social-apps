.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299001
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1299002
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1299003
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;
    .locals 1

    .prologue
    .line 1299004
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    .line 1299005
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1299006
    invoke-static {p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    move-result-object v0

    return-object v0
.end method
