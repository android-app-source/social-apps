.class public Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298944
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298943
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298942
    new-instance v0, LX/879;

    invoke-direct {v0}, LX/879;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1298936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298937
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    .line 1298938
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    .line 1298939
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    .line 1298940
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    .line 1298941
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;)V
    .locals 1

    .prologue
    .line 1298930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298931
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    .line 1298932
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    .line 1298933
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    .line 1298934
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    .line 1298935
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
    .locals 2

    .prologue
    .line 1298929
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1298928
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298915
    if-ne p0, p1, :cond_1

    .line 1298916
    :cond_0
    :goto_0
    return v0

    .line 1298917
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    if-nez v2, :cond_2

    move v0, v1

    .line 1298918
    goto :goto_0

    .line 1298919
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 1298920
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1298921
    goto :goto_0

    .line 1298922
    :cond_3
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1298923
    goto :goto_0

    .line 1298924
    :cond_4
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1298925
    goto :goto_0

    .line 1298926
    :cond_5
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1298927
    goto :goto_0
.end method

.method public getBottom()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bottom"
    .end annotation

    .prologue
    .line 1298914
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    return v0
.end method

.method public getLeft()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left"
    .end annotation

    .prologue
    .line 1298905
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    return v0
.end method

.method public getRight()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "right"
    .end annotation

    .prologue
    .line 1298913
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    return v0
.end method

.method public getTop()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top"
    .end annotation

    .prologue
    .line 1298912
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1298911
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1298906
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298907
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298908
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298909
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298910
    return-void
.end method
