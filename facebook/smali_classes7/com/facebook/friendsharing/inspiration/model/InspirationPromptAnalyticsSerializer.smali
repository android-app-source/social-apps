.class public Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299041
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1299042
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299043
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299044
    if-nez p0, :cond_0

    .line 1299045
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1299046
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1299047
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;LX/0nX;LX/0my;)V

    .line 1299048
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1299049
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1299050
    const-string v0, "prompt_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->getPromptId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299051
    const-string v0, "prompt_tracking_string"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->getPromptTrackingString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299052
    const-string v0, "prompt_type"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->getPromptType()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299053
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299054
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;LX/0nX;LX/0my;)V

    return-void
.end method
