.class public Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298217
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1298218
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298219
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298220
    if-nez p0, :cond_0

    .line 1298221
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1298222
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1298223
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;LX/0nX;LX/0my;)V

    .line 1298224
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1298225
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1298226
    const-string v0, "color_selection"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298227
    const-string v0, "drawing_mode"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getDrawingMode()LX/86w;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298228
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298229
    const-string v0, "media_rect"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298230
    const-string v0, "stroke_width"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getStrokeWidth()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1298231
    const-string v0, "uri"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298232
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298233
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;LX/0nX;LX/0my;)V

    return-void
.end method
