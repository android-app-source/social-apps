.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:I

.field public b:F

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298014
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298013
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
    .locals 2

    .prologue
    .line 1298011
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;)V

    return-object v0
.end method

.method public setDoodleColorCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_color_count"
    .end annotation

    .prologue
    .line 1298009
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->a:I

    .line 1298010
    return-object p0
.end method

.method public setDoodleMaxBrushSize(F)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_max_brush_size"
    .end annotation

    .prologue
    .line 1298007
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->b:F

    .line 1298008
    return-object p0
.end method

.method public setDoodleSizeCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_size_count"
    .end annotation

    .prologue
    .line 1298005
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->c:I

    .line 1298006
    return-object p0
.end method

.method public setDoodleStrokeCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_stroke_count"
    .end annotation

    .prologue
    .line 1298003
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->d:I

    .line 1298004
    return-object p0
.end method

.method public setDoodleUndoCount(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_undo_count"
    .end annotation

    .prologue
    .line 1298001
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->e:I

    .line 1298002
    return-object p0
.end method
