.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/875;


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:J

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:J

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:J

.field public m:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:LX/874;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field public q:LX/875;

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:J

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:J

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298321
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298381
    new-instance v0, LX/872;

    invoke-direct {v0}, LX/872;-><init>()V

    .line 1298382
    sget-object v0, LX/875;->CAPTURE:LX/875;

    move-object v0, v0

    .line 1298383
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a:LX/875;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1298378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298379
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a:LX/875;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->q:LX/875;

    .line 1298380
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V
    .locals 2

    .prologue
    .line 1298329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298330
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1298331
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    if-eqz v0, :cond_0

    .line 1298332
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 1298333
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->b:Ljava/lang/String;

    .line 1298334
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->c:J

    .line 1298335
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->d:Ljava/lang/String;

    .line 1298336
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->e:J

    .line 1298337
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->f:Ljava/lang/String;

    .line 1298338
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->g:J

    .line 1298339
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->h:Ljava/lang/String;

    .line 1298340
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->i:J

    .line 1298341
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->j:Ljava/lang/String;

    .line 1298342
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->k:J

    .line 1298343
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->l:J

    .line 1298344
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->m:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298345
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->n:J

    .line 1298346
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->o:LX/874;

    .line 1298347
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->p:I

    .line 1298348
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->q:LX/875;

    .line 1298349
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->r:Ljava/lang/String;

    .line 1298350
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->s:J

    .line 1298351
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->t:Ljava/lang/String;

    .line 1298352
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->u:J

    .line 1298353
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->v:Ljava/lang/String;

    .line 1298354
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->w:J

    .line 1298355
    :goto_0
    return-void

    .line 1298356
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->b:Ljava/lang/String;

    .line 1298357
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->c:J

    .line 1298358
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->d:Ljava/lang/String;

    .line 1298359
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->e:J

    .line 1298360
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->f:Ljava/lang/String;

    .line 1298361
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->g:J

    .line 1298362
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->h:Ljava/lang/String;

    .line 1298363
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->i:J

    .line 1298364
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->j:Ljava/lang/String;

    .line 1298365
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->k:J

    .line 1298366
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getImpressionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->l:J

    .line 1298367
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationDoodleExtraLoggingData()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->m:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298368
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->n:J

    .line 1298369
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getLoggingSurface()LX/874;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->o:LX/874;

    .line 1298370
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaIndex()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->p:I

    .line 1298371
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->q:LX/875;

    .line 1298372
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->r:Ljava/lang/String;

    .line 1298373
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->s:J

    .line 1298374
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->t:Ljava/lang/String;

    .line 1298375
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->u:J

    .line 1298376
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->v:Ljava/lang/String;

    .line 1298377
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->w:J

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;
    .locals 2

    .prologue
    .line 1298328
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;)V

    return-object v0
.end method

.method public setCameraSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_session_id"
    .end annotation

    .prologue
    .line 1298326
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->b:Ljava/lang/String;

    .line 1298327
    return-object p0
.end method

.method public setCameraSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_session_start_time"
    .end annotation

    .prologue
    .line 1298324
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->c:J

    .line 1298325
    return-object p0
.end method

.method public setDoodleSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_session_id"
    .end annotation

    .prologue
    .line 1298322
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->d:Ljava/lang/String;

    .line 1298323
    return-object p0
.end method

.method public setDoodleSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_session_start_time"
    .end annotation

    .prologue
    .line 1298311
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->e:J

    .line 1298312
    return-object p0
.end method

.method public setEditingSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "editing_session_id"
    .end annotation

    .prologue
    .line 1298319
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->f:Ljava/lang/String;

    .line 1298320
    return-object p0
.end method

.method public setEditingSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "editing_session_start_time"
    .end annotation

    .prologue
    .line 1298317
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->g:J

    .line 1298318
    return-object p0
.end method

.method public setEffectsTraySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "effects_tray_session_id"
    .end annotation

    .prologue
    .line 1298315
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->h:Ljava/lang/String;

    .line 1298316
    return-object p0
.end method

.method public setEffectsTraySessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "effects_tray_session_start_time"
    .end annotation

    .prologue
    .line 1298313
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->i:J

    .line 1298314
    return-object p0
.end method

.method public setGallerySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gallery_session_id"
    .end annotation

    .prologue
    .line 1298384
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->j:Ljava/lang/String;

    .line 1298385
    return-object p0
.end method

.method public setGallerySessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gallery_session_start_time"
    .end annotation

    .prologue
    .line 1298285
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->k:J

    .line 1298286
    return-object p0
.end method

.method public setImpressionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "impression_start_time"
    .end annotation

    .prologue
    .line 1298287
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->l:J

    .line 1298288
    return-object p0
.end method

.method public setInspirationDoodleExtraLoggingData(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_doodle_extra_logging_data"
    .end annotation

    .prologue
    .line 1298289
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->m:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298290
    return-object p0
.end method

.method public setInspirationSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_session_start_time"
    .end annotation

    .prologue
    .line 1298291
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->n:J

    .line 1298292
    return-object p0
.end method

.method public setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # LX/874;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "logging_surface"
    .end annotation

    .prologue
    .line 1298293
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->o:LX/874;

    .line 1298294
    return-object p0
.end method

.method public setMediaIndex(I)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_index"
    .end annotation

    .prologue
    .line 1298295
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->p:I

    .line 1298296
    return-object p0
.end method

.method public setMediaSource(LX/875;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_source"
    .end annotation

    .prologue
    .line 1298297
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->q:LX/875;

    .line 1298298
    return-object p0
.end method

.method public setNuxSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nux_session_id"
    .end annotation

    .prologue
    .line 1298299
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->r:Ljava/lang/String;

    .line 1298300
    return-object p0
.end method

.method public setNuxSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nux_session_start_time"
    .end annotation

    .prologue
    .line 1298301
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->s:J

    .line 1298302
    return-object p0
.end method

.method public setShareSheetSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_sheet_session_id"
    .end annotation

    .prologue
    .line 1298303
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->t:Ljava/lang/String;

    .line 1298304
    return-object p0
.end method

.method public setShareSheetSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_sheet_session_start_time"
    .end annotation

    .prologue
    .line 1298305
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->u:J

    .line 1298306
    return-object p0
.end method

.method public setTextSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_session_id"
    .end annotation

    .prologue
    .line 1298307
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->v:Ljava/lang/String;

    .line 1298308
    return-object p0
.end method

.method public setTextSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_session_start_time"
    .end annotation

    .prologue
    .line 1298309
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->w:J

    .line 1298310
    return-object p0
.end method
