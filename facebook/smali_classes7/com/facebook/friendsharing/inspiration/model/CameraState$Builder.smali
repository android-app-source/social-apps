.class public final Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/CameraState_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/86q;


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/86q;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297847
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/CameraState_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1297851
    new-instance v0, LX/86r;

    invoke-direct {v0}, LX/86r;-><init>()V

    .line 1297852
    sget-object v0, LX/86q;->UNINITIALIZED:LX/86q;

    move-object v0, v0

    .line 1297853
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a:LX/86q;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1297848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297849
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a:LX/86q;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->d:LX/86q;

    .line 1297850
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/CameraState;)V
    .locals 2

    .prologue
    .line 1297830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297831
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1297832
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    if-eqz v0, :cond_0

    .line 1297833
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 1297834
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->b:Ljava/lang/String;

    .line 1297835
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->c:Ljava/lang/String;

    .line 1297836
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->d:LX/86q;

    .line 1297837
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->e:Ljava/lang/String;

    .line 1297838
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->f:Z

    .line 1297839
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->g:J

    .line 1297840
    :goto_0
    return-void

    .line 1297841
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCameraRollPermissionState()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->b:Ljava/lang/String;

    .line 1297842
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCapturePermissionState()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->c:Ljava/lang/String;

    .line 1297843
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->d:LX/86q;

    .line 1297844
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->e:Ljava/lang/String;

    .line 1297845
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->f:Z

    .line 1297846
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getVideoRecordStartTimeMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->g:J

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/CameraState;
    .locals 2

    .prologue
    .line 1297829
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;-><init>(Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;)V

    return-object v0
.end method

.method public setCameraRollPermissionState(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_roll_permission_state"
    .end annotation

    .prologue
    .line 1297854
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->b:Ljava/lang/String;

    .line 1297855
    return-object p0
.end method

.method public setCapturePermissionState(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capture_permission_state"
    .end annotation

    .prologue
    .line 1297827
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->c:Ljava/lang/String;

    .line 1297828
    return-object p0
.end method

.method public setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capture_state"
    .end annotation

    .prologue
    .line 1297825
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->d:LX/86q;

    .line 1297826
    return-object p0
.end method

.method public setFlashMode(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "flash_mode"
    .end annotation

    .prologue
    .line 1297819
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->e:Ljava/lang/String;

    .line 1297820
    return-object p0
.end method

.method public setIsCameraFrontFacing(Z)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_camera_front_facing"
    .end annotation

    .prologue
    .line 1297823
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->f:Z

    .line 1297824
    return-object p0
.end method

.method public setVideoRecordStartTimeMs(J)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_record_start_time_ms"
    .end annotation

    .prologue
    .line 1297821
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->g:J

    .line 1297822
    return-object p0
.end method
