.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298714
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298708
    new-instance v0, LX/877;

    invoke-direct {v0}, LX/877;-><init>()V

    .line 1298709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->MANUAL:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1298710
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a:Ljava/lang/String;

    .line 1298711
    new-instance v0, LX/878;

    invoke-direct {v0}, LX/878;-><init>()V

    .line 1298712
    const-string v0, "0"

    move-object v0, v0

    .line 1298713
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1298703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298704
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->d:Ljava/lang/String;

    .line 1298705
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->h:Ljava/lang/String;

    .line 1298706
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->l:Ljava/lang/String;

    .line 1298707
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 2

    .prologue
    .line 1298702
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;)V

    return-object v0
.end method

.method public setFrame(Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame"
    .end annotation

    .prologue
    .line 1298700
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->c:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    .line 1298701
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1298698
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->d:Ljava/lang/String;

    .line 1298699
    return-object p0
.end method

.method public setIsLoggingDisabled(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_logging_disabled"
    .end annotation

    .prologue
    .line 1298715
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->e:Z

    .line 1298716
    return-object p0
.end method

.method public setMask(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mask"
    .end annotation

    .prologue
    .line 1298696
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->f:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1298697
    return-object p0
.end method

.method public setParticleEffect(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "particle_effect"
    .end annotation

    .prologue
    .line 1298694
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->g:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    .line 1298695
    return-object p0
.end method

.method public setPromptType(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation

    .prologue
    .line 1298692
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->h:Ljava/lang/String;

    .line 1298693
    return-object p0
.end method

.method public setShaderFilter(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shader_filter"
    .end annotation

    .prologue
    .line 1298690
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->i:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1298691
    return-object p0
.end method

.method public setStyleTransfer(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style_transfer"
    .end annotation

    .prologue
    .line 1298684
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->j:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    .line 1298685
    return-object p0
.end method

.method public setThumbnailUri(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "thumbnail_uri"
    .end annotation

    .prologue
    .line 1298688
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->k:Ljava/lang/String;

    .line 1298689
    return-object p0
.end method

.method public setTrackingString(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tracking_string"
    .end annotation

    .prologue
    .line 1298686
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->l:Ljava/lang/String;

    .line 1298687
    return-object p0
.end method
