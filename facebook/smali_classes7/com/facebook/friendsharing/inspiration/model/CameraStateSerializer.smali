.class public Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/CameraState;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1297947
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1297948
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1297946
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/CameraState;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1297940
    if-nez p0, :cond_0

    .line 1297941
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1297942
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1297943
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/CameraState;LX/0nX;LX/0my;)V

    .line 1297944
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1297945
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/CameraState;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1297933
    const-string v0, "camera_roll_permission_state"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCameraRollPermissionState()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297934
    const-string v0, "capture_permission_state"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCapturePermissionState()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297935
    const-string v0, "capture_state"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1297936
    const-string v0, "flash_mode"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getFlashMode()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297937
    const-string v0, "is_camera_front_facing"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1297938
    const-string v0, "video_record_start_time_ms"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getVideoRecordStartTimeMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1297939
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1297932
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;LX/0nX;LX/0my;)V

    return-void
.end method
