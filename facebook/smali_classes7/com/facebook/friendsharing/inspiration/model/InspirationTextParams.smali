.class public Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:F

.field public final d:F

.field public final e:Z

.field public final f:F

.field public final g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

.field public final h:F

.field public final i:D

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:F

.field public final m:F

.field public final n:F

.field public final o:I

.field public final p:Ljava/lang/String;

.field public final q:I

.field public final r:I

.field public final s:F

.field public final t:I

.field public final u:F

.field public final v:Ljava/lang/String;

.field public final w:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299883
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299884
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299885
    new-instance v0, LX/87K;

    invoke-direct {v0}, LX/87K;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1299886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299887
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    .line 1299888
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    .line 1299889
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    .line 1299890
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    .line 1299891
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    .line 1299892
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    .line 1299893
    sget-object v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299894
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    .line 1299895
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    .line 1299896
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    .line 1299897
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    .line 1299898
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    .line 1299899
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    .line 1299900
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    .line 1299901
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    .line 1299902
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    .line 1299903
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    .line 1299904
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    .line 1299905
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    .line 1299906
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    .line 1299907
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    .line 1299908
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    .line 1299909
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    .line 1299910
    return-void

    .line 1299911
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;)V
    .locals 2

    .prologue
    .line 1299778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299779
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    .line 1299780
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    .line 1299781
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    .line 1299782
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->e:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    .line 1299783
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    .line 1299784
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->g:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    .line 1299785
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299786
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    .line 1299787
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->j:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    .line 1299788
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    .line 1299789
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->l:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    .line 1299790
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->m:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    .line 1299791
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->n:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    .line 1299792
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->o:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    .line 1299793
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    .line 1299794
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->q:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    .line 1299795
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    .line 1299796
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->s:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    .line 1299797
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->t:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    .line 1299798
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->u:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    .line 1299799
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->v:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    .line 1299800
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    .line 1299801
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->x:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    .line 1299802
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 2

    .prologue
    .line 1299912
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 2

    .prologue
    .line 1299913
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1299914
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1299831
    if-ne p0, p1, :cond_1

    .line 1299832
    :cond_0
    :goto_0
    return v0

    .line 1299833
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1299834
    goto :goto_0

    .line 1299835
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 1299836
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1299837
    goto :goto_0

    .line 1299838
    :cond_3
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1299839
    goto :goto_0

    .line 1299840
    :cond_4
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_5

    move v0, v1

    .line 1299841
    goto :goto_0

    .line 1299842
    :cond_5
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_6

    move v0, v1

    .line 1299843
    goto :goto_0

    .line 1299844
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1299845
    goto :goto_0

    .line 1299846
    :cond_7
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_8

    move v0, v1

    .line 1299847
    goto :goto_0

    .line 1299848
    :cond_8
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1299849
    goto :goto_0

    .line 1299850
    :cond_9
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_a

    move v0, v1

    .line 1299851
    goto :goto_0

    .line 1299852
    :cond_a
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_b

    move v0, v1

    .line 1299853
    goto :goto_0

    .line 1299854
    :cond_b
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1299855
    goto :goto_0

    .line 1299856
    :cond_c
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1299857
    goto :goto_0

    .line 1299858
    :cond_d
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_e

    move v0, v1

    .line 1299859
    goto :goto_0

    .line 1299860
    :cond_e
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_f

    move v0, v1

    .line 1299861
    goto/16 :goto_0

    .line 1299862
    :cond_f
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_10

    move v0, v1

    .line 1299863
    goto/16 :goto_0

    .line 1299864
    :cond_10
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1299865
    goto/16 :goto_0

    .line 1299866
    :cond_11
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 1299867
    goto/16 :goto_0

    .line 1299868
    :cond_12
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1299869
    goto/16 :goto_0

    .line 1299870
    :cond_13
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 1299871
    goto/16 :goto_0

    .line 1299872
    :cond_14
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_15

    move v0, v1

    .line 1299873
    goto/16 :goto_0

    .line 1299874
    :cond_15
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1299875
    goto/16 :goto_0

    .line 1299876
    :cond_16
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_17

    move v0, v1

    .line 1299877
    goto/16 :goto_0

    .line 1299878
    :cond_17
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 1299879
    goto/16 :goto_0

    .line 1299880
    :cond_18
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    .line 1299881
    goto/16 :goto_0
.end method

.method public getBorderAlpha()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_alpha"
    .end annotation

    .prologue
    .line 1299915
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    return v0
.end method

.method public getBorderColor()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_color"
    .end annotation

    .prologue
    .line 1299916
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    return v0
.end method

.method public getBorderWidth()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_width"
    .end annotation

    .prologue
    .line 1299917
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    return v0
.end method

.method public getHeightPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height_percentage"
    .end annotation

    .prologue
    .line 1299918
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    return v0
.end method

.method public getLeftPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left_percentage"
    .end annotation

    .prologue
    .line 1299919
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    return v0
.end method

.method public getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_rect"
    .end annotation

    .prologue
    .line 1299920
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-object v0
.end method

.method public getRotation()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation"
    .end annotation

    .prologue
    .line 1299921
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    return v0
.end method

.method public getScaleFactor()D
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "scale_factor"
    .end annotation

    .prologue
    .line 1299922
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    return-wide v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 1299882
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getShadowColor()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_color"
    .end annotation

    .prologue
    .line 1299766
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    return v0
.end method

.method public getShadowDX()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_d_x"
    .end annotation

    .prologue
    .line 1299767
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    return v0
.end method

.method public getShadowDY()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_d_y"
    .end annotation

    .prologue
    .line 1299768
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    return v0
.end method

.method public getShadowRadius()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_radius"
    .end annotation

    .prologue
    .line 1299769
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    return v0
.end method

.method public getSizeMultiplier()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size_multiplier"
    .end annotation

    .prologue
    .line 1299770
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation

    .prologue
    .line 1299771
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_color"
    .end annotation

    .prologue
    .line 1299772
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    return v0
.end method

.method public getTextHeight()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_height"
    .end annotation

    .prologue
    .line 1299773
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    return v0
.end method

.method public getTextSize()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_size"
    .end annotation

    .prologue
    .line 1299774
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    return v0
.end method

.method public getTextWidth()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_width"
    .end annotation

    .prologue
    .line 1299775
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    return v0
.end method

.method public getTopPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top_percentage"
    .end annotation

    .prologue
    .line 1299776
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    return v0
.end method

.method public getTypeface()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "typeface"
    .end annotation

    .prologue
    .line 1299777
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    return-object v0
.end method

.method public getWidthPercentage()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width_percentage"
    .end annotation

    .prologue
    .line 1299803
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1299804
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isKeyboardOpen()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_keyboard_open"
    .end annotation

    .prologue
    .line 1299805
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1299806
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299807
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299808
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299809
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299810
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299811
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299812
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1299813
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299814
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1299815
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299816
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299817
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299818
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299819
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299820
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299821
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299822
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299823
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299824
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299825
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299826
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299827
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299828
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1299829
    return-void

    .line 1299830
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
