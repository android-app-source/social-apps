.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298899
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1298901
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1298902
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
    .locals 1

    .prologue
    .line 1298903
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    .line 1298904
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1298900
    invoke-static {p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    return-object v0
.end method
