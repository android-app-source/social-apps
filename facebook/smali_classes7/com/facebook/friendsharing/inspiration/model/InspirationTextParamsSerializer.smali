.class public Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299949
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1299950
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299948
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299951
    if-nez p0, :cond_0

    .line 1299952
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1299953
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1299954
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;LX/0nX;LX/0my;)V

    .line 1299955
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1299956
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1299923
    const-string v0, "border_alpha"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderAlpha()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299924
    const-string v0, "border_color"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299925
    const-string v0, "border_width"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderWidth()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299926
    const-string v0, "height_percentage"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getHeightPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299927
    const-string v0, "is_keyboard_open"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->isKeyboardOpen()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299928
    const-string v0, "left_percentage"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getLeftPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299929
    const-string v0, "media_rect"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1299930
    const-string v0, "rotation"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getRotation()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299931
    const-string v0, "scale_factor"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getScaleFactor()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1299932
    const-string v0, "session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299933
    const-string v0, "shadow_color"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299934
    const-string v0, "shadow_d_x"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299935
    const-string v0, "shadow_d_y"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299936
    const-string v0, "shadow_radius"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowRadius()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299937
    const-string v0, "size_multiplier"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299938
    const-string v0, "text"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299939
    const-string v0, "text_color"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299940
    const-string v0, "text_height"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299941
    const-string v0, "text_size"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextSize()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299942
    const-string v0, "text_width"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299943
    const-string v0, "top_percentage"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTopPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299944
    const-string v0, "typeface"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTypeface()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299945
    const-string v0, "width_percentage"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getWidthPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1299946
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299947
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;LX/0nX;LX/0my;)V

    return-void
.end method
