.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/86w;

.field private static final b:Lcom/facebook/photos/creativeediting/model/PersistableRect;


# instance fields
.field public c:I

.field public d:LX/86w;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

.field public g:F

.field public h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298111
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298112
    new-instance v0, LX/86x;

    invoke-direct {v0}, LX/86x;-><init>()V

    .line 1298113
    sget-object v0, LX/86w;->HIDDEN:LX/86w;

    move-object v0, v0

    .line 1298114
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a:LX/86w;

    .line 1298115
    new-instance v0, LX/86y;

    invoke-direct {v0}, LX/86y;-><init>()V

    const/4 v1, 0x0

    .line 1298116
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    move-object v0, v0

    .line 1298117
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1298118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298119
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a:LX/86w;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->d:LX/86w;

    .line 1298120
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->e:Ljava/lang/String;

    .line 1298121
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->b:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298122
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)V
    .locals 1

    .prologue
    .line 1298123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298124
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1298125
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    if-eqz v0, :cond_0

    .line 1298126
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 1298127
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->c:I

    .line 1298128
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->d:LX/86w;

    .line 1298129
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->e:Ljava/lang/String;

    .line 1298130
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298131
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->g:F

    .line 1298132
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->h:Landroid/net/Uri;

    .line 1298133
    :goto_0
    return-void

    .line 1298134
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->c:I

    .line 1298135
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getDrawingMode()LX/86w;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->d:LX/86w;

    .line 1298136
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->e:Ljava/lang/String;

    .line 1298137
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298138
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getStrokeWidth()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->g:F

    .line 1298139
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getUri()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->h:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
    .locals 2

    .prologue
    .line 1298140
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;)V

    return-object v0
.end method

.method public setColorSelection(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "color_selection"
    .end annotation

    .prologue
    .line 1298141
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->c:I

    .line 1298142
    return-object p0
.end method

.method public setDrawingMode(LX/86w;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "drawing_mode"
    .end annotation

    .prologue
    .line 1298143
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->d:LX/86w;

    .line 1298144
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1298145
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->e:Ljava/lang/String;

    .line 1298146
    return-object p0
.end method

.method public setMediaRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_rect"
    .end annotation

    .prologue
    .line 1298147
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298148
    return-object p0
.end method

.method public setStrokeWidth(F)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stroke_width"
    .end annotation

    .prologue
    .line 1298149
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->g:F

    .line 1298150
    return-object p0
.end method

.method public setUri(Landroid/net/Uri;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation

    .prologue
    .line 1298151
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->h:Landroid/net/Uri;

    .line 1298152
    return-object p0
.end method
