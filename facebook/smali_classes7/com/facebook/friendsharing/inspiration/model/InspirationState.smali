.class public Lcom/facebook/friendsharing/inspiration/model/InspirationState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationState$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/870;

.field public final b:Z

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/86t;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/86o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Z

.field public final o:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299412
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299411
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299410
    new-instance v0, LX/87E;

    invoke-direct {v0}, LX/87E;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1299374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299375
    invoke-static {}, LX/870;->values()[LX/870;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    .line 1299376
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    .line 1299377
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [LX/86t;

    move v0, v2

    .line 1299378
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 1299379
    invoke-static {}, LX/86t;->values()[LX/86t;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v4, v4, v5

    .line 1299380
    aput-object v4, v3, v0

    .line 1299381
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1299382
    goto :goto_0

    .line 1299383
    :cond_1
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    .line 1299384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    .line 1299385
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    .line 1299386
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    .line 1299387
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    .line 1299388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    .line 1299389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    .line 1299390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    .line 1299391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    .line 1299392
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    .line 1299393
    iput-object v6, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1299394
    :goto_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    .line 1299395
    iput-object v6, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    .line 1299396
    :goto_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_c

    :goto_c
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    .line 1299397
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    .line 1299398
    return-void

    :cond_2
    move v0, v2

    .line 1299399
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1299400
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1299401
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1299402
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1299403
    goto :goto_6

    :cond_7
    move v0, v2

    .line 1299404
    goto :goto_7

    :cond_8
    move v0, v2

    .line 1299405
    goto :goto_8

    :cond_9
    move v0, v2

    .line 1299406
    goto :goto_9

    .line 1299407
    :cond_a
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    goto :goto_a

    .line 1299408
    :cond_b
    invoke-static {}, LX/86o;->values()[LX/86o;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    goto :goto_b

    :cond_c
    move v1, v2

    .line 1299409
    goto :goto_c
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;)V
    .locals 1

    .prologue
    .line 1299357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299358
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->c:LX/870;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/870;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    .line 1299359
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    .line 1299360
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    .line 1299361
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    .line 1299362
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    .line 1299363
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    .line 1299364
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    .line 1299365
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    .line 1299366
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    .line 1299367
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    .line 1299368
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    .line 1299369
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->n:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1299370
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->o:LX/86o;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    .line 1299371
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    .line 1299372
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->q:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    .line 1299373
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 2

    .prologue
    .line 1299356
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 2

    .prologue
    .line 1299355
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1299354
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1299319
    if-ne p0, p1, :cond_1

    .line 1299320
    :cond_0
    :goto_0
    return v0

    .line 1299321
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    if-nez v2, :cond_2

    move v0, v1

    .line 1299322
    goto :goto_0

    .line 1299323
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 1299324
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1299325
    goto :goto_0

    .line 1299326
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1299327
    goto :goto_0

    .line 1299328
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1299329
    goto :goto_0

    .line 1299330
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1299331
    goto :goto_0

    .line 1299332
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1299333
    goto :goto_0

    .line 1299334
    :cond_7
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1299335
    goto :goto_0

    .line 1299336
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1299337
    goto :goto_0

    .line 1299338
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1299339
    goto :goto_0

    .line 1299340
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1299341
    goto :goto_0

    .line 1299342
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1299343
    goto :goto_0

    .line 1299344
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1299345
    goto :goto_0

    .line 1299346
    :cond_d
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1299347
    goto :goto_0

    .line 1299348
    :cond_e
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1299349
    goto :goto_0

    .line 1299350
    :cond_f
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1299351
    goto/16 :goto_0

    .line 1299352
    :cond_10
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1299353
    goto/16 :goto_0
.end method

.method public getFormatMode()LX/870;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "format_mode"
    .end annotation

    .prologue
    .line 1299318
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    return-object v0
.end method

.method public getInspirationBackStack()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_back_stack"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/86t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1299315
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    return-object v0
.end method

.method public getLocation()Lcom/facebook/ipc/composer/model/ComposerLocation;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299317
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    return-object v0
.end method

.method public getOpenBottomTray()LX/86o;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "open_bottom_tray"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299316
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    return-object v0
.end method

.method public getTextColorSelection()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_color_selection"
    .end annotation

    .prologue
    .line 1299413
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    return v0
.end method

.method public hasChangedInspiration()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_changed_inspiration"
    .end annotation

    .prologue
    .line 1299270
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1299269
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isBackPressed()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_back_pressed"
    .end annotation

    .prologue
    .line 1299271
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    return v0
.end method

.method public isBottomTrayTransitioning()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_bottom_tray_transitioning"
    .end annotation

    .prologue
    .line 1299272
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    return v0
.end method

.method public isInNuxMode()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_in_nux_mode"
    .end annotation

    .prologue
    .line 1299273
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    return v0
.end method

.method public isLocationFetchAndRequeryingEffectsInProgress()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_location_fetch_and_requerying_effects_in_progress"
    .end annotation

    .prologue
    .line 1299274
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    return v0
.end method

.method public isMirrorOn()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_mirror_on"
    .end annotation

    .prologue
    .line 1299275
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    return v0
.end method

.method public isMuted()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_muted"
    .end annotation

    .prologue
    .line 1299276
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    return v0
.end method

.method public isRecordingAtLimit()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_recording_at_limit"
    .end annotation

    .prologue
    .line 1299277
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    return v0
.end method

.method public isReleaseVideoPlayerRequested()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_release_video_player_requested"
    .end annotation

    .prologue
    .line 1299278
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    return v0
.end method

.method public shouldRefreshCameraRoll()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_refresh_camera_roll"
    .end annotation

    .prologue
    .line 1299279
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1299280
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    invoke-virtual {v0}, LX/870;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299281
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299282
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299283
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/86t;

    .line 1299284
    invoke-virtual {v0}, LX/86t;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299285
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1299286
    goto :goto_0

    .line 1299287
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299288
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299289
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299290
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299291
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299292
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299293
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299294
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299295
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    if-nez v0, :cond_a

    .line 1299296
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299297
    :goto_a
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    if-nez v0, :cond_b

    .line 1299298
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299299
    :goto_b
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    if-eqz v0, :cond_c

    :goto_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299300
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299301
    return-void

    :cond_2
    move v0, v2

    .line 1299302
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1299303
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1299304
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1299305
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1299306
    goto :goto_6

    :cond_7
    move v0, v2

    .line 1299307
    goto :goto_7

    :cond_8
    move v0, v2

    .line 1299308
    goto :goto_8

    :cond_9
    move v0, v2

    .line 1299309
    goto :goto_9

    .line 1299310
    :cond_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299311
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_a

    .line 1299312
    :cond_b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299313
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    invoke-virtual {v0}, LX/86o;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    :cond_c
    move v1, v2

    .line 1299314
    goto :goto_c
.end method
