.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298722
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1298720
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1298721
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
    .locals 1

    .prologue
    .line 1298718
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;->a:Lcom/facebook/friendsharing/inspiration/model/InspirationModel_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    .line 1298719
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1298717
    invoke-static {p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    return-object v0
.end method
