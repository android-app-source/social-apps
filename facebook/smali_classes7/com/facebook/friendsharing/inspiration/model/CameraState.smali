.class public Lcom/facebook/friendsharing/inspiration/model/CameraState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/CameraState$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/CameraState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/86q;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public final f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297914
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1297913
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/CameraStateSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1297912
    new-instance v0, LX/86p;

    invoke-direct {v0}, LX/86p;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1297897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297898
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_0

    .line 1297899
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    .line 1297900
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_1

    .line 1297901
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    .line 1297902
    :goto_1
    invoke-static {}, LX/86q;->values()[LX/86q;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    .line 1297903
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_2

    .line 1297904
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    .line 1297905
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_3

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    .line 1297906
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    .line 1297907
    return-void

    .line 1297908
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    goto :goto_0

    .line 1297909
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    goto :goto_1

    .line 1297910
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    goto :goto_2

    .line 1297911
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;)V
    .locals 2

    .prologue
    .line 1297889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297890
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    .line 1297891
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    .line 1297892
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->d:LX/86q;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/86q;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    .line 1297893
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    .line 1297894
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    .line 1297895
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->g:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    .line 1297896
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 2

    .prologue
    .line 1297883
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/CameraState;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;
    .locals 2

    .prologue
    .line 1297888
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1297887
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1297915
    if-ne p0, p1, :cond_1

    .line 1297916
    :cond_0
    :goto_0
    return v0

    .line 1297917
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    if-nez v2, :cond_2

    move v0, v1

    .line 1297918
    goto :goto_0

    .line 1297919
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 1297920
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1297921
    goto :goto_0

    .line 1297922
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1297923
    goto :goto_0

    .line 1297924
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1297925
    goto :goto_0

    .line 1297926
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1297927
    goto :goto_0

    .line 1297928
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1297929
    goto :goto_0

    .line 1297930
    :cond_7
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 1297931
    goto :goto_0
.end method

.method public getCameraRollPermissionState()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_roll_permission_state"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1297886
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getCapturePermissionState()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capture_permission_state"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1297885
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getCaptureState()LX/86q;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capture_state"
    .end annotation

    .prologue
    .line 1297884
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    return-object v0
.end method

.method public getFlashMode()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "flash_mode"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1297882
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoRecordStartTimeMs()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "video_record_start_time_ms"
    .end annotation

    .prologue
    .line 1297881
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1297880
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isCameraFrontFacing()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_camera_front_facing"
    .end annotation

    .prologue
    .line 1297879
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1297862
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1297863
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297864
    :goto_0
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 1297865
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297866
    :goto_1
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->c:LX/86q;

    invoke-virtual {v2}, LX/86q;->ordinal()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297867
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 1297868
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297869
    :goto_2
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->e:Z

    if-eqz v2, :cond_3

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297870
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1297871
    return-void

    .line 1297872
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297873
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1297874
    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297875
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1297876
    :cond_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1297877
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/CameraState;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1297878
    goto :goto_3
.end method
