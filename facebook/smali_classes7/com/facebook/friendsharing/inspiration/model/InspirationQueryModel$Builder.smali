.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299081
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299082
    new-instance v0, LX/87D;

    invoke-direct {v0}, LX/87D;-><init>()V

    .line 1299083
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1299084
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1299085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299086
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->c:LX/0Px;

    .line 1299087
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V
    .locals 1

    .prologue
    .line 1299088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299089
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1299090
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    if-eqz v0, :cond_0

    .line 1299091
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 1299092
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->b:I

    .line 1299093
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->c:LX/0Px;

    .line 1299094
    :goto_0
    return-void

    .line 1299095
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getDefaultInspirationLandingIndex()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->b:I

    .line 1299096
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->c:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;
    .locals 2

    .prologue
    .line 1299097
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;)V

    return-object v0
.end method

.method public setDefaultInspirationLandingIndex(I)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "default_inspiration_landing_index"
    .end annotation

    .prologue
    .line 1299098
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->b:I

    .line 1299099
    return-object p0
.end method

.method public setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_models"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;)",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;"
        }
    .end annotation

    .prologue
    .line 1299100
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->c:LX/0Px;

    .line 1299101
    return-object p0
.end method
