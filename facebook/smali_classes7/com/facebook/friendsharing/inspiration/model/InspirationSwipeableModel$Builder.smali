.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299481
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299483
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V
    .locals 1

    .prologue
    .line 1299484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299485
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1299486
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    if-eqz v0, :cond_0

    .line 1299487
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 1299488
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a:Z

    .line 1299489
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->b:LX/0Px;

    .line 1299490
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->c:Z

    .line 1299491
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->d:Ljava/lang/String;

    .line 1299492
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->e:Ljava/lang/String;

    .line 1299493
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->f:Ljava/lang/String;

    .line 1299494
    :goto_0
    return-void

    .line 1299495
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->areEffectsLoading()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a:Z

    .line 1299496
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->b:LX/0Px;

    .line 1299497
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->isFromTray()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->c:Z

    .line 1299498
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->d:Ljava/lang/String;

    .line 1299499
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->e:Ljava/lang/String;

    .line 1299500
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->f:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;
    .locals 2

    .prologue
    .line 1299501
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;)V

    return-object v0
.end method

.method public setAreEffectsLoading(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "are_effects_loading"
    .end annotation

    .prologue
    .line 1299502
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a:Z

    .line 1299503
    return-object p0
.end method

.method public setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_models"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;)",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;"
        }
    .end annotation

    .prologue
    .line 1299504
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->b:LX/0Px;

    .line 1299505
    return-object p0
.end method

.method public setIsFromTray(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_from_tray"
    .end annotation

    .prologue
    .line 1299506
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->c:Z

    .line 1299507
    return-object p0
.end method

.method public setLastSelectedPreCaptureId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_selected_pre_capture_id"
    .end annotation

    .prologue
    .line 1299508
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->d:Ljava/lang/String;

    .line 1299509
    return-object p0
.end method

.method public setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_id"
    .end annotation

    .prologue
    .line 1299510
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->e:Ljava/lang/String;

    .line 1299511
    return-object p0
.end method

.method public setStarId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "star_id"
    .end annotation

    .prologue
    .line 1299512
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->f:Ljava/lang/String;

    .line 1299513
    return-object p0
.end method
