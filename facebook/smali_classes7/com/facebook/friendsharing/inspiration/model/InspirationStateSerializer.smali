.class public Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationState;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299414
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1299415
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1299439
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299433
    if-nez p0, :cond_0

    .line 1299434
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1299435
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1299436
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/0nX;LX/0my;)V

    .line 1299437
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1299438
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1299417
    const-string v0, "format_mode"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1299418
    const-string v0, "has_changed_inspiration"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->hasChangedInspiration()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299419
    const-string v0, "inspiration_back_stack"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1299420
    const-string v0, "is_back_pressed"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBackPressed()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299421
    const-string v0, "is_bottom_tray_transitioning"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBottomTrayTransitioning()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299422
    const-string v0, "is_in_nux_mode"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299423
    const-string v0, "is_location_fetch_and_requerying_effects_in_progress"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299424
    const-string v0, "is_mirror_on"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299425
    const-string v0, "is_muted"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299426
    const-string v0, "is_recording_at_limit"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isRecordingAtLimit()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299427
    const-string v0, "is_release_video_player_requested"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299428
    const-string v0, "location"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getLocation()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1299429
    const-string v0, "open_bottom_tray"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1299430
    const-string v0, "should_refresh_camera_roll"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->shouldRefreshCameraRoll()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1299431
    const-string v0, "text_color_selection"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getTextColorSelection()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1299432
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1299416
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationStateSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/0nX;LX/0my;)V

    return-void
.end method
