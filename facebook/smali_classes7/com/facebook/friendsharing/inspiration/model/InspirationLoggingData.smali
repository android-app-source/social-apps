.class public Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:J

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:J

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:J

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:J

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:J

.field public final k:J

.field public final l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:J

.field public final n:LX/874;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:I

.field public final p:LX/875;

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:J

.field public final s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final t:J

.field public final u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298460
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298462
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298463
    new-instance v0, LX/871;

    invoke-direct {v0}, LX/871;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1298464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298465
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1298466
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    .line 1298467
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    .line 1298468
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1298469
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    .line 1298470
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    .line 1298471
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1298472
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    .line 1298473
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    .line 1298474
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1298475
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    .line 1298476
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    .line 1298477
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1298478
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    .line 1298479
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    .line 1298480
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    .line 1298481
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1298482
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298483
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    .line 1298484
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1298485
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    .line 1298486
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    .line 1298487
    invoke-static {}, LX/875;->values()[LX/875;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    .line 1298488
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    .line 1298489
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    .line 1298490
    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    .line 1298491
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    .line 1298492
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    .line 1298493
    :goto_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    .line 1298494
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    .line 1298495
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    .line 1298496
    :goto_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    .line 1298497
    return-void

    .line 1298498
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1298499
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 1298500
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    goto/16 :goto_2

    .line 1298501
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    goto/16 :goto_3

    .line 1298502
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    goto/16 :goto_4

    .line 1298503
    :cond_5
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    goto :goto_5

    .line 1298504
    :cond_6
    invoke-static {}, LX/874;->values()[LX/874;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    goto :goto_6

    .line 1298505
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    goto :goto_7

    .line 1298506
    :cond_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    goto :goto_8

    .line 1298507
    :cond_9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    goto :goto_9
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;)V
    .locals 2

    .prologue
    .line 1298508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298509
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    .line 1298510
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    .line 1298511
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    .line 1298512
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    .line 1298513
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    .line 1298514
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->g:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    .line 1298515
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    .line 1298516
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->i:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    .line 1298517
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    .line 1298518
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->k:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    .line 1298519
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->l:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    .line 1298520
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->m:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298521
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->n:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    .line 1298522
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->o:LX/874;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    .line 1298523
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    .line 1298524
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->q:LX/875;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/875;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    .line 1298525
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    .line 1298526
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->s:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    .line 1298527
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    .line 1298528
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->u:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    .line 1298529
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    .line 1298530
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->w:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    .line 1298531
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 2

    .prologue
    .line 1298532
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;
    .locals 2

    .prologue
    .line 1298533
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1298534
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298540
    if-ne p0, p1, :cond_1

    .line 1298541
    :cond_0
    :goto_0
    return v0

    .line 1298542
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1298543
    goto :goto_0

    .line 1298544
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 1298545
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1298546
    goto :goto_0

    .line 1298547
    :cond_3
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 1298548
    goto :goto_0

    .line 1298549
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1298550
    goto :goto_0

    .line 1298551
    :cond_5
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 1298552
    goto :goto_0

    .line 1298553
    :cond_6
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1298554
    goto :goto_0

    .line 1298555
    :cond_7
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 1298556
    goto :goto_0

    .line 1298557
    :cond_8
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1298558
    goto :goto_0

    .line 1298559
    :cond_9
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    move v0, v1

    .line 1298560
    goto :goto_0

    .line 1298561
    :cond_a
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1298562
    goto :goto_0

    .line 1298563
    :cond_b
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 1298564
    goto :goto_0

    .line 1298565
    :cond_c
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    move v0, v1

    .line 1298566
    goto :goto_0

    .line 1298567
    :cond_d
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1298568
    goto/16 :goto_0

    .line 1298569
    :cond_e
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 1298570
    goto/16 :goto_0

    .line 1298571
    :cond_f
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 1298572
    goto/16 :goto_0

    .line 1298573
    :cond_10
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1298574
    goto/16 :goto_0

    .line 1298575
    :cond_11
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 1298576
    goto/16 :goto_0

    .line 1298577
    :cond_12
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 1298578
    goto/16 :goto_0

    .line 1298579
    :cond_13
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    move v0, v1

    .line 1298580
    goto/16 :goto_0

    .line 1298581
    :cond_14
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 1298582
    goto/16 :goto_0

    .line 1298583
    :cond_15
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    move v0, v1

    .line 1298584
    goto/16 :goto_0

    .line 1298585
    :cond_16
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 1298586
    goto/16 :goto_0

    .line 1298587
    :cond_17
    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    iget-wide v4, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 1298588
    goto/16 :goto_0
.end method

.method public getCameraSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298535
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getCameraSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camera_session_start_time"
    .end annotation

    .prologue
    .line 1298590
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    return-wide v0
.end method

.method public getDoodleSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298539
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getDoodleSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_session_start_time"
    .end annotation

    .prologue
    .line 1298589
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    return-wide v0
.end method

.method public getEditingSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "editing_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298538
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getEditingSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "editing_session_start_time"
    .end annotation

    .prologue
    .line 1298537
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    return-wide v0
.end method

.method public getEffectsTraySessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "effects_tray_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298536
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getEffectsTraySessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "effects_tray_session_start_time"
    .end annotation

    .prologue
    .line 1298461
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    return-wide v0
.end method

.method public getGallerySessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gallery_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298393
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getGallerySessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "gallery_session_start_time"
    .end annotation

    .prologue
    .line 1298394
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    return-wide v0
.end method

.method public getImpressionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "impression_start_time"
    .end annotation

    .prologue
    .line 1298395
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    return-wide v0
.end method

.method public getInspirationDoodleExtraLoggingData()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_doodle_extra_logging_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298396
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    return-object v0
.end method

.method public getInspirationSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_session_start_time"
    .end annotation

    .prologue
    .line 1298397
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    return-wide v0
.end method

.method public getLoggingSurface()LX/874;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "logging_surface"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298398
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    return-object v0
.end method

.method public getMediaIndex()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_index"
    .end annotation

    .prologue
    .line 1298399
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    return v0
.end method

.method public getMediaSource()LX/875;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_source"
    .end annotation

    .prologue
    .line 1298400
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    return-object v0
.end method

.method public getNuxSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nux_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298392
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getNuxSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nux_session_start_time"
    .end annotation

    .prologue
    .line 1298401
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    return-wide v0
.end method

.method public getShareSheetSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_sheet_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298402
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    return-object v0
.end method

.method public getShareSheetSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_sheet_session_start_time"
    .end annotation

    .prologue
    .line 1298403
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    return-wide v0
.end method

.method public getTextSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298404
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    return-object v0
.end method

.method public getTextSessionStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_session_start_time"
    .end annotation

    .prologue
    .line 1298405
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1298406
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-wide v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1298407
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1298408
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298409
    :goto_0
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298410
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1298411
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298412
    :goto_1
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298413
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1298414
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298415
    :goto_2
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298416
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1298417
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298418
    :goto_3
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298419
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1298420
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298421
    :goto_4
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298422
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298423
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    if-nez v0, :cond_5

    .line 1298424
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298425
    :goto_5
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->m:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298426
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    if-nez v0, :cond_6

    .line 1298427
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298428
    :goto_6
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298429
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->p:LX/875;

    invoke-virtual {v0}, LX/875;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298430
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1298431
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298432
    :goto_7
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->r:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298433
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 1298434
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298435
    :goto_8
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->t:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298436
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1298437
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298438
    :goto_9
    iget-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->v:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1298439
    return-void

    .line 1298440
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298441
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1298442
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298443
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1298444
    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298445
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1298446
    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298447
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1298448
    :cond_4
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298449
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    .line 1298450
    :cond_5
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298451
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_5

    .line 1298452
    :cond_6
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298453
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->n:LX/874;

    invoke-virtual {v0}, LX/874;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 1298454
    :cond_7
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298455
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_7

    .line 1298456
    :cond_8
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298457
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_8

    .line 1298458
    :cond_9
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298459
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_9
.end method
