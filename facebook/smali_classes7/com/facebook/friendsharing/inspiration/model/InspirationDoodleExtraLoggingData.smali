.class public Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:F

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298042
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298051
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298050
    new-instance v0, LX/86u;

    invoke-direct {v0}, LX/86u;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1298043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298044
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    .line 1298045
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    .line 1298046
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    .line 1298047
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    .line 1298048
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    .line 1298049
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;)V
    .locals 1

    .prologue
    .line 1298035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298036
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    .line 1298037
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    .line 1298038
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    .line 1298039
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    .line 1298040
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    .line 1298041
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;
    .locals 2

    .prologue
    .line 1298034
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1298033
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298052
    if-ne p0, p1, :cond_1

    .line 1298053
    :cond_0
    :goto_0
    return v0

    .line 1298054
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1298055
    goto :goto_0

    .line 1298056
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    .line 1298057
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1298058
    goto :goto_0

    .line 1298059
    :cond_3
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    move v0, v1

    .line 1298060
    goto :goto_0

    .line 1298061
    :cond_4
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1298062
    goto :goto_0

    .line 1298063
    :cond_5
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1298064
    goto :goto_0

    .line 1298065
    :cond_6
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1298066
    goto :goto_0
.end method

.method public getDoodleColorCount()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_color_count"
    .end annotation

    .prologue
    .line 1298032
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    return v0
.end method

.method public getDoodleMaxBrushSize()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_max_brush_size"
    .end annotation

    .prologue
    .line 1298031
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    return v0
.end method

.method public getDoodleSizeCount()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_size_count"
    .end annotation

    .prologue
    .line 1298030
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    return v0
.end method

.method public getDoodleStrokeCount()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_stroke_count"
    .end annotation

    .prologue
    .line 1298021
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    return v0
.end method

.method public getDoodleUndoCount()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "doodle_undo_count"
    .end annotation

    .prologue
    .line 1298029
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1298028
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1298022
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298023
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1298024
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298025
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298026
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298027
    return-void
.end method
