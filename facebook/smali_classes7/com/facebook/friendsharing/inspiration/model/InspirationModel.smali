.class public Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298764
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298765
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298766
    new-instance v0, LX/876;

    invoke-direct {v0}, LX/876;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1298767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298768
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1298769
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    .line 1298770
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    .line 1298771
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    .line 1298772
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1298773
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1298774
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1298775
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    .line 1298776
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    .line 1298777
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1298778
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1298779
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1298780
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    .line 1298781
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1298782
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    .line 1298783
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    .line 1298784
    return-void

    .line 1298785
    :cond_0
    sget-object v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    goto :goto_0

    .line 1298786
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1298787
    :cond_2
    sget-object v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    goto :goto_2

    .line 1298788
    :cond_3
    sget-object v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    goto :goto_3

    .line 1298789
    :cond_4
    sget-object v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    goto :goto_4

    .line 1298790
    :cond_5
    sget-object v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    goto :goto_5

    .line 1298791
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    goto :goto_6
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;)V
    .locals 1

    .prologue
    .line 1298819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298820
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->c:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    .line 1298821
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    .line 1298822
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    .line 1298823
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->f:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1298824
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->g:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    .line 1298825
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    .line 1298826
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->i:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1298827
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->j:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    .line 1298828
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    .line 1298829
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->l:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    .line 1298830
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;
    .locals 2

    .prologue
    .line 1298792
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1298793
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298794
    if-ne p0, p1, :cond_1

    .line 1298795
    :cond_0
    :goto_0
    return v0

    .line 1298796
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1298797
    goto :goto_0

    .line 1298798
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1298799
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1298800
    goto :goto_0

    .line 1298801
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1298802
    goto :goto_0

    .line 1298803
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1298804
    goto :goto_0

    .line 1298805
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1298806
    goto :goto_0

    .line 1298807
    :cond_6
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1298808
    goto :goto_0

    .line 1298809
    :cond_7
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1298810
    goto :goto_0

    .line 1298811
    :cond_8
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1298812
    goto :goto_0

    .line 1298813
    :cond_9
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1298814
    goto :goto_0

    .line 1298815
    :cond_a
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1298816
    goto :goto_0

    .line 1298817
    :cond_b
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1298818
    goto :goto_0
.end method

.method public getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "frame"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298762
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1298763
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getMask()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mask"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298761
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    return-object v0
.end method

.method public getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "particle_effect"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298760
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    return-object v0
.end method

.method public getPromptType()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation

    .prologue
    .line 1298759
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shader_filter"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298758
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    return-object v0
.end method

.method public getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style_transfer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298757
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    return-object v0
.end method

.method public getThumbnailUri()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "thumbnail_uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298756
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackingString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tracking_string"
    .end annotation

    .prologue
    .line 1298755
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1298754
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isLoggingDisabled()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_logging_disabled"
    .end annotation

    .prologue
    .line 1298753
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1298723
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    if-nez v0, :cond_0

    .line 1298724
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298725
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1298726
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298727
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    if-nez v0, :cond_2

    .line 1298728
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298729
    :goto_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    if-nez v0, :cond_3

    .line 1298730
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298731
    :goto_3
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1298732
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-nez v0, :cond_4

    .line 1298733
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298734
    :goto_4
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    if-nez v0, :cond_5

    .line 1298735
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298736
    :goto_5
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1298737
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298738
    :goto_6
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1298739
    return-void

    .line 1298740
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298741
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->a:Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1298742
    goto :goto_1

    .line 1298743
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298744
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->d:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2

    .line 1298745
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298746
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->e:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_3

    .line 1298747
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298748
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->g:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_4

    .line 1298749
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298750
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->h:Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_5

    .line 1298751
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298752
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_6
.end method
