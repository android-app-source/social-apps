.class public Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298945
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1298946
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298947
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298948
    if-nez p0, :cond_0

    .line 1298949
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1298950
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1298951
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;LX/0nX;LX/0my;)V

    .line 1298952
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1298953
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1298954
    const-string v0, "bottom"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getBottom()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298955
    const-string v0, "left"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getLeft()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298956
    const-string v0, "right"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getRight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298957
    const-string v0, "top"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getTop()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298958
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298959
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;LX/0nX;LX/0my;)V

    return-void
.end method
