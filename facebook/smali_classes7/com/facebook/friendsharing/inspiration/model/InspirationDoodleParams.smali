.class public Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:LX/86w;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

.field public final e:F

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298216
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298215
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298214
    new-instance v0, LX/86v;

    invoke-direct {v0}, LX/86v;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1298204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    .line 1298206
    invoke-static {}, LX/86w;->values()[LX/86w;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    .line 1298207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    .line 1298208
    sget-object v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298209
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    .line 1298210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1298211
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    .line 1298212
    :goto_0
    return-void

    .line 1298213
    :cond_0
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;)V
    .locals 1

    .prologue
    .line 1298196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298197
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    .line 1298198
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->d:LX/86w;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/86w;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    .line 1298199
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    .line 1298200
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->f:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1298201
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->g:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    .line 1298202
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->h:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    .line 1298203
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 2

    .prologue
    .line 1298195
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;
    .locals 2

    .prologue
    .line 1298194
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1298193
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298176
    if-ne p0, p1, :cond_1

    .line 1298177
    :cond_0
    :goto_0
    return v0

    .line 1298178
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1298179
    goto :goto_0

    .line 1298180
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 1298181
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1298182
    goto :goto_0

    .line 1298183
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1298184
    goto :goto_0

    .line 1298185
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1298186
    goto :goto_0

    .line 1298187
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1298188
    goto :goto_0

    .line 1298189
    :cond_6
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_7

    move v0, v1

    .line 1298190
    goto :goto_0

    .line 1298191
    :cond_7
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1298192
    goto :goto_0
.end method

.method public getColorSelection()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "color_selection"
    .end annotation

    .prologue
    .line 1298175
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    return v0
.end method

.method public getDrawingMode()LX/86w;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "drawing_mode"
    .end annotation

    .prologue
    .line 1298174
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .prologue
    .line 1298159
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_rect"
    .end annotation

    .prologue
    .line 1298173
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-object v0
.end method

.method public getStrokeWidth()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stroke_width"
    .end annotation

    .prologue
    .line 1298172
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    return v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uri"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1298171
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1298170
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1298160
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298161
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->b:LX/86w;

    invoke-virtual {v0}, LX/86w;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298162
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1298163
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->d:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1298164
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1298165
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1298166
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298167
    :goto_0
    return-void

    .line 1298168
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1298169
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0
.end method
