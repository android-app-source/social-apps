.class public Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298831
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1298832
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298833
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298834
    if-nez p0, :cond_0

    .line 1298835
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1298836
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1298837
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0nX;LX/0my;)V

    .line 1298838
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1298839
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1298840
    const-string v0, "frame"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298841
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298842
    const-string v0, "is_logging_disabled"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->isLoggingDisabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1298843
    const-string v0, "mask"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getMask()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298844
    const-string v0, "particle_effect"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298845
    const-string v0, "prompt_type"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getPromptType()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298846
    const-string v0, "shader_filter"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298847
    const-string v0, "style_transfer"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298848
    const-string v0, "thumbnail_uri"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getThumbnailUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298849
    const-string v0, "tracking_string"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298850
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298851
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationModelSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0nX;LX/0my;)V

    return-void
.end method
