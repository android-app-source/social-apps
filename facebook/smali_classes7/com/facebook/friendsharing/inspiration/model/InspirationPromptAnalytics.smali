.class public Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299029
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299028
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalyticsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299030
    new-instance v0, LX/87B;

    invoke-direct {v0}, LX/87B;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1299031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299032
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    .line 1299033
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    .line 1299034
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    .line 1299035
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;)V
    .locals 1

    .prologue
    .line 1299036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299037
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    .line 1299038
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    .line 1299039
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    .line 1299040
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;
    .locals 2

    .prologue
    .line 1299026
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1299027
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1299015
    if-ne p0, p1, :cond_1

    .line 1299016
    :cond_0
    :goto_0
    return v0

    .line 1299017
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    if-nez v2, :cond_2

    move v0, v1

    .line 1299018
    goto :goto_0

    .line 1299019
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    .line 1299020
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1299021
    goto :goto_0

    .line 1299022
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1299023
    goto :goto_0

    .line 1299024
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1299025
    goto :goto_0
.end method

.method public getPromptId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_id"
    .end annotation

    .prologue
    .line 1299014
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptTrackingString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_tracking_string"
    .end annotation

    .prologue
    .line 1299013
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptType()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation

    .prologue
    .line 1299012
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1299007
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1299008
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299009
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299010
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1299011
    return-void
.end method
