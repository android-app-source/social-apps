.class public Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298591
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1298592
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298593
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298594
    if-nez p0, :cond_0

    .line 1298595
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1298596
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1298597
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;LX/0nX;LX/0my;)V

    .line 1298598
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1298599
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1298600
    const-string v0, "camera_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298601
    const-string v0, "camera_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298602
    const-string v0, "doodle_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298603
    const-string v0, "doodle_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298604
    const-string v0, "editing_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298605
    const-string v0, "editing_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298606
    const-string v0, "effects_tray_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298607
    const-string v0, "effects_tray_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298608
    const-string v0, "gallery_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298609
    const-string v0, "gallery_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298610
    const-string v0, "impression_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getImpressionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298611
    const-string v0, "inspiration_doodle_extra_logging_data"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationDoodleExtraLoggingData()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298612
    const-string v0, "inspiration_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298613
    const-string v0, "logging_surface"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getLoggingSurface()LX/874;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298614
    const-string v0, "media_index"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298615
    const-string v0, "media_source"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1298616
    const-string v0, "nux_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298617
    const-string v0, "nux_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298618
    const-string v0, "share_sheet_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298619
    const-string v0, "share_sheet_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298620
    const-string v0, "text_session_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298621
    const-string v0, "text_session_start_time"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1298622
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298623
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;LX/0nX;LX/0my;)V

    return-void
.end method
