.class public Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1298081
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1298082
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298067
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298075
    if-nez p0, :cond_0

    .line 1298076
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1298077
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1298078
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;LX/0nX;LX/0my;)V

    .line 1298079
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1298080
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1298069
    const-string v0, "doodle_color_count"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleColorCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298070
    const-string v0, "doodle_max_brush_size"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleMaxBrushSize()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1298071
    const-string v0, "doodle_size_count"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleSizeCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298072
    const-string v0, "doodle_stroke_count"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleStrokeCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298073
    const-string v0, "doodle_undo_count"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleUndoCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298074
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1298068
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingDataSerializer;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;LX/0nX;LX/0my;)V

    return-void
.end method
