.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1298887
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298889
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
    .locals 2

    .prologue
    .line 1298890
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;)V

    return-object v0
.end method

.method public setBottom(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bottom"
    .end annotation

    .prologue
    .line 1298891
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a:I

    .line 1298892
    return-object p0
.end method

.method public setLeft(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left"
    .end annotation

    .prologue
    .line 1298893
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->b:I

    .line 1298894
    return-object p0
.end method

.method public setRight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "right"
    .end annotation

    .prologue
    .line 1298895
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->c:I

    .line 1298896
    return-object p0
.end method

.method public setTop(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top"
    .end annotation

    .prologue
    .line 1298897
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->d:I

    .line 1298898
    return-object p0
.end method
