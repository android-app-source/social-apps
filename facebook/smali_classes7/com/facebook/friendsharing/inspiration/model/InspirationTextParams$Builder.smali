.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/photos/creativeediting/model/PersistableRect;


# instance fields
.field public b:I

.field public c:I

.field public d:F

.field public e:F

.field public f:Z

.field public g:F

.field public h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

.field public i:F

.field public j:D

.field public k:Ljava/lang/String;

.field public l:I

.field public m:F

.field public n:F

.field public o:F

.field public p:I

.field public q:Ljava/lang/String;

.field public r:I

.field public s:I

.field public t:F

.field public u:I

.field public v:F

.field public w:Ljava/lang/String;

.field public x:F


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299682
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1299755
    new-instance v0, LX/87L;

    invoke-direct {v0}, LX/87L;-><init>()V

    const/4 v1, 0x0

    .line 1299756
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    move-object v0, v0

    .line 1299757
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1299749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299750
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299751
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->k:Ljava/lang/String;

    .line 1299752
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->q:Ljava/lang/String;

    .line 1299753
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->w:Ljava/lang/String;

    .line 1299754
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)V
    .locals 2

    .prologue
    .line 1299698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299699
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1299700
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    if-eqz v0, :cond_0

    .line 1299701
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 1299702
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->b:I

    .line 1299703
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->b:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->c:I

    .line 1299704
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->c:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->d:F

    .line 1299705
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->d:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->e:F

    .line 1299706
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->e:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->f:Z

    .line 1299707
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->f:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->g:F

    .line 1299708
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->g:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299709
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->h:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->i:F

    .line 1299710
    iget-wide v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->i:D

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->j:D

    .line 1299711
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->k:Ljava/lang/String;

    .line 1299712
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->k:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->l:I

    .line 1299713
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->l:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->m:F

    .line 1299714
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->m:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->n:F

    .line 1299715
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->n:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->o:F

    .line 1299716
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->o:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->p:I

    .line 1299717
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->q:Ljava/lang/String;

    .line 1299718
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->q:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->r:I

    .line 1299719
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->r:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->s:I

    .line 1299720
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->s:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->t:F

    .line 1299721
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->t:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->u:I

    .line 1299722
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->u:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->v:F

    .line 1299723
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->w:Ljava/lang/String;

    .line 1299724
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->w:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->x:F

    .line 1299725
    :goto_0
    return-void

    .line 1299726
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderAlpha()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->b:I

    .line 1299727
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderColor()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->c:I

    .line 1299728
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getBorderWidth()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->d:F

    .line 1299729
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getHeightPercentage()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->e:F

    .line 1299730
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->isKeyboardOpen()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->f:Z

    .line 1299731
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getLeftPercentage()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->g:F

    .line 1299732
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getMediaRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299733
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getRotation()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->i:F

    .line 1299734
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getScaleFactor()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->j:D

    .line 1299735
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->k:Ljava/lang/String;

    .line 1299736
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowColor()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->l:I

    .line 1299737
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDX()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->m:F

    .line 1299738
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowDY()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->n:F

    .line 1299739
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getShadowRadius()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->o:F

    .line 1299740
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getSizeMultiplier()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->p:I

    .line 1299741
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->q:Ljava/lang/String;

    .line 1299742
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextColor()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->r:I

    .line 1299743
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->s:I

    .line 1299744
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->t:F

    .line 1299745
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTextWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->u:I

    .line 1299746
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTopPercentage()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->v:F

    .line 1299747
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getTypeface()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->w:Ljava/lang/String;

    .line 1299748
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getWidthPercentage()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->x:F

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .locals 2

    .prologue
    .line 1299697
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;)V

    return-object v0
.end method

.method public setBorderAlpha(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_alpha"
    .end annotation

    .prologue
    .line 1299695
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->b:I

    .line 1299696
    return-object p0
.end method

.method public setBorderColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_color"
    .end annotation

    .prologue
    .line 1299693
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->c:I

    .line 1299694
    return-object p0
.end method

.method public setBorderWidth(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "border_width"
    .end annotation

    .prologue
    .line 1299691
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->d:F

    .line 1299692
    return-object p0
.end method

.method public setHeightPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height_percentage"
    .end annotation

    .prologue
    .line 1299689
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->e:F

    .line 1299690
    return-object p0
.end method

.method public setIsKeyboardOpen(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_keyboard_open"
    .end annotation

    .prologue
    .line 1299687
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->f:Z

    .line 1299688
    return-object p0
.end method

.method public setLeftPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "left_percentage"
    .end annotation

    .prologue
    .line 1299685
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->g:F

    .line 1299686
    return-object p0
.end method

.method public setMediaRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_rect"
    .end annotation

    .prologue
    .line 1299683
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->h:Lcom/facebook/photos/creativeediting/model/PersistableRect;

    .line 1299684
    return-object p0
.end method

.method public setRotation(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation"
    .end annotation

    .prologue
    .line 1299680
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->i:F

    .line 1299681
    return-object p0
.end method

.method public setScaleFactor(D)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "scale_factor"
    .end annotation

    .prologue
    .line 1299758
    iput-wide p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->j:D

    .line 1299759
    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 1299678
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->k:Ljava/lang/String;

    .line 1299679
    return-object p0
.end method

.method public setShadowColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_color"
    .end annotation

    .prologue
    .line 1299676
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->l:I

    .line 1299677
    return-object p0
.end method

.method public setShadowDX(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_d_x"
    .end annotation

    .prologue
    .line 1299652
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->m:F

    .line 1299653
    return-object p0
.end method

.method public setShadowDY(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_d_y"
    .end annotation

    .prologue
    .line 1299674
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->n:F

    .line 1299675
    return-object p0
.end method

.method public setShadowRadius(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow_radius"
    .end annotation

    .prologue
    .line 1299672
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->o:F

    .line 1299673
    return-object p0
.end method

.method public setSizeMultiplier(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size_multiplier"
    .end annotation

    .prologue
    .line 1299670
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->p:I

    .line 1299671
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation

    .prologue
    .line 1299668
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->q:Ljava/lang/String;

    .line 1299669
    return-object p0
.end method

.method public setTextColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_color"
    .end annotation

    .prologue
    .line 1299666
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->r:I

    .line 1299667
    return-object p0
.end method

.method public setTextHeight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_height"
    .end annotation

    .prologue
    .line 1299664
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->s:I

    .line 1299665
    return-object p0
.end method

.method public setTextSize(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_size"
    .end annotation

    .prologue
    .line 1299662
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->t:F

    .line 1299663
    return-object p0
.end method

.method public setTextWidth(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_width"
    .end annotation

    .prologue
    .line 1299660
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->u:I

    .line 1299661
    return-object p0
.end method

.method public setTopPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "top_percentage"
    .end annotation

    .prologue
    .line 1299658
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->v:F

    .line 1299659
    return-object p0
.end method

.method public setTypeface(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "typeface"
    .end annotation

    .prologue
    .line 1299656
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->w:Ljava/lang/String;

    .line 1299657
    return-object p0
.end method

.method public setWidthPercentage(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width_percentage"
    .end annotation

    .prologue
    .line 1299654
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->x:F

    .line 1299655
    return-object p0
.end method
