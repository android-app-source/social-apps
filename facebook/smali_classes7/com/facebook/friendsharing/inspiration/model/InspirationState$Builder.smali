.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationState_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/870;

.field private static final b:LX/86o;


# instance fields
.field public c:LX/870;

.field public d:Z

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/86t;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/86o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299262
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299256
    new-instance v0, LX/87F;

    invoke-direct {v0}, LX/87F;-><init>()V

    .line 1299257
    sget-object v0, LX/870;->NO_FORMAT_IN_PROCESS:LX/870;

    move-object v0, v0

    .line 1299258
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a:LX/870;

    .line 1299259
    new-instance v0, LX/87G;

    invoke-direct {v0}, LX/87G;-><init>()V

    .line 1299260
    sget-object v0, LX/86o;->NONE:LX/86o;

    move-object v0, v0

    .line 1299261
    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->b:LX/86o;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1299250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299251
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a:LX/870;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->c:LX/870;

    .line 1299252
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1299253
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->e:LX/0Px;

    .line 1299254
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->b:LX/86o;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->o:LX/86o;

    .line 1299255
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)V
    .locals 1

    .prologue
    .line 1299215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299216
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1299217
    instance-of v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    if-eqz v0, :cond_0

    .line 1299218
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 1299219
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a:LX/870;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->c:LX/870;

    .line 1299220
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->b:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->d:Z

    .line 1299221
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->e:LX/0Px;

    .line 1299222
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->d:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->f:Z

    .line 1299223
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->e:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->g:Z

    .line 1299224
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->f:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->h:Z

    .line 1299225
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->g:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->i:Z

    .line 1299226
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->h:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->j:Z

    .line 1299227
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->i:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->k:Z

    .line 1299228
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->j:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->l:Z

    .line 1299229
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->k:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->m:Z

    .line 1299230
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->l:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->n:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1299231
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->m:LX/86o;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->o:LX/86o;

    .line 1299232
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->n:Z

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->p:Z

    .line 1299233
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->o:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->q:I

    .line 1299234
    :goto_0
    return-void

    .line 1299235
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->c:LX/870;

    .line 1299236
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->hasChangedInspiration()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->d:Z

    .line 1299237
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->e:LX/0Px;

    .line 1299238
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBackPressed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->f:Z

    .line 1299239
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBottomTrayTransitioning()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->g:Z

    .line 1299240
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->h:Z

    .line 1299241
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->i:Z

    .line 1299242
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->j:Z

    .line 1299243
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->k:Z

    .line 1299244
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isRecordingAtLimit()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->l:Z

    .line 1299245
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->m:Z

    .line 1299246
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getLocation()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->n:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1299247
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->o:LX/86o;

    .line 1299248
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->shouldRefreshCameraRoll()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->p:Z

    .line 1299249
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getTextColorSelection()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->q:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;
    .locals 2

    .prologue
    .line 1299214
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;)V

    return-object v0
.end method

.method public setFormatMode(LX/870;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "format_mode"
    .end annotation

    .prologue
    .line 1299212
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->c:LX/870;

    .line 1299213
    return-object p0
.end method

.method public setHasChangedInspiration(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_changed_inspiration"
    .end annotation

    .prologue
    .line 1299210
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->d:Z

    .line 1299211
    return-object p0
.end method

.method public setInspirationBackStack(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_back_stack"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/86t;",
            ">;)",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;"
        }
    .end annotation

    .prologue
    .line 1299208
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->e:LX/0Px;

    .line 1299209
    return-object p0
.end method

.method public setIsBackPressed(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_back_pressed"
    .end annotation

    .prologue
    .line 1299206
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->f:Z

    .line 1299207
    return-object p0
.end method

.method public setIsBottomTrayTransitioning(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_bottom_tray_transitioning"
    .end annotation

    .prologue
    .line 1299184
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->g:Z

    .line 1299185
    return-object p0
.end method

.method public setIsInNuxMode(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_in_nux_mode"
    .end annotation

    .prologue
    .line 1299204
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->h:Z

    .line 1299205
    return-object p0
.end method

.method public setIsLocationFetchAndRequeryingEffectsInProgress(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_location_fetch_and_requerying_effects_in_progress"
    .end annotation

    .prologue
    .line 1299202
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->i:Z

    .line 1299203
    return-object p0
.end method

.method public setIsMirrorOn(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_mirror_on"
    .end annotation

    .prologue
    .line 1299200
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->j:Z

    .line 1299201
    return-object p0
.end method

.method public setIsMuted(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_muted"
    .end annotation

    .prologue
    .line 1299198
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->k:Z

    .line 1299199
    return-object p0
.end method

.method public setIsRecordingAtLimit(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_recording_at_limit"
    .end annotation

    .prologue
    .line 1299196
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->l:Z

    .line 1299197
    return-object p0
.end method

.method public setIsReleaseVideoPlayerRequested(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_release_video_player_requested"
    .end annotation

    .prologue
    .line 1299194
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->m:Z

    .line 1299195
    return-object p0
.end method

.method public setLocation(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerLocation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location"
    .end annotation

    .prologue
    .line 1299192
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->n:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1299193
    return-object p0
.end method

.method public setOpenBottomTray(LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .param p1    # LX/86o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "open_bottom_tray"
    .end annotation

    .prologue
    .line 1299190
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->o:LX/86o;

    .line 1299191
    return-object p0
.end method

.method public setShouldRefreshCameraRoll(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_refresh_camera_roll"
    .end annotation

    .prologue
    .line 1299188
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->p:Z

    .line 1299189
    return-object p0
.end method

.method public setTextColorSelection(I)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_color_selection"
    .end annotation

    .prologue
    .line 1299186
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->q:I

    .line 1299187
    return-object p0
.end method
