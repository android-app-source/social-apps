.class public Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299141
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299143
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModelSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299142
    new-instance v0, LX/87C;

    invoke-direct {v0}, LX/87C;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1299127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    .line 1299129
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1299130
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 1299131
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1299132
    aput-object v0, v2, v1

    .line 1299133
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1299134
    :cond_0
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    .line 1299135
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;)V
    .locals 1

    .prologue
    .line 1299137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299138
    iget v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    .line 1299139
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    .line 1299140
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;
    .locals 2

    .prologue
    .line 1299136
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;
    .locals 2

    .prologue
    .line 1299144
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1299108
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1299109
    if-ne p0, p1, :cond_1

    .line 1299110
    :cond_0
    :goto_0
    return v0

    .line 1299111
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1299112
    goto :goto_0

    .line 1299113
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 1299114
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    iget v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1299115
    goto :goto_0

    .line 1299116
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1299117
    goto :goto_0
.end method

.method public getDefaultInspirationLandingIndex()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "default_inspiration_landing_index"
    .end annotation

    .prologue
    .line 1299118
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    return v0
.end method

.method public getInspirationModels()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_models"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1299119
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1299120
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1299121
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299122
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299123
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1299124
    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1299125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1299126
    :cond_0
    return-void
.end method
