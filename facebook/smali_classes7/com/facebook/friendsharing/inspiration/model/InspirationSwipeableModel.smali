.class public Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299569
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299570
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1299571
    new-instance v0, LX/87I;

    invoke-direct {v0}, LX/87I;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 1299582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299583
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    .line 1299584
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1299585
    iput-object v5, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    .line 1299586
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    .line 1299587
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1299588
    iput-object v5, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    .line 1299589
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1299590
    iput-object v5, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    .line 1299591
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1299592
    iput-object v5, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    .line 1299593
    :goto_5
    return-void

    :cond_0
    move v0, v2

    .line 1299594
    goto :goto_0

    .line 1299595
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move v3, v2

    .line 1299596
    :goto_6
    array-length v0, v4

    if-ge v3, v0, :cond_2

    .line 1299597
    sget-object v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1299598
    aput-object v0, v4, v3

    .line 1299599
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 1299600
    :cond_2
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1299601
    goto :goto_2

    .line 1299602
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    goto :goto_3

    .line 1299603
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    goto :goto_4

    .line 1299604
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    goto :goto_5
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;)V
    .locals 1

    .prologue
    .line 1299572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299573
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    .line 1299574
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    .line 1299575
    iget-boolean v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    .line 1299576
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    .line 1299577
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    .line 1299578
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    .line 1299579
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 2

    .prologue
    .line 1299580
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;
    .locals 2

    .prologue
    .line 1299581
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public areEffectsLoading()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "are_effects_loading"
    .end annotation

    .prologue
    .line 1299567
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1299568
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1299550
    if-ne p0, p1, :cond_1

    .line 1299551
    :cond_0
    :goto_0
    return v0

    .line 1299552
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1299553
    goto :goto_0

    .line 1299554
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 1299555
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1299556
    goto :goto_0

    .line 1299557
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1299558
    goto :goto_0

    .line 1299559
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    iget-boolean v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1299560
    goto :goto_0

    .line 1299561
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1299562
    goto :goto_0

    .line 1299563
    :cond_6
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1299564
    goto :goto_0

    .line 1299565
    :cond_7
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1299566
    goto :goto_0
.end method

.method public getInspirationModels()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inspiration_models"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299549
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    return-object v0
.end method

.method public getLastSelectedPreCaptureId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_selected_pre_capture_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299548
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299547
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getStarId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "star_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1299546
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1299545
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isFromTray()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_from_tray"
    .end annotation

    .prologue
    .line 1299544
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1299520
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299521
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    if-nez v0, :cond_2

    .line 1299522
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299523
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299524
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1299525
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299526
    :goto_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1299527
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299528
    :goto_3
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1299529
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299530
    :goto_4
    return-void

    :cond_1
    move v0, v2

    .line 1299531
    goto :goto_0

    .line 1299532
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299533
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299534
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1299535
    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1299536
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_3
    move v0, v2

    .line 1299537
    goto :goto_1

    .line 1299538
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299539
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 1299540
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299541
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 1299542
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1299543
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4
.end method
