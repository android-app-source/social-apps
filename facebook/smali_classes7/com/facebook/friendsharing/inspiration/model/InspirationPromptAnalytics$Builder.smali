.class public final Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1299000
    const-class v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1298995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1298996
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->a:Ljava/lang/String;

    .line 1298997
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->b:Ljava/lang/String;

    .line 1298998
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->c:Ljava/lang/String;

    .line 1298999
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;
    .locals 2

    .prologue
    .line 1298994
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;)V

    return-object v0
.end method

.method public setPrompt_id(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_id"
    .end annotation

    .prologue
    .line 1298992
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->a:Ljava/lang/String;

    .line 1298993
    return-object p0
.end method

.method public setPrompt_tracking_string(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_tracking_string"
    .end annotation

    .prologue
    .line 1298990
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->b:Ljava/lang/String;

    .line 1298991
    return-object p0
.end method

.method public setPrompt_type(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_type"
    .end annotation

    .prologue
    .line 1298988
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics$Builder;->c:Ljava/lang/String;

    .line 1298989
    return-object p0
.end method
