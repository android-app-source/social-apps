.class public Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;
.super LX/0Yd;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0YZ;

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0YZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1170853
    const-class v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;

    sput-object v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->a:Ljava/lang/Class;

    .line 1170854
    new-instance v0, LX/76E;

    invoke-direct {v0}, LX/76E;-><init>()V

    sput-object v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->b:LX/0YZ;

    .line 1170855
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1170856
    sput-object v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->c:Ljava/util/Map;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    sget-object v2, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->b:LX/0YZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1170857
    sget-object v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->c:Ljava/util/Map;

    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    sget-object v2, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->b:LX/0YZ;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1170858
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1170859
    sget-object v0, Lcom/facebook/push/mqtt/receiver/BootCompleteBroadcastReceiver;->c:Ljava/util/Map;

    invoke-direct {p0, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    .line 1170860
    return-void
.end method
