.class public Lcom/facebook/push/mqtt/service/MqttPushHelperService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/29A;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1170873
    const-class v0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;

    sput-object v0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1170874
    sget-object v0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1170875
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1170876
    iput-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->b:LX/0Ot;

    .line 1170877
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1170878
    iput-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->c:LX/0Ot;

    .line 1170879
    return-void
.end method

.method private static a(Lcom/facebook/push/mqtt/service/MqttPushHelperService;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/push/mqtt/service/MqttPushHelperService;",
            "LX/0Ot",
            "<",
            "LX/29A;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1170880
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->c:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;

    const/16 v1, 0x1020

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x29f

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->a(Lcom/facebook/push/mqtt/service/MqttPushHelperService;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x22c4ff99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1170881
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 1170882
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/29A;

    invoke-virtual {v0}, LX/29A;->init()V

    .line 1170883
    const/16 v0, 0x25

    const v2, 0x2c3557a2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x687b503b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1170884
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1170885
    invoke-static {p0, p0}, Lcom/facebook/push/mqtt/service/MqttPushHelperService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1170886
    const/16 v1, 0x25

    const v2, -0x68c8290e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
