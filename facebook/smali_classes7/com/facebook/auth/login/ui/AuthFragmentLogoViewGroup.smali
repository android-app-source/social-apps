.class public abstract Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;
.super Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/7hV;",
        ">",
        "Lcom/facebook/auth/login/ui/AuthFragmentViewGroup",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final mBottomGroup:Landroid/view/View;

.field public mDynamicLayoutUtil:LX/46x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final mEnterTransitionAnimation:I

.field private final mExitTransitionAnimation:I

.field public final mHelpButton:Landroid/widget/ImageButton;

.field public final mHideLogoOnSmallDisplays:Z

.field private final mLayoutDelegate:LX/7hX;

.field public final mLogo1ResId:I

.field public final mLogo1View:Landroid/widget/ImageView;

.field public final mLogo2ResId:I

.field public final mLogo2View:Landroid/widget/ImageView;

.field public final mLogoGroup:Landroid/view/View;

.field public final mMainGroup:Landroid/view/View;

.field private final mPopEnterTransitionAnimation:I

.field private final mPopExitTransitionAnimation:I

.field public final mRootGroup:Landroid/view/View;

.field public final mSplashGroup:Landroid/view/View;

.field public mSplashLogo1ResId:I

.field public final mSplashLogo1View:Landroid/widget/ImageView;

.field public mSplashLogo2ResId:I

.field public final mSplashLogo2View:Landroid/widget/ImageView;


# direct methods
.method public static $ul_injectMe(Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;LX/46x;)V
    .locals 0

    .prologue
    .line 1226413
    iput-object p1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mDynamicLayoutUtil:LX/46x;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7hV;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1226345
    invoke-direct {p0, p1, p2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;-><init>(Landroid/content/Context;LX/7hV;)V

    .line 1226346
    const-class v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-static {v0, p0}, LX/0QA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1226347
    const-string v0, "orca:authparam:layout_resource"

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getDefaultLayoutResource()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1226348
    const v0, 0x7f0d10b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashGroup:Landroid/view/View;

    .line 1226349
    const v0, 0x7f0d20ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1View:Landroid/widget/ImageView;

    .line 1226350
    const v0, 0x7f0d20ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2View:Landroid/widget/ImageView;

    .line 1226351
    const v0, 0x7f0d108e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mRootGroup:Landroid/view/View;

    .line 1226352
    const v0, 0x7f0d10a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mMainGroup:Landroid/view/View;

    .line 1226353
    const v0, 0x7f0d10b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mBottomGroup:Landroid/view/View;

    .line 1226354
    const v0, 0x7f0d20ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogoGroup:Landroid/view/View;

    .line 1226355
    const v0, 0x7f0d20af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1View:Landroid/widget/ImageView;

    .line 1226356
    const v0, 0x7f0d20b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2View:Landroid/widget/ImageView;

    .line 1226357
    const v0, 0x7f0d20b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHelpButton:Landroid/widget/ImageButton;

    .line 1226358
    const-string v0, "orca:authparam:logo1"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1ResId:I

    .line 1226359
    const-string v0, "orca:authparam:logo2"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2ResId:I

    .line 1226360
    const-string v0, "orca:authparam:spash_logo1"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1ResId:I

    .line 1226361
    const-string v0, "orca:authparam:spash_logo2"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2ResId:I

    .line 1226362
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1ResId:I

    if-nez v0, :cond_0

    .line 1226363
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1ResId:I

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1ResId:I

    .line 1226364
    :cond_0
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2ResId:I

    if-nez v0, :cond_1

    .line 1226365
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2ResId:I

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2ResId:I

    .line 1226366
    :cond_1
    const-string v0, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mEnterTransitionAnimation:I

    .line 1226367
    const-string v0, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mExitTransitionAnimation:I

    .line 1226368
    const-string v0, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mPopEnterTransitionAnimation:I

    .line 1226369
    const-string v0, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mPopExitTransitionAnimation:I

    .line 1226370
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1226371
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orca:authparam:hide_logo"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHideLogoOnSmallDisplays:Z

    .line 1226372
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHideLogoOnSmallDisplays:Z

    if-eqz v3, :cond_2

    .line 1226373
    iget-object v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mDynamicLayoutUtil:LX/46x;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const v6, 0x7f0d20ae

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/46x;->a(Landroid/view/View;ILjava/util/List;)V

    .line 1226374
    iget-object v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mDynamicLayoutUtil:LX/46x;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const v6, 0x7f0d02c4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    const v7, 0x7f0b013f

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    const v8, 0x7f0b013e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v8}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/46x;->a(Landroid/view/View;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1226375
    :cond_2
    const/16 v5, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1226376
    new-array v3, v5, [Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashGroup:Landroid/view/View;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1View:Landroid/widget/ImageView;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2View:Landroid/widget/ImageView;

    aput-object v4, v3, v2

    const/4 v2, 0x3

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mRootGroup:Landroid/view/View;

    aput-object v4, v3, v2

    const/4 v2, 0x4

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mMainGroup:Landroid/view/View;

    aput-object v4, v3, v2

    const/4 v2, 0x5

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mBottomGroup:Landroid/view/View;

    aput-object v4, v3, v2

    const/4 v2, 0x6

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogoGroup:Landroid/view/View;

    aput-object v4, v3, v2

    const/4 v2, 0x7

    iget-object v4, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHelpButton:Landroid/widget/ImageButton;

    aput-object v4, v3, v2

    move v2, v1

    .line 1226377
    :goto_1
    if-ge v2, v5, :cond_6

    aget-object v4, v3, v2

    .line 1226378
    if-nez v4, :cond_5

    .line 1226379
    :goto_2
    move v0, v0

    .line 1226380
    if-eqz v0, :cond_4

    .line 1226381
    new-instance v0, LX/7hY;

    invoke-direct {v0, p0}, LX/7hY;-><init>(Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;)V

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLayoutDelegate:LX/7hX;

    .line 1226382
    :goto_3
    return-void

    .line 1226383
    :cond_3
    iput-boolean v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHideLogoOnSmallDisplays:Z

    goto/16 :goto_0

    .line 1226384
    :cond_4
    new-instance v0, LX/7hb;

    invoke-direct {v0, p0}, LX/7hb;-><init>(Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;)V

    iput-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLayoutDelegate:LX/7hX;

    goto :goto_3

    .line 1226385
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1226386
    goto :goto_2
.end method

.method public static createParameterBundle(III)Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1226412
    sget-object v5, LX/7hc;->NONE:LX/7hc;

    const/4 v6, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    invoke-static/range {v0 .. v6}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->createParameterBundle(IIIIILX/7hc;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static createParameterBundle(IIIIILX/7hc;IIIIZLjava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1226398
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1226399
    const-string v1, "orca:authparam:layout_resource"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226400
    const-string v1, "orca:authparam:logo1"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226401
    const-string v1, "orca:authparam:logo2"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226402
    const-string v1, "orca:authparam:spash_logo1"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226403
    const-string v1, "orca:authparam:spash_logo2"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226404
    const-string v1, "orca:authparam:splash_transition"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1226405
    const-string v1, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226406
    const-string v1, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226407
    const-string v1, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226408
    const-string v1, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {v0, v1, p9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226409
    const-string v1, "orca:authparam:help_url"

    invoke-virtual {v0, v1, p11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226410
    const-string v1, "orca:authparam:hide_logo"

    invoke-virtual {v0, v1, p10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1226411
    return-object v0
.end method

.method public static createParameterBundle(IIIIILX/7hc;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 12

    .prologue
    .line 1226397
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v11, p6

    invoke-static/range {v0 .. v11}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->createParameterBundle(IIIIILX/7hc;IIIIZLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static createParameterBundle(IIILjava/lang/String;)Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1226396
    sget-object v5, LX/7hc;->NONE:LX/7hc;

    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->createParameterBundle(IIIIILX/7hc;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static createParameterBundle(IIIZ)Landroid/os/Bundle;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 1226395
    sget-object v5, LX/7hc;->NONE:LX/7hc;

    const/4 v11, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    move v10, p3

    invoke-static/range {v0 .. v11}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->createParameterBundle(IIIIILX/7hc;IIIIZLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract getDefaultLayoutResource()I
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6b5312fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1226392
    invoke-super {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->onAttachedToWindow()V

    .line 1226393
    iget-object v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLayoutDelegate:LX/7hX;

    invoke-interface {v1}, LX/7hX;->a()V

    .line 1226394
    const/16 v1, 0x2d

    const v2, -0x237e2fae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1226389
    invoke-super {p0, p1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1226390
    iget-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLayoutDelegate:LX/7hX;

    invoke-interface {v0}, LX/7hX;->b()V

    .line 1226391
    return-void
.end method

.method public setCustomAnimations(LX/42p;)V
    .locals 4

    .prologue
    .line 1226387
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mEnterTransitionAnimation:I

    iget v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mExitTransitionAnimation:I

    iget v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mPopEnterTransitionAnimation:I

    iget v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mPopExitTransitionAnimation:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/42p;->a(IIII)LX/42p;

    .line 1226388
    return-void
.end method
