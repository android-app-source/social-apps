.class public Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;
.super Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup",
        "<",
        "LX/7he;",
        ">;"
    }
.end annotation


# instance fields
.field private final loginButton:Landroid/widget/Button;

.field private final loginText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7he;)V
    .locals 2

    .prologue
    .line 1226434
    invoke-direct {p0, p1, p2}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;-><init>(Landroid/content/Context;LX/7hV;)V

    .line 1226435
    const v0, 0x7f0d20b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginButton:Landroid/widget/Button;

    .line 1226436
    const v0, 0x7f0d20b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginText:Landroid/widget/TextView;

    .line 1226437
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1226438
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginButton:Landroid/widget/Button;

    new-instance v1, LX/7hf;

    invoke-direct {v1, p0}, LX/7hf;-><init>(Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226439
    return-void
.end method


# virtual methods
.method public getDefaultLayoutResource()I
    .locals 1

    .prologue
    .line 1226440
    const v0, 0x7f030d20

    return v0
.end method

.method public onSsoFailure(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0
    .param p1    # Lcom/facebook/fbservice/service/ServiceException;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226441
    return-void
.end method

.method public onSsoSuccess()V
    .locals 0

    .prologue
    .line 1226442
    return-void
.end method

.method public setSsoSessionInfo(LX/3dh;)V
    .locals 6

    .prologue
    const/16 v5, 0x21

    .line 1226443
    iget-object v0, p1, LX/3dh;->b:Ljava/lang/String;

    .line 1226444
    const/16 v1, 0x20

    const/16 v2, 0xa0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 1226445
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1226446
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1226447
    const v3, 0x7f080157

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1226448
    const-string v3, "[[name]]"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1226449
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginButton:Landroid/widget/Button;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1226450
    new-instance v0, LX/63A;

    invoke-direct {v0}, LX/63A;-><init>()V

    .line 1226451
    new-instance v2, LX/7hg;

    invoke-direct {v2, p0}, LX/7hg;-><init>(Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;)V

    .line 1226452
    iput-object v2, v0, LX/63A;->a:LX/639;

    .line 1226453
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1226454
    invoke-virtual {v2, v0, v5}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 1226455
    const v0, 0x7f080158

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1226456
    invoke-virtual {v2}, LX/47x;->a()LX/47x;

    .line 1226457
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginText:Landroid/widget/TextView;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226458
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericFirstPartySsoViewGroup;->loginText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSaveEnabled(Z)V

    .line 1226459
    return-void
.end method
