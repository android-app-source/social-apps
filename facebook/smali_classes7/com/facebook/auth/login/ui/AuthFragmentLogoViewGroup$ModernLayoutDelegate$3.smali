.class public final Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:LX/7hb;


# direct methods
.method public constructor <init>(LX/7hb;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1226245
    iput-object p1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iput-object p2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1f4

    .line 1226246
    iget-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v1, v1, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v1, v1, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1View:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v2, v2, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1View:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, LX/7hb;->a(LX/7hb;Landroid/view/View;Landroid/view/View;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    .line 1226247
    iget-object v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v2, v2, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2View:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v3, v3, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v3, v3, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2View:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3}, LX/7hb;->a(LX/7hb;Landroid/view/View;Landroid/view/View;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    .line 1226248
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 1226249
    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 1226250
    new-instance v2, LX/7ha;

    invoke-direct {v2, p0}, LX/7ha;-><init>(Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1226251
    iget-object v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v2, v2, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1View:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1226252
    iget-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v0, v0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2View:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1226253
    iget-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->b:LX/7hb;

    iget-object v0, v0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x177

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1226254
    return-void
.end method
