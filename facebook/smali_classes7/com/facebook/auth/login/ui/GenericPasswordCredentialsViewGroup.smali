.class public Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;
.super Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup",
        "<",
        "LX/7hm;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final CALLER_CONTEXT:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final emailText:Landroid/widget/TextView;

.field private final loginButton:Landroid/widget/Button;

.field public mPasswordCredentialsViewGroupHelper:LX/7hs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final notYouLink:Landroid/widget/TextView;

.field private final passwordText:Landroid/widget/TextView;

.field public final signupButton:Landroid/widget/Button;

.field public final userName:Landroid/widget/TextView;

.field public final userPhoto:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static $ul_injectMe(Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;LX/7hs;)V
    .locals 0

    .prologue
    .line 1226568
    iput-object p1, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->mPasswordCredentialsViewGroupHelper:LX/7hs;

    return-void
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226569
    const-class v0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->CALLER_CONTEXT:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7hm;)V
    .locals 8

    .prologue
    .line 1226504
    invoke-direct {p0, p1, p2}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;-><init>(Landroid/content/Context;LX/7hV;)V

    .line 1226505
    const v0, 0x7f0d20b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userPhoto:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1226506
    const v0, 0x7f0d1ab8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userName:Landroid/widget/TextView;

    .line 1226507
    const v0, 0x7f0d20b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    .line 1226508
    const v0, 0x7f0d20b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    .line 1226509
    const v0, 0x7f0d0c15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->passwordText:Landroid/widget/TextView;

    .line 1226510
    const v0, 0x7f0d20b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->loginButton:Landroid/widget/Button;

    .line 1226511
    const v0, 0x7f0d20b5

    invoke-virtual {p0, v0}, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    .line 1226512
    const-class v0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;

    invoke-static {v0, p0}, LX/0QA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1226513
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->mPasswordCredentialsViewGroupHelper:LX/7hs;

    iget-object v3, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->passwordText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->loginButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p2

    .line 1226514
    iput-object v1, v0, LX/7hs;->h:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    .line 1226515
    iput-object v2, v0, LX/7hs;->i:LX/7hm;

    .line 1226516
    iput-object v3, v0, LX/7hs;->j:Landroid/widget/TextView;

    .line 1226517
    iput-object v4, v0, LX/7hs;->k:Landroid/widget/TextView;

    .line 1226518
    iput-object v5, v0, LX/7hs;->l:Landroid/view/View;

    .line 1226519
    iput-object v6, v0, LX/7hs;->m:Landroid/widget/Button;

    .line 1226520
    iput-object v7, v0, LX/7hs;->n:LX/7hr;

    .line 1226521
    invoke-static {v0}, LX/7hs;->c(LX/7hs;)V

    .line 1226522
    new-instance p1, LX/7hn;

    invoke-direct {p1, v0}, LX/7hn;-><init>(LX/7hs;)V

    .line 1226523
    iget-object p2, v0, LX/7hs;->j:Landroid/widget/TextView;

    instance-of p2, p2, Landroid/widget/AutoCompleteTextView;

    if-eqz p2, :cond_3

    .line 1226524
    iget-object p2, v0, LX/7hs;->j:Landroid/widget/TextView;

    check-cast p2, Landroid/widget/AutoCompleteTextView;

    .line 1226525
    invoke-static {}, LX/0RA;->d()Ljava/util/TreeSet;

    move-result-object v2

    .line 1226526
    iget-object v1, v0, LX/7hs;->d:Landroid/content/pm/PackageManager;

    const-string v3, "android.permission.READ_PHONE_STATE"

    iget-object v4, v0, LX/7hs;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/7hs;->f:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    .line 1226527
    iget-object v1, v0, LX/7hs;->f:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    .line 1226528
    if-eqz v1, :cond_0

    sget-object v3, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1226529
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1226530
    :cond_0
    iget-object v1, v0, LX/7hs;->d:Landroid/content/pm/PackageManager;

    const-string v3, "android.permission.GET_ACCOUNTS"

    iget-object v4, v0, LX/7hs;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, LX/7hs;->e:Landroid/accounts/AccountManager;

    if-eqz v1, :cond_2

    .line 1226531
    iget-object v1, v0, LX/7hs;->e:Landroid/accounts/AccountManager;

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 1226532
    sget-object v6, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v7, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1226533
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1226534
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1226535
    :cond_2
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p2}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x109000a

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v3, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 1226536
    invoke-virtual {p2, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1226537
    :cond_3
    iget-object p2, v0, LX/7hs;->j:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1226538
    iget-object p2, v0, LX/7hs;->k:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1226539
    iget-object p1, v0, LX/7hs;->l:Landroid/view/View;

    new-instance p2, LX/7ho;

    invoke-direct {p2, v0}, LX/7ho;-><init>(LX/7hs;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226540
    iget-object p1, v0, LX/7hs;->m:Landroid/widget/Button;

    if-eqz p1, :cond_4

    .line 1226541
    iget-object p1, v0, LX/7hs;->m:Landroid/widget/Button;

    new-instance p2, LX/7hp;

    invoke-direct {p2, v0}, LX/7hp;-><init>(LX/7hs;)V

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226542
    :cond_4
    iget-object p1, v0, LX/7hs;->k:Landroid/widget/TextView;

    new-instance p2, LX/7hq;

    invoke-direct {p2, v0}, LX/7hq;-><init>(LX/7hs;)V

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1226543
    iget-object p1, v0, LX/7hs;->k:Landroid/widget/TextView;

    sget-object p2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1226544
    new-instance v0, LX/63A;

    invoke-direct {v0}, LX/63A;-><init>()V

    .line 1226545
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1226546
    new-instance v2, LX/47x;

    invoke-direct {v2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1226547
    const/16 v3, 0x21

    invoke-virtual {v2, v0, v3}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 1226548
    const v0, 0x7f080158

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1226549
    invoke-virtual {v2}, LX/47x;->a()LX/47x;

    .line 1226550
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226551
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSaveEnabled(Z)V

    .line 1226552
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    new-instance v1, LX/7hj;

    invoke-direct {v1, p0}, LX/7hj;-><init>(Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226553
    return-void
.end method


# virtual methods
.method public getDefaultLayoutResource()I
    .locals 1

    .prologue
    .line 1226567
    const v0, 0x7f030d1c

    return v0
.end method

.method public onAuthFailure(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0
    .param p1    # Lcom/facebook/fbservice/service/ServiceException;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226566
    return-void
.end method

.method public onAuthSuccess()V
    .locals 0

    .prologue
    .line 1226565
    return-void
.end method

.method public setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1226554
    invoke-static {p4}, LX/0PB;->checkState(Z)V

    .line 1226555
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226556
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226557
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userPhoto:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->CALLER_CONTEXT:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1226558
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userPhoto:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1226559
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userName:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226560
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userName:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226561
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226562
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1226563
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1226564
    :cond_0
    return-void
.end method
