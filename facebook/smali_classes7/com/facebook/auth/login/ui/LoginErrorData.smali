.class public Lcom/facebook/auth/login/ui/LoginErrorData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/login/ui/LoginErrorData;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226581
    const-class v0, Lcom/facebook/auth/login/ui/LoginErrorData;

    sput-object v0, Lcom/facebook/auth/login/ui/LoginErrorData;->d:Ljava/lang/Class;

    .line 1226582
    new-instance v0, LX/7hl;

    invoke-direct {v0}, LX/7hl;-><init>()V

    sput-object v0, Lcom/facebook/auth/login/ui/LoginErrorData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1226583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226584
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1226585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226586
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->a:J

    .line 1226587
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->b:Ljava/lang/String;

    .line 1226588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->c:Ljava/lang/String;

    .line 1226589
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1226590
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1226591
    iget-wide v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1226592
    iget-object v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1226593
    iget-object v0, p0, Lcom/facebook/auth/login/ui/LoginErrorData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1226594
    return-void
.end method
