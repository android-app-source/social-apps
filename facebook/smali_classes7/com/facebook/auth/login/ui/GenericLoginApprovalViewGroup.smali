.class public Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;
.super Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/auth/login/ui/AuthFragmentViewGroup",
        "<",
        "LX/7hk;",
        ">;"
    }
.end annotation


# instance fields
.field public inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final loginButton:Landroid/view/View;

.field public mDynamicLayoutUtil:LX/46x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final mHideLogoOnSmallDisplays:Z

.field private final passwordText:Landroid/widget/TextView;


# direct methods
.method public static $ul_injectMe(Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;Landroid/view/inputmethod/InputMethodManager;LX/46x;)V
    .locals 0

    .prologue
    .line 1226468
    iput-object p1, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iput-object p2, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mDynamicLayoutUtil:LX/46x;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7hk;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1226469
    invoke-direct {p0, p1, p2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;-><init>(Landroid/content/Context;LX/7hV;)V

    .line 1226470
    const-class v0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;

    invoke-static {v0, p0}, LX/0QA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1226471
    const-string v0, "orca:authparam:login_approval_layout"

    const v1, 0x7f030d1d

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1226472
    const v0, 0x7f0d0c15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->passwordText:Landroid/widget/TextView;

    .line 1226473
    const v0, 0x7f0d20b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->loginButton:Landroid/view/View;

    .line 1226474
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1226475
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orca:authparam:hide_logo"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mHideLogoOnSmallDisplays:Z

    .line 1226476
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mHideLogoOnSmallDisplays:Z

    if-eqz v3, :cond_0

    .line 1226477
    iget-object v3, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mDynamicLayoutUtil:LX/46x;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const v6, 0x7f0d20ae

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/46x;->a(Landroid/view/View;ILjava/util/List;)V

    .line 1226478
    iget-object v3, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mDynamicLayoutUtil:LX/46x;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const v6, 0x7f0d02c4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const v7, 0x7f0d1bb7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    const v7, 0x7f0b0146

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const v8, 0x7f0b0148

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7, v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    const v8, 0x7f0b0145

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const v9, 0x7f0b0147

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v8, v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/46x;->a(Landroid/view/View;ILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1226479
    :cond_0
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->loginButton:Landroid/view/View;

    new-instance v1, LX/7hh;

    invoke-direct {v1, p0}, LX/7hh;-><init>(Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226480
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->passwordText:Landroid/widget/TextView;

    new-instance v1, LX/7hi;

    invoke-direct {v1, p0}, LX/7hi;-><init>(Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1226481
    return-void

    .line 1226482
    :cond_1
    iput-boolean v2, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->mHideLogoOnSmallDisplays:Z

    goto/16 :goto_0
.end method

.method public static createParameterBundle(I)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1226483
    const/4 v0, 0x0

    .line 1226484
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1226485
    const-string v2, "orca:authparam:login_approval_layout"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226486
    const-string v2, "orca:authparam:hide_logo"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1226487
    move-object v0, v1

    .line 1226488
    return-object v0
.end method

.method public static onLoginClick(Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;)V
    .locals 3

    .prologue
    .line 1226489
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->passwordText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1226490
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1226491
    :goto_0
    return-void

    .line 1226492
    :cond_0
    iget-object v0, p0, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1226493
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/GenericLoginApprovalViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080155

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method
