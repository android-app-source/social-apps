.class public Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/7hV;",
        ">",
        "Lcom/facebook/widget/CustomViewGroup;"
    }
.end annotation


# instance fields
.field public final control:LX/7hV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mEnterTransitionAnimation:I

.field private final mExitTransitionAnimation:I

.field private final mPopEnterTransitionAnimation:I

.field private final mPopExitTransitionAnimation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7hV;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1226323
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1226324
    iput-object p2, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->control:LX/7hV;

    .line 1226325
    const-string v0, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mEnterTransitionAnimation:I

    .line 1226326
    const-string v0, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mExitTransitionAnimation:I

    .line 1226327
    const-string v0, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mPopEnterTransitionAnimation:I

    .line 1226328
    const-string v0, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mPopExitTransitionAnimation:I

    .line 1226329
    return-void
.end method

.method public static createParameterBundle(IIII)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1226330
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1226331
    const-string v1, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226332
    const-string v1, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226333
    const-string v1, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226334
    const-string v1, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1226335
    return-object v0
.end method


# virtual methods
.method public getArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1226336
    iget-object v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->control:LX/7hV;

    invoke-interface {v0}, LX/7hV;->a()Lcom/facebook/auth/login/ui/AuthFragmentConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentConfig;->b:Landroid/os/Bundle;

    return-object v0
.end method

.method public getResourceArgument(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1226337
    invoke-virtual {p0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 1226338
    const/4 v0, 0x0

    .line 1226339
    if-eqz v1, :cond_0

    .line 1226340
    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1226341
    :cond_0
    if-nez v0, :cond_1

    .line 1226342
    :goto_0
    return p2

    :cond_1
    move p2, v0

    goto :goto_0
.end method

.method public setCustomAnimations(LX/42p;)V
    .locals 4

    .prologue
    .line 1226343
    iget v0, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mEnterTransitionAnimation:I

    iget v1, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mExitTransitionAnimation:I

    iget v2, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mPopEnterTransitionAnimation:I

    iget v3, p0, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->mPopExitTransitionAnimation:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/42p;->a(IIII)LX/42p;

    .line 1226344
    return-void
.end method
