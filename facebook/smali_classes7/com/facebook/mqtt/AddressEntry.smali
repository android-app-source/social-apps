.class public Lcom/facebook/mqtt/AddressEntry;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/mqtt/AddressEntryDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public mAddressListData:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "address_list_data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mFailCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fail_count"
    .end annotation
.end field

.field public final mHostName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "host_name"
    .end annotation
.end field

.field public final mPriority:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "priority"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1143461
    const-class v0, Lcom/facebook/mqtt/AddressEntryDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1143462
    const-class v0, Lcom/facebook/mqtt/AddressEntrySerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1143463
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/facebook/mqtt/AddressEntry;-><init>(Ljava/lang/String;LX/0Px;II)V

    .line 1143464
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/0Px;II)V
    .locals 5
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/net/InetAddress;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1143465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143466
    iput-object p1, p0, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    .line 1143467
    iput-object p2, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    .line 1143468
    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    if-eqz v0, :cond_1

    .line 1143469
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1143470
    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 1143471
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1143472
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1143473
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->mAddressListData:LX/0Px;

    .line 1143474
    :cond_1
    iput p3, p0, Lcom/facebook/mqtt/AddressEntry;->mPriority:I

    .line 1143475
    iput p4, p0, Lcom/facebook/mqtt/AddressEntry;->mFailCount:I

    .line 1143476
    return-void
.end method

.method private a()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1143477
    iget-object v1, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    if-nez v1, :cond_2

    .line 1143478
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1143479
    iget-object v1, p0, Lcom/facebook/mqtt/AddressEntry;->mAddressListData:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->mAddressListData:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1143480
    const/4 v1, 0x0

    .line 1143481
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1143482
    :goto_1
    if-eqz v0, :cond_0

    .line 1143483
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1143484
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1143485
    :catch_0
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-object v0, v1

    goto :goto_1

    .line 1143486
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    .line 1143487
    :cond_2
    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->a:LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1143488
    if-ne p0, p1, :cond_1

    .line 1143489
    :cond_0
    :goto_0
    return v0

    .line 1143490
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1143491
    :cond_3
    check-cast p1, Lcom/facebook/mqtt/AddressEntry;

    .line 1143492
    invoke-direct {p0}, Lcom/facebook/mqtt/AddressEntry;->a()LX/0Px;

    move-result-object v2

    invoke-direct {p1}, Lcom/facebook/mqtt/AddressEntry;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1143493
    iget-object v0, p0, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1143494
    mul-int/lit8 v0, v0, 0x1f

    invoke-direct {p0}, Lcom/facebook/mqtt/AddressEntry;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1143495
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1143496
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AE{\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/mqtt/AddressEntry;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/mqtt/AddressEntry;->mPriority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/mqtt/AddressEntry;->mFailCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
