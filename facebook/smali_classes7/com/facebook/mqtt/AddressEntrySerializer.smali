.class public Lcom/facebook/mqtt/AddressEntrySerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/mqtt/AddressEntry;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1143521
    const-class v0, Lcom/facebook/mqtt/AddressEntry;

    new-instance v1, Lcom/facebook/mqtt/AddressEntrySerializer;

    invoke-direct {v1}, Lcom/facebook/mqtt/AddressEntrySerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1143522
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1143523
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/mqtt/AddressEntry;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1143524
    if-nez p0, :cond_0

    .line 1143525
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1143526
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1143527
    invoke-static {p0, p1, p2}, Lcom/facebook/mqtt/AddressEntrySerializer;->b(Lcom/facebook/mqtt/AddressEntry;LX/0nX;LX/0my;)V

    .line 1143528
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1143529
    return-void
.end method

.method private static b(Lcom/facebook/mqtt/AddressEntry;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1143530
    const-string v0, "host_name"

    iget-object v1, p0, Lcom/facebook/mqtt/AddressEntry;->mHostName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1143531
    const-string v0, "priority"

    iget v1, p0, Lcom/facebook/mqtt/AddressEntry;->mPriority:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1143532
    const-string v0, "fail_count"

    iget v1, p0, Lcom/facebook/mqtt/AddressEntry;->mFailCount:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1143533
    const-string v0, "address_list_data"

    iget-object v1, p0, Lcom/facebook/mqtt/AddressEntry;->mAddressListData:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1143534
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1143535
    check-cast p1, Lcom/facebook/mqtt/AddressEntry;

    invoke-static {p1, p2, p3}, Lcom/facebook/mqtt/AddressEntrySerializer;->a(Lcom/facebook/mqtt/AddressEntry;LX/0nX;LX/0my;)V

    return-void
.end method
