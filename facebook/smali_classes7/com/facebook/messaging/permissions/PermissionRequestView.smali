.class public Lcom/facebook/messaging/permissions/PermissionRequestView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1311009
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/permissions/PermissionRequestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1311010
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1311011
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/permissions/PermissionRequestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1311012
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1311013
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1311014
    invoke-direct {p0, p2}, Lcom/facebook/messaging/permissions/PermissionRequestView;->a(Landroid/util/AttributeSet;)V

    .line 1311015
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1311016
    const-class v0, Lcom/facebook/messaging/permissions/PermissionRequestView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/permissions/PermissionRequestView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1311017
    const v0, 0x7f030f2e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1311018
    const v0, 0x7f0d24c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestView;->b:Landroid/widget/TextView;

    .line 1311019
    const v0, 0x7f0d24ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1311020
    new-instance v1, LX/8CW;

    invoke-direct {v1, p0}, LX/8CW;-><init>(Lcom/facebook/messaging/permissions/PermissionRequestView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311021
    invoke-virtual {p0}, Lcom/facebook/messaging/permissions/PermissionRequestView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->PermissionRequestView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1311022
    invoke-virtual {p0}, Lcom/facebook/messaging/permissions/PermissionRequestView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1

    invoke-static {v1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    .line 1311023
    iget-object v2, p0, Lcom/facebook/messaging/permissions/PermissionRequestView;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311024
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1311025
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/permissions/PermissionRequestView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/permissions/PermissionRequestView;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    iput-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestView;->a:LX/1Ml;

    return-void
.end method


# virtual methods
.method public setDescription(I)V
    .locals 1

    .prologue
    .line 1311026
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1311027
    return-void
.end method
