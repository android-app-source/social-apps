.class public Lcom/facebook/messaging/permissions/PermissionRequestIconView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1310970
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1310971
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310972
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1310973
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310974
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310975
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1310976
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310977
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310978
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v3, -0x80000000

    .line 1310979
    const v0, 0x7f030f2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1310980
    const v0, 0x7f0d19dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1310981
    const v0, 0x7f0d19de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1310982
    const v0, 0x7f0d19df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1310983
    sget-object v0, LX/03r;->PermissionRequestView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1310984
    const/16 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1310985
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->setIcon(I)V

    .line 1310986
    invoke-virtual {p0}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1

    invoke-static {v1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    .line 1310987
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->setText(Ljava/lang/String;)V

    .line 1310988
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1310989
    if-eq v1, v3, :cond_0

    .line 1310990
    iget-object v2, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1310991
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1310992
    if-eq v1, v3, :cond_1

    .line 1310993
    iget-object v2, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1310994
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1310995
    return-void
.end method


# virtual methods
.method public getRequestButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 1310996
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->c:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public setButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1310997
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1310998
    return-void
.end method

.method public setIcon(I)V
    .locals 2

    .prologue
    .line 1310999
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1311000
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1311001
    :goto_0
    return-void

    .line 1311002
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311003
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1311004
    iget-object v0, p0, Lcom/facebook/messaging/permissions/PermissionRequestIconView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311005
    return-void
.end method
