.class public Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadSuperClassBroadcastReceiver.SecureBroadcastReceiver"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field public a:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/8C0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1310037
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1310038
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1310039
    return-void
.end method

.method private static a(Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;LX/1Ml;Ljava/util/Set;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;",
            "LX/1Ml;",
            "Ljava/util/Set",
            "<",
            "LX/8C0;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1310040
    iput-object p1, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->a:LX/1Ml;

    iput-object p2, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    iput-object p3, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->c:LX/0Uh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;

    invoke-static {v1}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    new-instance v2, LX/0U8;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance p1, LX/8C4;

    invoke-direct {p1, v1}, LX/8C4;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v2

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->a(Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;LX/1Ml;Ljava/util/Set;LX/0Uh;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/16 v7, 0xa

    const/16 v0, 0x26

    const v1, 0x37c6487a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1310041
    invoke-static {p0, p1}, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1310042
    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->c:LX/0Uh;

    const/16 v1, 0x1f8

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1310043
    :cond_0
    const/16 v0, 0x27

    const v1, -0x57c078e8

    invoke-static {p2, v4, v0, v1, v2}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 1310044
    :goto_0
    return-void

    .line 1310045
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1310046
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1310047
    const v0, 0x17a0f2ba

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 1310048
    :cond_2
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1310049
    const-string v0, "wifiInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiInfo;

    .line 1310050
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1310051
    :cond_3
    const v0, -0x986669b

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 1310052
    :cond_4
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 1310053
    new-instance v3, LX/8C1;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    invoke-static {v0, v7}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-direct {v3, v4, v5, v0, v1}, LX/8C1;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/net/NetworkInfo$State;)V

    .line 1310054
    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 1310055
    :cond_5
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1310056
    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->a:LX/1Ml;

    sget-object v1, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->d:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1310057
    const v0, -0x1b15708c    # -3.4615E22f

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 1310058
    :cond_6
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1310059
    if-nez v0, :cond_7

    .line 1310060
    const v0, -0x15ce01b2

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 1310061
    :cond_7
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 1310062
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1310063
    :cond_8
    const v0, -0xd406dcc

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 1310064
    :cond_9
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1310065
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 1310066
    iget-object v4, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 1310067
    iget v4, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v4, v7}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v4

    .line 1310068
    new-instance v5, LX/8C1;

    iget-object v6, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-direct {v5, v6, v0, v4}, LX/8C1;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1310069
    invoke-virtual {v1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1310070
    :cond_b
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    .line 1310071
    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_3

    .line 1310072
    :cond_c
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1310073
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1310074
    if-nez v0, :cond_d

    .line 1310075
    const v0, -0x35fab473

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 1310076
    :cond_d
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 1310077
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1310078
    :cond_e
    const v0, 0x52c258f3

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 1310079
    :cond_f
    new-instance v1, LX/8C1;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    invoke-static {v0, v7}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    invoke-direct {v1, v3, v4, v0}, LX/8C1;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1310080
    iget-object v0, p0, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_4

    .line 1310081
    :cond_10
    const v0, -0x2b4198d4

    invoke-static {p2, v0, v2}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0
.end method
