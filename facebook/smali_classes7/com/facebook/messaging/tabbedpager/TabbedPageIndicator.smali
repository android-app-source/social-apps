.class public Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/7Um;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field public b:Landroid/support/v7/widget/RecyclerView;

.field private c:Landroid/support/v4/view/ViewPager;

.field public d:LX/0hc;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1311120
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1311121
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    .line 1311122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1311102
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311103
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    .line 1311104
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1311095
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1311096
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    .line 1311097
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311098
    return-void
.end method

.method private a()F
    .locals 3

    .prologue
    .line 1311106
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1311107
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->b:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1311108
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    mul-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1311109
    sget-object v0, LX/03r;->EmojiCategoryPageIndicator:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1311110
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->e:I

    .line 1311111
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    .line 1311112
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->g:I

    .line 1311113
    const/16 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1311114
    const/16 v2, 0x4

    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a01c5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1311115
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1311116
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1311117
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1311118
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1311119
    return-void
.end method

.method private getIndicatorLeft()F
    .locals 4

    .prologue
    .line 1311124
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getIndicatorWidth()F

    move-result v0

    .line 1311125
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    int-to-float v1, v1

    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getScrollPosition()F

    move-result v2

    iget v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->h:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a()F

    move-result v1

    sub-float/2addr v0, v1

    .line 1311126
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private getIndicatorRight()F
    .locals 4

    .prologue
    .line 1311127
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getIndicatorWidth()F

    move-result v0

    .line 1311128
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    int-to-float v1, v1

    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getScrollPosition()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->h:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a()F

    move-result v1

    sub-float/2addr v0, v1

    .line 1311129
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->g:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private getScrollPosition()F
    .locals 1

    .prologue
    .line 1311130
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 1311131
    :cond_0
    const/4 v0, 0x0

    .line 1311132
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->i:F

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 1311133
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    if-eqz v0, :cond_0

    .line 1311134
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 1311135
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 1311136
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->invalidate()V

    .line 1311137
    iput p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->h:I

    .line 1311138
    iput p2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->i:F

    .line 1311139
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    if-eqz v0, :cond_0

    .line 1311140
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 1311141
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1311099
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    if-eqz v0, :cond_0

    .line 1311100
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1311101
    :cond_0
    return-void
.end method

.method public getIndicatorWidth()F
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1311088
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v2

    .line 1311089
    if-nez v2, :cond_1

    move v0, v1

    .line 1311090
    :cond_0
    :goto_0
    return v0

    .line 1311091
    :cond_1
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->e:I

    int-to-float v0, v0

    .line 1311092
    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 1311093
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->g:I

    sub-int/2addr v0, v1

    .line 1311094
    div-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_0
.end method

.method public getLeftTrackPadding()I
    .locals 1

    .prologue
    .line 1311087
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    return v0
.end method

.method public getRightTrackPadding()I
    .locals 1

    .prologue
    .line 1311086
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->g:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1311082
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1311083
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 1311084
    :goto_0
    return-void

    .line 1311085
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getIndicatorLeft()F

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getIndicatorRight()F

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->getMeasuredHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 0

    .prologue
    .line 1311081
    return-void
.end method

.method public setLeftTrackPadding(I)V
    .locals 0

    .prologue
    .line 1311062
    iput p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->f:I

    .line 1311063
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->invalidate()V

    .line 1311064
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 0

    .prologue
    .line 1311079
    iput-object p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    .line 1311080
    return-void
.end method

.method public setRightTrackPadding(I)V
    .locals 0

    .prologue
    .line 1311076
    iput p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->g:I

    .line 1311077
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->invalidate()V

    .line 1311078
    return-void
.end method

.method public setTabRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 1311074
    iput-object p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1311075
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1311065
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_0

    .line 1311066
    :goto_0
    return-void

    .line 1311067
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 1311068
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1311069
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1311070
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1311071
    :cond_2
    iput-object p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    .line 1311072
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1311073
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->invalidate()V

    goto :goto_0
.end method
