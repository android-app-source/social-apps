.class public Lcom/facebook/messaging/tabbedpager/TabbedPager;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/8Cf;

.field public b:LX/8CZ;

.field private c:Landroid/widget/TextView;

.field private d:LX/1P1;

.field private e:Landroid/support/v7/widget/RecyclerView;

.field public f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ImageButton;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/support/v4/view/ViewPager;

.field public n:Z

.field private o:Z

.field public p:LX/8Cd;

.field public q:LX/8CX;

.field private r:Ljava/lang/String;

.field private s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1311228
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1311229
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    .line 1311230
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311231
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1311232
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311233
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    .line 1311234
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1311235
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1311236
    const-class v0, Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-static {v0, p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1311237
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->TabbedPager:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1311238
    const/16 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x3

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1311239
    const v0, 0x7f031468

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1311240
    :goto_0
    const v0, 0x7f0d1ea9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c:Landroid/widget/TextView;

    .line 1311241
    const v0, 0x7f0d2c0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    .line 1311242
    const v0, 0x7f0d1803

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    .line 1311243
    const v0, 0x7f0d2e73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->g:Landroid/view/View;

    .line 1311244
    const v0, 0x7f0d2e71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->h:Landroid/widget/ImageButton;

    .line 1311245
    const v0, 0x7f0d2e72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->i:Landroid/widget/TextView;

    .line 1311246
    const v0, 0x7f0d2e74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->j:Landroid/view/View;

    .line 1311247
    const v0, 0x7f0d2e75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    .line 1311248
    const v0, 0x7f0d2e76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->l:Landroid/widget/TextView;

    .line 1311249
    const v0, 0x7f0d1241

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->m:Landroid/support/v4/view/ViewPager;

    .line 1311250
    new-instance v0, LX/1P1;

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    .line 1311251
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0, v4}, LX/1P1;->b(I)V

    .line 1311252
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1311253
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, LX/1Oe;

    invoke-direct {v2}, LX/1Oe;-><init>()V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1311254
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1311255
    const/16 v0, 0x0

    invoke-static {p1, v1, v0}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->r:Ljava/lang/String;

    .line 1311256
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1311257
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1311258
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1311259
    :cond_1
    const/16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1311260
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->o:Z

    .line 1311261
    :cond_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1311262
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setOrientation(I)V

    .line 1311263
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->m:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1311264
    return-void

    .line 1311265
    :cond_3
    const v0, 0x7f031465

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;

    invoke-static {v0}, LX/8Cf;->b(LX/0QB;)LX/8Cf;

    move-result-object v0

    check-cast v0, LX/8Cf;

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1311266
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    if-nez v0, :cond_0

    .line 1311267
    :goto_0
    return-void

    .line 1311268
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311269
    iget-object v1, v0, LX/8Cf;->d:LX/0Px;

    move-object v0, v1

    .line 1311270
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1311271
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c:Landroid/widget/TextView;

    if-lez v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1311272
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v3

    move v0, v1

    .line 1311273
    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1311274
    add-int v2, v3, v0

    iget v4, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    .line 1311275
    :goto_1
    iget-object v4, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1311276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 1311277
    goto :goto_1

    .line 1311278
    :cond_1
    return-void
.end method

.method public static d(Lcom/facebook/messaging/tabbedpager/TabbedPager;I)V
    .locals 3

    .prologue
    .line 1311359
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311360
    iget-object v1, v0, LX/8Cf;->d:LX/0Px;

    move-object v0, v1

    .line 1311361
    if-ltz p1, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 1311362
    :cond_0
    const/4 p1, 0x0

    .line 1311363
    :cond_1
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    if-ne p1, v1, :cond_2

    .line 1311364
    :goto_0
    return-void

    .line 1311365
    :cond_2
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1311366
    iget v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    .line 1311367
    :cond_3
    iput p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    .line 1311368
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->q:LX/8CX;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, LX/8CX;->d(Ljava/lang/Object;)V

    .line 1311369
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    if-eqz v1, :cond_4

    .line 1311370
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, LX/8Cd;->a(Ljava/lang/Object;)V

    .line 1311371
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1311372
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d()V

    .line 1311373
    invoke-static {p0, p1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e(Lcom/facebook/messaging/tabbedpager/TabbedPager;I)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1311279
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLeft()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->setLeftTrackPadding(I)V

    .line 1311280
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->setRightTrackPadding(I)V

    .line 1311281
    return-void
.end method

.method public static e(Lcom/facebook/messaging/tabbedpager/TabbedPager;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1311282
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1311283
    :cond_0
    :goto_0
    return-void

    .line 1311284
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    add-int/lit8 v1, p1, -0x1

    if-lt v0, v1, :cond_2

    .line 1311285
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1311286
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    .line 1311287
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 1311288
    add-int/lit8 v2, p1, -0x1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    mul-int/2addr v0, v2

    int-to-float v0, v0

    .line 1311289
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {v2, v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    goto :goto_0

    .line 1311290
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1311291
    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311292
    iget-object v2, v1, LX/8Cf;->d:LX/0Px;

    move-object v1, v2

    .line 1311293
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1311294
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1311295
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v2}, LX/1P1;->n()I

    move-result v2

    .line 1311296
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 1311297
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    int-to-float v0, v0

    .line 1311298
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    sub-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1311180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->n:Z

    .line 1311181
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1311182
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    .line 1311183
    iput-object v1, v0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1311184
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/8Ca;

    invoke-direct {v1, p0}, LX/8Ca;-><init>(Lcom/facebook/messaging/tabbedpager/TabbedPager;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1311185
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    new-instance v1, LX/8Cb;

    invoke-direct {v1, p0}, LX/8Cb;-><init>(Lcom/facebook/messaging/tabbedpager/TabbedPager;)V

    .line 1311186
    iput-object v1, v0, Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;->d:LX/0hc;

    .line 1311187
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1311188
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1311189
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311190
    :goto_0
    return-void

    .line 1311191
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1311299
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1311300
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311301
    iget-object v2, v1, LX/8Cf;->d:LX/0Px;

    move-object v1, v2

    .line 1311302
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1311303
    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1311304
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v1, v0}, LX/8Cf;->a(Ljava/util/List;)V

    .line 1311305
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311306
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1311307
    iget-object v2, v0, LX/8CZ;->d:LX/0Px;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1311308
    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1311309
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8CZ;->d:LX/0Px;

    .line 1311310
    invoke-virtual {v0, p1}, LX/1OM;->j_(I)V

    .line 1311311
    invoke-static {v0}, LX/8CZ;->d(LX/8CZ;)V

    .line 1311312
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1311313
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0, p1}, LX/8Cf;->a(Ljava/lang/String;)I

    move-result v0

    .line 1311314
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311315
    iput-object p1, v1, LX/8CZ;->e:Ljava/lang/String;

    .line 1311316
    if-ltz v0, :cond_0

    .line 1311317
    invoke-static {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d(Lcom/facebook/messaging/tabbedpager/TabbedPager;I)V

    .line 1311318
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1311319
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0, p1}, LX/8Cf;->a(Ljava/lang/String;)I

    move-result v0

    .line 1311320
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1311321
    :goto_0
    return-void

    .line 1311322
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    if-ge p2, v1, :cond_1

    .line 1311323
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v1, v0, p2}, LX/1P1;->d(II)V

    goto :goto_0

    .line 1311324
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 1311325
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1311326
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    .line 1311327
    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311328
    iget-object v3, v2, LX/8Cf;->d:LX/0Px;

    move-object v2, v3

    .line 1311329
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1311330
    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1311331
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v2, v1}, LX/8Cf;->a(Ljava/util/List;)V

    .line 1311332
    iget-object v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311333
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1311334
    iget-object v4, v2, LX/8CZ;->d:LX/0Px;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1311335
    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1311336
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/8CZ;->d:LX/0Px;

    .line 1311337
    invoke-virtual {v2, p1}, LX/1OM;->d(I)V

    .line 1311338
    invoke-static {v2}, LX/8CZ;->d(LX/8CZ;)V

    .line 1311339
    if-eqz v0, :cond_0

    .line 1311340
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    invoke-static {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e(Lcom/facebook/messaging/tabbedpager/TabbedPager;I)V

    .line 1311341
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    iget v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8Cd;->a(Ljava/lang/Object;)V

    .line 1311342
    :cond_0
    return-void

    .line 1311343
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTabContainerScrollOffsetToRestore()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1311344
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1311345
    :cond_0
    :goto_0
    return v0

    .line 1311346
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    .line 1311347
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1311348
    iget-object v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v3}, LX/1P1;->m()I

    move-result v3

    .line 1311349
    iget-object v4, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v4}, LX/1P1;->o()I

    move-result v4

    .line 1311350
    iget v5, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    add-int/lit8 v5, v5, -0x1

    if-le v3, v5, :cond_2

    .line 1311351
    iget v2, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1311352
    :cond_2
    iget v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    add-int/lit8 v3, v3, 0x1

    if-ge v4, v3, :cond_4

    .line 1311353
    iget v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    iget-object v4, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    .line 1311354
    iget-object v5, v4, LX/8Cf;->d:LX/0Px;

    move-object v4, v5

    .line 1311355
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_3

    const/4 v0, 0x1

    .line 1311356
    :cond_3
    add-int/lit8 v0, v0, 0x1

    mul-int/2addr v0, v1

    sub-int v0, v2, v0

    goto :goto_0

    .line 1311357
    :cond_4
    iget v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->s:I

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->d:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1311358
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method

.method public getTabCount()I
    .locals 1

    .prologue
    .line 1311226
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    return v0
.end method

.method public getTabListAdapter()LX/8CZ;
    .locals 1

    .prologue
    .line 1311227
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    return-object v0
.end method

.method public getTabbedPageIndicator()Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;
    .locals 1

    .prologue
    .line 1311157
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->f:Lcom/facebook/messaging/tabbedpager/TabbedPageIndicator;

    return-object v0
.end method

.method public onMeasure(II)V
    .locals 0

    .prologue
    .line 1311158
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1311159
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e()V

    .line 1311160
    return-void
.end method

.method public setAdapter(LX/8CX;)V
    .locals 2

    .prologue
    .line 1311161
    iput-object p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->q:LX/8CX;

    .line 1311162
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0, p1}, LX/8Cf;->a(LX/8CX;)V

    .line 1311163
    new-instance v0, LX/8CZ;

    invoke-direct {v0}, LX/8CZ;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311164
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311165
    iput-object p1, v0, LX/8CZ;->c:LX/8CX;

    .line 1311166
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1311167
    invoke-static {v0}, LX/8CZ;->d(LX/8CZ;)V

    .line 1311168
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    new-instance v1, LX/8Cc;

    invoke-direct {v1, p0}, LX/8Cc;-><init>(Lcom/facebook/messaging/tabbedpager/TabbedPager;)V

    .line 1311169
    iput-object v1, v0, LX/8CZ;->b:LX/8Cc;

    .line 1311170
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1311171
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c()V

    .line 1311172
    return-void
.end method

.method public setEndTabButtonBadgeText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1311173
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311174
    return-void
.end method

.method public setEndTabButtonBadgeVisibility(Z)V
    .locals 2

    .prologue
    .line 1311175
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->l:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1311176
    return-void

    .line 1311177
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setEndTabButtonContentDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1311178
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1311179
    return-void
.end method

.method public setEndTabButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1311192
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311193
    return-void
.end method

.method public setEndTabButtonOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1311194
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1311195
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1311196
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a:LX/8Cf;

    invoke-virtual {v0, p1}, LX/8Cf;->a(Ljava/util/List;)V

    .line 1311197
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->b:LX/8CZ;

    .line 1311198
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/8CZ;->d:LX/0Px;

    .line 1311199
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1311200
    invoke-static {v0}, LX/8CZ;->d(LX/8CZ;)V

    .line 1311201
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->c()V

    .line 1311202
    return-void
.end method

.method public setListener(LX/8Cd;)V
    .locals 0

    .prologue
    .line 1311203
    iput-object p1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->p:LX/8Cd;

    .line 1311204
    return-void
.end method

.method public setShowEndTabButton(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1311205
    iget-object v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->k:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1311206
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->j:Landroid/view/View;

    if-eqz p1, :cond_1

    iget-boolean v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->o:Z

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1311207
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e()V

    .line 1311208
    return-void

    :cond_0
    move v0, v2

    .line 1311209
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1311210
    goto :goto_1
.end method

.method public setShowStartTabButton(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1311211
    iget-object v3, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->h:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1311212
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->g:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1311213
    invoke-direct {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->e()V

    .line 1311214
    return-void

    :cond_0
    move v0, v2

    .line 1311215
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1311216
    goto :goto_1
.end method

.method public setStartTabButtonBadgeText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1311217
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311218
    return-void
.end method

.method public setStartTabButtonBadgeVisibility(Z)V
    .locals 2

    .prologue
    .line 1311219
    iget-object v1, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->i:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1311220
    return-void

    .line 1311221
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStartTabButtonContentDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1311222
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1311223
    return-void
.end method

.method public setStartTabButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1311224
    iget-object v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311225
    return-void
.end method
