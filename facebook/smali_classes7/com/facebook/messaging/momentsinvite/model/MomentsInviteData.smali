.class public Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1121417
    new-instance v0, LX/6gA;

    invoke-direct {v0}, LX/6gA;-><init>()V

    sput-object v0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6gB;)V
    .locals 1

    .prologue
    .line 1121418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121419
    iget-object v0, p1, LX/6gB;->a:Ljava/util/List;

    move-object v0, v0

    .line 1121420
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->a:LX/0Px;

    .line 1121421
    iget v0, p1, LX/6gB;->b:I

    move v0, v0

    .line 1121422
    iput v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->b:I

    .line 1121423
    iget-object v0, p1, LX/6gB;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1121424
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    .line 1121425
    iget-object v0, p1, LX/6gB;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1121426
    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->d:Ljava/lang/String;

    .line 1121427
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1121428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121429
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->a:LX/0Px;

    .line 1121430
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->b:I

    .line 1121431
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    .line 1121432
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->d:Ljava/lang/String;

    .line 1121433
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1121434
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1121435
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1121436
    iget v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1121437
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1121438
    iget-object v0, p0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1121439
    return-void
.end method
