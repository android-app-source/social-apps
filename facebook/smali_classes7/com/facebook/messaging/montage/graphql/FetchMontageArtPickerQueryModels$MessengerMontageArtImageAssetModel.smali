.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x434c7118
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:D

.field private k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:D

.field private o:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1122317
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1122346
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1122344
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1122345
    return-void
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122342
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    .line 1122343
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 1122318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1122319
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x61a67ea

    invoke-static {v1, v0, v2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1122320
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->s()Lcom/facebook/graphql/enums/GraphQLMessengerMontageImageAssetType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1122321
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1122322
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1122323
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1122324
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1122325
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1122326
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1122327
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v10

    invoke-static {p1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1122328
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1122329
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1122330
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1122331
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1122332
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1122333
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1122334
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1122335
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1122336
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1122337
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1122338
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1122339
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1122340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1122341
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1122243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1122244
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1122245
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x61a67ea

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1122246
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1122247
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122248
    iput v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->e:I

    move-object v1, v0

    .line 1122249
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1122250
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122251
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1122252
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122253
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122254
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1122255
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122256
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1122257
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122258
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->h:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122259
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1122260
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122261
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1122262
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122263
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122264
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1122265
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122266
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1122267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122268
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122269
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1122270
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122271
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1122272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122273
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122274
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1122275
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122276
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1122277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122278
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122279
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1122280
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    .line 1122281
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1122282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    .line 1122283
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    .line 1122284
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1122285
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 1122286
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_8
    move-object p0, v1

    .line 1122287
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1122315
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1122316
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1122310
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1122311
    const/4 v0, 0x0

    const v1, -0x61a67ea

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->e:I

    .line 1122312
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j:D

    .line 1122313
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n:D

    .line 1122314
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1122307
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;-><init>()V

    .line 1122308
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1122309
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1122306
    const v0, 0x7c5a006c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1122347
    const v0, -0xa2ca3e7

    return v0
.end method

.method public final j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122304
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122305
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122302
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->h:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->h:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122303
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->h:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122300
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122301
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    return-object v0
.end method

.method public final m()D
    .locals 2

    .prologue
    .line 1122298
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1122299
    iget-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->j:D

    return-wide v0
.end method

.method public final n()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122296
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    .line 1122297
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    return-object v0
.end method

.method public final o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122294
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    .line 1122295
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->l:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetPositionModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122292
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    .line 1122293
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetSizeModel;

    return-object v0
.end method

.method public final q()D
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1122290
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1122291
    iget-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->n:D

    return-wide v0
.end method

.method public final r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122288
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    .line 1122289
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtImageAssetModel;->o:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerStickerModel;

    return-object v0
.end method
