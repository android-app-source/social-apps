.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1127772
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1127773
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1127774
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1127775
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1127776
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1127777
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1127778
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1127779
    if-eqz v2, :cond_2

    .line 1127780
    const-string p0, "messenger_montage_viewers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1127781
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1127782
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1127783
    if-eqz p0, :cond_1

    .line 1127784
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1127785
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1127786
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1127787
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1}, LX/6hC;->a(LX/15i;ILX/0nX;)V

    .line 1127788
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1127789
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1127790
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1127791
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1127792
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1127793
    check-cast p1, Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel$Serializer;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageViewersQueryModels$FetchMontageViewersQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
