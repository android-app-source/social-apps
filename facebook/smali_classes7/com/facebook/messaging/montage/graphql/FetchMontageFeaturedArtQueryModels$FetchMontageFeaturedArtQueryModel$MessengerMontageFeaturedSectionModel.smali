.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6cb7ffb1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127471
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127472
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1127473
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1127474
    return-void
.end method

.method private j()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127477
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    .line 1127478
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127475
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->f:Ljava/lang/String;

    .line 1127476
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127469
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->g:Ljava/lang/String;

    .line 1127470
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1127445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127446
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1127447
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1127448
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1127449
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1127450
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1127451
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1127452
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1127453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127454
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1127461
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127462
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1127463
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    .line 1127464
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1127465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;

    .line 1127466
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    .line 1127467
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127468
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127460
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1127457
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel;-><init>()V

    .line 1127458
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1127459
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1127456
    const v0, -0x6f8d9dd0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1127455
    const v0, 0x4ba5e798    # 2.1745456E7f

    return v0
.end method
