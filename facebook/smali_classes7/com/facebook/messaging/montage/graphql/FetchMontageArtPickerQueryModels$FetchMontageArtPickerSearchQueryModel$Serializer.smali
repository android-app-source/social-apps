.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1121984
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1121985
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1121986
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1121987
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1121988
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1121989
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1121990
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1121991
    if-eqz v2, :cond_d

    .line 1121992
    const-string v3, "search_results"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1121993
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1121994
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1121995
    if-eqz v3, :cond_c

    .line 1121996
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1121997
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1121998
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_b

    .line 1121999
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1122000
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1122001
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1122002
    if-eqz v6, :cond_a

    .line 1122003
    const-string v7, "node"

    invoke-virtual {p1, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122004
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1122005
    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1122006
    if-eqz v7, :cond_0

    .line 1122007
    const-string p0, "section_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122008
    invoke-virtual {p1, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1122009
    :cond_0
    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1122010
    if-eqz v7, :cond_1

    .line 1122011
    const-string p0, "section_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122012
    invoke-virtual {p1, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1122013
    :cond_1
    const/4 v7, 0x2

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1122014
    if-eqz v7, :cond_9

    .line 1122015
    const-string p0, "section_units"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122016
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1122017
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1122018
    if-eqz p0, :cond_4

    .line 1122019
    const-string v0, "edges"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122020
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1122021
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 1122022
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1122023
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1122024
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1122025
    if-eqz v5, :cond_2

    .line 1122026
    const-string v6, "node"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122027
    invoke-static {v1, v5, p1, p2}, LX/6gT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1122028
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122029
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1122030
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1122031
    :cond_4
    const/4 p0, 0x1

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1122032
    if-eqz p0, :cond_5

    .line 1122033
    const-string v0, "order_token"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122034
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1122035
    :cond_5
    const/4 p0, 0x2

    invoke-virtual {v1, v7, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1122036
    if-eqz p0, :cond_8

    .line 1122037
    const-string v0, "page_info"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122038
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1122039
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1122040
    if-eqz v0, :cond_6

    .line 1122041
    const-string v2, "end_cursor"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122042
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1122043
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1122044
    if-eqz v0, :cond_7

    .line 1122045
    const-string v2, "has_next_page"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1122046
    invoke-virtual {p1, v0}, LX/0nX;->a(Z)V

    .line 1122047
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122048
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122049
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122050
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122051
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1122052
    :cond_b
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1122053
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122054
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1122055
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1122056
    check-cast p1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Serializer;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
