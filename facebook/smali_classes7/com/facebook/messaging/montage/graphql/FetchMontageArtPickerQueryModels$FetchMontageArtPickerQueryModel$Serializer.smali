.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1121867
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1121868
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1121869
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1121870
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1121871
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1121872
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1121873
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1121874
    if-eqz v2, :cond_3

    .line 1121875
    const-string v3, "messenger_montage_art_picker_sections"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1121876
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1121877
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1121878
    if-eqz v3, :cond_2

    .line 1121879
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1121880
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1121881
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_1

    .line 1121882
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1121883
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1121884
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1121885
    if-eqz v0, :cond_0

    .line 1121886
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1121887
    invoke-static {v1, v0, p1, p2}, LX/6gR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1121888
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1121889
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1121890
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1121891
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1121892
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1121893
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1121894
    check-cast p1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel$Serializer;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
