.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2f7b3acb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1122124
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1122127
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1122125
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1122126
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1122116
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1122117
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1122118
    invoke-virtual {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1122119
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1122120
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1122121
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1122122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1122123
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1122128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1122129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1122130
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122107
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    .line 1122108
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->e:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1122109
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;-><init>()V

    .line 1122110
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1122111
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1122115
    const v0, 0x228e94b6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1122112
    const v0, -0x3170b83f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1122113
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    .line 1122114
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtAssetAnchoringModel;->f:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetVerticalAlignmentType;

    return-object v0
.end method
