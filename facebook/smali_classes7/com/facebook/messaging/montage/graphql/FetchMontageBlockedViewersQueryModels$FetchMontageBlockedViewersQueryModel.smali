.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x654add73
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127266
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127265
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1127237
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1127238
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerMontageBlockedViewers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1127263
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1127264
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1127257
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127258
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x7ed279e0

    invoke-static {v1, v0, v2}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1127259
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1127260
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1127261
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127262
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1127247
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127248
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1127249
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7ed279e0

    invoke-static {v2, v0, v3}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1127250
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1127251
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;

    .line 1127252
    iput v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->e:I

    .line 1127253
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127254
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1127255
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1127256
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1127244
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1127245
    const/4 v0, 0x0

    const v1, -0x7ed279e0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;->e:I

    .line 1127246
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1127241
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;-><init>()V

    .line 1127242
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1127243
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1127240
    const v0, -0x8f58f5e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1127239
    const v0, -0x6747e1ce

    return v0
.end method
