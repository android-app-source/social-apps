.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1127123
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1127124
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1127125
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1127126
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1127127
    const/4 v2, 0x0

    .line 1127128
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1127129
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1127130
    :goto_0
    move v1, v2

    .line 1127131
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1127132
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1127133
    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageBlockedViewersQueryModels$FetchMontageBlockedViewersQueryModel;-><init>()V

    .line 1127134
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1127135
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1127136
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1127137
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1127138
    :cond_0
    return-object v1

    .line 1127139
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1127140
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1127141
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1127142
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1127143
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1127144
    const-string v4, "messenger_montage_blocked_viewers"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1127145
    const/4 v3, 0x0

    .line 1127146
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1127147
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1127148
    :goto_2
    move v1, v3

    .line 1127149
    goto :goto_1

    .line 1127150
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1127151
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1127152
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1127153
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1127154
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_8

    .line 1127155
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1127156
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1127157
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1127158
    const-string p0, "nodes"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1127159
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1127160
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_7

    .line 1127161
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1127162
    invoke-static {p1, v0}, LX/6h4;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1127163
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1127164
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1127165
    goto :goto_3

    .line 1127166
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1127167
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1127168
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3
.end method
