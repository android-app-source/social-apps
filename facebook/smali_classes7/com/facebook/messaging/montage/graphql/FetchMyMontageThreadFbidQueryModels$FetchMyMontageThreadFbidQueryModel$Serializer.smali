.class public final Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1128000
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1128001
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1127999
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1127979
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1127980
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1127981
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1127982
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1127983
    if-eqz v2, :cond_2

    .line 1127984
    const-string p0, "messenger_montage_thread"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1127985
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1127986
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1127987
    if-eqz p0, :cond_1

    .line 1127988
    const-string v0, "thread_key"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1127989
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1127990
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1127991
    if-eqz v0, :cond_0

    .line 1127992
    const-string v2, "thread_fbid"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1127993
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1127994
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1127995
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1127996
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1127997
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1127998
    check-cast p1, Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel$Serializer;->a(Lcom/facebook/messaging/montage/graphql/FetchMyMontageThreadFbidQueryModels$FetchMyMontageThreadFbidQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
