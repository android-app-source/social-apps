.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4e9e4178
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127435
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1127434
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1127432
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1127433
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127430
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->e:Ljava/lang/String;

    .line 1127431
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127428
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->f:Ljava/lang/String;

    .line 1127429
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127426
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    .line 1127427
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127436
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->h:Ljava/lang/String;

    .line 1127437
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1127424
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->i:Ljava/lang/String;

    .line 1127425
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1127410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127411
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1127412
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1127413
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1127414
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1127415
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1127416
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1127417
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1127418
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1127419
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1127420
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1127421
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1127422
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127423
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1127402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1127403
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1127404
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    .line 1127405
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1127406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    .line 1127407
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel$IconImageModel;

    .line 1127408
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1127409
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1127397
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageFeaturedArtQueryModels$FetchMontageFeaturedArtQueryModel$MessengerMontageFeaturedSectionModel$FeaturedCameraModel;-><init>()V

    .line 1127398
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1127399
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1127401
    const v0, 0x6999b4ab

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1127400
    const v0, -0xe9df49

    return v0
.end method
