.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5edac088
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:D

.field private h:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1123989
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1123964
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1123987
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1123988
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1123979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1123980
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1123981
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1123982
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1123983
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->g:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1123984
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->h:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1123985
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1123986
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1123976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1123977
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1123978
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1123970
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1123971
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->e:D

    .line 1123972
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->f:D

    .line 1123973
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->g:D

    .line 1123974
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;->h:D

    .line 1123975
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1123967
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;-><init>()V

    .line 1123968
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1123969
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1123966
    const v0, -0x34f5097

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1123965
    const v0, -0x65db1f3e

    return v0
.end method
