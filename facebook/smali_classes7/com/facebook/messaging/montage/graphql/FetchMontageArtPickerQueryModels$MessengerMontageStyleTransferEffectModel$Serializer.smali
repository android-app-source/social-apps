.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1124619
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1124620
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1124621
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1124610
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1124611
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1124612
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1124613
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1124614
    if-eqz v2, :cond_0

    .line 1124615
    const-string p0, "style_transfer_effect"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1124616
    invoke-static {v1, v2, p1, p2}, LX/6h1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1124617
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1124618
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1124609
    check-cast p1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel$Serializer;->a(Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageStyleTransferEffectModel;LX/0nX;LX/0my;)V

    return-void
.end method
