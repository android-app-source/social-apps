.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1121699
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1121700
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1121697
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1121698
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1121595
    if-nez p1, :cond_0

    move v0, v1

    .line 1121596
    :goto_0
    return v0

    .line 1121597
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1121598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1121599
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121600
    const v2, -0x70ff4931

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1121601
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121602
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121603
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1121604
    :sswitch_1
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel;

    .line 1121605
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1121606
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121607
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121608
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1121609
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121610
    const v2, 0x72cada16

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1121611
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121612
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121613
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1121614
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121615
    const v2, -0x6a7c8762

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1121616
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121617
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121618
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1121619
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121620
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121621
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1121622
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1121623
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1121624
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1121625
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1121626
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121627
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1121628
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1121629
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121630
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121631
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121632
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121633
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121634
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121635
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121636
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121637
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121638
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121639
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121640
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121641
    const v2, 0x34f405de

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1121642
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1121643
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1121644
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1121645
    const v4, 0x59177caa

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1121646
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1121647
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121648
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1121649
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1121650
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121651
    :sswitch_8
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    .line 1121652
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1121653
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121654
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121655
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121656
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121657
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121658
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1121659
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1121660
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121661
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1121662
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121663
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121664
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121665
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1121666
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1121667
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1121668
    const v4, 0x3a14f848

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1121669
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1121670
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121671
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1121672
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1121673
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121674
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121675
    const v2, -0x7c5cc8ab

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1121676
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1121677
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1121678
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1121679
    const v4, -0x51ac1ab2

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1121680
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1121681
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121682
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1121683
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1121684
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121685
    :sswitch_c
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtSearchSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtSearchSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    .line 1121686
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1121687
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1121688
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121689
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1121690
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1121691
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1121692
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1121693
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1121694
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1121695
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1121696
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7c5cc8ab -> :sswitch_c
        -0x729ba91b -> :sswitch_6
        -0x70ff4931 -> :sswitch_1
        -0x6a7c8762 -> :sswitch_a
        -0x51ac1ab2 -> :sswitch_d
        -0x36dc1bf3 -> :sswitch_2
        -0x61a67ea -> :sswitch_5
        0x12bb6eb3 -> :sswitch_4
        0x1d6764f7 -> :sswitch_0
        0x34f405de -> :sswitch_8
        0x3a14f848 -> :sswitch_b
        0x59177caa -> :sswitch_9
        0x72cada16 -> :sswitch_3
        0x78d53af0 -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1121594
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1121589
    if-eqz p0, :cond_0

    .line 1121590
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1121591
    if-eq v0, p0, :cond_0

    .line 1121592
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1121593
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1121564
    sparse-switch p2, :sswitch_data_0

    .line 1121565
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1121566
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121567
    const v1, -0x70ff4931

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1121568
    :goto_0
    :sswitch_1
    return-void

    .line 1121569
    :sswitch_2
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel;

    .line 1121570
    invoke-static {v0, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1121571
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121572
    const v1, 0x72cada16

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1121573
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121574
    const v1, -0x6a7c8762

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1121575
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121576
    const v1, 0x34f405de

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1121577
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1121578
    const v1, 0x59177caa

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1121579
    :sswitch_6
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtPickerSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    .line 1121580
    invoke-static {v0, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1121581
    :sswitch_7
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1121582
    const v1, 0x3a14f848

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1121583
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1121584
    const v1, -0x7c5cc8ab

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1121585
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1121586
    const v1, -0x51ac1ab2

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1121587
    :sswitch_9
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtSearchSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageArtSearchSectionModel$SectionUnitsModel$EdgesModel$NodeModel;

    .line 1121588
    invoke-static {v0, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7c5cc8ab -> :sswitch_9
        -0x729ba91b -> :sswitch_1
        -0x70ff4931 -> :sswitch_2
        -0x6a7c8762 -> :sswitch_7
        -0x51ac1ab2 -> :sswitch_1
        -0x36dc1bf3 -> :sswitch_3
        -0x61a67ea -> :sswitch_1
        0x12bb6eb3 -> :sswitch_1
        0x1d6764f7 -> :sswitch_0
        0x34f405de -> :sswitch_6
        0x3a14f848 -> :sswitch_8
        0x59177caa -> :sswitch_1
        0x72cada16 -> :sswitch_4
        0x78d53af0 -> :sswitch_5
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1121554
    if-nez p1, :cond_0

    move v0, v1

    .line 1121555
    :goto_0
    return v0

    .line 1121556
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1121557
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1121558
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1121559
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1121560
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1121561
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1121562
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1121563
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1121547
    if-eqz p1, :cond_0

    .line 1121548
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1121549
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1121550
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1121551
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1121552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1121553
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1121541
    if-eqz p1, :cond_0

    .line 1121542
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1121543
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    .line 1121544
    if-eq v0, v1, :cond_0

    .line 1121545
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1121546
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1121540
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1121701
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1121702
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1121535
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1121536
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1121537
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1121538
    iput p2, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->b:I

    .line 1121539
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1121534
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1121533
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1121530
    iget v0, p0, LX/1vt;->c:I

    .line 1121531
    move v0, v0

    .line 1121532
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1121527
    iget v0, p0, LX/1vt;->c:I

    .line 1121528
    move v0, v0

    .line 1121529
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1121524
    iget v0, p0, LX/1vt;->b:I

    .line 1121525
    move v0, v0

    .line 1121526
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1121521
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1121522
    move-object v0, v0

    .line 1121523
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1121512
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1121513
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1121514
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1121515
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1121516
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1121517
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1121518
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1121519
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1121520
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1121509
    iget v0, p0, LX/1vt;->c:I

    .line 1121510
    move v0, v0

    .line 1121511
    return v0
.end method
