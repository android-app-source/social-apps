.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2570d53
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1123481
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1123480
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1123457
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1123458
    return-void
.end method

.method private a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1123478
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    .line 1123479
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1123472
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1123473
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1123474
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1123475
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1123476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1123477
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1123464
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1123465
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1123466
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    .line 1123467
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->a()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1123468
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;

    .line 1123469
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel$AnimationAssetsEdgesNodeModel;

    .line 1123470
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1123471
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1123461
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel$AnimationAssetsEdgesModel;-><init>()V

    .line 1123462
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1123463
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1123460
    const v0, 0xc682a24

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1123459
    const v0, -0x1324434e

    return v0
.end method
