.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1121982
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;

    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1121983
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1121981
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1121925
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1121926
    const/4 v2, 0x0

    .line 1121927
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1121928
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121929
    :goto_0
    move v1, v2

    .line 1121930
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1121931
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1121932
    new-instance v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$FetchMontageArtPickerSearchQueryModel;-><init>()V

    .line 1121933
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1121934
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1121935
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1121936
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1121937
    :cond_0
    return-object v1

    .line 1121938
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121939
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1121940
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1121941
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1121942
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1121943
    const-string v4, "search_results"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1121944
    const/4 v3, 0x0

    .line 1121945
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1121946
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121947
    :goto_2
    move v1, v3

    .line 1121948
    goto :goto_1

    .line 1121949
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1121950
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1121951
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1121952
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121953
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1121954
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1121955
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1121956
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1121957
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1121958
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1121959
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1121960
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1121961
    const/4 v5, 0x0

    .line 1121962
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1121963
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121964
    :goto_5
    move v4, v5

    .line 1121965
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1121966
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1121967
    goto :goto_3

    .line 1121968
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1121969
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1121970
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1121971
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1121972
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 1121973
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1121974
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1121975
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 1121976
    const-string p0, "node"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1121977
    invoke-static {p1, v0}, LX/6gU;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 1121978
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1121979
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1121980
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method
