.class public final Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4764aa28
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private A:D

.field private B:D

.field private C:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:D

.field private E:D

.field private F:D

.field private G:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:D

.field private m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:D

.field private p:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I

.field private r:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I

.field private t:D

.field private u:I

.field private v:D

.field private w:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:D

.field private y:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1124320
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1124321
    const-class v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1124322
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1124323
    return-void
.end method

.method private j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124324
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    .line 1124325
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124326
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    .line 1124327
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124328
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1124329
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124330
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    .line 1124331
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124332
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1124333
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124334
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    .line 1124335
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    return-object v0
.end method

.method private p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124336
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    .line 1124337
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    return-object v0
.end method

.method private q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124316
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    .line 1124317
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    return-object v0
.end method

.method private r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124318
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    .line 1124319
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    return-object v0
.end method

.method private s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124314
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    .line 1124315
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    return-object v0
.end method

.method private t()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124312
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    .line 1124313
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    return-object v0
.end method

.method private u()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124310
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->y:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->y:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    .line 1124311
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->y:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    return-object v0
.end method

.method private v()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124308
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->C:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->C:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    .line 1124309
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->C:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    return-object v0
.end method

.method private w()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124306
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->G:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    iput-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->G:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    .line 1124307
    iget-object v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->G:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 1124259
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1124260
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1124261
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1124262
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1124263
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1124264
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1124265
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1124266
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1124267
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1124268
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1124269
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1124270
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1124271
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1124272
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1124273
    invoke-direct/range {p0 .. p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1124274
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1124275
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1124276
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->f:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124277
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1124278
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1124279
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1124280
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1124281
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1124282
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->l:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124283
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1124284
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1124285
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124286
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1124287
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1124288
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124289
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1124290
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124291
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1124292
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124293
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124294
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->x:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124295
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124296
    const/16 v2, 0x15

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->z:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1124297
    const/16 v3, 0x16

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->A:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124298
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->B:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124299
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124300
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->D:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124301
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->E:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124302
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->F:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1124303
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1124304
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1124305
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1124196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1124197
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1124198
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    .line 1124199
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->j()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1124200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124201
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->e:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$AnimationAssetsModel;

    .line 1124202
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1124203
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    .line 1124204
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1124205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124206
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->g:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$EmitterAssetsModel;

    .line 1124207
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1124208
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    .line 1124209
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1124210
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124211
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->i:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$GravityModel;

    .line 1124212
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1124213
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    .line 1124214
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1124215
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124216
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->k:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxPositionModel;

    .line 1124217
    :cond_3
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1124218
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    .line 1124219
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1124220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124221
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->m:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMaxVelocityModel;

    .line 1124222
    :cond_4
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1124223
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    .line 1124224
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1124225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124226
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinPositionModel;

    .line 1124227
    :cond_5
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1124228
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    .line 1124229
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1124230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124231
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->p:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$InitMinVelocityModel;

    .line 1124232
    :cond_6
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1124233
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    .line 1124234
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1124235
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124236
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->r:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxColorHsvaModel;

    .line 1124237
    :cond_7
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1124238
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    .line 1124239
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1124240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124241
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MaxPositionModel;

    .line 1124242
    :cond_8
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1124243
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    .line 1124244
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1124245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124246
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->y:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinColorHsvaModel;

    .line 1124247
    :cond_9
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1124248
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    .line 1124249
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1124250
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124251
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->C:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$MinPositionModel;

    .line 1124252
    :cond_a
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1124253
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    .line 1124254
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->w()Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1124255
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    .line 1124256
    iput-object v0, v1, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->G:Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel$VelocityScalarModel;

    .line 1124257
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1124258
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1124195
    invoke-direct {p0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1124178
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1124179
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->f:D

    .line 1124180
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->l:D

    .line 1124181
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->o:D

    .line 1124182
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->q:I

    .line 1124183
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->s:I

    .line 1124184
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->t:D

    .line 1124185
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->u:I

    .line 1124186
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->v:D

    .line 1124187
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->x:D

    .line 1124188
    const/16 v0, 0x15

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->z:I

    .line 1124189
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->A:D

    .line 1124190
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->B:D

    .line 1124191
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->D:D

    .line 1124192
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->E:D

    .line 1124193
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;->F:D

    .line 1124194
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1124173
    new-instance v0, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/messaging/montage/graphql/FetchMontageArtPickerQueryModels$MessengerMontageParticleEffectModel$ParticleEffectModel$EmittersModel$EdgesModel$NodeModel;-><init>()V

    .line 1124174
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1124175
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1124177
    const v0, 0x2a373a03

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1124176
    const v0, 0x2afc3f97

    return v0
.end method
