.class public Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;
.super Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:LX/6eo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6eo",
            "<",
            "Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6ez;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118718
    new-instance v0, LX/6ey;

    invoke-direct {v0}, LX/6ey;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->CREATOR:LX/6eo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/6ez;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1118708
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;-><init>()V

    .line 1118709
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    .line 1118710
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    .line 1118711
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    .line 1118712
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    .line 1118713
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    .line 1118714
    iput-object p6, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    .line 1118715
    iput-object p7, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    .line 1118716
    iput-object p8, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    .line 1118717
    return-void
.end method

.method public static a(Lorg/json/JSONArray;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "LX/0Px",
            "<",
            "LX/6ez;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118696
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1118697
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1118698
    :try_start_0
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118699
    :try_start_1
    const-string v3, "score_str"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1118700
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1118701
    const-string v3, "score"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1118702
    :cond_0
    const-string v4, "id"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "name"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "rank"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, LX/6ez;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6ez;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v3

    .line 1118703
    :goto_1
    move-object v2, v3

    .line 1118704
    if-eqz v2, :cond_1

    .line 1118705
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1118706
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1118707
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_2

    :catch_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;
    .locals 9
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/6ez;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118693
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1118694
    :cond_0
    const/4 v0, 0x0

    .line 1118695
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118688
    const/4 v5, 0x0

    .line 1118689
    if-eqz p5, :cond_0

    .line 1118690
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1118691
    invoke-static {v0}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a(Lorg/json/JSONArray;)LX/0Px;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :cond_0
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p6

    move-object v7, p7

    .line 1118692
    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_0
.end method

.method private b()Lorg/json/JSONArray;
    .locals 7

    .prologue
    .line 1118674
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    if-nez v0, :cond_0

    .line 1118675
    const/4 v0, 0x0

    .line 1118676
    :goto_0
    return-object v0

    .line 1118677
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1118678
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ez;

    .line 1118679
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1118680
    const-string v5, "id"

    iget-object v6, v0, LX/6ez;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118681
    const-string v5, "name"

    iget-object v6, v0, LX/6ez;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118682
    const-string v5, "score_str"

    iget-object v6, v0, LX/6ez;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118683
    const-string v5, "rank"

    iget-object v6, v0, LX/6ez;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118684
    :goto_2
    move-object v0, v4

    .line 1118685
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1118686
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1118687
    goto :goto_0

    :catch_0
    const/4 v4, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 1118664
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1118665
    :try_start_0
    const-string v1, "game_id"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118666
    const-string v1, "update_type"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118667
    const-string v1, "game_name"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118668
    const-string v1, "game_icon"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118669
    const-string v1, "score"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118670
    const-string v1, "leaderboard"

    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b()Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118671
    const-string v1, "collapsed_text"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118672
    const-string v1, "expanded_text"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118673
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118663
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1118647
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    if-nez v1, :cond_1

    .line 1118648
    :cond_0
    :goto_0
    return v0

    .line 1118649
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;

    .line 1118650
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1118662
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->f:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1118651
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118652
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118653
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118654
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118655
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118656
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->b()Lorg/json/JSONArray;

    move-result-object v0

    .line 1118657
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118658
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118659
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118660
    return-void

    .line 1118661
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
