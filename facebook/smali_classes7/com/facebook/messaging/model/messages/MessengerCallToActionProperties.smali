.class public Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;
.super Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
.source ""


# static fields
.field public static final CREATOR:LX/6eo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6eo",
            "<",
            "Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119618
    new-instance v0, LX/6fE;

    invoke-direct {v0}, LX/6fE;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->CREATOR:LX/6eo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V
    .locals 0

    .prologue
    .line 1119599
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;-><init>()V

    .line 1119600
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    .line 1119601
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1119602
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;
    .locals 2

    .prologue
    .line 1119615
    const/4 v0, 0x0

    .line 1119616
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/4gg;->a(LX/0lF;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1119617
    :goto_0
    new-instance v1, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;

    invoke-direct {v1, p0, v0}, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;-><init>(Ljava/lang/String;Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    return-object v1

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 1119611
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1119612
    :try_start_0
    const-string v1, "item_title"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1119613
    const-string v1, "call_to_action"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-static {v2}, LX/4gg;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1119614
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119619
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1119607
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;

    if-nez v1, :cond_1

    .line 1119608
    :cond_0
    :goto_0
    return v0

    .line 1119609
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;

    .line 1119610
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1119606
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-static {v2}, LX/4gg;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/0m9;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119603
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119604
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->b:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119605
    return-void
.end method
