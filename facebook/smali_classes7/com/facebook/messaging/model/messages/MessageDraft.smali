.class public Lcom/facebook/messaging/model/messages/MessageDraft;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/MessageDraft;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119290
    new-instance v0, LX/6f8;

    invoke-direct {v0}, LX/6f8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/MessageDraft;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119292
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    .line 1119293
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    .line 1119294
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    .line 1119295
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    .line 1119296
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1119297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119298
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    .line 1119299
    iput p2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    .line 1119300
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    .line 1119301
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    .line 1119302
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1119303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119304
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119305
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    .line 1119306
    iput p2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    .line 1119307
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    .line 1119308
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    .line 1119309
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119310
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1119311
    if-ne p0, p1, :cond_1

    .line 1119312
    :cond_0
    :goto_0
    return v0

    .line 1119313
    :cond_1
    instance-of v2, p1, Lcom/facebook/messaging/model/messages/MessageDraft;

    if-nez v2, :cond_2

    move v0, v1

    .line 1119314
    goto :goto_0

    .line 1119315
    :cond_2
    check-cast p1, Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 1119316
    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1119317
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1119318
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1119319
    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    move-object v2, v2

    .line 1119320
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    move-object v3, v3

    .line 1119321
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1119322
    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1119323
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1119324
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1119325
    iget v2, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    move v2, v2

    .line 1119326
    iget v3, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    move v3, v3

    .line 1119327
    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119328
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119329
    iget v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119330
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1119331
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119332
    return-void
.end method
