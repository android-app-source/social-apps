.class public final Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final eventBeforeTime:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_seconds_to_notify_before"
    .end annotation
.end field

.field public final eventCreatorId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_creator_id"
    .end annotation
.end field

.field public final eventId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_id"
    .end annotation
.end field

.field public final eventLocationName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_location_name"
    .end annotation
.end field

.field public final eventTime:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_time"
    .end annotation
.end field

.field public final eventTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_title"
    .end annotation
.end field

.field public final eventTrackRsvp:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_track_rsvp"
    .end annotation
.end field

.field public final eventType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "event_type"
    .end annotation
.end field

.field public final guestId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "guest_id"
    .end annotation
.end field

.field public final guestStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "guest_status"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118251
    new-instance v0, LX/6es;

    invoke-direct {v0}, LX/6es;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1118252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    .line 1118254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    .line 1118255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventBeforeTime:Ljava/lang/String;

    .line 1118256
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    .line 1118257
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestId:Ljava/lang/String;

    .line 1118258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestStatus:Ljava/lang/String;

    .line 1118259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    .line 1118260
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    .line 1118261
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventCreatorId:Ljava/lang/String;

    .line 1118262
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventLocationName:Ljava/lang/String;

    .line 1118263
    return-void

    .line 1118264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1118265
    invoke-direct {p0, p1}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_id"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_type"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_time"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_seconds_to_notify_before"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_title"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_creator_id"
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "guest_id"
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "guest_status"
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_track_rsvp"
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "event_location_name"
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 1118266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118267
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    .line 1118268
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    .line 1118269
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    .line 1118270
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventBeforeTime:Ljava/lang/String;

    .line 1118271
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    .line 1118272
    iput-object p6, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventCreatorId:Ljava/lang/String;

    .line 1118273
    iput-object p7, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestId:Ljava/lang/String;

    .line 1118274
    iput-object p8, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestStatus:Ljava/lang/String;

    .line 1118275
    iput-object p9, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    .line 1118276
    iput-object p10, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventLocationName:Ljava/lang/String;

    .line 1118277
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;
    .locals 11
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118278
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118279
    const/4 v0, 0x0

    .line 1118280
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118281
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1118282
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118283
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118284
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventBeforeTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118285
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118286
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118287
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118288
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118289
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118290
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventCreatorId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118291
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventLocationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118292
    return-void
.end method
