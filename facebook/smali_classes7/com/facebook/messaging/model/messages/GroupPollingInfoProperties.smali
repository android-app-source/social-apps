.class public Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;
.super Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
.source ""


# static fields
.field public static final CREATOR:LX/6eo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6eo",
            "<",
            "Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Z

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6ex;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118616
    new-instance v0, LX/6ew;

    invoke-direct {v0}, LX/6ew;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->CREATOR:LX/6eo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;IZLX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ",
            "LX/0Px",
            "<",
            "LX/6ex;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1118609
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;-><init>()V

    .line 1118610
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    .line 1118611
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    .line 1118612
    iput p3, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    .line 1118613
    iput-boolean p4, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    .line 1118614
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    .line 1118615
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IZLX/0Px;)Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ",
            "LX/0Px",
            "<",
            "LX/6ex;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;"
        }
    .end annotation

    .prologue
    .line 1118548
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118549
    const/4 v0, 0x0

    .line 1118550
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;-><init>(Ljava/lang/String;Ljava/lang/String;IZLX/0Px;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1118592
    const/4 v1, 0x0

    .line 1118593
    if-eqz p4, :cond_2

    .line 1118594
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1118595
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1118596
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118597
    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1118598
    :try_start_2
    const-string v5, "voters"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 1118599
    const-string v6, "text"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string p4, "total_count"

    invoke-virtual {v4, p4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p4

    invoke-static {v5}, LX/16N;->b(Lorg/json/JSONArray;)LX/0Px;

    move-result-object v5

    invoke-static {v6, p4, v5}, LX/6ex;->a(Ljava/lang/String;ILX/0Px;)LX/6ex;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    move-result-object v5

    .line 1118600
    :goto_1
    move-object v4, v5

    .line 1118601
    if-eqz v4, :cond_0

    .line 1118602
    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1118603
    :cond_0
    :goto_2
    :try_start_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1118604
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1118605
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v0

    .line 1118606
    :cond_2
    :goto_3
    invoke-static {p0, p1, v0, p3, v1}, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a(Ljava/lang/String;Ljava/lang/String;IZLX/0Px;)Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;

    move-result-object v0

    return-object v0

    .line 1118607
    :catch_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1118608
    goto :goto_3

    :catch_1
    :try_start_5
    goto :goto_2
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private b()Lorg/json/JSONArray;
    .locals 10

    .prologue
    .line 1118573
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    if-nez v0, :cond_0

    .line 1118574
    const/4 v0, 0x0

    .line 1118575
    :goto_0
    return-object v0

    .line 1118576
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1118577
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ex;

    .line 1118578
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1118579
    const-string v5, "text"

    iget-object v6, v0, LX/6ex;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118580
    const-string v5, "total_count"

    iget v6, v0, LX/6ex;->b:I

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1118581
    const-string v5, "voters"

    .line 1118582
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 1118583
    iget-object v6, v0, LX/6ex;->c:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_2
    if-ge v7, v9, :cond_1

    iget-object v6, v0, LX/6ex;->c:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1118584
    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1118585
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 1118586
    :cond_1
    move-object v6, v8

    .line 1118587
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118588
    :goto_3
    move-object v0, v4

    .line 1118589
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1118590
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1118591
    goto :goto_0

    :catch_0
    const/4 v4, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 1118566
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1118567
    :try_start_0
    const-string v1, "id"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118568
    const-string v1, "text"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118569
    const-string v1, "total_count"

    iget v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1118570
    const-string v1, "viewer_has_voted"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1118571
    const-string v1, "options"

    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b()Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118572
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118565
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1118561
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;

    if-nez v1, :cond_1

    .line 1118562
    :cond_0
    :goto_0
    return v0

    .line 1118563
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;

    .line 1118564
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    iget v2, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1118560
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->e:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1118551
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118552
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118553
    iget v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118554
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1118555
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->b()Lorg/json/JSONArray;

    move-result-object v0

    .line 1118556
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118557
    return-void

    .line 1118558
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1118559
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
