.class public final Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final participantId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "participantId"
    .end annotation
.end field

.field public final suggestions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118328
    new-instance v0, LX/6eu;

    invoke-direct {v0}, LX/6eu;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1118324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118325
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->participantId:Ljava/lang/String;

    .line 1118326
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->suggestions:LX/0Px;

    .line 1118327
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "participantId"
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "suggestions"
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1118320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118321
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->participantId:Ljava/lang/String;

    .line 1118322
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->suggestions:LX/0Px;

    .line 1118323
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118319
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1118316
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118317
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->suggestions:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1118318
    return-void
.end method
