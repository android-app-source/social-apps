.class public final Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description"
    .end annotation
.end field

.field public final iconUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "icon"
    .end annotation
.end field

.field public final id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "botID"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118226
    new-instance v0, LX/6er;

    invoke-direct {v0}, LX/6er;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # J
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "botID"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "title"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "description"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "icon"
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 1118227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118228
    iput-wide p1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->id:J

    .line 1118229
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->title:Ljava/lang/String;

    .line 1118230
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->description:Ljava/lang/String;

    .line 1118231
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->iconUri:Ljava/lang/String;

    .line 1118232
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1118233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118234
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->id:J

    .line 1118235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->title:Ljava/lang/String;

    .line 1118236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->description:Ljava/lang/String;

    .line 1118237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->iconUri:Ljava/lang/String;

    .line 1118238
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1118239
    invoke-direct {p0, p1}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118240
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1118241
    iget-wide v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118242
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118243
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118244
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->iconUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118245
    return-void
.end method
