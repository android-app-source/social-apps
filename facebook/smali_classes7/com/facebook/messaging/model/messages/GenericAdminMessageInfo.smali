.class public Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public final d:I

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:I

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:Z

.field public u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/6et;

.field public w:Z

.field public final x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1118453
    invoke-static {}, LX/0Rh;->d()LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CONFIRM_FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ACCEPT_PENDING_THREAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RAMP_UP_WELCOME_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INVITE_ACCEPTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INVITE_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TURN_ON_PUSH:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_COLOR:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_LIKE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ORDERED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_BOT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_MANAGE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_JOINABLE_SETTING:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_JOINABLE_PROMOTION_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOINABLE_GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADD_CONTACT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_CREATE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_DECLINE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_REQUEST_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_USER_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->POKE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    invoke-virtual {v0}, LX/4y1;->a()LX/0Rh;

    move-result-object v0

    .line 1118454
    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a:LX/0Rh;

    invoke-virtual {v0}, LX/0Rh;->e()LX/0Rh;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->b:LX/0Rh;

    .line 1118455
    new-instance v0, LX/6ep;

    invoke-direct {v0}, LX/6ep;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1118412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118413
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 1118414
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    .line 1118415
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    .line 1118416
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    .line 1118417
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    .line 1118418
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->h:I

    .line 1118419
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ltz v0, :cond_0

    .line 1118420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1118421
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1118422
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    .line 1118423
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1118424
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1118425
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1118426
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    .line 1118427
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ltz v0, :cond_2

    .line 1118428
    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    .line 1118429
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v2

    .line 1118430
    :goto_3
    if-eqz v0, :cond_4

    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    .line 1118431
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->m:Ljava/lang/String;

    .line 1118432
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    .line 1118433
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_5

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o:Z

    .line 1118434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p:Ljava/lang/String;

    .line 1118435
    const-class v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->q:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 1118436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    .line 1118437
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->s:I

    .line 1118438
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    :goto_6
    iput-boolean v2, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->t:Z

    .line 1118439
    const-class v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    .line 1118440
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6et;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->v:LX/6et;

    .line 1118441
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->w:Z

    .line 1118442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->x:Ljava/lang/String;

    .line 1118443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->y:Ljava/lang/String;

    .line 1118444
    const-class v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    .line 1118445
    return-void

    .line 1118446
    :cond_0
    iput-object v1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    goto/16 :goto_0

    .line 1118447
    :cond_1
    iput-object v1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    goto/16 :goto_1

    .line 1118448
    :cond_2
    iput-object v1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    goto/16 :goto_2

    :cond_3
    move v0, v3

    .line 1118449
    goto/16 :goto_3

    :cond_4
    move-object v0, v1

    .line 1118450
    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 1118451
    goto :goto_5

    :cond_6
    move v2, v3

    .line 1118452
    goto :goto_6
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Px;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;Ljava/lang/String;IZLcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;LX/6et;ZLjava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;)V
    .locals 1
    .param p16    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$NicknameChoice;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$BotChoice;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;",
            "Ljava/lang/String;",
            "IZ",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;",
            "LX/6et;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1118386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118387
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 1118388
    iput p2, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    .line 1118389
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    .line 1118390
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    .line 1118391
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    .line 1118392
    iput p6, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->h:I

    .line 1118393
    iput-object p7, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    .line 1118394
    iput-object p8, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    .line 1118395
    iput-object p9, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    .line 1118396
    iput-object p10, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    .line 1118397
    iput-object p11, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->m:Ljava/lang/String;

    .line 1118398
    iput-object p12, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    .line 1118399
    iput-boolean p13, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o:Z

    .line 1118400
    iput-object p14, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p:Ljava/lang/String;

    .line 1118401
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->q:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 1118402
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    .line 1118403
    move/from16 v0, p17

    iput v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->s:I

    .line 1118404
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->t:Z

    .line 1118405
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    .line 1118406
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->v:LX/6et;

    .line 1118407
    move/from16 v0, p21

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->w:Z

    .line 1118408
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->x:Ljava/lang/String;

    .line 1118409
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->y:Ljava/lang/String;

    .line 1118410
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    .line 1118411
    return-void
.end method

.method public static a(I)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;
    .locals 2

    .prologue
    .line 1118385
    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->b:LX/0Rh;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->b:LX/0Rh;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0
.end method

.method public static newBuilder()LX/6ev;
    .locals 1

    .prologue
    .line 1118384
    new-instance v0, LX/6ev;

    invoke-direct {v0}, LX/6ev;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118383
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final B()I
    .locals 1

    .prologue
    .line 1118382
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->s:I

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 1118329
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->t:Z

    return v0
.end method

.method public final E()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118381
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    return-object v0
.end method

.method public final I()Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118378
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    if-nez v0, :cond_0

    .line 1118379
    const/4 v0, 0x0

    .line 1118380
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1118377
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1118376
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1118375
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118374
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 1118373
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1118372
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118371
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118370
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1118369
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 1118368
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->h:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 1118330
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1118331
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118332
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118333
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118334
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118335
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118336
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118337
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    if-eqz v0, :cond_0

    .line 1118338
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1118339
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118340
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    if-eqz v0, :cond_1

    .line 1118341
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1118342
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118343
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    if-eqz v0, :cond_2

    .line 1118344
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1118345
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118346
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    if-eqz v0, :cond_4

    .line 1118347
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1118348
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118349
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118350
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118351
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118352
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->q:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118353
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118354
    iget v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->s:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118355
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->t:Z

    if-eqz v0, :cond_9

    :goto_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118356
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118357
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->v:LX/6et;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1118358
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->w:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1118359
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118360
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118361
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->z:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118362
    return-void

    :cond_5
    move v0, v1

    .line 1118363
    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 1118364
    goto/16 :goto_1

    :cond_7
    move v0, v1

    .line 1118365
    goto :goto_2

    :cond_8
    move v0, v3

    .line 1118366
    goto :goto_3

    :cond_9
    move v2, v3

    .line 1118367
    goto :goto_4
.end method
