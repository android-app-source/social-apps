.class public abstract Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;",
            "LX/6eo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118180
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1118181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118182
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;)LX/6eo;
    .locals 3

    .prologue
    .line 1118183
    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a:LX/0P1;

    if-nez v0, :cond_0

    .line 1118184
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v2, Lcom/facebook/messaging/model/messages/InstantGameInfoProperties;->CREATOR:LX/6eo;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v2, Lcom/facebook/messaging/model/messages/GroupPollingInfoProperties;->CREATOR:LX/6eo;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v2, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->CREATOR:LX/6eo;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v2, Lcom/facebook/messaging/model/messages/MessengerCartInfoProperties;->CREATOR:LX/6eo;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v2, Lcom/facebook/messaging/model/messages/MessengerCallToActionProperties;->CREATOR:LX/6eo;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a:LX/0P1;

    .line 1118185
    :cond_0
    sget-object v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eo;

    return-object v0
.end method


# virtual methods
.method public abstract a()Lorg/json/JSONObject;
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1118186
    const/4 v0, 0x0

    return v0
.end method
