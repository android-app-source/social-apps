.class public Lcom/facebook/messaging/model/messages/Message;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

.field public final B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

.field public final C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

.field public final D:Z

.field public final E:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

.field public final G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final J:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final K:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final L:Lcom/facebook/messaging/model/mms/MmsData;

.field public final M:Z

.field public final N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final O:Z

.field public final P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;",
            ">;"
        }
    .end annotation
.end field

.field public final R:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/5ds;",
            "Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public final S:Z

.field public final T:LX/18f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/18f",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final U:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final V:I

.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final c:J

.field public final d:J

.field public final e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public final f:Ljava/lang/String;

.field public final g:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final h:Z

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/share/Share;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/String;

.field public final l:LX/2uW;

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/lang/String;

.field public final o:Z

.field public final p:Ljava/lang/String;

.field public final q:LX/6f2;

.field public final r:LX/6f3;

.field public final s:Lcom/facebook/messaging/model/messages/Publicity;

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Lcom/facebook/messaging/model/share/SentShareAttachment;

.field public final v:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Lcom/facebook/messaging/model/send/SendError;

.field public final x:Ljava/lang/String;

.field public final y:Ljava/lang/String;

.field public final z:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118781
    new-instance v0, LX/6f1;

    invoke-direct {v0}, LX/6f1;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6f7;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1118939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118940
    iget-object v0, p1, LX/6f7;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1118941
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 1118942
    iget-object v0, p1, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1118943
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1118944
    iget-wide v6, p1, LX/6f7;->c:J

    move-wide v4, v6

    .line 1118945
    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 1118946
    iget-wide v6, p1, LX/6f7;->d:J

    move-wide v4, v6

    .line 1118947
    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 1118948
    iget-object v0, p1, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v0, v0

    .line 1118949
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1118950
    iget-object v0, p1, LX/6f7;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1118951
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 1118952
    iget-wide v6, p1, LX/6f7;->g:J

    move-wide v4, v6

    .line 1118953
    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->g:J

    .line 1118954
    iget-boolean v0, p1, LX/6f7;->h:Z

    move v0, v0

    .line 1118955
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->h:Z

    .line 1118956
    iget-object v0, p1, LX/6f7;->i:Ljava/util/List;

    move-object v0, v0

    .line 1118957
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    .line 1118958
    iget-object v0, p1, LX/6f7;->j:Ljava/util/List;

    move-object v0, v0

    .line 1118959
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    .line 1118960
    iget-object v0, p1, LX/6f7;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1118961
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    .line 1118962
    iget-object v0, p1, LX/6f7;->l:LX/2uW;

    move-object v0, v0

    .line 1118963
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    .line 1118964
    iget-object v0, p1, LX/6f7;->m:Ljava/util/List;

    move-object v0, v0

    .line 1118965
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    .line 1118966
    iget-object v0, p1, LX/6f7;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1118967
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 1118968
    iget-boolean v0, p1, LX/6f7;->o:Z

    move v0, v0

    .line 1118969
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    .line 1118970
    iget-object v0, p1, LX/6f7;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1118971
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    .line 1118972
    iget-object v0, p1, LX/6f7;->q:LX/6f2;

    move-object v0, v0

    .line 1118973
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    .line 1118974
    iget-object v0, p1, LX/6f7;->w:LX/6f3;

    move-object v0, v0

    .line 1118975
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    .line 1118976
    iget-object v0, p1, LX/6f7;->r:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1118977
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1118978
    :goto_0
    move-object v0, v0

    .line 1118979
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 1118980
    iget-object v0, p1, LX/6f7;->s:Lcom/facebook/messaging/model/share/SentShareAttachment;

    move-object v0, v0

    .line 1118981
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 1118982
    iget-object v0, p1, LX/6f7;->t:Ljava/util/Map;

    move-object v0, v0

    .line 1118983
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 1118984
    iget-object v0, p1, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    move-object v0, v0

    .line 1118985
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    .line 1118986
    iget-object v0, p1, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    move-object v0, v0

    .line 1118987
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1118988
    iget-object v0, p1, LX/6f7;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1118989
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    .line 1118990
    iget-object v0, p1, LX/6f7;->y:Ljava/lang/String;

    move-object v0, v0

    .line 1118991
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    .line 1118992
    iget-object v0, p1, LX/6f7;->z:Ljava/util/Map;

    move-object v0, v0

    .line 1118993
    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    .line 1118994
    iget-object v0, p1, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-object v0, v0

    .line 1118995
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 1118996
    iget-boolean v0, p1, LX/6f7;->D:Z

    move v0, v0

    .line 1118997
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->D:Z

    .line 1118998
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    iget-object v3, p0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v3, v3, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    sget-object v4, LX/6fP;->NONE:LX/6fP;

    if-ne v3, v4, :cond_5

    move v3, v1

    :goto_3
    xor-int/2addr v0, v3

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1118999
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1119000
    iget-object v0, p1, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    move-object v0, v0

    .line 1119001
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 1119002
    iget-object v0, p1, LX/6f7;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    move-object v0, v0

    .line 1119003
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 1119004
    iget-object v0, p1, LX/6f7;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-object v0, v0

    .line 1119005
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1119006
    iget-object v0, p1, LX/6f7;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v0, v0

    .line 1119007
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1119008
    iget-object v0, p1, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-object v0, v0

    .line 1119009
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1119010
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1119011
    iget-object v0, p1, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-object v0, v0

    .line 1119012
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 1119013
    iget-object v0, p1, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-object v0, v0

    .line 1119014
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 1119015
    iget-object v0, p1, LX/6f7;->J:Ljava/lang/Integer;

    move-object v0, v0

    .line 1119016
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 1119017
    iget-object v0, p1, LX/6f7;->K:Ljava/lang/Long;

    move-object v0, v0

    .line 1119018
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    .line 1119019
    iget-object v0, p1, LX/6f7;->L:Lcom/facebook/messaging/model/mms/MmsData;

    move-object v0, v0

    .line 1119020
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 1119021
    iget-boolean v0, p1, LX/6f7;->M:Z

    move v0, v0

    .line 1119022
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->M:Z

    .line 1119023
    iget-object v0, p1, LX/6f7;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1119024
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    .line 1119025
    iget-boolean v0, p1, LX/6f7;->O:Z

    move v0, v0

    .line 1119026
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->O:Z

    .line 1119027
    iget-object v0, p1, LX/6f7;->P:Ljava/lang/String;

    move-object v0, v0

    .line 1119028
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    .line 1119029
    iget-object v0, p1, LX/6f7;->Q:Ljava/util/List;

    move-object v0, v0

    .line 1119030
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    .line 1119031
    iget-object v0, p1, LX/6f7;->R:Ljava/util/Map;

    move-object v0, v0

    .line 1119032
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    .line 1119033
    iget-boolean v0, p1, LX/6f7;->S:Z

    move v0, v0

    .line 1119034
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->S:Z

    .line 1119035
    iget-object v0, p1, LX/6f7;->T:LX/0Xu;

    move-object v0, v0

    .line 1119036
    invoke-static {v0}, LX/18f;->c(LX/0Xu;)LX/18f;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    .line 1119037
    iget v0, p1, LX/6f7;->U:I

    move v0, v0

    .line 1119038
    iput v0, p0, Lcom/facebook/messaging/model/messages/Message;->V:I

    .line 1119039
    iget-object v0, p1, LX/6f7;->V:LX/0Px;

    move-object v0, v0

    .line 1119040
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    .line 1119041
    return-void

    .line 1119042
    :cond_3
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 1119043
    goto/16 :goto_2

    :cond_5
    move v3, v2

    goto/16 :goto_3

    :cond_6
    move v0, v2

    .line 1119044
    goto/16 :goto_4

    :cond_7
    iget-object v0, p1, LX/6f7;->r:Ljava/util/List;

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1118868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118869
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 1118870
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1118871
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 1118872
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 1118873
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1118874
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 1118875
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->g:J

    .line 1118876
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->h:Z

    .line 1118877
    const-class v0, Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    .line 1118878
    const-class v0, Lcom/facebook/messaging/model/share/Share;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    .line 1118879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    .line 1118880
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2uW;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    .line 1118881
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    .line 1118882
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 1118883
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    .line 1118884
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    .line 1118885
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6f2;->valueOf(Ljava/lang/String;)LX/6f2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    .line 1118886
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 1118887
    const-class v0, Lcom/facebook/messaging/model/share/SentShareAttachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/share/SentShareAttachment;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 1118888
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 1118889
    const-class v0, Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/send/SendError;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    .line 1118890
    const-class v0, Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Publicity;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1118891
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    .line 1118892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    .line 1118893
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1118894
    const-class v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p1, v0, v3}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;Ljava/lang/Class;)V

    .line 1118895
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    .line 1118896
    const-class v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 1118897
    const-class v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 1118898
    const-class v0, Lcom/facebook/messaging/model/payment/PaymentRequestData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/payment/PaymentRequestData;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 1118899
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->D:Z

    .line 1118900
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1118901
    const-class v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1118902
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 1118903
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1118904
    const-class v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 1118905
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 1118906
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1118907
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v0, LX/6f3;->UNKNOWN:LX/6f3;

    :goto_3
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    .line 1118908
    const-class v0, Lcom/facebook/messaging/model/mms/MmsData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/mms/MmsData;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    .line 1118909
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    :goto_4
    iput-boolean v1, p0, Lcom/facebook/messaging/model/messages/Message;->M:Z

    .line 1118910
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    .line 1118911
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->O:Z

    .line 1118912
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    .line 1118913
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    .line 1118914
    const-class v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    .line 1118915
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1118916
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1118917
    const-class v0, Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1118918
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 1118919
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1118920
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/5ds;->fromRawValue(Ljava/lang/String;)LX/5ds;

    move-result-object v0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1118921
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1118922
    :cond_0
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 1118923
    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    .line 1118924
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->S:Z

    .line 1118925
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    .line 1118926
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1118927
    invoke-static {p1, v1}, LX/46R;->e(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1118928
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1118929
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-interface {v0, v3, v1}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_6

    .line 1118930
    :cond_1
    invoke-static {v0}, LX/18f;->c(LX/0Xu;)LX/18f;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    .line 1118931
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/Message;->V:I

    .line 1118932
    sget-object v0, Lcom/facebook/messaging/model/messages/ProfileRange;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    .line 1118933
    return-void

    :cond_2
    move v0, v2

    .line 1118934
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1118935
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 1118936
    goto/16 :goto_2

    .line 1118937
    :cond_5
    invoke-static {v0}, LX/6f3;->valueOf(Ljava/lang/String;)LX/6f3;

    move-result-object v0

    goto/16 :goto_3

    :cond_6
    move v1, v2

    .line 1118938
    goto/16 :goto_4
.end method

.method public static newBuilder()LX/6f7;
    .locals 1

    .prologue
    .line 1118867
    new-instance v0, LX/6f7;

    invoke-direct {v0}, LX/6f7;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1118866
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v0, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118865
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 1118845
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1118846
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1118847
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1118848
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1118849
    :goto_0
    return-object v0

    .line 1118850
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1118851
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1118852
    :cond_1
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1118853
    const-string v1, " t: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1118854
    const-string v1, " st: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1118855
    const-string v1, " na: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1118856
    const-string v1, " ua: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/messaging/model/messages/Message;->D:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1118857
    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1118858
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 1118859
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1118860
    const-string v1, "[empty]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1118861
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1118862
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_3

    .line 1118863
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1118864
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1118782
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118783
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118784
    iget-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118785
    iget-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118786
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118787
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118788
    iget-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1118789
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->h:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118790
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118791
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118792
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118793
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1118794
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118795
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118796
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118797
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118798
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v0}, LX/6f2;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118799
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118800
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118801
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1118802
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118803
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118804
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118805
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118806
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1118807
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118808
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118809
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118810
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->D:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118811
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118812
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118813
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1118814
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118815
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118816
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1118817
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    invoke-virtual {v0}, LX/6f3;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118818
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1118819
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->M:Z

    if-eqz v0, :cond_5

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118820
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118821
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->O:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1118822
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118823
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1118824
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118825
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    .line 1118826
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1118827
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5ds;

    .line 1118828
    iget-object v1, v1, LX/5ds;->value:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1118829
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 1118830
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1118831
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1118832
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/Message;->S:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1118833
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    .line 1118834
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1118835
    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1118836
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 1118837
    :cond_1
    invoke-static {p1, v2}, LX/46R;->d(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1118838
    iget v0, p0, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1118839
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0Px;)V

    .line 1118840
    return-void

    :cond_2
    move v0, v2

    .line 1118841
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1118842
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 1118843
    goto/16 :goto_2

    :cond_5
    move v1, v2

    .line 1118844
    goto/16 :goto_3
.end method
