.class public Lcom/facebook/messaging/model/messages/ProfileRange;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "i"
    .end annotation
.end field

.field public final length:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "l"
    .end annotation
.end field

.field public final offset:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "o"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119730
    new-instance v0, LX/6fI;

    invoke-direct {v0}, LX/6fI;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/ProfileRange;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119726
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->id:Ljava/lang/String;

    .line 1119727
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->offset:I

    .line 1119728
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->length:I

    .line 1119729
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "i"
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "o"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
            value = "l"
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 1119720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119721
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->id:Ljava/lang/String;

    .line 1119722
    iput p2, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->offset:I

    .line 1119723
    iput p3, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->length:I

    .line 1119724
    return-void
.end method

.method public static a(LX/0lC;Ljava/lang/String;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ProfileRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1119710
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1119711
    const/4 v0, 0x0

    .line 1119712
    :try_start_0
    invoke-virtual {p0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1119713
    :goto_0
    if-eqz v0, :cond_0

    .line 1119714
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1119715
    new-instance v3, Lcom/facebook/messaging/model/messages/ProfileRange;

    const-string v4, "i"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "o"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->d(LX/0lF;)I

    move-result v5

    const-string v6, "l"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    invoke-direct {v3, v4, v5, v0}, Lcom/facebook/messaging/model/messages/ProfileRange;-><init>(Ljava/lang/String;II)V

    .line 1119716
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1119717
    :catch_0
    move-exception v2

    .line 1119718
    const-class v3, Lcom/facebook/messaging/model/messages/ProfileRange;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error while parsing ProfileRanges"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1119719
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0m9;
    .locals 3

    .prologue
    .line 1119700
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1119701
    const-string v1, "o"

    iget v2, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->offset:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1119702
    const-string v1, "l"

    iget v2, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->length:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1119703
    const-string v1, "i"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1119704
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119709
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119705
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119706
    iget v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->offset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119707
    iget v0, p0, Lcom/facebook/messaging/model/messages/ProfileRange;->length:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119708
    return-void
.end method
