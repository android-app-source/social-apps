.class public Lcom/facebook/messaging/model/messages/Publicity;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/Publicity;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/messages/Publicity;

.field public static final b:Lcom/facebook/messaging/model/messages/Publicity;

.field public static final c:Lcom/facebook/messaging/model/messages/Publicity;

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/messages/Publicity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1119734
    new-instance v0, Lcom/facebook/messaging/model/messages/Publicity;

    const-string v1, "unknown"

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messages/Publicity;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119735
    new-instance v0, Lcom/facebook/messaging/model/messages/Publicity;

    const-string v1, "local only"

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messages/Publicity;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/model/messages/Publicity;->b:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119736
    new-instance v0, Lcom/facebook/messaging/model/messages/Publicity;

    const-string v1, "from server"

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messages/Publicity;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/model/messages/Publicity;->c:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119737
    sget-object v0, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    sget-object v1, Lcom/facebook/messaging/model/messages/Publicity;->b:Lcom/facebook/messaging/model/messages/Publicity;

    sget-object v2, Lcom/facebook/messaging/model/messages/Publicity;->c:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/messages/Publicity;->d:LX/0Rf;

    .line 1119738
    new-instance v0, LX/6fJ;

    invoke-direct {v0}, LX/6fJ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/Publicity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119740
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    .line 1119741
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1119742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119743
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    .line 1119744
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119745
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1119746
    if-ne p0, p1, :cond_0

    .line 1119747
    const/4 v0, 0x1

    .line 1119748
    :goto_0
    return v0

    .line 1119749
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 1119750
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1119751
    :cond_2
    check-cast p1, Lcom/facebook/messaging/model/messages/Publicity;

    .line 1119752
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1119753
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119754
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119755
    return-void
.end method
