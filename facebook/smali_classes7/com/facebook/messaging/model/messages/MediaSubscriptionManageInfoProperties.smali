.class public Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;
.super Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;
.source ""


# static fields
.field public static final CREATOR:LX/6eo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6eo",
            "<",
            "Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118759
    new-instance v0, LX/6f0;

    invoke-direct {v0}, LX/6f0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->CREATOR:LX/6eo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1118752
    invoke-direct {p0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;-><init>()V

    .line 1118753
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    .line 1118754
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    .line 1118755
    iput-object p3, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    .line 1118756
    iput-object p4, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    .line 1118757
    iput-object p5, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    .line 1118758
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118750
    invoke-static {p2}, LX/4gg;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 1118751
    new-instance v0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;

    invoke-static {p4}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 1118742
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1118743
    :try_start_0
    const-string v0, "page_name"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118744
    const-string v0, "page_profile_pic_url"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118745
    const-string v0, "ctas_json"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    invoke-static {v2}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118746
    const-string v0, "collapsed_manage_description"

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1118747
    const-string v2, "message_state"

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118748
    :goto_1
    return-object v1

    .line 1118749
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1118760
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1118738
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;

    if-nez v1, :cond_1

    .line 1118739
    :cond_0
    :goto_0
    return v0

    .line 1118740
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;

    .line 1118741
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    invoke-static {v1}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    invoke-static {v2}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1118737
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    invoke-static {v2}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1118730
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118731
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118732
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->c:LX/0Px;

    invoke-static {v0}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118733
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118734
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MediaSubscriptionManageInfoProperties;->e:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1118735
    return-void

    .line 1118736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
