.class public Lcom/facebook/messaging/model/messages/MessagesCollection;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119413
    new-instance v0, LX/6fB;

    invoke-direct {v0}, LX/6fB;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1119426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119427
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119428
    sget-object v0, Lcom/facebook/messaging/model/messages/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    .line 1119429
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    .line 1119430
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    const/4 v1, 0x0

    invoke-static {v0, v2, v2, v1, v2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(LX/0Px;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V

    .line 1119431
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1119420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119421
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119422
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    .line 1119423
    iput-boolean p3, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    .line 1119424
    const/4 v0, 0x0

    invoke-static {p2, v1, v1, v0, v1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(LX/0Px;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V

    .line 1119425
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;ZLcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V
    .locals 0
    .param p4    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;Z",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Z",
            "Lcom/facebook/user/model/User;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1119414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119415
    iput-object p1, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119416
    iput-object p2, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    .line 1119417
    iput-boolean p3, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    .line 1119418
    invoke-static {p2, p4, p5, p6, p7}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(LX/0Px;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V

    .line 1119419
    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 4

    .prologue
    .line 1119412
    new-instance v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1119402
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1119403
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1119404
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v0

    .line 1119405
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1119406
    const-string v5, "{"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1119407
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v0, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1119408
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1119409
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1119410
    :cond_0
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1119411
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0Px;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;ZLcom/facebook/user/model/User;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Z",
            "Lcom/facebook/user/model/User;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1119391
    invoke-static {p0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1119392
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p4, :cond_0

    .line 1119393
    iget-boolean v0, p4, Lcom/facebook/user/model/User;->o:Z

    move v0, v0

    .line 1119394
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x64

    if-le v0, v1, :cond_4

    .line 1119395
    :cond_2
    const-string v0, "Thread messages were not in order"

    .line 1119396
    :goto_0
    move-object v0, v0

    .line 1119397
    const-string v1, "MessagesCollection"

    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119398
    :cond_3
    return-void

    .line 1119399
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1119400
    const-string v1, "Thread messages were not in order, requireOverlap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, ", newMessages="

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, ", oldMessages="

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1119401
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0Px;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const/4 v6, 0x0

    .line 1119383
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    move-wide v4, v2

    :goto_0
    if-ge v7, v8, :cond_1

    invoke-virtual {p0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 1119384
    iget-boolean v1, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v1, :cond_2

    .line 1119385
    iget-wide v10, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v1, v10, v4

    if-lez v1, :cond_0

    invoke-static {v0}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    move v0, v6

    .line 1119386
    :goto_1
    return v0

    .line 1119387
    :cond_0
    iget-wide v2, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 1119388
    invoke-static {v0}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v0

    .line 1119389
    :goto_2
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move-wide v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 1119390
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    move-wide v0, v2

    move-wide v2, v4

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 1

    .prologue
    .line 1119382
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1119432
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v0, v0

    .line 1119433
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1119381
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    return-object v0
.end method

.method public final b(I)Lcom/facebook/messaging/model/messages/Message;
    .locals 1

    .prologue
    .line 1119380
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    return-object v0
.end method

.method public final c()Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1119379
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method

.method public final d()Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1119378
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119377
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1119370
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1119376
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1119375
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119371
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119372
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1119373
    iget-boolean v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1119374
    return-void
.end method
