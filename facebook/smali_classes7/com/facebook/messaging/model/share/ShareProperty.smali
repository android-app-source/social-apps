.class public Lcom/facebook/messaging/model/share/ShareProperty;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/share/ShareProperty;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120051
    new-instance v0, LX/6fY;

    invoke-direct {v0}, LX/6fY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/share/ShareProperty;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fZ;)V
    .locals 1

    .prologue
    .line 1120052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120053
    iget-object v0, p1, LX/6fZ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1120054
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->a:Ljava/lang/String;

    .line 1120055
    iget-object v0, p1, LX/6fZ;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1120056
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->b:Ljava/lang/String;

    .line 1120057
    iget-object v0, p1, LX/6fZ;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1120058
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->c:Ljava/lang/String;

    .line 1120059
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120061
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->a:Ljava/lang/String;

    .line 1120062
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->b:Ljava/lang/String;

    .line 1120063
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->c:Ljava/lang/String;

    .line 1120064
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120065
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120066
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120067
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120068
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareProperty;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120069
    return-void
.end method
