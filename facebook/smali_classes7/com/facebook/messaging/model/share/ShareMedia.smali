.class public Lcom/facebook/messaging/model/share/ShareMedia;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/share/ShareMedia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120020
    new-instance v0, LX/6fV;

    invoke-direct {v0}, LX/6fV;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/share/ShareMedia;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fX;)V
    .locals 1

    .prologue
    .line 1120021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120022
    iget-object v0, p1, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    move-object v0, v0

    .line 1120023
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120024
    iget-object v0, p1, LX/6fX;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1120025
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->b:Ljava/lang/String;

    .line 1120026
    iget-object v0, p1, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    move-object v0, v0

    .line 1120027
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 1120028
    iget-object v0, p1, LX/6fX;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1120029
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->c:Ljava/lang/String;

    .line 1120030
    iget-object v0, p1, LX/6fX;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1120031
    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    .line 1120032
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120034
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->b:Ljava/lang/String;

    .line 1120035
    const-class v0, Lcom/facebook/messaging/model/share/ShareMedia$Type;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/share/ShareMedia$Type;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 1120036
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->c:Ljava/lang/String;

    .line 1120037
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    .line 1120038
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120039
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1120040
    const-class v0, Lcom/facebook/messaging/model/share/ShareMedia;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "href"

    iget-object v2, p0, Lcom/facebook/messaging/model/share/ShareMedia;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/share/ShareMedia$Type;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "src"

    iget-object v2, p0, Lcom/facebook/messaging/model/share/ShareMedia;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "playableSrc"

    iget-object v2, p0, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120041
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120042
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120043
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120044
    iget-object v0, p0, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120045
    return-void
.end method
