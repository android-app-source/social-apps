.class public Lcom/facebook/messaging/model/share/Share;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/share/Share;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/share/ShareMedia;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/share/ShareProperty;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

.field public final m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119978
    new-instance v0, LX/6fS;

    invoke-direct {v0}, LX/6fS;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/share/Share;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fT;)V
    .locals 1

    .prologue
    .line 1119950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119951
    iget-object v0, p1, LX/6fT;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1119952
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    .line 1119953
    iget-object v0, p1, LX/6fT;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1119954
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    .line 1119955
    iget-object v0, p1, LX/6fT;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1119956
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->c:Ljava/lang/String;

    .line 1119957
    iget-object v0, p1, LX/6fT;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1119958
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->d:Ljava/lang/String;

    .line 1119959
    iget-object v0, p1, LX/6fT;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1119960
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->e:Ljava/lang/String;

    .line 1119961
    iget-object v0, p1, LX/6fT;->g:Ljava/util/List;

    move-object v0, v0

    .line 1119962
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->g:LX/0Px;

    .line 1119963
    iget-object v0, p1, LX/6fT;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1119964
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->f:Ljava/lang/String;

    .line 1119965
    iget-object v0, p1, LX/6fT;->h:Ljava/util/List;

    move-object v0, v0

    .line 1119966
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->h:LX/0Px;

    .line 1119967
    iget-object v0, p1, LX/6fT;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    move-object v0, v0

    .line 1119968
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 1119969
    iget-object v0, p1, LX/6fT;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1119970
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->j:Ljava/lang/String;

    .line 1119971
    iget-object v0, p1, LX/6fT;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1119972
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->k:Ljava/lang/String;

    .line 1119973
    iget-object v0, p1, LX/6fT;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-object v0, v0

    .line 1119974
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1119975
    iget-object v0, p1, LX/6fT;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    move-object v0, v0

    .line 1119976
    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 1119977
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119980
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    .line 1119981
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    .line 1119982
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->c:Ljava/lang/String;

    .line 1119983
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->d:Ljava/lang/String;

    .line 1119984
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->e:Ljava/lang/String;

    .line 1119985
    const-class v0, Lcom/facebook/messaging/model/share/ShareMedia;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->g:LX/0Px;

    .line 1119986
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->f:Ljava/lang/String;

    .line 1119987
    const-class v0, Lcom/facebook/messaging/model/share/ShareProperty;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->h:LX/0Px;

    .line 1119988
    const-class v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 1119989
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->j:Ljava/lang/String;

    .line 1119990
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->k:Ljava/lang/String;

    .line 1119991
    const-class v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1119992
    const-class v0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 1119993
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119949
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119935
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119936
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119937
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119938
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119939
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119940
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->g:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1119941
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119942
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->h:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1119943
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119944
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119945
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119946
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119947
    iget-object v0, p0, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119948
    return-void
.end method
