.class public Lcom/facebook/messaging/model/share/SentShareAttachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/share/SentShareAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6fR;

.field public final b:Lcom/facebook/messaging/model/share/Share;

.field public final c:Lcom/facebook/messaging/model/payment/SentPayment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119916
    new-instance v0, LX/6fQ;

    invoke-direct {v0}, LX/6fQ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fR;Lcom/facebook/messaging/model/share/Share;Lcom/facebook/messaging/model/payment/SentPayment;)V
    .locals 0

    .prologue
    .line 1119917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119918
    iput-object p1, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    .line 1119919
    iput-object p2, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    .line 1119920
    iput-object p3, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    .line 1119921
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119923
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6fR;->fromDBSerialValue(Ljava/lang/String;)LX/6fR;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    .line 1119924
    const-class v0, Lcom/facebook/messaging/model/share/Share;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/share/Share;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    .line 1119925
    const-class v0, Lcom/facebook/messaging/model/payment/SentPayment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/payment/SentPayment;

    iput-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    .line 1119926
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119927
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119928
    iget-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    iget-object v0, v0, LX/6fR;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119929
    iget-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119930
    iget-object v0, p0, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119931
    return-void
.end method
