.class public Lcom/facebook/messaging/model/send/PendingSendQueueKey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/send/PendingSendQueueKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:LX/6fM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1119780
    new-instance v0, LX/6fL;

    invoke-direct {v0}, LX/6fL;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1119781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119782
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119783
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6fM;

    iput-object v0, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    .line 1119784
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V
    .locals 0

    .prologue
    .line 1119785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119786
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119787
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119788
    iput-object p1, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1119789
    iput-object p2, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    .line 1119790
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119791
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1119792
    if-ne p0, p1, :cond_1

    .line 1119793
    :cond_0
    :goto_0
    return v0

    .line 1119794
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1119795
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 1119796
    iget-object v2, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    iget-object v3, p1, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 1119797
    :cond_4
    iget-object v2, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1119798
    iget-object v0, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->hashCode()I

    move-result v0

    .line 1119799
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-virtual {v1}, LX/6fM;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1119800
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1119801
    iget-object v0, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1119802
    iget-object v0, p0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1119803
    return-void
.end method
