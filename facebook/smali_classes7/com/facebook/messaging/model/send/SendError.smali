.class public Lcom/facebook/messaging/model/send/SendError;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/send/SendError;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/send/SendError;


# instance fields
.field public final b:LX/6fP;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:J

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1119817
    new-instance v0, Lcom/facebook/messaging/model/send/SendError;

    sget-object v1, LX/6fP;->NONE:LX/6fP;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/send/SendError;-><init>(LX/6fP;)V

    sput-object v0, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 1119818
    new-instance v0, LX/6fN;

    invoke-direct {v0}, LX/6fN;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/send/SendError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fO;)V
    .locals 4

    .prologue
    .line 1119819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119820
    iget-object v0, p1, LX/6fO;->a:LX/6fP;

    move-object v0, v0

    .line 1119821
    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    .line 1119822
    iget-object v0, p1, LX/6fO;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1119823
    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    .line 1119824
    iget-wide v2, p1, LX/6fO;->c:J

    move-wide v0, v2

    .line 1119825
    iput-wide v0, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    .line 1119826
    iget-object v0, p1, LX/6fO;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1119827
    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    .line 1119828
    iget v0, p1, LX/6fO;->e:I

    move v0, v0

    .line 1119829
    iput v0, p0, Lcom/facebook/messaging/model/send/SendError;->d:I

    .line 1119830
    iget-object v0, p1, LX/6fO;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1119831
    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->g:Ljava/lang/String;

    .line 1119832
    return-void
.end method

.method private constructor <init>(LX/6fP;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1119833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119834
    iput-object p1, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    .line 1119835
    iput-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    .line 1119836
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    .line 1119837
    iput-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    .line 1119838
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/model/send/SendError;->d:I

    .line 1119839
    iput-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->g:Ljava/lang/String;

    .line 1119840
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1119841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119842
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6fP;

    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    .line 1119843
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    .line 1119844
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    .line 1119845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    .line 1119846
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/send/SendError;->d:I

    .line 1119847
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->g:Ljava/lang/String;

    .line 1119848
    return-void
.end method

.method public static a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;
    .locals 1

    .prologue
    .line 1119849
    new-instance v0, Lcom/facebook/messaging/model/send/SendError;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/send/SendError;-><init>(LX/6fP;)V

    return-object v0
.end method

.method public static newBuilder()LX/6fO;
    .locals 1

    .prologue
    .line 1119850
    new-instance v0, LX/6fO;

    invoke-direct {v0}, LX/6fO;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1119851
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1119852
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "timeStamp"

    iget-wide v2, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v2, v2, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "errorMessage"

    iget-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "errorNumber"

    iget v2, p0, Lcom/facebook/messaging/model/send/SendError;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "errorUrl"

    iget-object v2, p0, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1119853
    iget-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1119854
    iget-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119855
    iget-wide v0, p0, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1119856
    iget-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119857
    iget v0, p0, Lcom/facebook/messaging/model/send/SendError;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119858
    iget-object v0, p0, Lcom/facebook/messaging/model/send/SendError;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119859
    return-void
.end method
