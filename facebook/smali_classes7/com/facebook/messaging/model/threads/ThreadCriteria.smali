.class public Lcom/facebook/messaging/model/threads/ThreadCriteria;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadCriteria;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120548
    new-instance v0, LX/6fp;

    invoke-direct {v0}, LX/6fp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    .line 1120497
    sget-object v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1120498
    if-nez v0, :cond_0

    .line 1120499
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1120500
    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    .line 1120501
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    .line 1120502
    return-void

    .line 1120503
    :cond_0
    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1120542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120543
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    .line 1120544
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1120545
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    .line 1120546
    iput-object p2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    .line 1120547
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1120537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120538
    iput-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    .line 1120539
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    .line 1120540
    iput-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    .line 1120541
    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;
    .locals 1

    .prologue
    .line 1120534
    invoke-static {p0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 1120535
    new-instance p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;-><init>(Ljava/util/Collection;)V

    move-object v0, p0

    .line 1120536
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadCriteria;
    .locals 3

    .prologue
    .line 1120533
    new-instance v1, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    const/4 v2, 0x0

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1120549
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120532
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120520
    if-ne p0, p1, :cond_1

    .line 1120521
    :cond_0
    :goto_0
    return v0

    .line 1120522
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1120523
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 1120524
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    .line 1120525
    instance-of v4, v2, Ljava/util/Collection;

    if-eqz v4, :cond_5

    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_5

    move-object v4, v2

    .line 1120526
    check-cast v4, Ljava/util/Collection;

    move-object v5, v3

    .line 1120527
    check-cast v5, Ljava/util/Collection;

    .line 1120528
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-eq v4, v5, :cond_5

    .line 1120529
    const/4 v4, 0x0

    .line 1120530
    :goto_1
    move v2, v4

    .line 1120531
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-static {v4, v5}, LX/0RZ;->a(Ljava/util/Iterator;Ljava/util/Iterator;)Z

    move-result v4

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1120519
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1120509
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1120510
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1120511
    const-string v0, "tid: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1120512
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1120513
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1120514
    const-string v0, "threadIdReferenceQuery: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1120515
    :cond_1
    const-string v0, "threadkeys: ["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1120516
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1120517
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1120518
    :cond_2
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120504
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120505
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1120506
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 1120507
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCriteria;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120508
    return-void
.end method
