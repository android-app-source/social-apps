.class public Lcom/facebook/messaging/model/threads/NicknamesMap;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/NicknamesMap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120270
    new-instance v0, LX/6fi;

    invoke-direct {v0}, LX/6fi;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120313
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1120314
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    .line 1120315
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1120307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120308
    const-string v0, "{}"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1120309
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    .line 1120310
    :goto_0
    return-void

    .line 1120311
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;LX/0P1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1120303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120304
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    .line 1120305
    iput-object p2, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    .line 1120306
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1120299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120300
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    .line 1120301
    return-void

    .line 1120302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0lC;)LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1120291
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    if-nez v0, :cond_0

    .line 1120292
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1120293
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1120294
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    .line 1120295
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    return-object v0

    .line 1120296
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    new-instance v1, LX/6fh;

    invoke-direct {v1, p0}, LX/6fh;-><init>(Lcom/facebook/messaging/model/threads/NicknamesMap;)V

    invoke-virtual {p1, v0, v1}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1120297
    :catch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1120298
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lC;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1120290
    invoke-direct {p0, p1}, Lcom/facebook/messaging/model/threads/NicknamesMap;->b(LX/0lC;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0lC;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1120289
    invoke-direct {p0, p2}, Lcom/facebook/messaging/model/threads/NicknamesMap;->b(LX/0lC;)LX/0P1;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120288
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1120284
    instance-of v1, p1, Lcom/facebook/messaging/model/threads/NicknamesMap;

    if-nez v1, :cond_1

    .line 1120285
    :cond_0
    :goto_0
    return v0

    .line 1120286
    :cond_1
    check-cast p1, Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120287
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1120280
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1120281
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1120282
    return v0

    :cond_1
    move v0, v1

    .line 1120283
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1120271
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120272
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1120273
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120274
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120275
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    if-eqz v0, :cond_1

    .line 1120276
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1120277
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1120278
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1120279
    goto :goto_1
.end method
