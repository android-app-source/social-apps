.class public Lcom/facebook/messaging/model/threads/ThreadSummary;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/6ek;

.field public final B:Lcom/facebook/messaging/model/messages/MessageDraft;

.field public final C:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public final D:Z

.field public final E:Z

.field public final F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public final G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

.field public final H:J

.field public final I:J

.field public final J:Z

.field public final K:I

.field public final L:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;"
        }
    .end annotation
.end field

.field public final M:Z

.field public final N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

.field public final O:J

.field public final P:F

.field public final Q:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threads/ThreadGameData;",
            ">;"
        }
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final S:LX/03R;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final T:LX/03R;

.field public final U:I

.field public final V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final X:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

.field private Z:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final c:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final d:J

.field public final e:J

.field public final f:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public final j:J

.field public final k:J

.field public final l:J

.field public final m:J

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/String;

.field public final s:Landroid/net/Uri;

.field public final t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final u:Z

.field public final v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public final w:Z

.field public final x:Z

.field public final y:LX/6g5;

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1121020
    new-instance v0, LX/6g3;

    invoke-direct {v0}, LX/6g3;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6g6;)V
    .locals 4

    .prologue
    .line 1121021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121022
    iget-object v0, p1, LX/6g6;->A:LX/6ek;

    move-object v0, v0

    .line 1121023
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121024
    iget-object v0, p1, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1121025
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121026
    iget-object v0, p1, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1121027
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121028
    iget-object v0, p1, LX/6g6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1121029
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    .line 1121030
    iget-wide v2, p1, LX/6g6;->c:J

    move-wide v0, v2

    .line 1121031
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 1121032
    iget-wide v2, p1, LX/6g6;->d:J

    move-wide v0, v2

    .line 1121033
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    .line 1121034
    iget-wide v2, p1, LX/6g6;->e:J

    move-wide v0, v2

    .line 1121035
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    .line 1121036
    iget-wide v2, p1, LX/6g6;->f:J

    move-wide v0, v2

    .line 1121037
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    .line 1121038
    iget-object v0, p1, LX/6g6;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1121039
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    .line 1121040
    iget-object v0, p1, LX/6g6;->h:Ljava/util/List;

    move-object v0, v0

    .line 1121041
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    .line 1121042
    iget-object v0, p1, LX/6g6;->i:Ljava/util/List;

    move-object v0, v0

    .line 1121043
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    .line 1121044
    iget-wide v2, p1, LX/6g6;->j:J

    move-wide v0, v2

    .line 1121045
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 1121046
    iget-wide v2, p1, LX/6g6;->k:J

    move-wide v0, v2

    .line 1121047
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    .line 1121048
    iget-wide v2, p1, LX/6g6;->l:J

    move-wide v0, v2

    .line 1121049
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    .line 1121050
    iget-wide v2, p1, LX/6g6;->m:J

    move-wide v0, v2

    .line 1121051
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    .line 1121052
    iget-object v0, p1, LX/6g6;->n:Ljava/util/List;

    move-object v0, v0

    .line 1121053
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    .line 1121054
    iget-object v0, p1, LX/6g6;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1121055
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    .line 1121056
    iget-object v0, p1, LX/6g6;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1121057
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    .line 1121058
    iget-object v0, p1, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v0, v0

    .line 1121059
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1121060
    iget-object v0, p1, LX/6g6;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1121061
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    .line 1121062
    iget-object v0, p1, LX/6g6;->s:Landroid/net/Uri;

    move-object v0, v0

    .line 1121063
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    .line 1121064
    iget-object v0, p1, LX/6g6;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-object v0, v0

    .line 1121065
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 1121066
    iget-boolean v0, p1, LX/6g6;->u:Z

    move v0, v0

    .line 1121067
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    .line 1121068
    iget-object v0, p1, LX/6g6;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-object v0, v0

    .line 1121069
    if-eqz v0, :cond_0

    .line 1121070
    iget-object v0, p1, LX/6g6;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    move-object v0, v0

    .line 1121071
    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1121072
    iget-boolean v0, p1, LX/6g6;->w:Z

    move v0, v0

    .line 1121073
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    .line 1121074
    iget-boolean v0, p1, LX/6g6;->x:Z

    move v0, v0

    .line 1121075
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->x:Z

    .line 1121076
    iget-object v0, p1, LX/6g6;->y:LX/6g5;

    move-object v0, v0

    .line 1121077
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->y:LX/6g5;

    .line 1121078
    iget-boolean v0, p1, LX/6g6;->z:Z

    move v0, v0

    .line 1121079
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->z:Z

    .line 1121080
    iget-object v0, p1, LX/6g6;->A:LX/6ek;

    move-object v0, v0

    .line 1121081
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    .line 1121082
    iget-object v0, p1, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    move-object v0, v0

    .line 1121083
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 1121084
    iget-object v0, p1, LX/6g6;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v0

    .line 1121085
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1121086
    iget-boolean v0, p1, LX/6g6;->D:Z

    move v0, v0

    .line 1121087
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->D:Z

    .line 1121088
    iget-boolean v0, p1, LX/6g6;->E:Z

    move v0, v0

    .line 1121089
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->E:Z

    .line 1121090
    iget-object v0, p1, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-object v0, v0

    .line 1121091
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1121092
    iget-wide v2, p1, LX/6g6;->G:J

    move-wide v0, v2

    .line 1121093
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->I:J

    .line 1121094
    iget-boolean v0, p1, LX/6g6;->H:Z

    move v0, v0

    .line 1121095
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    .line 1121096
    iget v0, p1, LX/6g6;->I:I

    move v0, v0

    .line 1121097
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    .line 1121098
    iget-object v0, p1, LX/6g6;->J:Ljava/util/List;

    move-object v0, v0

    .line 1121099
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    .line 1121100
    iget-boolean v0, p1, LX/6g6;->K:Z

    move v0, v0

    .line 1121101
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->M:Z

    .line 1121102
    iget-object v0, p1, LX/6g6;->L:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-object v0, v0

    .line 1121103
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1121104
    iget-wide v2, p1, LX/6g6;->M:J

    move-wide v0, v2

    .line 1121105
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->O:J

    .line 1121106
    iget v0, p1, LX/6g6;->N:F

    move v0, v0

    .line 1121107
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->P:F

    .line 1121108
    iget-object v0, p1, LX/6g6;->O:Ljava/util/Map;

    move-object v0, v0

    .line 1121109
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    .line 1121110
    iget-object v0, p1, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    move-object v0, v0

    .line 1121111
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1121112
    iget-object v0, p1, LX/6g6;->Q:Ljava/lang/String;

    move-object v0, v0

    .line 1121113
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    .line 1121114
    iget-object v0, p1, LX/6g6;->R:LX/03R;

    move-object v0, v0

    .line 1121115
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->S:LX/03R;

    .line 1121116
    iget-object v0, p1, LX/6g6;->S:LX/03R;

    move-object v0, v0

    .line 1121117
    invoke-direct {p0, v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(LX/03R;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    .line 1121118
    iget v0, p1, LX/6g6;->T:I

    move v0, v0

    .line 1121119
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->U:I

    .line 1121120
    iget-object v0, p1, LX/6g6;->U:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    move-object v0, v0

    .line 1121121
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 1121122
    iget-wide v2, p1, LX/6g6;->V:J

    move-wide v0, v2

    .line 1121123
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    .line 1121124
    iget-object v0, p1, LX/6g6;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    move-object v0, v0

    .line 1121125
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 1121126
    iget-object v0, p1, LX/6g6;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1121127
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121128
    iget-object v0, p1, LX/6g6;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-object v0, v0

    .line 1121129
    invoke-direct {p0, v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 1121130
    return-void

    .line 1121131
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1121132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121133
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    .line 1121135
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 1121136
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    .line 1121137
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    .line 1121138
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    .line 1121139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    .line 1121140
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    .line 1121141
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    .line 1121142
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 1121143
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    .line 1121144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    .line 1121145
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    .line 1121146
    sget-object v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    .line 1121147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    .line 1121148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    .line 1121149
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1121150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    .line 1121151
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    .line 1121152
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 1121153
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    .line 1121154
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1121155
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 1121156
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    .line 1121157
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->x:Z

    .line 1121158
    const-class v0, LX/6g5;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6g5;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->y:LX/6g5;

    .line 1121159
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->z:Z

    .line 1121160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    .line 1121161
    const-class v0, Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/MessageDraft;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 1121162
    const-class v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1121163
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->D:Z

    .line 1121164
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->E:Z

    .line 1121165
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1121166
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->I:J

    .line 1121167
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    .line 1121168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    .line 1121169
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    .line 1121170
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->M:Z

    .line 1121171
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->O:J

    .line 1121172
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->P:F

    .line 1121173
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadGameData;

    .line 1121174
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1121175
    invoke-static {p1, v1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;Ljava/lang/Class;)V

    .line 1121176
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 1121177
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    .line 1121178
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1121179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    .line 1121180
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->S:LX/03R;

    .line 1121181
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(LX/03R;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    .line 1121182
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->U:I

    .line 1121183
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    .line 1121184
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    .line 1121185
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1121186
    const-class v0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 1121187
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1121188
    const-class v0, Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Lcom/facebook/messaging/model/threads/RoomThreadData;)Lcom/facebook/messaging/model/threads/RoomThreadData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 1121189
    return-void

    .line 1121190
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto/16 :goto_0
.end method

.method private a(LX/03R;)LX/03R;
    .locals 1

    .prologue
    .line 1120956
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120957
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1120958
    sget-object v0, LX/03R;->YES:LX/03R;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1120959
    :cond_0
    return-object p1

    .line 1120960
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threads/RoomThreadData;)Lcom/facebook/messaging/model/threads/RoomThreadData;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1121191
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121192
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1121193
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1121194
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1121195
    :cond_0
    return-object p1

    :cond_1
    move v0, v2

    .line 1121196
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1121197
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1121198
    if-nez p0, :cond_0

    .line 1121199
    const/4 v0, 0x0

    .line 1121200
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "\n"

    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static newBuilder()LX/6g6;
    .locals 1

    .prologue
    .line 1121019
    new-instance v0, LX/6g6;

    invoke-direct {v0}, LX/6g6;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1121201
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Z:LX/0P1;

    if-nez v0, :cond_2

    .line 1121202
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    const/4 v3, 0x0

    .line 1121203
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 1121204
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 1121205
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    invoke-interface {v5, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121206
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 1121207
    :cond_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 1121208
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121209
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1121210
    :cond_1
    invoke-static {v5}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    move-object v0, v2

    .line 1121211
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Z:LX/0P1;

    .line 1121212
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Z:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1121018
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1121017
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1121016
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1121015
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 1121014
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1121013
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "threadKey"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "folder"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v2}, LX/6ek;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "unread"

    invoke-virtual {p0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "timestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastReadWatermarkTimestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "participants"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "senders"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "snippet"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "adminSnippet"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "threadCustomization"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "outgoingMessageLifetime"

    iget v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "subscribed"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "canReplyTo"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "lastCallMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120961
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120962
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120963
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120964
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120965
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120966
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120967
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120968
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1120969
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1120970
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120971
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120972
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120973
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120974
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1120975
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120976
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120977
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120978
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120979
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120980
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120981
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120982
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1120983
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120984
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->x:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120985
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->y:LX/6g5;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1120986
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->z:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120987
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120988
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120989
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120990
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->D:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120991
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->E:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120992
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120993
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->I:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120994
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120995
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120996
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1120997
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->M:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120998
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->O:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120999
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->P:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1121000
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1121001
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121002
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1121003
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->S:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1121004
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1121005
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->U:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1121006
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121007
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1121008
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121009
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->W:Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121010
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121011
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1121012
    return-void
.end method
