.class public Lcom/facebook/messaging/model/threads/NotificationSetting;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public static final b:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public static final c:Lcom/facebook/messaging/model/threads/NotificationSetting;


# instance fields
.field public final d:Z

.field public final e:J

.field public final f:Z

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1120319
    new-instance v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    sput-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1120320
    new-instance v6, Lcom/facebook/messaging/model/threads/NotificationSetting;

    move v7, v4

    move-wide v8, v2

    move v10, v4

    move v11, v4

    invoke-direct/range {v6 .. v11}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    sput-object v6, Lcom/facebook/messaging/model/threads/NotificationSetting;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1120321
    new-instance v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    sput-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1120322
    new-instance v0, LX/6fj;

    invoke-direct {v0}, LX/6fj;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1120378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120379
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    .line 1120380
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    .line 1120381
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    .line 1120382
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    .line 1120383
    return-void

    :cond_0
    move v0, v2

    .line 1120384
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1120385
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1120386
    goto :goto_2
.end method

.method private constructor <init>(ZJZZ)V
    .locals 0

    .prologue
    .line 1120372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120373
    iput-boolean p1, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    .line 1120374
    iput-wide p2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    .line 1120375
    iput-boolean p4, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    .line 1120376
    iput-boolean p5, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    .line 1120377
    return-void
.end method

.method public static a(J)Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1120371
    new-instance v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    const/4 v1, 0x1

    move-wide v2, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    return-object v0
.end method

.method public static b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1120361
    const-wide/16 v2, -0x1

    cmp-long v0, p0, v2

    if-nez v0, :cond_0

    .line 1120362
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1120363
    :goto_0
    return-object v0

    .line 1120364
    :cond_0
    const-wide/16 v2, -0x2

    cmp-long v0, p0, v2

    if-nez v0, :cond_1

    .line 1120365
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    goto :goto_0

    .line 1120366
    :cond_1
    cmp-long v0, p0, v6

    if-nez v0, :cond_2

    .line 1120367
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    goto :goto_0

    .line 1120368
    :cond_2
    cmp-long v0, p0, v6

    if-gez v0, :cond_3

    .line 1120369
    new-instance v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    neg-long v2, p0

    move v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    goto :goto_0

    .line 1120370
    :cond_3
    new-instance v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-wide v2, p0

    move v4, v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/model/threads/NotificationSetting;-><init>(ZJZZ)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1120356
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-eqz v0, :cond_1

    .line 1120357
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x2

    .line 1120358
    :goto_0
    return-wide v0

    .line 1120359
    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    goto :goto_0

    .line 1120360
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1120355
    invoke-virtual {p0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->c()LX/6fk;

    move-result-object v0

    sget-object v1, LX/6fk;->Enabled:LX/6fk;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/6fk;
    .locals 8

    .prologue
    .line 1120350
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-nez v0, :cond_0

    .line 1120351
    sget-object v0, LX/6fk;->PermanentlyDisabled:LX/6fk;

    .line 1120352
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    .line 1120353
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    move-wide v2, v4

    .line 1120354
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    sget-object v0, LX/6fk;->TemporarilyMuted:LX/6fk;

    goto :goto_0

    :cond_1
    sget-object v0, LX/6fk;->Enabled:LX/6fk;

    goto :goto_0
.end method

.method public final d()LX/0m9;
    .locals 4

    .prologue
    .line 1120349
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "enabled"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    move-result-object v0

    const-string v1, "mutedUntilSeconds"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v0

    const-string v1, "automaticallyMuted"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    move-result-object v0

    const-string v1, "chatheadDisabled"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120348
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120340
    if-ne p0, p1, :cond_1

    .line 1120341
    :cond_0
    :goto_0
    return v0

    .line 1120342
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1120343
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1120344
    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 1120345
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 1120346
    :cond_5
    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    .line 1120347
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    iget-boolean v3, p1, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1120332
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 1120333
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    iget-wide v6, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 1120334
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 1120335
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1120336
    return v0

    :cond_0
    move v0, v2

    .line 1120337
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1120338
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1120339
    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1120331
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "enabled"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "mutedUntilSeconds"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "automaticallyMuted"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "chatheadDisabled"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1120323
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120324
    iget-wide v4, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->e:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120325
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120326
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/NotificationSetting;->g:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120327
    return-void

    :cond_0
    move v0, v2

    .line 1120328
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1120329
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1120330
    goto :goto_2
.end method
