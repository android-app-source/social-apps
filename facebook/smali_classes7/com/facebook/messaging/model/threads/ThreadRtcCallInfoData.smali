.class public Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;


# instance fields
.field public final b:LX/6g2;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1120928
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    sget-object v1, LX/6g2;->UNKNOWN:LX/6g2;

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;-><init>(LX/6g2;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1120929
    new-instance v0, LX/6g0;

    invoke-direct {v0}, LX/6g0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6g1;)V
    .locals 1

    .prologue
    .line 1120920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120921
    iget-object v0, p1, LX/6g1;->a:LX/6g2;

    move-object v0, v0

    .line 1120922
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    .line 1120923
    iget-object v0, p1, LX/6g1;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1120924
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    .line 1120925
    iget-object v0, p1, LX/6g1;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1120926
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    .line 1120927
    return-void
.end method

.method private constructor <init>(LX/6g2;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1120915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120916
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    .line 1120917
    iput-object p2, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    .line 1120918
    iput-object p3, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    .line 1120919
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120911
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6g2;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    .line 1120912
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    .line 1120913
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    .line 1120914
    return-void
.end method

.method public static newBuilder()LX/6g1;
    .locals 1

    .prologue
    .line 1120909
    new-instance v0, LX/6g1;

    invoke-direct {v0}, LX/6g1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1120908
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    sget-object v1, LX/6g2;->AUDIO_GROUP_CALL:LX/6g2;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    sget-object v1, LX/6g2;->VIDEO_GROUP_CALL:LX/6g2;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1120890
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    sget-object v1, LX/6g2;->VIDEO_GROUP_CALL:LX/6g2;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120907
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120901
    if-ne p0, p1, :cond_1

    .line 1120902
    :cond_0
    :goto_0
    return v0

    .line 1120903
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1120904
    goto :goto_0

    .line 1120905
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 1120906
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1120895
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    invoke-virtual {v0}, LX/6g2;->hashCode()I

    move-result v0

    .line 1120896
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1120897
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1120898
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1120899
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1120900
    :cond_1
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120891
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1120892
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120893
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120894
    return-void
.end method
