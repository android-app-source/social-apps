.class public Lcom/facebook/messaging/model/threads/DualSimSetting;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/DualSimSetting;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/threads/DualSimSetting;


# instance fields
.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1120147
    new-instance v0, Lcom/facebook/messaging/model/threads/DualSimSetting;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/threads/DualSimSetting;-><init>(I)V

    sput-object v0, Lcom/facebook/messaging/model/threads/DualSimSetting;->a:Lcom/facebook/messaging/model/threads/DualSimSetting;

    .line 1120148
    new-instance v0, LX/6fd;

    invoke-direct {v0}, LX/6fd;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/DualSimSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1120149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120150
    iput p1, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    .line 1120151
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    .line 1120154
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120155
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120156
    if-ne p0, p1, :cond_1

    .line 1120157
    :cond_0
    :goto_0
    return v0

    .line 1120158
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1120159
    goto :goto_0

    .line 1120160
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/DualSimSetting;

    .line 1120161
    iget v2, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    iget v3, p1, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1120162
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1120163
    iget v0, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1120164
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "subscriptionId"

    iget v2, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120165
    iget v0, p0, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120166
    return-void
.end method
