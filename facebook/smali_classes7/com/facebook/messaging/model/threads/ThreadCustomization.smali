.class public Lcom/facebook/messaging/model/threads/ThreadCustomization;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadCustomization;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/threads/ThreadCustomization;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lcom/facebook/messaging/model/threads/NicknamesMap;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120605
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-direct {v0}, Lcom/facebook/messaging/model/threads/ThreadCustomization;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1120606
    new-instance v0, LX/6fq;

    invoke-direct {v0}, LX/6fq;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1120582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120583
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    .line 1120584
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    .line 1120585
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    .line 1120586
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    .line 1120587
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    .line 1120588
    new-instance v0, Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-direct {v0}, Lcom/facebook/messaging/model/threads/NicknamesMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120589
    return-void
.end method

.method public constructor <init>(LX/6fr;)V
    .locals 1

    .prologue
    .line 1120615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120616
    iget v0, p1, LX/6fr;->a:I

    move v0, v0

    .line 1120617
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    .line 1120618
    iget v0, p1, LX/6fr;->b:I

    move v0, v0

    .line 1120619
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    .line 1120620
    iget v0, p1, LX/6fr;->c:I

    move v0, v0

    .line 1120621
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    .line 1120622
    iget v0, p1, LX/6fr;->d:I

    move v0, v0

    .line 1120623
    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    .line 1120624
    iget-object v0, p1, LX/6fr;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1120625
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    .line 1120626
    iget-object v0, p1, LX/6fr;->f:Lcom/facebook/messaging/model/threads/NicknamesMap;

    move-object v0, v0

    .line 1120627
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120628
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120608
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    .line 1120609
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    .line 1120610
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    .line 1120611
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    .line 1120612
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    .line 1120613
    const-class v0, Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/NicknamesMap;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 1120614
    return-void
.end method

.method public static newBuilder()LX/6fr;
    .locals 2

    .prologue
    .line 1120603
    new-instance v0, LX/6fr;

    invoke-direct {v0}, LX/6fr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120604
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120598
    if-ne p0, p1, :cond_1

    .line 1120599
    :cond_0
    :goto_0
    return v0

    .line 1120600
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1120601
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1120602
    iget v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    iget v3, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    iget v3, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    iget v3, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/model/threads/NicknamesMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1120591
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    .line 1120592
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    add-int/2addr v0, v1

    .line 1120593
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    add-int/2addr v0, v1

    .line 1120594
    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1120595
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1120596
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NicknamesMap;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1120597
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1120590
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "wallpaperColor"

    const-string v2, "%x"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "meBubbleColor"

    const-string v2, "%x"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "otherBubbleColor"

    const-string v2, "%x"

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "emojilikeString"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "nicknamesMap"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120575
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120576
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120577
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120578
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120579
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120580
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120581
    return-void
.end method
