.class public Lcom/facebook/messaging/model/threads/ThreadsCollection;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/messaging/model/threads/ThreadsCollection;


# instance fields
.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1121380
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a:Ljava/lang/Class;

    .line 1121381
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1121382
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1121383
    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1121384
    new-instance v0, LX/6g8;

    invoke-direct {v0}, LX/6g8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1121385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121386
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    .line 1121387
    iput-boolean p2, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    .line 1121388
    const/4 v2, 0x0

    .line 1121389
    const-wide v0, 0x7fffffffffffffffL

    .line 1121390
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move-wide v4, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1121391
    iget-wide v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v6, v6, v4

    if-lez v6, :cond_1

    .line 1121392
    sget-object v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a:Ljava/lang/Class;

    const-string v3, "Threads were not in order, this timestamp=%d, lastTimestampMs=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-wide v8, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v2

    const/4 v0, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1121393
    :cond_0
    return-void

    .line 1121394
    :cond_1
    iget-wide v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 1121395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1121396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121397
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    .line 1121398
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    .line 1121399
    return-void

    .line 1121400
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 1

    .prologue
    .line 1121401
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 1121402
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1121403
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1121404
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1121405
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1121406
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1121407
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1121408
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1121409
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1121410
    return-void

    .line 1121411
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
