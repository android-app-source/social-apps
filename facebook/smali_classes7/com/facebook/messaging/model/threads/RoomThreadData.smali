.class public Lcom/facebook/messaging/model/threads/RoomThreadData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/RoomThreadData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadJoinRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120445
    new-instance v0, LX/6fl;

    invoke-direct {v0}, LX/6fl;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/RoomThreadData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1120411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120412
    iput-object v1, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    .line 1120413
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    .line 1120414
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    .line 1120415
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    .line 1120416
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1120417
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    .line 1120418
    iput-object v1, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    .line 1120419
    return-void
.end method

.method public constructor <init>(LX/6fm;)V
    .locals 1

    .prologue
    .line 1120420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120421
    iget-object v0, p1, LX/6fm;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    .line 1120422
    iget-boolean v0, p1, LX/6fm;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    .line 1120423
    iget-boolean v0, p1, LX/6fm;->c:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    .line 1120424
    iget-boolean v0, p1, LX/6fm;->d:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    .line 1120425
    iget-object v0, p1, LX/6fm;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    .line 1120426
    iget-object v0, p1, LX/6fm;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    .line 1120427
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120429
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    .line 1120430
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    .line 1120431
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    .line 1120432
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    .line 1120433
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    .line 1120434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    .line 1120435
    return-void
.end method

.method public static newBuilder()LX/6fm;
    .locals 2

    .prologue
    .line 1120436
    new-instance v0, LX/6fm;

    invoke-direct {v0}, LX/6fm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120437
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120438
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120439
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120440
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120441
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120442
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1120443
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120444
    return-void
.end method
