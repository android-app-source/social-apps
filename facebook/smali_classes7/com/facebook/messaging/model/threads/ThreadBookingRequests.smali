.class public Lcom/facebook/messaging/model/threads/ThreadBookingRequests;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadBookingRequests;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120491
    new-instance v0, LX/6fn;

    invoke-direct {v0}, LX/6fn;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fo;)V
    .locals 1

    .prologue
    .line 1120483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120484
    iget-object v0, p1, LX/6fo;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    .line 1120485
    iget v0, p1, LX/6fo;->c:I

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    .line 1120486
    iget v0, p1, LX/6fo;->b:I

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    .line 1120487
    iget v0, p1, LX/6fo;->d:I

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    .line 1120488
    iget-object v0, p1, LX/6fo;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->e:Ljava/lang/String;

    .line 1120489
    iget-object v0, p1, LX/6fo;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    .line 1120490
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120476
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    .line 1120477
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    .line 1120478
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    .line 1120479
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    .line 1120480
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->e:Ljava/lang/String;

    .line 1120481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    .line 1120482
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1120474
    const-string v1, "requestCount=%d,pendingCount=%d,confirmedCount=%d,pageId=%s,detail.requestId=%s,detail.userId=%s, detail.pageId=%s"

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget v3, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-nez v0, :cond_0

    const-string v0, "NA"

    :goto_0
    aput-object v0, v2, v3

    const/4 v3, 0x4

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-nez v0, :cond_1

    const-string v0, "NA"

    :goto_1
    aput-object v0, v2, v3

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-nez v0, :cond_2

    const-string v0, "NA"

    :goto_2
    aput-object v0, v2, v3

    const/4 v3, 0x6

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    if-nez v0, :cond_3

    const-string v0, "NA"

    :goto_3
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->k:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->h:Ljava/lang/String;

    goto :goto_3
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120473
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120466
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120467
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120468
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120469
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120470
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120471
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120472
    return-void
.end method
