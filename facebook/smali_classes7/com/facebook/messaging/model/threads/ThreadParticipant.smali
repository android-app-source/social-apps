.class public Lcom/facebook/messaging/model/threads/ThreadParticipant;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public final b:J

.field public final c:J

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:J

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120806
    new-instance v0, LX/6fy;

    invoke-direct {v0}, LX/6fy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fz;)V
    .locals 4

    .prologue
    .line 1120829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120830
    iget-object v0, p1, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v0, v0

    .line 1120831
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1120832
    iget-wide v2, p1, LX/6fz;->b:J

    move-wide v0, v2

    .line 1120833
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    .line 1120834
    iget-wide v2, p1, LX/6fz;->c:J

    move-wide v0, v2

    .line 1120835
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    .line 1120836
    iget-object v0, p1, LX/6fz;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1120837
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    .line 1120838
    iget-wide v2, p1, LX/6fz;->e:J

    move-wide v0, v2

    .line 1120839
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    .line 1120840
    iget-boolean v0, p1, LX/6fz;->f:Z

    move v0, v0

    .line 1120841
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    .line 1120842
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1120821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120822
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1120823
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    .line 1120824
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    .line 1120825
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    .line 1120826
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    .line 1120827
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    .line 1120828
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/user/model/UserKey;
    .locals 1

    .prologue
    .line 1120820
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120819
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120818
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120817
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120816
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1120815
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1120814
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "participantInfo"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "lastReadReceiptActionTimestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastReadReceiptWatermarkTimestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "lastDeliveredReceiptMsgId"

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "lastDeliveredReceiptTimestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "isAdmin"

    iget-boolean v2, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120807
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120808
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120809
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120810
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120811
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120812
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120813
    return-void
.end method
