.class public Lcom/facebook/messaging/model/threads/MontageThreadPreview;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/MontageThreadPreview;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/attachment/Attachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:J

.field public final e:J

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120205
    new-instance v0, LX/6ff;

    invoke-direct {v0}, LX/6ff;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fg;)V
    .locals 2

    .prologue
    .line 1120206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120207
    iget-wide v0, p1, LX/6fg;->b:J

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    .line 1120208
    iget-wide v0, p1, LX/6fg;->c:J

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    .line 1120209
    iget-object v0, p1, LX/6fg;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    .line 1120210
    iget-object v0, p1, LX/6fg;->d:Lcom/facebook/messaging/model/attachment/Attachment;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    .line 1120211
    iget-object v0, p1, LX/6fg;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    .line 1120212
    iget-object v0, p1, LX/6fg;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    .line 1120213
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1120214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120215
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    .line 1120216
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    .line 1120217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    .line 1120218
    const-class v0, Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    .line 1120219
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    .line 1120220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    .line 1120221
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/6fg;
    .locals 1

    .prologue
    .line 1120222
    new-instance v0, LX/6fg;

    invoke-direct {v0, p0}, LX/6fg;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1120223
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120224
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1120225
    if-ne p0, p1, :cond_1

    .line 1120226
    :cond_0
    :goto_0
    return v0

    .line 1120227
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1120228
    goto :goto_0

    .line 1120229
    :cond_3
    check-cast p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;

    .line 1120230
    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 1120231
    goto :goto_0

    .line 1120232
    :cond_4
    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 1120233
    goto :goto_0

    .line 1120234
    :cond_5
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1120235
    goto :goto_0

    .line 1120236
    :cond_6
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1120237
    goto :goto_0

    .line 1120238
    :cond_8
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    if-nez v2, :cond_7

    .line 1120239
    :cond_9
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1120240
    goto :goto_0

    .line 1120241
    :cond_b
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1120242
    :cond_c
    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 1120243
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1120244
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1120245
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    iget-wide v4, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1120246
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 1120247
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 1120248
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1120249
    return v0

    :cond_1
    move v0, v1

    .line 1120250
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1120251
    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120252
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120253
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120254
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120255
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->a:Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120256
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120257
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/MontageThreadPreview;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120258
    return-void
.end method
