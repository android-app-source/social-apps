.class public Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadMediaPreview;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6fx;

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120802
    new-instance v0, LX/6fw;

    invoke-direct {v0}, LX/6fw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1120797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120798
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a:LX/6fx;

    .line 1120799
    iput-object p2, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b:Landroid/net/Uri;

    .line 1120800
    iput-object p3, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c:Ljava/lang/String;

    .line 1120801
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1120792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120793
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6fx;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a:LX/6fx;

    .line 1120794
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b:Landroid/net/Uri;

    .line 1120795
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c:Ljava/lang/String;

    .line 1120796
    return-void
.end method

.method public static a(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 3

    .prologue
    .line 1120791
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    sget-object v1, LX/6fx;->PHOTO:LX/6fx;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;-><init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 3

    .prologue
    .line 1120790
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    sget-object v1, LX/6fx;->STICKER:LX/6fx;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;-><init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 3

    .prologue
    .line 1120789
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    sget-object v1, LX/6fx;->VIDEO:LX/6fx;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;-><init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 3

    .prologue
    .line 1120788
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    sget-object v1, LX/6fx;->LOCATION_IMAGE:LX/6fx;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;-><init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 3

    .prologue
    .line 1120787
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    sget-object v1, LX/6fx;->SPONSORED_MESSAGE_IMAGE:LX/6fx;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;-><init>(LX/6fx;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120782
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1120783
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a:LX/6fx;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1120784
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120785
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120786
    return-void
.end method
