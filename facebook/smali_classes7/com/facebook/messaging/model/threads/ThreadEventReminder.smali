.class public Lcom/facebook/messaging/model/threads/ThreadEventReminder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Z

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120731
    new-instance v0, LX/6fs;

    invoke-direct {v0}, LX/6fs;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ft;)V
    .locals 4

    .prologue
    .line 1120713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120714
    iget-object v0, p1, LX/6ft;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1120715
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    .line 1120716
    iget-object v0, p1, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-object v0, v0

    .line 1120717
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1120718
    iget-wide v2, p1, LX/6ft;->c:J

    move-wide v0, v2

    .line 1120719
    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    .line 1120720
    iget-object v0, p1, LX/6ft;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1120721
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    .line 1120722
    iget-boolean v0, p1, LX/6ft;->e:Z

    move v0, v0

    .line 1120723
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->e:Z

    .line 1120724
    iget-object v0, p1, LX/6ft;->f:LX/0P1;

    move-object v0, v0

    .line 1120725
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    .line 1120726
    iget-boolean v0, p1, LX/6ft;->g:Z

    move v0, v0

    .line 1120727
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    .line 1120728
    iget-object v0, p1, LX/6ft;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1120729
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    .line 1120730
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1120691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120692
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    .line 1120693
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    .line 1120694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->e:Z

    .line 1120695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    .line 1120696
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1120697
    const/4 v0, -0x1

    if-ne v2, v0, :cond_2

    .line 1120698
    const/4 v0, 0x0

    .line 1120699
    :goto_1
    move-object v0, v0

    .line 1120700
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    .line 1120701
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    .line 1120702
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v0

    .line 1120703
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->EVENT:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    :cond_0
    move-object v0, v0

    .line 1120704
    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1120705
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    .line 1120706
    return-void

    .line 1120707
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1120708
    :cond_2
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    .line 1120709
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_3

    .line 1120710
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1120711
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1120712
    :cond_3
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final c()J
    .locals 4

    .prologue
    .line 1120690
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 7

    .prologue
    .line 1120732
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 1120733
    iget-wide v5, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    move-wide v0, v5

    .line 1120734
    iget-wide v5, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    move-wide v2, v5

    .line 1120735
    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120689
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1120688
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1120687
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1120669
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120670
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120671
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120672
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120673
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    .line 1120674
    if-nez v0, :cond_2

    .line 1120675
    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120676
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1120677
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 1120678
    if-nez v0, :cond_4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120679
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120680
    return-void

    .line 1120681
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1120682
    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120683
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1120684
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120685
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 1120686
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
