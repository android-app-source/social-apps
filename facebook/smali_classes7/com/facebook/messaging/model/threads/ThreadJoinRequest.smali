.class public Lcom/facebook/messaging/model/threads/ThreadJoinRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadJoinRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120755
    new-instance v0, LX/6fv;

    invoke-direct {v0}, LX/6fv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1120756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120757
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->a:Lcom/facebook/user/model/UserKey;

    .line 1120758
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->b:J

    .line 1120759
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/UserKey;J)V
    .locals 0

    .prologue
    .line 1120760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120761
    iput-object p1, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->a:Lcom/facebook/user/model/UserKey;

    .line 1120762
    iput-wide p2, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->b:J

    .line 1120763
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120764
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120765
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1120766
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/ThreadJoinRequest;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120767
    return-void
.end method
