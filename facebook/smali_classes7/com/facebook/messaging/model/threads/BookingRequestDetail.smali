.class public Lcom/facebook/messaging/model/threads/BookingRequestDetail;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/model/threads/BookingRequestDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:J

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1120142
    new-instance v0, LX/6fc;

    invoke-direct {v0}, LX/6fc;-><init>()V

    invoke-virtual {v0}, LX/6fc;->a()Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    .line 1120143
    new-instance v0, LX/6fb;

    invoke-direct {v0}, LX/6fb;-><init>()V

    sput-object v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6fc;)V
    .locals 2

    .prologue
    .line 1120102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120103
    iget-object v0, p1, LX/6fc;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    .line 1120104
    iget-object v0, p1, LX/6fc;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    .line 1120105
    iget-object v0, p1, LX/6fc;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 1120106
    iget-object v0, p1, LX/6fc;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    .line 1120107
    iget-wide v0, p1, LX/6fc;->e:J

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    .line 1120108
    iget-object v0, p1, LX/6fc;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->g:Ljava/lang/String;

    .line 1120109
    iget-object v0, p1, LX/6fc;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->h:Ljava/lang/String;

    .line 1120110
    iget-object v0, p1, LX/6fc;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    .line 1120111
    iget-object v0, p1, LX/6fc;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    .line 1120112
    iget-object v0, p1, LX/6fc;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->k:Ljava/lang/String;

    .line 1120113
    iget-object v0, p1, LX/6fc;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->l:Ljava/lang/String;

    .line 1120114
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1120129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    .line 1120131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    .line 1120132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 1120133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    .line 1120134
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    .line 1120135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->g:Ljava/lang/String;

    .line 1120136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->h:Ljava/lang/String;

    .line 1120137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    .line 1120138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    .line 1120139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->k:Ljava/lang/String;

    .line 1120140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->l:Ljava/lang/String;

    .line 1120141
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1120128
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1120115
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120116
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120117
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120118
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120119
    iget-wide v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1120120
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120121
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120122
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120123
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120124
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120125
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1120126
    return-void

    .line 1120127
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
