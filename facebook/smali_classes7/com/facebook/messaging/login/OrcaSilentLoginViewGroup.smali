.class public Lcom/facebook/messaging/login/OrcaSilentLoginViewGroup;
.super Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/auth/login/ui/AuthFragmentViewGroup",
        "<",
        "LX/7hV;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7hV;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1310640
    invoke-direct {p0, p1, p2}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;-><init>(Landroid/content/Context;LX/7hV;)V

    .line 1310641
    const-string v0, "orca:authparam:silent_login_layout"

    const v1, 0x7f030d1f

    invoke-virtual {p0, v0, v1}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getResourceArgument(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1310642
    const v0, 0x7f0d20b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1310643
    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1310644
    invoke-static {p0}, LX/63Z;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1310645
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1310646
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 1310647
    iput v2, v1, LX/108;->a:I

    .line 1310648
    move-object v1, v1

    .line 1310649
    invoke-virtual {p0}, Lcom/facebook/messaging/login/OrcaSilentLoginViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02123c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1310650
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 1310651
    move-object v1, v1

    .line 1310652
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1310653
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1310654
    :cond_0
    return-void
.end method

.method public static createParameterBundle(I)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1310637
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1310638
    const-string v1, "orca:authparam:silent_login_layout"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1310639
    return-object v0
.end method
