.class public Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1310234
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1310235
    invoke-direct {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a()V

    .line 1310236
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1310237
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310238
    invoke-direct {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a()V

    .line 1310239
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1310240
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310241
    invoke-direct {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a()V

    .line 1310242
    return-void
.end method

.method private static a(Landroid/widget/EditText;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1310243
    invoke-virtual {p0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 1310244
    if-eq v1, v0, :cond_0

    .line 1310245
    invoke-virtual {p0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 1310246
    :cond_0
    return v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1310247
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x10102eb

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0WH;->d(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a:I

    .line 1310248
    return-void
.end method

.method private a(I)V
    .locals 10

    .prologue
    .line 1310249
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getChildCount()I

    move-result v2

    .line 1310250
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getPaddingLeft()I

    move-result v3

    .line 1310251
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 1310252
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1310253
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_1

    .line 1310254
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1310255
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 1310256
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1310257
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v3

    .line 1310258
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, p1

    .line 1310259
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xb

    if-ge v8, v9, :cond_0

    move v0, p1

    .line 1310260
    :cond_0
    add-int/2addr v5, v7

    add-int/2addr v6, v0

    invoke-virtual {v4, v7, v0, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 1310261
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1310262
    :cond_2
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1310263
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1310264
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_1

    .line 1310265
    check-cast v0, Landroid/widget/EditText;

    .line 1310266
    if-gez p3, :cond_0

    .line 1310267
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v2, v1

    .line 1310268
    invoke-virtual {v0}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1310269
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1310270
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    .line 1310271
    add-int/2addr v1, p3

    invoke-virtual {v0}, Landroid/widget/EditText;->getTop()I

    move-result v3

    add-int/2addr v1, v3

    .line 1310272
    invoke-static {v0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a(Landroid/widget/EditText;)I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getLineHeight()I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getLineHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 1310273
    add-int v1, p3, p5

    iget v3, p0, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a:I

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 1310274
    int-to-float v0, v0

    sub-float v0, v1, v0

    .line 1310275
    neg-float v1, v2

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    float-to-int v0, v0

    .line 1310276
    invoke-direct {p0, v0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a(I)V

    .line 1310277
    :goto_0
    return-void

    .line 1310278
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getMeasuredHeight()I

    move-result v0

    if-gt v0, p5, :cond_1

    .line 1310279
    iget v0, p0, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->a:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->offsetTopAndBottom(I)V

    .line 1310280
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1310281
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getChildCount()I

    move-result v5

    .line 1310282
    const/4 v1, 0x0

    move v2, v4

    .line 1310283
    :goto_0
    if-ge v2, v5, :cond_7

    .line 1310284
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1310285
    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    .line 1310286
    check-cast v0, Landroid/widget/ImageView;

    .line 1310287
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1310288
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 1310289
    :cond_1
    return-void

    .line 1310290
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1310291
    :cond_3
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1310292
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1310293
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 1310294
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 1310295
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1310296
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 1310297
    int-to-float v3, v3

    int-to-float v0, v0

    div-float v6, v3, v0

    .line 1310298
    mul-float v3, v6, v2

    .line 1310299
    div-float v0, v1, v6

    .line 1310300
    cmpl-float v7, v0, v2

    if-lez v7, :cond_5

    .line 1310301
    mul-float v0, v2, v6

    move v1, v2

    .line 1310302
    :goto_2
    float-to-int v3, v0

    float-to-int v6, v1

    invoke-virtual {p0, v3, v6}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->setMeasuredDimension(II)V

    .line 1310303
    float-to-int v0, v0

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move v3, v4

    .line 1310304
    :goto_3
    if-ge v3, v5, :cond_1

    .line 1310305
    invoke-virtual {p0, v3}, Lcom/facebook/messaging/doodle/AutoResizeImageFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1310306
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v7, 0x8

    if-eq v0, v7, :cond_4

    .line 1310307
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1310308
    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v7, v7

    sub-float v7, v2, v7

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    .line 1310309
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1310310
    invoke-virtual {v4, v6, v0}, Landroid/view/View;->measure(II)V

    .line 1310311
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1310312
    :cond_5
    cmpl-float v6, v3, v1

    if-lez v6, :cond_6

    move v9, v0

    move v0, v1

    move v1, v9

    .line 1310313
    goto :goto_2

    :cond_6
    move v1, v0

    move v0, v3

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_1
.end method
