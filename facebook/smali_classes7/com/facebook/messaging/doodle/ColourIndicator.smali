.class public Lcom/facebook/messaging/doodle/ColourIndicator;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Bitmap;

.field public c:Landroid/graphics/Paint;

.field public d:F

.field private e:F

.field public f:F

.field public g:F

.field private h:Z

.field private i:F

.field private j:Landroid/graphics/Path;

.field public k:LX/0wW;

.field private l:LX/0wd;

.field public m:Z

.field private n:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1310545
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/doodle/ColourIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310546
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1310543
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/doodle/ColourIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310544
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1310525
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310526
    iput-boolean v6, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->h:Z

    .line 1310527
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->i:F

    .line 1310528
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->j:Landroid/graphics/Path;

    .line 1310529
    const-class v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-static {v0, p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1310530
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->ColourIndicator:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1310531
    const/16 v1, 0x0

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->g:F

    .line 1310532
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1310533
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021980

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    .line 1310534
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0250

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->n:F

    .line 1310535
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->a:Landroid/graphics/Paint;

    .line 1310536
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1310537
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    .line 1310538
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1310539
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1310540
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1310541
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->k:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v1, LX/8CD;

    invoke-direct {v1, p0}, LX/8CD;-><init>(Lcom/facebook/messaging/doodle/ColourIndicator;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->l:LX/0wd;

    .line 1310542
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1310520
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1310521
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->j:Landroid/graphics/Path;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1310522
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->j:Landroid/graphics/Path;

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p2

    invoke-virtual {v0, v1, p3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1310523
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->j:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1310524
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->k:LX/0wW;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1310516
    iput-boolean v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->h:Z

    .line 1310517
    iput-boolean v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->m:Z

    .line 1310518
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->l:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1310519
    return-void
.end method

.method public final a(IFFF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1310505
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getPaddingRight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p2, v0

    iget-object v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->n:F

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->getRight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    .line 1310506
    iput p3, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->e:F

    .line 1310507
    iget-boolean v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->h:Z

    if-nez v0, :cond_0

    .line 1310508
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->l:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1310509
    :goto_0
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 1310510
    iput-boolean v4, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->h:Z

    .line 1310511
    iput p4, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->i:F

    .line 1310512
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->i:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1310513
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->invalidate()V

    .line 1310514
    return-void

    .line 1310515
    :cond_0
    iput-boolean v4, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->m:Z

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1310501
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1310502
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    iget v2, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->e:F

    iget-object v3, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1310503
    iget v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->d:F

    iget-object v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x5

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->e:F

    iget-object v2, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V

    .line 1310504
    return-void
.end method

.method public setColour(I)V
    .locals 1

    .prologue
    .line 1310498
    iget-object v0, p0, Lcom/facebook/messaging/doodle/ColourIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1310499
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/ColourIndicator;->invalidate()V

    .line 1310500
    return-void
.end method
