.class public Lcom/facebook/messaging/doodle/CaptionEditorView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/view/GestureDetector;

.field public final b:Landroid/view/inputmethod/InputMethodManager;

.field public c:LX/8CB;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1310460
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1310461
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1310458
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310459
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1310445
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1310446
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/8CC;

    invoke-direct {v1, p0}, LX/8CC;-><init>(Lcom/facebook/messaging/doodle/CaptionEditorView;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->a:Landroid/view/GestureDetector;

    .line 1310447
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 1310448
    new-instance v0, LX/8CB;

    invoke-direct {v0, p1}, LX/8CB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    .line 1310449
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/8CB;->setTextColor(I)V

    .line 1310450
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0, v7}, LX/8CB;->setGravity(I)V

    .line 1310451
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, LX/8CB;->setMaxEms(I)V

    .line 1310452
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0, v6}, LX/8CB;->setSingleLine(Z)V

    .line 1310453
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/high16 v1, 0x40400000    # 3.0f

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a018b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8CB;->setShadowLayer(FFFI)V

    .line 1310454
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1310455
    invoke-virtual {p0, v7}, Lcom/facebook/messaging/doodle/CaptionEditorView;->setFocusableInTouchMode(Z)V

    .line 1310456
    invoke-virtual {p0, v6}, Lcom/facebook/messaging/doodle/CaptionEditorView;->setEnabled(Z)V

    .line 1310457
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/doodle/CaptionEditorView;F)V
    .locals 4

    .prologue
    const/4 v2, -0x2

    .line 1310438
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1310439
    :goto_0
    return-void

    .line 1310440
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x31

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1310441
    float-to-int v1, p1

    iget-object v2, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v2}, LX/8CB;->getLineHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1310442
    iget-object v1, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1310443
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->requestFocus()Z

    .line 1310444
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1310436
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v1}, LX/8CB;->getLineHeight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-static {p0, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->a$redex0(Lcom/facebook/messaging/doodle/CaptionEditorView;F)V

    .line 1310437
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1310435
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->isFocused()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1310462
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1310463
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->h()V

    .line 1310464
    :goto_0
    return-void

    .line 1310465
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1310466
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1310467
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1310468
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/8CB;->setText(Ljava/lang/CharSequence;)V

    .line 1310469
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->removeView(Landroid/view/View;)V

    .line 1310470
    invoke-direct {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->h()V

    goto :goto_0

    .line 1310471
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->requestFocus()Z

    .line 1310472
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    goto :goto_0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1310432
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1310433
    const/4 v0, 0x0

    .line 1310434
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1310430
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->removeView(Landroid/view/View;)V

    .line 1310431
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1310429
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0}, LX/8CB;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1310426
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->b:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1310427
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->requestFocus()Z

    .line 1310428
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1310423
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1310424
    const/4 v0, 0x0

    .line 1310425
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, 0x47ae508c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1310418
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1310419
    const/4 v0, 0x0

    const v2, 0x78ae0dbb

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1310420
    :goto_0
    return v0

    .line 1310421
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1310422
    const v2, 0x73413201

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1310413
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setEnabled(Z)V

    .line 1310414
    iget-object v0, p0, Lcom/facebook/messaging/doodle/CaptionEditorView;->c:LX/8CB;

    invoke-virtual {v0, p1}, LX/8CB;->setEnabled(Z)V

    .line 1310415
    if-nez p1, :cond_0

    .line 1310416
    invoke-virtual {p0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->g()V

    .line 1310417
    :cond_0
    return-void
.end method
