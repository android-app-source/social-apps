.class public final Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final NICKNAME_PAIR_CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129672
    new-instance v0, LX/6ig;

    invoke-direct {v0}, LX/6ig;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->NICKNAME_PAIR_CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129669
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->a:Ljava/lang/String;

    .line 1129670
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->b:Ljava/lang/String;

    .line 1129671
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129664
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129665
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129666
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129667
    return-void
.end method
