.class public Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;


# instance fields
.field public final b:J

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128767
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v0

    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 1128768
    new-instance v0, LX/6hu;

    invoke-direct {v0}, LX/6hu;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6hv;)V
    .locals 4

    .prologue
    .line 1128782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128783
    iget-object v0, p1, LX/6hv;->a:Ljava/util/List;

    move-object v0, v0

    .line 1128784
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    .line 1128785
    iget-boolean v0, p1, LX/6hv;->b:Z

    move v0, v0

    .line 1128786
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    .line 1128787
    iget-wide v2, p1, LX/6hv;->c:J

    move-wide v0, v2

    .line 1128788
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    .line 1128789
    iget-wide v2, p1, LX/6hv;->d:J

    move-wide v0, v2

    .line 1128790
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    .line 1128791
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128777
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    .line 1128778
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    .line 1128779
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    .line 1128780
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    .line 1128781
    return-void
.end method

.method public static newBuilder()LX/6hv;
    .locals 1

    .prologue
    .line 1128775
    new-instance v0, LX/6hv;

    invoke-direct {v0}, LX/6hv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128774
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128769
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1128770
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1128771
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128772
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128773
    return-void
.end method
