.class public Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/messages/Message;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/fbtrace/FbTraceNode;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130195
    new-instance v0, LX/6j0;

    invoke-direct {v0}, LX/6j0;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130190
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1130191
    const-class v0, Lcom/facebook/user/model/UserIdentifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->b:LX/0Px;

    .line 1130192
    const-class v0, Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbtrace/FbTraceNode;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1130193
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/messages/Message;LX/0Px;Lcom/facebook/fbtrace/FbTraceNode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1130184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130185
    iput-object p1, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1130186
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->b:LX/0Px;

    .line 1130187
    iput-object p3, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1130188
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/messages/Message;
    .locals 1

    .prologue
    .line 1130194
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130183
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130179
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130180
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130181
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130182
    return-void
.end method
