.class public Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/model/messages/Message;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130115
    new-instance v0, LX/6ix;

    invoke-direct {v0}, LX/6ix;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->d:Z

    .line 1130118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->a:Ljava/lang/String;

    .line 1130119
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 1130120
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    .line 1130121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1130122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->d:Z

    .line 1130124
    iput-object p1, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->a:Ljava/lang/String;

    .line 1130125
    iput-object p2, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 1130126
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    .line 1130127
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130128
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130129
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130130
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130131
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130132
    return-void
.end method
