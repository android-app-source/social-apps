.class public Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6iD;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final b:LX/6iD;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final c:LX/6iC;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129084
    new-instance v0, LX/6iB;

    invoke-direct {v0}, LX/6iB;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/6iD;LX/6iD;LX/6iC;)V
    .locals 1
    .param p1    # LX/6iD;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # LX/6iD;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # LX/6iC;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1129079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129080
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6iD;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a:LX/6iD;

    .line 1129081
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6iD;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->b:LX/6iD;

    .line 1129082
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6iC;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->c:LX/6iC;

    .line 1129083
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129075
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/6iD;->fromParcelValue(I)LX/6iD;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a:LX/6iD;

    .line 1129076
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/6iD;->fromParcelValue(I)LX/6iD;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->b:LX/6iD;

    .line 1129077
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/6iC;->fromParcelValue(I)LX/6iC;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->c:LX/6iC;

    .line 1129078
    return-void
.end method

.method public static a()Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;
    .locals 4

    .prologue
    .line 1129073
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    sget-object v1, LX/6iD;->CACHE:LX/6iD;

    sget-object v2, LX/6iD;->DATABASE:LX/6iD;

    sget-object v3, LX/6iC;->NOT_IN_MEMORY_CACHE:LX/6iC;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;-><init>(LX/6iD;LX/6iD;LX/6iC;)V

    return-object v0
.end method

.method public static a(LX/6iC;)Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;
    .locals 3
    .param p0    # LX/6iC;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1129064
    sget-object v0, LX/6iC;->NOT_MOSTLY_CACHED:LX/6iC;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/6iC;->NEED_MORE_RECENT_MESSAGES:LX/6iC;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/6iC;->NEED_OLDER_MESSAGES:LX/6iC;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1129065
    new-instance v0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    sget-object v1, LX/6iD;->DATABASE:LX/6iD;

    sget-object v2, LX/6iD;->SERVER:LX/6iD;

    invoke-direct {v0, v1, v2, p0}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;-><init>(LX/6iD;LX/6iD;LX/6iC;)V

    return-object v0

    .line 1129066
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129072
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1129071
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a:LX/6iD;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->b:LX/6iD;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->c:LX/6iC;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129067
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a:LX/6iD;

    iget v0, v0, LX/6iD;->parcelValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129068
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->b:LX/6iD;

    iget v0, v0, LX/6iD;->parcelValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129069
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->c:LX/6iC;

    iget v0, v0, LX/6iC;->parcelValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129070
    return-void
.end method
