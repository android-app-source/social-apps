.class public Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field private final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129574
    const-string v0, "markMontageNuxAsReadParams"

    sput-object v0, Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;->a:Ljava/lang/String;

    .line 1129575
    new-instance v0, LX/6iY;

    invoke-direct {v0}, LX/6iY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1129576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129577
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;->b:J

    .line 1129578
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129573
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129571
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/MarkMontageNuxAsReadParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129572
    return-void
.end method
