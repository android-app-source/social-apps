.class public Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130036
    new-instance v0, LX/6it;

    invoke-direct {v0}, LX/6it;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6iu;)V
    .locals 6

    .prologue
    .line 1130050
    iget-object v0, p1, LX/6iu;->a:LX/0ta;

    move-object v0, v0

    .line 1130051
    iget-wide v4, p1, LX/6iu;->d:J

    move-wide v2, v4

    .line 1130052
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 1130053
    iget-object v0, p1, LX/6iu;->a:LX/0ta;

    move-object v0, v0

    .line 1130054
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130055
    iget-object v0, p1, LX/6iu;->b:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v0, v0

    .line 1130056
    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1130057
    iget-object v0, p1, LX/6iu;->c:Ljava/util/List;

    move-object v0, v0

    .line 1130058
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->b:LX/0Px;

    .line 1130059
    iget-wide v4, p1, LX/6iu;->e:J

    move-wide v0, v4

    .line 1130060
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->c:J

    .line 1130061
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1130044
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1130045
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1130046
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->b:LX/0Px;

    .line 1130047
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->c:J

    .line 1130048
    return-void
.end method

.method public static a()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
    .locals 1

    .prologue
    .line 1130049
    invoke-static {}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->newBuilder()LX/6iu;

    move-result-object v0

    invoke-virtual {v0}, LX/6iu;->f()Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder()LX/6iu;
    .locals 1

    .prologue
    .line 1130043
    new-instance v0, LX/6iu;

    invoke-direct {v0}, LX/6iu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130042
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1130037
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1130038
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130039
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130040
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1130041
    return-void
.end method
