.class public Lcom/facebook/messaging/service/model/FetchThreadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

.field public final b:LX/0rS;

.field public final c:LX/0rS;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:J

.field public final g:I

.field public final h:J

.field public final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129307
    new-instance v0, LX/6iL;

    invoke-direct {v0}, LX/6iL;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6iM;)V
    .locals 4

    .prologue
    .line 1129340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129341
    iget-object v0, p1, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v0

    .line 1129342
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 1129343
    iget-object v0, p1, LX/6iM;->b:LX/0rS;

    move-object v0, v0

    .line 1129344
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    .line 1129345
    iget-object v0, p1, LX/6iM;->c:LX/0rS;

    if-nez v0, :cond_0

    .line 1129346
    iget-object v0, p1, LX/6iM;->b:LX/0rS;

    .line 1129347
    :goto_0
    move-object v0, v0

    .line 1129348
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    .line 1129349
    iget-object v0, p1, LX/6iM;->d:LX/0Px;

    move-object v0, v0

    .line 1129350
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->d:LX/0Px;

    .line 1129351
    iget-boolean v0, p1, LX/6iM;->f:Z

    move v0, v0

    .line 1129352
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->e:Z

    .line 1129353
    iget-wide v2, p1, LX/6iM;->e:J

    move-wide v0, v2

    .line 1129354
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->f:J

    .line 1129355
    iget v0, p1, LX/6iM;->g:I

    move v0, v0

    .line 1129356
    iput v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    .line 1129357
    iget-wide v2, p1, LX/6iM;->h:J

    move-wide v0, v2

    .line 1129358
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->h:J

    .line 1129359
    iget-boolean v0, p1, LX/6iM;->i:Z

    move v0, v0

    .line 1129360
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    .line 1129361
    return-void

    :cond_0
    iget-object v0, p1, LX/6iM;->c:LX/0rS;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1129327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129328
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadCriteria;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 1129329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    .line 1129330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    .line 1129331
    sget-object v0, Lcom/facebook/user/model/User;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->d:LX/0Px;

    .line 1129332
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->e:Z

    .line 1129333
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->f:J

    .line 1129334
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    .line 1129335
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->h:J

    .line 1129336
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    .line 1129337
    return-void

    :cond_0
    move v0, v2

    .line 1129338
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1129339
    goto :goto_1
.end method

.method public static newBuilder()LX/6iM;
    .locals 1

    .prologue
    .line 1129326
    new-instance v0, LX/6iM;

    invoke-direct {v0}, LX/6iM;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/threads/ThreadCriteria;
    .locals 1

    .prologue
    .line 1129325
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1129324
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->d:LX/0Px;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129323
    const/4 v0, 0x0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1129322
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1129321
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1129320
    const-class v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "threadCriteria"

    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dataFreshness"

    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "originalDataFreshness"

    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "updateLastRead"

    iget-boolean v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "updateLastReadTimeActionId"

    iget-wide v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->f:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "numToFetch"

    iget v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "sinceActionId"

    iget-wide v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->h:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "shouldTraceFetch"

    iget-boolean v2, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1129308
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129309
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129310
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129311
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->d:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0Px;)V

    .line 1129312
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129313
    iget-wide v4, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->f:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129314
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129315
    iget-wide v4, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->h:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129316
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadParams;->i:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129317
    return-void

    :cond_0
    move v0, v2

    .line 1129318
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1129319
    goto :goto_1
.end method
