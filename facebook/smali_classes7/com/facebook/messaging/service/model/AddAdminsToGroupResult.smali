.class public Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1128439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128440
    iput-boolean p1, p0, Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;->a:Z

    .line 1128441
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128442
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128443
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/AddAdminsToGroupResult;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1128444
    return-void
.end method
