.class public Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128836
    new-instance v0, LX/6hz;

    invoke-direct {v0}, LX/6hz;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128838
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->a:Z

    .line 1128839
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1128840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128841
    iput-boolean p1, p0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->a:Z

    .line 1128842
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128843
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128844
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1128845
    return-void
.end method
